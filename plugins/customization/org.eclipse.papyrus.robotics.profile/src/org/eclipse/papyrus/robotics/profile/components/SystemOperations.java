/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for System stereotype
 */
public class SystemOperations {

	/**
	 * @param sys
	 *            a Robotics system
	 * @return the instances defined in the passed system
	 */
	public static EList<ComponentInstance> getInstances(System sys) {
		EList<ComponentInstance> ciList = new UniqueEList<>();
		if (sys.getBase_Class() != null) {
			for (Property property : sys.getBase_Class().getAllAttributes()) {
				ComponentInstance ci = UMLUtil.getStereotypeApplication(property, ComponentInstance.class);
				if (ci != null) {
					ciList.add(ci);
				}
			}
		}
		return ciList;
	}

}
