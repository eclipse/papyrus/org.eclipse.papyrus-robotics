/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterInstance;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for ComponentInstance stereotype
 */
public class ComponentInstanceOperations {

	/**
	 * @param ci
	 *            a component instance
	 * @return the component or system (type) of that instance
	 */
	public static ComponentOrSystem getCompdefOrSys(ComponentInstance ci) {
		if (ci.getBase_Property() != null) {
			Type type = ci.getBase_Property().getType();
			if (type != null) {
				return UMLUtil.getStereotypeApplication(type, ComponentOrSystem.class);
			}
		}
		return null;
	}

	/**
	 * @param ci
	 *            a component instance
	 * @return the list of ports provided by the type of the instance
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static EList<ComponentPort> getPort(ComponentInstance ci) {
		ComponentOrSystem definition = getCompdefOrSys(ci);
		if (definition != null) {
			// CompDef.ports subsets BCP ports
			return (EList) definition.getPort();
		}
		return new BasicEList<>();
	}

	/**
	 * @param ci
	 *            a component instance
	 * @return the parameter instance associated with the component instance if there is one (null otherwise)
	 */
	public static ParameterInstance getParamInstance(ComponentInstance ci) {
		if (ci.getBase_Property() != null) {
			ValueSpecification vs = ci.getBase_Property().getDefaultValue();
			if (vs instanceof InstanceValue) {
				InstanceSpecification is = ((InstanceValue) vs).getInstance();
				return UMLUtil.getStereotypeApplication(is, ParameterInstance.class);
			}
		}
		return null;
	}

}
