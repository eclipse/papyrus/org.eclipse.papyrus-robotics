/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.skills;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for SkillDefinitionSet stereotype
 */
public class SkillDefinitionSetOperations {

	/**
	 * Get the skill definitions, i.e. all operations of an
	 * interface representing a skill definition set
	 * 
	 * @param sds
	 *            a skill definition set
	 * @return the contained skill definitions
	 */
	public static EList<SkillDefinition> getSkills(SkillDefinitionSet sds) {
		EList<SkillDefinition> skills = new BasicEList<>();
		for (Operation operation : sds.getBase_Interface().getOperations()) {
			SkillDefinition skill = UMLUtil.getStereotypeApplication(operation, SkillDefinition.class);
			if (skill != null) {
				skills.add(skill);
			}
		}
		return skills;
	}
}
