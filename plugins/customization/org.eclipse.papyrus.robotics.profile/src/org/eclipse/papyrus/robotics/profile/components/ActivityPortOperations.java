/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for ActivityPort stereotype
 */
public class ActivityPortOperations {

	/**
	 * Calculate list of functions associated with an activity port
	 * 
	 * @param ap
	 *            an activity port (Robotics stereotype)
	 * @return the list of functions (Robotics stereotype) associated with the activity port
	 */
	public static EList<Function> getFunctions(ActivityPort ap) {
		EList<Function> functionList = new BasicEList<>();
		org.eclipse.uml2.uml.Class activityCl = ap.getBase_Port().getClass_();
		Activity activity = UMLUtil.getStereotypeApplication(activityCl, Activity.class);
		if (activity != null) {
			for (Function function : activity.getFunctions()) {
				if (function.getActivityPort() == ap) {
					functionList.add(function);
				}
			}
		}
		return functionList;
	}

}
