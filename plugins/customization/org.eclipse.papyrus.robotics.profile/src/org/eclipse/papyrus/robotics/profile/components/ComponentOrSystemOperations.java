/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for abstract ComponentOrSystem stereotype
 */
public class ComponentOrSystemOperations {

	/**
	 * @param cos
	 *            a component or system
	 * @return the list of provided or required component-services
	 */
	public static EList<ComponentService> getServices(ComponentOrSystem cos) {
		EList<ComponentService> serviceList = new UniqueEList<>();
		if (cos.getBase_Class() != null) {
			for (Port port : cos.getBase_Class().getOwnedPorts()) {
				Type type = port.getType();
				if (type != null) {
					ComponentService service = UMLUtil.getStereotypeApplication(type, ComponentService.class);
					if (service != null) {
						serviceList.add(service);
					}
				}
			}
		}
		return serviceList;
	}

}
