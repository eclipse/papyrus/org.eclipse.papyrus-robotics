/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.services;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for Service stereotype
 */
public class ServiceOperations {
	/**
	 * Used by CoordinationService and ServiceDefinition
	 *
	 * @param sd
	 *            a service definition
	 * @return the list of service properties of the passed definition
	 */
	public static EList<ServiceProperty> getSvcProperty(Interface sd) {
		EList<ServiceProperty> svcPropList = new UniqueEList<>();
		if (sd != null) {
			for (Property property : sd.getAllAttributes()) {
				ServiceProperty svcProp = UMLUtil.getStereotypeApplication(property, ServiceProperty.class);
				if (svcProp != null) {
					svcPropList.add(svcProp);
				}
			}
		}
		return svcPropList;
	}

}
