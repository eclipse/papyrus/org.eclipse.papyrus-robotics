/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.behavior;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for Task stereotype
 */
public class TaskOperations {
	public static EList<Task> getTask(Task task) {
		EList<Task> tasks = new BasicEList<>();
		// In the robotics profile, Task extends UML::Behavior
		// We need to manage the different kinds of UML::Behaviors to get the list of (sub)tasks
		//
		// case 1 (the only one supported at today (2020): UML::Activity
		if (task.getBase_Behavior() instanceof Activity) {
			Activity act = (Activity) task.getBase_Behavior();
			for (ActivityNode node : act.getOwnedNodes()) {
				// Only nodes with incoming edges are considered to return a list of (sub) Tasks that are really used in the specification of the (main) task
				if (!node.getIncomings().isEmpty()) {
					// We look for (sub) Tasks by looking at UML::Activities referenced by CallBehaviorActions
					if (node instanceof CallBehaviorAction) {
						Task tsk = UMLUtil.getStereotypeApplication(((CallBehaviorAction) node).getBehavior(), Task.class);
						if (tsk != null) {
							tasks.add(tsk);
						}
					}
				}
			}
		}
		return tasks;
	}
}
