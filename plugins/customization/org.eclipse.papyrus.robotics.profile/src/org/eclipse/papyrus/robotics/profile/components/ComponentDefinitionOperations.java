/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for SkillDefinition stereotype
 */
public class ComponentDefinitionOperations {

	/**
	 * @param cd
	 *            a component definition
	 * @return a list of contained activities
	 */
	public static EList<Activity> getActivities(ComponentDefinition cd) {
		EList<Activity> activityList = new UniqueEList<>();
		if (cd.getBase_Class() != null) {
			for (Classifier ne : cd.getBase_Class().getNestedClassifiers()) {
				Activity activity = UMLUtil.getStereotypeApplication(ne, Activity.class);
				if (activity != null) {
					activityList.add(activity);
				}
			}
		}
		return activityList;
	}

	/**
	 * @param cd
	 *            a component definition
	 * @return a list of defined parameters
	 */
	public static Parameter getParameter(ComponentDefinition cd) {
		Parameter param = null;
		if (cd.getBase_Class() != null) {
			for (Classifier cl : cd.getBase_Class().getNestedClassifiers()) {
				Parameter p = UMLUtil.getStereotypeApplication(cl, Parameter.class);
				if (p != null) {
					param = p;
				}
			}
		}
		return param;
	}

}
