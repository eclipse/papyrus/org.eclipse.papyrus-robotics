/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.skills;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFailState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSuccessState;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for SkillSemantic stereotype
 */
public class SkillSemanticOperations {

	/**
	 * @param skillSem
	 *            a skill semantics
	 * @return the defined SkillFailState
	 */
	public static SkillFailState getFail(SkillSemantic skillSem) {
		SkillFailState res = null;
		EList<Region> regions = skillSem.getBase_StateMachine().getRegions();
		if (!regions.isEmpty()) {
			EList<Vertex> vertices = regions.get(0).getSubvertices();
			for (Vertex v : vertices) {
				res = UMLUtil.getStereotypeApplication(v, SkillFailState.class);
				if (res != null) {
					break;
				}
			}
		}
		return res;
	}

	/**
	 * @param skillSem
	 *            a skill semantics
	 * @return the list of operational-states
	 */
	public static EList<SkillOperationalState> getOperational(SkillSemantic skillSem) {
		EList<SkillOperationalState> list = new BasicEList<>();
		EList<Region> regions = skillSem.getBase_StateMachine().getRegions();
		if (!regions.isEmpty()) {
			EList<Vertex> vertices = regions.get(0).getSubvertices();
			for (Vertex v : vertices) {
				SkillOperationalState ops = UMLUtil.getStereotypeApplication(v, SkillOperationalState.class);
				if (ops != null) {
					list.add(ops);
				}
			}
		}
		return list;
	}

	/**
	 * @param skillSem
	 *            a skill semantics
	 * @return the skill-success state
	 */
	public static SkillSuccessState getSuccess(SkillSemantic skillSem) {
		SkillSuccessState res = null;
		EList<Region> regions = skillSem.getBase_StateMachine().getRegions();
		if (!regions.isEmpty()) {
			EList<Vertex> vertices = regions.get(0).getSubvertices();
			for (Vertex v : vertices) {
				res = UMLUtil.getStereotypeApplication(v, SkillSuccessState.class);
				if (res != null) {
					break;
				}
			}
		}
		return res;
	}

	/**
	 * @param skillSem
	 *            a skill semantics
	 * @return the list of "fail" coordination events
	 */
	public static EList<CoordinationEvent> getFailEvts(SkillSemantic skillSem) {
		return getSuccOrFailEvts(skillSem, SkillFailState.class);
	}

	/**
	 * @param skillSem
	 *            a skill semantics
	 * @return the list of "success" coordination events
	 */
	public static EList<CoordinationEvent> getSuccEvts(SkillSemantic skillSem) {
		return getSuccOrFailEvts(skillSem, SkillSuccessState.class);
	}

	/**
	 * Internal helper (avoid code duplication)
	 * 
	 * @param skillSem
	 *            a skill semantics
	 * @param stereotype
	 *            success or fail
	 * @return the list of coordination-events depending on passed stereotypes
	 */
	protected static EList<CoordinationEvent> getSuccOrFailEvts(SkillSemantic skillSem, Class<? extends EObject> stereotype) {
		EList<CoordinationEvent> events = new BasicEList<>();
		EList<Region> regions = skillSem.getBase_StateMachine().getRegions();
		if (!regions.isEmpty()) {
			EList<Transition> transitions = regions.get(0).getTransitions();
			// check all the transitions without break;
			// depending on the modeling style, one can define 1 transition with all the events causing it
			// or many different transitions to the same state each one with 1 (or more) events causing it
			for (Transition t : transitions) {
				// if t goes to a fail state
				EObject fail = UMLUtil.getStereotypeApplication(t.getTarget(), stereotype);
				if (fail != null) {
					for (Trigger g : t.getTriggers()) {
						// check all the triggers, add each coordination event to the result list
						CoordinationEvent ce = UMLUtil.getStereotypeApplication(g.getEvent(), CoordinationEvent.class);
						if (ce != null) {
							events.add(ce);
						}
					}
				}
			}
		}
		return events;
	}
}
