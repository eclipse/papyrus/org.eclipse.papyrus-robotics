/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.commobj;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.DataAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.DataType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for DataType stereotype
 */
public class DataTypeOperations {

	/**
	 * @param dt
	 *            a data type
	 * @return the (stereotyped) attributes of a data-type
	 */
	public static EList<DataAttribute> getAttributes(DataType dt) {
		EList<DataAttribute> daList = new BasicEList<>();
		for (Property p : dt.getBase_DataType().getOwnedAttributes()) {
			DataAttribute da = UMLUtil.getStereotypeApplication(p, DataAttribute.class);
			if (da != null) {
				daList.add(da);
			}
		}
		return daList;
	}
}
