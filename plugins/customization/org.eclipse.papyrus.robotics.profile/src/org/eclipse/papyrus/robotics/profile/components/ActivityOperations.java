/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for Activity stereotype
 */
public class ActivityOperations {

	/**
	 * Calculate list of functions via attributes of base Class
	 * 
	 * @param activity
	 *            an activity (Robotics stereotype)
	 * @return the list of functions (Robotics stereotype) associated with the activity
	 */
	public static EList<Function> getFunctions(Activity activity) {
		EList<Function> functionList = new BasicEList<>();
		if (activity.getBase_Class() != null) {
			for (Property attribute : activity.getBase_Class().getOwnedAttributes()) {
				if (attribute.getType() != null) {
					Function function = UMLUtil.getStereotypeApplication(attribute.getType(), Function.class);
					if (function != null) {
						functionList.add(function);
					}
				}
			}
		}
		return functionList;
	}

}
