/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.services;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for ComponentService stereotype
 */
public class ComponentServiceOperations {

	/**
	 * @param cs
	 *            a component-service
	 * @return the list of service-definitions referenced by the component-service
	 */
	public static EList<ServiceDefinition> getSvcDefinitions(ComponentService cs) {
		EList<ServiceDefinition> svcDefList = new UniqueEList<>();
		if (cs.getBase_Class() != null) {
			for (DirectedRelationship dr : cs.getBase_Class().getSourceDirectedRelationships()) {
				if ((dr instanceof Realization || dr instanceof Usage) && dr.getTargets().size() > 0) {
					ServiceDefinition svcDef = UMLUtil.getStereotypeApplication(dr.getTargets().get(0), ServiceDefinition.class);
					svcDefList.add(svcDef);
				}
			}
		}
		return svcDefList;
	}

}
