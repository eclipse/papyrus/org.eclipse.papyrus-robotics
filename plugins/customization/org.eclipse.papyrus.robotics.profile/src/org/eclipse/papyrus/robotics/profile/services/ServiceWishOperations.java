/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.services;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWishProperty;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for ServiceWish stereotype
 */
public class ServiceWishOperations {
	/**
	 * Used by CoordinationService and ServiceDefinition
	 *
	 * @param sw
	 *            a service wish
	 * @return the list of service-wish properties of the passed service-wish
	 */
	public static EList<ServiceWishProperty> getProperties(ServiceWish sw) {
			EList<ServiceWishProperty> swList = new BasicEList<ServiceWishProperty>();
			for (Slot slot : sw.getBase_InstanceSpecification().getSlots()) {
				ServiceWishProperty wishProperty = UMLUtil.getStereotypeApplication(slot, ServiceWishProperty.class);
				if (wishProperty != null) {
					swList.add(wishProperty);
				}
			}
			return swList;
	}
}
