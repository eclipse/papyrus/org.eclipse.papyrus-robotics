/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.components;

import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for ComponentPort stereotype
 */
public class ComponentPortOperations {

	/**
	 * @param port
	 *            a component port
	 * @return a provided service definition if there is one (null otherwise)
	 */
	public static ServiceDefinition getProvides(ComponentPort port) {
		if (port.getBase_Port() != null) {
			Type type = port.getBase_Port().getType();
			if (type instanceof org.eclipse.uml2.uml.Class && UMLUtil.getStereotypeApplication(type, ComponentService.class) != null) {
				for (DirectedRelationship dr : type.getSourceDirectedRelationships()) {
					if (dr instanceof Realization && dr.getTargets().size() > 0) {
						ServiceDefinition svcDef = UMLUtil.getStereotypeApplication(dr.getTargets().get(0), ServiceDefinition.class);
						return svcDef;
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param port
	 *            a component port
	 * @return a required service definition if there is one (null otherwise)
	 */
	public static ServiceDefinition getRequires(ComponentPort port) {
		if (port.getBase_Port() != null) {
			Type type = port.getBase_Port().getType();
			if (type instanceof org.eclipse.uml2.uml.Class && UMLUtil.getStereotypeApplication(type, ComponentService.class) != null) {
				for (DirectedRelationship dr : type.getSourceDirectedRelationships()) {
					if (dr instanceof Usage && dr.getTargets().size() > 0) {
						ServiceDefinition svcDef = UMLUtil.getStereotypeApplication(dr.getTargets().get(0), ServiceDefinition.class);
						return svcDef;
					}
				}
			}
		}
		return null;
	}
}
