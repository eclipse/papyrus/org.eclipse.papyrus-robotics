/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.skills;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for SkillDefinition stereotype
 */
public class SkillDefinitionOperations {

	/**
	 * Return the default semantics given by the first method of an
	 * skill definition (UML operation)
	 * 
	 * @param sd
	 *            a skill definition
	 * @return the default semantics
	 */
	public static SkillSemantic getDefaultSemantic(SkillDefinition sd) {
		SkillSemantic res = null;
		EList<Behavior> semantic_elem = sd.getBase_Operation().getMethods();
		if (!semantic_elem.isEmpty()) {
			res = UMLUtil.getStereotypeApplication(semantic_elem.get(0), SkillSemantic.class);
		}
		return res;
	}

	/**
	 * @param sd
	 *            a skill definition
	 * @return a list of in-attributes
	 */
	public static EList<InAttribute> getIns(SkillDefinition sd) {
		EList<InAttribute> list = new BasicEList<>();
		for (Parameter param : sd.getBase_Operation().getOwnedParameters()) {
			if (param.getDirection() == ParameterDirectionKind.IN_LITERAL
					|| param.getDirection() == ParameterDirectionKind.INOUT_LITERAL) {
				InAttribute ia = UMLUtil.getStereotypeApplication(param, InAttribute.class);
				if (ia != null) {
					list.add(ia);
				}
			}
		}
		return list;
	}

	/**
	 * @param sd
	 *            a skill definition
	 * @return a list of out-attributes
	 */
	public static EList<OutAttribute> getOuts(SkillDefinition sd) {
		EList<OutAttribute> list = new BasicEList<>();
		for (Parameter param : sd.getBase_Operation().getOwnedParameters()) {
			if (param.getDirection() == ParameterDirectionKind.OUT_LITERAL
					|| param.getDirection() == ParameterDirectionKind.INOUT_LITERAL) {
				OutAttribute oa = UMLUtil.getStereotypeApplication(param, OutAttribute.class);
				if (oa != null) {
					list.add(oa);
				}
			}
		}
		return list;
	}

	/**
	 * @param sd
	 *            a skill definition
	 * @return a list of skill results
	 */
	public static EList<SkillResult> getRes(SkillDefinition sd) {
		EList<SkillResult> list = new BasicEList<>();
		for (Parameter param : sd.getBase_Operation().getOwnedParameters()) {
			if (param.getDirection() == ParameterDirectionKind.RETURN_LITERAL) {
				SkillResult res = UMLUtil.getStereotypeApplication(param, SkillResult.class);
				if (res != null) {
					list.add(res);
				}
			}
		}
		return list;
	}
}
