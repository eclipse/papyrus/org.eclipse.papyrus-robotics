/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.profile.commobj;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.Enumeration;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.EnumerationLiteral;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for Enumeration stereotype
 */
public class EnumerationOperations {
	/**
	 * @param enumeration
	 *            an enumeration (Robotics stereotype)
	 * @return stereotyped literals from UML literals
	 */
	public static EList<EnumerationLiteral> getLiterals(Enumeration enumeration) {
		EList<EnumerationLiteral> elList = new BasicEList<>();
		for (org.eclipse.uml2.uml.EnumerationLiteral enumLit :
				((org.eclipse.uml2.uml.Enumeration) enumeration.getBase_DataType()).getOwnedLiterals()) {
			EnumerationLiteral da = UMLUtil.getStereotypeApplication(enumLit, EnumerationLiteral.class);
			if (da != null) {
				elList.add(da);
			}
		}
		return elList;
	}
}
