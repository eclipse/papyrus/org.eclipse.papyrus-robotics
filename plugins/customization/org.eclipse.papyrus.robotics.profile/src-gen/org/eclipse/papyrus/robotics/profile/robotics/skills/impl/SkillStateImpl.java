/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Skill State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillStateImpl#getBase_State <em>Base State</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillStateImpl#getFsm <em>Fsm</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SkillStateImpl extends BlockImpl implements SkillState {
	/**
	 * The cached value of the '{@link #getBase_State() <em>Base State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_State()
	 * @generated
	 * @ordered
	 */
	protected State base_State;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SkillStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SkillsPackage.Literals.SKILL_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public State getBase_State() {
		if (base_State != null && base_State.eIsProxy()) {
			InternalEObject oldBase_State = (InternalEObject)base_State;
			base_State = (State)eResolveProxy(oldBase_State);
			if (base_State != oldBase_State) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SkillsPackage.SKILL_STATE__BASE_STATE, oldBase_State, base_State));
			}
		}
		return base_State;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetBase_State() {
		return base_State;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_State(State newBase_State) {
		State oldBase_State = base_State;
		base_State = newBase_State;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SkillsPackage.SKILL_STATE__BASE_STATE, oldBase_State, base_State));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillSemantic getFsm() {
		SkillSemantic fsm = basicGetFsm();
		return fsm != null && fsm.eIsProxy() ? (SkillSemantic)eResolveProxy((InternalEObject)fsm) : fsm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SkillSemantic basicGetFsm() {
		SkillSemantic res = null;
		for (Classifier context : getBase_State().getRedefinitionContexts()) {
			if (context instanceof StateMachine) {
				res = UMLUtil.getStereotypeApplication((StateMachine) context, SkillSemantic.class);
				if (res != null)
					break;
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFsm(SkillSemantic newFsm) {
		// TODO: implement this method to set the 'Fsm' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SkillsPackage.SKILL_STATE__BASE_STATE:
				if (resolve) return getBase_State();
				return basicGetBase_State();
			case SkillsPackage.SKILL_STATE__FSM:
				if (resolve) return getFsm();
				return basicGetFsm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SkillsPackage.SKILL_STATE__BASE_STATE:
				setBase_State((State)newValue);
				return;
			case SkillsPackage.SKILL_STATE__FSM:
				setFsm((SkillSemantic)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_STATE__BASE_STATE:
				setBase_State((State)null);
				return;
			case SkillsPackage.SKILL_STATE__FSM:
				setFsm((SkillSemantic)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_STATE__BASE_STATE:
				return base_State != null;
			case SkillsPackage.SKILL_STATE__FSM:
				return basicGetFsm() != null;
		}
		return super.eIsSet(featureID);
	}

} //SkillStateImpl
