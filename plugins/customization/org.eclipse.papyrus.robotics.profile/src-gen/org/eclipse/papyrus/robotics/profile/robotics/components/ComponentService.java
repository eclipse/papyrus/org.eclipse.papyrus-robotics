/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.components;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * ComponentService extends class. A service corresponds to the type of a port. The service will (aligned with the standard UML rules) provide or require a ServiceDefinition (interface) via the interface-realization or <<use>> dependency, respectively. Thus, it becomes a derived information of the component definition (and in turn has a derived relationship to a service definition)
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService#getSvcDefinitions <em>Svc Definitions</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService#getConfiguration <em>Configuration</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage#getComponentService()
 * @model
 * @generated
 */
public interface ComponentService extends Entity {
	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage#getComponentService_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Svc Definitions</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Svc Definitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Svc Definitions</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage#getComponentService_SvcDefinitions()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<ServiceDefinition> getSvcDefinitions();

	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' reference.
	 * @see #setConfiguration(ServiceConfiguration)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage#getComponentService_Configuration()
	 * @model ordered="false"
	 * @generated
	 */
	ServiceConfiguration getConfiguration();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService#getConfiguration <em>Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(ServiceConfiguration value);

} // ComponentService
