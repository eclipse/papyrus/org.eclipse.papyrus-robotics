/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFailState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSuccessState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.robotics.profile.skills.SkillSemanticOperations;
import org.eclipse.uml2.uml.StateMachine;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Skill Semantic</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl#getSuccess <em>Success</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl#getFail <em>Fail</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl#getOperational <em>Operational</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl#getSuccEvts <em>Succ Evts</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl#getFailEvts <em>Fail Evts</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl#getBase_StateMachine <em>Base State Machine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SkillSemanticImpl extends BlockImpl implements SkillSemantic {
	/**
	 * The cached value of the '{@link #getBase_StateMachine() <em>Base State Machine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_StateMachine()
	 * @generated
	 * @ordered
	 */
	protected StateMachine base_StateMachine;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SkillSemanticImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SkillsPackage.Literals.SKILL_SEMANTIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillSuccessState getSuccess() {
		SkillSuccessState success = basicGetSuccess();
		return success != null && success.eIsProxy() ? (SkillSuccessState)eResolveProxy((InternalEObject)success) : success;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SkillSuccessState basicGetSuccess() {
		return SkillSemanticOperations.getSuccess(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setSuccess(SkillSuccessState newSuccess) {
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillFailState getFail() {
		SkillFailState fail = basicGetFail();
		return fail != null && fail.eIsProxy() ? (SkillFailState)eResolveProxy((InternalEObject)fail) : fail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SkillFailState basicGetFail() {
		return SkillSemanticOperations.getFail(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<SkillOperationalState> getOperational() {
		return SkillSemanticOperations.getOperational(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<CoordinationEvent> getSuccEvts() {
		return SkillSemanticOperations.getSuccEvts(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<CoordinationEvent> getFailEvts() {
		return SkillSemanticOperations.getFailEvts(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StateMachine getBase_StateMachine() {
		if (base_StateMachine != null && base_StateMachine.eIsProxy()) {
			InternalEObject oldBase_StateMachine = (InternalEObject)base_StateMachine;
			base_StateMachine = (StateMachine)eResolveProxy(oldBase_StateMachine);
			if (base_StateMachine != oldBase_StateMachine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SkillsPackage.SKILL_SEMANTIC__BASE_STATE_MACHINE, oldBase_StateMachine, base_StateMachine));
			}
		}
		return base_StateMachine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine basicGetBase_StateMachine() {
		return base_StateMachine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_StateMachine(StateMachine newBase_StateMachine) {
		StateMachine oldBase_StateMachine = base_StateMachine;
		base_StateMachine = newBase_StateMachine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SkillsPackage.SKILL_SEMANTIC__BASE_STATE_MACHINE, oldBase_StateMachine, base_StateMachine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SkillsPackage.SKILL_SEMANTIC__SUCCESS:
				if (resolve) return getSuccess();
				return basicGetSuccess();
			case SkillsPackage.SKILL_SEMANTIC__FAIL:
				if (resolve) return getFail();
				return basicGetFail();
			case SkillsPackage.SKILL_SEMANTIC__OPERATIONAL:
				return getOperational();
			case SkillsPackage.SKILL_SEMANTIC__SUCC_EVTS:
				return getSuccEvts();
			case SkillsPackage.SKILL_SEMANTIC__FAIL_EVTS:
				return getFailEvts();
			case SkillsPackage.SKILL_SEMANTIC__BASE_STATE_MACHINE:
				if (resolve) return getBase_StateMachine();
				return basicGetBase_StateMachine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SkillsPackage.SKILL_SEMANTIC__BASE_STATE_MACHINE:
				setBase_StateMachine((StateMachine)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_SEMANTIC__BASE_STATE_MACHINE:
				setBase_StateMachine((StateMachine)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_SEMANTIC__SUCCESS:
				return basicGetSuccess() != null;
			case SkillsPackage.SKILL_SEMANTIC__FAIL:
				return basicGetFail() != null;
			case SkillsPackage.SKILL_SEMANTIC__OPERATIONAL:
				return !getOperational().isEmpty();
			case SkillsPackage.SKILL_SEMANTIC__SUCC_EVTS:
				return !getSuccEvts().isEmpty();
			case SkillsPackage.SKILL_SEMANTIC__FAIL_EVTS:
				return !getFailEvts().isEmpty();
			case SkillsPackage.SKILL_SEMANTIC__BASE_STATE_MACHINE:
				return base_StateMachine != null;
		}
		return super.eIsSet(featureID);
	}

} //SkillSemanticImpl
