/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.behavior;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorFactory
 * @model kind="package"
 * @generated
 */
public interface BehaviorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "behavior"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/behavior/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "robotics.behavior"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BehaviorPackage eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 4;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorDefinitionImpl <em>Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorDefinitionImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getBehaviorDefinition()
	 * @generated
	 */
	int BEHAVIOR_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__TASK = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comp Arch</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__COMP_ARCH = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Behaviored Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION__BASE_BEHAVIORED_CLASSIFIER = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_DEFINITION_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskParameterImpl <em>Task Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskParameterImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getTaskParameter()
	 * @generated
	 */
	int TASK_PARAMETER = 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__PROPERTY = BPCPackage.PORT__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__INSTANCE_UID = BPCPackage.PORT__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__DESCRIPTION = BPCPackage.PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__AUTHORSHIP = BPCPackage.PORT__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__PROVENANCE = BPCPackage.PORT__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__MODEL_UID = BPCPackage.PORT__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__METAMODEL_UID = BPCPackage.PORT__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER__BASE_PARAMETER = BPCPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Task Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER_FEATURE_COUNT = BPCPackage.PORT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Task Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_PARAMETER_OPERATION_COUNT = BPCPackage.PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.InAttributeImpl <em>In Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.InAttributeImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getInAttribute()
	 * @generated
	 */
	int IN_ATTRIBUTE = 2;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__PROPERTY = TASK_PARAMETER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__INSTANCE_UID = TASK_PARAMETER__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__DESCRIPTION = TASK_PARAMETER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__AUTHORSHIP = TASK_PARAMETER__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__PROVENANCE = TASK_PARAMETER__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__MODEL_UID = TASK_PARAMETER__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__METAMODEL_UID = TASK_PARAMETER__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__BASE_PARAMETER = TASK_PARAMETER__BASE_PARAMETER;

	/**
	 * The number of structural features of the '<em>In Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE_FEATURE_COUNT = TASK_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>In Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE_OPERATION_COUNT = TASK_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.OutAttributeImpl <em>Out Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.OutAttributeImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getOutAttribute()
	 * @generated
	 */
	int OUT_ATTRIBUTE = 3;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__PROPERTY = TASK_PARAMETER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__INSTANCE_UID = TASK_PARAMETER__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__DESCRIPTION = TASK_PARAMETER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__AUTHORSHIP = TASK_PARAMETER__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__PROVENANCE = TASK_PARAMETER__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__MODEL_UID = TASK_PARAMETER__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__METAMODEL_UID = TASK_PARAMETER__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__BASE_PARAMETER = TASK_PARAMETER__BASE_PARAMETER;

	/**
	 * The number of structural features of the '<em>Out Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE_FEATURE_COUNT = TASK_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Out Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE_OPERATION_COUNT = TASK_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__PROPERTY = BPCPackage.BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__INSTANCE_UID = BPCPackage.BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__DESCRIPTION = BPCPackage.BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__AUTHORSHIP = BPCPackage.BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__PROVENANCE = BPCPackage.BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__MODEL_UID = BPCPackage.BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__METAMODEL_UID = BPCPackage.BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__PORT = BPCPackage.BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__CONNECTOR = BPCPackage.BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__COLLECTION = BPCPackage.BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__BLOCK = BPCPackage.BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__RELATION = BPCPackage.BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__BASE_CLASS = BPCPackage.BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__TASK = BPCPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Skills</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SKILLS = BPCPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Behavior</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__BASE_BEHAVIOR = BPCPackage.BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ins</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__INS = BPCPackage.BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Outs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__OUTS = BPCPackage.BLOCK_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = BPCPackage.BLOCK_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = BPCPackage.BLOCK_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Task</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getTask()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Task();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getSkills <em>Skills</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Skills</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getSkills()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Skills();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getBase_Behavior <em>Base Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Behavior</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getBase_Behavior()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Base_Behavior();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getIns <em>Ins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ins</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getIns()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Ins();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getOuts <em>Outs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outs</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.Task#getOuts()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Outs();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition <em>Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Definition</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition
	 * @generated
	 */
	EClass getBehaviorDefinition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getTask()
	 * @see #getBehaviorDefinition()
	 * @generated
	 */
	EReference getBehaviorDefinition_Task();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getCompArch <em>Comp Arch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Comp Arch</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getCompArch()
	 * @see #getBehaviorDefinition()
	 * @generated
	 */
	EReference getBehaviorDefinition_CompArch();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getBase_BehavioredClassifier <em>Base Behaviored Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Behaviored Classifier</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getBase_BehavioredClassifier()
	 * @see #getBehaviorDefinition()
	 * @generated
	 */
	EReference getBehaviorDefinition_Base_BehavioredClassifier();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.TaskParameter <em>Task Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Parameter</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.TaskParameter
	 * @generated
	 */
	EClass getTaskParameter();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.TaskParameter#getBase_Parameter <em>Base Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Parameter</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.TaskParameter#getBase_Parameter()
	 * @see #getTaskParameter()
	 * @generated
	 */
	EReference getTaskParameter_Base_Parameter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.InAttribute <em>In Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.InAttribute
	 * @generated
	 */
	EClass getInAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.OutAttribute <em>Out Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.OutAttribute
	 * @generated
	 */
	EClass getOutAttribute();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BehaviorFactory getBehaviorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__TASK = eINSTANCE.getTask_Task();

		/**
		 * The meta object literal for the '<em><b>Skills</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__SKILLS = eINSTANCE.getTask_Skills();

		/**
		 * The meta object literal for the '<em><b>Base Behavior</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__BASE_BEHAVIOR = eINSTANCE.getTask_Base_Behavior();

		/**
		 * The meta object literal for the '<em><b>Ins</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__INS = eINSTANCE.getTask_Ins();

		/**
		 * The meta object literal for the '<em><b>Outs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__OUTS = eINSTANCE.getTask_Outs();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorDefinitionImpl <em>Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorDefinitionImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getBehaviorDefinition()
		 * @generated
		 */
		EClass BEHAVIOR_DEFINITION = eINSTANCE.getBehaviorDefinition();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_DEFINITION__TASK = eINSTANCE.getBehaviorDefinition_Task();

		/**
		 * The meta object literal for the '<em><b>Comp Arch</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_DEFINITION__COMP_ARCH = eINSTANCE.getBehaviorDefinition_CompArch();

		/**
		 * The meta object literal for the '<em><b>Base Behaviored Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_DEFINITION__BASE_BEHAVIORED_CLASSIFIER = eINSTANCE.getBehaviorDefinition_Base_BehavioredClassifier();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskParameterImpl <em>Task Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskParameterImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getTaskParameter()
		 * @generated
		 */
		EClass TASK_PARAMETER = eINSTANCE.getTaskParameter();

		/**
		 * The meta object literal for the '<em><b>Base Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_PARAMETER__BASE_PARAMETER = eINSTANCE.getTaskParameter_Base_Parameter();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.InAttributeImpl <em>In Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.InAttributeImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getInAttribute()
		 * @generated
		 */
		EClass IN_ATTRIBUTE = eINSTANCE.getInAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.OutAttributeImpl <em>Out Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.OutAttributeImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl#getOutAttribute()
		 * @generated
		 */
		EClass OUT_ATTRIBUTE = eINSTANCE.getOutAttribute();

	}

} //BehaviorPackage
