/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.commobject;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.commobject.EnumerationLiteral#getBase_EnumerationLiteral <em>Base Enumeration Literal</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage#getEnumerationLiteral()
 * @model
 * @generated
 */
public interface EnumerationLiteral extends Property {
	/**
	 * Returns the value of the '<em><b>Base Enumeration Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Enumeration Literal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Enumeration Literal</em>' reference.
	 * @see #setBase_EnumerationLiteral(org.eclipse.uml2.uml.EnumerationLiteral)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage#getEnumerationLiteral_Base_EnumerationLiteral()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.EnumerationLiteral getBase_EnumerationLiteral();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.commobject.EnumerationLiteral#getBase_EnumerationLiteral <em>Base Enumeration Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Enumeration Literal</em>' reference.
	 * @see #getBase_EnumerationLiteral()
	 * @generated
	 */
	void setBase_EnumerationLiteral(org.eclipse.uml2.uml.EnumerationLiteral value);

} // EnumerationLiteral
