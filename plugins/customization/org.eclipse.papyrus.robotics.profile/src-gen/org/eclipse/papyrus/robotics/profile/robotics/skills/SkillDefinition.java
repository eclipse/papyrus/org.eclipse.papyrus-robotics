/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.uml2.uml.Operation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getIns <em>Ins</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getOuts <em>Outs</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getRes <em>Res</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getDefaultSemantic <em>Default Semantic</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getBase_Operation <em>Base Operation</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillDefinition()
 * @model
 * @generated
 */
public interface SkillDefinition extends Block {
	/**
	 * Returns the value of the '<em><b>Ins</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ins</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ins</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillDefinition_Ins()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<InAttribute> getIns();

	/**
	 * Returns the value of the '<em><b>Outs</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outs</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillDefinition_Outs()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<OutAttribute> getOuts();

	/**
	 * Returns the value of the '<em><b>Res</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Res</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Res</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillDefinition_Res()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<SkillResult> getRes();

	/**
	 * Returns the value of the '<em><b>Default Semantic</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Semantic</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Semantic</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillDefinition_DefaultSemantic()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SkillSemantic getDefaultSemantic();

	/**
	 * Returns the value of the '<em><b>Base Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Operation</em>' reference.
	 * @see #setBase_Operation(Operation)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillDefinition_Base_Operation()
	 * @model ordered="false"
	 * @generated
	 */
	Operation getBase_Operation();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getBase_Operation <em>Base Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Operation</em>' reference.
	 * @see #getBase_Operation()
	 * @generated
	 */
	void setBase_Operation(Operation value);

} // SkillDefinition
