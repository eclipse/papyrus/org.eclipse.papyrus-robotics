/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.commobject.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CommunicationObjectImpl extends DataTypeImpl implements CommunicationObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CommobjectPackage.Literals.COMMUNICATION_OBJECT;
	}

} //CommunicationObjectImpl
