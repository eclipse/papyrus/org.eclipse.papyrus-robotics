/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OutAttributeImpl extends SkillParameterImpl implements OutAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SkillsPackage.Literals.OUT_ATTRIBUTE;
	}

} //OutAttributeImpl
