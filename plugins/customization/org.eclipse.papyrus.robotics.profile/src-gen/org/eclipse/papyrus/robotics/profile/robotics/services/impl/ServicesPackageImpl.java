/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.impl.CommobjectPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.DeploymentPackage;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.generics.GenericsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.generics.impl.GenericsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.impl.roboticsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersPackage;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.impl.ParametersPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinitionModel;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWishProperty;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesFactory;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.SystemServiceArchitectureModel;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ServicesPackageImpl extends EPackageImpl implements ServicesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass servicePropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceWishEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceWishPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemServiceArchitectureModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceFulfillmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceDefinitionModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coordinationServiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coordinationEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceDefinitionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ServicesPackageImpl() {
		super(eNS_URI, ServicesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ServicesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ServicesPackage init() {
		if (isInited) return (ServicesPackage)EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredServicesPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ServicesPackageImpl theServicesPackage = registeredServicesPackage instanceof ServicesPackageImpl ? (ServicesPackageImpl)registeredServicesPackage : new ServicesPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BPCPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(roboticsPackage.eNS_URI);
		roboticsPackageImpl theroboticsPackage = (roboticsPackageImpl)(registeredPackage instanceof roboticsPackageImpl ? registeredPackage : roboticsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		ComponentsPackageImpl theComponentsPackage = (ComponentsPackageImpl)(registeredPackage instanceof ComponentsPackageImpl ? registeredPackage : ComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FunctionsPackage.eNS_URI);
		FunctionsPackageImpl theFunctionsPackage = (FunctionsPackageImpl)(registeredPackage instanceof FunctionsPackageImpl ? registeredPackage : FunctionsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ParametersPackage.eNS_URI);
		ParametersPackageImpl theParametersPackage = (ParametersPackageImpl)(registeredPackage instanceof ParametersPackageImpl ? registeredPackage : ParametersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		BehaviorPackageImpl theBehaviorPackage = (BehaviorPackageImpl)(registeredPackage instanceof BehaviorPackageImpl ? registeredPackage : BehaviorPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);
		SkillsPackageImpl theSkillsPackage = (SkillsPackageImpl)(registeredPackage instanceof SkillsPackageImpl ? registeredPackage : SkillsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommpatternPackage.eNS_URI);
		CommpatternPackageImpl theCommpatternPackage = (CommpatternPackageImpl)(registeredPackage instanceof CommpatternPackageImpl ? registeredPackage : CommpatternPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(registeredPackage instanceof DeploymentPackageImpl ? registeredPackage : DeploymentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommobjectPackage.eNS_URI);
		CommobjectPackageImpl theCommobjectPackage = (CommobjectPackageImpl)(registeredPackage instanceof CommobjectPackageImpl ? registeredPackage : CommobjectPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GenericsPackage.eNS_URI);
		GenericsPackageImpl theGenericsPackage = (GenericsPackageImpl)(registeredPackage instanceof GenericsPackageImpl ? registeredPackage : GenericsPackage.eINSTANCE);

		// Create package meta-data objects
		theServicesPackage.createPackageContents();
		theroboticsPackage.createPackageContents();
		theComponentsPackage.createPackageContents();
		theFunctionsPackage.createPackageContents();
		theParametersPackage.createPackageContents();
		theBehaviorPackage.createPackageContents();
		theSkillsPackage.createPackageContents();
		theCommpatternPackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theCommobjectPackage.createPackageContents();
		theGenericsPackage.createPackageContents();

		// Initialize created meta-data
		theServicesPackage.initializePackageContents();
		theroboticsPackage.initializePackageContents();
		theComponentsPackage.initializePackageContents();
		theFunctionsPackage.initializePackageContents();
		theParametersPackage.initializePackageContents();
		theBehaviorPackage.initializePackageContents();
		theSkillsPackage.initializePackageContents();
		theCommpatternPackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theCommobjectPackage.initializePackageContents();
		theGenericsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theServicesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ServicesPackage.eNS_URI, theServicesPackage);
		return theServicesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceProperty() {
		return servicePropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceProperty_Base_Property() {
		return (EReference)servicePropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceLink() {
		return serviceLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceLink_Base_Association() {
		return (EReference)serviceLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceLink_SrcWish() {
		return (EReference)serviceLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceLink_TgtWish() {
		return (EReference)serviceLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceLink_Base_Usage() {
		return (EReference)serviceLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceWish() {
		return serviceWishEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceWish_Base_InstanceSpecification() {
		return (EReference)serviceWishEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceWish_Properties() {
		return (EReference)serviceWishEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceWishProperty() {
		return serviceWishPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceWishProperty_Base_Slot() {
		return (EReference)serviceWishPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSystemServiceArchitectureModel() {
		return systemServiceArchitectureModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSystemServiceArchitectureModel_Base_Package() {
		return (EReference)systemServiceArchitectureModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceFulfillment() {
		return serviceFulfillmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceFulfillment_Wish() {
		return (EReference)serviceFulfillmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceFulfillment_CInstance() {
		return (EReference)serviceFulfillmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceFulfillment_CPort() {
		return (EReference)serviceFulfillmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceFulfillment_Base_Association() {
		return (EReference)serviceFulfillmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceDefinitionModel() {
		return serviceDefinitionModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceDefinitionModel_Base_Package() {
		return (EReference)serviceDefinitionModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCoordinationService() {
		return coordinationServiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCoordinationService_SvcProperty() {
		return (EReference)coordinationServiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCoordinationService_Base_Interface() {
		return (EReference)coordinationServiceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCoordinationEvent() {
		return coordinationEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCoordinationEvent_Event() {
		return (EReference)coordinationEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCoordinationEvent_Base_Event() {
		return (EReference)coordinationEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getServiceDefinition() {
		return serviceDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceDefinition_Base_Interface() {
		return (EReference)serviceDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getServiceDefinition_SvcProperty() {
		return (EReference)serviceDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServicesFactory getServicesFactory() {
		return (ServicesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		servicePropertyEClass = createEClass(SERVICE_PROPERTY);
		createEReference(servicePropertyEClass, SERVICE_PROPERTY__BASE_PROPERTY);

		serviceLinkEClass = createEClass(SERVICE_LINK);
		createEReference(serviceLinkEClass, SERVICE_LINK__BASE_ASSOCIATION);
		createEReference(serviceLinkEClass, SERVICE_LINK__SRC_WISH);
		createEReference(serviceLinkEClass, SERVICE_LINK__TGT_WISH);
		createEReference(serviceLinkEClass, SERVICE_LINK__BASE_USAGE);

		serviceWishEClass = createEClass(SERVICE_WISH);
		createEReference(serviceWishEClass, SERVICE_WISH__BASE_INSTANCE_SPECIFICATION);
		createEReference(serviceWishEClass, SERVICE_WISH__PROPERTIES);

		serviceWishPropertyEClass = createEClass(SERVICE_WISH_PROPERTY);
		createEReference(serviceWishPropertyEClass, SERVICE_WISH_PROPERTY__BASE_SLOT);

		systemServiceArchitectureModelEClass = createEClass(SYSTEM_SERVICE_ARCHITECTURE_MODEL);
		createEReference(systemServiceArchitectureModelEClass, SYSTEM_SERVICE_ARCHITECTURE_MODEL__BASE_PACKAGE);

		serviceFulfillmentEClass = createEClass(SERVICE_FULFILLMENT);
		createEReference(serviceFulfillmentEClass, SERVICE_FULFILLMENT__WISH);
		createEReference(serviceFulfillmentEClass, SERVICE_FULFILLMENT__CINSTANCE);
		createEReference(serviceFulfillmentEClass, SERVICE_FULFILLMENT__CPORT);
		createEReference(serviceFulfillmentEClass, SERVICE_FULFILLMENT__BASE_ASSOCIATION);

		serviceDefinitionModelEClass = createEClass(SERVICE_DEFINITION_MODEL);
		createEReference(serviceDefinitionModelEClass, SERVICE_DEFINITION_MODEL__BASE_PACKAGE);

		coordinationServiceEClass = createEClass(COORDINATION_SERVICE);
		createEReference(coordinationServiceEClass, COORDINATION_SERVICE__SVC_PROPERTY);
		createEReference(coordinationServiceEClass, COORDINATION_SERVICE__BASE_INTERFACE);

		coordinationEventEClass = createEClass(COORDINATION_EVENT);
		createEReference(coordinationEventEClass, COORDINATION_EVENT__EVENT);
		createEReference(coordinationEventEClass, COORDINATION_EVENT__BASE_EVENT);

		serviceDefinitionEClass = createEClass(SERVICE_DEFINITION);
		createEReference(serviceDefinitionEClass, SERVICE_DEFINITION__BASE_INTERFACE);
		createEReference(serviceDefinitionEClass, SERVICE_DEFINITION__SVC_PROPERTY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BPCPackage theBPCPackage = (BPCPackage)EPackage.Registry.INSTANCE.getEPackage(BPCPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		servicePropertyEClass.getESuperTypes().add(theBPCPackage.getProperty());
		serviceLinkEClass.getESuperTypes().add(theBPCPackage.getRelation());
		serviceWishEClass.getESuperTypes().add(theBPCPackage.getEntity());
		serviceWishPropertyEClass.getESuperTypes().add(theBPCPackage.getProperty());
		systemServiceArchitectureModelEClass.getESuperTypes().add(theBPCPackage.getEntity());
		serviceFulfillmentEClass.getESuperTypes().add(theBPCPackage.getRelation());
		serviceDefinitionModelEClass.getESuperTypes().add(theBPCPackage.getEntity());
		coordinationServiceEClass.getESuperTypes().add(theBPCPackage.getEntity());
		coordinationEventEClass.getESuperTypes().add(theBPCPackage.getEntity());
		serviceDefinitionEClass.getESuperTypes().add(theBPCPackage.getEntity());

		// Initialize classes, features, and operations; add parameters
		initEClass(servicePropertyEClass, ServiceProperty.class, "ServiceProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceProperty_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ServiceProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(serviceLinkEClass, ServiceLink.class, "ServiceLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceLink_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 1, 1, ServiceLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceLink_SrcWish(), this.getServiceWish(), null, "srcWish", null, 1, 1, ServiceLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceLink_TgtWish(), this.getServiceWish(), null, "tgtWish", null, 1, 1, ServiceLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceLink_Base_Usage(), theUMLPackage.getUsage(), null, "base_Usage", null, 1, 1, ServiceLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(serviceWishEClass, ServiceWish.class, "ServiceWish", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceWish_Base_InstanceSpecification(), theUMLPackage.getInstanceSpecification(), null, "base_InstanceSpecification", null, 1, 1, ServiceWish.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceWish_Properties(), this.getServiceWishProperty(), null, "properties", null, 0, -1, ServiceWish.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(serviceWishPropertyEClass, ServiceWishProperty.class, "ServiceWishProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceWishProperty_Base_Slot(), theUMLPackage.getSlot(), null, "base_Slot", null, 1, 1, ServiceWishProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(systemServiceArchitectureModelEClass, SystemServiceArchitectureModel.class, "SystemServiceArchitectureModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSystemServiceArchitectureModel_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, SystemServiceArchitectureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(serviceFulfillmentEClass, ServiceFulfillment.class, "ServiceFulfillment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceFulfillment_Wish(), this.getServiceWish(), null, "wish", null, 1, 1, ServiceFulfillment.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceFulfillment_CInstance(), theComponentsPackage.getComponentInstance(), null, "cInstance", null, 1, 1, ServiceFulfillment.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceFulfillment_CPort(), theComponentsPackage.getComponentPort(), null, "cPort", null, 1, 1, ServiceFulfillment.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceFulfillment_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 1, 1, ServiceFulfillment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(serviceDefinitionModelEClass, ServiceDefinitionModel.class, "ServiceDefinitionModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceDefinitionModel_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, ServiceDefinitionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(coordinationServiceEClass, CoordinationService.class, "CoordinationService", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getCoordinationService_SvcProperty(), this.getServiceProperty(), null, "svcProperty", null, 0, -1, CoordinationService.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getCoordinationService_Base_Interface(), theUMLPackage.getInterface(), null, "base_Interface", null, 0, 1, CoordinationService.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(coordinationEventEClass, CoordinationEvent.class, "CoordinationEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getCoordinationEvent_Event(), this.getServiceDefinition(), null, "event", null, 0, 1, CoordinationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getCoordinationEvent_Base_Event(), theUMLPackage.getEvent(), null, "base_Event", null, 0, 1, CoordinationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(serviceDefinitionEClass, ServiceDefinition.class, "ServiceDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getServiceDefinition_Base_Interface(), theUMLPackage.getInterface(), null, "base_Interface", null, 0, 1, ServiceDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getServiceDefinition_SvcProperty(), this.getServiceProperty(), null, "svcProperty", null, 0, -1, ServiceDefinition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
	}

} //ServicesPackageImpl
