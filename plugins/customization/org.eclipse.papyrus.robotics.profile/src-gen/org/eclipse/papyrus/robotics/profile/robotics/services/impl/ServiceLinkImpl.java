/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.RelationImpl;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Usage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl#getBase_Association <em>Base Association</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl#getSrcWish <em>Src Wish</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl#getTgtWish <em>Tgt Wish</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl#getBase_Usage <em>Base Usage</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServiceLinkImpl extends RelationImpl implements ServiceLink {
	/**
	 * The cached value of the '{@link #getBase_Association() <em>Base Association</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Association()
	 * @generated
	 * @ordered
	 */
	protected Association base_Association;

	/**
	 * The cached value of the '{@link #getSrcWish() <em>Src Wish</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcWish()
	 * @generated
	 * @ordered
	 */
	protected ServiceWish srcWish;

	/**
	 * The cached value of the '{@link #getTgtWish() <em>Tgt Wish</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTgtWish()
	 * @generated
	 * @ordered
	 */
	protected ServiceWish tgtWish;

	/**
	 * The cached value of the '{@link #getBase_Usage() <em>Base Usage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Usage()
	 * @generated
	 * @ordered
	 */
	protected Usage base_Usage;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicesPackage.Literals.SERVICE_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Association getBase_Association() {
		if (base_Association != null && base_Association.eIsProxy()) {
			InternalEObject oldBase_Association = (InternalEObject)base_Association;
			base_Association = (Association)eResolveProxy(oldBase_Association);
			if (base_Association != oldBase_Association) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesPackage.SERVICE_LINK__BASE_ASSOCIATION, oldBase_Association, base_Association));
			}
		}
		return base_Association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetBase_Association() {
		return base_Association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Association(Association newBase_Association) {
		Association oldBase_Association = base_Association;
		base_Association = newBase_Association;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesPackage.SERVICE_LINK__BASE_ASSOCIATION, oldBase_Association, base_Association));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceWish getSrcWish() {
		if (srcWish != null && srcWish.eIsProxy()) {
			InternalEObject oldSrcWish = (InternalEObject)srcWish;
			srcWish = (ServiceWish)eResolveProxy(oldSrcWish);
			if (srcWish != oldSrcWish) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesPackage.SERVICE_LINK__SRC_WISH, oldSrcWish, srcWish));
			}
		}
		return srcWish;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceWish basicGetSrcWish() {
		return srcWish;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSrcWish(ServiceWish newSrcWish) {
		ServiceWish oldSrcWish = srcWish;
		srcWish = newSrcWish;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesPackage.SERVICE_LINK__SRC_WISH, oldSrcWish, srcWish));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceWish getTgtWish() {
		if (tgtWish != null && tgtWish.eIsProxy()) {
			InternalEObject oldTgtWish = (InternalEObject)tgtWish;
			tgtWish = (ServiceWish)eResolveProxy(oldTgtWish);
			if (tgtWish != oldTgtWish) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesPackage.SERVICE_LINK__TGT_WISH, oldTgtWish, tgtWish));
			}
		}
		return tgtWish;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceWish basicGetTgtWish() {
		return tgtWish;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTgtWish(ServiceWish newTgtWish) {
		ServiceWish oldTgtWish = tgtWish;
		tgtWish = newTgtWish;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesPackage.SERVICE_LINK__TGT_WISH, oldTgtWish, tgtWish));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Usage getBase_Usage() {
		if (base_Usage != null && base_Usage.eIsProxy()) {
			InternalEObject oldBase_Usage = (InternalEObject)base_Usage;
			base_Usage = (Usage)eResolveProxy(oldBase_Usage);
			if (base_Usage != oldBase_Usage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServicesPackage.SERVICE_LINK__BASE_USAGE, oldBase_Usage, base_Usage));
			}
		}
		return base_Usage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Usage basicGetBase_Usage() {
		return base_Usage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Usage(Usage newBase_Usage) {
		Usage oldBase_Usage = base_Usage;
		base_Usage = newBase_Usage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServicesPackage.SERVICE_LINK__BASE_USAGE, oldBase_Usage, base_Usage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServicesPackage.SERVICE_LINK__BASE_ASSOCIATION:
				if (resolve) return getBase_Association();
				return basicGetBase_Association();
			case ServicesPackage.SERVICE_LINK__SRC_WISH:
				if (resolve) return getSrcWish();
				return basicGetSrcWish();
			case ServicesPackage.SERVICE_LINK__TGT_WISH:
				if (resolve) return getTgtWish();
				return basicGetTgtWish();
			case ServicesPackage.SERVICE_LINK__BASE_USAGE:
				if (resolve) return getBase_Usage();
				return basicGetBase_Usage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServicesPackage.SERVICE_LINK__BASE_ASSOCIATION:
				setBase_Association((Association)newValue);
				return;
			case ServicesPackage.SERVICE_LINK__SRC_WISH:
				setSrcWish((ServiceWish)newValue);
				return;
			case ServicesPackage.SERVICE_LINK__TGT_WISH:
				setTgtWish((ServiceWish)newValue);
				return;
			case ServicesPackage.SERVICE_LINK__BASE_USAGE:
				setBase_Usage((Usage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServicesPackage.SERVICE_LINK__BASE_ASSOCIATION:
				setBase_Association((Association)null);
				return;
			case ServicesPackage.SERVICE_LINK__SRC_WISH:
				setSrcWish((ServiceWish)null);
				return;
			case ServicesPackage.SERVICE_LINK__TGT_WISH:
				setTgtWish((ServiceWish)null);
				return;
			case ServicesPackage.SERVICE_LINK__BASE_USAGE:
				setBase_Usage((Usage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServicesPackage.SERVICE_LINK__BASE_ASSOCIATION:
				return base_Association != null;
			case ServicesPackage.SERVICE_LINK__SRC_WISH:
				return srcWish != null;
			case ServicesPackage.SERVICE_LINK__TGT_WISH:
				return tgtWish != null;
			case ServicesPackage.SERVICE_LINK__BASE_USAGE:
				return base_Usage != null;
		}
		return super.eIsSet(featureID);
	}

} //ServiceLinkImpl
