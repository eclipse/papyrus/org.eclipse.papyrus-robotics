/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.components.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.profile.components.ComponentDefinitionOperations;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionImpl#getActivities <em>Activities</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionImpl#isLifecycle <em>Is Lifecycle</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentDefinitionImpl extends ComponentOrSystemImpl implements ComponentDefinition {
	/**
	 * The default value of the '{@link #isLifecycle() <em>Is Lifecycle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLifecycle()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_LIFECYCLE_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isLifecycle() <em>Is Lifecycle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLifecycle()
	 * @generated
	 * @ordered
	 */
	protected boolean isLifecycle = IS_LIFECYCLE_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsPackage.Literals.COMPONENT_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public EList<Activity> getActivities() {
		return ComponentDefinitionOperations.getActivities(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isLifecycle() {
		return isLifecycle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsLifecycle(boolean newIsLifecycle) {
		boolean oldIsLifecycle = isLifecycle;
		isLifecycle = newIsLifecycle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_DEFINITION__IS_LIFECYCLE, oldIsLifecycle, isLifecycle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Parameter getParameter() {
		Parameter parameter = basicGetParameter();
		return parameter != null && parameter.eIsProxy() ? (Parameter)eResolveProxy((InternalEObject)parameter) : parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public Parameter basicGetParameter() {
		return ComponentDefinitionOperations.getParameter(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION__PARAMETER:
				if (resolve) return getParameter();
				return basicGetParameter();
			case ComponentsPackage.COMPONENT_DEFINITION__ACTIVITIES:
				return getActivities();
			case ComponentsPackage.COMPONENT_DEFINITION__IS_LIFECYCLE:
				return isLifecycle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION__IS_LIFECYCLE:
				setIsLifecycle((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION__IS_LIFECYCLE:
				setIsLifecycle(IS_LIFECYCLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION__PARAMETER:
				return basicGetParameter() != null;
			case ComponentsPackage.COMPONENT_DEFINITION__ACTIVITIES:
				return !getActivities().isEmpty();
			case ComponentsPackage.COMPONENT_DEFINITION__IS_LIFECYCLE:
				return isLifecycle != IS_LIFECYCLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (isLifecycle: "); //$NON-NLS-1$
		result.append(isLifecycle);
		result.append(')');
		return result.toString();
	}

} // ComponentDefinitionImpl
