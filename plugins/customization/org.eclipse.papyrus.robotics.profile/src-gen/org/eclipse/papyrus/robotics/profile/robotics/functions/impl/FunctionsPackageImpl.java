/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.functions.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.impl.CommobjectPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.DeploymentPackage;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Argument;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsFactory;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.generics.GenericsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.generics.impl.GenericsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.impl.roboticsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersPackage;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.impl.ParametersPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionsPackageImpl extends EPackageImpl implements FunctionsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum functionKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FunctionsPackageImpl() {
		super(eNS_URI, FunctionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FunctionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FunctionsPackage init() {
		if (isInited) return (FunctionsPackage)EPackage.Registry.INSTANCE.getEPackage(FunctionsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFunctionsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FunctionsPackageImpl theFunctionsPackage = registeredFunctionsPackage instanceof FunctionsPackageImpl ? (FunctionsPackageImpl)registeredFunctionsPackage : new FunctionsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BPCPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(roboticsPackage.eNS_URI);
		roboticsPackageImpl theroboticsPackage = (roboticsPackageImpl)(registeredPackage instanceof roboticsPackageImpl ? registeredPackage : roboticsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		ComponentsPackageImpl theComponentsPackage = (ComponentsPackageImpl)(registeredPackage instanceof ComponentsPackageImpl ? registeredPackage : ComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);
		ServicesPackageImpl theServicesPackage = (ServicesPackageImpl)(registeredPackage instanceof ServicesPackageImpl ? registeredPackage : ServicesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ParametersPackage.eNS_URI);
		ParametersPackageImpl theParametersPackage = (ParametersPackageImpl)(registeredPackage instanceof ParametersPackageImpl ? registeredPackage : ParametersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		BehaviorPackageImpl theBehaviorPackage = (BehaviorPackageImpl)(registeredPackage instanceof BehaviorPackageImpl ? registeredPackage : BehaviorPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);
		SkillsPackageImpl theSkillsPackage = (SkillsPackageImpl)(registeredPackage instanceof SkillsPackageImpl ? registeredPackage : SkillsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommpatternPackage.eNS_URI);
		CommpatternPackageImpl theCommpatternPackage = (CommpatternPackageImpl)(registeredPackage instanceof CommpatternPackageImpl ? registeredPackage : CommpatternPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(registeredPackage instanceof DeploymentPackageImpl ? registeredPackage : DeploymentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommobjectPackage.eNS_URI);
		CommobjectPackageImpl theCommobjectPackage = (CommobjectPackageImpl)(registeredPackage instanceof CommobjectPackageImpl ? registeredPackage : CommobjectPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GenericsPackage.eNS_URI);
		GenericsPackageImpl theGenericsPackage = (GenericsPackageImpl)(registeredPackage instanceof GenericsPackageImpl ? registeredPackage : GenericsPackage.eINSTANCE);

		// Create package meta-data objects
		theFunctionsPackage.createPackageContents();
		theroboticsPackage.createPackageContents();
		theComponentsPackage.createPackageContents();
		theServicesPackage.createPackageContents();
		theParametersPackage.createPackageContents();
		theBehaviorPackage.createPackageContents();
		theSkillsPackage.createPackageContents();
		theCommpatternPackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theCommobjectPackage.createPackageContents();
		theGenericsPackage.createPackageContents();

		// Initialize created meta-data
		theFunctionsPackage.initializePackageContents();
		theroboticsPackage.initializePackageContents();
		theComponentsPackage.initializePackageContents();
		theServicesPackage.initializePackageContents();
		theParametersPackage.initializePackageContents();
		theBehaviorPackage.initializePackageContents();
		theSkillsPackage.initializePackageContents();
		theCommpatternPackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theCommobjectPackage.initializePackageContents();
		theGenericsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFunctionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FunctionsPackage.eNS_URI, theFunctionsPackage);
		return theFunctionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getArgument() {
		return argumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getArgument_Base_Port() {
		return (EReference)argumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFunction() {
		return functionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFunction_Arguments() {
		return (EReference)functionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFunction_Kind() {
		return (EAttribute)functionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFunction_CodeInModel() {
		return (EAttribute)functionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFunction_ActivityPort() {
		return (EReference)functionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getFunctionKind() {
		return functionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FunctionsFactory getFunctionsFactory() {
		return (FunctionsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		argumentEClass = createEClass(ARGUMENT);
		createEReference(argumentEClass, ARGUMENT__BASE_PORT);

		functionEClass = createEClass(FUNCTION);
		createEReference(functionEClass, FUNCTION__ARGUMENTS);
		createEAttribute(functionEClass, FUNCTION__KIND);
		createEAttribute(functionEClass, FUNCTION__CODE_IN_MODEL);
		createEReference(functionEClass, FUNCTION__ACTIVITY_PORT);

		// Create enums
		functionKindEEnum = createEEnum(FUNCTION_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BPCPackage theBPCPackage = (BPCPackage)EPackage.Registry.INSTANCE.getEPackage(BPCPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		argumentEClass.getESuperTypes().add(theBPCPackage.getPort());
		functionEClass.getESuperTypes().add(theBPCPackage.getBlock());

		// Initialize classes, features, and operations; add parameters
		initEClass(argumentEClass, Argument.class, "Argument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getArgument_Base_Port(), theUMLPackage.getPort(), null, "base_Port", null, 1, 1, Argument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(functionEClass, Function.class, "Function", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getFunction_Arguments(), this.getArgument(), null, "arguments", null, 0, -1, Function.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getFunction_Kind(), this.getFunctionKind(), "kind", "HANDLER", 1, 1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(getFunction_CodeInModel(), theTypesPackage.getBoolean(), "codeInModel", "false", 1, 1, Function.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(getFunction_ActivityPort(), theComponentsPackage.getActivityPort(), null, "activityPort", null, 0, 1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		// Initialize enums and add enum literals
		initEEnum(functionKindEEnum, FunctionKind.class, "FunctionKind"); //$NON-NLS-1$
		addEEnumLiteral(functionKindEEnum, FunctionKind.ON_CONFIGURE);
		addEEnumLiteral(functionKindEEnum, FunctionKind.ON_ACTIVATE);
		addEEnumLiteral(functionKindEEnum, FunctionKind.ON_DEACTIVATE);
		addEEnumLiteral(functionKindEEnum, FunctionKind.ON_SHUTDOWN);
		addEEnumLiteral(functionKindEEnum, FunctionKind.ON_CLEANUP);
		addEEnumLiteral(functionKindEEnum, FunctionKind.ON_ERROR);
		addEEnumLiteral(functionKindEEnum, FunctionKind.HANDLER);
		addEEnumLiteral(functionKindEEnum, FunctionKind.PERIODIC);
		addEEnumLiteral(functionKindEEnum, FunctionKind.MANUALLY_CALLED);
	}

} //FunctionsPackageImpl
