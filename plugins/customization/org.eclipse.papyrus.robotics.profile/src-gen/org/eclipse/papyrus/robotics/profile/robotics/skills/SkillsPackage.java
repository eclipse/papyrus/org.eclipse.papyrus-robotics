/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsFactory
 * @model kind="package"
 * @generated
 */
public interface SkillsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "skills"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/skills/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "robotics.skills"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SkillsPackage eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionSetImpl <em>Skill Definition Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionSetImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillDefinitionSet()
	 * @generated
	 */
	int SKILL_DEFINITION_SET = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Skills</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__SKILLS = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET__BASE_INTERFACE = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Skill Definition Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Skill Definition Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_SET_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl <em>Skill Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillDefinition()
	 * @generated
	 */
	int SKILL_DEFINITION = 13;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillParameterImpl <em>Skill Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillParameterImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillParameter()
	 * @generated
	 */
	int SKILL_PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__PROPERTY = BPCPackage.PORT__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__INSTANCE_UID = BPCPackage.PORT__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__DESCRIPTION = BPCPackage.PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__AUTHORSHIP = BPCPackage.PORT__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__PROVENANCE = BPCPackage.PORT__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__MODEL_UID = BPCPackage.PORT__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__METAMODEL_UID = BPCPackage.PORT__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER__BASE_PARAMETER = BPCPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Skill Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER_FEATURE_COUNT = BPCPackage.PORT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Skill Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_PARAMETER_OPERATION_COUNT = BPCPackage.PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.InAttributeImpl <em>In Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.InAttributeImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getInAttribute()
	 * @generated
	 */
	int IN_ATTRIBUTE = 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__PROPERTY = SKILL_PARAMETER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__INSTANCE_UID = SKILL_PARAMETER__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__DESCRIPTION = SKILL_PARAMETER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__AUTHORSHIP = SKILL_PARAMETER__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__PROVENANCE = SKILL_PARAMETER__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__MODEL_UID = SKILL_PARAMETER__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__METAMODEL_UID = SKILL_PARAMETER__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE__BASE_PARAMETER = SKILL_PARAMETER__BASE_PARAMETER;

	/**
	 * The number of structural features of the '<em>In Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE_FEATURE_COUNT = SKILL_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>In Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_ATTRIBUTE_OPERATION_COUNT = SKILL_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.OutAttributeImpl <em>Out Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.OutAttributeImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getOutAttribute()
	 * @generated
	 */
	int OUT_ATTRIBUTE = 3;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__PROPERTY = SKILL_PARAMETER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__INSTANCE_UID = SKILL_PARAMETER__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__DESCRIPTION = SKILL_PARAMETER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__AUTHORSHIP = SKILL_PARAMETER__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__PROVENANCE = SKILL_PARAMETER__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__MODEL_UID = SKILL_PARAMETER__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__METAMODEL_UID = SKILL_PARAMETER__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE__BASE_PARAMETER = SKILL_PARAMETER__BASE_PARAMETER;

	/**
	 * The number of structural features of the '<em>Out Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE_FEATURE_COUNT = SKILL_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Out Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_ATTRIBUTE_OPERATION_COUNT = SKILL_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillResultImpl <em>Skill Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillResultImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillResult()
	 * @generated
	 */
	int SKILL_RESULT = 4;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__PROPERTY = SKILL_PARAMETER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__INSTANCE_UID = SKILL_PARAMETER__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__DESCRIPTION = SKILL_PARAMETER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__AUTHORSHIP = SKILL_PARAMETER__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__PROVENANCE = SKILL_PARAMETER__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__MODEL_UID = SKILL_PARAMETER__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__METAMODEL_UID = SKILL_PARAMETER__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__BASE_PARAMETER = SKILL_PARAMETER__BASE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__KIND = SKILL_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT__VALUE = SKILL_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Skill Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT_FEATURE_COUNT = SKILL_PARAMETER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Skill Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_RESULT_OPERATION_COUNT = SKILL_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl <em>Skill Semantic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillSemantic()
	 * @generated
	 */
	int SKILL_SEMANTIC = 5;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__PROPERTY = BPCPackage.BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__INSTANCE_UID = BPCPackage.BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__DESCRIPTION = BPCPackage.BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__AUTHORSHIP = BPCPackage.BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__PROVENANCE = BPCPackage.BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__MODEL_UID = BPCPackage.BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__METAMODEL_UID = BPCPackage.BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__PORT = BPCPackage.BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__CONNECTOR = BPCPackage.BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__COLLECTION = BPCPackage.BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__BLOCK = BPCPackage.BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__RELATION = BPCPackage.BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__BASE_CLASS = BPCPackage.BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Success</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__SUCCESS = BPCPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fail</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__FAIL = BPCPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operational</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__OPERATIONAL = BPCPackage.BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Succ Evts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__SUCC_EVTS = BPCPackage.BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Fail Evts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__FAIL_EVTS = BPCPackage.BLOCK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Base State Machine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC__BASE_STATE_MACHINE = BPCPackage.BLOCK_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Skill Semantic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC_FEATURE_COUNT = BPCPackage.BLOCK_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Skill Semantic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SEMANTIC_OPERATION_COUNT = BPCPackage.BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillStateImpl <em>Skill State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillStateImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillState()
	 * @generated
	 */
	int SKILL_STATE = 7;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__PROPERTY = BPCPackage.BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__INSTANCE_UID = BPCPackage.BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__DESCRIPTION = BPCPackage.BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__AUTHORSHIP = BPCPackage.BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__PROVENANCE = BPCPackage.BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__MODEL_UID = BPCPackage.BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__METAMODEL_UID = BPCPackage.BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__PORT = BPCPackage.BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__CONNECTOR = BPCPackage.BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__COLLECTION = BPCPackage.BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__BLOCK = BPCPackage.BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__RELATION = BPCPackage.BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__BASE_CLASS = BPCPackage.BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__BASE_STATE = BPCPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fsm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE__FSM = BPCPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Skill State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE_FEATURE_COUNT = BPCPackage.BLOCK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Skill State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_STATE_OPERATION_COUNT = BPCPackage.BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSuccessStateImpl <em>Skill Success State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSuccessStateImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillSuccessState()
	 * @generated
	 */
	int SKILL_SUCCESS_STATE = 6;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__PROPERTY = SKILL_STATE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__INSTANCE_UID = SKILL_STATE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__DESCRIPTION = SKILL_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__AUTHORSHIP = SKILL_STATE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__PROVENANCE = SKILL_STATE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__MODEL_UID = SKILL_STATE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__METAMODEL_UID = SKILL_STATE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__PORT = SKILL_STATE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__CONNECTOR = SKILL_STATE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__COLLECTION = SKILL_STATE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__BLOCK = SKILL_STATE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__RELATION = SKILL_STATE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__BASE_CLASS = SKILL_STATE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__BASE_STATE = SKILL_STATE__BASE_STATE;

	/**
	 * The feature id for the '<em><b>Fsm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE__FSM = SKILL_STATE__FSM;

	/**
	 * The number of structural features of the '<em>Skill Success State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE_FEATURE_COUNT = SKILL_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Skill Success State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_SUCCESS_STATE_OPERATION_COUNT = SKILL_STATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFailStateImpl <em>Skill Fail State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFailStateImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillFailState()
	 * @generated
	 */
	int SKILL_FAIL_STATE = 8;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__PROPERTY = SKILL_STATE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__INSTANCE_UID = SKILL_STATE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__DESCRIPTION = SKILL_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__AUTHORSHIP = SKILL_STATE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__PROVENANCE = SKILL_STATE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__MODEL_UID = SKILL_STATE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__METAMODEL_UID = SKILL_STATE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__PORT = SKILL_STATE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__CONNECTOR = SKILL_STATE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__COLLECTION = SKILL_STATE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__BLOCK = SKILL_STATE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__RELATION = SKILL_STATE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__BASE_CLASS = SKILL_STATE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__BASE_STATE = SKILL_STATE__BASE_STATE;

	/**
	 * The feature id for the '<em><b>Fsm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE__FSM = SKILL_STATE__FSM;

	/**
	 * The number of structural features of the '<em>Skill Fail State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE_FEATURE_COUNT = SKILL_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Skill Fail State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FAIL_STATE_OPERATION_COUNT = SKILL_STATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillOperationalStateImpl <em>Skill Operational State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillOperationalStateImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillOperationalState()
	 * @generated
	 */
	int SKILL_OPERATIONAL_STATE = 9;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__PROPERTY = SKILL_STATE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__INSTANCE_UID = SKILL_STATE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__DESCRIPTION = SKILL_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__AUTHORSHIP = SKILL_STATE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__PROVENANCE = SKILL_STATE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__MODEL_UID = SKILL_STATE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__METAMODEL_UID = SKILL_STATE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__PORT = SKILL_STATE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__CONNECTOR = SKILL_STATE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__COLLECTION = SKILL_STATE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__BLOCK = SKILL_STATE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__RELATION = SKILL_STATE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__BASE_CLASS = SKILL_STATE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__BASE_STATE = SKILL_STATE__BASE_STATE;

	/**
	 * The feature id for the '<em><b>Fsm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__FSM = SKILL_STATE__FSM;

	/**
	 * The feature id for the '<em><b>Comp Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE__COMP_INTERFACE = SKILL_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Skill Operational State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE_FEATURE_COUNT = SKILL_STATE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Skill Operational State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_OPERATIONAL_STATE_OPERATION_COUNT = SKILL_STATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.TransitionEdgeImpl <em>Transition Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.TransitionEdgeImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getTransitionEdge()
	 * @generated
	 */
	int TRANSITION_EDGE = 10;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__PROPERTY = BPCPackage.CONNECTS__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__INSTANCE_UID = BPCPackage.CONNECTS__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__DESCRIPTION = BPCPackage.CONNECTS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__AUTHORSHIP = BPCPackage.CONNECTS__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__PROVENANCE = BPCPackage.CONNECTS__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__MODEL_UID = BPCPackage.CONNECTS__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__METAMODEL_UID = BPCPackage.CONNECTS__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__PORT = BPCPackage.CONNECTS__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__CONNECTOR = BPCPackage.CONNECTS__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__COLLECTION = BPCPackage.CONNECTS__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__BLOCK = BPCPackage.CONNECTS__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__RELATION = BPCPackage.CONNECTS__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__BASE_CLASS = BPCPackage.CONNECTS__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__REIFICATION = BPCPackage.CONNECTS__REIFICATION;

	/**
	 * The feature id for the '<em><b>Base Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE__BASE_TRANSITION = BPCPackage.CONNECTS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Transition Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE_FEATURE_COUNT = BPCPackage.CONNECTS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Transition Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_EDGE_OPERATION_COUNT = BPCPackage.CONNECTS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillInitialStateImpl <em>Skill Initial State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillInitialStateImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillInitialState()
	 * @generated
	 */
	int SKILL_INITIAL_STATE = 11;

	/**
	 * The feature id for the '<em><b>Base Pseudostate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_INITIAL_STATE__BASE_PSEUDOSTATE = 0;

	/**
	 * The number of structural features of the '<em>Skill Initial State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_INITIAL_STATE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Skill Initial State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_INITIAL_STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFSMRegionImpl <em>Skill FSM Region</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFSMRegionImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillFSMRegion()
	 * @generated
	 */
	int SKILL_FSM_REGION = 12;

	/**
	 * The feature id for the '<em><b>Base Region</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FSM_REGION__BASE_REGION = 0;

	/**
	 * The number of structural features of the '<em>Skill FSM Region</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FSM_REGION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Skill FSM Region</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_FSM_REGION_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__PROPERTY = BPCPackage.BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__INSTANCE_UID = BPCPackage.BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__DESCRIPTION = BPCPackage.BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__AUTHORSHIP = BPCPackage.BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__PROVENANCE = BPCPackage.BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__MODEL_UID = BPCPackage.BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__METAMODEL_UID = BPCPackage.BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__PORT = BPCPackage.BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__CONNECTOR = BPCPackage.BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__COLLECTION = BPCPackage.BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__BLOCK = BPCPackage.BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__RELATION = BPCPackage.BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__BASE_CLASS = BPCPackage.BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Ins</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__INS = BPCPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__OUTS = BPCPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Res</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__RES = BPCPackage.BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Default Semantic</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__DEFAULT_SEMANTIC = BPCPackage.BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Base Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION__BASE_OPERATION = BPCPackage.BLOCK_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Skill Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_FEATURE_COUNT = BPCPackage.BLOCK_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Skill Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKILL_DEFINITION_OPERATION_COUNT = BPCPackage.BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind <em>Skill Result Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillResultKind()
	 * @generated
	 */
	int SKILL_RESULT_KIND = 14;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet <em>Skill Definition Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Definition Set</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet
	 * @generated
	 */
	EClass getSkillDefinitionSet();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet#getSkills <em>Skills</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Skills</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet#getSkills()
	 * @see #getSkillDefinitionSet()
	 * @generated
	 */
	EReference getSkillDefinitionSet_Skills();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet#getBase_Interface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Interface</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet#getBase_Interface()
	 * @see #getSkillDefinitionSet()
	 * @generated
	 */
	EReference getSkillDefinitionSet_Base_Interface();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition <em>Skill Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Definition</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition
	 * @generated
	 */
	EClass getSkillDefinition();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getIns <em>Ins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ins</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getIns()
	 * @see #getSkillDefinition()
	 * @generated
	 */
	EReference getSkillDefinition_Ins();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getOuts <em>Outs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outs</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getOuts()
	 * @see #getSkillDefinition()
	 * @generated
	 */
	EReference getSkillDefinition_Outs();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getRes <em>Res</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Res</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getRes()
	 * @see #getSkillDefinition()
	 * @generated
	 */
	EReference getSkillDefinition_Res();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getDefaultSemantic <em>Default Semantic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Semantic</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getDefaultSemantic()
	 * @see #getSkillDefinition()
	 * @generated
	 */
	EReference getSkillDefinition_DefaultSemantic();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getBase_Operation <em>Base Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Operation</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition#getBase_Operation()
	 * @see #getSkillDefinition()
	 * @generated
	 */
	EReference getSkillDefinition_Base_Operation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute <em>In Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute
	 * @generated
	 */
	EClass getInAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter <em>Skill Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Parameter</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter
	 * @generated
	 */
	EClass getSkillParameter();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter#getBase_Parameter <em>Base Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Parameter</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter#getBase_Parameter()
	 * @see #getSkillParameter()
	 * @generated
	 */
	EReference getSkillParameter_Base_Parameter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute <em>Out Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute
	 * @generated
	 */
	EClass getOutAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult <em>Skill Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Result</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult
	 * @generated
	 */
	EClass getSkillResult();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult#getKind()
	 * @see #getSkillResult()
	 * @generated
	 */
	EAttribute getSkillResult_Kind();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult#getValue()
	 * @see #getSkillResult()
	 * @generated
	 */
	EAttribute getSkillResult_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic <em>Skill Semantic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Semantic</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic
	 * @generated
	 */
	EClass getSkillSemantic();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getSuccess <em>Success</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Success</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getSuccess()
	 * @see #getSkillSemantic()
	 * @generated
	 */
	EReference getSkillSemantic_Success();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getFail <em>Fail</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fail</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getFail()
	 * @see #getSkillSemantic()
	 * @generated
	 */
	EReference getSkillSemantic_Fail();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getOperational <em>Operational</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Operational</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getOperational()
	 * @see #getSkillSemantic()
	 * @generated
	 */
	EReference getSkillSemantic_Operational();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getSuccEvts <em>Succ Evts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Succ Evts</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getSuccEvts()
	 * @see #getSkillSemantic()
	 * @generated
	 */
	EReference getSkillSemantic_SuccEvts();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getFailEvts <em>Fail Evts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Fail Evts</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getFailEvts()
	 * @see #getSkillSemantic()
	 * @generated
	 */
	EReference getSkillSemantic_FailEvts();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getBase_StateMachine <em>Base State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base State Machine</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getBase_StateMachine()
	 * @see #getSkillSemantic()
	 * @generated
	 */
	EReference getSkillSemantic_Base_StateMachine();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSuccessState <em>Skill Success State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Success State</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSuccessState
	 * @generated
	 */
	EClass getSkillSuccessState();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState <em>Skill State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill State</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState
	 * @generated
	 */
	EClass getSkillState();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState#getBase_State <em>Base State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base State</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState#getBase_State()
	 * @see #getSkillState()
	 * @generated
	 */
	EReference getSkillState_Base_State();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState#getFsm <em>Fsm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Fsm</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState#getFsm()
	 * @see #getSkillState()
	 * @generated
	 */
	EReference getSkillState_Fsm();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFailState <em>Skill Fail State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Fail State</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFailState
	 * @generated
	 */
	EClass getSkillFailState();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState <em>Skill Operational State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Operational State</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState
	 * @generated
	 */
	EClass getSkillOperationalState();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState#getCompInterface <em>Comp Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Comp Interface</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState#getCompInterface()
	 * @see #getSkillOperationalState()
	 * @generated
	 */
	EReference getSkillOperationalState_CompInterface();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.TransitionEdge <em>Transition Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition Edge</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.TransitionEdge
	 * @generated
	 */
	EClass getTransitionEdge();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.TransitionEdge#getBase_Transition <em>Base Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Transition</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.TransitionEdge#getBase_Transition()
	 * @see #getTransitionEdge()
	 * @generated
	 */
	EReference getTransitionEdge_Base_Transition();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillInitialState <em>Skill Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill Initial State</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillInitialState
	 * @generated
	 */
	EClass getSkillInitialState();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillInitialState#getBase_Pseudostate <em>Base Pseudostate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Pseudostate</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillInitialState#getBase_Pseudostate()
	 * @see #getSkillInitialState()
	 * @generated
	 */
	EReference getSkillInitialState_Base_Pseudostate();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFSMRegion <em>Skill FSM Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Skill FSM Region</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFSMRegion
	 * @generated
	 */
	EClass getSkillFSMRegion();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFSMRegion#getBase_Region <em>Base Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Region</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFSMRegion#getBase_Region()
	 * @see #getSkillFSMRegion()
	 * @generated
	 */
	EReference getSkillFSMRegion_Base_Region();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind <em>Skill Result Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Skill Result Kind</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind
	 * @generated
	 */
	EEnum getSkillResultKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SkillsFactory getSkillsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionSetImpl <em>Skill Definition Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionSetImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillDefinitionSet()
		 * @generated
		 */
		EClass SKILL_DEFINITION_SET = eINSTANCE.getSkillDefinitionSet();

		/**
		 * The meta object literal for the '<em><b>Skills</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION_SET__SKILLS = eINSTANCE.getSkillDefinitionSet_Skills();

		/**
		 * The meta object literal for the '<em><b>Base Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION_SET__BASE_INTERFACE = eINSTANCE.getSkillDefinitionSet_Base_Interface();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl <em>Skill Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillDefinition()
		 * @generated
		 */
		EClass SKILL_DEFINITION = eINSTANCE.getSkillDefinition();

		/**
		 * The meta object literal for the '<em><b>Ins</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION__INS = eINSTANCE.getSkillDefinition_Ins();

		/**
		 * The meta object literal for the '<em><b>Outs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION__OUTS = eINSTANCE.getSkillDefinition_Outs();

		/**
		 * The meta object literal for the '<em><b>Res</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION__RES = eINSTANCE.getSkillDefinition_Res();

		/**
		 * The meta object literal for the '<em><b>Default Semantic</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION__DEFAULT_SEMANTIC = eINSTANCE.getSkillDefinition_DefaultSemantic();

		/**
		 * The meta object literal for the '<em><b>Base Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_DEFINITION__BASE_OPERATION = eINSTANCE.getSkillDefinition_Base_Operation();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.InAttributeImpl <em>In Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.InAttributeImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getInAttribute()
		 * @generated
		 */
		EClass IN_ATTRIBUTE = eINSTANCE.getInAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillParameterImpl <em>Skill Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillParameterImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillParameter()
		 * @generated
		 */
		EClass SKILL_PARAMETER = eINSTANCE.getSkillParameter();

		/**
		 * The meta object literal for the '<em><b>Base Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_PARAMETER__BASE_PARAMETER = eINSTANCE.getSkillParameter_Base_Parameter();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.OutAttributeImpl <em>Out Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.OutAttributeImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getOutAttribute()
		 * @generated
		 */
		EClass OUT_ATTRIBUTE = eINSTANCE.getOutAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillResultImpl <em>Skill Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillResultImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillResult()
		 * @generated
		 */
		EClass SKILL_RESULT = eINSTANCE.getSkillResult();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL_RESULT__KIND = eINSTANCE.getSkillResult_Kind();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKILL_RESULT__VALUE = eINSTANCE.getSkillResult_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl <em>Skill Semantic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSemanticImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillSemantic()
		 * @generated
		 */
		EClass SKILL_SEMANTIC = eINSTANCE.getSkillSemantic();

		/**
		 * The meta object literal for the '<em><b>Success</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_SEMANTIC__SUCCESS = eINSTANCE.getSkillSemantic_Success();

		/**
		 * The meta object literal for the '<em><b>Fail</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_SEMANTIC__FAIL = eINSTANCE.getSkillSemantic_Fail();

		/**
		 * The meta object literal for the '<em><b>Operational</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_SEMANTIC__OPERATIONAL = eINSTANCE.getSkillSemantic_Operational();

		/**
		 * The meta object literal for the '<em><b>Succ Evts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_SEMANTIC__SUCC_EVTS = eINSTANCE.getSkillSemantic_SuccEvts();

		/**
		 * The meta object literal for the '<em><b>Fail Evts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_SEMANTIC__FAIL_EVTS = eINSTANCE.getSkillSemantic_FailEvts();

		/**
		 * The meta object literal for the '<em><b>Base State Machine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_SEMANTIC__BASE_STATE_MACHINE = eINSTANCE.getSkillSemantic_Base_StateMachine();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSuccessStateImpl <em>Skill Success State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillSuccessStateImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillSuccessState()
		 * @generated
		 */
		EClass SKILL_SUCCESS_STATE = eINSTANCE.getSkillSuccessState();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillStateImpl <em>Skill State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillStateImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillState()
		 * @generated
		 */
		EClass SKILL_STATE = eINSTANCE.getSkillState();

		/**
		 * The meta object literal for the '<em><b>Base State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_STATE__BASE_STATE = eINSTANCE.getSkillState_Base_State();

		/**
		 * The meta object literal for the '<em><b>Fsm</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_STATE__FSM = eINSTANCE.getSkillState_Fsm();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFailStateImpl <em>Skill Fail State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFailStateImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillFailState()
		 * @generated
		 */
		EClass SKILL_FAIL_STATE = eINSTANCE.getSkillFailState();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillOperationalStateImpl <em>Skill Operational State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillOperationalStateImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillOperationalState()
		 * @generated
		 */
		EClass SKILL_OPERATIONAL_STATE = eINSTANCE.getSkillOperationalState();

		/**
		 * The meta object literal for the '<em><b>Comp Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_OPERATIONAL_STATE__COMP_INTERFACE = eINSTANCE.getSkillOperationalState_CompInterface();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.TransitionEdgeImpl <em>Transition Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.TransitionEdgeImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getTransitionEdge()
		 * @generated
		 */
		EClass TRANSITION_EDGE = eINSTANCE.getTransitionEdge();

		/**
		 * The meta object literal for the '<em><b>Base Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION_EDGE__BASE_TRANSITION = eINSTANCE.getTransitionEdge_Base_Transition();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillInitialStateImpl <em>Skill Initial State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillInitialStateImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillInitialState()
		 * @generated
		 */
		EClass SKILL_INITIAL_STATE = eINSTANCE.getSkillInitialState();

		/**
		 * The meta object literal for the '<em><b>Base Pseudostate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_INITIAL_STATE__BASE_PSEUDOSTATE = eINSTANCE.getSkillInitialState_Base_Pseudostate();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFSMRegionImpl <em>Skill FSM Region</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillFSMRegionImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillFSMRegion()
		 * @generated
		 */
		EClass SKILL_FSM_REGION = eINSTANCE.getSkillFSMRegion();

		/**
		 * The meta object literal for the '<em><b>Base Region</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKILL_FSM_REGION__BASE_REGION = eINSTANCE.getSkillFSMRegion_Base_Region();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind <em>Skill Result Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind
		 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl#getSkillResultKind()
		 * @generated
		 */
		EEnum SKILL_RESULT_KIND = eINSTANCE.getSkillResultKind();

	}

} //SkillsPackage
