/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.components.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinitionModel;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Definition Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionModelImpl#getBase_Package <em>Base Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionModelImpl#getDependsPackage <em>Depends Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionModelImpl#isRegistered <em>Registered</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionModelImpl#isExternal <em>External</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentDefinitionModelImpl#getLicense <em>License</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentDefinitionModelImpl extends EntityImpl implements ComponentDefinitionModel {
	/**
	 * The cached value of the '{@link #getBase_Package() <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Package()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package base_Package;

	/**
	 * The cached value of the '{@link #getDependsPackage() <em>Depends Package</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependsPackage()
	 * @generated
	 * @ordered
	 */
	protected EList<String> dependsPackage;

	/**
	 * The default value of the '{@link #isRegistered() <em>Registered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRegistered()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REGISTERED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRegistered() <em>Registered</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRegistered()
	 * @generated
	 * @ordered
	 */
	protected boolean registered = REGISTERED_EDEFAULT;

	/**
	 * The default value of the '{@link #isExternal() <em>External</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXTERNAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExternal() <em>External</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExternal()
	 * @generated
	 * @ordered
	 */
	protected boolean external = EXTERNAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected static final String LICENSE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLicense() <em>License</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLicense()
	 * @generated
	 * @ordered
	 */
	protected String license = LICENSE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentDefinitionModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsPackage.Literals.COMPONENT_DEFINITION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Package getBase_Package() {
		if (base_Package != null && base_Package.eIsProxy()) {
			InternalEObject oldBase_Package = (InternalEObject)base_Package;
			base_Package = (org.eclipse.uml2.uml.Package)eResolveProxy(oldBase_Package);
			if (base_Package != oldBase_Package) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComponentsPackage.COMPONENT_DEFINITION_MODEL__BASE_PACKAGE, oldBase_Package, base_Package));
			}
		}
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetBase_Package() {
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Package(org.eclipse.uml2.uml.Package newBase_Package) {
		org.eclipse.uml2.uml.Package oldBase_Package = base_Package;
		base_Package = newBase_Package;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_DEFINITION_MODEL__BASE_PACKAGE, oldBase_Package, base_Package));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getDependsPackage() {
		if (dependsPackage == null) {
			dependsPackage = new EDataTypeUniqueEList<String>(String.class, this, ComponentsPackage.COMPONENT_DEFINITION_MODEL__DEPENDS_PACKAGE);
		}
		return dependsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isRegistered() {
		return registered;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRegistered(boolean newRegistered) {
		boolean oldRegistered = registered;
		registered = newRegistered;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_DEFINITION_MODEL__REGISTERED, oldRegistered, registered));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isExternal() {
		return external;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExternal(boolean newExternal) {
		boolean oldExternal = external;
		external = newExternal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_DEFINITION_MODEL__EXTERNAL, oldExternal, external));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLicense() {
		return license;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLicense(String newLicense) {
		String oldLicense = license;
		license = newLicense;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_DEFINITION_MODEL__LICENSE, oldLicense, license));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__BASE_PACKAGE:
				if (resolve) return getBase_Package();
				return basicGetBase_Package();
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__DEPENDS_PACKAGE:
				return getDependsPackage();
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__REGISTERED:
				return isRegistered();
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__EXTERNAL:
				return isExternal();
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__LICENSE:
				return getLicense();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)newValue);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__DEPENDS_PACKAGE:
				getDependsPackage().clear();
				getDependsPackage().addAll((Collection<? extends String>)newValue);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__REGISTERED:
				setRegistered((Boolean)newValue);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__EXTERNAL:
				setExternal((Boolean)newValue);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__LICENSE:
				setLicense((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)null);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__DEPENDS_PACKAGE:
				getDependsPackage().clear();
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__REGISTERED:
				setRegistered(REGISTERED_EDEFAULT);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__EXTERNAL:
				setExternal(EXTERNAL_EDEFAULT);
				return;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__LICENSE:
				setLicense(LICENSE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__BASE_PACKAGE:
				return base_Package != null;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__DEPENDS_PACKAGE:
				return dependsPackage != null && !dependsPackage.isEmpty();
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__REGISTERED:
				return registered != REGISTERED_EDEFAULT;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__EXTERNAL:
				return external != EXTERNAL_EDEFAULT;
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL__LICENSE:
				return LICENSE_EDEFAULT == null ? license != null : !LICENSE_EDEFAULT.equals(license);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dependsPackage: "); //$NON-NLS-1$
		result.append(dependsPackage);
		result.append(", registered: "); //$NON-NLS-1$
		result.append(registered);
		result.append(", external: "); //$NON-NLS-1$
		result.append(external);
		result.append(", license: "); //$NON-NLS-1$
		result.append(license);
		result.append(')');
		return result.toString();
	}

} //ComponentDefinitionModelImpl
