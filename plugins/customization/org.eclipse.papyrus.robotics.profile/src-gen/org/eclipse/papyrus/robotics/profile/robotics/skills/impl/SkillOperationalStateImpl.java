/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Skill Operational State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillOperationalStateImpl#getCompInterface <em>Comp Interface</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SkillOperationalStateImpl extends SkillStateImpl implements SkillOperationalState {
	/**
	 * The cached value of the '{@link #getCompInterface() <em>Comp Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompInterface()
	 * @generated
	 * @ordered
	 */
	protected CoordinationService compInterface;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SkillOperationalStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SkillsPackage.Literals.SKILL_OPERATIONAL_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CoordinationService getCompInterface() {
		if (compInterface != null && compInterface.eIsProxy()) {
			InternalEObject oldCompInterface = (InternalEObject)compInterface;
			compInterface = (CoordinationService)eResolveProxy(oldCompInterface);
			if (compInterface != oldCompInterface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SkillsPackage.SKILL_OPERATIONAL_STATE__COMP_INTERFACE, oldCompInterface, compInterface));
			}
		}
		return compInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoordinationService basicGetCompInterface() {
		return compInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCompInterface(CoordinationService newCompInterface) {
		CoordinationService oldCompInterface = compInterface;
		compInterface = newCompInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SkillsPackage.SKILL_OPERATIONAL_STATE__COMP_INTERFACE, oldCompInterface, compInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SkillsPackage.SKILL_OPERATIONAL_STATE__COMP_INTERFACE:
				if (resolve) return getCompInterface();
				return basicGetCompInterface();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SkillsPackage.SKILL_OPERATIONAL_STATE__COMP_INTERFACE:
				setCompInterface((CoordinationService)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_OPERATIONAL_STATE__COMP_INTERFACE:
				setCompInterface((CoordinationService)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_OPERATIONAL_STATE__COMP_INTERFACE:
				return compInterface != null;
		}
		return super.eIsSet(featureID);
	}

} //SkillOperationalStateImpl
