/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.commpattern;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternPackage
 * @generated
 */
public interface CommpatternFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CommpatternFactory eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Communication Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Pattern</em>'.
	 * @generated
	 */
	CommunicationPattern createCommunicationPattern();

	/**
	 * Returns a new object of class '<em>Communication Pattern Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Pattern Definition</em>'.
	 * @generated
	 */
	CommunicationPatternDefinition createCommunicationPatternDefinition();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CommpatternPackage getCommpatternPackage();

} //CommpatternFactory
