/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.impl;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

import org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage;

import org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage;

import org.eclipse.papyrus.robotics.profile.robotics.commobject.impl.CommobjectPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternPackage;

import org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;

import org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentsPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.deployment.DeploymentPackage;

import org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.dummy;

import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage;

import org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionsPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.generics.GenericsPackage;

import org.eclipse.papyrus.robotics.profile.robotics.generics.impl.GenericsPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersPackage;

import org.eclipse.papyrus.robotics.profile.robotics.parameters.impl.ParametersPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.roboticsFactory;
import org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage;

import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage;

import org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl;

import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;

import org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class roboticsPackageImpl extends EPackageImpl implements roboticsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dummyEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private roboticsPackageImpl() {
		super(eNS_URI, roboticsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link roboticsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static roboticsPackage init() {
		if (isInited) return (roboticsPackage)EPackage.Registry.INSTANCE.getEPackage(roboticsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredroboticsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		roboticsPackageImpl theroboticsPackage = registeredroboticsPackage instanceof roboticsPackageImpl ? (roboticsPackageImpl)registeredroboticsPackage : new roboticsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BPCPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		ComponentsPackageImpl theComponentsPackage = (ComponentsPackageImpl)(registeredPackage instanceof ComponentsPackageImpl ? registeredPackage : ComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FunctionsPackage.eNS_URI);
		FunctionsPackageImpl theFunctionsPackage = (FunctionsPackageImpl)(registeredPackage instanceof FunctionsPackageImpl ? registeredPackage : FunctionsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);
		ServicesPackageImpl theServicesPackage = (ServicesPackageImpl)(registeredPackage instanceof ServicesPackageImpl ? registeredPackage : ServicesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ParametersPackage.eNS_URI);
		ParametersPackageImpl theParametersPackage = (ParametersPackageImpl)(registeredPackage instanceof ParametersPackageImpl ? registeredPackage : ParametersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		BehaviorPackageImpl theBehaviorPackage = (BehaviorPackageImpl)(registeredPackage instanceof BehaviorPackageImpl ? registeredPackage : BehaviorPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);
		SkillsPackageImpl theSkillsPackage = (SkillsPackageImpl)(registeredPackage instanceof SkillsPackageImpl ? registeredPackage : SkillsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommpatternPackage.eNS_URI);
		CommpatternPackageImpl theCommpatternPackage = (CommpatternPackageImpl)(registeredPackage instanceof CommpatternPackageImpl ? registeredPackage : CommpatternPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(registeredPackage instanceof DeploymentPackageImpl ? registeredPackage : DeploymentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommobjectPackage.eNS_URI);
		CommobjectPackageImpl theCommobjectPackage = (CommobjectPackageImpl)(registeredPackage instanceof CommobjectPackageImpl ? registeredPackage : CommobjectPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GenericsPackage.eNS_URI);
		GenericsPackageImpl theGenericsPackage = (GenericsPackageImpl)(registeredPackage instanceof GenericsPackageImpl ? registeredPackage : GenericsPackage.eINSTANCE);

		// Create package meta-data objects
		theroboticsPackage.createPackageContents();
		theComponentsPackage.createPackageContents();
		theFunctionsPackage.createPackageContents();
		theServicesPackage.createPackageContents();
		theParametersPackage.createPackageContents();
		theBehaviorPackage.createPackageContents();
		theSkillsPackage.createPackageContents();
		theCommpatternPackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theCommobjectPackage.createPackageContents();
		theGenericsPackage.createPackageContents();

		// Initialize created meta-data
		theroboticsPackage.initializePackageContents();
		theComponentsPackage.initializePackageContents();
		theFunctionsPackage.initializePackageContents();
		theServicesPackage.initializePackageContents();
		theParametersPackage.initializePackageContents();
		theBehaviorPackage.initializePackageContents();
		theSkillsPackage.initializePackageContents();
		theCommpatternPackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theCommobjectPackage.initializePackageContents();
		theGenericsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theroboticsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(roboticsPackage.eNS_URI, theroboticsPackage);
		return theroboticsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getdummy() {
		return dummyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public roboticsFactory getroboticsFactory() {
		return (roboticsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create enums
		dummyEEnum = createEEnum(DUMMY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ComponentsPackage theComponentsPackage = (ComponentsPackage)EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		FunctionsPackage theFunctionsPackage = (FunctionsPackage)EPackage.Registry.INSTANCE.getEPackage(FunctionsPackage.eNS_URI);
		ServicesPackage theServicesPackage = (ServicesPackage)EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);
		ParametersPackage theParametersPackage = (ParametersPackage)EPackage.Registry.INSTANCE.getEPackage(ParametersPackage.eNS_URI);
		BehaviorPackage theBehaviorPackage = (BehaviorPackage)EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		SkillsPackage theSkillsPackage = (SkillsPackage)EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);
		CommpatternPackage theCommpatternPackage = (CommpatternPackage)EPackage.Registry.INSTANCE.getEPackage(CommpatternPackage.eNS_URI);
		DeploymentPackage theDeploymentPackage = (DeploymentPackage)EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		CommobjectPackage theCommobjectPackage = (CommobjectPackage)EPackage.Registry.INSTANCE.getEPackage(CommobjectPackage.eNS_URI);
		GenericsPackage theGenericsPackage = (GenericsPackage)EPackage.Registry.INSTANCE.getEPackage(GenericsPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theComponentsPackage);
		getESubpackages().add(theFunctionsPackage);
		getESubpackages().add(theServicesPackage);
		getESubpackages().add(theParametersPackage);
		getESubpackages().add(theBehaviorPackage);
		getESubpackages().add(theSkillsPackage);
		getESubpackages().add(theCommpatternPackage);
		getESubpackages().add(theDeploymentPackage);
		getESubpackages().add(theCommobjectPackage);
		getESubpackages().add(theGenericsPackage);

		// Initialize enums and add enum literals
		initEEnum(dummyEEnum, dummy.class, "dummy"); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);
	}

} //roboticsPackageImpl
