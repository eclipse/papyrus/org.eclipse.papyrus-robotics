/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.functions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Function Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#getFunctionKind()
 * @model
 * @generated
 */
public enum FunctionKind implements Enumerator {
	/**
	 * The '<em><b>ON CONFIGURE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_CONFIGURE_VALUE
	 * @generated
	 * @ordered
	 */
	ON_CONFIGURE(0, "ON_CONFIGURE", "ON_CONFIGURE"), /**
	 * The '<em><b>ON ACTIVATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_ACTIVATE_VALUE
	 * @generated
	 * @ordered
	 */
	ON_ACTIVATE(1, "ON_ACTIVATE", "ON_ACTIVATE"), /**
	 * The '<em><b>ON DEACTIVATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_DEACTIVATE_VALUE
	 * @generated
	 * @ordered
	 */
	ON_DEACTIVATE(2, "ON_DEACTIVATE", "ON_DEACTIVATE"), /**
	 * The '<em><b>ON SHUTDOWN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_SHUTDOWN_VALUE
	 * @generated
	 * @ordered
	 */
	ON_SHUTDOWN(3, "ON_SHUTDOWN", "ON_SHUTDOWN"), /**
	 * The '<em><b>ON CLEANUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_CLEANUP_VALUE
	 * @generated
	 * @ordered
	 */
	ON_CLEANUP(4, "ON_CLEANUP", "ON_CLEANUP"), /**
	 * The '<em><b>ON ERROR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_ERROR_VALUE
	 * @generated
	 * @ordered
	 */
	ON_ERROR(5, "ON_ERROR", "ON_ERROR"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>HANDLER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HANDLER_VALUE
	 * @generated
	 * @ordered
	 */
	HANDLER(6, "HANDLER", "HANDLER"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>PERIODIC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PERIODIC_VALUE
	 * @generated
	 * @ordered
	 */
	PERIODIC(7, "PERIODIC", "PERIODIC"), /**
	 * The '<em><b>MANUALLY CALLED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MANUALLY_CALLED_VALUE
	 * @generated
	 * @ordered
	 */
	MANUALLY_CALLED(8, "MANUALLY_CALLED", "MANUALLY_CALLED"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>ON CONFIGURE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_CONFIGURE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ON_CONFIGURE_VALUE = 0;

	/**
	 * The '<em><b>ON ACTIVATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_ACTIVATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ON_ACTIVATE_VALUE = 1;

	/**
	 * The '<em><b>ON DEACTIVATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_DEACTIVATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ON_DEACTIVATE_VALUE = 2;

	/**
	 * The '<em><b>ON SHUTDOWN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_SHUTDOWN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ON_SHUTDOWN_VALUE = 3;

	/**
	 * The '<em><b>ON CLEANUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_CLEANUP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ON_CLEANUP_VALUE = 4;

	/**
	 * The '<em><b>ON ERROR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ON_ERROR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ON_ERROR_VALUE = 5;

	/**
	 * The '<em><b>HANDLER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HANDLER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int HANDLER_VALUE = 6;

	/**
	 * The '<em><b>PERIODIC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PERIODIC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PERIODIC_VALUE = 7;

	/**
	 * The '<em><b>MANUALLY CALLED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MANUALLY_CALLED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MANUALLY_CALLED_VALUE = 8;

	/**
	 * An array of all the '<em><b>Function Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final FunctionKind[] VALUES_ARRAY =
		new FunctionKind[] {
			ON_CONFIGURE,
			ON_ACTIVATE,
			ON_DEACTIVATE,
			ON_SHUTDOWN,
			ON_CLEANUP,
			ON_ERROR,
			HANDLER,
			PERIODIC,
			MANUALLY_CALLED,
		};

	/**
	 * A public read-only list of all the '<em><b>Function Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<FunctionKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Function Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FunctionKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FunctionKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Function Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FunctionKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			FunctionKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Function Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static FunctionKind get(int value) {
		switch (value) {
			case ON_CONFIGURE_VALUE: return ON_CONFIGURE;
			case ON_ACTIVATE_VALUE: return ON_ACTIVATE;
			case ON_DEACTIVATE_VALUE: return ON_DEACTIVATE;
			case ON_SHUTDOWN_VALUE: return ON_SHUTDOWN;
			case ON_CLEANUP_VALUE: return ON_CLEANUP;
			case ON_ERROR_VALUE: return ON_ERROR;
			case HANDLER_VALUE: return HANDLER;
			case PERIODIC_VALUE: return PERIODIC;
			case MANUALLY_CALLED_VALUE: return MANUALLY_CALLED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private FunctionKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //FunctionKind
