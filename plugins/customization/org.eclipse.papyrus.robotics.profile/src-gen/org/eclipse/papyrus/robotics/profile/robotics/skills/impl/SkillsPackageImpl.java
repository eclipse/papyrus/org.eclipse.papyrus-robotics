/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.impl.CommobjectPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.DeploymentPackage;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.generics.GenericsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.generics.impl.GenericsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.impl.roboticsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersPackage;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.impl.ParametersPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFSMRegion;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillFailState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillInitialState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSuccessState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsFactory;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.skills.TransitionEdge;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SkillsPackageImpl extends EPackageImpl implements SkillsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillDefinitionSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillSemanticEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillSuccessStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillFailStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillOperationalStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillInitialStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass skillFSMRegionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum skillResultKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SkillsPackageImpl() {
		super(eNS_URI, SkillsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link SkillsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SkillsPackage init() {
		if (isInited) return (SkillsPackage)EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSkillsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		SkillsPackageImpl theSkillsPackage = registeredSkillsPackage instanceof SkillsPackageImpl ? (SkillsPackageImpl)registeredSkillsPackage : new SkillsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BPCPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(roboticsPackage.eNS_URI);
		roboticsPackageImpl theroboticsPackage = (roboticsPackageImpl)(registeredPackage instanceof roboticsPackageImpl ? registeredPackage : roboticsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		ComponentsPackageImpl theComponentsPackage = (ComponentsPackageImpl)(registeredPackage instanceof ComponentsPackageImpl ? registeredPackage : ComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FunctionsPackage.eNS_URI);
		FunctionsPackageImpl theFunctionsPackage = (FunctionsPackageImpl)(registeredPackage instanceof FunctionsPackageImpl ? registeredPackage : FunctionsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);
		ServicesPackageImpl theServicesPackage = (ServicesPackageImpl)(registeredPackage instanceof ServicesPackageImpl ? registeredPackage : ServicesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ParametersPackage.eNS_URI);
		ParametersPackageImpl theParametersPackage = (ParametersPackageImpl)(registeredPackage instanceof ParametersPackageImpl ? registeredPackage : ParametersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		BehaviorPackageImpl theBehaviorPackage = (BehaviorPackageImpl)(registeredPackage instanceof BehaviorPackageImpl ? registeredPackage : BehaviorPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommpatternPackage.eNS_URI);
		CommpatternPackageImpl theCommpatternPackage = (CommpatternPackageImpl)(registeredPackage instanceof CommpatternPackageImpl ? registeredPackage : CommpatternPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(registeredPackage instanceof DeploymentPackageImpl ? registeredPackage : DeploymentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommobjectPackage.eNS_URI);
		CommobjectPackageImpl theCommobjectPackage = (CommobjectPackageImpl)(registeredPackage instanceof CommobjectPackageImpl ? registeredPackage : CommobjectPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GenericsPackage.eNS_URI);
		GenericsPackageImpl theGenericsPackage = (GenericsPackageImpl)(registeredPackage instanceof GenericsPackageImpl ? registeredPackage : GenericsPackage.eINSTANCE);

		// Create package meta-data objects
		theSkillsPackage.createPackageContents();
		theroboticsPackage.createPackageContents();
		theComponentsPackage.createPackageContents();
		theFunctionsPackage.createPackageContents();
		theServicesPackage.createPackageContents();
		theParametersPackage.createPackageContents();
		theBehaviorPackage.createPackageContents();
		theCommpatternPackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theCommobjectPackage.createPackageContents();
		theGenericsPackage.createPackageContents();

		// Initialize created meta-data
		theSkillsPackage.initializePackageContents();
		theroboticsPackage.initializePackageContents();
		theComponentsPackage.initializePackageContents();
		theFunctionsPackage.initializePackageContents();
		theServicesPackage.initializePackageContents();
		theParametersPackage.initializePackageContents();
		theBehaviorPackage.initializePackageContents();
		theCommpatternPackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theCommobjectPackage.initializePackageContents();
		theGenericsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSkillsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SkillsPackage.eNS_URI, theSkillsPackage);
		return theSkillsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillDefinitionSet() {
		return skillDefinitionSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinitionSet_Skills() {
		return (EReference)skillDefinitionSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinitionSet_Base_Interface() {
		return (EReference)skillDefinitionSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillDefinition() {
		return skillDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinition_Ins() {
		return (EReference)skillDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinition_Outs() {
		return (EReference)skillDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinition_Res() {
		return (EReference)skillDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinition_DefaultSemantic() {
		return (EReference)skillDefinitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillDefinition_Base_Operation() {
		return (EReference)skillDefinitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInAttribute() {
		return inAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillParameter() {
		return skillParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillParameter_Base_Parameter() {
		return (EReference)skillParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOutAttribute() {
		return outAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillResult() {
		return skillResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkillResult_Kind() {
		return (EAttribute)skillResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSkillResult_Value() {
		return (EAttribute)skillResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillSemantic() {
		return skillSemanticEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillSemantic_Success() {
		return (EReference)skillSemanticEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillSemantic_Fail() {
		return (EReference)skillSemanticEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillSemantic_Operational() {
		return (EReference)skillSemanticEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillSemantic_SuccEvts() {
		return (EReference)skillSemanticEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillSemantic_FailEvts() {
		return (EReference)skillSemanticEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillSemantic_Base_StateMachine() {
		return (EReference)skillSemanticEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillSuccessState() {
		return skillSuccessStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillState() {
		return skillStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillState_Base_State() {
		return (EReference)skillStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillState_Fsm() {
		return (EReference)skillStateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
  @Override
	public EClass getSkillFailState() {
		return skillFailStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillOperationalState() {
		return skillOperationalStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillOperationalState_CompInterface() {
		return (EReference)skillOperationalStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTransitionEdge() {
		return transitionEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTransitionEdge_Base_Transition() {
		return (EReference)transitionEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillInitialState() {
		return skillInitialStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillInitialState_Base_Pseudostate() {
		return (EReference)skillInitialStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSkillFSMRegion() {
		return skillFSMRegionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSkillFSMRegion_Base_Region() {
		return (EReference)skillFSMRegionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSkillResultKind() {
		return skillResultKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillsFactory getSkillsFactory() {
		return (SkillsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		skillDefinitionSetEClass = createEClass(SKILL_DEFINITION_SET);
		createEReference(skillDefinitionSetEClass, SKILL_DEFINITION_SET__SKILLS);
		createEReference(skillDefinitionSetEClass, SKILL_DEFINITION_SET__BASE_INTERFACE);

		inAttributeEClass = createEClass(IN_ATTRIBUTE);

		skillParameterEClass = createEClass(SKILL_PARAMETER);
		createEReference(skillParameterEClass, SKILL_PARAMETER__BASE_PARAMETER);

		outAttributeEClass = createEClass(OUT_ATTRIBUTE);

		skillResultEClass = createEClass(SKILL_RESULT);
		createEAttribute(skillResultEClass, SKILL_RESULT__KIND);
		createEAttribute(skillResultEClass, SKILL_RESULT__VALUE);

		skillSemanticEClass = createEClass(SKILL_SEMANTIC);
		createEReference(skillSemanticEClass, SKILL_SEMANTIC__SUCCESS);
		createEReference(skillSemanticEClass, SKILL_SEMANTIC__FAIL);
		createEReference(skillSemanticEClass, SKILL_SEMANTIC__OPERATIONAL);
		createEReference(skillSemanticEClass, SKILL_SEMANTIC__SUCC_EVTS);
		createEReference(skillSemanticEClass, SKILL_SEMANTIC__FAIL_EVTS);
		createEReference(skillSemanticEClass, SKILL_SEMANTIC__BASE_STATE_MACHINE);

		skillSuccessStateEClass = createEClass(SKILL_SUCCESS_STATE);

		skillStateEClass = createEClass(SKILL_STATE);
		createEReference(skillStateEClass, SKILL_STATE__BASE_STATE);
		createEReference(skillStateEClass, SKILL_STATE__FSM);

		skillFailStateEClass = createEClass(SKILL_FAIL_STATE);

		skillOperationalStateEClass = createEClass(SKILL_OPERATIONAL_STATE);
		createEReference(skillOperationalStateEClass, SKILL_OPERATIONAL_STATE__COMP_INTERFACE);

		transitionEdgeEClass = createEClass(TRANSITION_EDGE);
		createEReference(transitionEdgeEClass, TRANSITION_EDGE__BASE_TRANSITION);

		skillInitialStateEClass = createEClass(SKILL_INITIAL_STATE);
		createEReference(skillInitialStateEClass, SKILL_INITIAL_STATE__BASE_PSEUDOSTATE);

		skillFSMRegionEClass = createEClass(SKILL_FSM_REGION);
		createEReference(skillFSMRegionEClass, SKILL_FSM_REGION__BASE_REGION);

		skillDefinitionEClass = createEClass(SKILL_DEFINITION);
		createEReference(skillDefinitionEClass, SKILL_DEFINITION__INS);
		createEReference(skillDefinitionEClass, SKILL_DEFINITION__OUTS);
		createEReference(skillDefinitionEClass, SKILL_DEFINITION__RES);
		createEReference(skillDefinitionEClass, SKILL_DEFINITION__DEFAULT_SEMANTIC);
		createEReference(skillDefinitionEClass, SKILL_DEFINITION__BASE_OPERATION);

		// Create enums
		skillResultKindEEnum = createEEnum(SKILL_RESULT_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BPCPackage theBPCPackage = (BPCPackage)EPackage.Registry.INSTANCE.getEPackage(BPCPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		ServicesPackage theServicesPackage = (ServicesPackage)EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		skillDefinitionSetEClass.getESuperTypes().add(theBPCPackage.getEntity());
		inAttributeEClass.getESuperTypes().add(this.getSkillParameter());
		skillParameterEClass.getESuperTypes().add(theBPCPackage.getPort());
		outAttributeEClass.getESuperTypes().add(this.getSkillParameter());
		skillResultEClass.getESuperTypes().add(this.getSkillParameter());
		skillSemanticEClass.getESuperTypes().add(theBPCPackage.getBlock());
		skillSuccessStateEClass.getESuperTypes().add(this.getSkillState());
		skillStateEClass.getESuperTypes().add(theBPCPackage.getBlock());
		skillFailStateEClass.getESuperTypes().add(this.getSkillState());
		skillOperationalStateEClass.getESuperTypes().add(this.getSkillState());
		transitionEdgeEClass.getESuperTypes().add(theBPCPackage.getConnects());
		skillDefinitionEClass.getESuperTypes().add(theBPCPackage.getBlock());

		// Initialize classes, features, and operations; add parameters
		initEClass(skillDefinitionSetEClass, SkillDefinitionSet.class, "SkillDefinitionSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillDefinitionSet_Skills(), this.getSkillDefinition(), null, "skills", null, 1, -1, SkillDefinitionSet.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillDefinitionSet_Base_Interface(), theUMLPackage.getInterface(), null, "base_Interface", null, 0, 1, SkillDefinitionSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(inAttributeEClass, InAttribute.class, "InAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(skillParameterEClass, SkillParameter.class, "SkillParameter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillParameter_Base_Parameter(), theUMLPackage.getParameter(), null, "base_Parameter", null, 0, 1, SkillParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(outAttributeEClass, OutAttribute.class, "OutAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(skillResultEClass, SkillResult.class, "SkillResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getSkillResult_Kind(), this.getSkillResultKind(), "kind", null, 1, 1, SkillResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSkillResult_Value(), theTypesPackage.getString(), "value", null, 0, 1, SkillResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(skillSemanticEClass, SkillSemantic.class, "SkillSemantic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillSemantic_Success(), this.getSkillSuccessState(), null, "success", null, 1, 1, SkillSemantic.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillSemantic_Fail(), this.getSkillFailState(), null, "fail", null, 1, 1, SkillSemantic.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillSemantic_Operational(), this.getSkillOperationalState(), null, "operational", null, 0, -1, SkillSemantic.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillSemantic_SuccEvts(), theServicesPackage.getCoordinationEvent(), null, "succEvts", null, 1, -1, SkillSemantic.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillSemantic_FailEvts(), theServicesPackage.getCoordinationEvent(), null, "failEvts", null, 1, -1, SkillSemantic.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillSemantic_Base_StateMachine(), theUMLPackage.getStateMachine(), null, "base_StateMachine", null, 0, 1, SkillSemantic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(skillSuccessStateEClass, SkillSuccessState.class, "SkillSuccessState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(skillStateEClass, SkillState.class, "SkillState", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillState_Base_State(), theUMLPackage.getState(), null, "base_State", null, 0, 1, SkillState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillState_Fsm(), this.getSkillSemantic(), null, "fsm", null, 1, 1, SkillState.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(skillFailStateEClass, SkillFailState.class, "SkillFailState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(skillOperationalStateEClass, SkillOperationalState.class, "SkillOperationalState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillOperationalState_CompInterface(), theServicesPackage.getCoordinationService(), null, "compInterface", null, 0, 1, SkillOperationalState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(transitionEdgeEClass, TransitionEdge.class, "TransitionEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getTransitionEdge_Base_Transition(), theUMLPackage.getTransition(), null, "base_Transition", null, 0, 1, TransitionEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(skillInitialStateEClass, SkillInitialState.class, "SkillInitialState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillInitialState_Base_Pseudostate(), theUMLPackage.getPseudostate(), null, "base_Pseudostate", null, 0, 1, SkillInitialState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(skillFSMRegionEClass, SkillFSMRegion.class, "SkillFSMRegion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillFSMRegion_Base_Region(), theUMLPackage.getRegion(), null, "base_Region", null, 0, 1, SkillFSMRegion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(skillDefinitionEClass, SkillDefinition.class, "SkillDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSkillDefinition_Ins(), this.getInAttribute(), null, "ins", null, 0, -1, SkillDefinition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillDefinition_Outs(), this.getOutAttribute(), null, "outs", null, 0, -1, SkillDefinition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillDefinition_Res(), this.getSkillResult(), null, "res", null, 0, -1, SkillDefinition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillDefinition_DefaultSemantic(), this.getSkillSemantic(), null, "defaultSemantic", null, 1, 1, SkillDefinition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSkillDefinition_Base_Operation(), theUMLPackage.getOperation(), null, "base_Operation", null, 0, 1, SkillDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		// Initialize enums and add enum literals
		initEEnum(skillResultKindEEnum, SkillResultKind.class, "SkillResultKind"); //$NON-NLS-1$
		addEEnumLiteral(skillResultKindEEnum, SkillResultKind.RUNNING);
		addEEnumLiteral(skillResultKindEEnum, SkillResultKind.SUCCESS);
		addEEnumLiteral(skillResultKindEEnum, SkillResultKind.FAILURE);
	}

} //SkillsPackageImpl
