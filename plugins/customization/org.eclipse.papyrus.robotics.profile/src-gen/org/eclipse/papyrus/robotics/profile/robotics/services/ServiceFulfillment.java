/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.uml2.uml.Association;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Fulfillment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getWish <em>Wish</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getCInstance <em>CInstance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getCPort <em>CPort</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getBase_Association <em>Base Association</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceFulfillment()
 * @model
 * @generated
 */
public interface ServiceFulfillment extends Relation {
	/**
	 * Returns the value of the '<em><b>Wish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wish</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wish</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceFulfillment_Wish()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ServiceWish getWish();

	/**
	 * Returns the value of the '<em><b>CInstance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>CInstance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CInstance</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceFulfillment_CInstance()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ComponentInstance getCInstance();

	/**
	 * Returns the value of the '<em><b>CPort</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>CPort</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CPort</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceFulfillment_CPort()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	ComponentPort getCPort();

	/**
	 * Returns the value of the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Association</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Association</em>' reference.
	 * @see #setBase_Association(Association)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceFulfillment_Base_Association()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Association getBase_Association();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getBase_Association <em>Base Association</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Association</em>' reference.
	 * @see #getBase_Association()
	 * @generated
	 */
	void setBase_Association(Association value);

} // ServiceFulfillment
