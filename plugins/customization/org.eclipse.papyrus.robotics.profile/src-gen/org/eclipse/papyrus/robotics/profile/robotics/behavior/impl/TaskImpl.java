/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.behavior.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl;
import org.eclipse.papyrus.robotics.profile.behavior.TaskOperations;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl#getSkills <em>Skills</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl#getBase_Behavior <em>Base Behavior</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl#getIns <em>Ins</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.TaskImpl#getOuts <em>Outs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskImpl extends BlockImpl implements Task {
	/**
	 * The cached value of the '{@link #getBase_Behavior() <em>Base Behavior</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Behavior()
	 * @generated
	 * @ordered
	 */
	protected Behavior base_Behavior;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BehaviorPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Task> getTask() {
		return TaskOperations.getTask(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<SkillDefinition> getSkills() {
		EList<SkillDefinition> skills = new BasicEList<>();
		//
		// Sub Tasks are considered at the end, now we focus on the SkillDefinitions of the main Task
		//
		// In the robotics profile, Task extends UML::Behavior and SkillDefinition extends UML::Operation
		// We need to manage the different kinds of UML::Behaviors to get the list of skills
		//
		// case 1 (the only one supported at today (2020): UML::Activity
		if (base_Behavior instanceof Activity) {
			Activity act = (Activity) base_Behavior;
			// We look for SkillDefinitions by looking at UML::Operations referenced by CallOperationActions
			for (ActivityNode node : act.getOwnedNodes()) {
				// Only nodes with incoming edges are considered to return a list of
				// SkillDefinitions that are really used in the specification of the task
				if (!node.getIncomings().isEmpty()) {
					if (node instanceof CallOperationAction) {
						SkillDefinition sdef = UMLUtil.getStereotypeApplication(((CallOperationAction) node).getOperation(), SkillDefinition.class);
						if (sdef != null) {
							skills.add(sdef);
						}
					}
				}
			}
		}
		//
		// Now we collect the SkillDefinitions of sub Tasks (if any)
		//
		for (Task task : getTask()) {
			skills.addAll(task.getSkills());
		}
		return skills;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Behavior getBase_Behavior() {
		if (base_Behavior != null && base_Behavior.eIsProxy()) {
			InternalEObject oldBase_Behavior = (InternalEObject)base_Behavior;
			base_Behavior = (Behavior)eResolveProxy(oldBase_Behavior);
			if (base_Behavior != oldBase_Behavior) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BehaviorPackage.TASK__BASE_BEHAVIOR, oldBase_Behavior, base_Behavior));
			}
		}
		return base_Behavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Behavior basicGetBase_Behavior() {
		return base_Behavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Behavior(Behavior newBase_Behavior) {
		Behavior oldBase_Behavior = base_Behavior;
		base_Behavior = newBase_Behavior;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviorPackage.TASK__BASE_BEHAVIOR, oldBase_Behavior, base_Behavior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<InAttribute> getIns() {
		EList<InAttribute> ins = new BasicEList<>();
		for (Parameter p : base_Behavior.getOwnedParameters()) {
			if (p.getDirection() == ParameterDirectionKind.IN_LITERAL) {
				InAttribute ina = UMLUtil.getStereotypeApplication(p, InAttribute.class);
				if (ina != null) {
					ins.add(ina);
				}
			}
		}
		return ins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<OutAttribute> getOuts() {
		EList<OutAttribute> outs = new BasicEList<>();
		for (Parameter p : base_Behavior.getOwnedParameters()) {
			if (p.getDirection() == ParameterDirectionKind.OUT_LITERAL) {
				OutAttribute outa = UMLUtil.getStereotypeApplication(p, OutAttribute.class);
				if (outa != null) {
					outs.add(outa);
				}
			}
		}
		return outs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BehaviorPackage.TASK__TASK:
				return getTask();
			case BehaviorPackage.TASK__SKILLS:
				return getSkills();
			case BehaviorPackage.TASK__BASE_BEHAVIOR:
				if (resolve) return getBase_Behavior();
				return basicGetBase_Behavior();
			case BehaviorPackage.TASK__INS:
				return getIns();
			case BehaviorPackage.TASK__OUTS:
				return getOuts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BehaviorPackage.TASK__BASE_BEHAVIOR:
				setBase_Behavior((Behavior)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BehaviorPackage.TASK__BASE_BEHAVIOR:
				setBase_Behavior((Behavior)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BehaviorPackage.TASK__TASK:
				return !getTask().isEmpty();
			case BehaviorPackage.TASK__SKILLS:
				return !getSkills().isEmpty();
			case BehaviorPackage.TASK__BASE_BEHAVIOR:
				return base_Behavior != null;
			case BehaviorPackage.TASK__INS:
				return !getIns().isEmpty();
			case BehaviorPackage.TASK__OUTS:
				return !getOuts().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TaskImpl
