/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.commpattern;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternFactory
 * @model kind="package"
 * @generated
 */
public interface CommpatternPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "commpattern"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/commpattern/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "robotics.commpattern"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CommpatternPackage eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternImpl <em>Communication Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl#getCommunicationPattern()
	 * @generated
	 */
	int COMMUNICATION_PATTERN = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternDefinitionImpl <em>Communication Pattern Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternDefinitionImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl#getCommunicationPatternDefinition()
	 * @generated
	 */
	int COMMUNICATION_PATTERN_DEFINITION = 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Collaboration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN__BASE_COLLABORATION = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Communication Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Communication Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION__BASE_CLASS = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Communication Pattern Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Communication Pattern Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PATTERN_DEFINITION_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern <em>Communication Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Pattern</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern
	 * @generated
	 */
	EClass getCommunicationPattern();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern#getBase_Collaboration <em>Base Collaboration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Collaboration</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern#getBase_Collaboration()
	 * @see #getCommunicationPattern()
	 * @generated
	 */
	EReference getCommunicationPattern_Base_Collaboration();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPatternDefinition <em>Communication Pattern Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Pattern Definition</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPatternDefinition
	 * @generated
	 */
	EClass getCommunicationPatternDefinition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPatternDefinition#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPatternDefinition#getBase_Class()
	 * @see #getCommunicationPatternDefinition()
	 * @generated
	 */
	EReference getCommunicationPatternDefinition_Base_Class();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CommpatternFactory getCommpatternFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternImpl <em>Communication Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl#getCommunicationPattern()
		 * @generated
		 */
		EClass COMMUNICATION_PATTERN = eINSTANCE.getCommunicationPattern();

		/**
		 * The meta object literal for the '<em><b>Base Collaboration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_PATTERN__BASE_COLLABORATION = eINSTANCE.getCommunicationPattern_Base_Collaboration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternDefinitionImpl <em>Communication Pattern Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommunicationPatternDefinitionImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl#getCommunicationPatternDefinition()
		 * @generated
		 */
		EClass COMMUNICATION_PATTERN_DEFINITION = eINSTANCE.getCommunicationPatternDefinition();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_PATTERN_DEFINITION__BASE_CLASS = eINSTANCE.getCommunicationPatternDefinition_Base_Class();

	}

} //CommpatternPackage
