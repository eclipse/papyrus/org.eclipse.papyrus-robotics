/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.papyrus.robotics.profile.robotics.services.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ServicesFactoryImpl extends EFactoryImpl implements ServicesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ServicesFactory init() {
		try {
			ServicesFactory theServicesFactory = (ServicesFactory)EPackage.Registry.INSTANCE.getEFactory(ServicesPackage.eNS_URI);
			if (theServicesFactory != null) {
				return theServicesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ServicesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServicesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ServicesPackage.SERVICE_PROPERTY: return createServiceProperty();
			case ServicesPackage.SERVICE_LINK: return createServiceLink();
			case ServicesPackage.SERVICE_WISH: return createServiceWish();
			case ServicesPackage.SERVICE_WISH_PROPERTY: return createServiceWishProperty();
			case ServicesPackage.SYSTEM_SERVICE_ARCHITECTURE_MODEL: return createSystemServiceArchitectureModel();
			case ServicesPackage.SERVICE_FULFILLMENT: return createServiceFulfillment();
			case ServicesPackage.SERVICE_DEFINITION_MODEL: return createServiceDefinitionModel();
			case ServicesPackage.COORDINATION_SERVICE: return createCoordinationService();
			case ServicesPackage.COORDINATION_EVENT: return createCoordinationEvent();
			case ServicesPackage.SERVICE_DEFINITION: return createServiceDefinition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceProperty createServiceProperty() {
		ServicePropertyImpl serviceProperty = new ServicePropertyImpl();
		return serviceProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceLink createServiceLink() {
		ServiceLinkImpl serviceLink = new ServiceLinkImpl();
		return serviceLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceWish createServiceWish() {
		ServiceWishImpl serviceWish = new ServiceWishImpl();
		return serviceWish;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceWishProperty createServiceWishProperty() {
		ServiceWishPropertyImpl serviceWishProperty = new ServiceWishPropertyImpl();
		return serviceWishProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SystemServiceArchitectureModel createSystemServiceArchitectureModel() {
		SystemServiceArchitectureModelImpl systemServiceArchitectureModel = new SystemServiceArchitectureModelImpl();
		return systemServiceArchitectureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceFulfillment createServiceFulfillment() {
		ServiceFulfillmentImpl serviceFulfillment = new ServiceFulfillmentImpl();
		return serviceFulfillment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceDefinitionModel createServiceDefinitionModel() {
		ServiceDefinitionModelImpl serviceDefinitionModel = new ServiceDefinitionModelImpl();
		return serviceDefinitionModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CoordinationService createCoordinationService() {
		CoordinationServiceImpl coordinationService = new CoordinationServiceImpl();
		return coordinationService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CoordinationEvent createCoordinationEvent() {
		CoordinationEventImpl coordinationEvent = new CoordinationEventImpl();
		return coordinationEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceDefinition createServiceDefinition() {
		ServiceDefinitionImpl serviceDefinition = new ServiceDefinitionImpl();
		return serviceDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServicesPackage getServicesPackage() {
		return (ServicesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ServicesPackage getPackage() {
		return ServicesPackage.eINSTANCE;
	}

} //ServicesFactoryImpl
