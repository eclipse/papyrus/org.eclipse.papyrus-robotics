/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage
 * @generated
 */
public interface SkillsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SkillsFactory eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Skill Definition Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Definition Set</em>'.
	 * @generated
	 */
	SkillDefinitionSet createSkillDefinitionSet();

	/**
	 * Returns a new object of class '<em>Skill Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Definition</em>'.
	 * @generated
	 */
	SkillDefinition createSkillDefinition();

	/**
	 * Returns a new object of class '<em>In Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>In Attribute</em>'.
	 * @generated
	 */
	InAttribute createInAttribute();

	/**
	 * Returns a new object of class '<em>Out Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Out Attribute</em>'.
	 * @generated
	 */
	OutAttribute createOutAttribute();

	/**
	 * Returns a new object of class '<em>Skill Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Result</em>'.
	 * @generated
	 */
	SkillResult createSkillResult();

	/**
	 * Returns a new object of class '<em>Skill Semantic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Semantic</em>'.
	 * @generated
	 */
	SkillSemantic createSkillSemantic();

	/**
	 * Returns a new object of class '<em>Skill Success State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Success State</em>'.
	 * @generated
	 */
	SkillSuccessState createSkillSuccessState();

	/**
	 * Returns a new object of class '<em>Skill Fail State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Fail State</em>'.
	 * @generated
	 */
	SkillFailState createSkillFailState();

	/**
	 * Returns a new object of class '<em>Skill Operational State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Operational State</em>'.
	 * @generated
	 */
	SkillOperationalState createSkillOperationalState();

	/**
	 * Returns a new object of class '<em>Transition Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition Edge</em>'.
	 * @generated
	 */
	TransitionEdge createTransitionEdge();

	/**
	 * Returns a new object of class '<em>Skill Initial State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill Initial State</em>'.
	 * @generated
	 */
	SkillInitialState createSkillInitialState();

	/**
	 * Returns a new object of class '<em>Skill FSM Region</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Skill FSM Region</em>'.
	 * @generated
	 */
	SkillFSMRegion createSkillFSMRegion();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SkillsPackage getSkillsPackage();

} //SkillsFactory
