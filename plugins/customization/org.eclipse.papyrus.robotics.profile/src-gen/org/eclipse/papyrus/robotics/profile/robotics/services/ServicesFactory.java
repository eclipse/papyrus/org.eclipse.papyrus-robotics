/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage
 * @generated
 */
public interface ServicesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ServicesFactory eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Service Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Property</em>'.
	 * @generated
	 */
	ServiceProperty createServiceProperty();

	/**
	 * Returns a new object of class '<em>Service Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Link</em>'.
	 * @generated
	 */
	ServiceLink createServiceLink();

	/**
	 * Returns a new object of class '<em>Service Wish</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Wish</em>'.
	 * @generated
	 */
	ServiceWish createServiceWish();

	/**
	 * Returns a new object of class '<em>Service Wish Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Wish Property</em>'.
	 * @generated
	 */
	ServiceWishProperty createServiceWishProperty();

	/**
	 * Returns a new object of class '<em>System Service Architecture Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Service Architecture Model</em>'.
	 * @generated
	 */
	SystemServiceArchitectureModel createSystemServiceArchitectureModel();

	/**
	 * Returns a new object of class '<em>Service Fulfillment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Fulfillment</em>'.
	 * @generated
	 */
	ServiceFulfillment createServiceFulfillment();

	/**
	 * Returns a new object of class '<em>Service Definition Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Definition Model</em>'.
	 * @generated
	 */
	ServiceDefinitionModel createServiceDefinitionModel();

	/**
	 * Returns a new object of class '<em>Coordination Service</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Coordination Service</em>'.
	 * @generated
	 */
	CoordinationService createCoordinationService();

	/**
	 * Returns a new object of class '<em>Coordination Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Coordination Event</em>'.
	 * @generated
	 */
	CoordinationEvent createCoordinationEvent();

	/**
	 * Returns a new object of class '<em>Service Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Definition</em>'.
	 * @generated
	 */
	ServiceDefinition createServiceDefinition();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ServicesPackage getServicesPackage();

} //ServicesFactory
