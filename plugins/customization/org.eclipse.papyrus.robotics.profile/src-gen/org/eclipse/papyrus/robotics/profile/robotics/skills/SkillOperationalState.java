/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills;

import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill Operational State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState#getCompInterface <em>Comp Interface</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillOperationalState()
 * @model
 * @generated
 */
public interface SkillOperationalState extends SkillState {
	/**
	 * Returns the value of the '<em><b>Comp Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comp Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comp Interface</em>' reference.
	 * @see #setCompInterface(CoordinationService)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillOperationalState_CompInterface()
	 * @model ordered="false"
	 * @generated
	 */
	CoordinationService getCompInterface();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState#getCompInterface <em>Comp Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comp Interface</em>' reference.
	 * @see #getCompInterface()
	 * @generated
	 */
	void setCompInterface(CoordinationService value);

} // SkillOperationalState
