/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.deployment;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.DeploymentFactory
 * @model kind="package"
 * @generated
 */
public interface DeploymentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "deployment"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/deployment/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "robotics.deployment"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DeploymentPackage eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.BehaviorArtefactImpl <em>Behavior Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.BehaviorArtefactImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl#getBehaviorArtefact()
	 * @generated
	 */
	int BEHAVIOR_ARTEFACT = 0;

	/**
	 * The feature id for the '<em><b>Base Artifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_ARTEFACT__BASE_ARTIFACT = 0;

	/**
	 * The number of structural features of the '<em>Behavior Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_ARTEFACT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Behavior Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_ARTEFACT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.ComponentArtefactImpl <em>Component Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.ComponentArtefactImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl#getComponentArtefact()
	 * @generated
	 */
	int COMPONENT_ARTEFACT = 1;

	/**
	 * The feature id for the '<em><b>Base Artifact</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARTEFACT__BASE_ARTIFACT = 0;

	/**
	 * The number of structural features of the '<em>Component Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARTEFACT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Component Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_ARTEFACT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.BehaviorArtefact <em>Behavior Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behavior Artefact</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.BehaviorArtefact
	 * @generated
	 */
	EClass getBehaviorArtefact();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.BehaviorArtefact#getBase_Artifact <em>Base Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Artifact</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.BehaviorArtefact#getBase_Artifact()
	 * @see #getBehaviorArtefact()
	 * @generated
	 */
	EReference getBehaviorArtefact_Base_Artifact();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.ComponentArtefact <em>Component Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Artefact</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.ComponentArtefact
	 * @generated
	 */
	EClass getComponentArtefact();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.ComponentArtefact#getBase_Artifact <em>Base Artifact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Artifact</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.ComponentArtefact#getBase_Artifact()
	 * @see #getComponentArtefact()
	 * @generated
	 */
	EReference getComponentArtefact_Base_Artifact();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DeploymentFactory getDeploymentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.BehaviorArtefactImpl <em>Behavior Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.BehaviorArtefactImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl#getBehaviorArtefact()
		 * @generated
		 */
		EClass BEHAVIOR_ARTEFACT = eINSTANCE.getBehaviorArtefact();

		/**
		 * The meta object literal for the '<em><b>Base Artifact</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_ARTEFACT__BASE_ARTIFACT = eINSTANCE.getBehaviorArtefact_Base_Artifact();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.ComponentArtefactImpl <em>Component Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.ComponentArtefactImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl#getComponentArtefact()
		 * @generated
		 */
		EClass COMPONENT_ARTEFACT = eINSTANCE.getComponentArtefact();

		/**
		 * The meta object literal for the '<em><b>Base Artifact</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_ARTEFACT__BASE_ARTIFACT = eINSTANCE.getComponentArtefact_Base_Artifact();

	}

} //DeploymentPackage
