/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent;
import org.eclipse.uml2.uml.StateMachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Skill Semantic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getSuccess <em>Success</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getFail <em>Fail</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getOperational <em>Operational</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getSuccEvts <em>Succ Evts</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getFailEvts <em>Fail Evts</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getBase_StateMachine <em>Base State Machine</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic()
 * @model
 * @generated
 */
public interface SkillSemantic extends Block {
	/**
	 * Returns the value of the '<em><b>Success</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Success</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Success</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic_Success()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SkillSuccessState getSuccess();

	/**
	 * Returns the value of the '<em><b>Fail</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fail</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fail</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic_Fail()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SkillFailState getFail();

	/**
	 * Returns the value of the '<em><b>Operational</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operational</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operational</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic_Operational()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<SkillOperationalState> getOperational();

	/**
	 * Returns the value of the '<em><b>Succ Evts</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succ Evts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succ Evts</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic_SuccEvts()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<CoordinationEvent> getSuccEvts();

	/**
	 * Returns the value of the '<em><b>Fail Evts</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fail Evts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fail Evts</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic_FailEvts()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<CoordinationEvent> getFailEvts();

	/**
	 * Returns the value of the '<em><b>Base State Machine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base State Machine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base State Machine</em>' reference.
	 * @see #setBase_StateMachine(StateMachine)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage#getSkillSemantic_Base_StateMachine()
	 * @model ordered="false"
	 * @generated
	 */
	StateMachine getBase_StateMachine();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic#getBase_StateMachine <em>Base State Machine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base State Machine</em>' reference.
	 * @see #getBase_StateMachine()
	 * @generated
	 */
	void setBase_StateMachine(StateMachine value);

} // SkillSemantic
