/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Usage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Association <em>Base Association</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getSrcWish <em>Src Wish</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getTgtWish <em>Tgt Wish</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Usage <em>Base Usage</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceLink()
 * @model
 * @generated
 */
public interface ServiceLink extends Relation {
	/**
	 * Returns the value of the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Association</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Association</em>' reference.
	 * @see #setBase_Association(Association)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceLink_Base_Association()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Association getBase_Association();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Association <em>Base Association</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Association</em>' reference.
	 * @see #getBase_Association()
	 * @generated
	 */
	void setBase_Association(Association value);

	/**
	 * Returns the value of the '<em><b>Src Wish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src Wish</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Wish</em>' reference.
	 * @see #setSrcWish(ServiceWish)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceLink_SrcWish()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ServiceWish getSrcWish();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getSrcWish <em>Src Wish</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src Wish</em>' reference.
	 * @see #getSrcWish()
	 * @generated
	 */
	void setSrcWish(ServiceWish value);

	/**
	 * Returns the value of the '<em><b>Tgt Wish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgt Wish</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgt Wish</em>' reference.
	 * @see #setTgtWish(ServiceWish)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceLink_TgtWish()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ServiceWish getTgtWish();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getTgtWish <em>Tgt Wish</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tgt Wish</em>' reference.
	 * @see #getTgtWish()
	 * @generated
	 */
	void setTgtWish(ServiceWish value);

	/**
	 * Returns the value of the '<em><b>Base Usage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Usage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Usage</em>' reference.
	 * @see #setBase_Usage(Usage)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getServiceLink_Base_Usage()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Usage getBase_Usage();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Usage <em>Base Usage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Usage</em>' reference.
	 * @see #getBase_Usage()
	 * @generated
	 */
	void setBase_Usage(Usage value);

} // ServiceLink
