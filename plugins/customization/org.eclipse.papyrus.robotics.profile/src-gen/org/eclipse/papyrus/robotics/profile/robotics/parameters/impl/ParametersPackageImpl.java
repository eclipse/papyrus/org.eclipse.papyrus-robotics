/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.parameters.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.impl.BehaviorPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommobjectPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.impl.CommobjectPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommpatternPackage;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.impl.CommpatternPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.DeploymentPackage;
import org.eclipse.papyrus.robotics.profile.robotics.deployment.impl.DeploymentPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.generics.GenericsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.generics.impl.GenericsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.impl.roboticsPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterInstance;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterSlot;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersFactory;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillsPackageImpl;
import org.eclipse.uml2.types.TypesPackage;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ParametersPackageImpl extends EPackageImpl implements ParametersPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterSlotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.profile.robotics.parameters.ParametersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ParametersPackageImpl() {
		super(eNS_URI, ParametersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ParametersPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ParametersPackage init() {
		if (isInited) return (ParametersPackage)EPackage.Registry.INSTANCE.getEPackage(ParametersPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredParametersPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ParametersPackageImpl theParametersPackage = registeredParametersPackage instanceof ParametersPackageImpl ? (ParametersPackageImpl)registeredParametersPackage : new ParametersPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BPCPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(roboticsPackage.eNS_URI);
		roboticsPackageImpl theroboticsPackage = (roboticsPackageImpl)(registeredPackage instanceof roboticsPackageImpl ? registeredPackage : roboticsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ComponentsPackage.eNS_URI);
		ComponentsPackageImpl theComponentsPackage = (ComponentsPackageImpl)(registeredPackage instanceof ComponentsPackageImpl ? registeredPackage : ComponentsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FunctionsPackage.eNS_URI);
		FunctionsPackageImpl theFunctionsPackage = (FunctionsPackageImpl)(registeredPackage instanceof FunctionsPackageImpl ? registeredPackage : FunctionsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ServicesPackage.eNS_URI);
		ServicesPackageImpl theServicesPackage = (ServicesPackageImpl)(registeredPackage instanceof ServicesPackageImpl ? registeredPackage : ServicesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		BehaviorPackageImpl theBehaviorPackage = (BehaviorPackageImpl)(registeredPackage instanceof BehaviorPackageImpl ? registeredPackage : BehaviorPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);
		SkillsPackageImpl theSkillsPackage = (SkillsPackageImpl)(registeredPackage instanceof SkillsPackageImpl ? registeredPackage : SkillsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommpatternPackage.eNS_URI);
		CommpatternPackageImpl theCommpatternPackage = (CommpatternPackageImpl)(registeredPackage instanceof CommpatternPackageImpl ? registeredPackage : CommpatternPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(registeredPackage instanceof DeploymentPackageImpl ? registeredPackage : DeploymentPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CommobjectPackage.eNS_URI);
		CommobjectPackageImpl theCommobjectPackage = (CommobjectPackageImpl)(registeredPackage instanceof CommobjectPackageImpl ? registeredPackage : CommobjectPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GenericsPackage.eNS_URI);
		GenericsPackageImpl theGenericsPackage = (GenericsPackageImpl)(registeredPackage instanceof GenericsPackageImpl ? registeredPackage : GenericsPackage.eINSTANCE);

		// Create package meta-data objects
		theParametersPackage.createPackageContents();
		theroboticsPackage.createPackageContents();
		theComponentsPackage.createPackageContents();
		theFunctionsPackage.createPackageContents();
		theServicesPackage.createPackageContents();
		theBehaviorPackage.createPackageContents();
		theSkillsPackage.createPackageContents();
		theCommpatternPackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theCommobjectPackage.createPackageContents();
		theGenericsPackage.createPackageContents();

		// Initialize created meta-data
		theParametersPackage.initializePackageContents();
		theroboticsPackage.initializePackageContents();
		theComponentsPackage.initializePackageContents();
		theFunctionsPackage.initializePackageContents();
		theServicesPackage.initializePackageContents();
		theBehaviorPackage.initializePackageContents();
		theSkillsPackage.initializePackageContents();
		theCommpatternPackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theCommobjectPackage.initializePackageContents();
		theGenericsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theParametersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ParametersPackage.eNS_URI, theParametersPackage);
		return theParametersPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParameterEntry() {
		return parameterEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameterEntry_Base_Property() {
		return (EReference)parameterEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParameterInstance() {
		return parameterInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameterInstance_Slots() {
		return (EReference)parameterInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameterInstance_Base_InstanceSpecification() {
		return (EReference)parameterInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParameterSlot() {
		return parameterSlotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameterSlot_Base_Slot() {
		return (EReference)parameterSlotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameter_Entries() {
		return (EReference)parameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameter_Base_Class() {
		return (EReference)parameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ParametersFactory getParametersFactory() {
		return (ParametersFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		parameterEClass = createEClass(PARAMETER);
		createEReference(parameterEClass, PARAMETER__ENTRIES);
		createEReference(parameterEClass, PARAMETER__BASE_CLASS);

		parameterEntryEClass = createEClass(PARAMETER_ENTRY);
		createEReference(parameterEntryEClass, PARAMETER_ENTRY__BASE_PROPERTY);

		parameterSlotEClass = createEClass(PARAMETER_SLOT);
		createEReference(parameterSlotEClass, PARAMETER_SLOT__BASE_SLOT);

		parameterInstanceEClass = createEClass(PARAMETER_INSTANCE);
		createEReference(parameterInstanceEClass, PARAMETER_INSTANCE__SLOTS);
		createEReference(parameterInstanceEClass, PARAMETER_INSTANCE__BASE_INSTANCE_SPECIFICATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BPCPackage theBPCPackage = (BPCPackage)EPackage.Registry.INSTANCE.getEPackage(BPCPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		parameterEClass.getESuperTypes().add(theBPCPackage.getEntity());
		parameterEntryEClass.getESuperTypes().add(theBPCPackage.getProperty());
		parameterSlotEClass.getESuperTypes().add(theBPCPackage.getProperty());
		parameterInstanceEClass.getESuperTypes().add(theBPCPackage.getEntity());

		// Initialize classes, features, and operations; add parameters
		initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getParameter_Entries(), this.getParameterEntry(), null, "entries", null, 0, -1, Parameter.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getParameter_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(parameterEntryEClass, ParameterEntry.class, "ParameterEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getParameterEntry_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, ParameterEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(parameterSlotEClass, ParameterSlot.class, "ParameterSlot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getParameterSlot_Base_Slot(), theUMLPackage.getSlot(), null, "base_Slot", null, 1, 1, ParameterSlot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(parameterInstanceEClass, ParameterInstance.class, "ParameterInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getParameterInstance_Slots(), this.getParameterSlot(), null, "slots", null, 0, -1, ParameterInstance.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getParameterInstance_Base_InstanceSpecification(), theUMLPackage.getInstanceSpecification(), null, "base_InstanceSpecification", null, 1, 1, ParameterInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
	}

} //ParametersPackageImpl
