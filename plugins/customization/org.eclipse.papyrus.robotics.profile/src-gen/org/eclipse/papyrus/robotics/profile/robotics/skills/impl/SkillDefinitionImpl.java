/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.robotics.profile.skills.SkillDefinitionOperations;
import org.eclipse.uml2.uml.Operation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Skill Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl#getIns <em>Ins</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl#getOuts <em>Outs</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl#getRes <em>Res</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl#getDefaultSemantic <em>Default Semantic</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.skills.impl.SkillDefinitionImpl#getBase_Operation <em>Base Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SkillDefinitionImpl extends BlockImpl implements SkillDefinition {
	/**
	 * The cached value of the '{@link #getBase_Operation() <em>Base Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Operation()
	 * @generated
	 * @ordered
	 */
	protected Operation base_Operation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SkillDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SkillsPackage.Literals.SKILL_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public EList<InAttribute> getIns() {
		return SkillDefinitionOperations.getIns(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	@Override
	public EList<OutAttribute> getOuts() {
		return SkillDefinitionOperations.getOuts(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<SkillResult> getRes() {
		return SkillDefinitionOperations.getRes(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillSemantic getDefaultSemantic() {
		SkillSemantic defaultSemantic = basicGetDefaultSemantic();
		return defaultSemantic != null && defaultSemantic.eIsProxy() ? (SkillSemantic)eResolveProxy((InternalEObject)defaultSemantic) : defaultSemantic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SkillSemantic basicGetDefaultSemantic() {
		return SkillDefinitionOperations.getDefaultSemantic(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Operation getBase_Operation() {
		if (base_Operation != null && base_Operation.eIsProxy()) {
			InternalEObject oldBase_Operation = (InternalEObject)base_Operation;
			base_Operation = (Operation)eResolveProxy(oldBase_Operation);
			if (base_Operation != oldBase_Operation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SkillsPackage.SKILL_DEFINITION__BASE_OPERATION, oldBase_Operation, base_Operation));
			}
		}
		return base_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetBase_Operation() {
		return base_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Operation(Operation newBase_Operation) {
		Operation oldBase_Operation = base_Operation;
		base_Operation = newBase_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SkillsPackage.SKILL_DEFINITION__BASE_OPERATION, oldBase_Operation, base_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SkillsPackage.SKILL_DEFINITION__INS:
				return getIns();
			case SkillsPackage.SKILL_DEFINITION__OUTS:
				return getOuts();
			case SkillsPackage.SKILL_DEFINITION__RES:
				return getRes();
			case SkillsPackage.SKILL_DEFINITION__DEFAULT_SEMANTIC:
				if (resolve) return getDefaultSemantic();
				return basicGetDefaultSemantic();
			case SkillsPackage.SKILL_DEFINITION__BASE_OPERATION:
				if (resolve) return getBase_Operation();
				return basicGetBase_Operation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SkillsPackage.SKILL_DEFINITION__BASE_OPERATION:
				setBase_Operation((Operation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_DEFINITION__BASE_OPERATION:
				setBase_Operation((Operation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SkillsPackage.SKILL_DEFINITION__INS:
				return !getIns().isEmpty();
			case SkillsPackage.SKILL_DEFINITION__OUTS:
				return !getOuts().isEmpty();
			case SkillsPackage.SKILL_DEFINITION__RES:
				return !getRes().isEmpty();
			case SkillsPackage.SKILL_DEFINITION__DEFAULT_SEMANTIC:
				return basicGetDefaultSemantic() != null;
			case SkillsPackage.SKILL_DEFINITION__BASE_OPERATION:
				return base_Operation != null;
		}
		return super.eIsSet(featureID);
	}

} // SkillDefinitionImpl
