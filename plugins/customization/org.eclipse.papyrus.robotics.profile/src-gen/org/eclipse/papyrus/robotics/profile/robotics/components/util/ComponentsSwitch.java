/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.components.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityConfiguration;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinitionModel;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.components.PeriodicTimer;
import org.eclipse.papyrus.robotics.profile.robotics.components.ServiceConfiguration;
import org.eclipse.papyrus.robotics.profile.robotics.components.SystemComponentArchitectureModel;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage
 * @generated
 */
public class ComponentsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ComponentsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentsSwitch() {
		if (modelPackage == null) {
			modelPackage = ComponentsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ComponentsPackage.ACTIVITY: {
				Activity activity = (Activity)theEObject;
				T result = caseActivity(activity);
				if (result == null) result = caseBlock(activity);
				if (result == null) result = caseEntity(activity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.ACTIVITY_PORT: {
				ActivityPort activityPort = (ActivityPort)theEObject;
				T result = caseActivityPort(activityPort);
				if (result == null) result = casePort(activityPort);
				if (result == null) result = caseEntity(activityPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.COMPONENT_DEFINITION: {
				ComponentDefinition componentDefinition = (ComponentDefinition)theEObject;
				T result = caseComponentDefinition(componentDefinition);
				if (result == null) result = caseComponentOrSystem(componentDefinition);
				if (result == null) result = caseBlock(componentDefinition);
				if (result == null) result = caseEntity(componentDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.COMPONENT_OR_SYSTEM: {
				ComponentOrSystem componentOrSystem = (ComponentOrSystem)theEObject;
				T result = caseComponentOrSystem(componentOrSystem);
				if (result == null) result = caseBlock(componentOrSystem);
				if (result == null) result = caseEntity(componentOrSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.COMPONENT_PORT: {
				ComponentPort componentPort = (ComponentPort)theEObject;
				T result = caseComponentPort(componentPort);
				if (result == null) result = casePort(componentPort);
				if (result == null) result = caseEntity(componentPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.COMPONENT_INSTANCE: {
				ComponentInstance componentInstance = (ComponentInstance)theEObject;
				T result = caseComponentInstance(componentInstance);
				if (result == null) result = caseEntity(componentInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.COMPONENT_SERVICE: {
				ComponentService componentService = (ComponentService)theEObject;
				T result = caseComponentService(componentService);
				if (result == null) result = caseEntity(componentService);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.SERVICE_CONFIGURATION: {
				ServiceConfiguration serviceConfiguration = (ServiceConfiguration)theEObject;
				T result = caseServiceConfiguration(serviceConfiguration);
				if (result == null) result = caseEntity(serviceConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.SYSTEM: {
				org.eclipse.papyrus.robotics.profile.robotics.components.System system = (org.eclipse.papyrus.robotics.profile.robotics.components.System)theEObject;
				T result = caseSystem(system);
				if (result == null) result = caseComponentOrSystem(system);
				if (result == null) result = caseBlock(system);
				if (result == null) result = caseEntity(system);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.ACTIVITY_INSTANCE: {
				ActivityInstance activityInstance = (ActivityInstance)theEObject;
				T result = caseActivityInstance(activityInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.ACTIVITY_CONFIGURATION: {
				ActivityConfiguration activityConfiguration = (ActivityConfiguration)theEObject;
				T result = caseActivityConfiguration(activityConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.COMPONENT_DEFINITION_MODEL: {
				ComponentDefinitionModel componentDefinitionModel = (ComponentDefinitionModel)theEObject;
				T result = caseComponentDefinitionModel(componentDefinitionModel);
				if (result == null) result = caseEntity(componentDefinitionModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.SYSTEM_COMPONENT_ARCHITECTURE_MODEL: {
				SystemComponentArchitectureModel systemComponentArchitectureModel = (SystemComponentArchitectureModel)theEObject;
				T result = caseSystemComponentArchitectureModel(systemComponentArchitectureModel);
				if (result == null) result = caseEntity(systemComponentArchitectureModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ComponentsPackage.PERIODIC_TIMER: {
				PeriodicTimer periodicTimer = (PeriodicTimer)theEObject;
				T result = casePeriodicTimer(periodicTimer);
				if (result == null) result = caseBlock(periodicTimer);
				if (result == null) result = caseEntity(periodicTimer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivity(Activity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivityPort(ActivityPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentDefinition(ComponentDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Or System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Or System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentOrSystem(ComponentOrSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentPort(ComponentPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstance(ComponentInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentService(ComponentService object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceConfiguration(ServiceConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystem(org.eclipse.papyrus.robotics.profile.robotics.components.System object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivityInstance(ActivityInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivityConfiguration(ActivityConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Definition Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Definition Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentDefinitionModel(ComponentDefinitionModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Component Architecture Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Component Architecture Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemComponentArchitectureModel(SystemComponentArchitectureModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Periodic Timer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Periodic Timer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePeriodicTimer(PeriodicTimer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ComponentsSwitch
