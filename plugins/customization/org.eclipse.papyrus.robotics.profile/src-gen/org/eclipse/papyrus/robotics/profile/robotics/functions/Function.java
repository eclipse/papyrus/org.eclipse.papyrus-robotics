/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.functions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.Function#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.Function#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.Function#isCodeInModel <em>Code In Model</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.Function#getActivityPort <em>Activity Port</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#getFunction()
 * @model
 * @generated
 */
public interface Function extends Block {
	/**
	 * Returns the value of the '<em><b>Arguments</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.profile.robotics.functions.Argument}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Arguments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Arguments</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#getFunction_Arguments()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Argument> getArguments();

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The default value is <code>"HANDLER"</code>.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind
	 * @see #setKind(FunctionKind)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#getFunction_Kind()
	 * @model default="HANDLER" required="true" ordered="false"
	 * @generated
	 */
	FunctionKind getKind();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.functions.Function#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(FunctionKind value);

	/**
	 * Returns the value of the '<em><b>Code In Model</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code In Model</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code In Model</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#getFunction_CodeInModel()
	 * @model default="false" dataType="org.eclipse.uml2.types.Boolean" required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	boolean isCodeInModel();

	/**
	 * Returns the value of the '<em><b>Activity Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Port</em>' reference.
	 * @see #setActivityPort(ActivityPort)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage#getFunction_ActivityPort()
	 * @model ordered="false"
	 * @generated
	 */
	ActivityPort getActivityPort();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.functions.Function#getActivityPort <em>Activity Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity Port</em>' reference.
	 * @see #getActivityPort()
	 * @generated
	 */
	void setActivityPort(ActivityPort value);

} // Function
