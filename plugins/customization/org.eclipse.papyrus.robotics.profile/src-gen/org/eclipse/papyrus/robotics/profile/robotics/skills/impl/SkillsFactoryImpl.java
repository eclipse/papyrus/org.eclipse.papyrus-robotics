/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.papyrus.robotics.profile.robotics.skills.*;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsFactory;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SkillsFactoryImpl extends EFactoryImpl implements SkillsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SkillsFactory init() {
		try {
			SkillsFactory theSkillsFactory = (SkillsFactory)EPackage.Registry.INSTANCE.getEFactory(SkillsPackage.eNS_URI);
			if (theSkillsFactory != null) {
				return theSkillsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SkillsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SkillsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SkillsPackage.SKILL_DEFINITION_SET: return createSkillDefinitionSet();
			case SkillsPackage.IN_ATTRIBUTE: return createInAttribute();
			case SkillsPackage.OUT_ATTRIBUTE: return createOutAttribute();
			case SkillsPackage.SKILL_RESULT: return createSkillResult();
			case SkillsPackage.SKILL_SEMANTIC: return createSkillSemantic();
			case SkillsPackage.SKILL_SUCCESS_STATE: return createSkillSuccessState();
			case SkillsPackage.SKILL_FAIL_STATE: return createSkillFailState();
			case SkillsPackage.SKILL_OPERATIONAL_STATE: return createSkillOperationalState();
			case SkillsPackage.TRANSITION_EDGE: return createTransitionEdge();
			case SkillsPackage.SKILL_INITIAL_STATE: return createSkillInitialState();
			case SkillsPackage.SKILL_FSM_REGION: return createSkillFSMRegion();
			case SkillsPackage.SKILL_DEFINITION: return createSkillDefinition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case SkillsPackage.SKILL_RESULT_KIND:
				return createSkillResultKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case SkillsPackage.SKILL_RESULT_KIND:
				return convertSkillResultKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillDefinitionSet createSkillDefinitionSet() {
		SkillDefinitionSetImpl skillDefinitionSet = new SkillDefinitionSetImpl();
		return skillDefinitionSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillDefinition createSkillDefinition() {
		SkillDefinitionImpl skillDefinition = new SkillDefinitionImpl();
		return skillDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InAttribute createInAttribute() {
		InAttributeImpl inAttribute = new InAttributeImpl();
		return inAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OutAttribute createOutAttribute() {
		OutAttributeImpl outAttribute = new OutAttributeImpl();
		return outAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillResult createSkillResult() {
		SkillResultImpl skillResult = new SkillResultImpl();
		return skillResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillSemantic createSkillSemantic() {
		SkillSemanticImpl skillSemantic = new SkillSemanticImpl();
		return skillSemantic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillSuccessState createSkillSuccessState() {
		SkillSuccessStateImpl skillSuccessState = new SkillSuccessStateImpl();
		return skillSuccessState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillFailState createSkillFailState() {
		SkillFailStateImpl skillFailState = new SkillFailStateImpl();
		return skillFailState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillOperationalState createSkillOperationalState() {
		SkillOperationalStateImpl skillOperationalState = new SkillOperationalStateImpl();
		return skillOperationalState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TransitionEdge createTransitionEdge() {
		TransitionEdgeImpl transitionEdge = new TransitionEdgeImpl();
		return transitionEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillInitialState createSkillInitialState() {
		SkillInitialStateImpl skillInitialState = new SkillInitialStateImpl();
		return skillInitialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillFSMRegion createSkillFSMRegion() {
		SkillFSMRegionImpl skillFSMRegion = new SkillFSMRegionImpl();
		return skillFSMRegion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SkillResultKind createSkillResultKindFromString(EDataType eDataType, String initialValue) {
		SkillResultKind result = SkillResultKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSkillResultKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillsPackage getSkillsPackage() {
		return (SkillsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SkillsPackage getPackage() {
		return SkillsPackage.eINSTANCE;
	}

} //SkillsFactoryImpl
