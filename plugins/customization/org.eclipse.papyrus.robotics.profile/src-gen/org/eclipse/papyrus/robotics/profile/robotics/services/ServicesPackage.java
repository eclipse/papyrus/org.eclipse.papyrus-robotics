/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesFactory
 * @model kind="package"
 * @generated
 */
public interface ServicesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "services"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/services/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "robotics.services"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ServicesPackage eINSTANCE = org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicePropertyImpl <em>Service Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicePropertyImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceProperty()
	 * @generated
	 */
	int SERVICE_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__PROPERTY = BPCPackage.PROPERTY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__INSTANCE_UID = BPCPackage.PROPERTY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__DESCRIPTION = BPCPackage.PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__AUTHORSHIP = BPCPackage.PROPERTY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__PROVENANCE = BPCPackage.PROPERTY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__MODEL_UID = BPCPackage.PROPERTY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__METAMODEL_UID = BPCPackage.PROPERTY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY__BASE_PROPERTY = BPCPackage.PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Service Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY_FEATURE_COUNT = BPCPackage.PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Service Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_PROPERTY_OPERATION_COUNT = BPCPackage.PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl <em>Service Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceLink()
	 * @generated
	 */
	int SERVICE_LINK = 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__PROPERTY = BPCPackage.RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__INSTANCE_UID = BPCPackage.RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__DESCRIPTION = BPCPackage.RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__AUTHORSHIP = BPCPackage.RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__PROVENANCE = BPCPackage.RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__MODEL_UID = BPCPackage.RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__METAMODEL_UID = BPCPackage.RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__PORT = BPCPackage.RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__CONNECTOR = BPCPackage.RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__COLLECTION = BPCPackage.RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__BLOCK = BPCPackage.RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__RELATION = BPCPackage.RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__BASE_CLASS = BPCPackage.RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__REIFICATION = BPCPackage.RELATION__REIFICATION;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__BASE_ASSOCIATION = BPCPackage.RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Src Wish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__SRC_WISH = BPCPackage.RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tgt Wish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__TGT_WISH = BPCPackage.RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Base Usage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK__BASE_USAGE = BPCPackage.RELATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Service Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK_FEATURE_COUNT = BPCPackage.RELATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Service Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LINK_OPERATION_COUNT = BPCPackage.RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishImpl <em>Service Wish</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceWish()
	 * @generated
	 */
	int SERVICE_WISH = 2;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Instance Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__BASE_INSTANCE_SPECIFICATION = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH__PROPERTIES = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service Wish</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Service Wish</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishPropertyImpl <em>Service Wish Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishPropertyImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceWishProperty()
	 * @generated
	 */
	int SERVICE_WISH_PROPERTY = 3;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__PROPERTY = BPCPackage.PROPERTY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__INSTANCE_UID = BPCPackage.PROPERTY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__DESCRIPTION = BPCPackage.PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__AUTHORSHIP = BPCPackage.PROPERTY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__PROVENANCE = BPCPackage.PROPERTY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__MODEL_UID = BPCPackage.PROPERTY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__METAMODEL_UID = BPCPackage.PROPERTY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY__BASE_SLOT = BPCPackage.PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Service Wish Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY_FEATURE_COUNT = BPCPackage.PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Service Wish Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_WISH_PROPERTY_OPERATION_COUNT = BPCPackage.PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.SystemServiceArchitectureModelImpl <em>System Service Architecture Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.SystemServiceArchitectureModelImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getSystemServiceArchitectureModel()
	 * @generated
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL = 4;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL__BASE_PACKAGE = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>System Service Architecture Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>System Service Architecture Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_SERVICE_ARCHITECTURE_MODEL_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceFulfillmentImpl <em>Service Fulfillment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceFulfillmentImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceFulfillment()
	 * @generated
	 */
	int SERVICE_FULFILLMENT = 5;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__PROPERTY = BPCPackage.RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__INSTANCE_UID = BPCPackage.RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__DESCRIPTION = BPCPackage.RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__AUTHORSHIP = BPCPackage.RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__PROVENANCE = BPCPackage.RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__MODEL_UID = BPCPackage.RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__METAMODEL_UID = BPCPackage.RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__PORT = BPCPackage.RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__CONNECTOR = BPCPackage.RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__COLLECTION = BPCPackage.RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__BLOCK = BPCPackage.RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__RELATION = BPCPackage.RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__BASE_CLASS = BPCPackage.RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__REIFICATION = BPCPackage.RELATION__REIFICATION;

	/**
	 * The feature id for the '<em><b>Wish</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__WISH = BPCPackage.RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CInstance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__CINSTANCE = BPCPackage.RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CPort</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__CPORT = BPCPackage.RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT__BASE_ASSOCIATION = BPCPackage.RELATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Service Fulfillment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT_FEATURE_COUNT = BPCPackage.RELATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Service Fulfillment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_FULFILLMENT_OPERATION_COUNT = BPCPackage.RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionModelImpl <em>Service Definition Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionModelImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceDefinitionModel()
	 * @generated
	 */
	int SERVICE_DEFINITION_MODEL = 6;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL__BASE_PACKAGE = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Service Definition Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Service Definition Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_MODEL_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationServiceImpl <em>Coordination Service</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationServiceImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getCoordinationService()
	 * @generated
	 */
	int COORDINATION_SERVICE = 7;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Svc Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__SVC_PROPERTY = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE__BASE_INTERFACE = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Coordination Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Coordination Service</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_SERVICE_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationEventImpl <em>Coordination Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationEventImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getCoordinationEvent()
	 * @generated
	 */
	int COORDINATION_EVENT = 8;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__EVENT = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT__BASE_EVENT = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Coordination Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Coordination Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORDINATION_EVENT_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionImpl <em>Service Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionImpl
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceDefinition()
	 * @generated
	 */
	int SERVICE_DEFINITION = 9;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__BASE_INTERFACE = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Svc Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION__SVC_PROPERTY = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Service Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_DEFINITION_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty <em>Service Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Property</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty
	 * @generated
	 */
	EClass getServiceProperty();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty#getBase_Property()
	 * @see #getServiceProperty()
	 * @generated
	 */
	EReference getServiceProperty_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink <em>Service Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Link</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink
	 * @generated
	 */
	EClass getServiceLink();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Association()
	 * @see #getServiceLink()
	 * @generated
	 */
	EReference getServiceLink_Base_Association();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getSrcWish <em>Src Wish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src Wish</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getSrcWish()
	 * @see #getServiceLink()
	 * @generated
	 */
	EReference getServiceLink_SrcWish();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getTgtWish <em>Tgt Wish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgt Wish</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getTgtWish()
	 * @see #getServiceLink()
	 * @generated
	 */
	EReference getServiceLink_TgtWish();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Usage <em>Base Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Usage</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceLink#getBase_Usage()
	 * @see #getServiceLink()
	 * @generated
	 */
	EReference getServiceLink_Base_Usage();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish <em>Service Wish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Wish</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish
	 * @generated
	 */
	EClass getServiceWish();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish#getBase_InstanceSpecification <em>Base Instance Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Instance Specification</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish#getBase_InstanceSpecification()
	 * @see #getServiceWish()
	 * @generated
	 */
	EReference getServiceWish_Base_InstanceSpecification();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Properties</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWish#getProperties()
	 * @see #getServiceWish()
	 * @generated
	 */
	EReference getServiceWish_Properties();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWishProperty <em>Service Wish Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Wish Property</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWishProperty
	 * @generated
	 */
	EClass getServiceWishProperty();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWishProperty#getBase_Slot <em>Base Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Slot</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceWishProperty#getBase_Slot()
	 * @see #getServiceWishProperty()
	 * @generated
	 */
	EReference getServiceWishProperty_Base_Slot();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.SystemServiceArchitectureModel <em>System Service Architecture Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Service Architecture Model</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.SystemServiceArchitectureModel
	 * @generated
	 */
	EClass getSystemServiceArchitectureModel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.SystemServiceArchitectureModel#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.SystemServiceArchitectureModel#getBase_Package()
	 * @see #getSystemServiceArchitectureModel()
	 * @generated
	 */
	EReference getSystemServiceArchitectureModel_Base_Package();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment <em>Service Fulfillment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Fulfillment</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment
	 * @generated
	 */
	EClass getServiceFulfillment();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getWish <em>Wish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Wish</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getWish()
	 * @see #getServiceFulfillment()
	 * @generated
	 */
	EReference getServiceFulfillment_Wish();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getCInstance <em>CInstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CInstance</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getCInstance()
	 * @see #getServiceFulfillment()
	 * @generated
	 */
	EReference getServiceFulfillment_CInstance();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getCPort <em>CPort</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CPort</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getCPort()
	 * @see #getServiceFulfillment()
	 * @generated
	 */
	EReference getServiceFulfillment_CPort();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceFulfillment#getBase_Association()
	 * @see #getServiceFulfillment()
	 * @generated
	 */
	EReference getServiceFulfillment_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinitionModel <em>Service Definition Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Definition Model</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinitionModel
	 * @generated
	 */
	EClass getServiceDefinitionModel();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinitionModel#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinitionModel#getBase_Package()
	 * @see #getServiceDefinitionModel()
	 * @generated
	 */
	EReference getServiceDefinitionModel_Base_Package();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService <em>Coordination Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coordination Service</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService
	 * @generated
	 */
	EClass getCoordinationService();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService#getSvcProperty <em>Svc Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Svc Property</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService#getSvcProperty()
	 * @see #getCoordinationService()
	 * @generated
	 */
	EReference getCoordinationService_SvcProperty();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService#getBase_Interface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Interface</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService#getBase_Interface()
	 * @see #getCoordinationService()
	 * @generated
	 */
	EReference getCoordinationService_Base_Interface();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent <em>Coordination Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coordination Event</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent
	 * @generated
	 */
	EClass getCoordinationEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getEvent()
	 * @see #getCoordinationEvent()
	 * @generated
	 */
	EReference getCoordinationEvent_Event();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getBase_Event <em>Base Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Event</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getBase_Event()
	 * @see #getCoordinationEvent()
	 * @generated
	 */
	EReference getCoordinationEvent_Base_Event();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition <em>Service Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Definition</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition
	 * @generated
	 */
	EClass getServiceDefinition();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition#getBase_Interface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Interface</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition#getBase_Interface()
	 * @see #getServiceDefinition()
	 * @generated
	 */
	EReference getServiceDefinition_Base_Interface();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition#getSvcProperty <em>Svc Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Svc Property</em>'.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition#getSvcProperty()
	 * @see #getServiceDefinition()
	 * @generated
	 */
	EReference getServiceDefinition_SvcProperty();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ServicesFactory getServicesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicePropertyImpl <em>Service Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicePropertyImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceProperty()
		 * @generated
		 */
		EClass SERVICE_PROPERTY = eINSTANCE.getServiceProperty();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_PROPERTY__BASE_PROPERTY = eINSTANCE.getServiceProperty_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl <em>Service Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceLinkImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceLink()
		 * @generated
		 */
		EClass SERVICE_LINK = eINSTANCE.getServiceLink();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_LINK__BASE_ASSOCIATION = eINSTANCE.getServiceLink_Base_Association();

		/**
		 * The meta object literal for the '<em><b>Src Wish</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_LINK__SRC_WISH = eINSTANCE.getServiceLink_SrcWish();

		/**
		 * The meta object literal for the '<em><b>Tgt Wish</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_LINK__TGT_WISH = eINSTANCE.getServiceLink_TgtWish();

		/**
		 * The meta object literal for the '<em><b>Base Usage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_LINK__BASE_USAGE = eINSTANCE.getServiceLink_Base_Usage();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishImpl <em>Service Wish</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceWish()
		 * @generated
		 */
		EClass SERVICE_WISH = eINSTANCE.getServiceWish();

		/**
		 * The meta object literal for the '<em><b>Base Instance Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_WISH__BASE_INSTANCE_SPECIFICATION = eINSTANCE.getServiceWish_Base_InstanceSpecification();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_WISH__PROPERTIES = eINSTANCE.getServiceWish_Properties();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishPropertyImpl <em>Service Wish Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceWishPropertyImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceWishProperty()
		 * @generated
		 */
		EClass SERVICE_WISH_PROPERTY = eINSTANCE.getServiceWishProperty();

		/**
		 * The meta object literal for the '<em><b>Base Slot</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_WISH_PROPERTY__BASE_SLOT = eINSTANCE.getServiceWishProperty_Base_Slot();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.SystemServiceArchitectureModelImpl <em>System Service Architecture Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.SystemServiceArchitectureModelImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getSystemServiceArchitectureModel()
		 * @generated
		 */
		EClass SYSTEM_SERVICE_ARCHITECTURE_MODEL = eINSTANCE.getSystemServiceArchitectureModel();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_SERVICE_ARCHITECTURE_MODEL__BASE_PACKAGE = eINSTANCE.getSystemServiceArchitectureModel_Base_Package();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceFulfillmentImpl <em>Service Fulfillment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceFulfillmentImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceFulfillment()
		 * @generated
		 */
		EClass SERVICE_FULFILLMENT = eINSTANCE.getServiceFulfillment();

		/**
		 * The meta object literal for the '<em><b>Wish</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_FULFILLMENT__WISH = eINSTANCE.getServiceFulfillment_Wish();

		/**
		 * The meta object literal for the '<em><b>CInstance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_FULFILLMENT__CINSTANCE = eINSTANCE.getServiceFulfillment_CInstance();

		/**
		 * The meta object literal for the '<em><b>CPort</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_FULFILLMENT__CPORT = eINSTANCE.getServiceFulfillment_CPort();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_FULFILLMENT__BASE_ASSOCIATION = eINSTANCE.getServiceFulfillment_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionModelImpl <em>Service Definition Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionModelImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceDefinitionModel()
		 * @generated
		 */
		EClass SERVICE_DEFINITION_MODEL = eINSTANCE.getServiceDefinitionModel();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_DEFINITION_MODEL__BASE_PACKAGE = eINSTANCE.getServiceDefinitionModel_Base_Package();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationServiceImpl <em>Coordination Service</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationServiceImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getCoordinationService()
		 * @generated
		 */
		EClass COORDINATION_SERVICE = eINSTANCE.getCoordinationService();

		/**
		 * The meta object literal for the '<em><b>Svc Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATION_SERVICE__SVC_PROPERTY = eINSTANCE.getCoordinationService_SvcProperty();

		/**
		 * The meta object literal for the '<em><b>Base Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATION_SERVICE__BASE_INTERFACE = eINSTANCE.getCoordinationService_Base_Interface();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationEventImpl <em>Coordination Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.CoordinationEventImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getCoordinationEvent()
		 * @generated
		 */
		EClass COORDINATION_EVENT = eINSTANCE.getCoordinationEvent();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATION_EVENT__EVENT = eINSTANCE.getCoordinationEvent_Event();

		/**
		 * The meta object literal for the '<em><b>Base Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORDINATION_EVENT__BASE_EVENT = eINSTANCE.getCoordinationEvent_Base_Event();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionImpl <em>Service Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServiceDefinitionImpl
		 * @see org.eclipse.papyrus.robotics.profile.robotics.services.impl.ServicesPackageImpl#getServiceDefinition()
		 * @generated
		 */
		EClass SERVICE_DEFINITION = eINSTANCE.getServiceDefinition();

		/**
		 * The meta object literal for the '<em><b>Base Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_DEFINITION__BASE_INTERFACE = eINSTANCE.getServiceDefinition_Base_Interface();

		/**
		 * The meta object literal for the '<em><b>Svc Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_DEFINITION__SVC_PROPERTY = eINSTANCE.getServiceDefinition_SvcProperty();

	}

} //ServicesPackage
