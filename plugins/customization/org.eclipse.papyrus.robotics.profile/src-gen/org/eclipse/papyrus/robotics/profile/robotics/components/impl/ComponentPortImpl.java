/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.components.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PortImpl;
import org.eclipse.papyrus.robotics.profile.components.ComponentPortOperations;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.uml2.uml.Port;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentPortImpl#getBase_Port <em>Base Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentPortImpl#getProvides <em>Provides</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentPortImpl#getRequires <em>Requires</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentPortImpl#getQos <em>Qos</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.components.impl.ComponentPortImpl#isCoordinationPort <em>Is Coordination Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentPortImpl extends PortImpl implements ComponentPort {
	/**
	 * The cached value of the '{@link #getBase_Port() <em>Base Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Port()
	 * @generated
	 * @ordered
	 */
	protected Port base_Port;

	/**
	 * The default value of the '{@link #getQos() <em>Qos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQos()
	 * @generated
	 * @ordered
	 */
	protected static final String QOS_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getQos() <em>Qos</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQos()
	 * @generated
	 * @ordered
	 */
	protected String qos = QOS_EDEFAULT;

	/**
	 * The default value of the '{@link #isCoordinationPort() <em>Is Coordination Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCoordinationPort()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_COORDINATION_PORT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCoordinationPort() <em>Is Coordination Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCoordinationPort()
	 * @generated
	 * @ordered
	 */
	protected boolean isCoordinationPort = IS_COORDINATION_PORT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ComponentsPackage.Literals.COMPONENT_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Port getBase_Port() {
		if (base_Port != null && base_Port.eIsProxy()) {
			InternalEObject oldBase_Port = (InternalEObject)base_Port;
			base_Port = (Port)eResolveProxy(oldBase_Port);
			if (base_Port != oldBase_Port) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ComponentsPackage.COMPONENT_PORT__BASE_PORT, oldBase_Port, base_Port));
			}
		}
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port basicGetBase_Port() {
		return base_Port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Port(Port newBase_Port) {
		Port oldBase_Port = base_Port;
		base_Port = newBase_Port;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_PORT__BASE_PORT, oldBase_Port, base_Port));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceDefinition getProvides() {
		ServiceDefinition provides = basicGetProvides();
		return provides != null && provides.eIsProxy() ? (ServiceDefinition)eResolveProxy((InternalEObject)provides) : provides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public ServiceDefinition basicGetProvides() {
		return ComponentPortOperations.getProvides(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ServiceDefinition getRequires() {
		ServiceDefinition requires = basicGetRequires();
		return requires != null && requires.eIsProxy() ? (ServiceDefinition)eResolveProxy((InternalEObject)requires) : requires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated NOT
	 */
	public ServiceDefinition basicGetRequires() {
		return ComponentPortOperations.getRequires(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getQos() {
		return qos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setQos(String newQos) {
		String oldQos = qos;
		qos = newQos;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_PORT__QOS, oldQos, qos));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isCoordinationPort() {
		return isCoordinationPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsCoordinationPort(boolean newIsCoordinationPort) {
		boolean oldIsCoordinationPort = isCoordinationPort;
		isCoordinationPort = newIsCoordinationPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ComponentsPackage.COMPONENT_PORT__IS_COORDINATION_PORT, oldIsCoordinationPort, isCoordinationPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_PORT__BASE_PORT:
				if (resolve) return getBase_Port();
				return basicGetBase_Port();
			case ComponentsPackage.COMPONENT_PORT__PROVIDES:
				if (resolve) return getProvides();
				return basicGetProvides();
			case ComponentsPackage.COMPONENT_PORT__REQUIRES:
				if (resolve) return getRequires();
				return basicGetRequires();
			case ComponentsPackage.COMPONENT_PORT__QOS:
				return getQos();
			case ComponentsPackage.COMPONENT_PORT__IS_COORDINATION_PORT:
				return isCoordinationPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_PORT__BASE_PORT:
				setBase_Port((Port)newValue);
				return;
			case ComponentsPackage.COMPONENT_PORT__QOS:
				setQos((String)newValue);
				return;
			case ComponentsPackage.COMPONENT_PORT__IS_COORDINATION_PORT:
				setIsCoordinationPort((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_PORT__BASE_PORT:
				setBase_Port((Port)null);
				return;
			case ComponentsPackage.COMPONENT_PORT__QOS:
				setQos(QOS_EDEFAULT);
				return;
			case ComponentsPackage.COMPONENT_PORT__IS_COORDINATION_PORT:
				setIsCoordinationPort(IS_COORDINATION_PORT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ComponentsPackage.COMPONENT_PORT__BASE_PORT:
				return base_Port != null;
			case ComponentsPackage.COMPONENT_PORT__PROVIDES:
				return basicGetProvides() != null;
			case ComponentsPackage.COMPONENT_PORT__REQUIRES:
				return basicGetRequires() != null;
			case ComponentsPackage.COMPONENT_PORT__QOS:
				return QOS_EDEFAULT == null ? qos != null : !QOS_EDEFAULT.equals(qos);
			case ComponentsPackage.COMPONENT_PORT__IS_COORDINATION_PORT:
				return isCoordinationPort != IS_COORDINATION_PORT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (qos: "); //$NON-NLS-1$
		result.append(qos);
		result.append(", isCoordinationPort: "); //$NON-NLS-1$
		result.append(isCoordinationPort);
		result.append(')');
		return result.toString();
	}

} // ComponentPortImpl
