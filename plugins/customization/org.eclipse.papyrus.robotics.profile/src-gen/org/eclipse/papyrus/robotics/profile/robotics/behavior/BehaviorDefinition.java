/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.behavior;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.uml2.uml.BehavioredClassifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getCompArch <em>Comp Arch</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getBase_BehavioredClassifier <em>Base Behaviored Classifier</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage#getBehaviorDefinition()
 * @model
 * @generated
 */
public interface BehaviorDefinition extends Entity {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage#getBehaviorDefinition_Task()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	Task getTask();

	/**
	 * Returns the value of the '<em><b>Comp Arch</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comp Arch</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comp Arch</em>' reference.
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage#getBehaviorDefinition_CompArch()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	org.eclipse.papyrus.robotics.profile.robotics.components.System getCompArch();

	/**
	 * Returns the value of the '<em><b>Base Behaviored Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Behaviored Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Behaviored Classifier</em>' reference.
	 * @see #setBase_BehavioredClassifier(BehavioredClassifier)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorPackage#getBehaviorDefinition_Base_BehavioredClassifier()
	 * @model ordered="false"
	 * @generated
	 */
	BehavioredClassifier getBase_BehavioredClassifier();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.behavior.BehaviorDefinition#getBase_BehavioredClassifier <em>Base Behaviored Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Behaviored Classifier</em>' reference.
	 * @see #getBase_BehavioredClassifier()
	 * @generated
	 */
	void setBase_BehavioredClassifier(BehavioredClassifier value);

} // BehaviorDefinition
