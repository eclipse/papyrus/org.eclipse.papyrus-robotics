/**
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.services;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.uml2.uml.Event;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Coordination Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getEvent <em>Event</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getBase_Event <em>Base Event</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getCoordinationEvent()
 * @model
 * @generated
 */
public interface CoordinationEvent extends Entity {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(ServiceDefinition)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getCoordinationEvent_Event()
	 * @model ordered="false"
	 * @generated
	 */
	ServiceDefinition getEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(ServiceDefinition value);

	/**
	 * Returns the value of the '<em><b>Base Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Event</em>' reference.
	 * @see #setBase_Event(Event)
	 * @see org.eclipse.papyrus.robotics.profile.robotics.services.ServicesPackage#getCoordinationEvent_Base_Event()
	 * @model ordered="false"
	 * @generated
	 */
	Event getBase_Event();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationEvent#getBase_Event <em>Base Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Event</em>' reference.
	 * @see #getBase_Event()
	 * @generated
	 */
	void setBase_Event(Event value);

} // CoordinationEvent
