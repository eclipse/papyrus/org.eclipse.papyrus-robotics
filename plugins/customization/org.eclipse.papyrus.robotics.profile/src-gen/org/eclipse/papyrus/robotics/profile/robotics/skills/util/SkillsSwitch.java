/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.skills.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connects;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;
import org.eclipse.papyrus.robotics.profile.robotics.skills.*;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinitionSet;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage
 * @generated
 */
public class SkillsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SkillsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SkillsSwitch() {
		if (modelPackage == null) {
			modelPackage = SkillsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SkillsPackage.SKILL_DEFINITION_SET: {
				SkillDefinitionSet skillDefinitionSet = (SkillDefinitionSet)theEObject;
				T result = caseSkillDefinitionSet(skillDefinitionSet);
				if (result == null) result = caseEntity(skillDefinitionSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.IN_ATTRIBUTE: {
				InAttribute inAttribute = (InAttribute)theEObject;
				T result = caseInAttribute(inAttribute);
				if (result == null) result = caseSkillParameter(inAttribute);
				if (result == null) result = casePort(inAttribute);
				if (result == null) result = caseEntity(inAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_PARAMETER: {
				SkillParameter skillParameter = (SkillParameter)theEObject;
				T result = caseSkillParameter(skillParameter);
				if (result == null) result = casePort(skillParameter);
				if (result == null) result = caseEntity(skillParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.OUT_ATTRIBUTE: {
				OutAttribute outAttribute = (OutAttribute)theEObject;
				T result = caseOutAttribute(outAttribute);
				if (result == null) result = caseSkillParameter(outAttribute);
				if (result == null) result = casePort(outAttribute);
				if (result == null) result = caseEntity(outAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_RESULT: {
				SkillResult skillResult = (SkillResult)theEObject;
				T result = caseSkillResult(skillResult);
				if (result == null) result = caseSkillParameter(skillResult);
				if (result == null) result = casePort(skillResult);
				if (result == null) result = caseEntity(skillResult);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_SEMANTIC: {
				SkillSemantic skillSemantic = (SkillSemantic)theEObject;
				T result = caseSkillSemantic(skillSemantic);
				if (result == null) result = caseBlock(skillSemantic);
				if (result == null) result = caseEntity(skillSemantic);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_SUCCESS_STATE: {
				SkillSuccessState skillSuccessState = (SkillSuccessState)theEObject;
				T result = caseSkillSuccessState(skillSuccessState);
				if (result == null) result = caseSkillState(skillSuccessState);
				if (result == null) result = caseBlock(skillSuccessState);
				if (result == null) result = caseEntity(skillSuccessState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_STATE: {
				SkillState skillState = (SkillState)theEObject;
				T result = caseSkillState(skillState);
				if (result == null) result = caseBlock(skillState);
				if (result == null) result = caseEntity(skillState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_FAIL_STATE: {
				SkillFailState skillFailState = (SkillFailState)theEObject;
				T result = caseSkillFailState(skillFailState);
				if (result == null) result = caseSkillState(skillFailState);
				if (result == null) result = caseBlock(skillFailState);
				if (result == null) result = caseEntity(skillFailState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_OPERATIONAL_STATE: {
				SkillOperationalState skillOperationalState = (SkillOperationalState)theEObject;
				T result = caseSkillOperationalState(skillOperationalState);
				if (result == null) result = caseSkillState(skillOperationalState);
				if (result == null) result = caseBlock(skillOperationalState);
				if (result == null) result = caseEntity(skillOperationalState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.TRANSITION_EDGE: {
				TransitionEdge transitionEdge = (TransitionEdge)theEObject;
				T result = caseTransitionEdge(transitionEdge);
				if (result == null) result = caseConnects(transitionEdge);
				if (result == null) result = caseRelation(transitionEdge);
				if (result == null) result = caseBlock(transitionEdge);
				if (result == null) result = caseEntity(transitionEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_INITIAL_STATE: {
				SkillInitialState skillInitialState = (SkillInitialState)theEObject;
				T result = caseSkillInitialState(skillInitialState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_FSM_REGION: {
				SkillFSMRegion skillFSMRegion = (SkillFSMRegion)theEObject;
				T result = caseSkillFSMRegion(skillFSMRegion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SkillsPackage.SKILL_DEFINITION: {
				SkillDefinition skillDefinition = (SkillDefinition)theEObject;
				T result = caseSkillDefinition(skillDefinition);
				if (result == null) result = caseBlock(skillDefinition);
				if (result == null) result = caseEntity(skillDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Definition Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Definition Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillDefinitionSet(SkillDefinitionSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillDefinition(SkillDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInAttribute(InAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillParameter(SkillParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Out Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Out Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutAttribute(OutAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillResult(SkillResult object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Semantic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Semantic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillSemantic(SkillSemantic object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Success State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Success State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillSuccessState(SkillSuccessState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillState(SkillState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Fail State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Fail State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillFailState(SkillFailState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Operational State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Operational State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillOperationalState(SkillOperationalState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransitionEdge(TransitionEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill Initial State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill Initial State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillInitialState(SkillInitialState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Skill FSM Region</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Skill FSM Region</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSkillFSMRegion(SkillFSMRegion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelation(Relation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connects</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connects</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnects(Connects object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SkillsSwitch
