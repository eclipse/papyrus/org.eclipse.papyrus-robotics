/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.profile.robotics.functions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Argument;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionsPackage;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionImpl#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionImpl#isCodeInModel <em>Code In Model</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.profile.robotics.functions.impl.FunctionImpl#getActivityPort <em>Activity Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionImpl extends BlockImpl implements Function {
	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final FunctionKind KIND_EDEFAULT = FunctionKind.HANDLER;
	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected FunctionKind kind = KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #isCodeInModel() <em>Code In Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCodeInModel()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CODE_IN_MODEL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getActivityPort() <em>Activity Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivityPort()
	 * @generated
	 * @ordered
	 */
	protected ActivityPort activityPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FunctionsPackage.Literals.FUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Argument> getArguments() {
		EList<Argument> argList = new BasicEList<>();
		for (Property p : getBase_Class().getOwnedAttributes()) {
			Argument argument = UMLUtil.getStereotypeApplication(p, Argument.class);
			if (argument != null) {
				argList.add(argument);
			}
		}
		return argList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FunctionKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKind(FunctionKind newKind) {
		FunctionKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FunctionsPackage.FUNCTION__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean isCodeInModel() {
		if (getBase_Class() instanceof OpaqueBehavior) {
			OpaqueBehavior ob = (OpaqueBehavior) getBase_Class();
			return ob.getBodies().size() > 0 && ob.getBodies().get(0).length() > 0;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityPort getActivityPort() {
		if (activityPort != null && activityPort.eIsProxy()) {
			InternalEObject oldActivityPort = (InternalEObject)activityPort;
			activityPort = (ActivityPort)eResolveProxy(oldActivityPort);
			if (activityPort != oldActivityPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FunctionsPackage.FUNCTION__ACTIVITY_PORT, oldActivityPort, activityPort));
			}
		}
		return activityPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityPort basicGetActivityPort() {
		return activityPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActivityPort(ActivityPort newActivityPort) {
		ActivityPort oldActivityPort = activityPort;
		activityPort = newActivityPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FunctionsPackage.FUNCTION__ACTIVITY_PORT, oldActivityPort, activityPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FunctionsPackage.FUNCTION__ARGUMENTS:
				return getArguments();
			case FunctionsPackage.FUNCTION__KIND:
				return getKind();
			case FunctionsPackage.FUNCTION__CODE_IN_MODEL:
				return isCodeInModel();
			case FunctionsPackage.FUNCTION__ACTIVITY_PORT:
				if (resolve) return getActivityPort();
				return basicGetActivityPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FunctionsPackage.FUNCTION__ARGUMENTS:
				getArguments().clear();
				getArguments().addAll((Collection<? extends Argument>)newValue);
				return;
			case FunctionsPackage.FUNCTION__KIND:
				setKind((FunctionKind)newValue);
				return;
			case FunctionsPackage.FUNCTION__ACTIVITY_PORT:
				setActivityPort((ActivityPort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FunctionsPackage.FUNCTION__ARGUMENTS:
				getArguments().clear();
				return;
			case FunctionsPackage.FUNCTION__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case FunctionsPackage.FUNCTION__ACTIVITY_PORT:
				setActivityPort((ActivityPort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FunctionsPackage.FUNCTION__ARGUMENTS:
				return !getArguments().isEmpty();
			case FunctionsPackage.FUNCTION__KIND:
				return kind != KIND_EDEFAULT;
			case FunctionsPackage.FUNCTION__CODE_IN_MODEL:
				return isCodeInModel() != CODE_IN_MODEL_EDEFAULT;
			case FunctionsPackage.FUNCTION__ACTIVITY_PORT:
				return activityPort != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (kind: "); //$NON-NLS-1$
		result.append(kind);
		result.append(')');
		return result.toString();
	}

} //FunctionImpl
