/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher(CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.navigator;

import org.eclipse.papyrus.uml.navigation.navigableElement.GenericNavigableElement;
import org.eclipse.uml2.uml.Type;

/**
 * Navigates from a TypedElement to its Type declaration
 *
 */
public class CommObjNavigableElement extends GenericNavigableElement {
	/**
	 *
	 * @param type
	 *            The Type to navigate to. May be null.
	 */
	public CommObjNavigableElement(Type type) {
		super(type);
	}

	public String getLabel() {
		String label = "Go to communication object" + getElementLabel() + "...";
		return label;
	}

	public String getDescription() {
		return "Go to the communication object exchanged by this port or connection: " + getElementLabel();
	}
}
