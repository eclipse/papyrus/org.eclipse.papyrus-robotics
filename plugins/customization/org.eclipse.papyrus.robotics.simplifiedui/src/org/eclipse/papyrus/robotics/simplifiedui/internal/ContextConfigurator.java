/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 */
package org.eclipse.papyrus.robotics.simplifiedui.internal;

import org.eclipse.papyrus.views.properties.runtime.ConfigurationManager;

public final class ContextConfigurator {

	public static final String[] DEFAULT_CONTEXTS = new String[] { Context.UML };

	public static final String[] RMS_CONTEXTS = new String[] { Context.MARTE };

	/** Configuration manager instance */
	public static final ConfigurationManager configurationManager = ConfigurationManager.getInstance();

	private static boolean showDefault = true;
	
	private static boolean showRMS = false;

	private ContextConfigurator() {
	}

	public static void showDefault() {
		if (!showDefault) {
			setVisibility(DEFAULT_CONTEXTS, true);
			showDefault = true;
		}
	}

	public static void hideDefault() {
		if (showDefault) {
			setVisibility(DEFAULT_CONTEXTS, false);
			showDefault = false;
		}
	}

	public static void showRobotics() {
		if (!showRMS) {
			setVisibility(RMS_CONTEXTS, true);
			showRMS = true;
		}
	}

	public static void hideRobotics() {
		if (showRMS) {
			setVisibility(RMS_CONTEXTS, false);
			showRMS = false;
		}
	}

	public static void setVisibility(String[] contextNames, boolean visibility) {
		for (String contextName : contextNames) {
			org.eclipse.papyrus.infra.properties.contexts.Context context = configurationManager
					.getContext(contextName);
			if (context != null) {
				try {
					if (visibility) {
						configurationManager.enableContext(context, true);
					} else {
						configurationManager.disableContext(context, true);
					}

				} catch (IllegalStateException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@SuppressWarnings("nls")
	public interface Context {
		/** AdvanceStyle */
		String ADVANCE_STYLE = "AdvanceStyle";

		/** CSS in Diagrams */
		String CSS = "CSS";

		/** Customization Models */
		String CUSTOMIZATION = "Customization";

		/** Diagram Notation */
		String NOTATION = "notation";

		/** Diagram Styles */
		String STYLE = "style";

		/** Diagram Synchronization */
		String SYNCHRONIZATION = "synchronization";

		/** UML Diagram Symbols */
		String SYMBOLS = "Symbols";

		/** UML Graphical Notation */
		String UML_NOTATION = "UMLNotation";

		/** UML Metamodel */
		String UML = "UML";

		/** MARTE Metamodel */
		String MARTE = "MARTE";

		/** UML Profile Externalization */
		String UML_STEREOTYPE_APPLICATION_EXTERNAL_RESOURCE = "UMLStereotypeApplicationExternalResource";

		/** UML Sequence Diagram Notation */
		String SEQUENCE_NOTATION = "SequenceNotation";
	}
}
