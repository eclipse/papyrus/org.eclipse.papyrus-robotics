/*****************************************************************************
 * Copyright (c) 2017 CEA LIST, Christian W. Damus, and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   Christian W. Damus - bug 510315
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.providers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.uml.tools.providers.UMLFilteredLabelProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Label provider used by the label provider service.
 */
public class RoboticsLabelProvider extends UMLFilteredLabelProvider {

	/**
	 * Constructor.
	 *
	 */
	public RoboticsLabelProvider() {
		super();
	}

	@Override
	public boolean accept(Object element) {
		if (element instanceof IStructuredSelection) {
			return accept((IStructuredSelection) element);
		}

		// The element is a UML Element or can be adapted to an EObject
		EObject eObject = EMFHelper.getEObject(element);
		if (eObject == null) {
			return false;
		}

		// UML Elements
		if (eObject instanceof Connector || eObject instanceof Port) {
			return true;
		}
		else if (eObject instanceof Element) {
			if (StereotypeUtil.isApplied((Element) eObject, ComponentService.class)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getText(Object element) {
		String result;

		if (element instanceof IStructuredSelection) {
			// Handle the Tabbed Properties View
			result = getText((IStructuredSelection) element);
		} else {
			EObject eObject = EMFHelper.getEObject(element);
			if (eObject != null) {
				result = getText(eObject);
			} else {
				result = super.getText(element);
			}
		}

		return result;
	}

	@Override
	protected String getText(EObject element) {
		if (element instanceof Connector) {
			return InteractionUtils.getConnectorLabel((Connector) element);
		}
		else if (element instanceof Port) {
			// use name only
			return ((Port) element).getName();
		}
		else if (element instanceof Element) {
			if (StereotypeUtil.isApplied((Element) element, ComponentService.class)) {
				for(Setting setting : UMLUtil.getNonNavigableInverseReferences(element)) {
					EObject portEObj = setting.getEObject();
					if (portEObj instanceof Port) {
						return String.format("CS for %s", ((Port) portEObj).getName());
					}
				}
			}
		}
		return super.getText(element);
	}

}
