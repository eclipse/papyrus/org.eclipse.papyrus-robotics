/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher(CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.navigator;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.papyrus.infra.services.navigation.service.NavigableElement;
import org.eclipse.papyrus.infra.services.navigation.service.NavigationContributor;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Type;

/**
 * NavigationContributor to navigate from a port to the service definition
 *
 */
public class RMSNavigationContributor implements NavigationContributor {

	public List<NavigableElement> getNavigableElements(Object fromElement) {
		List<NavigableElement> result = new LinkedList<NavigableElement>();

		Element element = UMLUtil.resolveUMLElement(fromElement);
		if (element instanceof Port) {
			Port port = (Port) element;
			for (Type commObject : InteractionUtils.getCommObjects(port)) {
				result.add(new CommObjNavigableElement(commObject));
			}
			CommunicationPattern pattern = InteractionUtils.getCommunicationPattern(port);
			if (pattern != null) {
				result.add(new CommPatternNavigableElement(pattern.getBase_Collaboration()));	
			}
		}
		else if (element instanceof Connector) {
			Connector connector = (Connector) element;
			for (Type commObject : InteractionUtils.getCommObjects(connector)) {
				result.add(new CommObjNavigableElement(commObject));
			}
			CommunicationPattern pattern = InteractionUtils.getCommunicationPattern(connector);
			if (pattern != null) {
				result.add(new CommPatternNavigableElement(pattern.getBase_Collaboration()));	
			}
		}
		return result;
	}
}
