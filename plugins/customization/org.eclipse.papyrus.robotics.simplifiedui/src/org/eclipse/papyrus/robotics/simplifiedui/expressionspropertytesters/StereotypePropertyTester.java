/*****************************************************************************
 * Copyright (c) 2017, 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie TATIBOUET (CEA LIST) - jeremie.tatibouet@cea.fr
 *  Matteo MORELLI (CEA LIST) - matteo.morelli@cea.fr - enable test for multiple selections
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.simplifiedui.expressionspropertytesters;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public abstract class StereotypePropertyTester extends PropertyTester {

	/*protected NamedElement getUMLSelection(IStructuredSelection selection){
		NamedElement element = null;
		EObject selectedEObject = EMFHelper.getEObject(selection.getFirstElement());
		if(selectedEObject != null
				&& selectedEObject instanceof NamedElement){
			element = (NamedElement) selectedEObject;
		}
		return element;
	}*/

	protected boolean isAllTrue(boolean[] array)
	{
	    for(boolean b : array) if(!b) return false;
	    return true;
	}

	protected boolean isAllFalse(boolean[] array)
	{
	    for(boolean b : array) if(b) return false;
	    return true;
	}

	protected NamedElement getUMLSelection(Object selectedObject){
		NamedElement element = null;
		EObject selectedEObject = EMFHelper.getEObject(selectedObject);
		if(selectedEObject != null
				&& selectedEObject instanceof NamedElement){
			element = (NamedElement) selectedEObject;
		}
		return element;
	}

	/*protected boolean isMultiple(IStructuredSelection selection){
		return selection.size() > 1;
	}*/

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if(!(property.equals(this.getProperty())) || expectedValue == null)
			return false;
		IStructuredSelection selection = (IStructuredSelection) receiver;
		boolean[] verdict = new boolean[selection.size()];
		int i = 0;
		for(Object obj : selection.toArray()) {
			NamedElement selectedElement = this.getUMLSelection(obj);
			verdict [i++] = ( selectedElement == null ||
					UMLUtil.getStereotypeApplication(selectedElement, this.getStereotypeDefinition()) == null ) ?
							false : true;
		}
		return ( (Boolean.valueOf(expectedValue.toString()) == true) ? isAllTrue(verdict) : isAllFalse(verdict) );
	}
	
	public abstract String getProperty();
	
	public abstract Class<? extends EObject> getStereotypeDefinition();

}
