/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.ui;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.papyrus.infra.architecture.listeners.IArchitectureDescriptionListener;
import org.eclipse.papyrus.robotics.core.RoboticsConstants;
import org.eclipse.papyrus.robotics.simplifiedui.internal.ContextConfigurator;

public class RoboticsArchitectureDescriptionListener implements IArchitectureDescriptionListener {

	@Override
	public void architectureContextChanged(Notification notification) {
		// Nothing to do for now
	}

	@Override
	public void architectureViewpointsChanged(Notification notification) {
		try {
			String newValue = notification.getNewStringValue();
			
			if (newValue != null) {
				if (newValue.contains(RoboticsConstants.ROBOTICS_COMPONENT_DEV_VIEWPOINT_ID) ||
						newValue.contains(RoboticsConstants.ROBOTICS_SERVICE_DESIGN_VIEWPOINT_ID) ||
						newValue.contains(RoboticsConstants.ROBOTICS_SERVICE_FULFILLMENT_VIEWPOINT_ID) ||
						newValue.contains(RoboticsConstants.ROBOTICS_SYSTEM_CONFIGURATION_VIEWPOINT_ID)) {
					ContextConfigurator.hideDefault();
					ContextConfigurator.showRobotics();
				} else {
					ContextConfigurator.hideRobotics();
					ContextConfigurator.showDefault();
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

}
