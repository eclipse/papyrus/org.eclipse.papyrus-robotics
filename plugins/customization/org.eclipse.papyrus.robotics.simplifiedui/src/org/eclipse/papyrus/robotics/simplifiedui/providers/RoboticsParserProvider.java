/**
 * Copyright (c) 2014 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.robotics.simplifiedui.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.papyrus.robotics.simplifiedui.parsers.RoboticsParser;
import org.eclipse.papyrus.uml.diagram.clazz.edit.parts.OperationForInterfaceEditpart;
import org.eclipse.papyrus.uml.diagram.composite.edit.parts.ConnectorNameEditPart;
import org.eclipse.papyrus.uml.diagram.composite.edit.parts.PortNameEditPart;
import org.eclipse.papyrus.uml.diagram.composite.part.UMLVisualIDRegistry;

/**
 * A parser for robotic elements (i.e. calculate labels)
 */
public class RoboticsParserProvider extends AbstractProvider implements IParserProvider {

	protected IParser roboticsParser = null;

	protected IParser getRoboticsParser() {
		if (roboticsParser == null) {
			roboticsParser = new RoboticsParser();
		}
		return roboticsParser;
	}

	/**
	 * Obtain a parser for a given visual ID
	 * 
	 * @param visualID
	 *            the visual ID of an element
	 * @return the associated parser
	 */
	protected IParser getParser(String visualID) {
		if (visualID != null) {
			switch (visualID) {
				case ConnectorNameEditPart.VISUAL_ID:
				case PortNameEditPart.VISUAL_ID:
				case OperationForInterfaceEditpart.VISUAL_ID:	// register parser for skill definition (edit advice is not called by default one)
					return getRoboticsParser();
			}
		}
		return null;
	}

	@Override
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			return getParser(hint) != null;
		}
		return false;
	}


	@Override
	public IParser getParser(IAdaptable hint) {
		String vid = hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(UMLVisualIDRegistry.getVisualID(vid));
		}
		return null;
	}
}
