/**
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 */
package org.eclipse.papyrus.robotics.simplifiedui.navigator;

import org.eclipse.ui.internal.navigator.resources.actions.OpenActionProvider;

/**
 * An Action Provider that provides the 'Open' and 'Open With' actions in the navigator.
 * 
 * In the Papyrus IM setup, these actions would be provided by the action provider of JDT,
 * which overrides the action provider of the navigator plugin. However, by hiding the JDT
 * contributions through activities, we also lose the action provider. This class restores
 * the lost actions.
 */
@SuppressWarnings("restriction")
public class PackageExplorerOpenActionProvider extends OpenActionProvider {
}
