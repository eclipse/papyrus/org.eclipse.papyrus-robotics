package org.eclipse.papyrus.robotics.simplifiedui.ui.menu.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.papyrus.robotics.simplifiedui.Activator;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.DataType;

public class MarkCommunicationObjectAsDataTypeHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection)
        {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			IRunnableWithProgress operation = new IRunnableWithProgress()
            {
                public void run(IProgressMonitor monitor)
                {
                    try {
                    	for(Object current : structuredSelection.toArray())
    						if(current instanceof IAdaptable) {
    							EObject eObj = (EObject) ((IAdaptable) current).getAdapter(EObject.class);
    		                    DataType umldt = (DataType) eObj;
    		                    // ====================================
    		                    // apply Stereotype CommunicationObject
    		                    // ====================================
    		                    // build commands
    		                    TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(umldt);
    		                    RecordingCommand addRmsCommObjStereotype = new RecordingCommand(ted,
    		                    		"Application of RMS CommunicationObject stereotype") { //$NON-NLS-1$
    		                    	@Override
    		                        protected void doExecute() {
    		                    		StereotypeUtil.unapply(umldt, CommunicationObject.class);
    		                    		StereotypeUtil.apply(umldt, org.eclipse.papyrus.robotics.profile.robotics.commobject.DataType.class);
    		                    	}
    		                    };
    		                    // execute
    		                    Display.getDefault().asyncExec(new Runnable() {
    		                    	public void run() {
    		                    		ted.getCommandStack().execute(addRmsCommObjStereotype);
    		                    	}
    		                    });
    		                }
                    } catch (Exception e) {
                        IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
                        Activator.getDefault().getLog().log(status);
                    }
                }
            };
            try {
                PlatformUI.getWorkbench().getProgressService().run(true, true, operation);
            } catch (InvocationTargetException e) {
                IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
                Activator.getDefault().getLog().log(status);
            } catch (InterruptedException e) {
                IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
                Activator.getDefault().getLog().log(status);
            }
        }
		return null;
	}

}
