/**
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 */
package org.eclipse.papyrus.robotics.simplifiedui.internal;

import org.eclipse.papyrus.infra.newchild.CreationMenuRegistry;
import org.eclipse.papyrus.infra.newchild.elementcreationmenumodel.Folder;

/**
 * Cleans the creation menu.
 */
public final class CreationMenuConfigurator {

	public static final String UML_NEW_CHILD_MENU = "/resource/UML.creationmenumodel"; //$NON-NLS-1$
	public static final String UML_NEW_RELATIONSHIP_MENU = "/resource/UMLEdges.creationmenumodel"; //$NON-NLS-1$

	public static final String[] DEFAULT_CHILD_MENUS = new String[] {
			UML_NEW_CHILD_MENU, UML_NEW_RELATIONSHIP_MENU
	};

	public static final String[] ROBMOSYS_CHILD_MENUS = new String[] {
			// HardwareModelingConstants.HW_NEW_CHILD_MENU, HardwareModelingConstants.HW_NEW_RELATIONSHIP_MENU
	};

	private CreationMenuConfigurator() {
		// hidden constructor.
	}

	public static void hideDefault() {
		setVisibility(DEFAULT_CHILD_MENUS, false);
	}

	public static void showDefault() {
		setVisibility(DEFAULT_CHILD_MENUS, true);
	}

	public static void hideHw() {
		// setVisibility(ROBMOSYS_CHILD_MENUS, false);
	}

	public static void showHw() {
		// setVisibility(ROBMOSYS_CHILD_MENUS, true);
	}

	public static void setVisibility(String[] childMenuPaths, boolean visibility) {
		CreationMenuRegistry instance = CreationMenuRegistry.getInstance();
		for (Folder folder : instance.getRootFolder()) {
			for (String childMenuPath : childMenuPaths) {
				if (folder.eResource().getURI().toString().endsWith(childMenuPath)) {
					instance.setCreationMenuVisibility(folder, visibility);
				}
			}
		}
	}
}
