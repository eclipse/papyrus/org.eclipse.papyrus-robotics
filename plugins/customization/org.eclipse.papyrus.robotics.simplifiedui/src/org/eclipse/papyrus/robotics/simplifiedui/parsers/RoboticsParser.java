/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.parsers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.uml.diagram.composite.parsers.MessageFormatParser;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * A robotics parser used in diagrams (see also RoboticsLabelProvider)
 * Its main purpose is to provide a nicer label for connectors and ports,
 * It is also used to assure that the SkillDefinition advice gets called
 * when editing.
 */
public class RoboticsParser extends MessageFormatParser implements IParser {

	public RoboticsParser() {
		super(new EAttribute[] { UMLPackage.eINSTANCE.getNamedElement_Name() });
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		EObject eObj = element.getAdapter(EObject.class);
		if (eObj instanceof Connector) {
			return InteractionUtils.getConnectorLabel((Connector) eObj);
		} else if (eObj instanceof Port) {
			return PortUtils.getPortLabel((Port) eObj);
		}
		else {
			return super.getPrintString(element, flags);
		}
	}
}
