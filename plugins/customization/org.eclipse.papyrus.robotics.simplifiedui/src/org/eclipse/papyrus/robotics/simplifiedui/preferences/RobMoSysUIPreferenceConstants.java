/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.preferences;

public class RobMoSysUIPreferenceConstants {

	/**
	 * Hide standard menus
	 */
	public static final String P_HIDE_STD = "hideStd"; //$NON-NLS-1$
}
