/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.ui;

import org.eclipse.papyrus.infra.architecture.ArchitectureDescriptionUtils;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.CoreMultiDiagramEditor;
import org.eclipse.papyrus.robotics.core.RoboticsConstants;
import org.eclipse.papyrus.robotics.simplifiedui.Activator;
import org.eclipse.papyrus.robotics.simplifiedui.internal.ContextConfigurator;
import org.eclipse.papyrus.robotics.simplifiedui.internal.CreationMenuConfigurator;
import org.eclipse.papyrus.robotics.simplifiedui.internal.PreferenceManipulation;
import org.eclipse.papyrus.robotics.simplifiedui.preferences.RobMoSysUIPreferenceUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Part listener that hides or activates properties view, depending on whether the current page has the RobMoSys
 * architectural context
 */
public class RoboticsPartListener implements IPartListener {

	boolean activate = false;

	@Override
	public void partOpened(IWorkbenchPart part) {
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {
	}

	@Override
	public void partClosed(IWorkbenchPart part) {
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
	}

	@Override
	public void partActivated(IWorkbenchPart part) {
		IWorkbenchPage page = part.getSite().getPage();
		activate = false;
		if (page.getActiveEditor() instanceof CoreMultiDiagramEditor) {
			ServicesRegistry sr = ((CoreMultiDiagramEditor) page.getActiveEditor()).getServicesRegistry();
			if (sr != null) {
				try {
					final ModelSet set = sr.getService(ModelSet.class);
					final ArchitectureDescriptionUtils adUtils = new ArchitectureDescriptionUtils(set);
					activate = adUtils.getArchitectureContextId().equals(RoboticsConstants.ROBOTICS_MODELING_CONTEXT);
				} catch (ServiceException e) {
					Activator.log.error(e);
				}
			}
		}
		Display.getCurrent().asyncExec(new Runnable() {
			public void run() {
				if (activate) {
					activateRobMoSys();
				} else {
					deactivateRobMoSys();
				}
			}
		});
	}

	protected void activateRobMoSys() {
		PreferenceManipulation.disablePopupBar();
		PreferenceManipulation.disableStdDrops();
		ContextConfigurator.showRobotics();
		// flag to temporarily show default UML menus/property view
		final boolean hideStd = RobMoSysUIPreferenceUtils.getHideStd();
		if (!hideStd) {
			CreationMenuConfigurator.showDefault();
			ContextConfigurator.showDefault();
		}
		else {
			CreationMenuConfigurator.hideDefault();
			ContextConfigurator.hideDefault();
		}
	}

	protected void deactivateRobMoSys() {
		PreferenceManipulation.enablePopupBar();
		PreferenceManipulation.enableStdDrops();
		ContextConfigurator.showDefault();
		CreationMenuConfigurator.showDefault();
		ContextConfigurator.hideRobotics();
	}
}

