/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) - matteo.morelli@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.simplifiedui.expressionspropertytesters;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;

public class CommunicationObjectTester extends StereotypePropertyTester {

	protected final static String PROPERTY_IS_COMMUNICATIONOBJECT = "isCommunicationObject"; //$NON-NLS-1$

	@Override
	public String getProperty() {
		return PROPERTY_IS_COMMUNICATIONOBJECT;
	}

	@Override
	public Class<? extends EObject> getStereotypeDefinition() {
		return CommunicationObject.class;
	}
}
