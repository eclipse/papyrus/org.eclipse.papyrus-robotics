/**
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - Initial API and implementation
 */
package org.eclipse.papyrus.robotics.simplifiedui.internal;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.gmf.runtime.diagram.ui.preferences.IPreferenceConstants;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

/**
 * Workaround, these particular preferences are ignored via plugin_customization.ini
 * May actually be a good solution for the future as well, since we need to re-enable the
 * default drop strategies for standard Papyrus models 
 */
public class PreferenceManipulation {
	public static final String DND_PORT_TO_TYPES_PORT_DROP_STRATEGY_IS_ACTIVE =
			"org.eclipse.papyrus.uml.diagram.parametric.dnd.PortToTypesPortDropStrategy.isActive"; //$NON-NLS-1$
	public static final String DND_CLASSIFIER_TO_STRUCTURE_COMP_AS_PROPERTY_DROP_IS_ACTIVE =
			"org.eclipse.papyrus.uml.diagram.dnd.ClassifierToStructureCompAsPropertyDrop.isActive"; //$NON-NLS-1$

	public static void disablePopupBar() {
		IPreferenceStore store = org.eclipse.papyrus.infra.gmfdiag.preferences.Activator.getDefault().getPreferenceStore();
		store.setDefault(IPreferenceConstants.PREF_SHOW_POPUP_BARS, false);
	}

	public static void enablePopupBar() {
		IPreferenceStore store = org.eclipse.papyrus.infra.gmfdiag.preferences.Activator.getDefault().getPreferenceStore();
		store.setDefault(IPreferenceConstants.PREF_SHOW_POPUP_BARS, true);
	}

	private static IPreferenceStore dndPrefStore = null;
	
	public static void disableStdDrops() {		
		getDndPrefStore();
		dndPrefStore.setDefault(DND_CLASSIFIER_TO_STRUCTURE_COMP_AS_PROPERTY_DROP_IS_ACTIVE, false);
		dndPrefStore.setDefault(DND_PORT_TO_TYPES_PORT_DROP_STRATEGY_IS_ACTIVE, false);
	}

	public static void enableStdDrops() {
		getDndPrefStore();
		dndPrefStore.setDefault(DND_CLASSIFIER_TO_STRUCTURE_COMP_AS_PROPERTY_DROP_IS_ACTIVE, true);
		dndPrefStore.setDefault(DND_PORT_TO_TYPES_PORT_DROP_STRATEGY_IS_ACTIVE, true);
	}
	
	protected static void getDndPrefStore() {
		// Activator of gmfdiag.dnd is not accessible
		// IPreferenceStore store = eclipse.papyrus.infra.gmfdiag.dnd.Activator.getDefault().getPreferenceStore();
		if (dndPrefStore == null) {
			dndPrefStore = new ScopedPreferenceStore(InstanceScope.INSTANCE, "org.eclipse.papyrus.infra.gmfdiag.dnd"); //$NON-NLS-1$
		}
	}
}
