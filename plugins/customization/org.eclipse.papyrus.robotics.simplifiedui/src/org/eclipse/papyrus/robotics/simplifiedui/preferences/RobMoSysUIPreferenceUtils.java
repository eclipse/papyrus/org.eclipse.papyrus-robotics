/*******************************************************************************
 * Copyright (c) 2006 - 2012 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.robotics.simplifiedui.preferences;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.papyrus.robotics.simplifiedui.Activator;

/**
 * Utility class that returns the preference values
 */
public class RobMoSysUIPreferenceUtils {

	protected static IPreferenceStore preferenceStore = null;

	public static boolean getHideStd() {
		initPreferenceStore();
		return preferenceStore.getBoolean(RobMoSysUIPreferenceConstants.P_HIDE_STD);
	}

	public static void initPreferenceStore() {
		if (preferenceStore == null) {
			preferenceStore = Activator.getDefault().getPreferenceStore();
		}
	}
}
