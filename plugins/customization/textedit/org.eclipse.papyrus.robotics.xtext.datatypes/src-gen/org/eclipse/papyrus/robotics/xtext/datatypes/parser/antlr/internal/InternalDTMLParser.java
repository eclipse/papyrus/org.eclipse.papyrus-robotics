package org.eclipse.papyrus.robotics.xtext.datatypes.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.papyrus.robotics.xtext.datatypes.services.DTMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDTMLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_VSL_COMMENT", "RULE_INT", "RULE_DOUBLE", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_INTEGER_VALUE", "RULE_WS", "RULE_ANY_OTHER", "'{'", "'}'", "'DataType'", "'CommObject'", "'attribute'", "':'", "'<Undefined>'", "'='", "';'", "'['", "']'", "'-'", "','", "'Enumeration'", "'::'", "'..'", "'*'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int RULE_DOUBLE=7;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_ID=4;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_VSL_COMMENT=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_INTEGER_VALUE=11;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDTMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDTMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDTMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDTML.g"; }



     	private DTMLGrammarAccess grammarAccess;

        public InternalDTMLParser(TokenStream input, DTMLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected DTMLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalDTML.g:65:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalDTML.g:65:46: (iv_ruleModel= ruleModel EOF )
            // InternalDTML.g:66:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalDTML.g:72:1: ruleModel returns [EObject current=null] : (this_XDataType_0= ruleXDataType | this_XAssignment_1= ruleXAssignment | this_XEnumeration_2= ruleXEnumeration ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject this_XDataType_0 = null;

        EObject this_XAssignment_1 = null;

        EObject this_XEnumeration_2 = null;



        	enterRule();

        try {
            // InternalDTML.g:78:2: ( (this_XDataType_0= ruleXDataType | this_XAssignment_1= ruleXAssignment | this_XEnumeration_2= ruleXEnumeration ) )
            // InternalDTML.g:79:2: (this_XDataType_0= ruleXDataType | this_XAssignment_1= ruleXAssignment | this_XEnumeration_2= ruleXEnumeration )
            {
            // InternalDTML.g:79:2: (this_XDataType_0= ruleXDataType | this_XAssignment_1= ruleXAssignment | this_XEnumeration_2= ruleXEnumeration )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 16:
            case 17:
                {
                alt1=1;
                }
                break;
            case RULE_ID:
                {
                alt1=2;
                }
                break;
            case 27:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalDTML.g:80:3: this_XDataType_0= ruleXDataType
                    {

                    			newCompositeNode(grammarAccess.getModelAccess().getXDataTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_XDataType_0=ruleXDataType();

                    state._fsp--;


                    			current = this_XDataType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDTML.g:89:3: this_XAssignment_1= ruleXAssignment
                    {

                    			newCompositeNode(grammarAccess.getModelAccess().getXAssignmentParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_XAssignment_1=ruleXAssignment();

                    state._fsp--;


                    			current = this_XAssignment_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDTML.g:98:3: this_XEnumeration_2= ruleXEnumeration
                    {

                    			newCompositeNode(grammarAccess.getModelAccess().getXEnumerationParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_XEnumeration_2=ruleXEnumeration();

                    state._fsp--;


                    			current = this_XEnumeration_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleXDataType"
    // InternalDTML.g:110:1: entryRuleXDataType returns [EObject current=null] : iv_ruleXDataType= ruleXDataType EOF ;
    public final EObject entryRuleXDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXDataType = null;


        try {
            // InternalDTML.g:110:50: (iv_ruleXDataType= ruleXDataType EOF )
            // InternalDTML.g:111:2: iv_ruleXDataType= ruleXDataType EOF
            {
             newCompositeNode(grammarAccess.getXDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXDataType=ruleXDataType();

            state._fsp--;

             current =iv_ruleXDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXDataType"


    // $ANTLR start "ruleXDataType"
    // InternalDTML.g:117:1: ruleXDataType returns [EObject current=null] : ( ruleDTKey ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleProperty ) )* otherlv_4= '}' ) ;
    public final EObject ruleXDataType() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_attributes_3_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:123:2: ( ( ruleDTKey ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleProperty ) )* otherlv_4= '}' ) )
            // InternalDTML.g:124:2: ( ruleDTKey ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleProperty ) )* otherlv_4= '}' )
            {
            // InternalDTML.g:124:2: ( ruleDTKey ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleProperty ) )* otherlv_4= '}' )
            // InternalDTML.g:125:3: ruleDTKey ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_attributes_3_0= ruleProperty ) )* otherlv_4= '}'
            {

            			newCompositeNode(grammarAccess.getXDataTypeAccess().getDTKeyParserRuleCall_0());
            		
            pushFollow(FOLLOW_3);
            ruleDTKey();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            // InternalDTML.g:132:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDTML.g:133:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDTML.g:133:4: (lv_name_1_0= RULE_ID )
            // InternalDTML.g:134:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getXDataTypeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXDataTypeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getXDataTypeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDTML.g:154:3: ( (lv_attributes_3_0= ruleProperty ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==18) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalDTML.g:155:4: (lv_attributes_3_0= ruleProperty )
            	    {
            	    // InternalDTML.g:155:4: (lv_attributes_3_0= ruleProperty )
            	    // InternalDTML.g:156:5: lv_attributes_3_0= ruleProperty
            	    {

            	    					newCompositeNode(grammarAccess.getXDataTypeAccess().getAttributesPropertyParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_attributes_3_0=ruleProperty();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXDataTypeRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_3_0,
            	    						"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.Property");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getXDataTypeAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXDataType"


    // $ANTLR start "entryRuleDTKey"
    // InternalDTML.g:181:1: entryRuleDTKey returns [String current=null] : iv_ruleDTKey= ruleDTKey EOF ;
    public final String entryRuleDTKey() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDTKey = null;


        try {
            // InternalDTML.g:181:45: (iv_ruleDTKey= ruleDTKey EOF )
            // InternalDTML.g:182:2: iv_ruleDTKey= ruleDTKey EOF
            {
             newCompositeNode(grammarAccess.getDTKeyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDTKey=ruleDTKey();

            state._fsp--;

             current =iv_ruleDTKey.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDTKey"


    // $ANTLR start "ruleDTKey"
    // InternalDTML.g:188:1: ruleDTKey returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'DataType' | kw= 'CommObject' ) ;
    public final AntlrDatatypeRuleToken ruleDTKey() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDTML.g:194:2: ( (kw= 'DataType' | kw= 'CommObject' ) )
            // InternalDTML.g:195:2: (kw= 'DataType' | kw= 'CommObject' )
            {
            // InternalDTML.g:195:2: (kw= 'DataType' | kw= 'CommObject' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==16) ) {
                alt3=1;
            }
            else if ( (LA3_0==17) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDTML.g:196:3: kw= 'DataType'
                    {
                    kw=(Token)match(input,16,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDTKeyAccess().getDataTypeKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDTML.g:202:3: kw= 'CommObject'
                    {
                    kw=(Token)match(input,17,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDTKeyAccess().getCommObjectKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDTKey"


    // $ANTLR start "entryRuleProperty"
    // InternalDTML.g:211:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalDTML.g:211:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalDTML.g:212:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalDTML.g:218:1: ruleProperty returns [EObject current=null] : (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_typeUndefined_4_0=null;
        Token otherlv_6=null;
        Token lv_comment_8_0=null;
        EObject lv_type_3_0 = null;

        EObject lv_multiplicity_5_0 = null;

        EObject lv_value_7_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:224:2: ( (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? ) )
            // InternalDTML.g:225:2: (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? )
            {
            // InternalDTML.g:225:2: (otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? )
            // InternalDTML.g:226:3: otherlv_0= 'attribute' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )?
            {
            otherlv_0=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPropertyAccess().getAttributeKeyword_0());
            		
            // InternalDTML.g:230:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDTML.g:231:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDTML.g:231:4: (lv_name_1_0= RULE_ID )
            // InternalDTML.g:232:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_1_0, grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            // InternalDTML.g:248:3: (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==19) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalDTML.g:249:4: otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) )
                    {
                    otherlv_2=(Token)match(input,19,FOLLOW_7); 

                    				newLeafNode(otherlv_2, grammarAccess.getPropertyAccess().getColonKeyword_2_0());
                    			
                    // InternalDTML.g:253:4: ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) )
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==RULE_ID) ) {
                        alt4=1;
                    }
                    else if ( (LA4_0==20) ) {
                        alt4=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 0, input);

                        throw nvae;
                    }
                    switch (alt4) {
                        case 1 :
                            // InternalDTML.g:254:5: ( (lv_type_3_0= ruleTypeRule ) )
                            {
                            // InternalDTML.g:254:5: ( (lv_type_3_0= ruleTypeRule ) )
                            // InternalDTML.g:255:6: (lv_type_3_0= ruleTypeRule )
                            {
                            // InternalDTML.g:255:6: (lv_type_3_0= ruleTypeRule )
                            // InternalDTML.g:256:7: lv_type_3_0= ruleTypeRule
                            {

                            							newCompositeNode(grammarAccess.getPropertyAccess().getTypeTypeRuleParserRuleCall_2_1_0_0());
                            						
                            pushFollow(FOLLOW_8);
                            lv_type_3_0=ruleTypeRule();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getPropertyRule());
                            							}
                            							set(
                            								current,
                            								"type",
                            								lv_type_3_0,
                            								"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.TypeRule");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalDTML.g:274:5: ( (lv_typeUndefined_4_0= '<Undefined>' ) )
                            {
                            // InternalDTML.g:274:5: ( (lv_typeUndefined_4_0= '<Undefined>' ) )
                            // InternalDTML.g:275:6: (lv_typeUndefined_4_0= '<Undefined>' )
                            {
                            // InternalDTML.g:275:6: (lv_typeUndefined_4_0= '<Undefined>' )
                            // InternalDTML.g:276:7: lv_typeUndefined_4_0= '<Undefined>'
                            {
                            lv_typeUndefined_4_0=(Token)match(input,20,FOLLOW_8); 

                            							newLeafNode(lv_typeUndefined_4_0, grammarAccess.getPropertyAccess().getTypeUndefinedUndefinedKeyword_2_1_1_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getPropertyRule());
                            							}
                            							setWithLastConsumed(current, "typeUndefined", lv_typeUndefined_4_0 != null, "<Undefined>");
                            						

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalDTML.g:290:3: ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==23) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalDTML.g:291:4: (lv_multiplicity_5_0= ruleMultiplicityRule )
                    {
                    // InternalDTML.g:291:4: (lv_multiplicity_5_0= ruleMultiplicityRule )
                    // InternalDTML.g:292:5: lv_multiplicity_5_0= ruleMultiplicityRule
                    {

                    					newCompositeNode(grammarAccess.getPropertyAccess().getMultiplicityMultiplicityRuleParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_9);
                    lv_multiplicity_5_0=ruleMultiplicityRule();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getPropertyRule());
                    					}
                    					set(
                    						current,
                    						"multiplicity",
                    						lv_multiplicity_5_0,
                    						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.MultiplicityRule");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDTML.g:309:3: (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalDTML.g:310:4: otherlv_6= '=' ( (lv_value_7_0= ruleValue ) )
                    {
                    otherlv_6=(Token)match(input,21,FOLLOW_10); 

                    				newLeafNode(otherlv_6, grammarAccess.getPropertyAccess().getEqualsSignKeyword_4_0());
                    			
                    // InternalDTML.g:314:4: ( (lv_value_7_0= ruleValue ) )
                    // InternalDTML.g:315:5: (lv_value_7_0= ruleValue )
                    {
                    // InternalDTML.g:315:5: (lv_value_7_0= ruleValue )
                    // InternalDTML.g:316:6: lv_value_7_0= ruleValue
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getValueValueParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_value_7_0=ruleValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.Value");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDTML.g:334:3: ( (lv_comment_8_0= RULE_VSL_COMMENT ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_VSL_COMMENT) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalDTML.g:335:4: (lv_comment_8_0= RULE_VSL_COMMENT )
                    {
                    // InternalDTML.g:335:4: (lv_comment_8_0= RULE_VSL_COMMENT )
                    // InternalDTML.g:336:5: lv_comment_8_0= RULE_VSL_COMMENT
                    {
                    lv_comment_8_0=(Token)match(input,RULE_VSL_COMMENT,FOLLOW_2); 

                    					newLeafNode(lv_comment_8_0, grammarAccess.getPropertyAccess().getCommentVSL_COMMENTTerminalRuleCall_5_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPropertyRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"comment",
                    						lv_comment_8_0,
                    						"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.VSL_COMMENT");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleXAssignment"
    // InternalDTML.g:356:1: entryRuleXAssignment returns [EObject current=null] : iv_ruleXAssignment= ruleXAssignment EOF ;
    public final EObject entryRuleXAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXAssignment = null;


        try {
            // InternalDTML.g:356:52: (iv_ruleXAssignment= ruleXAssignment EOF )
            // InternalDTML.g:357:2: iv_ruleXAssignment= ruleXAssignment EOF
            {
             newCompositeNode(grammarAccess.getXAssignmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXAssignment=ruleXAssignment();

            state._fsp--;

             current =iv_ruleXAssignment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXAssignment"


    // $ANTLR start "ruleXAssignment"
    // InternalDTML.g:363:1: ruleXAssignment returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpressionString ) ) otherlv_3= ';' ) ;
    public final EObject ruleXAssignment() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_expression_2_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:369:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpressionString ) ) otherlv_3= ';' ) )
            // InternalDTML.g:370:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpressionString ) ) otherlv_3= ';' )
            {
            // InternalDTML.g:370:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpressionString ) ) otherlv_3= ';' )
            // InternalDTML.g:371:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expression_2_0= ruleExpressionString ) ) otherlv_3= ';'
            {
            // InternalDTML.g:371:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDTML.g:372:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDTML.g:372:4: (lv_name_0_0= RULE_ID )
            // InternalDTML.g:373:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(lv_name_0_0, grammarAccess.getXAssignmentAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXAssignmentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,21,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getXAssignmentAccess().getEqualsSignKeyword_1());
            		
            // InternalDTML.g:393:3: ( (lv_expression_2_0= ruleExpressionString ) )
            // InternalDTML.g:394:4: (lv_expression_2_0= ruleExpressionString )
            {
            // InternalDTML.g:394:4: (lv_expression_2_0= ruleExpressionString )
            // InternalDTML.g:395:5: lv_expression_2_0= ruleExpressionString
            {

            					newCompositeNode(grammarAccess.getXAssignmentAccess().getExpressionExpressionStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_14);
            lv_expression_2_0=ruleExpressionString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getXAssignmentRule());
            					}
            					set(
            						current,
            						"expression",
            						lv_expression_2_0,
            						"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.ExpressionString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,22,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getXAssignmentAccess().getSemicolonKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXAssignment"


    // $ANTLR start "entryRuleExpressionString"
    // InternalDTML.g:420:1: entryRuleExpressionString returns [String current=null] : iv_ruleExpressionString= ruleExpressionString EOF ;
    public final String entryRuleExpressionString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleExpressionString = null;


        try {
            // InternalDTML.g:420:56: (iv_ruleExpressionString= ruleExpressionString EOF )
            // InternalDTML.g:421:2: iv_ruleExpressionString= ruleExpressionString EOF
            {
             newCompositeNode(grammarAccess.getExpressionStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpressionString=ruleExpressionString();

            state._fsp--;

             current =iv_ruleExpressionString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpressionString"


    // $ANTLR start "ruleExpressionString"
    // InternalDTML.g:427:1: ruleExpressionString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID | kw= '[' | kw= ']' | kw= '-' | kw= ',' | this_INT_5= RULE_INT | this_DOUBLE_6= RULE_DOUBLE )* ;
    public final AntlrDatatypeRuleToken ruleExpressionString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_INT_5=null;
        Token this_DOUBLE_6=null;


        	enterRule();

        try {
            // InternalDTML.g:433:2: ( (this_ID_0= RULE_ID | kw= '[' | kw= ']' | kw= '-' | kw= ',' | this_INT_5= RULE_INT | this_DOUBLE_6= RULE_DOUBLE )* )
            // InternalDTML.g:434:2: (this_ID_0= RULE_ID | kw= '[' | kw= ']' | kw= '-' | kw= ',' | this_INT_5= RULE_INT | this_DOUBLE_6= RULE_DOUBLE )*
            {
            // InternalDTML.g:434:2: (this_ID_0= RULE_ID | kw= '[' | kw= ']' | kw= '-' | kw= ',' | this_INT_5= RULE_INT | this_DOUBLE_6= RULE_DOUBLE )*
            loop9:
            do {
                int alt9=8;
                switch ( input.LA(1) ) {
                case RULE_ID:
                    {
                    alt9=1;
                    }
                    break;
                case 23:
                    {
                    alt9=2;
                    }
                    break;
                case 24:
                    {
                    alt9=3;
                    }
                    break;
                case 25:
                    {
                    alt9=4;
                    }
                    break;
                case 26:
                    {
                    alt9=5;
                    }
                    break;
                case RULE_INT:
                    {
                    alt9=6;
                    }
                    break;
                case RULE_DOUBLE:
                    {
                    alt9=7;
                    }
                    break;

                }

                switch (alt9) {
            	case 1 :
            	    // InternalDTML.g:435:3: this_ID_0= RULE_ID
            	    {
            	    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            	    			current.merge(this_ID_0);
            	    		

            	    			newLeafNode(this_ID_0, grammarAccess.getExpressionStringAccess().getIDTerminalRuleCall_0());
            	    		

            	    }
            	    break;
            	case 2 :
            	    // InternalDTML.g:443:3: kw= '['
            	    {
            	    kw=(Token)match(input,23,FOLLOW_15); 

            	    			current.merge(kw);
            	    			newLeafNode(kw, grammarAccess.getExpressionStringAccess().getLeftSquareBracketKeyword_1());
            	    		

            	    }
            	    break;
            	case 3 :
            	    // InternalDTML.g:449:3: kw= ']'
            	    {
            	    kw=(Token)match(input,24,FOLLOW_15); 

            	    			current.merge(kw);
            	    			newLeafNode(kw, grammarAccess.getExpressionStringAccess().getRightSquareBracketKeyword_2());
            	    		

            	    }
            	    break;
            	case 4 :
            	    // InternalDTML.g:455:3: kw= '-'
            	    {
            	    kw=(Token)match(input,25,FOLLOW_15); 

            	    			current.merge(kw);
            	    			newLeafNode(kw, grammarAccess.getExpressionStringAccess().getHyphenMinusKeyword_3());
            	    		

            	    }
            	    break;
            	case 5 :
            	    // InternalDTML.g:461:3: kw= ','
            	    {
            	    kw=(Token)match(input,26,FOLLOW_15); 

            	    			current.merge(kw);
            	    			newLeafNode(kw, grammarAccess.getExpressionStringAccess().getCommaKeyword_4());
            	    		

            	    }
            	    break;
            	case 6 :
            	    // InternalDTML.g:467:3: this_INT_5= RULE_INT
            	    {
            	    this_INT_5=(Token)match(input,RULE_INT,FOLLOW_15); 

            	    			current.merge(this_INT_5);
            	    		

            	    			newLeafNode(this_INT_5, grammarAccess.getExpressionStringAccess().getINTTerminalRuleCall_5());
            	    		

            	    }
            	    break;
            	case 7 :
            	    // InternalDTML.g:475:3: this_DOUBLE_6= RULE_DOUBLE
            	    {
            	    this_DOUBLE_6=(Token)match(input,RULE_DOUBLE,FOLLOW_15); 

            	    			current.merge(this_DOUBLE_6);
            	    		

            	    			newLeafNode(this_DOUBLE_6, grammarAccess.getExpressionStringAccess().getDOUBLETerminalRuleCall_6());
            	    		

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpressionString"


    // $ANTLR start "entryRuleValue"
    // InternalDTML.g:486:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalDTML.g:486:46: (iv_ruleValue= ruleValue EOF )
            // InternalDTML.g:487:2: iv_ruleValue= ruleValue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;

             current =iv_ruleValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalDTML.g:493:1: ruleValue returns [EObject current=null] : ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        Token lv_str_0_0=null;
        Token lv_ival_1_0=null;
        Token lv_dval_2_0=null;


        	enterRule();

        try {
            // InternalDTML.g:499:2: ( ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) ) )
            // InternalDTML.g:500:2: ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) )
            {
            // InternalDTML.g:500:2: ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) )
            int alt10=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt10=1;
                }
                break;
            case RULE_INT:
                {
                alt10=2;
                }
                break;
            case RULE_DOUBLE:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalDTML.g:501:3: ( (lv_str_0_0= RULE_STRING ) )
                    {
                    // InternalDTML.g:501:3: ( (lv_str_0_0= RULE_STRING ) )
                    // InternalDTML.g:502:4: (lv_str_0_0= RULE_STRING )
                    {
                    // InternalDTML.g:502:4: (lv_str_0_0= RULE_STRING )
                    // InternalDTML.g:503:5: lv_str_0_0= RULE_STRING
                    {
                    lv_str_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_str_0_0, grammarAccess.getValueAccess().getStrSTRINGTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"str",
                    						lv_str_0_0,
                    						"org.eclipse.papyrus.uml.alf.Common.STRING");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:520:3: ( (lv_ival_1_0= RULE_INT ) )
                    {
                    // InternalDTML.g:520:3: ( (lv_ival_1_0= RULE_INT ) )
                    // InternalDTML.g:521:4: (lv_ival_1_0= RULE_INT )
                    {
                    // InternalDTML.g:521:4: (lv_ival_1_0= RULE_INT )
                    // InternalDTML.g:522:5: lv_ival_1_0= RULE_INT
                    {
                    lv_ival_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    					newLeafNode(lv_ival_1_0, grammarAccess.getValueAccess().getIvalINTTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"ival",
                    						lv_ival_1_0,
                    						"org.eclipse.papyrus.uml.alf.Common.INT");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalDTML.g:539:3: ( (lv_dval_2_0= RULE_DOUBLE ) )
                    {
                    // InternalDTML.g:539:3: ( (lv_dval_2_0= RULE_DOUBLE ) )
                    // InternalDTML.g:540:4: (lv_dval_2_0= RULE_DOUBLE )
                    {
                    // InternalDTML.g:540:4: (lv_dval_2_0= RULE_DOUBLE )
                    // InternalDTML.g:541:5: lv_dval_2_0= RULE_DOUBLE
                    {
                    lv_dval_2_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); 

                    					newLeafNode(lv_dval_2_0, grammarAccess.getValueAccess().getDvalDOUBLETerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"dval",
                    						lv_dval_2_0,
                    						"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.DOUBLE");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleXEnumeration"
    // InternalDTML.g:561:1: entryRuleXEnumeration returns [EObject current=null] : iv_ruleXEnumeration= ruleXEnumeration EOF ;
    public final EObject entryRuleXEnumeration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXEnumeration = null;


        try {
            // InternalDTML.g:561:53: (iv_ruleXEnumeration= ruleXEnumeration EOF )
            // InternalDTML.g:562:2: iv_ruleXEnumeration= ruleXEnumeration EOF
            {
             newCompositeNode(grammarAccess.getXEnumerationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXEnumeration=ruleXEnumeration();

            state._fsp--;

             current =iv_ruleXEnumeration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXEnumeration"


    // $ANTLR start "ruleXEnumeration"
    // InternalDTML.g:568:1: ruleXEnumeration returns [EObject current=null] : (otherlv_0= 'Enumeration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_literals_3_0= ruleXEnumLiteral ) )* otherlv_4= '}' ) ;
    public final EObject ruleXEnumeration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_literals_3_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:574:2: ( (otherlv_0= 'Enumeration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_literals_3_0= ruleXEnumLiteral ) )* otherlv_4= '}' ) )
            // InternalDTML.g:575:2: (otherlv_0= 'Enumeration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_literals_3_0= ruleXEnumLiteral ) )* otherlv_4= '}' )
            {
            // InternalDTML.g:575:2: (otherlv_0= 'Enumeration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_literals_3_0= ruleXEnumLiteral ) )* otherlv_4= '}' )
            // InternalDTML.g:576:3: otherlv_0= 'Enumeration' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_literals_3_0= ruleXEnumLiteral ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getXEnumerationAccess().getEnumerationKeyword_0());
            		
            // InternalDTML.g:580:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDTML.g:581:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDTML.g:581:4: (lv_name_1_0= RULE_ID )
            // InternalDTML.g:582:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getXEnumerationAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXEnumerationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getXEnumerationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDTML.g:602:3: ( (lv_literals_3_0= ruleXEnumLiteral ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalDTML.g:603:4: (lv_literals_3_0= ruleXEnumLiteral )
            	    {
            	    // InternalDTML.g:603:4: (lv_literals_3_0= ruleXEnumLiteral )
            	    // InternalDTML.g:604:5: lv_literals_3_0= ruleXEnumLiteral
            	    {

            	    					newCompositeNode(grammarAccess.getXEnumerationAccess().getLiteralsXEnumLiteralParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_16);
            	    lv_literals_3_0=ruleXEnumLiteral();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXEnumerationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"literals",
            	    						lv_literals_3_0,
            	    						"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.XEnumLiteral");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getXEnumerationAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXEnumeration"


    // $ANTLR start "entryRuleXEnumLiteral"
    // InternalDTML.g:629:1: entryRuleXEnumLiteral returns [EObject current=null] : iv_ruleXEnumLiteral= ruleXEnumLiteral EOF ;
    public final EObject entryRuleXEnumLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXEnumLiteral = null;


        try {
            // InternalDTML.g:629:53: (iv_ruleXEnumLiteral= ruleXEnumLiteral EOF )
            // InternalDTML.g:630:2: iv_ruleXEnumLiteral= ruleXEnumLiteral EOF
            {
             newCompositeNode(grammarAccess.getXEnumLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXEnumLiteral=ruleXEnumLiteral();

            state._fsp--;

             current =iv_ruleXEnumLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXEnumLiteral"


    // $ANTLR start "ruleXEnumLiteral"
    // InternalDTML.g:636:1: ruleXEnumLiteral returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleValue ) ) )? ( (lv_comment_3_0= RULE_VSL_COMMENT ) )? ) ;
    public final EObject ruleXEnumLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_comment_3_0=null;
        EObject lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:642:2: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleValue ) ) )? ( (lv_comment_3_0= RULE_VSL_COMMENT ) )? ) )
            // InternalDTML.g:643:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleValue ) ) )? ( (lv_comment_3_0= RULE_VSL_COMMENT ) )? )
            {
            // InternalDTML.g:643:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleValue ) ) )? ( (lv_comment_3_0= RULE_VSL_COMMENT ) )? )
            // InternalDTML.g:644:3: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '=' ( (lv_value_2_0= ruleValue ) ) )? ( (lv_comment_3_0= RULE_VSL_COMMENT ) )?
            {
            // InternalDTML.g:644:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalDTML.g:645:4: (lv_name_0_0= RULE_ID )
            {
            // InternalDTML.g:645:4: (lv_name_0_0= RULE_ID )
            // InternalDTML.g:646:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_0_0, grammarAccess.getXEnumLiteralAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXEnumLiteralRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            // InternalDTML.g:662:3: (otherlv_1= '=' ( (lv_value_2_0= ruleValue ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==21) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDTML.g:663:4: otherlv_1= '=' ( (lv_value_2_0= ruleValue ) )
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_10); 

                    				newLeafNode(otherlv_1, grammarAccess.getXEnumLiteralAccess().getEqualsSignKeyword_1_0());
                    			
                    // InternalDTML.g:667:4: ( (lv_value_2_0= ruleValue ) )
                    // InternalDTML.g:668:5: (lv_value_2_0= ruleValue )
                    {
                    // InternalDTML.g:668:5: (lv_value_2_0= ruleValue )
                    // InternalDTML.g:669:6: lv_value_2_0= ruleValue
                    {

                    						newCompositeNode(grammarAccess.getXEnumLiteralAccess().getValueValueParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_value_2_0=ruleValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getXEnumLiteralRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_2_0,
                    							"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.Value");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDTML.g:687:3: ( (lv_comment_3_0= RULE_VSL_COMMENT ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_VSL_COMMENT) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalDTML.g:688:4: (lv_comment_3_0= RULE_VSL_COMMENT )
                    {
                    // InternalDTML.g:688:4: (lv_comment_3_0= RULE_VSL_COMMENT )
                    // InternalDTML.g:689:5: lv_comment_3_0= RULE_VSL_COMMENT
                    {
                    lv_comment_3_0=(Token)match(input,RULE_VSL_COMMENT,FOLLOW_2); 

                    					newLeafNode(lv_comment_3_0, grammarAccess.getXEnumLiteralAccess().getCommentVSL_COMMENTTerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getXEnumLiteralRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"comment",
                    						lv_comment_3_0,
                    						"org.eclipse.papyrus.robotics.xtext.datatypes.DTML.VSL_COMMENT");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXEnumLiteral"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDTML.g:709:1: entryRuleQualifiedName returns [EObject current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final EObject entryRuleQualifiedName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQualifiedName = null;


        try {
            // InternalDTML.g:709:54: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalDTML.g:710:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDTML.g:716:1: ruleQualifiedName returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? ) ;
    public final EObject ruleQualifiedName() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_remaining_2_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:722:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? ) )
            // InternalDTML.g:723:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? )
            {
            // InternalDTML.g:723:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? )
            // InternalDTML.g:724:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )?
            {
            // InternalDTML.g:724:3: ( (otherlv_0= RULE_ID ) )
            // InternalDTML.g:725:4: (otherlv_0= RULE_ID )
            {
            // InternalDTML.g:725:4: (otherlv_0= RULE_ID )
            // InternalDTML.g:726:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQualifiedNameRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_17); 

            					newLeafNode(otherlv_0, grammarAccess.getQualifiedNameAccess().getPathNamespaceCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_18); 

            			newLeafNode(otherlv_1, grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1());
            		
            // InternalDTML.g:741:3: ( (lv_remaining_2_0= ruleQualifiedName ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID) ) {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==28) ) {
                    alt14=1;
                }
            }
            switch (alt14) {
                case 1 :
                    // InternalDTML.g:742:4: (lv_remaining_2_0= ruleQualifiedName )
                    {
                    // InternalDTML.g:742:4: (lv_remaining_2_0= ruleQualifiedName )
                    // InternalDTML.g:743:5: lv_remaining_2_0= ruleQualifiedName
                    {

                    					newCompositeNode(grammarAccess.getQualifiedNameAccess().getRemainingQualifiedNameParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_remaining_2_0=ruleQualifiedName();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getQualifiedNameRule());
                    					}
                    					set(
                    						current,
                    						"remaining",
                    						lv_remaining_2_0,
                    						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.QualifiedName");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleTypeRule"
    // InternalDTML.g:764:1: entryRuleTypeRule returns [EObject current=null] : iv_ruleTypeRule= ruleTypeRule EOF ;
    public final EObject entryRuleTypeRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeRule = null;


        try {
            // InternalDTML.g:764:49: (iv_ruleTypeRule= ruleTypeRule EOF )
            // InternalDTML.g:765:2: iv_ruleTypeRule= ruleTypeRule EOF
            {
             newCompositeNode(grammarAccess.getTypeRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeRule=ruleTypeRule();

            state._fsp--;

             current =iv_ruleTypeRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeRule"


    // $ANTLR start "ruleTypeRule"
    // InternalDTML.g:771:1: ruleTypeRule returns [EObject current=null] : ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleTypeRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_path_0_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:777:2: ( ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) ) )
            // InternalDTML.g:778:2: ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalDTML.g:778:2: ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) )
            // InternalDTML.g:779:3: ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) )
            {
            // InternalDTML.g:779:3: ( (lv_path_0_0= ruleQualifiedName ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_ID) ) {
                int LA15_1 = input.LA(2);

                if ( (LA15_1==28) ) {
                    alt15=1;
                }
            }
            switch (alt15) {
                case 1 :
                    // InternalDTML.g:780:4: (lv_path_0_0= ruleQualifiedName )
                    {
                    // InternalDTML.g:780:4: (lv_path_0_0= ruleQualifiedName )
                    // InternalDTML.g:781:5: lv_path_0_0= ruleQualifiedName
                    {

                    					newCompositeNode(grammarAccess.getTypeRuleAccess().getPathQualifiedNameParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_path_0_0=ruleQualifiedName();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTypeRuleRule());
                    					}
                    					set(
                    						current,
                    						"path",
                    						lv_path_0_0,
                    						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.QualifiedName");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDTML.g:798:3: ( (otherlv_1= RULE_ID ) )
            // InternalDTML.g:799:4: (otherlv_1= RULE_ID )
            {
            // InternalDTML.g:799:4: (otherlv_1= RULE_ID )
            // InternalDTML.g:800:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTypeRuleRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getTypeRuleAccess().getTypeTypeCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeRule"


    // $ANTLR start "entryRuleMultiplicityRule"
    // InternalDTML.g:815:1: entryRuleMultiplicityRule returns [EObject current=null] : iv_ruleMultiplicityRule= ruleMultiplicityRule EOF ;
    public final EObject entryRuleMultiplicityRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicityRule = null;


        try {
            // InternalDTML.g:815:57: (iv_ruleMultiplicityRule= ruleMultiplicityRule EOF )
            // InternalDTML.g:816:2: iv_ruleMultiplicityRule= ruleMultiplicityRule EOF
            {
             newCompositeNode(grammarAccess.getMultiplicityRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultiplicityRule=ruleMultiplicityRule();

            state._fsp--;

             current =iv_ruleMultiplicityRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicityRule"


    // $ANTLR start "ruleMultiplicityRule"
    // InternalDTML.g:822:1: ruleMultiplicityRule returns [EObject current=null] : (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' ) ;
    public final EObject ruleMultiplicityRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_bounds_1_0 = null;

        EObject lv_bounds_3_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:828:2: ( (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' ) )
            // InternalDTML.g:829:2: (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' )
            {
            // InternalDTML.g:829:2: (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' )
            // InternalDTML.g:830:3: otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getMultiplicityRuleAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalDTML.g:834:3: ( (lv_bounds_1_0= ruleBoundSpecification ) )
            // InternalDTML.g:835:4: (lv_bounds_1_0= ruleBoundSpecification )
            {
            // InternalDTML.g:835:4: (lv_bounds_1_0= ruleBoundSpecification )
            // InternalDTML.g:836:5: lv_bounds_1_0= ruleBoundSpecification
            {

            					newCompositeNode(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_20);
            lv_bounds_1_0=ruleBoundSpecification();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMultiplicityRuleRule());
            					}
            					add(
            						current,
            						"bounds",
            						lv_bounds_1_0,
            						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.BoundSpecification");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDTML.g:853:3: (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==29) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDTML.g:854:4: otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) )
                    {
                    otherlv_2=(Token)match(input,29,FOLLOW_19); 

                    				newLeafNode(otherlv_2, grammarAccess.getMultiplicityRuleAccess().getFullStopFullStopKeyword_2_0());
                    			
                    // InternalDTML.g:858:4: ( (lv_bounds_3_0= ruleBoundSpecification ) )
                    // InternalDTML.g:859:5: (lv_bounds_3_0= ruleBoundSpecification )
                    {
                    // InternalDTML.g:859:5: (lv_bounds_3_0= ruleBoundSpecification )
                    // InternalDTML.g:860:6: lv_bounds_3_0= ruleBoundSpecification
                    {

                    						newCompositeNode(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_bounds_3_0=ruleBoundSpecification();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getMultiplicityRuleRule());
                    						}
                    						add(
                    							current,
                    							"bounds",
                    							lv_bounds_3_0,
                    							"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.BoundSpecification");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,24,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getMultiplicityRuleAccess().getRightSquareBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicityRule"


    // $ANTLR start "entryRuleBoundSpecification"
    // InternalDTML.g:886:1: entryRuleBoundSpecification returns [EObject current=null] : iv_ruleBoundSpecification= ruleBoundSpecification EOF ;
    public final EObject entryRuleBoundSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoundSpecification = null;


        try {
            // InternalDTML.g:886:59: (iv_ruleBoundSpecification= ruleBoundSpecification EOF )
            // InternalDTML.g:887:2: iv_ruleBoundSpecification= ruleBoundSpecification EOF
            {
             newCompositeNode(grammarAccess.getBoundSpecificationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoundSpecification=ruleBoundSpecification();

            state._fsp--;

             current =iv_ruleBoundSpecification; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoundSpecification"


    // $ANTLR start "ruleBoundSpecification"
    // InternalDTML.g:893:1: ruleBoundSpecification returns [EObject current=null] : ( (lv_value_0_0= ruleUnlimitedLiteral ) ) ;
    public final EObject ruleBoundSpecification() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;



        	enterRule();

        try {
            // InternalDTML.g:899:2: ( ( (lv_value_0_0= ruleUnlimitedLiteral ) ) )
            // InternalDTML.g:900:2: ( (lv_value_0_0= ruleUnlimitedLiteral ) )
            {
            // InternalDTML.g:900:2: ( (lv_value_0_0= ruleUnlimitedLiteral ) )
            // InternalDTML.g:901:3: (lv_value_0_0= ruleUnlimitedLiteral )
            {
            // InternalDTML.g:901:3: (lv_value_0_0= ruleUnlimitedLiteral )
            // InternalDTML.g:902:4: lv_value_0_0= ruleUnlimitedLiteral
            {

            				newCompositeNode(grammarAccess.getBoundSpecificationAccess().getValueUnlimitedLiteralParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_value_0_0=ruleUnlimitedLiteral();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getBoundSpecificationRule());
            				}
            				set(
            					current,
            					"value",
            					lv_value_0_0,
            					"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.UnlimitedLiteral");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoundSpecification"


    // $ANTLR start "entryRuleUnlimitedLiteral"
    // InternalDTML.g:922:1: entryRuleUnlimitedLiteral returns [String current=null] : iv_ruleUnlimitedLiteral= ruleUnlimitedLiteral EOF ;
    public final String entryRuleUnlimitedLiteral() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleUnlimitedLiteral = null;


        try {
            // InternalDTML.g:922:56: (iv_ruleUnlimitedLiteral= ruleUnlimitedLiteral EOF )
            // InternalDTML.g:923:2: iv_ruleUnlimitedLiteral= ruleUnlimitedLiteral EOF
            {
             newCompositeNode(grammarAccess.getUnlimitedLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnlimitedLiteral=ruleUnlimitedLiteral();

            state._fsp--;

             current =iv_ruleUnlimitedLiteral.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnlimitedLiteral"


    // $ANTLR start "ruleUnlimitedLiteral"
    // InternalDTML.g:929:1: ruleUnlimitedLiteral returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT | kw= '*' ) ;
    public final AntlrDatatypeRuleToken ruleUnlimitedLiteral() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;


        	enterRule();

        try {
            // InternalDTML.g:935:2: ( (this_INT_0= RULE_INT | kw= '*' ) )
            // InternalDTML.g:936:2: (this_INT_0= RULE_INT | kw= '*' )
            {
            // InternalDTML.g:936:2: (this_INT_0= RULE_INT | kw= '*' )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_INT) ) {
                alt17=1;
            }
            else if ( (LA17_0==30) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalDTML.g:937:3: this_INT_0= RULE_INT
                    {
                    this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    			current.merge(this_INT_0);
                    		

                    			newLeafNode(this_INT_0, grammarAccess.getUnlimitedLiteralAccess().getINTTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDTML.g:945:3: kw= '*'
                    {
                    kw=(Token)match(input,30,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getUnlimitedLiteralAccess().getAsteriskKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnlimitedLiteral"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000048000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000A80022L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000100010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000A00022L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200022L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000001C0L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000007C000D0L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x00000000078000D2L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000040000040L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000021000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000001000000L});

}