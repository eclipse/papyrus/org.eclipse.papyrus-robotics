/*
 * generated by Xtext 2.19.0
 */
package org.eclipse.papyrus.robotics.xtext.datatypes


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class DTMLStandaloneSetup extends DTMLStandaloneSetupGenerated {

	def static void doSetup() {
		new DTMLStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
