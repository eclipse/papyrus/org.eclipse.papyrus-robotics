package org.eclipse.papyrus.robotics.xtext.compdef.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.gmfdiag.extensionpoints.editors.configuration.IDirectEditorConstraint;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;

public class CompDefEditorConstraint implements IDirectEditorConstraint {

	/**
	 * @see org.eclipse.papyrus.extensionpoints.editors.configuration.IDirectEditorConstraint#getLabel()
	 *
	 * @return label
	 */
	public String getLabel() {
		return "CompDef"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.papyrus.extensionpoints.editors.configuration.IDirectEditorConstraint#appliesTo(java.lang.Object)
	 *
	 * @param selection
	 * @return true, if selection corresponds to a Parameter
	 */
	public boolean appliesTo(Object selection) {
		EObject resolvedEObject = EMFHelper.getEObject(selection);
		if (resolvedEObject instanceof Class) {
			Class compDef = (Class) resolvedEObject;
			if (StereotypeUtil.isApplied(compDef, ComponentDefinition.class)) {
				return true;
			}
		}
		return false;
	}
}
