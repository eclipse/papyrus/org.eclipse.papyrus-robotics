/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
 
 package org.eclipse.papyrus.robotics.xtext.compdef.ui.contribution

import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.Property
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity
import org.eclipse.uml2.uml.util.UMLUtil

class UnparseCompDef {
	
	def static getCompDefText(Class clazz) '''
		component «clazz.name» {
			«FOR port : clazz.ownedPorts»
				port «port.name» «port.provided»«port.required»
			«ENDFOR»
			«FOR attribute : clazz.ownedAttributes»
				«val  type = attribute.type»
				«IF type !== null && StereotypeUtil.isApplied(type, Activity)»
					activity «type.name»«attribute.defaultValue»
				«ENDIF»
			«ENDFOR»
			«val paramClass = ParameterUtils.getParameterClass(clazz)»
			«IF paramClass !== null»
				«FOR parameter : paramClass.attributes»
					parameter «parameter.name»«parameter.typeStr»«parameter.defaultValueStr»«parameter.getComment»
				«ENDFOR»
			«ENDIF»
		}
	'''
	
	def static String getProvided(Port port) {
		val provideds = port.getProvideds()
		if (provideds.size() > 0) {
			val provided = provideds.get(0)
			if (provided !== null && provided.getName() !== null) {
				return '''provides «provided.qualifiedName»'''
			}
		}
		return ""
	}

	def static String getRequired(Port port) {
		val requireds = port.getRequireds()
		if (requireds.size() > 0) {
			val required = requireds.get(0)
			if (required !== null && required.getName() !== null) {
				// add space, if provided is non empty
				return '''«IF port.provided.length > 0» «ENDIF»requires «required.qualifiedName»'''
			}
		}
		return ""
	}

	def static getDefaultValueStr(Property attribute) {
		val value = attribute.getDefaultValue()
		if (value !== null) {
			var strVal = value.stringValue()
			if (strVal !== null) {
				// quote all non numeric values.
				if (!(strVal.charAt(0) >= '0' && strVal.charAt(0) <= '9')) {
					strVal = "\"" + strVal + "\""
				}
				return " = " + strVal
			}
		}
		return ""
	}

	def static getTypeStr(Property attribute) {
		if (attribute.type !== null) {
			return ": " + attribute.type.qualifiedName
		}
		return ""
	}


	def static getComment(Property parameter) {
		val entity = UMLUtil.getStereotypeApplication(parameter, Entity)
		if (entity !== null && entity.getDescription !== null) {
			return "	// " + entity.getDescription()
		}
		else return ""
	}
		
}