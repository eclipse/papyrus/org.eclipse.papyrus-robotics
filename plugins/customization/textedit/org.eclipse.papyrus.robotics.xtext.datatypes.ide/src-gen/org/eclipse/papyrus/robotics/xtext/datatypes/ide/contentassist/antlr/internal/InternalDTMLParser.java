package org.eclipse.papyrus.robotics.xtext.datatypes.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.eclipse.papyrus.robotics.xtext.datatypes.services.DTMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDTMLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_DOUBLE", "RULE_VSL_COMMENT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_INTEGER_VALUE", "RULE_WS", "RULE_ANY_OTHER", "'DataType'", "'CommObject'", "'['", "']'", "'-'", "','", "'*'", "'{'", "'}'", "'attribute'", "':'", "'='", "';'", "'Enumeration'", "'::'", "'..'", "'<Undefined>'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int RULE_DOUBLE=6;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int RULE_ID=4;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_VSL_COMMENT=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_INTEGER_VALUE=11;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDTMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDTMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDTMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDTML.g"; }


    	private DTMLGrammarAccess grammarAccess;

    	public void setGrammarAccess(DTMLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalDTML.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalDTML.g:54:1: ( ruleModel EOF )
            // InternalDTML.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalDTML.g:62:1: ruleModel : ( ( rule__Model__Alternatives ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:66:2: ( ( ( rule__Model__Alternatives ) ) )
            // InternalDTML.g:67:2: ( ( rule__Model__Alternatives ) )
            {
            // InternalDTML.g:67:2: ( ( rule__Model__Alternatives ) )
            // InternalDTML.g:68:3: ( rule__Model__Alternatives )
            {
             before(grammarAccess.getModelAccess().getAlternatives()); 
            // InternalDTML.g:69:3: ( rule__Model__Alternatives )
            // InternalDTML.g:69:4: rule__Model__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Model__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleXDataType"
    // InternalDTML.g:78:1: entryRuleXDataType : ruleXDataType EOF ;
    public final void entryRuleXDataType() throws RecognitionException {
        try {
            // InternalDTML.g:79:1: ( ruleXDataType EOF )
            // InternalDTML.g:80:1: ruleXDataType EOF
            {
             before(grammarAccess.getXDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleXDataType();

            state._fsp--;

             after(grammarAccess.getXDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXDataType"


    // $ANTLR start "ruleXDataType"
    // InternalDTML.g:87:1: ruleXDataType : ( ( rule__XDataType__Group__0 ) ) ;
    public final void ruleXDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:91:2: ( ( ( rule__XDataType__Group__0 ) ) )
            // InternalDTML.g:92:2: ( ( rule__XDataType__Group__0 ) )
            {
            // InternalDTML.g:92:2: ( ( rule__XDataType__Group__0 ) )
            // InternalDTML.g:93:3: ( rule__XDataType__Group__0 )
            {
             before(grammarAccess.getXDataTypeAccess().getGroup()); 
            // InternalDTML.g:94:3: ( rule__XDataType__Group__0 )
            // InternalDTML.g:94:4: rule__XDataType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XDataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXDataType"


    // $ANTLR start "entryRuleDTKey"
    // InternalDTML.g:103:1: entryRuleDTKey : ruleDTKey EOF ;
    public final void entryRuleDTKey() throws RecognitionException {
        try {
            // InternalDTML.g:104:1: ( ruleDTKey EOF )
            // InternalDTML.g:105:1: ruleDTKey EOF
            {
             before(grammarAccess.getDTKeyRule()); 
            pushFollow(FOLLOW_1);
            ruleDTKey();

            state._fsp--;

             after(grammarAccess.getDTKeyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDTKey"


    // $ANTLR start "ruleDTKey"
    // InternalDTML.g:112:1: ruleDTKey : ( ( rule__DTKey__Alternatives ) ) ;
    public final void ruleDTKey() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:116:2: ( ( ( rule__DTKey__Alternatives ) ) )
            // InternalDTML.g:117:2: ( ( rule__DTKey__Alternatives ) )
            {
            // InternalDTML.g:117:2: ( ( rule__DTKey__Alternatives ) )
            // InternalDTML.g:118:3: ( rule__DTKey__Alternatives )
            {
             before(grammarAccess.getDTKeyAccess().getAlternatives()); 
            // InternalDTML.g:119:3: ( rule__DTKey__Alternatives )
            // InternalDTML.g:119:4: rule__DTKey__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DTKey__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDTKeyAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDTKey"


    // $ANTLR start "entryRuleProperty"
    // InternalDTML.g:128:1: entryRuleProperty : ruleProperty EOF ;
    public final void entryRuleProperty() throws RecognitionException {
        try {
            // InternalDTML.g:129:1: ( ruleProperty EOF )
            // InternalDTML.g:130:1: ruleProperty EOF
            {
             before(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalDTML.g:137:1: ruleProperty : ( ( rule__Property__Group__0 ) ) ;
    public final void ruleProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:141:2: ( ( ( rule__Property__Group__0 ) ) )
            // InternalDTML.g:142:2: ( ( rule__Property__Group__0 ) )
            {
            // InternalDTML.g:142:2: ( ( rule__Property__Group__0 ) )
            // InternalDTML.g:143:3: ( rule__Property__Group__0 )
            {
             before(grammarAccess.getPropertyAccess().getGroup()); 
            // InternalDTML.g:144:3: ( rule__Property__Group__0 )
            // InternalDTML.g:144:4: rule__Property__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleXAssignment"
    // InternalDTML.g:153:1: entryRuleXAssignment : ruleXAssignment EOF ;
    public final void entryRuleXAssignment() throws RecognitionException {
        try {
            // InternalDTML.g:154:1: ( ruleXAssignment EOF )
            // InternalDTML.g:155:1: ruleXAssignment EOF
            {
             before(grammarAccess.getXAssignmentRule()); 
            pushFollow(FOLLOW_1);
            ruleXAssignment();

            state._fsp--;

             after(grammarAccess.getXAssignmentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXAssignment"


    // $ANTLR start "ruleXAssignment"
    // InternalDTML.g:162:1: ruleXAssignment : ( ( rule__XAssignment__Group__0 ) ) ;
    public final void ruleXAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:166:2: ( ( ( rule__XAssignment__Group__0 ) ) )
            // InternalDTML.g:167:2: ( ( rule__XAssignment__Group__0 ) )
            {
            // InternalDTML.g:167:2: ( ( rule__XAssignment__Group__0 ) )
            // InternalDTML.g:168:3: ( rule__XAssignment__Group__0 )
            {
             before(grammarAccess.getXAssignmentAccess().getGroup()); 
            // InternalDTML.g:169:3: ( rule__XAssignment__Group__0 )
            // InternalDTML.g:169:4: rule__XAssignment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XAssignment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXAssignmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXAssignment"


    // $ANTLR start "entryRuleExpressionString"
    // InternalDTML.g:178:1: entryRuleExpressionString : ruleExpressionString EOF ;
    public final void entryRuleExpressionString() throws RecognitionException {
        try {
            // InternalDTML.g:179:1: ( ruleExpressionString EOF )
            // InternalDTML.g:180:1: ruleExpressionString EOF
            {
             before(grammarAccess.getExpressionStringRule()); 
            pushFollow(FOLLOW_1);
            ruleExpressionString();

            state._fsp--;

             after(grammarAccess.getExpressionStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpressionString"


    // $ANTLR start "ruleExpressionString"
    // InternalDTML.g:187:1: ruleExpressionString : ( ( rule__ExpressionString__Alternatives )* ) ;
    public final void ruleExpressionString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:191:2: ( ( ( rule__ExpressionString__Alternatives )* ) )
            // InternalDTML.g:192:2: ( ( rule__ExpressionString__Alternatives )* )
            {
            // InternalDTML.g:192:2: ( ( rule__ExpressionString__Alternatives )* )
            // InternalDTML.g:193:3: ( rule__ExpressionString__Alternatives )*
            {
             before(grammarAccess.getExpressionStringAccess().getAlternatives()); 
            // InternalDTML.g:194:3: ( rule__ExpressionString__Alternatives )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_ID && LA1_0<=RULE_DOUBLE)||(LA1_0>=16 && LA1_0<=19)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDTML.g:194:4: rule__ExpressionString__Alternatives
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__ExpressionString__Alternatives();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getExpressionStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpressionString"


    // $ANTLR start "entryRuleValue"
    // InternalDTML.g:203:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // InternalDTML.g:204:1: ( ruleValue EOF )
            // InternalDTML.g:205:1: ruleValue EOF
            {
             before(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalDTML.g:212:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:216:2: ( ( ( rule__Value__Alternatives ) ) )
            // InternalDTML.g:217:2: ( ( rule__Value__Alternatives ) )
            {
            // InternalDTML.g:217:2: ( ( rule__Value__Alternatives ) )
            // InternalDTML.g:218:3: ( rule__Value__Alternatives )
            {
             before(grammarAccess.getValueAccess().getAlternatives()); 
            // InternalDTML.g:219:3: ( rule__Value__Alternatives )
            // InternalDTML.g:219:4: rule__Value__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Value__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRuleXEnumeration"
    // InternalDTML.g:228:1: entryRuleXEnumeration : ruleXEnumeration EOF ;
    public final void entryRuleXEnumeration() throws RecognitionException {
        try {
            // InternalDTML.g:229:1: ( ruleXEnumeration EOF )
            // InternalDTML.g:230:1: ruleXEnumeration EOF
            {
             before(grammarAccess.getXEnumerationRule()); 
            pushFollow(FOLLOW_1);
            ruleXEnumeration();

            state._fsp--;

             after(grammarAccess.getXEnumerationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXEnumeration"


    // $ANTLR start "ruleXEnumeration"
    // InternalDTML.g:237:1: ruleXEnumeration : ( ( rule__XEnumeration__Group__0 ) ) ;
    public final void ruleXEnumeration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:241:2: ( ( ( rule__XEnumeration__Group__0 ) ) )
            // InternalDTML.g:242:2: ( ( rule__XEnumeration__Group__0 ) )
            {
            // InternalDTML.g:242:2: ( ( rule__XEnumeration__Group__0 ) )
            // InternalDTML.g:243:3: ( rule__XEnumeration__Group__0 )
            {
             before(grammarAccess.getXEnumerationAccess().getGroup()); 
            // InternalDTML.g:244:3: ( rule__XEnumeration__Group__0 )
            // InternalDTML.g:244:4: rule__XEnumeration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XEnumeration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXEnumerationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXEnumeration"


    // $ANTLR start "entryRuleXEnumLiteral"
    // InternalDTML.g:253:1: entryRuleXEnumLiteral : ruleXEnumLiteral EOF ;
    public final void entryRuleXEnumLiteral() throws RecognitionException {
        try {
            // InternalDTML.g:254:1: ( ruleXEnumLiteral EOF )
            // InternalDTML.g:255:1: ruleXEnumLiteral EOF
            {
             before(grammarAccess.getXEnumLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleXEnumLiteral();

            state._fsp--;

             after(grammarAccess.getXEnumLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXEnumLiteral"


    // $ANTLR start "ruleXEnumLiteral"
    // InternalDTML.g:262:1: ruleXEnumLiteral : ( ( rule__XEnumLiteral__Group__0 ) ) ;
    public final void ruleXEnumLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:266:2: ( ( ( rule__XEnumLiteral__Group__0 ) ) )
            // InternalDTML.g:267:2: ( ( rule__XEnumLiteral__Group__0 ) )
            {
            // InternalDTML.g:267:2: ( ( rule__XEnumLiteral__Group__0 ) )
            // InternalDTML.g:268:3: ( rule__XEnumLiteral__Group__0 )
            {
             before(grammarAccess.getXEnumLiteralAccess().getGroup()); 
            // InternalDTML.g:269:3: ( rule__XEnumLiteral__Group__0 )
            // InternalDTML.g:269:4: rule__XEnumLiteral__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXEnumLiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXEnumLiteral"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDTML.g:278:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalDTML.g:279:1: ( ruleQualifiedName EOF )
            // InternalDTML.g:280:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDTML.g:287:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:291:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalDTML.g:292:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalDTML.g:292:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalDTML.g:293:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalDTML.g:294:3: ( rule__QualifiedName__Group__0 )
            // InternalDTML.g:294:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleTypeRule"
    // InternalDTML.g:303:1: entryRuleTypeRule : ruleTypeRule EOF ;
    public final void entryRuleTypeRule() throws RecognitionException {
        try {
            // InternalDTML.g:304:1: ( ruleTypeRule EOF )
            // InternalDTML.g:305:1: ruleTypeRule EOF
            {
             before(grammarAccess.getTypeRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleTypeRule();

            state._fsp--;

             after(grammarAccess.getTypeRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTypeRule"


    // $ANTLR start "ruleTypeRule"
    // InternalDTML.g:312:1: ruleTypeRule : ( ( rule__TypeRule__Group__0 ) ) ;
    public final void ruleTypeRule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:316:2: ( ( ( rule__TypeRule__Group__0 ) ) )
            // InternalDTML.g:317:2: ( ( rule__TypeRule__Group__0 ) )
            {
            // InternalDTML.g:317:2: ( ( rule__TypeRule__Group__0 ) )
            // InternalDTML.g:318:3: ( rule__TypeRule__Group__0 )
            {
             before(grammarAccess.getTypeRuleAccess().getGroup()); 
            // InternalDTML.g:319:3: ( rule__TypeRule__Group__0 )
            // InternalDTML.g:319:4: rule__TypeRule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TypeRule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTypeRule"


    // $ANTLR start "entryRuleMultiplicityRule"
    // InternalDTML.g:328:1: entryRuleMultiplicityRule : ruleMultiplicityRule EOF ;
    public final void entryRuleMultiplicityRule() throws RecognitionException {
        try {
            // InternalDTML.g:329:1: ( ruleMultiplicityRule EOF )
            // InternalDTML.g:330:1: ruleMultiplicityRule EOF
            {
             before(grammarAccess.getMultiplicityRuleRule()); 
            pushFollow(FOLLOW_1);
            ruleMultiplicityRule();

            state._fsp--;

             after(grammarAccess.getMultiplicityRuleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiplicityRule"


    // $ANTLR start "ruleMultiplicityRule"
    // InternalDTML.g:337:1: ruleMultiplicityRule : ( ( rule__MultiplicityRule__Group__0 ) ) ;
    public final void ruleMultiplicityRule() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:341:2: ( ( ( rule__MultiplicityRule__Group__0 ) ) )
            // InternalDTML.g:342:2: ( ( rule__MultiplicityRule__Group__0 ) )
            {
            // InternalDTML.g:342:2: ( ( rule__MultiplicityRule__Group__0 ) )
            // InternalDTML.g:343:3: ( rule__MultiplicityRule__Group__0 )
            {
             before(grammarAccess.getMultiplicityRuleAccess().getGroup()); 
            // InternalDTML.g:344:3: ( rule__MultiplicityRule__Group__0 )
            // InternalDTML.g:344:4: rule__MultiplicityRule__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicityRuleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplicityRule"


    // $ANTLR start "entryRuleBoundSpecification"
    // InternalDTML.g:353:1: entryRuleBoundSpecification : ruleBoundSpecification EOF ;
    public final void entryRuleBoundSpecification() throws RecognitionException {
        try {
            // InternalDTML.g:354:1: ( ruleBoundSpecification EOF )
            // InternalDTML.g:355:1: ruleBoundSpecification EOF
            {
             before(grammarAccess.getBoundSpecificationRule()); 
            pushFollow(FOLLOW_1);
            ruleBoundSpecification();

            state._fsp--;

             after(grammarAccess.getBoundSpecificationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoundSpecification"


    // $ANTLR start "ruleBoundSpecification"
    // InternalDTML.g:362:1: ruleBoundSpecification : ( ( rule__BoundSpecification__ValueAssignment ) ) ;
    public final void ruleBoundSpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:366:2: ( ( ( rule__BoundSpecification__ValueAssignment ) ) )
            // InternalDTML.g:367:2: ( ( rule__BoundSpecification__ValueAssignment ) )
            {
            // InternalDTML.g:367:2: ( ( rule__BoundSpecification__ValueAssignment ) )
            // InternalDTML.g:368:3: ( rule__BoundSpecification__ValueAssignment )
            {
             before(grammarAccess.getBoundSpecificationAccess().getValueAssignment()); 
            // InternalDTML.g:369:3: ( rule__BoundSpecification__ValueAssignment )
            // InternalDTML.g:369:4: rule__BoundSpecification__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__BoundSpecification__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBoundSpecificationAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoundSpecification"


    // $ANTLR start "entryRuleUnlimitedLiteral"
    // InternalDTML.g:378:1: entryRuleUnlimitedLiteral : ruleUnlimitedLiteral EOF ;
    public final void entryRuleUnlimitedLiteral() throws RecognitionException {
        try {
            // InternalDTML.g:379:1: ( ruleUnlimitedLiteral EOF )
            // InternalDTML.g:380:1: ruleUnlimitedLiteral EOF
            {
             before(grammarAccess.getUnlimitedLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleUnlimitedLiteral();

            state._fsp--;

             after(grammarAccess.getUnlimitedLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnlimitedLiteral"


    // $ANTLR start "ruleUnlimitedLiteral"
    // InternalDTML.g:387:1: ruleUnlimitedLiteral : ( ( rule__UnlimitedLiteral__Alternatives ) ) ;
    public final void ruleUnlimitedLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:391:2: ( ( ( rule__UnlimitedLiteral__Alternatives ) ) )
            // InternalDTML.g:392:2: ( ( rule__UnlimitedLiteral__Alternatives ) )
            {
            // InternalDTML.g:392:2: ( ( rule__UnlimitedLiteral__Alternatives ) )
            // InternalDTML.g:393:3: ( rule__UnlimitedLiteral__Alternatives )
            {
             before(grammarAccess.getUnlimitedLiteralAccess().getAlternatives()); 
            // InternalDTML.g:394:3: ( rule__UnlimitedLiteral__Alternatives )
            // InternalDTML.g:394:4: rule__UnlimitedLiteral__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__UnlimitedLiteral__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnlimitedLiteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnlimitedLiteral"


    // $ANTLR start "rule__Model__Alternatives"
    // InternalDTML.g:402:1: rule__Model__Alternatives : ( ( ruleXDataType ) | ( ruleXAssignment ) | ( ruleXEnumeration ) );
    public final void rule__Model__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:406:1: ( ( ruleXDataType ) | ( ruleXAssignment ) | ( ruleXEnumeration ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 14:
            case 15:
                {
                alt2=1;
                }
                break;
            case RULE_ID:
                {
                alt2=2;
                }
                break;
            case 27:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalDTML.g:407:2: ( ruleXDataType )
                    {
                    // InternalDTML.g:407:2: ( ruleXDataType )
                    // InternalDTML.g:408:3: ruleXDataType
                    {
                     before(grammarAccess.getModelAccess().getXDataTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleXDataType();

                    state._fsp--;

                     after(grammarAccess.getModelAccess().getXDataTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:413:2: ( ruleXAssignment )
                    {
                    // InternalDTML.g:413:2: ( ruleXAssignment )
                    // InternalDTML.g:414:3: ruleXAssignment
                    {
                     before(grammarAccess.getModelAccess().getXAssignmentParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleXAssignment();

                    state._fsp--;

                     after(grammarAccess.getModelAccess().getXAssignmentParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDTML.g:419:2: ( ruleXEnumeration )
                    {
                    // InternalDTML.g:419:2: ( ruleXEnumeration )
                    // InternalDTML.g:420:3: ruleXEnumeration
                    {
                     before(grammarAccess.getModelAccess().getXEnumerationParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleXEnumeration();

                    state._fsp--;

                     after(grammarAccess.getModelAccess().getXEnumerationParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Alternatives"


    // $ANTLR start "rule__DTKey__Alternatives"
    // InternalDTML.g:429:1: rule__DTKey__Alternatives : ( ( 'DataType' ) | ( 'CommObject' ) );
    public final void rule__DTKey__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:433:1: ( ( 'DataType' ) | ( 'CommObject' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            else if ( (LA3_0==15) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDTML.g:434:2: ( 'DataType' )
                    {
                    // InternalDTML.g:434:2: ( 'DataType' )
                    // InternalDTML.g:435:3: 'DataType'
                    {
                     before(grammarAccess.getDTKeyAccess().getDataTypeKeyword_0()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getDTKeyAccess().getDataTypeKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:440:2: ( 'CommObject' )
                    {
                    // InternalDTML.g:440:2: ( 'CommObject' )
                    // InternalDTML.g:441:3: 'CommObject'
                    {
                     before(grammarAccess.getDTKeyAccess().getCommObjectKeyword_1()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getDTKeyAccess().getCommObjectKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DTKey__Alternatives"


    // $ANTLR start "rule__Property__Alternatives_2_1"
    // InternalDTML.g:450:1: rule__Property__Alternatives_2_1 : ( ( ( rule__Property__TypeAssignment_2_1_0 ) ) | ( ( rule__Property__TypeUndefinedAssignment_2_1_1 ) ) );
    public final void rule__Property__Alternatives_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:454:1: ( ( ( rule__Property__TypeAssignment_2_1_0 ) ) | ( ( rule__Property__TypeUndefinedAssignment_2_1_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==30) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalDTML.g:455:2: ( ( rule__Property__TypeAssignment_2_1_0 ) )
                    {
                    // InternalDTML.g:455:2: ( ( rule__Property__TypeAssignment_2_1_0 ) )
                    // InternalDTML.g:456:3: ( rule__Property__TypeAssignment_2_1_0 )
                    {
                     before(grammarAccess.getPropertyAccess().getTypeAssignment_2_1_0()); 
                    // InternalDTML.g:457:3: ( rule__Property__TypeAssignment_2_1_0 )
                    // InternalDTML.g:457:4: rule__Property__TypeAssignment_2_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__TypeAssignment_2_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPropertyAccess().getTypeAssignment_2_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:461:2: ( ( rule__Property__TypeUndefinedAssignment_2_1_1 ) )
                    {
                    // InternalDTML.g:461:2: ( ( rule__Property__TypeUndefinedAssignment_2_1_1 ) )
                    // InternalDTML.g:462:3: ( rule__Property__TypeUndefinedAssignment_2_1_1 )
                    {
                     before(grammarAccess.getPropertyAccess().getTypeUndefinedAssignment_2_1_1()); 
                    // InternalDTML.g:463:3: ( rule__Property__TypeUndefinedAssignment_2_1_1 )
                    // InternalDTML.g:463:4: rule__Property__TypeUndefinedAssignment_2_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__TypeUndefinedAssignment_2_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPropertyAccess().getTypeUndefinedAssignment_2_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Alternatives_2_1"


    // $ANTLR start "rule__ExpressionString__Alternatives"
    // InternalDTML.g:471:1: rule__ExpressionString__Alternatives : ( ( RULE_ID ) | ( '[' ) | ( ']' ) | ( '-' ) | ( ',' ) | ( RULE_INT ) | ( RULE_DOUBLE ) );
    public final void rule__ExpressionString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:475:1: ( ( RULE_ID ) | ( '[' ) | ( ']' ) | ( '-' ) | ( ',' ) | ( RULE_INT ) | ( RULE_DOUBLE ) )
            int alt5=7;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt5=1;
                }
                break;
            case 16:
                {
                alt5=2;
                }
                break;
            case 17:
                {
                alt5=3;
                }
                break;
            case 18:
                {
                alt5=4;
                }
                break;
            case 19:
                {
                alt5=5;
                }
                break;
            case RULE_INT:
                {
                alt5=6;
                }
                break;
            case RULE_DOUBLE:
                {
                alt5=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalDTML.g:476:2: ( RULE_ID )
                    {
                    // InternalDTML.g:476:2: ( RULE_ID )
                    // InternalDTML.g:477:3: RULE_ID
                    {
                     before(grammarAccess.getExpressionStringAccess().getIDTerminalRuleCall_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getIDTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:482:2: ( '[' )
                    {
                    // InternalDTML.g:482:2: ( '[' )
                    // InternalDTML.g:483:3: '['
                    {
                     before(grammarAccess.getExpressionStringAccess().getLeftSquareBracketKeyword_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getLeftSquareBracketKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDTML.g:488:2: ( ']' )
                    {
                    // InternalDTML.g:488:2: ( ']' )
                    // InternalDTML.g:489:3: ']'
                    {
                     before(grammarAccess.getExpressionStringAccess().getRightSquareBracketKeyword_2()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getRightSquareBracketKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDTML.g:494:2: ( '-' )
                    {
                    // InternalDTML.g:494:2: ( '-' )
                    // InternalDTML.g:495:3: '-'
                    {
                     before(grammarAccess.getExpressionStringAccess().getHyphenMinusKeyword_3()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getHyphenMinusKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDTML.g:500:2: ( ',' )
                    {
                    // InternalDTML.g:500:2: ( ',' )
                    // InternalDTML.g:501:3: ','
                    {
                     before(grammarAccess.getExpressionStringAccess().getCommaKeyword_4()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getCommaKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDTML.g:506:2: ( RULE_INT )
                    {
                    // InternalDTML.g:506:2: ( RULE_INT )
                    // InternalDTML.g:507:3: RULE_INT
                    {
                     before(grammarAccess.getExpressionStringAccess().getINTTerminalRuleCall_5()); 
                    match(input,RULE_INT,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getINTTerminalRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalDTML.g:512:2: ( RULE_DOUBLE )
                    {
                    // InternalDTML.g:512:2: ( RULE_DOUBLE )
                    // InternalDTML.g:513:3: RULE_DOUBLE
                    {
                     before(grammarAccess.getExpressionStringAccess().getDOUBLETerminalRuleCall_6()); 
                    match(input,RULE_DOUBLE,FOLLOW_2); 
                     after(grammarAccess.getExpressionStringAccess().getDOUBLETerminalRuleCall_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ExpressionString__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // InternalDTML.g:522:1: rule__Value__Alternatives : ( ( ( rule__Value__StrAssignment_0 ) ) | ( ( rule__Value__IvalAssignment_1 ) ) | ( ( rule__Value__DvalAssignment_2 ) ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:526:1: ( ( ( rule__Value__StrAssignment_0 ) ) | ( ( rule__Value__IvalAssignment_1 ) ) | ( ( rule__Value__DvalAssignment_2 ) ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt6=1;
                }
                break;
            case RULE_INT:
                {
                alt6=2;
                }
                break;
            case RULE_DOUBLE:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalDTML.g:527:2: ( ( rule__Value__StrAssignment_0 ) )
                    {
                    // InternalDTML.g:527:2: ( ( rule__Value__StrAssignment_0 ) )
                    // InternalDTML.g:528:3: ( rule__Value__StrAssignment_0 )
                    {
                     before(grammarAccess.getValueAccess().getStrAssignment_0()); 
                    // InternalDTML.g:529:3: ( rule__Value__StrAssignment_0 )
                    // InternalDTML.g:529:4: rule__Value__StrAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Value__StrAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getStrAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:533:2: ( ( rule__Value__IvalAssignment_1 ) )
                    {
                    // InternalDTML.g:533:2: ( ( rule__Value__IvalAssignment_1 ) )
                    // InternalDTML.g:534:3: ( rule__Value__IvalAssignment_1 )
                    {
                     before(grammarAccess.getValueAccess().getIvalAssignment_1()); 
                    // InternalDTML.g:535:3: ( rule__Value__IvalAssignment_1 )
                    // InternalDTML.g:535:4: rule__Value__IvalAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Value__IvalAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getIvalAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDTML.g:539:2: ( ( rule__Value__DvalAssignment_2 ) )
                    {
                    // InternalDTML.g:539:2: ( ( rule__Value__DvalAssignment_2 ) )
                    // InternalDTML.g:540:3: ( rule__Value__DvalAssignment_2 )
                    {
                     before(grammarAccess.getValueAccess().getDvalAssignment_2()); 
                    // InternalDTML.g:541:3: ( rule__Value__DvalAssignment_2 )
                    // InternalDTML.g:541:4: rule__Value__DvalAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Value__DvalAssignment_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getDvalAssignment_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__UnlimitedLiteral__Alternatives"
    // InternalDTML.g:549:1: rule__UnlimitedLiteral__Alternatives : ( ( RULE_INT ) | ( '*' ) );
    public final void rule__UnlimitedLiteral__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:553:1: ( ( RULE_INT ) | ( '*' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_INT) ) {
                alt7=1;
            }
            else if ( (LA7_0==20) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalDTML.g:554:2: ( RULE_INT )
                    {
                    // InternalDTML.g:554:2: ( RULE_INT )
                    // InternalDTML.g:555:3: RULE_INT
                    {
                     before(grammarAccess.getUnlimitedLiteralAccess().getINTTerminalRuleCall_0()); 
                    match(input,RULE_INT,FOLLOW_2); 
                     after(grammarAccess.getUnlimitedLiteralAccess().getINTTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDTML.g:560:2: ( '*' )
                    {
                    // InternalDTML.g:560:2: ( '*' )
                    // InternalDTML.g:561:3: '*'
                    {
                     before(grammarAccess.getUnlimitedLiteralAccess().getAsteriskKeyword_1()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getUnlimitedLiteralAccess().getAsteriskKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnlimitedLiteral__Alternatives"


    // $ANTLR start "rule__XDataType__Group__0"
    // InternalDTML.g:570:1: rule__XDataType__Group__0 : rule__XDataType__Group__0__Impl rule__XDataType__Group__1 ;
    public final void rule__XDataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:574:1: ( rule__XDataType__Group__0__Impl rule__XDataType__Group__1 )
            // InternalDTML.g:575:2: rule__XDataType__Group__0__Impl rule__XDataType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__XDataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XDataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__0"


    // $ANTLR start "rule__XDataType__Group__0__Impl"
    // InternalDTML.g:582:1: rule__XDataType__Group__0__Impl : ( ruleDTKey ) ;
    public final void rule__XDataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:586:1: ( ( ruleDTKey ) )
            // InternalDTML.g:587:1: ( ruleDTKey )
            {
            // InternalDTML.g:587:1: ( ruleDTKey )
            // InternalDTML.g:588:2: ruleDTKey
            {
             before(grammarAccess.getXDataTypeAccess().getDTKeyParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDTKey();

            state._fsp--;

             after(grammarAccess.getXDataTypeAccess().getDTKeyParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__0__Impl"


    // $ANTLR start "rule__XDataType__Group__1"
    // InternalDTML.g:597:1: rule__XDataType__Group__1 : rule__XDataType__Group__1__Impl rule__XDataType__Group__2 ;
    public final void rule__XDataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:601:1: ( rule__XDataType__Group__1__Impl rule__XDataType__Group__2 )
            // InternalDTML.g:602:2: rule__XDataType__Group__1__Impl rule__XDataType__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__XDataType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XDataType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__1"


    // $ANTLR start "rule__XDataType__Group__1__Impl"
    // InternalDTML.g:609:1: rule__XDataType__Group__1__Impl : ( ( rule__XDataType__NameAssignment_1 ) ) ;
    public final void rule__XDataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:613:1: ( ( ( rule__XDataType__NameAssignment_1 ) ) )
            // InternalDTML.g:614:1: ( ( rule__XDataType__NameAssignment_1 ) )
            {
            // InternalDTML.g:614:1: ( ( rule__XDataType__NameAssignment_1 ) )
            // InternalDTML.g:615:2: ( rule__XDataType__NameAssignment_1 )
            {
             before(grammarAccess.getXDataTypeAccess().getNameAssignment_1()); 
            // InternalDTML.g:616:2: ( rule__XDataType__NameAssignment_1 )
            // InternalDTML.g:616:3: rule__XDataType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__XDataType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getXDataTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__1__Impl"


    // $ANTLR start "rule__XDataType__Group__2"
    // InternalDTML.g:624:1: rule__XDataType__Group__2 : rule__XDataType__Group__2__Impl rule__XDataType__Group__3 ;
    public final void rule__XDataType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:628:1: ( rule__XDataType__Group__2__Impl rule__XDataType__Group__3 )
            // InternalDTML.g:629:2: rule__XDataType__Group__2__Impl rule__XDataType__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__XDataType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XDataType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__2"


    // $ANTLR start "rule__XDataType__Group__2__Impl"
    // InternalDTML.g:636:1: rule__XDataType__Group__2__Impl : ( '{' ) ;
    public final void rule__XDataType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:640:1: ( ( '{' ) )
            // InternalDTML.g:641:1: ( '{' )
            {
            // InternalDTML.g:641:1: ( '{' )
            // InternalDTML.g:642:2: '{'
            {
             before(grammarAccess.getXDataTypeAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getXDataTypeAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__2__Impl"


    // $ANTLR start "rule__XDataType__Group__3"
    // InternalDTML.g:651:1: rule__XDataType__Group__3 : rule__XDataType__Group__3__Impl rule__XDataType__Group__4 ;
    public final void rule__XDataType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:655:1: ( rule__XDataType__Group__3__Impl rule__XDataType__Group__4 )
            // InternalDTML.g:656:2: rule__XDataType__Group__3__Impl rule__XDataType__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__XDataType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XDataType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__3"


    // $ANTLR start "rule__XDataType__Group__3__Impl"
    // InternalDTML.g:663:1: rule__XDataType__Group__3__Impl : ( ( rule__XDataType__AttributesAssignment_3 )* ) ;
    public final void rule__XDataType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:667:1: ( ( ( rule__XDataType__AttributesAssignment_3 )* ) )
            // InternalDTML.g:668:1: ( ( rule__XDataType__AttributesAssignment_3 )* )
            {
            // InternalDTML.g:668:1: ( ( rule__XDataType__AttributesAssignment_3 )* )
            // InternalDTML.g:669:2: ( rule__XDataType__AttributesAssignment_3 )*
            {
             before(grammarAccess.getXDataTypeAccess().getAttributesAssignment_3()); 
            // InternalDTML.g:670:2: ( rule__XDataType__AttributesAssignment_3 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==23) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalDTML.g:670:3: rule__XDataType__AttributesAssignment_3
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__XDataType__AttributesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getXDataTypeAccess().getAttributesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__3__Impl"


    // $ANTLR start "rule__XDataType__Group__4"
    // InternalDTML.g:678:1: rule__XDataType__Group__4 : rule__XDataType__Group__4__Impl ;
    public final void rule__XDataType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:682:1: ( rule__XDataType__Group__4__Impl )
            // InternalDTML.g:683:2: rule__XDataType__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XDataType__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__4"


    // $ANTLR start "rule__XDataType__Group__4__Impl"
    // InternalDTML.g:689:1: rule__XDataType__Group__4__Impl : ( '}' ) ;
    public final void rule__XDataType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:693:1: ( ( '}' ) )
            // InternalDTML.g:694:1: ( '}' )
            {
            // InternalDTML.g:694:1: ( '}' )
            // InternalDTML.g:695:2: '}'
            {
             before(grammarAccess.getXDataTypeAccess().getRightCurlyBracketKeyword_4()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getXDataTypeAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__Group__4__Impl"


    // $ANTLR start "rule__Property__Group__0"
    // InternalDTML.g:705:1: rule__Property__Group__0 : rule__Property__Group__0__Impl rule__Property__Group__1 ;
    public final void rule__Property__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:709:1: ( rule__Property__Group__0__Impl rule__Property__Group__1 )
            // InternalDTML.g:710:2: rule__Property__Group__0__Impl rule__Property__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Property__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0"


    // $ANTLR start "rule__Property__Group__0__Impl"
    // InternalDTML.g:717:1: rule__Property__Group__0__Impl : ( 'attribute' ) ;
    public final void rule__Property__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:721:1: ( ( 'attribute' ) )
            // InternalDTML.g:722:1: ( 'attribute' )
            {
            // InternalDTML.g:722:1: ( 'attribute' )
            // InternalDTML.g:723:2: 'attribute'
            {
             before(grammarAccess.getPropertyAccess().getAttributeKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getAttributeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0__Impl"


    // $ANTLR start "rule__Property__Group__1"
    // InternalDTML.g:732:1: rule__Property__Group__1 : rule__Property__Group__1__Impl rule__Property__Group__2 ;
    public final void rule__Property__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:736:1: ( rule__Property__Group__1__Impl rule__Property__Group__2 )
            // InternalDTML.g:737:2: rule__Property__Group__1__Impl rule__Property__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Property__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1"


    // $ANTLR start "rule__Property__Group__1__Impl"
    // InternalDTML.g:744:1: rule__Property__Group__1__Impl : ( ( rule__Property__NameAssignment_1 ) ) ;
    public final void rule__Property__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:748:1: ( ( ( rule__Property__NameAssignment_1 ) ) )
            // InternalDTML.g:749:1: ( ( rule__Property__NameAssignment_1 ) )
            {
            // InternalDTML.g:749:1: ( ( rule__Property__NameAssignment_1 ) )
            // InternalDTML.g:750:2: ( rule__Property__NameAssignment_1 )
            {
             before(grammarAccess.getPropertyAccess().getNameAssignment_1()); 
            // InternalDTML.g:751:2: ( rule__Property__NameAssignment_1 )
            // InternalDTML.g:751:3: rule__Property__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Property__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1__Impl"


    // $ANTLR start "rule__Property__Group__2"
    // InternalDTML.g:759:1: rule__Property__Group__2 : rule__Property__Group__2__Impl rule__Property__Group__3 ;
    public final void rule__Property__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:763:1: ( rule__Property__Group__2__Impl rule__Property__Group__3 )
            // InternalDTML.g:764:2: rule__Property__Group__2__Impl rule__Property__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Property__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__2"


    // $ANTLR start "rule__Property__Group__2__Impl"
    // InternalDTML.g:771:1: rule__Property__Group__2__Impl : ( ( rule__Property__Group_2__0 )? ) ;
    public final void rule__Property__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:775:1: ( ( ( rule__Property__Group_2__0 )? ) )
            // InternalDTML.g:776:1: ( ( rule__Property__Group_2__0 )? )
            {
            // InternalDTML.g:776:1: ( ( rule__Property__Group_2__0 )? )
            // InternalDTML.g:777:2: ( rule__Property__Group_2__0 )?
            {
             before(grammarAccess.getPropertyAccess().getGroup_2()); 
            // InternalDTML.g:778:2: ( rule__Property__Group_2__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==24) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDTML.g:778:3: rule__Property__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__2__Impl"


    // $ANTLR start "rule__Property__Group__3"
    // InternalDTML.g:786:1: rule__Property__Group__3 : rule__Property__Group__3__Impl rule__Property__Group__4 ;
    public final void rule__Property__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:790:1: ( rule__Property__Group__3__Impl rule__Property__Group__4 )
            // InternalDTML.g:791:2: rule__Property__Group__3__Impl rule__Property__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Property__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__3"


    // $ANTLR start "rule__Property__Group__3__Impl"
    // InternalDTML.g:798:1: rule__Property__Group__3__Impl : ( ( rule__Property__MultiplicityAssignment_3 )? ) ;
    public final void rule__Property__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:802:1: ( ( ( rule__Property__MultiplicityAssignment_3 )? ) )
            // InternalDTML.g:803:1: ( ( rule__Property__MultiplicityAssignment_3 )? )
            {
            // InternalDTML.g:803:1: ( ( rule__Property__MultiplicityAssignment_3 )? )
            // InternalDTML.g:804:2: ( rule__Property__MultiplicityAssignment_3 )?
            {
             before(grammarAccess.getPropertyAccess().getMultiplicityAssignment_3()); 
            // InternalDTML.g:805:2: ( rule__Property__MultiplicityAssignment_3 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==16) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDTML.g:805:3: rule__Property__MultiplicityAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__MultiplicityAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getMultiplicityAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__3__Impl"


    // $ANTLR start "rule__Property__Group__4"
    // InternalDTML.g:813:1: rule__Property__Group__4 : rule__Property__Group__4__Impl rule__Property__Group__5 ;
    public final void rule__Property__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:817:1: ( rule__Property__Group__4__Impl rule__Property__Group__5 )
            // InternalDTML.g:818:2: rule__Property__Group__4__Impl rule__Property__Group__5
            {
            pushFollow(FOLLOW_8);
            rule__Property__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__4"


    // $ANTLR start "rule__Property__Group__4__Impl"
    // InternalDTML.g:825:1: rule__Property__Group__4__Impl : ( ( rule__Property__Group_4__0 )? ) ;
    public final void rule__Property__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:829:1: ( ( ( rule__Property__Group_4__0 )? ) )
            // InternalDTML.g:830:1: ( ( rule__Property__Group_4__0 )? )
            {
            // InternalDTML.g:830:1: ( ( rule__Property__Group_4__0 )? )
            // InternalDTML.g:831:2: ( rule__Property__Group_4__0 )?
            {
             before(grammarAccess.getPropertyAccess().getGroup_4()); 
            // InternalDTML.g:832:2: ( rule__Property__Group_4__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==25) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDTML.g:832:3: rule__Property__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__4__Impl"


    // $ANTLR start "rule__Property__Group__5"
    // InternalDTML.g:840:1: rule__Property__Group__5 : rule__Property__Group__5__Impl ;
    public final void rule__Property__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:844:1: ( rule__Property__Group__5__Impl )
            // InternalDTML.g:845:2: rule__Property__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__5"


    // $ANTLR start "rule__Property__Group__5__Impl"
    // InternalDTML.g:851:1: rule__Property__Group__5__Impl : ( ( rule__Property__CommentAssignment_5 )? ) ;
    public final void rule__Property__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:855:1: ( ( ( rule__Property__CommentAssignment_5 )? ) )
            // InternalDTML.g:856:1: ( ( rule__Property__CommentAssignment_5 )? )
            {
            // InternalDTML.g:856:1: ( ( rule__Property__CommentAssignment_5 )? )
            // InternalDTML.g:857:2: ( rule__Property__CommentAssignment_5 )?
            {
             before(grammarAccess.getPropertyAccess().getCommentAssignment_5()); 
            // InternalDTML.g:858:2: ( rule__Property__CommentAssignment_5 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_VSL_COMMENT) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDTML.g:858:3: rule__Property__CommentAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__CommentAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getCommentAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__5__Impl"


    // $ANTLR start "rule__Property__Group_2__0"
    // InternalDTML.g:867:1: rule__Property__Group_2__0 : rule__Property__Group_2__0__Impl rule__Property__Group_2__1 ;
    public final void rule__Property__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:871:1: ( rule__Property__Group_2__0__Impl rule__Property__Group_2__1 )
            // InternalDTML.g:872:2: rule__Property__Group_2__0__Impl rule__Property__Group_2__1
            {
            pushFollow(FOLLOW_9);
            rule__Property__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__0"


    // $ANTLR start "rule__Property__Group_2__0__Impl"
    // InternalDTML.g:879:1: rule__Property__Group_2__0__Impl : ( ':' ) ;
    public final void rule__Property__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:883:1: ( ( ':' ) )
            // InternalDTML.g:884:1: ( ':' )
            {
            // InternalDTML.g:884:1: ( ':' )
            // InternalDTML.g:885:2: ':'
            {
             before(grammarAccess.getPropertyAccess().getColonKeyword_2_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getColonKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__0__Impl"


    // $ANTLR start "rule__Property__Group_2__1"
    // InternalDTML.g:894:1: rule__Property__Group_2__1 : rule__Property__Group_2__1__Impl ;
    public final void rule__Property__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:898:1: ( rule__Property__Group_2__1__Impl )
            // InternalDTML.g:899:2: rule__Property__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__1"


    // $ANTLR start "rule__Property__Group_2__1__Impl"
    // InternalDTML.g:905:1: rule__Property__Group_2__1__Impl : ( ( rule__Property__Alternatives_2_1 ) ) ;
    public final void rule__Property__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:909:1: ( ( ( rule__Property__Alternatives_2_1 ) ) )
            // InternalDTML.g:910:1: ( ( rule__Property__Alternatives_2_1 ) )
            {
            // InternalDTML.g:910:1: ( ( rule__Property__Alternatives_2_1 ) )
            // InternalDTML.g:911:2: ( rule__Property__Alternatives_2_1 )
            {
             before(grammarAccess.getPropertyAccess().getAlternatives_2_1()); 
            // InternalDTML.g:912:2: ( rule__Property__Alternatives_2_1 )
            // InternalDTML.g:912:3: rule__Property__Alternatives_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Property__Alternatives_2_1();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getAlternatives_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_2__1__Impl"


    // $ANTLR start "rule__Property__Group_4__0"
    // InternalDTML.g:921:1: rule__Property__Group_4__0 : rule__Property__Group_4__0__Impl rule__Property__Group_4__1 ;
    public final void rule__Property__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:925:1: ( rule__Property__Group_4__0__Impl rule__Property__Group_4__1 )
            // InternalDTML.g:926:2: rule__Property__Group_4__0__Impl rule__Property__Group_4__1
            {
            pushFollow(FOLLOW_10);
            rule__Property__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_4__0"


    // $ANTLR start "rule__Property__Group_4__0__Impl"
    // InternalDTML.g:933:1: rule__Property__Group_4__0__Impl : ( '=' ) ;
    public final void rule__Property__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:937:1: ( ( '=' ) )
            // InternalDTML.g:938:1: ( '=' )
            {
            // InternalDTML.g:938:1: ( '=' )
            // InternalDTML.g:939:2: '='
            {
             before(grammarAccess.getPropertyAccess().getEqualsSignKeyword_4_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getEqualsSignKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_4__0__Impl"


    // $ANTLR start "rule__Property__Group_4__1"
    // InternalDTML.g:948:1: rule__Property__Group_4__1 : rule__Property__Group_4__1__Impl ;
    public final void rule__Property__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:952:1: ( rule__Property__Group_4__1__Impl )
            // InternalDTML.g:953:2: rule__Property__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_4__1"


    // $ANTLR start "rule__Property__Group_4__1__Impl"
    // InternalDTML.g:959:1: rule__Property__Group_4__1__Impl : ( ( rule__Property__ValueAssignment_4_1 ) ) ;
    public final void rule__Property__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:963:1: ( ( ( rule__Property__ValueAssignment_4_1 ) ) )
            // InternalDTML.g:964:1: ( ( rule__Property__ValueAssignment_4_1 ) )
            {
            // InternalDTML.g:964:1: ( ( rule__Property__ValueAssignment_4_1 ) )
            // InternalDTML.g:965:2: ( rule__Property__ValueAssignment_4_1 )
            {
             before(grammarAccess.getPropertyAccess().getValueAssignment_4_1()); 
            // InternalDTML.g:966:2: ( rule__Property__ValueAssignment_4_1 )
            // InternalDTML.g:966:3: rule__Property__ValueAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Property__ValueAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getValueAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group_4__1__Impl"


    // $ANTLR start "rule__XAssignment__Group__0"
    // InternalDTML.g:975:1: rule__XAssignment__Group__0 : rule__XAssignment__Group__0__Impl rule__XAssignment__Group__1 ;
    public final void rule__XAssignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:979:1: ( rule__XAssignment__Group__0__Impl rule__XAssignment__Group__1 )
            // InternalDTML.g:980:2: rule__XAssignment__Group__0__Impl rule__XAssignment__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__XAssignment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XAssignment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__0"


    // $ANTLR start "rule__XAssignment__Group__0__Impl"
    // InternalDTML.g:987:1: rule__XAssignment__Group__0__Impl : ( ( rule__XAssignment__NameAssignment_0 ) ) ;
    public final void rule__XAssignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:991:1: ( ( ( rule__XAssignment__NameAssignment_0 ) ) )
            // InternalDTML.g:992:1: ( ( rule__XAssignment__NameAssignment_0 ) )
            {
            // InternalDTML.g:992:1: ( ( rule__XAssignment__NameAssignment_0 ) )
            // InternalDTML.g:993:2: ( rule__XAssignment__NameAssignment_0 )
            {
             before(grammarAccess.getXAssignmentAccess().getNameAssignment_0()); 
            // InternalDTML.g:994:2: ( rule__XAssignment__NameAssignment_0 )
            // InternalDTML.g:994:3: rule__XAssignment__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XAssignment__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXAssignmentAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__0__Impl"


    // $ANTLR start "rule__XAssignment__Group__1"
    // InternalDTML.g:1002:1: rule__XAssignment__Group__1 : rule__XAssignment__Group__1__Impl rule__XAssignment__Group__2 ;
    public final void rule__XAssignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1006:1: ( rule__XAssignment__Group__1__Impl rule__XAssignment__Group__2 )
            // InternalDTML.g:1007:2: rule__XAssignment__Group__1__Impl rule__XAssignment__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__XAssignment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XAssignment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__1"


    // $ANTLR start "rule__XAssignment__Group__1__Impl"
    // InternalDTML.g:1014:1: rule__XAssignment__Group__1__Impl : ( '=' ) ;
    public final void rule__XAssignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1018:1: ( ( '=' ) )
            // InternalDTML.g:1019:1: ( '=' )
            {
            // InternalDTML.g:1019:1: ( '=' )
            // InternalDTML.g:1020:2: '='
            {
             before(grammarAccess.getXAssignmentAccess().getEqualsSignKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getXAssignmentAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__1__Impl"


    // $ANTLR start "rule__XAssignment__Group__2"
    // InternalDTML.g:1029:1: rule__XAssignment__Group__2 : rule__XAssignment__Group__2__Impl rule__XAssignment__Group__3 ;
    public final void rule__XAssignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1033:1: ( rule__XAssignment__Group__2__Impl rule__XAssignment__Group__3 )
            // InternalDTML.g:1034:2: rule__XAssignment__Group__2__Impl rule__XAssignment__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__XAssignment__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XAssignment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__2"


    // $ANTLR start "rule__XAssignment__Group__2__Impl"
    // InternalDTML.g:1041:1: rule__XAssignment__Group__2__Impl : ( ( rule__XAssignment__ExpressionAssignment_2 ) ) ;
    public final void rule__XAssignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1045:1: ( ( ( rule__XAssignment__ExpressionAssignment_2 ) ) )
            // InternalDTML.g:1046:1: ( ( rule__XAssignment__ExpressionAssignment_2 ) )
            {
            // InternalDTML.g:1046:1: ( ( rule__XAssignment__ExpressionAssignment_2 ) )
            // InternalDTML.g:1047:2: ( rule__XAssignment__ExpressionAssignment_2 )
            {
             before(grammarAccess.getXAssignmentAccess().getExpressionAssignment_2()); 
            // InternalDTML.g:1048:2: ( rule__XAssignment__ExpressionAssignment_2 )
            // InternalDTML.g:1048:3: rule__XAssignment__ExpressionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__XAssignment__ExpressionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXAssignmentAccess().getExpressionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__2__Impl"


    // $ANTLR start "rule__XAssignment__Group__3"
    // InternalDTML.g:1056:1: rule__XAssignment__Group__3 : rule__XAssignment__Group__3__Impl ;
    public final void rule__XAssignment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1060:1: ( rule__XAssignment__Group__3__Impl )
            // InternalDTML.g:1061:2: rule__XAssignment__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XAssignment__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__3"


    // $ANTLR start "rule__XAssignment__Group__3__Impl"
    // InternalDTML.g:1067:1: rule__XAssignment__Group__3__Impl : ( ';' ) ;
    public final void rule__XAssignment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1071:1: ( ( ';' ) )
            // InternalDTML.g:1072:1: ( ';' )
            {
            // InternalDTML.g:1072:1: ( ';' )
            // InternalDTML.g:1073:2: ';'
            {
             before(grammarAccess.getXAssignmentAccess().getSemicolonKeyword_3()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getXAssignmentAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__Group__3__Impl"


    // $ANTLR start "rule__XEnumeration__Group__0"
    // InternalDTML.g:1083:1: rule__XEnumeration__Group__0 : rule__XEnumeration__Group__0__Impl rule__XEnumeration__Group__1 ;
    public final void rule__XEnumeration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1087:1: ( rule__XEnumeration__Group__0__Impl rule__XEnumeration__Group__1 )
            // InternalDTML.g:1088:2: rule__XEnumeration__Group__0__Impl rule__XEnumeration__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__XEnumeration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumeration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__0"


    // $ANTLR start "rule__XEnumeration__Group__0__Impl"
    // InternalDTML.g:1095:1: rule__XEnumeration__Group__0__Impl : ( 'Enumeration' ) ;
    public final void rule__XEnumeration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1099:1: ( ( 'Enumeration' ) )
            // InternalDTML.g:1100:1: ( 'Enumeration' )
            {
            // InternalDTML.g:1100:1: ( 'Enumeration' )
            // InternalDTML.g:1101:2: 'Enumeration'
            {
             before(grammarAccess.getXEnumerationAccess().getEnumerationKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getXEnumerationAccess().getEnumerationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__0__Impl"


    // $ANTLR start "rule__XEnumeration__Group__1"
    // InternalDTML.g:1110:1: rule__XEnumeration__Group__1 : rule__XEnumeration__Group__1__Impl rule__XEnumeration__Group__2 ;
    public final void rule__XEnumeration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1114:1: ( rule__XEnumeration__Group__1__Impl rule__XEnumeration__Group__2 )
            // InternalDTML.g:1115:2: rule__XEnumeration__Group__1__Impl rule__XEnumeration__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__XEnumeration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumeration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__1"


    // $ANTLR start "rule__XEnumeration__Group__1__Impl"
    // InternalDTML.g:1122:1: rule__XEnumeration__Group__1__Impl : ( ( rule__XEnumeration__NameAssignment_1 ) ) ;
    public final void rule__XEnumeration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1126:1: ( ( ( rule__XEnumeration__NameAssignment_1 ) ) )
            // InternalDTML.g:1127:1: ( ( rule__XEnumeration__NameAssignment_1 ) )
            {
            // InternalDTML.g:1127:1: ( ( rule__XEnumeration__NameAssignment_1 ) )
            // InternalDTML.g:1128:2: ( rule__XEnumeration__NameAssignment_1 )
            {
             before(grammarAccess.getXEnumerationAccess().getNameAssignment_1()); 
            // InternalDTML.g:1129:2: ( rule__XEnumeration__NameAssignment_1 )
            // InternalDTML.g:1129:3: rule__XEnumeration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__XEnumeration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getXEnumerationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__1__Impl"


    // $ANTLR start "rule__XEnumeration__Group__2"
    // InternalDTML.g:1137:1: rule__XEnumeration__Group__2 : rule__XEnumeration__Group__2__Impl rule__XEnumeration__Group__3 ;
    public final void rule__XEnumeration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1141:1: ( rule__XEnumeration__Group__2__Impl rule__XEnumeration__Group__3 )
            // InternalDTML.g:1142:2: rule__XEnumeration__Group__2__Impl rule__XEnumeration__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__XEnumeration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumeration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__2"


    // $ANTLR start "rule__XEnumeration__Group__2__Impl"
    // InternalDTML.g:1149:1: rule__XEnumeration__Group__2__Impl : ( '{' ) ;
    public final void rule__XEnumeration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1153:1: ( ( '{' ) )
            // InternalDTML.g:1154:1: ( '{' )
            {
            // InternalDTML.g:1154:1: ( '{' )
            // InternalDTML.g:1155:2: '{'
            {
             before(grammarAccess.getXEnumerationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getXEnumerationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__2__Impl"


    // $ANTLR start "rule__XEnumeration__Group__3"
    // InternalDTML.g:1164:1: rule__XEnumeration__Group__3 : rule__XEnumeration__Group__3__Impl rule__XEnumeration__Group__4 ;
    public final void rule__XEnumeration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1168:1: ( rule__XEnumeration__Group__3__Impl rule__XEnumeration__Group__4 )
            // InternalDTML.g:1169:2: rule__XEnumeration__Group__3__Impl rule__XEnumeration__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__XEnumeration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumeration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__3"


    // $ANTLR start "rule__XEnumeration__Group__3__Impl"
    // InternalDTML.g:1176:1: rule__XEnumeration__Group__3__Impl : ( ( rule__XEnumeration__LiteralsAssignment_3 )* ) ;
    public final void rule__XEnumeration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1180:1: ( ( ( rule__XEnumeration__LiteralsAssignment_3 )* ) )
            // InternalDTML.g:1181:1: ( ( rule__XEnumeration__LiteralsAssignment_3 )* )
            {
            // InternalDTML.g:1181:1: ( ( rule__XEnumeration__LiteralsAssignment_3 )* )
            // InternalDTML.g:1182:2: ( rule__XEnumeration__LiteralsAssignment_3 )*
            {
             before(grammarAccess.getXEnumerationAccess().getLiteralsAssignment_3()); 
            // InternalDTML.g:1183:2: ( rule__XEnumeration__LiteralsAssignment_3 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalDTML.g:1183:3: rule__XEnumeration__LiteralsAssignment_3
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__XEnumeration__LiteralsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getXEnumerationAccess().getLiteralsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__3__Impl"


    // $ANTLR start "rule__XEnumeration__Group__4"
    // InternalDTML.g:1191:1: rule__XEnumeration__Group__4 : rule__XEnumeration__Group__4__Impl ;
    public final void rule__XEnumeration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1195:1: ( rule__XEnumeration__Group__4__Impl )
            // InternalDTML.g:1196:2: rule__XEnumeration__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XEnumeration__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__4"


    // $ANTLR start "rule__XEnumeration__Group__4__Impl"
    // InternalDTML.g:1202:1: rule__XEnumeration__Group__4__Impl : ( '}' ) ;
    public final void rule__XEnumeration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1206:1: ( ( '}' ) )
            // InternalDTML.g:1207:1: ( '}' )
            {
            // InternalDTML.g:1207:1: ( '}' )
            // InternalDTML.g:1208:2: '}'
            {
             before(grammarAccess.getXEnumerationAccess().getRightCurlyBracketKeyword_4()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getXEnumerationAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__Group__4__Impl"


    // $ANTLR start "rule__XEnumLiteral__Group__0"
    // InternalDTML.g:1218:1: rule__XEnumLiteral__Group__0 : rule__XEnumLiteral__Group__0__Impl rule__XEnumLiteral__Group__1 ;
    public final void rule__XEnumLiteral__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1222:1: ( rule__XEnumLiteral__Group__0__Impl rule__XEnumLiteral__Group__1 )
            // InternalDTML.g:1223:2: rule__XEnumLiteral__Group__0__Impl rule__XEnumLiteral__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__XEnumLiteral__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group__0"


    // $ANTLR start "rule__XEnumLiteral__Group__0__Impl"
    // InternalDTML.g:1230:1: rule__XEnumLiteral__Group__0__Impl : ( ( rule__XEnumLiteral__NameAssignment_0 ) ) ;
    public final void rule__XEnumLiteral__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1234:1: ( ( ( rule__XEnumLiteral__NameAssignment_0 ) ) )
            // InternalDTML.g:1235:1: ( ( rule__XEnumLiteral__NameAssignment_0 ) )
            {
            // InternalDTML.g:1235:1: ( ( rule__XEnumLiteral__NameAssignment_0 ) )
            // InternalDTML.g:1236:2: ( rule__XEnumLiteral__NameAssignment_0 )
            {
             before(grammarAccess.getXEnumLiteralAccess().getNameAssignment_0()); 
            // InternalDTML.g:1237:2: ( rule__XEnumLiteral__NameAssignment_0 )
            // InternalDTML.g:1237:3: rule__XEnumLiteral__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXEnumLiteralAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group__0__Impl"


    // $ANTLR start "rule__XEnumLiteral__Group__1"
    // InternalDTML.g:1245:1: rule__XEnumLiteral__Group__1 : rule__XEnumLiteral__Group__1__Impl rule__XEnumLiteral__Group__2 ;
    public final void rule__XEnumLiteral__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1249:1: ( rule__XEnumLiteral__Group__1__Impl rule__XEnumLiteral__Group__2 )
            // InternalDTML.g:1250:2: rule__XEnumLiteral__Group__1__Impl rule__XEnumLiteral__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__XEnumLiteral__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group__1"


    // $ANTLR start "rule__XEnumLiteral__Group__1__Impl"
    // InternalDTML.g:1257:1: rule__XEnumLiteral__Group__1__Impl : ( ( rule__XEnumLiteral__Group_1__0 )? ) ;
    public final void rule__XEnumLiteral__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1261:1: ( ( ( rule__XEnumLiteral__Group_1__0 )? ) )
            // InternalDTML.g:1262:1: ( ( rule__XEnumLiteral__Group_1__0 )? )
            {
            // InternalDTML.g:1262:1: ( ( rule__XEnumLiteral__Group_1__0 )? )
            // InternalDTML.g:1263:2: ( rule__XEnumLiteral__Group_1__0 )?
            {
             before(grammarAccess.getXEnumLiteralAccess().getGroup_1()); 
            // InternalDTML.g:1264:2: ( rule__XEnumLiteral__Group_1__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==25) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalDTML.g:1264:3: rule__XEnumLiteral__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__XEnumLiteral__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getXEnumLiteralAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group__1__Impl"


    // $ANTLR start "rule__XEnumLiteral__Group__2"
    // InternalDTML.g:1272:1: rule__XEnumLiteral__Group__2 : rule__XEnumLiteral__Group__2__Impl ;
    public final void rule__XEnumLiteral__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1276:1: ( rule__XEnumLiteral__Group__2__Impl )
            // InternalDTML.g:1277:2: rule__XEnumLiteral__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group__2"


    // $ANTLR start "rule__XEnumLiteral__Group__2__Impl"
    // InternalDTML.g:1283:1: rule__XEnumLiteral__Group__2__Impl : ( ( rule__XEnumLiteral__CommentAssignment_2 )? ) ;
    public final void rule__XEnumLiteral__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1287:1: ( ( ( rule__XEnumLiteral__CommentAssignment_2 )? ) )
            // InternalDTML.g:1288:1: ( ( rule__XEnumLiteral__CommentAssignment_2 )? )
            {
            // InternalDTML.g:1288:1: ( ( rule__XEnumLiteral__CommentAssignment_2 )? )
            // InternalDTML.g:1289:2: ( rule__XEnumLiteral__CommentAssignment_2 )?
            {
             before(grammarAccess.getXEnumLiteralAccess().getCommentAssignment_2()); 
            // InternalDTML.g:1290:2: ( rule__XEnumLiteral__CommentAssignment_2 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_VSL_COMMENT) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDTML.g:1290:3: rule__XEnumLiteral__CommentAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__XEnumLiteral__CommentAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getXEnumLiteralAccess().getCommentAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group__2__Impl"


    // $ANTLR start "rule__XEnumLiteral__Group_1__0"
    // InternalDTML.g:1299:1: rule__XEnumLiteral__Group_1__0 : rule__XEnumLiteral__Group_1__0__Impl rule__XEnumLiteral__Group_1__1 ;
    public final void rule__XEnumLiteral__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1303:1: ( rule__XEnumLiteral__Group_1__0__Impl rule__XEnumLiteral__Group_1__1 )
            // InternalDTML.g:1304:2: rule__XEnumLiteral__Group_1__0__Impl rule__XEnumLiteral__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__XEnumLiteral__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group_1__0"


    // $ANTLR start "rule__XEnumLiteral__Group_1__0__Impl"
    // InternalDTML.g:1311:1: rule__XEnumLiteral__Group_1__0__Impl : ( '=' ) ;
    public final void rule__XEnumLiteral__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1315:1: ( ( '=' ) )
            // InternalDTML.g:1316:1: ( '=' )
            {
            // InternalDTML.g:1316:1: ( '=' )
            // InternalDTML.g:1317:2: '='
            {
             before(grammarAccess.getXEnumLiteralAccess().getEqualsSignKeyword_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getXEnumLiteralAccess().getEqualsSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group_1__0__Impl"


    // $ANTLR start "rule__XEnumLiteral__Group_1__1"
    // InternalDTML.g:1326:1: rule__XEnumLiteral__Group_1__1 : rule__XEnumLiteral__Group_1__1__Impl ;
    public final void rule__XEnumLiteral__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1330:1: ( rule__XEnumLiteral__Group_1__1__Impl )
            // InternalDTML.g:1331:2: rule__XEnumLiteral__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group_1__1"


    // $ANTLR start "rule__XEnumLiteral__Group_1__1__Impl"
    // InternalDTML.g:1337:1: rule__XEnumLiteral__Group_1__1__Impl : ( ( rule__XEnumLiteral__ValueAssignment_1_1 ) ) ;
    public final void rule__XEnumLiteral__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1341:1: ( ( ( rule__XEnumLiteral__ValueAssignment_1_1 ) ) )
            // InternalDTML.g:1342:1: ( ( rule__XEnumLiteral__ValueAssignment_1_1 ) )
            {
            // InternalDTML.g:1342:1: ( ( rule__XEnumLiteral__ValueAssignment_1_1 ) )
            // InternalDTML.g:1343:2: ( rule__XEnumLiteral__ValueAssignment_1_1 )
            {
             before(grammarAccess.getXEnumLiteralAccess().getValueAssignment_1_1()); 
            // InternalDTML.g:1344:2: ( rule__XEnumLiteral__ValueAssignment_1_1 )
            // InternalDTML.g:1344:3: rule__XEnumLiteral__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__XEnumLiteral__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getXEnumLiteralAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__Group_1__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalDTML.g:1353:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1357:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalDTML.g:1358:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalDTML.g:1365:1: rule__QualifiedName__Group__0__Impl : ( ( rule__QualifiedName__PathAssignment_0 ) ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1369:1: ( ( ( rule__QualifiedName__PathAssignment_0 ) ) )
            // InternalDTML.g:1370:1: ( ( rule__QualifiedName__PathAssignment_0 ) )
            {
            // InternalDTML.g:1370:1: ( ( rule__QualifiedName__PathAssignment_0 ) )
            // InternalDTML.g:1371:2: ( rule__QualifiedName__PathAssignment_0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getPathAssignment_0()); 
            // InternalDTML.g:1372:2: ( rule__QualifiedName__PathAssignment_0 )
            // InternalDTML.g:1372:3: rule__QualifiedName__PathAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__PathAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getPathAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalDTML.g:1380:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl rule__QualifiedName__Group__2 ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1384:1: ( rule__QualifiedName__Group__1__Impl rule__QualifiedName__Group__2 )
            // InternalDTML.g:1385:2: rule__QualifiedName__Group__1__Impl rule__QualifiedName__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalDTML.g:1392:1: rule__QualifiedName__Group__1__Impl : ( '::' ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1396:1: ( ( '::' ) )
            // InternalDTML.g:1397:1: ( '::' )
            {
            // InternalDTML.g:1397:1: ( '::' )
            // InternalDTML.g:1398:2: '::'
            {
             before(grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__2"
    // InternalDTML.g:1407:1: rule__QualifiedName__Group__2 : rule__QualifiedName__Group__2__Impl ;
    public final void rule__QualifiedName__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1411:1: ( rule__QualifiedName__Group__2__Impl )
            // InternalDTML.g:1412:2: rule__QualifiedName__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__2"


    // $ANTLR start "rule__QualifiedName__Group__2__Impl"
    // InternalDTML.g:1418:1: rule__QualifiedName__Group__2__Impl : ( ( rule__QualifiedName__RemainingAssignment_2 )? ) ;
    public final void rule__QualifiedName__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1422:1: ( ( ( rule__QualifiedName__RemainingAssignment_2 )? ) )
            // InternalDTML.g:1423:1: ( ( rule__QualifiedName__RemainingAssignment_2 )? )
            {
            // InternalDTML.g:1423:1: ( ( rule__QualifiedName__RemainingAssignment_2 )? )
            // InternalDTML.g:1424:2: ( rule__QualifiedName__RemainingAssignment_2 )?
            {
             before(grammarAccess.getQualifiedNameAccess().getRemainingAssignment_2()); 
            // InternalDTML.g:1425:2: ( rule__QualifiedName__RemainingAssignment_2 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                int LA16_1 = input.LA(2);

                if ( (LA16_1==28) ) {
                    alt16=1;
                }
            }
            switch (alt16) {
                case 1 :
                    // InternalDTML.g:1425:3: rule__QualifiedName__RemainingAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__QualifiedName__RemainingAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameAccess().getRemainingAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__2__Impl"


    // $ANTLR start "rule__TypeRule__Group__0"
    // InternalDTML.g:1434:1: rule__TypeRule__Group__0 : rule__TypeRule__Group__0__Impl rule__TypeRule__Group__1 ;
    public final void rule__TypeRule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1438:1: ( rule__TypeRule__Group__0__Impl rule__TypeRule__Group__1 )
            // InternalDTML.g:1439:2: rule__TypeRule__Group__0__Impl rule__TypeRule__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__TypeRule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TypeRule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__Group__0"


    // $ANTLR start "rule__TypeRule__Group__0__Impl"
    // InternalDTML.g:1446:1: rule__TypeRule__Group__0__Impl : ( ( rule__TypeRule__PathAssignment_0 )? ) ;
    public final void rule__TypeRule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1450:1: ( ( ( rule__TypeRule__PathAssignment_0 )? ) )
            // InternalDTML.g:1451:1: ( ( rule__TypeRule__PathAssignment_0 )? )
            {
            // InternalDTML.g:1451:1: ( ( rule__TypeRule__PathAssignment_0 )? )
            // InternalDTML.g:1452:2: ( rule__TypeRule__PathAssignment_0 )?
            {
             before(grammarAccess.getTypeRuleAccess().getPathAssignment_0()); 
            // InternalDTML.g:1453:2: ( rule__TypeRule__PathAssignment_0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_ID) ) {
                int LA17_1 = input.LA(2);

                if ( (LA17_1==28) ) {
                    alt17=1;
                }
            }
            switch (alt17) {
                case 1 :
                    // InternalDTML.g:1453:3: rule__TypeRule__PathAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__TypeRule__PathAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTypeRuleAccess().getPathAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__Group__0__Impl"


    // $ANTLR start "rule__TypeRule__Group__1"
    // InternalDTML.g:1461:1: rule__TypeRule__Group__1 : rule__TypeRule__Group__1__Impl ;
    public final void rule__TypeRule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1465:1: ( rule__TypeRule__Group__1__Impl )
            // InternalDTML.g:1466:2: rule__TypeRule__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TypeRule__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__Group__1"


    // $ANTLR start "rule__TypeRule__Group__1__Impl"
    // InternalDTML.g:1472:1: rule__TypeRule__Group__1__Impl : ( ( rule__TypeRule__TypeAssignment_1 ) ) ;
    public final void rule__TypeRule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1476:1: ( ( ( rule__TypeRule__TypeAssignment_1 ) ) )
            // InternalDTML.g:1477:1: ( ( rule__TypeRule__TypeAssignment_1 ) )
            {
            // InternalDTML.g:1477:1: ( ( rule__TypeRule__TypeAssignment_1 ) )
            // InternalDTML.g:1478:2: ( rule__TypeRule__TypeAssignment_1 )
            {
             before(grammarAccess.getTypeRuleAccess().getTypeAssignment_1()); 
            // InternalDTML.g:1479:2: ( rule__TypeRule__TypeAssignment_1 )
            // InternalDTML.g:1479:3: rule__TypeRule__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TypeRule__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeRuleAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__Group__1__Impl"


    // $ANTLR start "rule__MultiplicityRule__Group__0"
    // InternalDTML.g:1488:1: rule__MultiplicityRule__Group__0 : rule__MultiplicityRule__Group__0__Impl rule__MultiplicityRule__Group__1 ;
    public final void rule__MultiplicityRule__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1492:1: ( rule__MultiplicityRule__Group__0__Impl rule__MultiplicityRule__Group__1 )
            // InternalDTML.g:1493:2: rule__MultiplicityRule__Group__0__Impl rule__MultiplicityRule__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__MultiplicityRule__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__0"


    // $ANTLR start "rule__MultiplicityRule__Group__0__Impl"
    // InternalDTML.g:1500:1: rule__MultiplicityRule__Group__0__Impl : ( '[' ) ;
    public final void rule__MultiplicityRule__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1504:1: ( ( '[' ) )
            // InternalDTML.g:1505:1: ( '[' )
            {
            // InternalDTML.g:1505:1: ( '[' )
            // InternalDTML.g:1506:2: '['
            {
             before(grammarAccess.getMultiplicityRuleAccess().getLeftSquareBracketKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getMultiplicityRuleAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__0__Impl"


    // $ANTLR start "rule__MultiplicityRule__Group__1"
    // InternalDTML.g:1515:1: rule__MultiplicityRule__Group__1 : rule__MultiplicityRule__Group__1__Impl rule__MultiplicityRule__Group__2 ;
    public final void rule__MultiplicityRule__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1519:1: ( rule__MultiplicityRule__Group__1__Impl rule__MultiplicityRule__Group__2 )
            // InternalDTML.g:1520:2: rule__MultiplicityRule__Group__1__Impl rule__MultiplicityRule__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__MultiplicityRule__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__1"


    // $ANTLR start "rule__MultiplicityRule__Group__1__Impl"
    // InternalDTML.g:1527:1: rule__MultiplicityRule__Group__1__Impl : ( ( rule__MultiplicityRule__BoundsAssignment_1 ) ) ;
    public final void rule__MultiplicityRule__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1531:1: ( ( ( rule__MultiplicityRule__BoundsAssignment_1 ) ) )
            // InternalDTML.g:1532:1: ( ( rule__MultiplicityRule__BoundsAssignment_1 ) )
            {
            // InternalDTML.g:1532:1: ( ( rule__MultiplicityRule__BoundsAssignment_1 ) )
            // InternalDTML.g:1533:2: ( rule__MultiplicityRule__BoundsAssignment_1 )
            {
             before(grammarAccess.getMultiplicityRuleAccess().getBoundsAssignment_1()); 
            // InternalDTML.g:1534:2: ( rule__MultiplicityRule__BoundsAssignment_1 )
            // InternalDTML.g:1534:3: rule__MultiplicityRule__BoundsAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__BoundsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicityRuleAccess().getBoundsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__1__Impl"


    // $ANTLR start "rule__MultiplicityRule__Group__2"
    // InternalDTML.g:1542:1: rule__MultiplicityRule__Group__2 : rule__MultiplicityRule__Group__2__Impl rule__MultiplicityRule__Group__3 ;
    public final void rule__MultiplicityRule__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1546:1: ( rule__MultiplicityRule__Group__2__Impl rule__MultiplicityRule__Group__3 )
            // InternalDTML.g:1547:2: rule__MultiplicityRule__Group__2__Impl rule__MultiplicityRule__Group__3
            {
            pushFollow(FOLLOW_19);
            rule__MultiplicityRule__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__2"


    // $ANTLR start "rule__MultiplicityRule__Group__2__Impl"
    // InternalDTML.g:1554:1: rule__MultiplicityRule__Group__2__Impl : ( ( rule__MultiplicityRule__Group_2__0 )? ) ;
    public final void rule__MultiplicityRule__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1558:1: ( ( ( rule__MultiplicityRule__Group_2__0 )? ) )
            // InternalDTML.g:1559:1: ( ( rule__MultiplicityRule__Group_2__0 )? )
            {
            // InternalDTML.g:1559:1: ( ( rule__MultiplicityRule__Group_2__0 )? )
            // InternalDTML.g:1560:2: ( rule__MultiplicityRule__Group_2__0 )?
            {
             before(grammarAccess.getMultiplicityRuleAccess().getGroup_2()); 
            // InternalDTML.g:1561:2: ( rule__MultiplicityRule__Group_2__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==29) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDTML.g:1561:3: rule__MultiplicityRule__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__MultiplicityRule__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMultiplicityRuleAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__2__Impl"


    // $ANTLR start "rule__MultiplicityRule__Group__3"
    // InternalDTML.g:1569:1: rule__MultiplicityRule__Group__3 : rule__MultiplicityRule__Group__3__Impl ;
    public final void rule__MultiplicityRule__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1573:1: ( rule__MultiplicityRule__Group__3__Impl )
            // InternalDTML.g:1574:2: rule__MultiplicityRule__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__3"


    // $ANTLR start "rule__MultiplicityRule__Group__3__Impl"
    // InternalDTML.g:1580:1: rule__MultiplicityRule__Group__3__Impl : ( ']' ) ;
    public final void rule__MultiplicityRule__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1584:1: ( ( ']' ) )
            // InternalDTML.g:1585:1: ( ']' )
            {
            // InternalDTML.g:1585:1: ( ']' )
            // InternalDTML.g:1586:2: ']'
            {
             before(grammarAccess.getMultiplicityRuleAccess().getRightSquareBracketKeyword_3()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getMultiplicityRuleAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group__3__Impl"


    // $ANTLR start "rule__MultiplicityRule__Group_2__0"
    // InternalDTML.g:1596:1: rule__MultiplicityRule__Group_2__0 : rule__MultiplicityRule__Group_2__0__Impl rule__MultiplicityRule__Group_2__1 ;
    public final void rule__MultiplicityRule__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1600:1: ( rule__MultiplicityRule__Group_2__0__Impl rule__MultiplicityRule__Group_2__1 )
            // InternalDTML.g:1601:2: rule__MultiplicityRule__Group_2__0__Impl rule__MultiplicityRule__Group_2__1
            {
            pushFollow(FOLLOW_18);
            rule__MultiplicityRule__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group_2__0"


    // $ANTLR start "rule__MultiplicityRule__Group_2__0__Impl"
    // InternalDTML.g:1608:1: rule__MultiplicityRule__Group_2__0__Impl : ( '..' ) ;
    public final void rule__MultiplicityRule__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1612:1: ( ( '..' ) )
            // InternalDTML.g:1613:1: ( '..' )
            {
            // InternalDTML.g:1613:1: ( '..' )
            // InternalDTML.g:1614:2: '..'
            {
             before(grammarAccess.getMultiplicityRuleAccess().getFullStopFullStopKeyword_2_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getMultiplicityRuleAccess().getFullStopFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group_2__0__Impl"


    // $ANTLR start "rule__MultiplicityRule__Group_2__1"
    // InternalDTML.g:1623:1: rule__MultiplicityRule__Group_2__1 : rule__MultiplicityRule__Group_2__1__Impl ;
    public final void rule__MultiplicityRule__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1627:1: ( rule__MultiplicityRule__Group_2__1__Impl )
            // InternalDTML.g:1628:2: rule__MultiplicityRule__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group_2__1"


    // $ANTLR start "rule__MultiplicityRule__Group_2__1__Impl"
    // InternalDTML.g:1634:1: rule__MultiplicityRule__Group_2__1__Impl : ( ( rule__MultiplicityRule__BoundsAssignment_2_1 ) ) ;
    public final void rule__MultiplicityRule__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1638:1: ( ( ( rule__MultiplicityRule__BoundsAssignment_2_1 ) ) )
            // InternalDTML.g:1639:1: ( ( rule__MultiplicityRule__BoundsAssignment_2_1 ) )
            {
            // InternalDTML.g:1639:1: ( ( rule__MultiplicityRule__BoundsAssignment_2_1 ) )
            // InternalDTML.g:1640:2: ( rule__MultiplicityRule__BoundsAssignment_2_1 )
            {
             before(grammarAccess.getMultiplicityRuleAccess().getBoundsAssignment_2_1()); 
            // InternalDTML.g:1641:2: ( rule__MultiplicityRule__BoundsAssignment_2_1 )
            // InternalDTML.g:1641:3: rule__MultiplicityRule__BoundsAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicityRule__BoundsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicityRuleAccess().getBoundsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__Group_2__1__Impl"


    // $ANTLR start "rule__XDataType__NameAssignment_1"
    // InternalDTML.g:1650:1: rule__XDataType__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__XDataType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1654:1: ( ( RULE_ID ) )
            // InternalDTML.g:1655:2: ( RULE_ID )
            {
            // InternalDTML.g:1655:2: ( RULE_ID )
            // InternalDTML.g:1656:3: RULE_ID
            {
             before(grammarAccess.getXDataTypeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXDataTypeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__NameAssignment_1"


    // $ANTLR start "rule__XDataType__AttributesAssignment_3"
    // InternalDTML.g:1665:1: rule__XDataType__AttributesAssignment_3 : ( ruleProperty ) ;
    public final void rule__XDataType__AttributesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1669:1: ( ( ruleProperty ) )
            // InternalDTML.g:1670:2: ( ruleProperty )
            {
            // InternalDTML.g:1670:2: ( ruleProperty )
            // InternalDTML.g:1671:3: ruleProperty
            {
             before(grammarAccess.getXDataTypeAccess().getAttributesPropertyParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getXDataTypeAccess().getAttributesPropertyParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XDataType__AttributesAssignment_3"


    // $ANTLR start "rule__Property__NameAssignment_1"
    // InternalDTML.g:1680:1: rule__Property__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Property__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1684:1: ( ( RULE_ID ) )
            // InternalDTML.g:1685:2: ( RULE_ID )
            {
            // InternalDTML.g:1685:2: ( RULE_ID )
            // InternalDTML.g:1686:3: RULE_ID
            {
             before(grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__NameAssignment_1"


    // $ANTLR start "rule__Property__TypeAssignment_2_1_0"
    // InternalDTML.g:1695:1: rule__Property__TypeAssignment_2_1_0 : ( ruleTypeRule ) ;
    public final void rule__Property__TypeAssignment_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1699:1: ( ( ruleTypeRule ) )
            // InternalDTML.g:1700:2: ( ruleTypeRule )
            {
            // InternalDTML.g:1700:2: ( ruleTypeRule )
            // InternalDTML.g:1701:3: ruleTypeRule
            {
             before(grammarAccess.getPropertyAccess().getTypeTypeRuleParserRuleCall_2_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTypeRule();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getTypeTypeRuleParserRuleCall_2_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__TypeAssignment_2_1_0"


    // $ANTLR start "rule__Property__TypeUndefinedAssignment_2_1_1"
    // InternalDTML.g:1710:1: rule__Property__TypeUndefinedAssignment_2_1_1 : ( ( '<Undefined>' ) ) ;
    public final void rule__Property__TypeUndefinedAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1714:1: ( ( ( '<Undefined>' ) ) )
            // InternalDTML.g:1715:2: ( ( '<Undefined>' ) )
            {
            // InternalDTML.g:1715:2: ( ( '<Undefined>' ) )
            // InternalDTML.g:1716:3: ( '<Undefined>' )
            {
             before(grammarAccess.getPropertyAccess().getTypeUndefinedUndefinedKeyword_2_1_1_0()); 
            // InternalDTML.g:1717:3: ( '<Undefined>' )
            // InternalDTML.g:1718:4: '<Undefined>'
            {
             before(grammarAccess.getPropertyAccess().getTypeUndefinedUndefinedKeyword_2_1_1_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getTypeUndefinedUndefinedKeyword_2_1_1_0()); 

            }

             after(grammarAccess.getPropertyAccess().getTypeUndefinedUndefinedKeyword_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__TypeUndefinedAssignment_2_1_1"


    // $ANTLR start "rule__Property__MultiplicityAssignment_3"
    // InternalDTML.g:1729:1: rule__Property__MultiplicityAssignment_3 : ( ruleMultiplicityRule ) ;
    public final void rule__Property__MultiplicityAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1733:1: ( ( ruleMultiplicityRule ) )
            // InternalDTML.g:1734:2: ( ruleMultiplicityRule )
            {
            // InternalDTML.g:1734:2: ( ruleMultiplicityRule )
            // InternalDTML.g:1735:3: ruleMultiplicityRule
            {
             before(grammarAccess.getPropertyAccess().getMultiplicityMultiplicityRuleParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiplicityRule();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getMultiplicityMultiplicityRuleParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__MultiplicityAssignment_3"


    // $ANTLR start "rule__Property__ValueAssignment_4_1"
    // InternalDTML.g:1744:1: rule__Property__ValueAssignment_4_1 : ( ruleValue ) ;
    public final void rule__Property__ValueAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1748:1: ( ( ruleValue ) )
            // InternalDTML.g:1749:2: ( ruleValue )
            {
            // InternalDTML.g:1749:2: ( ruleValue )
            // InternalDTML.g:1750:3: ruleValue
            {
             before(grammarAccess.getPropertyAccess().getValueValueParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getValueValueParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__ValueAssignment_4_1"


    // $ANTLR start "rule__Property__CommentAssignment_5"
    // InternalDTML.g:1759:1: rule__Property__CommentAssignment_5 : ( RULE_VSL_COMMENT ) ;
    public final void rule__Property__CommentAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1763:1: ( ( RULE_VSL_COMMENT ) )
            // InternalDTML.g:1764:2: ( RULE_VSL_COMMENT )
            {
            // InternalDTML.g:1764:2: ( RULE_VSL_COMMENT )
            // InternalDTML.g:1765:3: RULE_VSL_COMMENT
            {
             before(grammarAccess.getPropertyAccess().getCommentVSL_COMMENTTerminalRuleCall_5_0()); 
            match(input,RULE_VSL_COMMENT,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getCommentVSL_COMMENTTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__CommentAssignment_5"


    // $ANTLR start "rule__XAssignment__NameAssignment_0"
    // InternalDTML.g:1774:1: rule__XAssignment__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__XAssignment__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1778:1: ( ( RULE_ID ) )
            // InternalDTML.g:1779:2: ( RULE_ID )
            {
            // InternalDTML.g:1779:2: ( RULE_ID )
            // InternalDTML.g:1780:3: RULE_ID
            {
             before(grammarAccess.getXAssignmentAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXAssignmentAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__NameAssignment_0"


    // $ANTLR start "rule__XAssignment__ExpressionAssignment_2"
    // InternalDTML.g:1789:1: rule__XAssignment__ExpressionAssignment_2 : ( ruleExpressionString ) ;
    public final void rule__XAssignment__ExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1793:1: ( ( ruleExpressionString ) )
            // InternalDTML.g:1794:2: ( ruleExpressionString )
            {
            // InternalDTML.g:1794:2: ( ruleExpressionString )
            // InternalDTML.g:1795:3: ruleExpressionString
            {
             before(grammarAccess.getXAssignmentAccess().getExpressionExpressionStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpressionString();

            state._fsp--;

             after(grammarAccess.getXAssignmentAccess().getExpressionExpressionStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XAssignment__ExpressionAssignment_2"


    // $ANTLR start "rule__Value__StrAssignment_0"
    // InternalDTML.g:1804:1: rule__Value__StrAssignment_0 : ( RULE_STRING ) ;
    public final void rule__Value__StrAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1808:1: ( ( RULE_STRING ) )
            // InternalDTML.g:1809:2: ( RULE_STRING )
            {
            // InternalDTML.g:1809:2: ( RULE_STRING )
            // InternalDTML.g:1810:3: RULE_STRING
            {
             before(grammarAccess.getValueAccess().getStrSTRINGTerminalRuleCall_0_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getValueAccess().getStrSTRINGTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__StrAssignment_0"


    // $ANTLR start "rule__Value__IvalAssignment_1"
    // InternalDTML.g:1819:1: rule__Value__IvalAssignment_1 : ( RULE_INT ) ;
    public final void rule__Value__IvalAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1823:1: ( ( RULE_INT ) )
            // InternalDTML.g:1824:2: ( RULE_INT )
            {
            // InternalDTML.g:1824:2: ( RULE_INT )
            // InternalDTML.g:1825:3: RULE_INT
            {
             before(grammarAccess.getValueAccess().getIvalINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getValueAccess().getIvalINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__IvalAssignment_1"


    // $ANTLR start "rule__Value__DvalAssignment_2"
    // InternalDTML.g:1834:1: rule__Value__DvalAssignment_2 : ( RULE_DOUBLE ) ;
    public final void rule__Value__DvalAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1838:1: ( ( RULE_DOUBLE ) )
            // InternalDTML.g:1839:2: ( RULE_DOUBLE )
            {
            // InternalDTML.g:1839:2: ( RULE_DOUBLE )
            // InternalDTML.g:1840:3: RULE_DOUBLE
            {
             before(grammarAccess.getValueAccess().getDvalDOUBLETerminalRuleCall_2_0()); 
            match(input,RULE_DOUBLE,FOLLOW_2); 
             after(grammarAccess.getValueAccess().getDvalDOUBLETerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__DvalAssignment_2"


    // $ANTLR start "rule__XEnumeration__NameAssignment_1"
    // InternalDTML.g:1849:1: rule__XEnumeration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__XEnumeration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1853:1: ( ( RULE_ID ) )
            // InternalDTML.g:1854:2: ( RULE_ID )
            {
            // InternalDTML.g:1854:2: ( RULE_ID )
            // InternalDTML.g:1855:3: RULE_ID
            {
             before(grammarAccess.getXEnumerationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXEnumerationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__NameAssignment_1"


    // $ANTLR start "rule__XEnumeration__LiteralsAssignment_3"
    // InternalDTML.g:1864:1: rule__XEnumeration__LiteralsAssignment_3 : ( ruleXEnumLiteral ) ;
    public final void rule__XEnumeration__LiteralsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1868:1: ( ( ruleXEnumLiteral ) )
            // InternalDTML.g:1869:2: ( ruleXEnumLiteral )
            {
            // InternalDTML.g:1869:2: ( ruleXEnumLiteral )
            // InternalDTML.g:1870:3: ruleXEnumLiteral
            {
             before(grammarAccess.getXEnumerationAccess().getLiteralsXEnumLiteralParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleXEnumLiteral();

            state._fsp--;

             after(grammarAccess.getXEnumerationAccess().getLiteralsXEnumLiteralParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumeration__LiteralsAssignment_3"


    // $ANTLR start "rule__XEnumLiteral__NameAssignment_0"
    // InternalDTML.g:1879:1: rule__XEnumLiteral__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__XEnumLiteral__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1883:1: ( ( RULE_ID ) )
            // InternalDTML.g:1884:2: ( RULE_ID )
            {
            // InternalDTML.g:1884:2: ( RULE_ID )
            // InternalDTML.g:1885:3: RULE_ID
            {
             before(grammarAccess.getXEnumLiteralAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXEnumLiteralAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__NameAssignment_0"


    // $ANTLR start "rule__XEnumLiteral__ValueAssignment_1_1"
    // InternalDTML.g:1894:1: rule__XEnumLiteral__ValueAssignment_1_1 : ( ruleValue ) ;
    public final void rule__XEnumLiteral__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1898:1: ( ( ruleValue ) )
            // InternalDTML.g:1899:2: ( ruleValue )
            {
            // InternalDTML.g:1899:2: ( ruleValue )
            // InternalDTML.g:1900:3: ruleValue
            {
             before(grammarAccess.getXEnumLiteralAccess().getValueValueParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getXEnumLiteralAccess().getValueValueParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__ValueAssignment_1_1"


    // $ANTLR start "rule__XEnumLiteral__CommentAssignment_2"
    // InternalDTML.g:1909:1: rule__XEnumLiteral__CommentAssignment_2 : ( RULE_VSL_COMMENT ) ;
    public final void rule__XEnumLiteral__CommentAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1913:1: ( ( RULE_VSL_COMMENT ) )
            // InternalDTML.g:1914:2: ( RULE_VSL_COMMENT )
            {
            // InternalDTML.g:1914:2: ( RULE_VSL_COMMENT )
            // InternalDTML.g:1915:3: RULE_VSL_COMMENT
            {
             before(grammarAccess.getXEnumLiteralAccess().getCommentVSL_COMMENTTerminalRuleCall_2_0()); 
            match(input,RULE_VSL_COMMENT,FOLLOW_2); 
             after(grammarAccess.getXEnumLiteralAccess().getCommentVSL_COMMENTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XEnumLiteral__CommentAssignment_2"


    // $ANTLR start "rule__QualifiedName__PathAssignment_0"
    // InternalDTML.g:1924:1: rule__QualifiedName__PathAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__QualifiedName__PathAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1928:1: ( ( ( RULE_ID ) ) )
            // InternalDTML.g:1929:2: ( ( RULE_ID ) )
            {
            // InternalDTML.g:1929:2: ( ( RULE_ID ) )
            // InternalDTML.g:1930:3: ( RULE_ID )
            {
             before(grammarAccess.getQualifiedNameAccess().getPathNamespaceCrossReference_0_0()); 
            // InternalDTML.g:1931:3: ( RULE_ID )
            // InternalDTML.g:1932:4: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getPathNamespaceIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getPathNamespaceIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getQualifiedNameAccess().getPathNamespaceCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__PathAssignment_0"


    // $ANTLR start "rule__QualifiedName__RemainingAssignment_2"
    // InternalDTML.g:1943:1: rule__QualifiedName__RemainingAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__QualifiedName__RemainingAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1947:1: ( ( ruleQualifiedName ) )
            // InternalDTML.g:1948:2: ( ruleQualifiedName )
            {
            // InternalDTML.g:1948:2: ( ruleQualifiedName )
            // InternalDTML.g:1949:3: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameAccess().getRemainingQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameAccess().getRemainingQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__RemainingAssignment_2"


    // $ANTLR start "rule__TypeRule__PathAssignment_0"
    // InternalDTML.g:1958:1: rule__TypeRule__PathAssignment_0 : ( ruleQualifiedName ) ;
    public final void rule__TypeRule__PathAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1962:1: ( ( ruleQualifiedName ) )
            // InternalDTML.g:1963:2: ( ruleQualifiedName )
            {
            // InternalDTML.g:1963:2: ( ruleQualifiedName )
            // InternalDTML.g:1964:3: ruleQualifiedName
            {
             before(grammarAccess.getTypeRuleAccess().getPathQualifiedNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getTypeRuleAccess().getPathQualifiedNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__PathAssignment_0"


    // $ANTLR start "rule__TypeRule__TypeAssignment_1"
    // InternalDTML.g:1973:1: rule__TypeRule__TypeAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__TypeRule__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1977:1: ( ( ( RULE_ID ) ) )
            // InternalDTML.g:1978:2: ( ( RULE_ID ) )
            {
            // InternalDTML.g:1978:2: ( ( RULE_ID ) )
            // InternalDTML.g:1979:3: ( RULE_ID )
            {
             before(grammarAccess.getTypeRuleAccess().getTypeTypeCrossReference_1_0()); 
            // InternalDTML.g:1980:3: ( RULE_ID )
            // InternalDTML.g:1981:4: RULE_ID
            {
             before(grammarAccess.getTypeRuleAccess().getTypeTypeIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTypeRuleAccess().getTypeTypeIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getTypeRuleAccess().getTypeTypeCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TypeRule__TypeAssignment_1"


    // $ANTLR start "rule__MultiplicityRule__BoundsAssignment_1"
    // InternalDTML.g:1992:1: rule__MultiplicityRule__BoundsAssignment_1 : ( ruleBoundSpecification ) ;
    public final void rule__MultiplicityRule__BoundsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:1996:1: ( ( ruleBoundSpecification ) )
            // InternalDTML.g:1997:2: ( ruleBoundSpecification )
            {
            // InternalDTML.g:1997:2: ( ruleBoundSpecification )
            // InternalDTML.g:1998:3: ruleBoundSpecification
            {
             before(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBoundSpecification();

            state._fsp--;

             after(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__BoundsAssignment_1"


    // $ANTLR start "rule__MultiplicityRule__BoundsAssignment_2_1"
    // InternalDTML.g:2007:1: rule__MultiplicityRule__BoundsAssignment_2_1 : ( ruleBoundSpecification ) ;
    public final void rule__MultiplicityRule__BoundsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:2011:1: ( ( ruleBoundSpecification ) )
            // InternalDTML.g:2012:2: ( ruleBoundSpecification )
            {
            // InternalDTML.g:2012:2: ( ruleBoundSpecification )
            // InternalDTML.g:2013:3: ruleBoundSpecification
            {
             before(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBoundSpecification();

            state._fsp--;

             after(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicityRule__BoundsAssignment_2_1"


    // $ANTLR start "rule__BoundSpecification__ValueAssignment"
    // InternalDTML.g:2022:1: rule__BoundSpecification__ValueAssignment : ( ruleUnlimitedLiteral ) ;
    public final void rule__BoundSpecification__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDTML.g:2026:1: ( ( ruleUnlimitedLiteral ) )
            // InternalDTML.g:2027:2: ( ruleUnlimitedLiteral )
            {
            // InternalDTML.g:2027:2: ( ruleUnlimitedLiteral )
            // InternalDTML.g:2028:3: ruleUnlimitedLiteral
            {
             before(grammarAccess.getBoundSpecificationAccess().getValueUnlimitedLiteralParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleUnlimitedLiteral();

            state._fsp--;

             after(grammarAccess.getBoundSpecificationAccess().getValueUnlimitedLiteralParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BoundSpecification__ValueAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000F0072L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000003010080L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000160L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000F0070L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000080L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000100020L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000020020000L});

}