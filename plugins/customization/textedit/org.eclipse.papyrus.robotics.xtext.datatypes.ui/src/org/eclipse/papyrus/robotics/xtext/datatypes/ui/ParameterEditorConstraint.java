package org.eclipse.papyrus.robotics.xtext.datatypes.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.gmfdiag.extensionpoints.editors.configuration.IDirectEditorConstraint;
import org.eclipse.papyrus.robotics.bt.profile.bt.Parameter;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ValueSpecificationAction;

public class ParameterEditorConstraint implements IDirectEditorConstraint {

	/**
	 * @see org.eclipse.papyrus.extensionpoints.editors.configuration.IDirectEditorConstraint#getLabel()
	 *
	 * @return label
	 */
	public String getLabel() {
		return "Parameter"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.papyrus.extensionpoints.editors.configuration.IDirectEditorConstraint#appliesTo(java.lang.Object)
	 *
	 * @param selection
	 * @return true, if selection corresponds to a Parameter
	 */
	public boolean appliesTo(Object selection) {
		EObject resolvedEObject = EMFHelper.getEObject(selection);
		if (resolvedEObject instanceof ValueSpecificationAction) {
			ValueSpecificationAction selectedVsa = (ValueSpecificationAction) resolvedEObject;
			if (StereotypeUtil.isApplied(selectedVsa, Parameter.class)) {
				return true;
			}
		}
		return false;
	}
}
