/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) <ansgar.radermache@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.xtext.datatypes.ui.contribution;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.DataAttribute;
import org.eclipse.papyrus.robotics.xtext.datatypes.dTML.Property;
import org.eclipse.papyrus.robotics.xtext.datatypes.dTML.XDataType;
import org.eclipse.papyrus.robotics.xtext.datatypes.ui.internal.DatatypesActivator;
import org.eclipse.papyrus.robotics.xtext.util.TrackNames;
import org.eclipse.papyrus.robotics.xtext.util.UpdateContextAdapter;
import org.eclipse.papyrus.uml.textedit.common.xtext.umlCommon.MultiplicityRule;
import org.eclipse.papyrus.uml.textedit.common.xtext.umlCommon.TypeRule;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.papyrus.uml.xtext.integration.AbstractXtextDirectEditorConfiguration;
import org.eclipse.papyrus.uml.xtext.integration.InvalidStringUtil;
import org.eclipse.papyrus.uml.xtext.integration.core.ContextElementAdapter.IContextElementProvider;
import org.eclipse.swt.SWT;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.UMLFactory;

import com.google.common.collect.Lists;
import com.google.inject.Injector;

public class DataTypeEditorConfigurationContribution extends AbstractXtextDirectEditorConfiguration {

	TrackNames<org.eclipse.uml2.uml.Property> lastAttributes;

	protected class UpdateDataTypeCommand extends AbstractTransactionalCommand {

		private DataType dt;
		private XDataType xDT;

		public UpdateDataTypeCommand(TransactionalEditingDomain domain, DataType dt, XDataType xDT) {
			super(domain, "Update datatype", getWorkspaceFiles(dt)); //$NON-NLS-1$
			this.dt = dt;
			this.xDT = xDT;
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {
			dt.setName(xDT.getName());
			// remove all elements (those that are retained are added later again).
			dt.getOwnedAttributes().clear();
			for (Property dtAttribute : xDT.getAttributes()) {
				TypeRule typeRule = dtAttribute.getType();
				MultiplicityRule mRule = dtAttribute.getMultiplicity();
				org.eclipse.uml2.uml.Property umlAttribute = lastAttributes.get(dtAttribute.getName());
				if (umlAttribute == null) {
					// does not exist, create
					umlAttribute = UMLFactory.eINSTANCE.createProperty();
				}
				umlAttribute.setName(dtAttribute.getName());
				umlAttribute.setType(typeRule != null ? typeRule.getType() : null);
				if (mRule != null) {
					if (mRule.getBounds().size() > 0) {
						String upperStr = mRule.getBounds().get(0).getValue();
						umlAttribute.setLower(0);
						if (upperStr.equals("*")) { //$NON-NLS-1$
							umlAttribute.setUpper(-1);
						}
						else {
							umlAttribute.setUpper(Integer.parseInt(upperStr));
						}
					}
				}
				else {
					umlAttribute.setLower(1);
					umlAttribute.setUpper(1);
				}
				// add it to attribute list, once named and typed (avoid editor refreshes with incomplete types)
				dt.getOwnedAttributes().add(umlAttribute);
				DataAttribute da = StereotypeUtil.applyApp(umlAttribute, DataAttribute.class);
				
				String comment = dtAttribute.getComment();
				if (da != null && comment != null) {
					comment = comment.substring(2).trim();
					da.setDescription(comment);
				}
			}
			return CommandResult.newOKCommandResult(dt);
		}
	}

	/**
	 * From data type to an Xtext expression
	 */
	@Override
	public String getTextToEdit(Object editedObject) {
		if (editedObject instanceof DataType) {
			DataType dt = (DataType) editedObject;
			String invalidStr = InvalidStringUtil.getTextualRepresentation(dt);
			if (invalidStr != null) {
				return invalidStr;
			}
			return UnparseDT.getDTtext(dt).toString();
		}
		return UnparseDT.WRONG_OBJECT;
	}

	/**
	 * {@inheritDoc}
	 * Initialize lastNames map
	 */
	@Override
	public Object preEditAction(Object objectToEdit) {
		if (objectToEdit instanceof DataType) {
			// fill map with existing attribute and port names
			DataType umlDt= (DataType) objectToEdit;
			// fill attribute map
			lastAttributes = new TrackNames<org.eclipse.uml2.uml.Property>(umlDt.getOwnedAttributes());
		}
		return super.preEditAction(objectToEdit);
	}
	
	@Override
	public IContextElementProvider getContextProvider() {
		return new UpdateContextAdapter(objectToEdit) {

			@Override
			public void updateLastNames() {
				XDataType dtm = (XDataType) xtextResource.getContents().get(0);

				List<String> attrNameList = new ArrayList<String>();
				for (Property dtAttribute : dtm.getAttributes()) {
					attrNameList.add(dtAttribute.getName());
				}
				lastAttributes.update(attrNameList);
			}
		};
	}
	
	@Override
	public int getStyle() {
		return SWT.MULTI | SWT.WRAP;
	}

	@Override
	public Injector getInjector() {
		return DatatypesActivator.getInstance().getInjector(DatatypesActivator.ORG_ECLIPSE_PAPYRUS_ROBOTICS_XTEXT_DATATYPES_DTML);
	}

	/**
	 * From Xtext expression to LiteralString (ValueSpecificationAction's defaultValue)
	 */
	@Override
	public ICommand getParseCommand(EObject umlObject, EObject xtextObject) {
		TransactionalEditingDomain dom = TransactionUtil.getEditingDomain(umlObject);
		if (umlObject instanceof DataType) {
			DataType dt = (DataType) umlObject;
			if (xtextObject instanceof XDataType) {
				CompositeCommand cc = new CompositeCommand("Update data type"); //$NON-NLS-1$
				// use "proper" destroy commands (calling element.destroy in the recording command leads to
				// artefacts in the diagram)
				for (org.eclipse.uml2.uml.Property umlAttribute : Lists.newArrayList(dt.getOwnedAttributes())) {
					if (!lastAttributes.containsValue(umlAttribute)) {
						// dt.getOwnedAttributes().remove(umlAttribute);
						DestroyElementRequest destroyReq = new DestroyElementRequest(umlAttribute,  false);
						ICommand destroyCmd = ElementEditServiceUtils.getCommandProvider(dt).getEditCommand(destroyReq);
						cc.add(destroyCmd);
					}
				}
				UpdateDataTypeCommand updateCommand = new UpdateDataTypeCommand(dom, dt, (XDataType) xtextObject);
				cc.add(updateCommand);
				return cc;
			}
		}
		return null;
	}
}
