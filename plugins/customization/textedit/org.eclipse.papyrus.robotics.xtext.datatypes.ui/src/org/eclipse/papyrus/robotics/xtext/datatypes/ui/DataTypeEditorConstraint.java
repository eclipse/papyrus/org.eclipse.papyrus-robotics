package org.eclipse.papyrus.robotics.xtext.datatypes.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.gmfdiag.extensionpoints.editors.configuration.IDirectEditorConstraint;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.DataType;

public class DataTypeEditorConstraint implements IDirectEditorConstraint {

	/**
	 * @see org.eclipse.papyrus.extensionpoints.editors.configuration.IDirectEditorConstraint#getLabel()
	 *
	 * @return label
	 */
	public String getLabel() {
		return "DataType"; //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.papyrus.extensionpoints.editors.configuration.IDirectEditorConstraint#appliesTo(java.lang.Object)
	 *
	 * @param selection
	 * @return true, if selection is commObject or DataType
	 */
	public boolean appliesTo(Object selection) {
		EObject resolvedEObject = EMFHelper.getEObject(selection);
		if (resolvedEObject instanceof DataType) {
			DataType dt = (DataType) resolvedEObject;
			if (StereotypeUtil.isApplied(dt, org.eclipse.papyrus.robotics.profile.robotics.commobject.DataType.class) ||
					StereotypeUtil.isApplied(dt, CommunicationObject.class)) {
				// editor does not support enumerations
				if (!StereotypeUtil.isApplied(dt, org.eclipse.papyrus.robotics.profile.robotics.commobject.Enumeration.class)) {
					return true;
				}
			}
		}
		return false;
	}
}
