/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) <ansgar.radermache@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.xtext.datatypes.ui.contribution;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.robotics.xtext.datatypes.dTML.XEnumLiteral;
import org.eclipse.papyrus.robotics.xtext.datatypes.dTML.XEnumeration;
import org.eclipse.papyrus.robotics.xtext.datatypes.ui.internal.DatatypesActivator;
import org.eclipse.papyrus.robotics.xtext.util.TrackNames;
import org.eclipse.papyrus.robotics.xtext.util.UpdateContextAdapter;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.papyrus.uml.xtext.integration.AbstractXtextDirectEditorConfiguration;
import org.eclipse.papyrus.uml.xtext.integration.InvalidStringUtil;
import org.eclipse.papyrus.uml.xtext.integration.core.ContextElementAdapter.IContextElementProvider;
import org.eclipse.swt.SWT;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;

import com.google.common.collect.Lists;
import com.google.inject.Injector;

public class EnumerationEditorConfigurationContribution extends AbstractXtextDirectEditorConfiguration {

	TrackNames<org.eclipse.uml2.uml.EnumerationLiteral> lastEnumLiterals;

	protected class UpdateEnumerationCommand extends AbstractTransactionalCommand {

		private Enumeration enumeration;
		private XEnumeration xEnum;

		public UpdateEnumerationCommand(TransactionalEditingDomain domain, Enumeration enumeration, XEnumeration xEnum) {
			super(domain, "Update enumeration", getWorkspaceFiles(enumeration)); //$NON-NLS-1$
			this.enumeration = enumeration;
			this.xEnum = xEnum;
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {
			enumeration.setName(xEnum.getName());
			// remove all elements (those that are retained are added later again).
			enumeration.getOwnedLiterals().clear();
			for (XEnumLiteral xEnumLiteral : xEnum.getLiterals()) {
				EnumerationLiteral enumLiteral = lastEnumLiterals.get(xEnumLiteral.getName());
				if (enumLiteral == null) {
					// does not exist, create
					enumLiteral = UMLFactory.eINSTANCE.createEnumerationLiteral();
				}
				enumLiteral.setName(xEnumLiteral.getName());
				// add it to literal list, once named and typed
				enumeration.getOwnedLiterals().add(enumLiteral);
				org.eclipse.papyrus.robotics.profile.robotics.commobject.EnumerationLiteral stEnumLiteral = StereotypeUtil.applyApp(enumLiteral, org.eclipse.papyrus.robotics.profile.robotics.commobject.EnumerationLiteral.class);

				String comment = xEnumLiteral.getComment();
				if (stEnumLiteral != null && comment != null) {
					comment = comment.substring(2).trim();
					stEnumLiteral.setDescription(comment);
				}
				if (xEnumLiteral.getValue() != null) {
					int value = xEnumLiteral.getValue().getIval();
					ValueSpecification vs = enumLiteral.getSpecification();
					if (vs instanceof LiteralInteger) {
						((LiteralInteger) vs).setValue(value);
					} else {
						if (vs != null) {
							vs.destroy();
						}
						LiteralInteger li = UMLFactory.eINSTANCE.createLiteralInteger();
						li.setValue(value);
						enumLiteral.setSpecification(li);
					}
				} else {
					ValueSpecification vs = enumLiteral.getSpecification();
					if (vs != null) {
						vs.destroy();
					}
				}
			}
			return CommandResult.newOKCommandResult(enumeration);
		}
	}

	/**
	 * From enumeration to an Xtext expression
	 */
	@Override
	public String getTextToEdit(Object editedObject) {
		if (editedObject instanceof Enumeration) {
			Enumeration umlEnum = (Enumeration) editedObject;
			String invalidStr = InvalidStringUtil.getTextualRepresentation(umlEnum);
			if (invalidStr != null) {
				return invalidStr;
			}
			return UnparseDT.getEnumText(umlEnum).toString();
		}
		return UnparseDT.WRONG_OBJECT;
	}

	/**
	 * {@inheritDoc}
	 * Initialize lastNames map
	 */
	@Override
	public Object preEditAction(Object objectToEdit) {
		if (objectToEdit instanceof Enumeration) {
			// fill map with existing attribute and port names
			Enumeration umlEnum = (Enumeration) objectToEdit;
			// fill attribute map
			lastEnumLiterals = new TrackNames<org.eclipse.uml2.uml.EnumerationLiteral>(umlEnum.getOwnedLiterals());
		}
		return super.preEditAction(objectToEdit);
	}

	@Override
	public IContextElementProvider getContextProvider() {
		return new UpdateContextAdapter(objectToEdit) {

			@Override
			public void updateLastNames() {
				XEnumeration xEnum = (XEnumeration) xtextResource.getContents().get(0);

				List<String> attrNameList = new ArrayList<String>();
				for (XEnumLiteral xEnumLiteral : xEnum.getLiterals()) {
					attrNameList.add(xEnumLiteral.getName());
				}
				lastEnumLiterals.update(attrNameList);
			}
		};
	}

	@Override
	public int getStyle() {
		return SWT.MULTI | SWT.WRAP;
	}

	@Override
	public Injector getInjector() {
		return DatatypesActivator.getInstance().getInjector(DatatypesActivator.ORG_ECLIPSE_PAPYRUS_ROBOTICS_XTEXT_DATATYPES_DTML);
	}

	/**
	 * From Xtext expression to LiteralString (ValueSpecificationAction's defaultValue)
	 */
	@Override
	public ICommand getParseCommand(EObject umlObject, EObject xtextObject) {
		TransactionalEditingDomain dom = TransactionUtil.getEditingDomain(umlObject);
		if (umlObject instanceof Enumeration) {
			Enumeration enumeration = (Enumeration) umlObject;
			if (xtextObject instanceof XEnumeration) {
				CompositeCommand cc = new CompositeCommand("Update enumeration"); //$NON-NLS-1$
				// use "proper" destroy commands (calling element.destroy in the recording command leads to
				// artefacts in the diagram)
				for (org.eclipse.uml2.uml.EnumerationLiteral umlEnumLiteral : Lists.newArrayList(enumeration.getOwnedLiterals())) {
					if (!lastEnumLiterals.containsValue(umlEnumLiteral)) {
						// dt.getOwnedAttributes().remove(umlAttribute);
						DestroyElementRequest destroyReq = new DestroyElementRequest(umlEnumLiteral, false);
						ICommand destroyCmd = ElementEditServiceUtils.getCommandProvider(enumeration).getEditCommand(destroyReq);
						cc.add(destroyCmd);
					}
				}
				UpdateEnumerationCommand updateCommand = new UpdateEnumerationCommand(dom, enumeration, (XEnumeration) xtextObject);
				cc.add(updateCommand);
				return cc;
			}
		}
		return null;
	}
}
