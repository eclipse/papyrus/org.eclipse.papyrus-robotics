/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.xtext.datatypes.ui.contribution;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.papyrus.robotics.xtext.datatypes.dTML.XAssignment;
import org.eclipse.papyrus.robotics.xtext.datatypes.ui.internal.DatatypesActivator;
import org.eclipse.papyrus.uml.xtext.integration.AbstractXtextDirectEditorConfiguration;
import org.eclipse.papyrus.uml.xtext.integration.InvalidStringUtil;
import org.eclipse.swt.SWT;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecificationAction;

import com.google.inject.Injector;

public class ParameterEditorConfigurationContribution extends AbstractXtextDirectEditorConfiguration {

	protected static class UpdateValueSpecificationCommand extends AbstractTransactionalCommand {

		private ValueSpecificationAction action;
		private XAssignment assign;

		public UpdateValueSpecificationCommand(TransactionalEditingDomain domain, ValueSpecificationAction action, XAssignment assign) {
			super(domain, "Assign LiteralString to UML::ValueSpecificationAction", getWorkspaceFiles(action)); //$NON-NLS-1$
			this.action = action;
			this.assign = assign;
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info)
				throws ExecutionException {
			if (assign != null &&
					assign.getName() != null &&
					assign.getExpression() != null) {
				// name
				action.setName(assign.getName());
				// expression
				LiteralString ls = null;
				if (action.getValue() instanceof LiteralString)
					ls = (LiteralString) action.getValue();
				else
					ls = (LiteralString) action.createValue(null, null, UMLPackage.eINSTANCE.getLiteralString());
				ls.setValue(assign.getExpression());
				action.setValue(ls);
			}
			return CommandResult.newOKCommandResult(action);
		}
	}

	/**
	 * From LiteralString (ValueSpecificationAction's defaultValue) to Xtext expression
	 */
	@Override
	public String getTextToEdit(Object editedObject) {
		if (editedObject instanceof ValueSpecificationAction) {
			ValueSpecificationAction action = (ValueSpecificationAction) editedObject;
			String invalidStr = InvalidStringUtil.getTextualRepresentation(action);
			if (invalidStr != null) {
				return invalidStr;
			}
			String textToEdit = action.getName() + " = "; //$NON-NLS-1$
			if (action.getValue() instanceof LiteralString) {
				LiteralString ls = (LiteralString) action.getValue();
				textToEdit = textToEdit + ls.getValue() + ";"; //$NON-NLS-1$
			}
			return textToEdit;
		}
		return UnparseDT.WRONG_OBJECT;
	}

	@Override
	public int getStyle() {
		return SWT.MULTI | SWT.WRAP;
	}

	@Override
	public Injector getInjector() {
		return DatatypesActivator.getInstance().getInjector(DatatypesActivator.ORG_ECLIPSE_PAPYRUS_ROBOTICS_XTEXT_DATATYPES_DTML);
	}

	/**
	 * From Xtext expression to LiteralString (ValueSpecificationAction's defaultValue)
	 */
	@Override
	public ICommand getParseCommand(EObject umlObject, EObject xtextObject) {
		TransactionalEditingDomain dom = TransactionUtil.getEditingDomain(umlObject);
		if (umlObject instanceof ValueSpecificationAction) {
			ValueSpecificationAction vsa = (ValueSpecificationAction) umlObject;
			if (xtextObject instanceof XAssignment) {
				UpdateValueSpecificationCommand updateCommand = new UpdateValueSpecificationCommand(dom, vsa, (XAssignment) xtextObject);
				return updateCommand;
			}
		}
		return null;
	}
}
