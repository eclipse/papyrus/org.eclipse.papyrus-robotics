/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) <ansgar.radermache@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.xtext.datatypes.ui.contribution

import org.eclipse.uml2.uml.DataType
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.Property
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.uml2.uml.ValueSpecification

/**
 * Provide textual representation of a data-type
 */
class UnparseDT {
	public static final String WRONG_OBJECT = "Unexpected editor object";

	def static getDTtext(DataType dt) '''
		«dt.commObjOrDT» «dt.name» {
			«FOR attribute : dt.ownedAttributes»
				attribute «attribute.name» : «attribute.type.undefOrQName»«attribute.multiplicity»«attribute.comment»
			«ENDFOR»
		}
	'''

	def static getEnumText(Enumeration enumeration) '''
		Enumeration «enumeration.name» {
			«FOR literal : enumeration.ownedLiterals»
				«literal.name»«literal.defaultValue»«literal.comment»
			«ENDFOR»
		}
	'''

	def static defaultValue(EnumerationLiteral literal) {
		if (literal.specification instanceof ValueSpecification) {
			return " = " + literal.specification.stringValue
		}
		return ""
	}

	def static getMultiplicity(Property attribute) {
		if (attribute.upper == -1) {
			return " [*]"
		} else if (attribute.upper != 1) {
			return ''' [«attribute.upper»]'''
		}
		return ""
	}

	def static getComment(Element element) {
		val entity = UMLUtil.getStereotypeApplication(element, Entity);
		if (entity !== null && entity.description !== null) {
			return '''	// «entity.description»'''
		}
		return ""
	}

	def static undefOrQName(Type type) {
		if (type === null) {
			return "<Undefined>";
		} else {
			return type.qualifiedName
		}
	}

	def static commObjOrDT(DataType dt) {
		if (StereotypeUtil.isApplied(dt, CommunicationObject)) {
			"CommObject"
		} else {
			"DataType"
		}
	}
}
