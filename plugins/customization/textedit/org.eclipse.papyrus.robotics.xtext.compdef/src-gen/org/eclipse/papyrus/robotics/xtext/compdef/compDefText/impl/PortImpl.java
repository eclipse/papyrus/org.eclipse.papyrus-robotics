/**
 * generated by Xtext 2.29.0
 */
package org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.xtext.compdef.compDefText.CompDefTextPackage;
import org.eclipse.papyrus.robotics.xtext.compdef.compDefText.Port;

import org.eclipse.papyrus.uml.textedit.common.xtext.umlCommon.TypeRule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl.PortImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl.PortImpl#getProv <em>Prov</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl.PortImpl#getReq <em>Req</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortImpl extends MinimalEObjectImpl.Container implements Port
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getProv() <em>Prov</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProv()
   * @generated
   * @ordered
   */
  protected TypeRule prov;

  /**
   * The cached value of the '{@link #getReq() <em>Req</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReq()
   * @generated
   * @ordered
   */
  protected TypeRule req;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PortImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CompDefTextPackage.Literals.PORT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CompDefTextPackage.PORT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeRule getProv()
  {
    return prov;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetProv(TypeRule newProv, NotificationChain msgs)
  {
    TypeRule oldProv = prov;
    prov = newProv;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CompDefTextPackage.PORT__PROV, oldProv, newProv);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setProv(TypeRule newProv)
  {
    if (newProv != prov)
    {
      NotificationChain msgs = null;
      if (prov != null)
        msgs = ((InternalEObject)prov).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CompDefTextPackage.PORT__PROV, null, msgs);
      if (newProv != null)
        msgs = ((InternalEObject)newProv).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CompDefTextPackage.PORT__PROV, null, msgs);
      msgs = basicSetProv(newProv, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CompDefTextPackage.PORT__PROV, newProv, newProv));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public TypeRule getReq()
  {
    return req;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReq(TypeRule newReq, NotificationChain msgs)
  {
    TypeRule oldReq = req;
    req = newReq;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CompDefTextPackage.PORT__REQ, oldReq, newReq);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setReq(TypeRule newReq)
  {
    if (newReq != req)
    {
      NotificationChain msgs = null;
      if (req != null)
        msgs = ((InternalEObject)req).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CompDefTextPackage.PORT__REQ, null, msgs);
      if (newReq != null)
        msgs = ((InternalEObject)newReq).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CompDefTextPackage.PORT__REQ, null, msgs);
      msgs = basicSetReq(newReq, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CompDefTextPackage.PORT__REQ, newReq, newReq));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CompDefTextPackage.PORT__PROV:
        return basicSetProv(null, msgs);
      case CompDefTextPackage.PORT__REQ:
        return basicSetReq(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CompDefTextPackage.PORT__NAME:
        return getName();
      case CompDefTextPackage.PORT__PROV:
        return getProv();
      case CompDefTextPackage.PORT__REQ:
        return getReq();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CompDefTextPackage.PORT__NAME:
        setName((String)newValue);
        return;
      case CompDefTextPackage.PORT__PROV:
        setProv((TypeRule)newValue);
        return;
      case CompDefTextPackage.PORT__REQ:
        setReq((TypeRule)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CompDefTextPackage.PORT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case CompDefTextPackage.PORT__PROV:
        setProv((TypeRule)null);
        return;
      case CompDefTextPackage.PORT__REQ:
        setReq((TypeRule)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CompDefTextPackage.PORT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case CompDefTextPackage.PORT__PROV:
        return prov != null;
      case CompDefTextPackage.PORT__REQ:
        return req != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //PortImpl
