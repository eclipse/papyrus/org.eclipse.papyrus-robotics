package org.eclipse.papyrus.robotics.xtext.compdef.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.papyrus.robotics.xtext.compdef.services.CompDefGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCompDefParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_VSL_COMMENT", "RULE_STRING", "RULE_INT", "RULE_DOUBLE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_INTEGER_VALUE", "RULE_WS", "RULE_ANY_OTHER", "'component'", "'{'", "'}'", "'parameter'", "':'", "'<Undefined>'", "'='", "'port'", "'provides'", "'requires'", "'activity'", "'::'", "'['", "'..'", "']'", "'*'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int RULE_DOUBLE=8;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_VSL_COMMENT=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_INTEGER_VALUE=11;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalCompDefParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalCompDefParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalCompDefParser.tokenNames; }
    public String getGrammarFileName() { return "InternalCompDef.g"; }



     	private CompDefGrammarAccess grammarAccess;

        public InternalCompDefParser(TokenStream input, CompDefGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Component";
       	}

       	@Override
       	protected CompDefGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleComponent"
    // InternalCompDef.g:65:1: entryRuleComponent returns [EObject current=null] : iv_ruleComponent= ruleComponent EOF ;
    public final EObject entryRuleComponent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponent = null;


        try {
            // InternalCompDef.g:65:50: (iv_ruleComponent= ruleComponent EOF )
            // InternalCompDef.g:66:2: iv_ruleComponent= ruleComponent EOF
            {
             newCompositeNode(grammarAccess.getComponentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComponent=ruleComponent();

            state._fsp--;

             current =iv_ruleComponent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponent"


    // $ANTLR start "ruleComponent"
    // InternalCompDef.g:72:1: ruleComponent returns [EObject current=null] : (otherlv_0= 'component' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ports_3_0= rulePort ) )* ( (lv_activity_4_0= ruleActivity ) )* ( (lv_parameters_5_0= ruleParameter ) )* otherlv_6= '}' ) ;
    public final EObject ruleComponent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_6=null;
        EObject lv_ports_3_0 = null;

        EObject lv_activity_4_0 = null;

        EObject lv_parameters_5_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:78:2: ( (otherlv_0= 'component' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ports_3_0= rulePort ) )* ( (lv_activity_4_0= ruleActivity ) )* ( (lv_parameters_5_0= ruleParameter ) )* otherlv_6= '}' ) )
            // InternalCompDef.g:79:2: (otherlv_0= 'component' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ports_3_0= rulePort ) )* ( (lv_activity_4_0= ruleActivity ) )* ( (lv_parameters_5_0= ruleParameter ) )* otherlv_6= '}' )
            {
            // InternalCompDef.g:79:2: (otherlv_0= 'component' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ports_3_0= rulePort ) )* ( (lv_activity_4_0= ruleActivity ) )* ( (lv_parameters_5_0= ruleParameter ) )* otherlv_6= '}' )
            // InternalCompDef.g:80:3: otherlv_0= 'component' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_ports_3_0= rulePort ) )* ( (lv_activity_4_0= ruleActivity ) )* ( (lv_parameters_5_0= ruleParameter ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getComponentAccess().getComponentKeyword_0());
            		
            // InternalCompDef.g:84:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalCompDef.g:85:4: (lv_name_1_0= RULE_ID )
            {
            // InternalCompDef.g:85:4: (lv_name_1_0= RULE_ID )
            // InternalCompDef.g:86:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getComponentAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComponentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getComponentAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalCompDef.g:106:3: ( (lv_ports_3_0= rulePort ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==21) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalCompDef.g:107:4: (lv_ports_3_0= rulePort )
            	    {
            	    // InternalCompDef.g:107:4: (lv_ports_3_0= rulePort )
            	    // InternalCompDef.g:108:5: lv_ports_3_0= rulePort
            	    {

            	    					newCompositeNode(grammarAccess.getComponentAccess().getPortsPortParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_ports_3_0=rulePort();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComponentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"ports",
            	    						lv_ports_3_0,
            	    						"org.eclipse.papyrus.robotics.xtext.compdef.CompDef.Port");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalCompDef.g:125:3: ( (lv_activity_4_0= ruleActivity ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==24) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCompDef.g:126:4: (lv_activity_4_0= ruleActivity )
            	    {
            	    // InternalCompDef.g:126:4: (lv_activity_4_0= ruleActivity )
            	    // InternalCompDef.g:127:5: lv_activity_4_0= ruleActivity
            	    {

            	    					newCompositeNode(grammarAccess.getComponentAccess().getActivityActivityParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_activity_4_0=ruleActivity();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComponentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"activity",
            	    						lv_activity_4_0,
            	    						"org.eclipse.papyrus.robotics.xtext.compdef.CompDef.Activity");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalCompDef.g:144:3: ( (lv_parameters_5_0= ruleParameter ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalCompDef.g:145:4: (lv_parameters_5_0= ruleParameter )
            	    {
            	    // InternalCompDef.g:145:4: (lv_parameters_5_0= ruleParameter )
            	    // InternalCompDef.g:146:5: lv_parameters_5_0= ruleParameter
            	    {

            	    					newCompositeNode(grammarAccess.getComponentAccess().getParametersParameterParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_parameters_5_0=ruleParameter();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getComponentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"parameters",
            	    						lv_parameters_5_0,
            	    						"org.eclipse.papyrus.robotics.xtext.compdef.CompDef.Parameter");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_6=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getComponentAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponent"


    // $ANTLR start "entryRuleParameter"
    // InternalCompDef.g:171:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // InternalCompDef.g:171:50: (iv_ruleParameter= ruleParameter EOF )
            // InternalCompDef.g:172:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalCompDef.g:178:1: ruleParameter returns [EObject current=null] : (otherlv_0= 'parameter' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_typeUndefined_4_0=null;
        Token otherlv_6=null;
        Token lv_comment_8_0=null;
        EObject lv_type_3_0 = null;

        EObject lv_multiplicity_5_0 = null;

        EObject lv_value_7_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:184:2: ( (otherlv_0= 'parameter' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? ) )
            // InternalCompDef.g:185:2: (otherlv_0= 'parameter' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? )
            {
            // InternalCompDef.g:185:2: (otherlv_0= 'parameter' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )? )
            // InternalCompDef.g:186:3: otherlv_0= 'parameter' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )? ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )? (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )? ( (lv_comment_8_0= RULE_VSL_COMMENT ) )?
            {
            otherlv_0=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getParameterAccess().getParameterKeyword_0());
            		
            // InternalCompDef.g:190:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalCompDef.g:191:4: (lv_name_1_0= RULE_ID )
            {
            // InternalCompDef.g:191:4: (lv_name_1_0= RULE_ID )
            // InternalCompDef.g:192:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            // InternalCompDef.g:208:3: (otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalCompDef.g:209:4: otherlv_2= ':' ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) )
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_9); 

                    				newLeafNode(otherlv_2, grammarAccess.getParameterAccess().getColonKeyword_2_0());
                    			
                    // InternalCompDef.g:213:4: ( ( (lv_type_3_0= ruleTypeRule ) ) | ( (lv_typeUndefined_4_0= '<Undefined>' ) ) )
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==RULE_ID) ) {
                        alt4=1;
                    }
                    else if ( (LA4_0==19) ) {
                        alt4=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 0, input);

                        throw nvae;
                    }
                    switch (alt4) {
                        case 1 :
                            // InternalCompDef.g:214:5: ( (lv_type_3_0= ruleTypeRule ) )
                            {
                            // InternalCompDef.g:214:5: ( (lv_type_3_0= ruleTypeRule ) )
                            // InternalCompDef.g:215:6: (lv_type_3_0= ruleTypeRule )
                            {
                            // InternalCompDef.g:215:6: (lv_type_3_0= ruleTypeRule )
                            // InternalCompDef.g:216:7: lv_type_3_0= ruleTypeRule
                            {

                            							newCompositeNode(grammarAccess.getParameterAccess().getTypeTypeRuleParserRuleCall_2_1_0_0());
                            						
                            pushFollow(FOLLOW_10);
                            lv_type_3_0=ruleTypeRule();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getParameterRule());
                            							}
                            							set(
                            								current,
                            								"type",
                            								lv_type_3_0,
                            								"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.TypeRule");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalCompDef.g:234:5: ( (lv_typeUndefined_4_0= '<Undefined>' ) )
                            {
                            // InternalCompDef.g:234:5: ( (lv_typeUndefined_4_0= '<Undefined>' ) )
                            // InternalCompDef.g:235:6: (lv_typeUndefined_4_0= '<Undefined>' )
                            {
                            // InternalCompDef.g:235:6: (lv_typeUndefined_4_0= '<Undefined>' )
                            // InternalCompDef.g:236:7: lv_typeUndefined_4_0= '<Undefined>'
                            {
                            lv_typeUndefined_4_0=(Token)match(input,19,FOLLOW_10); 

                            							newLeafNode(lv_typeUndefined_4_0, grammarAccess.getParameterAccess().getTypeUndefinedUndefinedKeyword_2_1_1_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getParameterRule());
                            							}
                            							setWithLastConsumed(current, "typeUndefined", lv_typeUndefined_4_0 != null, "<Undefined>");
                            						

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }

            // InternalCompDef.g:250:3: ( (lv_multiplicity_5_0= ruleMultiplicityRule ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==26) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalCompDef.g:251:4: (lv_multiplicity_5_0= ruleMultiplicityRule )
                    {
                    // InternalCompDef.g:251:4: (lv_multiplicity_5_0= ruleMultiplicityRule )
                    // InternalCompDef.g:252:5: lv_multiplicity_5_0= ruleMultiplicityRule
                    {

                    					newCompositeNode(grammarAccess.getParameterAccess().getMultiplicityMultiplicityRuleParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_11);
                    lv_multiplicity_5_0=ruleMultiplicityRule();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getParameterRule());
                    					}
                    					set(
                    						current,
                    						"multiplicity",
                    						lv_multiplicity_5_0,
                    						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.MultiplicityRule");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalCompDef.g:269:3: (otherlv_6= '=' ( (lv_value_7_0= ruleValue ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalCompDef.g:270:4: otherlv_6= '=' ( (lv_value_7_0= ruleValue ) )
                    {
                    otherlv_6=(Token)match(input,20,FOLLOW_12); 

                    				newLeafNode(otherlv_6, grammarAccess.getParameterAccess().getEqualsSignKeyword_4_0());
                    			
                    // InternalCompDef.g:274:4: ( (lv_value_7_0= ruleValue ) )
                    // InternalCompDef.g:275:5: (lv_value_7_0= ruleValue )
                    {
                    // InternalCompDef.g:275:5: (lv_value_7_0= ruleValue )
                    // InternalCompDef.g:276:6: lv_value_7_0= ruleValue
                    {

                    						newCompositeNode(grammarAccess.getParameterAccess().getValueValueParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_value_7_0=ruleValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getParameterRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"org.eclipse.papyrus.robotics.xtext.compdef.CompDef.Value");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalCompDef.g:294:3: ( (lv_comment_8_0= RULE_VSL_COMMENT ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_VSL_COMMENT) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalCompDef.g:295:4: (lv_comment_8_0= RULE_VSL_COMMENT )
                    {
                    // InternalCompDef.g:295:4: (lv_comment_8_0= RULE_VSL_COMMENT )
                    // InternalCompDef.g:296:5: lv_comment_8_0= RULE_VSL_COMMENT
                    {
                    lv_comment_8_0=(Token)match(input,RULE_VSL_COMMENT,FOLLOW_2); 

                    					newLeafNode(lv_comment_8_0, grammarAccess.getParameterAccess().getCommentVSL_COMMENTTerminalRuleCall_5_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getParameterRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"comment",
                    						lv_comment_8_0,
                    						"org.eclipse.papyrus.robotics.xtext.compdef.CompDef.VSL_COMMENT");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleValue"
    // InternalCompDef.g:316:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalCompDef.g:316:46: (iv_ruleValue= ruleValue EOF )
            // InternalCompDef.g:317:2: iv_ruleValue= ruleValue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;

             current =iv_ruleValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalCompDef.g:323:1: ruleValue returns [EObject current=null] : ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        Token lv_str_0_0=null;
        Token lv_ival_1_0=null;
        Token lv_dval_2_0=null;


        	enterRule();

        try {
            // InternalCompDef.g:329:2: ( ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) ) )
            // InternalCompDef.g:330:2: ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) )
            {
            // InternalCompDef.g:330:2: ( ( (lv_str_0_0= RULE_STRING ) ) | ( (lv_ival_1_0= RULE_INT ) ) | ( (lv_dval_2_0= RULE_DOUBLE ) ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt9=1;
                }
                break;
            case RULE_INT:
                {
                alt9=2;
                }
                break;
            case RULE_DOUBLE:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalCompDef.g:331:3: ( (lv_str_0_0= RULE_STRING ) )
                    {
                    // InternalCompDef.g:331:3: ( (lv_str_0_0= RULE_STRING ) )
                    // InternalCompDef.g:332:4: (lv_str_0_0= RULE_STRING )
                    {
                    // InternalCompDef.g:332:4: (lv_str_0_0= RULE_STRING )
                    // InternalCompDef.g:333:5: lv_str_0_0= RULE_STRING
                    {
                    lv_str_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    					newLeafNode(lv_str_0_0, grammarAccess.getValueAccess().getStrSTRINGTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"str",
                    						lv_str_0_0,
                    						"org.eclipse.papyrus.uml.alf.Common.STRING");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalCompDef.g:350:3: ( (lv_ival_1_0= RULE_INT ) )
                    {
                    // InternalCompDef.g:350:3: ( (lv_ival_1_0= RULE_INT ) )
                    // InternalCompDef.g:351:4: (lv_ival_1_0= RULE_INT )
                    {
                    // InternalCompDef.g:351:4: (lv_ival_1_0= RULE_INT )
                    // InternalCompDef.g:352:5: lv_ival_1_0= RULE_INT
                    {
                    lv_ival_1_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    					newLeafNode(lv_ival_1_0, grammarAccess.getValueAccess().getIvalINTTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"ival",
                    						lv_ival_1_0,
                    						"org.eclipse.papyrus.uml.alf.Common.INT");
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalCompDef.g:369:3: ( (lv_dval_2_0= RULE_DOUBLE ) )
                    {
                    // InternalCompDef.g:369:3: ( (lv_dval_2_0= RULE_DOUBLE ) )
                    // InternalCompDef.g:370:4: (lv_dval_2_0= RULE_DOUBLE )
                    {
                    // InternalCompDef.g:370:4: (lv_dval_2_0= RULE_DOUBLE )
                    // InternalCompDef.g:371:5: lv_dval_2_0= RULE_DOUBLE
                    {
                    lv_dval_2_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); 

                    					newLeafNode(lv_dval_2_0, grammarAccess.getValueAccess().getDvalDOUBLETerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getValueRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"dval",
                    						lv_dval_2_0,
                    						"org.eclipse.papyrus.robotics.xtext.compdef.CompDef.DOUBLE");
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRulePort"
    // InternalCompDef.g:391:1: entryRulePort returns [EObject current=null] : iv_rulePort= rulePort EOF ;
    public final EObject entryRulePort() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePort = null;


        try {
            // InternalCompDef.g:391:45: (iv_rulePort= rulePort EOF )
            // InternalCompDef.g:392:2: iv_rulePort= rulePort EOF
            {
             newCompositeNode(grammarAccess.getPortRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePort=rulePort();

            state._fsp--;

             current =iv_rulePort; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePort"


    // $ANTLR start "rulePort"
    // InternalCompDef.g:398:1: rulePort returns [EObject current=null] : (otherlv_0= 'port' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) ) )? (otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) ) )? ) ;
    public final EObject rulePort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_prov_3_0 = null;

        EObject lv_req_5_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:404:2: ( (otherlv_0= 'port' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) ) )? (otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) ) )? ) )
            // InternalCompDef.g:405:2: (otherlv_0= 'port' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) ) )? (otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) ) )? )
            {
            // InternalCompDef.g:405:2: (otherlv_0= 'port' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) ) )? (otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) ) )? )
            // InternalCompDef.g:406:3: otherlv_0= 'port' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) ) )? (otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) ) )?
            {
            otherlv_0=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPortAccess().getPortKeyword_0());
            		
            // InternalCompDef.g:410:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalCompDef.g:411:4: (lv_name_1_0= RULE_ID )
            {
            // InternalCompDef.g:411:4: (lv_name_1_0= RULE_ID )
            // InternalCompDef.g:412:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_14); 

            					newLeafNode(lv_name_1_0, grammarAccess.getPortAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPortRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }

            // InternalCompDef.g:428:3: (otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==22) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalCompDef.g:429:4: otherlv_2= 'provides' ( (lv_prov_3_0= ruleTypeRule ) )
                    {
                    otherlv_2=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getPortAccess().getProvidesKeyword_2_0());
                    			
                    // InternalCompDef.g:433:4: ( (lv_prov_3_0= ruleTypeRule ) )
                    // InternalCompDef.g:434:5: (lv_prov_3_0= ruleTypeRule )
                    {
                    // InternalCompDef.g:434:5: (lv_prov_3_0= ruleTypeRule )
                    // InternalCompDef.g:435:6: lv_prov_3_0= ruleTypeRule
                    {

                    						newCompositeNode(grammarAccess.getPortAccess().getProvTypeRuleParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_prov_3_0=ruleTypeRule();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPortRule());
                    						}
                    						set(
                    							current,
                    							"prov",
                    							lv_prov_3_0,
                    							"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.TypeRule");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalCompDef.g:453:3: (otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalCompDef.g:454:4: otherlv_4= 'requires' ( (lv_req_5_0= ruleTypeRule ) )
                    {
                    otherlv_4=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getPortAccess().getRequiresKeyword_3_0());
                    			
                    // InternalCompDef.g:458:4: ( (lv_req_5_0= ruleTypeRule ) )
                    // InternalCompDef.g:459:5: (lv_req_5_0= ruleTypeRule )
                    {
                    // InternalCompDef.g:459:5: (lv_req_5_0= ruleTypeRule )
                    // InternalCompDef.g:460:6: lv_req_5_0= ruleTypeRule
                    {

                    						newCompositeNode(grammarAccess.getPortAccess().getReqTypeRuleParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_req_5_0=ruleTypeRule();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPortRule());
                    						}
                    						set(
                    							current,
                    							"req",
                    							lv_req_5_0,
                    							"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.TypeRule");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePort"


    // $ANTLR start "entryRuleActivity"
    // InternalCompDef.g:482:1: entryRuleActivity returns [EObject current=null] : iv_ruleActivity= ruleActivity EOF ;
    public final EObject entryRuleActivity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActivity = null;


        try {
            // InternalCompDef.g:482:49: (iv_ruleActivity= ruleActivity EOF )
            // InternalCompDef.g:483:2: iv_ruleActivity= ruleActivity EOF
            {
             newCompositeNode(grammarAccess.getActivityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActivity=ruleActivity();

            state._fsp--;

             current =iv_ruleActivity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActivity"


    // $ANTLR start "ruleActivity"
    // InternalCompDef.g:489:1: ruleActivity returns [EObject current=null] : (otherlv_0= 'activity' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleActivity() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalCompDef.g:495:2: ( (otherlv_0= 'activity' ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalCompDef.g:496:2: (otherlv_0= 'activity' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalCompDef.g:496:2: (otherlv_0= 'activity' ( (lv_name_1_0= RULE_ID ) ) )
            // InternalCompDef.g:497:3: otherlv_0= 'activity' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getActivityAccess().getActivityKeyword_0());
            		
            // InternalCompDef.g:501:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalCompDef.g:502:4: (lv_name_1_0= RULE_ID )
            {
            // InternalCompDef.g:502:4: (lv_name_1_0= RULE_ID )
            // InternalCompDef.g:503:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getActivityAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getActivityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.papyrus.uml.alf.Common.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActivity"


    // $ANTLR start "entryRuleConnectorEnd"
    // InternalCompDef.g:523:1: entryRuleConnectorEnd returns [EObject current=null] : iv_ruleConnectorEnd= ruleConnectorEnd EOF ;
    public final EObject entryRuleConnectorEnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConnectorEnd = null;


        try {
            // InternalCompDef.g:523:53: (iv_ruleConnectorEnd= ruleConnectorEnd EOF )
            // InternalCompDef.g:524:2: iv_ruleConnectorEnd= ruleConnectorEnd EOF
            {
             newCompositeNode(grammarAccess.getConnectorEndRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConnectorEnd=ruleConnectorEnd();

            state._fsp--;

             current =iv_ruleConnectorEnd; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConnectorEnd"


    // $ANTLR start "ruleConnectorEnd"
    // InternalCompDef.g:530:1: ruleConnectorEnd returns [EObject current=null] : ( (lv_port_0_0= RULE_ID ) ) ;
    public final EObject ruleConnectorEnd() throws RecognitionException {
        EObject current = null;

        Token lv_port_0_0=null;


        	enterRule();

        try {
            // InternalCompDef.g:536:2: ( ( (lv_port_0_0= RULE_ID ) ) )
            // InternalCompDef.g:537:2: ( (lv_port_0_0= RULE_ID ) )
            {
            // InternalCompDef.g:537:2: ( (lv_port_0_0= RULE_ID ) )
            // InternalCompDef.g:538:3: (lv_port_0_0= RULE_ID )
            {
            // InternalCompDef.g:538:3: (lv_port_0_0= RULE_ID )
            // InternalCompDef.g:539:4: lv_port_0_0= RULE_ID
            {
            lv_port_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(lv_port_0_0, grammarAccess.getConnectorEndAccess().getPortIDTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getConnectorEndRule());
            				}
            				setWithLastConsumed(
            					current,
            					"port",
            					lv_port_0_0,
            					"org.eclipse.papyrus.uml.alf.Common.ID");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConnectorEnd"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalCompDef.g:558:1: entryRuleQualifiedName returns [EObject current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final EObject entryRuleQualifiedName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQualifiedName = null;


        try {
            // InternalCompDef.g:558:54: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalCompDef.g:559:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalCompDef.g:565:1: ruleQualifiedName returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? ) ;
    public final EObject ruleQualifiedName() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_remaining_2_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:571:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? ) )
            // InternalCompDef.g:572:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? )
            {
            // InternalCompDef.g:572:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )? )
            // InternalCompDef.g:573:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '::' ( (lv_remaining_2_0= ruleQualifiedName ) )?
            {
            // InternalCompDef.g:573:3: ( (otherlv_0= RULE_ID ) )
            // InternalCompDef.g:574:4: (otherlv_0= RULE_ID )
            {
            // InternalCompDef.g:574:4: (otherlv_0= RULE_ID )
            // InternalCompDef.g:575:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQualifiedNameRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(otherlv_0, grammarAccess.getQualifiedNameAccess().getPathNamespaceCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_17); 

            			newLeafNode(otherlv_1, grammarAccess.getQualifiedNameAccess().getColonColonKeyword_1());
            		
            // InternalCompDef.g:590:3: ( (lv_remaining_2_0= ruleQualifiedName ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                int LA12_1 = input.LA(2);

                if ( (LA12_1==25) ) {
                    alt12=1;
                }
            }
            switch (alt12) {
                case 1 :
                    // InternalCompDef.g:591:4: (lv_remaining_2_0= ruleQualifiedName )
                    {
                    // InternalCompDef.g:591:4: (lv_remaining_2_0= ruleQualifiedName )
                    // InternalCompDef.g:592:5: lv_remaining_2_0= ruleQualifiedName
                    {

                    					newCompositeNode(grammarAccess.getQualifiedNameAccess().getRemainingQualifiedNameParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_remaining_2_0=ruleQualifiedName();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getQualifiedNameRule());
                    					}
                    					set(
                    						current,
                    						"remaining",
                    						lv_remaining_2_0,
                    						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.QualifiedName");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleTypeRule"
    // InternalCompDef.g:613:1: entryRuleTypeRule returns [EObject current=null] : iv_ruleTypeRule= ruleTypeRule EOF ;
    public final EObject entryRuleTypeRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeRule = null;


        try {
            // InternalCompDef.g:613:49: (iv_ruleTypeRule= ruleTypeRule EOF )
            // InternalCompDef.g:614:2: iv_ruleTypeRule= ruleTypeRule EOF
            {
             newCompositeNode(grammarAccess.getTypeRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTypeRule=ruleTypeRule();

            state._fsp--;

             current =iv_ruleTypeRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeRule"


    // $ANTLR start "ruleTypeRule"
    // InternalCompDef.g:620:1: ruleTypeRule returns [EObject current=null] : ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleTypeRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_path_0_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:626:2: ( ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) ) )
            // InternalCompDef.g:627:2: ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalCompDef.g:627:2: ( ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) ) )
            // InternalCompDef.g:628:3: ( (lv_path_0_0= ruleQualifiedName ) )? ( (otherlv_1= RULE_ID ) )
            {
            // InternalCompDef.g:628:3: ( (lv_path_0_0= ruleQualifiedName ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID) ) {
                int LA13_1 = input.LA(2);

                if ( (LA13_1==25) ) {
                    alt13=1;
                }
            }
            switch (alt13) {
                case 1 :
                    // InternalCompDef.g:629:4: (lv_path_0_0= ruleQualifiedName )
                    {
                    // InternalCompDef.g:629:4: (lv_path_0_0= ruleQualifiedName )
                    // InternalCompDef.g:630:5: lv_path_0_0= ruleQualifiedName
                    {

                    					newCompositeNode(grammarAccess.getTypeRuleAccess().getPathQualifiedNameParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_path_0_0=ruleQualifiedName();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTypeRuleRule());
                    					}
                    					set(
                    						current,
                    						"path",
                    						lv_path_0_0,
                    						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.QualifiedName");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalCompDef.g:647:3: ( (otherlv_1= RULE_ID ) )
            // InternalCompDef.g:648:4: (otherlv_1= RULE_ID )
            {
            // InternalCompDef.g:648:4: (otherlv_1= RULE_ID )
            // InternalCompDef.g:649:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTypeRuleRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getTypeRuleAccess().getTypeTypeCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeRule"


    // $ANTLR start "entryRuleMultiplicityRule"
    // InternalCompDef.g:664:1: entryRuleMultiplicityRule returns [EObject current=null] : iv_ruleMultiplicityRule= ruleMultiplicityRule EOF ;
    public final EObject entryRuleMultiplicityRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicityRule = null;


        try {
            // InternalCompDef.g:664:57: (iv_ruleMultiplicityRule= ruleMultiplicityRule EOF )
            // InternalCompDef.g:665:2: iv_ruleMultiplicityRule= ruleMultiplicityRule EOF
            {
             newCompositeNode(grammarAccess.getMultiplicityRuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultiplicityRule=ruleMultiplicityRule();

            state._fsp--;

             current =iv_ruleMultiplicityRule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicityRule"


    // $ANTLR start "ruleMultiplicityRule"
    // InternalCompDef.g:671:1: ruleMultiplicityRule returns [EObject current=null] : (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' ) ;
    public final EObject ruleMultiplicityRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_bounds_1_0 = null;

        EObject lv_bounds_3_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:677:2: ( (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' ) )
            // InternalCompDef.g:678:2: (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' )
            {
            // InternalCompDef.g:678:2: (otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']' )
            // InternalCompDef.g:679:3: otherlv_0= '[' ( (lv_bounds_1_0= ruleBoundSpecification ) ) (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )? otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_18); 

            			newLeafNode(otherlv_0, grammarAccess.getMultiplicityRuleAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalCompDef.g:683:3: ( (lv_bounds_1_0= ruleBoundSpecification ) )
            // InternalCompDef.g:684:4: (lv_bounds_1_0= ruleBoundSpecification )
            {
            // InternalCompDef.g:684:4: (lv_bounds_1_0= ruleBoundSpecification )
            // InternalCompDef.g:685:5: lv_bounds_1_0= ruleBoundSpecification
            {

            					newCompositeNode(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_19);
            lv_bounds_1_0=ruleBoundSpecification();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMultiplicityRuleRule());
            					}
            					add(
            						current,
            						"bounds",
            						lv_bounds_1_0,
            						"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.BoundSpecification");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalCompDef.g:702:3: (otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==27) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalCompDef.g:703:4: otherlv_2= '..' ( (lv_bounds_3_0= ruleBoundSpecification ) )
                    {
                    otherlv_2=(Token)match(input,27,FOLLOW_18); 

                    				newLeafNode(otherlv_2, grammarAccess.getMultiplicityRuleAccess().getFullStopFullStopKeyword_2_0());
                    			
                    // InternalCompDef.g:707:4: ( (lv_bounds_3_0= ruleBoundSpecification ) )
                    // InternalCompDef.g:708:5: (lv_bounds_3_0= ruleBoundSpecification )
                    {
                    // InternalCompDef.g:708:5: (lv_bounds_3_0= ruleBoundSpecification )
                    // InternalCompDef.g:709:6: lv_bounds_3_0= ruleBoundSpecification
                    {

                    						newCompositeNode(grammarAccess.getMultiplicityRuleAccess().getBoundsBoundSpecificationParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_bounds_3_0=ruleBoundSpecification();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getMultiplicityRuleRule());
                    						}
                    						add(
                    							current,
                    							"bounds",
                    							lv_bounds_3_0,
                    							"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.BoundSpecification");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getMultiplicityRuleAccess().getRightSquareBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicityRule"


    // $ANTLR start "entryRuleBoundSpecification"
    // InternalCompDef.g:735:1: entryRuleBoundSpecification returns [EObject current=null] : iv_ruleBoundSpecification= ruleBoundSpecification EOF ;
    public final EObject entryRuleBoundSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBoundSpecification = null;


        try {
            // InternalCompDef.g:735:59: (iv_ruleBoundSpecification= ruleBoundSpecification EOF )
            // InternalCompDef.g:736:2: iv_ruleBoundSpecification= ruleBoundSpecification EOF
            {
             newCompositeNode(grammarAccess.getBoundSpecificationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoundSpecification=ruleBoundSpecification();

            state._fsp--;

             current =iv_ruleBoundSpecification; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoundSpecification"


    // $ANTLR start "ruleBoundSpecification"
    // InternalCompDef.g:742:1: ruleBoundSpecification returns [EObject current=null] : ( (lv_value_0_0= ruleUnlimitedLiteral ) ) ;
    public final EObject ruleBoundSpecification() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;



        	enterRule();

        try {
            // InternalCompDef.g:748:2: ( ( (lv_value_0_0= ruleUnlimitedLiteral ) ) )
            // InternalCompDef.g:749:2: ( (lv_value_0_0= ruleUnlimitedLiteral ) )
            {
            // InternalCompDef.g:749:2: ( (lv_value_0_0= ruleUnlimitedLiteral ) )
            // InternalCompDef.g:750:3: (lv_value_0_0= ruleUnlimitedLiteral )
            {
            // InternalCompDef.g:750:3: (lv_value_0_0= ruleUnlimitedLiteral )
            // InternalCompDef.g:751:4: lv_value_0_0= ruleUnlimitedLiteral
            {

            				newCompositeNode(grammarAccess.getBoundSpecificationAccess().getValueUnlimitedLiteralParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_value_0_0=ruleUnlimitedLiteral();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getBoundSpecificationRule());
            				}
            				set(
            					current,
            					"value",
            					lv_value_0_0,
            					"org.eclipse.papyrus.uml.textedit.common.xtext.UmlCommon.UnlimitedLiteral");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoundSpecification"


    // $ANTLR start "entryRuleUnlimitedLiteral"
    // InternalCompDef.g:771:1: entryRuleUnlimitedLiteral returns [String current=null] : iv_ruleUnlimitedLiteral= ruleUnlimitedLiteral EOF ;
    public final String entryRuleUnlimitedLiteral() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleUnlimitedLiteral = null;


        try {
            // InternalCompDef.g:771:56: (iv_ruleUnlimitedLiteral= ruleUnlimitedLiteral EOF )
            // InternalCompDef.g:772:2: iv_ruleUnlimitedLiteral= ruleUnlimitedLiteral EOF
            {
             newCompositeNode(grammarAccess.getUnlimitedLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnlimitedLiteral=ruleUnlimitedLiteral();

            state._fsp--;

             current =iv_ruleUnlimitedLiteral.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnlimitedLiteral"


    // $ANTLR start "ruleUnlimitedLiteral"
    // InternalCompDef.g:778:1: ruleUnlimitedLiteral returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT | kw= '*' ) ;
    public final AntlrDatatypeRuleToken ruleUnlimitedLiteral() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;


        	enterRule();

        try {
            // InternalCompDef.g:784:2: ( (this_INT_0= RULE_INT | kw= '*' ) )
            // InternalCompDef.g:785:2: (this_INT_0= RULE_INT | kw= '*' )
            {
            // InternalCompDef.g:785:2: (this_INT_0= RULE_INT | kw= '*' )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==RULE_INT) ) {
                alt15=1;
            }
            else if ( (LA15_0==29) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalCompDef.g:786:3: this_INT_0= RULE_INT
                    {
                    this_INT_0=(Token)match(input,RULE_INT,FOLLOW_2); 

                    			current.merge(this_INT_0);
                    		

                    			newLeafNode(this_INT_0, grammarAccess.getUnlimitedLiteralAccess().getINTTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalCompDef.g:794:3: kw= '*'
                    {
                    kw=(Token)match(input,29,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getUnlimitedLiteralAccess().getAsteriskKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnlimitedLiteral"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000001230000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000001030000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000004140022L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000004100022L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100022L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000001C0L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000C00002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000020000080L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010000000L});

}