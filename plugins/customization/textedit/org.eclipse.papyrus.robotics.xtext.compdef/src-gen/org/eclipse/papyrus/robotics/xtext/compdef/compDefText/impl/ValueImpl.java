/**
 * generated by Xtext 2.29.0
 */
package org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.xtext.compdef.compDefText.CompDefTextPackage;
import org.eclipse.papyrus.robotics.xtext.compdef.compDefText.Value;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl.ValueImpl#getStr <em>Str</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl.ValueImpl#getIval <em>Ival</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.xtext.compdef.compDefText.impl.ValueImpl#getDval <em>Dval</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueImpl extends MinimalEObjectImpl.Container implements Value
{
  /**
   * The default value of the '{@link #getStr() <em>Str</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStr()
   * @generated
   * @ordered
   */
  protected static final String STR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getStr() <em>Str</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStr()
   * @generated
   * @ordered
   */
  protected String str = STR_EDEFAULT;

  /**
   * The default value of the '{@link #getIval() <em>Ival</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIval()
   * @generated
   * @ordered
   */
  protected static final int IVAL_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getIval() <em>Ival</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIval()
   * @generated
   * @ordered
   */
  protected int ival = IVAL_EDEFAULT;

  /**
   * The default value of the '{@link #getDval() <em>Dval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDval()
   * @generated
   * @ordered
   */
  protected static final double DVAL_EDEFAULT = 0.0;

  /**
   * The cached value of the '{@link #getDval() <em>Dval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDval()
   * @generated
   * @ordered
   */
  protected double dval = DVAL_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ValueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CompDefTextPackage.Literals.VALUE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getStr()
  {
    return str;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setStr(String newStr)
  {
    String oldStr = str;
    str = newStr;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CompDefTextPackage.VALUE__STR, oldStr, str));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int getIval()
  {
    return ival;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setIval(int newIval)
  {
    int oldIval = ival;
    ival = newIval;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CompDefTextPackage.VALUE__IVAL, oldIval, ival));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public double getDval()
  {
    return dval;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDval(double newDval)
  {
    double oldDval = dval;
    dval = newDval;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CompDefTextPackage.VALUE__DVAL, oldDval, dval));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CompDefTextPackage.VALUE__STR:
        return getStr();
      case CompDefTextPackage.VALUE__IVAL:
        return getIval();
      case CompDefTextPackage.VALUE__DVAL:
        return getDval();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CompDefTextPackage.VALUE__STR:
        setStr((String)newValue);
        return;
      case CompDefTextPackage.VALUE__IVAL:
        setIval((Integer)newValue);
        return;
      case CompDefTextPackage.VALUE__DVAL:
        setDval((Double)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CompDefTextPackage.VALUE__STR:
        setStr(STR_EDEFAULT);
        return;
      case CompDefTextPackage.VALUE__IVAL:
        setIval(IVAL_EDEFAULT);
        return;
      case CompDefTextPackage.VALUE__DVAL:
        setDval(DVAL_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CompDefTextPackage.VALUE__STR:
        return STR_EDEFAULT == null ? str != null : !STR_EDEFAULT.equals(str);
      case CompDefTextPackage.VALUE__IVAL:
        return ival != IVAL_EDEFAULT;
      case CompDefTextPackage.VALUE__DVAL:
        return dval != DVAL_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (str: ");
    result.append(str);
    result.append(", ival: ");
    result.append(ival);
    result.append(", dval: ");
    result.append(dval);
    result.append(')');
    return result.toString();
  }

} //ValueImpl
