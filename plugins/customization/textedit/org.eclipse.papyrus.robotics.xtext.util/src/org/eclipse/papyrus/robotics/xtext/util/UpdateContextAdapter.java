package org.eclipse.papyrus.robotics.xtext.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.xtext.integration.core.ContextElementAdapter;
import org.eclipse.xtext.resource.XtextResource;

public abstract class UpdateContextAdapter implements ContextElementAdapter.IContextElementProviderWithInit {
 
	protected XtextResource xtextResource;
	protected Object objectToEdit;
	
	public UpdateContextAdapter(Object objectToEdit) {
		this.objectToEdit = objectToEdit;
		}
	
	@Override
	public EObject getContextObject() {
		if (objectToEdit instanceof EObject) {
			return (EObject) objectToEdit;
		}
		return null;
	}

	@Override
	public void initResource(XtextResource xtextResourceParam) {
		xtextResource = xtextResourceParam;
		xtextResource.eAdapters().add(new Adapter() {

			public void notifyChanged(Notification notification) {
				if (xtextResource.getContents().size() > 0) {
					updateLastNames();
				}
			}

			public Notifier getTarget() {
				return null;
			}

			public void setTarget(Notifier newTarget) {
			}

			public boolean isAdapterForType(Object type) {
				return false;
			}
		});
	}
	
	/**
	 * Update lastNames maps based on the assumption that no more than one element is renamed at the same time
	 */
	public abstract void updateLastNames();
};
