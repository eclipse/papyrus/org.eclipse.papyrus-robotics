/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.xtext.util;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.ResourceSetChangeEvent;
import org.eclipse.emf.transaction.ResourceSetListener;
import org.eclipse.emf.transaction.ResourceSetListenerImpl;
import org.eclipse.emf.transaction.RollbackException;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.uml.properties.xtext.widget.property.AbstractXtextPropertyEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;

/**
 * A Property handling changes transactionally, i.e. only update once a transaction is finished.
 */
abstract public class TransactionalXtextEditor extends AbstractXtextPropertyEditor {

	protected boolean changePending;

	protected ResourceSetListener rsl;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            The composite in which the widget is created
	 * @param style
	 *            The style of the editor
	 */
	public TransactionalXtextEditor(String name, Composite parent, int style) {
		super(name, parent, style);
		xtextEditor.getTextControl().addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (rsl != null) {
					TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(elementToEdit);
					domain.removeResourceSetListener(rsl);
				}
			}
		});
	}

	/**
	 * treat change event, update editor contents
	 *
	 * @param event
	 */
	@Override
	public void handleChange(ChangeEvent event) {
		changePending = true;
	}

	@Override
	protected void doBinding() {
		super.doBinding();
		if (rsl == null) {
			rsl = new ResourceSetListenerImpl() {
				@Override
				public Command transactionAboutToCommit(ResourceSetChangeEvent event) throws RollbackException {
					if (changePending) {
						if (!xtextEditor.getTextControl().isDisposed()) {
							display();
						}
						changePending = false;
					}
					return null;
				}
			};
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(elementToEdit);
			domain.addResourceSetListener(rsl);
		}
	}
}
