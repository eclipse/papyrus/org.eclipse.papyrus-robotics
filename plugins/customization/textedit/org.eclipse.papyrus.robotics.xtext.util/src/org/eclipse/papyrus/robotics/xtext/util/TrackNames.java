/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) <ansgar.radermache@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.xtext.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.uml2.uml.NamedElement;

/**
 * Track the relation between names in the xtext model and their associated UML model
 * elements. The objective of this class is being able to distinguish whether an element
 * in the xtext model remains the same one as before, but has been renamed or whether it
 * is a new one.
 * The problem can not be solved based on element identity as xtext is deletes and then
 * recreates elements when it re-parses the text.
 *
 * @param <T>
 *            the meta-class of the UML element to track.
 */
public class TrackNames<T extends NamedElement> {

	Map<String, T> lastNames = new HashMap<String, T>();
	EList<T> umlList;

	/**
	 * Constructor.
	 *
	 * @param umlList
	 *            the list of UML elements to track
	 */
	public TrackNames(EList<T> umlList) {
		this.umlList = umlList;
		for (T named : umlList) {
			if (named.getName() != null) {
				lastNames.put(named.getName(), named);
			}
		}
	}

	/**
	 * provide access to map internal map
	 */
	public T get(String name) {
		return lastNames.get(name);
	}

	public boolean containsValue(T value) {
		return lastNames.containsValue(value);
	}

	/**
	 * update naming information. This method should be called whenever the xtext models updates
	 * (from a specialization of the @see UpdateContextAdapter)
	 *
	 * @param nameListXt
	 *            the name list in the xtext model
	 */
	@SuppressWarnings("unchecked")
	public void update(List<String> nameListXt) {
		// update lastNames, iterate over array as a copy (avoid concurrent modification exception)
		for (String name : lastNames.keySet().toArray(new String[0])) {
			boolean found = false;
			for (String nameXt : nameListXt) {
				if (nameXt != null && name.equals(nameXt)) {
					found = true;
					break;
				}
			}
			if (!found) {
				// name is not present in updated xtext model, check whether is has been removed
				// or renamed
				// first check whether a name in the xtext list is not present in the lastNames map
				for (String nameXt : nameListXt) {
					if (nameListXt != null && nameXt != null && !lastNames.containsKey(nameXt)) {
						T existingParam = lastNames.get(name);
						lastNames.put(nameXt, existingParam);
						break;
					}
				}
				// remove name from lastNames list
				lastNames.remove(name);
			}
		}
		// add elements that are not in the lastNames list. This assures that all elements are in the lastNames list
		// and enables the correct assignment of a renamed element
		for (String nameXt : nameListXt) {
			if (nameXt != null && !lastNames.containsKey(nameXt)) {
				// put new name into list, but without a value (unless there is already such a parameter on the UML level
				// which can happen, if an element is deleted and then added again)
				T existingProperty = null;
				if (umlList != null) {
					existingProperty = (T) ElementUtils.getNamedElementFromList(umlList, nameXt);
				}
				lastNames.put(nameXt, existingProperty);
			}
		}
	}
}
