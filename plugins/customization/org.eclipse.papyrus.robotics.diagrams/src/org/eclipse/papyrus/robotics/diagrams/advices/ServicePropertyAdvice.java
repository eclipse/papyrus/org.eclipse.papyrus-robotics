package org.eclipse.papyrus.robotics.diagrams.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceProperty;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Interface;

/**
 * Edit Helper Advice for {@link ServiceProperty}
 */
public class ServicePropertyAdvice extends AbstractEditHelperAdvice {

	/**
     * Allow creation only if container is a parameter
     */
    protected boolean approveCreateElementRequest(CreateElementRequest request) {
        IElementType type = request.getElementType();
        EObject container = request.getContainer();
        if (type != null && container instanceof Interface) {
            if (StereotypeUtil.isApplied((Interface) container, ServiceDefinition.class)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean approveRequest(IEditCommandRequest request) {
        if (request instanceof GetEditContextRequest) {
            GetEditContextRequest context = (GetEditContextRequest) request;
            if (context.getEditCommandRequest() instanceof CreateElementRequest) {
                return approveCreateElementRequest((CreateElementRequest) context.getEditCommandRequest());
            }
        }
        return super.approveRequest(request);
    }
	
}
