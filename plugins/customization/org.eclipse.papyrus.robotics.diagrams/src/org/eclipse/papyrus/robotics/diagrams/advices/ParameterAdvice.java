package org.eclipse.papyrus.robotics.diagrams.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;

public class ParameterAdvice extends AbstractEditHelperAdvice {

	/**
     * Allow creation only if container is a component definition or activity
     */
    protected boolean approveCreateElementRequest(CreateElementRequest request) {
        IElementType type = request.getElementType();
        EObject container = request.getContainer();
        if (type != null && container instanceof Class) {
            if ((StereotypeUtil.isApplied((Class) container, ComponentDefinition.class)) ||
            		(StereotypeUtil.isApplied((Class) container, Activity.class))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean approveRequest(IEditCommandRequest request) {
        if (request instanceof GetEditContextRequest) {
            GetEditContextRequest context = (GetEditContextRequest) request;
            if (context.getEditCommandRequest() instanceof CreateElementRequest) {
                return approveCreateElementRequest((CreateElementRequest) context.getEditCommandRequest());
            }
        }
        return super.approveRequest(request);
    }
	
}
