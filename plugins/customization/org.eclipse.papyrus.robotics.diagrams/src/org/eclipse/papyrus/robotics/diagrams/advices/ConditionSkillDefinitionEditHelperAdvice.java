/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - initial API and implementation (Bug #581690)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.diagrams.advices;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.uml2.uml.Operation;

public class ConditionSkillDefinitionEditHelperAdvice extends SkillDefinitionEditHelperAdvice {

	@Override
	protected RecordingCommand createSetResultsCommand(Operation op) {
		return new RecordingCommand(TransactionUtil.getEditingDomain(op)) {
			@Override
			protected void doExecute() {
				createSkillSuccessReturnResult(op);
				createSkillFailureReturnResult(op);
			}
		};
	}

}
