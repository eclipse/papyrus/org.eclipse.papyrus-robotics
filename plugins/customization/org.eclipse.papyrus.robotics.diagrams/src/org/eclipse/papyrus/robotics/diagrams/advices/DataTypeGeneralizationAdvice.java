package org.eclipse.papyrus.robotics.diagrams.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.DataType;

public class DataTypeGeneralizationAdvice extends AbstractEditHelperAdvice {

	/**
     * Allow creation only if container is a robotics::datatype and the target is a uml::datatype
     */
    protected boolean approveCreateGeneralizationRequest(CreateRelationshipRequest request) {
    	EObject source = request.getSource();
		EObject target = request.getTarget();

		boolean verifiedCondOnSource = (source instanceof DataType) && (StereotypeUtil.isApplied((DataType) source, org.eclipse.papyrus.robotics.profile.robotics.commobject.DataType.class));
		boolean verifiedCondOnTarget = (target instanceof DataType);

		if (verifiedCondOnSource && verifiedCondOnTarget)
			return true;
		return false;
    }

    @Override
    public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateRelationshipRequest) {
				return approveCreateGeneralizationRequest((CreateRelationshipRequest) context.getEditCommandRequest());
			}
		}
		return super.approveRequest(request);
	}
	
}
