/*****************************************************************************
 * Copyright (c) 2017, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *    Ansgar Radermacher - bug 581833
 *    Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581835
 *****************************************************************************/

package org.eclipse.papyrus.robotics.diagrams.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.robotics.core.commands.CreateFunctionsCommand;
import org.eclipse.papyrus.robotics.core.commands.DeferredCompositeCommand;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.uml.service.types.utils.RequestParameterUtils;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Edit Helper Advice for Connects
 */
public class ConnectsEditHelperAdvice extends AbstractEditHelperAdvice {

	protected EObject source;
	protected EObject target;

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateRelationshipRequest) {
				boolean canCreate = approveCreateRelationshipRequest((CreateRelationshipRequest) context.getEditCommandRequest());
				return canCreate;
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Check that the source of the assignment is a technical policy
	 * 
	 * @param request
	 * @return
	 */
	protected boolean approveCreateRelationshipRequest(CreateRelationshipRequest request) {

		source = request.getSource();
		target = request.getTarget();

		boolean sourceOnPart = false;
		boolean targetOnPart = false;

		// 1st case allowed - connection between activity and component ports
		boolean sourceIsActivityPort = (source instanceof Port) && (StereotypeUtil.isApplied((Port) source, ActivityPort.class));
		boolean targetIsComponentPort = (target instanceof Port) && (StereotypeUtil.isApplied((Port) target, ComponentPort.class));
		boolean sourceIsComponentPort = (source instanceof Port) && (StereotypeUtil.isApplied((Port) source, ComponentPort.class));
		boolean targetIsActvitiyPort = (target instanceof Port) && (StereotypeUtil.isApplied((Port) target, ActivityPort.class));
		if ((sourceIsActivityPort && targetIsComponentPort) || (sourceIsComponentPort && targetIsActvitiyPort)) {
			Port sourcePt = (Port) source;
			ComponentPort sourceCp = UMLUtil.getStereotypeApplication((Port) sourcePt, ComponentPort.class);
			if (sourceCp != null && sourceCp.isCoordinationPort() && !(sourcePt.getEnds().isEmpty()))
				return false; // A coordination port can be connected to 1 and only 1 activity port
			return true;
		}

		// 2nd case allowed - connection between component _data-flow_ ports when service definitions are compatible
		if (sourceIsComponentPort && targetIsComponentPort) {
			View sourceView = RequestParameterUtils.getSourceView(request);
			View targetView = RequestParameterUtils.getTargetView(request);
			if (source instanceof Port && sourceView.eContainer() instanceof View) {
				View sourceContainer = (View) sourceView.eContainer();
				sourceOnPart = sourceContainer.getElement() instanceof Property;
			}
			if (target instanceof Port && targetView.eContainer() instanceof View) {
				View targetContainer = (View) targetView.eContainer();
				targetOnPart = targetContainer.getElement() instanceof Property;
			}

			if (sourceOnPart || targetOnPart) {
				Port sourcePt = (Port) source;
				Port targetPt = (Port) target;
				ComponentPort sourceCp = UMLUtil.getStereotypeApplication((Port) sourcePt, ComponentPort.class);
				ComponentPort targetCp = UMLUtil.getStereotypeApplication((Port) targetPt, ComponentPort.class);
				if (sourceCp != null && targetCp != null) {
					if (sourceCp.isCoordinationPort() || targetCp.isCoordinationPort())
						return false; // Remark: only data-flow ports can be connected, not coordination ports!
					if (sourceOnPart == targetOnPart) {
						// assembly connector, check whether one provides and the other requires
						return sourceCp.getProvides() == targetCp.getRequires()
								&& sourceCp.getRequires() == targetCp.getProvides();
					} else {
						// delegation connector, check whether both provide or require (size is equal)
						return sourceCp.getProvides() == targetCp.getProvides()
								&& sourceCp.getRequires() == targetCp.getRequires();
					}
				}
			}
		}
		return false;
	}

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      Assure that the part-with-port attribute of a connector end targeting an
	 *      activity port is set, even if the user draws the connector towards the Activity
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Connector)) {
			return super.getAfterConfigureCommand(request);
		}
		final Connector connector = (Connector) newElement;

		// Use deferred command, since connector ends are not yet known.
		DeferredCompositeCommand wrapper = new DeferredCompositeCommand() {

			@Override
			public ICommand createDeferredCommand() {
				ICommand command = null;
				if (connector.getEnds().size() >= 2) {
					ConnectorEnd end1 = connector.getEnds().get(0);
					ConnectorEnd end2 = connector.getEnds().get(1);
					if (end1.getPartWithPort() == null) {
						ConnectableElement role1 = end1.getRole();
						if (role1 != null && StereotypeUtil.isApplied(role1, ActivityPort.class)) {
							command = PortUtils.setPartWithPortCommand(end1);
						}
					}
					if (command == null && end2.getPartWithPort() == null) {
						ConnectableElement role2 = end2.getRole();
						if (role2 != null && StereotypeUtil.isApplied(role2, ActivityPort.class)) {
							command = PortUtils.setPartWithPortCommand(end2);
						}
					}
				}
				return command;
			}
		};

		boolean sourceIsActivityPort = (source instanceof Port) && (StereotypeUtil.isApplied((Port) source, ActivityPort.class));
		boolean targetIsActivityPort = (target instanceof Port) && (StereotypeUtil.isApplied((Port) target, ActivityPort.class));

		if (sourceIsActivityPort) {
			// target must be component-port (1st parameter for call below, 2nd parameter is activity port)
			RecordingCommand rc = new CreateFunctionsCommand((Port) target, (Port) source);
			wrapper.add(EMFtoGMFCommandWrapper.wrap(rc));
		}
		if (targetIsActivityPort) {
			// source must be component-port (1st parameter for call below, 2nd parameter is activity port)
			RecordingCommand rc = new CreateFunctionsCommand((Port) source, (Port) target);
			wrapper.add(EMFtoGMFCommandWrapper.wrap(rc));
		}
		return wrapper;
	}
}