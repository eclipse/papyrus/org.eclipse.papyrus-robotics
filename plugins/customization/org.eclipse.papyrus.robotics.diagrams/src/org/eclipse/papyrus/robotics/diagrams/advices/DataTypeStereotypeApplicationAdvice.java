package org.eclipse.papyrus.robotics.diagrams.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.DataType;

public class DataTypeStereotypeApplicationAdvice extends AbstractEditHelperAdvice {

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 * Add data-type.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// start building command
		CompositeCommand compositeCommand = new CompositeCommand("Application of RMS DataType stereotype"); //$NON-NLS-1$

		// get the element under action
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof DataType)) {
			return super.getAfterConfigureCommand(request);
		}
		DataType umldt = (DataType) newElement;

		// prepare the command to configure the element under action
		RecordingCommand addRmsDataTypeStereotype = new RecordingCommand(TransactionUtil.getEditingDomain(umldt)) {
			@Override
			protected void doExecute() {
				StereotypeUtil.apply(umldt, org.eclipse.papyrus.robotics.profile.robotics.commobject.DataType.class);
			}
		};
		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(addRmsDataTypeStereotype));

		// execute the command
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

}
