/*****************************************************************************
 * Copyright (c) 2010, 2013, 2019, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Tatiana Fesenko (CEA LIST) - Initial API and implementation
 *  Christian W. Damus (CEA) - Support creating models in repositories (CDO)
 *  Ansgar Radermacher (CEA) - Robotics variant, support working sets
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.wizards.wizards;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.core.utils.FolderNames;
import org.eclipse.papyrus.robotics.wizards.WizardConstants;
import org.eclipse.papyrus.uml.diagram.wizards.Activator;
import org.eclipse.papyrus.uml.diagram.wizards.messages.Messages;
import org.eclipse.papyrus.uml.diagram.wizards.pages.PapyrusProjectCreationPage;
import org.eclipse.papyrus.uml.diagram.wizards.pages.SelectArchitectureContextPage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

/**
 * The Wizard creates a new Project and a Papyrus-for-Robotics Model inside it.
 */
public class NewRoboticsProjectWizard extends NewRoboticsModelWizard {

	private static final String DOT = "."; //$NON-NLS-1$

	private static final String SLASH = "/"; //$NON-NLS-1$


	/** The Constant WIZARD_ID. */
	public static final String WIZARD_ID = "org.eclipse.papyrus.robotics.wizards.createproject"; //$NON-NLS-1$

	/** The new project page. */
	private PapyrusProjectCreationPage myProjectPage;

	@Override
	public boolean isCreateProjectWizard() {
		return true;
	}

	/**
	 * Initializes the wizard 
	 *
	 * @param workbench
	 *            the workbench
	 * @param selection
	 *            the selection {@inheritDoc}
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		setWindowTitle(Messages.NewPapyrusProjectWizard_new_papyrus_project);
		setMyProjectPage(new PapyrusProjectCreationPage(Messages.NewPapyrusProjectWizard_0));
		getMyProjectPage().setDescription(Messages.NewPapyrusProjectWizard_1);
		setDefaultPageImageDescriptor(Activator.imageDescriptorFromPlugin(Activator.PLUGIN_ID, "icons/papyrus/PapyrusProjectWizban_75x66.gif")); //$NON-NLS-1$
	}



	/**
	 * Adds the pages.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void addPages() {
		// Gives the CreateModelWizard the newProjectPage to display it after the selectDiagramCategoryPage
		setNewProjectPage(getMyProjectPage());

		selectArchitectureContextPage = new SelectArchitectureContextPage();

		super.addPages();
	}

	/**
	 * Perform finish.
	 *
	 * @return true, if successful {@inheritDoc}
	 */
	@Override
	public boolean performFinish() {
		IProject newProjectHandle;
		try {
			newProjectHandle = createNewProject();
		} catch (CoreException e) {
			Activator.log.error(Messages.NewPapyrusProjectWizard_exception_on_opening, e);
			return false;
		}
		if (newProjectHandle == null) {
			return false;
		}

		return super.performFinish();
	}

	/**
	 * Creates the new project.
	 *
	 * @return the i project
	 * @throws CoreException
	 *             the core exception
	 */
	protected IProject createNewProject() throws CoreException {
		// get a project handle
		final IProject project = getMyProjectPage().getProjectHandle();

		// get a project descriptor
		java.net.URI projectLocationURI = null;
		if (!getMyProjectPage().useDefaults()) {
			projectLocationURI = getMyProjectPage().getLocationURI();
		}

		IProjectDescription projectDescription = null;
		NullProgressMonitor progressMonitor = new NullProgressMonitor();
		if (!project.exists()) {
			projectDescription = ResourcesPlugin.getWorkspace().newProjectDescription(project.getName());
			if (projectLocationURI != null) {
				projectDescription.setLocationURI(projectLocationURI);
			}
			project.create(projectDescription, SubMonitor.convert(progressMonitor, 1));
			project.open(SubMonitor.convert(progressMonitor, 1));
			IFolder models = project.getFolder(FolderNames.MODELS);
			models.create(true, true, null);
			String id = selectViewPointPage.getSelectedID();
			if (id != null && id.startsWith(WizardConstants.VIEWPOINT_PREFIX)) {
				if (id.endsWith(WizardConstants.SERVICE_DESIGN_VIEWPOINT) ||
					id.endsWith(WizardConstants.BEHAVIOR_DESIGN_VIEWPOINT)) {
					// Tier2 activities (service definitions should not be mixed with other definitions)
					IFolder services = models.getFolder(FolderNames.SERVICES);
					services.create(true, true, null);
					IFolder skills = models.getFolder(FolderNames.SKILLS);
					skills.create(true, true, null);
				} else {
					// Tier 3 activities
					IFolder components = models.getFolder(FolderNames.COMPONENTS);
					components.create(true, true, null);
					IFolder systems = models.getFolder(FolderNames.SYSTEM);
					systems.create(true, true, null);
					IFolder btrees = models.getFolder(FolderNames.TASKS);
					btrees.create(true, true, null);
					IFolder taskbasedhara = models.getFolder(FolderNames.TASKBASEDHARA);
					taskbasedhara.create(true, true, null);
				}
			}
		} else {
			project.open(SubMonitor.convert(progressMonitor, 1));
		}
		IWorkingSet[] workingSets = getMyProjectPage().getSelectedWorkingSets();
		if (workingSets.length > 0) {
			PlatformUI.getWorkbench().getWorkingSetManager().addToWorkingSets(project, workingSets);
		}

		return project;
	}

	@Override
	protected URI createNewModelURI(String categoryId) {
		// handle extension
		String extension = getExtension();
		String id = selectViewPointPage.getSelectedID();
		String folder = ""; //$NON-NLS-1$
		if (id.endsWith(WizardConstants.COMPONENT_DEVELOPMENT_VIEWPOINT)) {
			folder = FolderNames.COMPONENTS;
		} else if (id.endsWith(WizardConstants.SERVICE_DESIGN_VIEWPOINT)) {
			folder = FolderNames.SERVICES;
		} else if (id.endsWith(WizardConstants.SYSTEM_CONFIG_VIEWPOINT)) {
			folder = FolderNames.SYSTEM;
		} else if (id.endsWith(WizardConstants.BEHAVIOR_DESIGN_VIEWPOINT)) {
			folder = FolderNames.SKILLS;
		} else if (id.endsWith(WizardConstants.BEHAVIOR_TREE_VIEWPOINT)) {
			folder = FolderNames.TASKS;
		} else if (id.endsWith(WizardConstants.TASKBASED_HARA_VIEWPOINT)) {
			folder = FolderNames.TASKBASEDHARA;
		}
		IPath newFilePath = getMyProjectPage().getProjectHandle().getFullPath().append(FolderNames.MODELS + SLASH + folder + SLASH + getMyProjectPage().getFileName() + DOT + extension);
		return URI.createPlatformResourceURI(newFilePath.toString(), true);
	}

	public PapyrusProjectCreationPage getMyProjectPage() {
		return myProjectPage;
	}

	public void setMyProjectPage(PapyrusProjectCreationPage myProjectPage) {
		this.myProjectPage = myProjectPage;
	}

	protected WizardNewProjectCreationPage createNewProjectCreationPage() {
		return this.myProjectPage;
	}

}
