/*****************************************************************************
 * Copyright (c) 2019 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher, CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.wizards;

/**
 * Wizard constants, notably about context and viewpoints.
 */
public class WizardConstants {

	public static final String ROBOTICS_CONTEXT = "org.eclipse.papyrus.robotics.architecture"; //$NON-NLS-1$

	public static final String VIEWPOINT_PREFIX = "org.eclipse.papyrus.robotics.viewpoint."; //$NON-NLS-1$

	public static final String COMPONENT_DEVELOPMENT_VIEWPOINT = "ComponentDevelopment"; //$NON-NLS-1$

	public static final String SERVICE_DESIGN_VIEWPOINT = "ServiceDesign"; //$NON-NLS-1$

	public static final String BEHAVIOR_DESIGN_VIEWPOINT = "BehaviorDesign"; //$NON-NLS-1$

	public static final String BEHAVIOR_TREE_VIEWPOINT = "BehaviorTreeDesign"; //$NON-NLS-1$

	public static final String SYSTEM_CONFIG_VIEWPOINT = "SystemConfiguration"; //$NON-NLS-1$

	public static final String TASKBASED_HARA_VIEWPOINT = "TadkBasedHazardAnalysis"; //$NON-NLS-1$

	public static final String SAFETY_MODELING_VIEWPOINT = "SafetyModeling"; //$NON-NLS-1$

	public static final String SERVICE_FULFILLMENT_VIEWPOINT = "ServiceFulfillment"; //$NON-NLS-1$

	public static final String ASSERTIONS_VIEWPOINT = "Assertions"; //$NON-NLS-1$
}
