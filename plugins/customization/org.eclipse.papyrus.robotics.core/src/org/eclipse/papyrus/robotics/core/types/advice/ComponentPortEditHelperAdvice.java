/*****************************************************************************
 * Copyright (c) 2017, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *    Ansgar Radermacher - bug 581833
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.widgets.providers.AbstractStaticContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.commands.DeferredCompositeCommand;
import org.eclipse.papyrus.robotics.core.commands.PortCommands;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu.SubSelect;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper Advice for creating a new type and typing the port with this type
 */
public class ComponentPortEditHelperAdvice extends AbstractEditHelperAdvice {

	private static final String PROVIDED = "provided"; //$NON-NLS-1$
	private static final String REQUIRED = "required"; //$NON-NLS-1$

	/**
	 * Create additional ComponentService and type port with that service.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Port)) {
			return super.getAfterConfigureCommand(request);
		}
		final Port port = (Port) newElement;

		CompositeCommand compositeCommand = new CompositeCommand("Update component port"); //$NON-NLS-1$

		ICommand associateCSCommand = PortUtils.associateCSCommand(port);
		if (associateCSCommand != null) {
			compositeCommand.add(associateCSCommand);
		}

		AbstractStaticContentProvider directions = new AbstractStaticContentProvider() {
			@Override
			public Object[] getElements() {
				return new String[] { PROVIDED, REQUIRED };
			}
		};

		EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(directions, "Port direction", false);
		if (popupMenuState.show(Display.getDefault().getActiveShell())) {
			Object menuResult = popupMenuState.getSubResult();

			EReference ref = menuResult == PROVIDED ? UMLPackage.eINSTANCE.getPort_Provided() : UMLPackage.eINSTANCE.getPort_Required();

			EncapsulatedContentProvider cpWithWS = new RoboticsContentProvider(port,
					new UMLContentProvider(port, ref),
					ServiceDefinition.class, FileExtensions.SERVICEDEF_UML);

			SubSelect serviceDef = new EnhancedPopupMenu.SubSelect("", cpWithWS); //$NON-NLS-1$
			Object value = serviceDef.getResult();

			if (value instanceof Classifier) {
				final Classifier newType = (Classifier) value;

				SetRequest setNameRequest = new SetRequest(port, UMLPackage.eINSTANCE.getNamedElement_Name(), "Port"); //$NON-NLS-1$
				ICommand setNameCmd = ElementEditServiceUtils.getCommandProvider(port).getEditCommand(setNameRequest);
				compositeCommand.add(setNameCmd);

				// Use deferred command, since connector ends are not yet known.
				DeferredCompositeCommand wrapper = new DeferredCompositeCommand() {

					@Override
					public ICommand createDeferredCommand() {
						if (menuResult == PROVIDED) {
							return PortCommands.addProvided(port, (Interface) newType);
						} else {
							return PortCommands.addRequired(port, (Interface) newType);
						}
					}
				};
				compositeCommand.add(wrapper);
			} else {
				return new CancelCommand(port);
			}
		} else {
			return new CancelCommand(port);
		}

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	/**
	 * Remove created CS and its relationships, if port is destroyed
	 */
	@Override
	protected ICommand getAfterDestroyDependentsCommand(DestroyDependentsRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Port destruction command"); //$NON-NLS-1$
		EObject destroyElement = request.getElementToDestroy();
		if (!(destroyElement instanceof Port)) {
			return super.getAfterDestroyDependentsCommand(request);
		}
		final Port port = (Port) destroyElement;
		Type cs = port.getType();

		final IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(port);

		if (cs != null && StereotypeUtil.isApplied(cs, ComponentService.class) && !PortUtils.isCSDuplicate(port)) {
			// destroy relationships (usage, interface realization)
			for (Dependency dep : cs.getClientDependencies()) {
				DestroyElementRequest destroyDepReq = new DestroyElementRequest(dep, false);
				ICommand destroyDepCmd = commandProvider.getEditCommand(destroyDepReq);
				compositeCommand.add(destroyDepCmd);
			}

			DestroyElementRequest destroyCSReq = new DestroyElementRequest(cs, false);
			ICommand destroyCSCmd = commandProvider.getEditCommand(destroyCSReq);
			compositeCommand.add(destroyCSCmd);
		}

		return compositeCommand.isEmpty() ? super.getAfterDestroyDependentsCommand(request) : compositeCommand;
	}

	@Override
	protected ICommand getAfterDuplicateCommand(DuplicateElementsRequest request) {
		return null;
	}

	protected EObject source;
	protected EObject target;
}
