package org.eclipse.papyrus.robotics.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind;
import org.eclipse.uml2.uml.Behavior;

public class FunctionUtils {

	/*
	 * Name constants used for QUERY AND SEND
	 */
	public static final String HANDLER = "handler"; //$NON-NLS-1$
	/*
	 * Name constants used for ACTIONS
	 */
	public static final String GOAL = "goal"; //$NON-NLS-1$ - used for provided and required

	public static final String R_FEEDBACK = "feedback"; //$NON-NLS-1$
	public static final String R_RESULT = "result"; //$NON-NLS-1$

	public static final String P_CANCEL = "cancel"; //$NON-NLS-1$
	public static final String P_ACCEPTED = "accepted"; //$NON-NLS-1$

	/**
	 * return a function within an activity of a specific kind
	 * 
	 * @param activityPort
	 *            an activity port
	 * @param kind
	 *            a specific kind (typically a handler)
	 * @param nameFilter
	 *            an optional name filter, lower case name of function must contain this name
	 * @return the found function or null
	 */
	public static Behavior getFunction(ActivityPort activityPort, FunctionKind kind, String nameFilter) {
		for (Function fct : activityPort.getFunctions()) {
			if (fct.getKind() == kind && fct.getBase_Class() instanceof Behavior) {
				Behavior base = (Behavior) fct.getBase_Class();
				if (nameFilter == null || base.getName().toLowerCase().contains(nameFilter)) {
					return base;
				}
			}
		}
		return null;
	}

	/**
	 * return a function within an activity of a specific kind
	 * 
	 * @param activity
	 *            an activity
	 * @param kind
	 *            a specific kind (typically a handler)
	 * @return the found function or null
	 */
	public static List<Behavior> getFunctions(Activity activity, FunctionKind kind) {
		List<Behavior> behaviorList = new ArrayList<Behavior>();
		for (Function fct : activity.getFunctions()) {
			if (fct.getKind() == kind && fct.getBase_Class() instanceof Behavior) {
				Behavior base = (Behavior) fct.getBase_Class();
				behaviorList.add(base);
			}
		}
		return behaviorList;
	}

	/**
	 * return a function within an activity of a specific kind
	 */
	public static Behavior getFunction(ActivityPort activityPort, FunctionKind kind) {
		return getFunction(activityPort, kind, null);
	}
}
