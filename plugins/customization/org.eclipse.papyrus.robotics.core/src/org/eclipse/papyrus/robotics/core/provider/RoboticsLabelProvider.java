/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.widgets.providers.DelegatingLabelProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.core.utils.IconUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

public class RoboticsLabelProvider extends DelegatingLabelProvider {

	public RoboticsLabelProvider(ILabelProvider delegate) {
		super(delegate);
	}

	/**
	 * Overridden in subclasses to provide custom images for certain {@code element}s.
	 *
	 * @param element
	 *            an element
	 * @return the custom image, or {@code null} to delegate
	 */
	@Override
	protected Image customGetImage(Object element) {
		if (element == RoboticsContentProvider.WORKSPACE_MATCHES) {
			return IconUtils.getIcon(IconUtils.workspacePath);
		} else if (element == RoboticsContentProvider.MODELS_VIA_PATHMAPS) {
			return IconUtils.getIcon(IconUtils.pluginsPath);
		} else if (element instanceof URI) {
			URI uri = (URI) element;
			if (uri.segmentCount() == 0 || uri.segment(0).length() == 0) {
				// pathmap case
				return IconUtils.getIcon(IconUtils.pluginsPath); 
			}
			String imagePath = FileExtensions.getImagePath(uri);
			if (imagePath != null) {
				return IconUtils.getIcon(imagePath);
			}
			// default, use folder icon
			return IconUtils.getIcon(IconUtils.workspacePath);
		}
		return null;
	}

	/**
	 * Overridden in subclasses to provide custom images for certain {@code element}s.
	 *
	 * @param element
	 *            an element
	 * @return the custom image, or {@code null} to delegate
	 */
	@Override
	protected String customGetText(Object element) {
		if (element instanceof URI) {
			URI uri = (URI) element;
			String extension = FileExtensions.getExtension(uri);
			if (extension != null) {
				String name = uri.lastSegment();
				return name;
			}
			String uriStr = uri.toString();
			if (uriStr.startsWith("pathmap")) { //$NON-NLS-1$
				return uriStr.substring(10);
			}
		}
		else if (element instanceof ComponentPort) {
			Port port = ((ComponentPort) element).getBase_Port();
			if (port.getProvideds().size() > 0) {
				return String.format("%s provides %s", port.getName(), port.getProvideds().get(0).getName()); //$NON-NLS-1$
			}
			else if (port.getRequireds().size() > 0) {
				return String.format("%s requires %s", port.getName(), port.getRequireds().get(0).getName()); //$NON-NLS-1$
			}
			// use name only
			return port.getName();
		}
		if (element instanceof Property) {
			Type fctCandidate = ((Property) element).getType();
			Function function = UMLUtil.getStereotypeApplication(fctCandidate,  Function.class);
			if (function != null) {
				return String.format("%s (%s)", fctCandidate.getName(), function.getKind()); //$NON-NLS-1$
			}
		}
		return null;
	}
}
