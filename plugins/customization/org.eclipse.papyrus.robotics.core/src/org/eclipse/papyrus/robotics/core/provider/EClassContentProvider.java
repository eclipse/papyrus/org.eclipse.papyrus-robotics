/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *           Bug 566126 - Some Robotics dialog don't have history and "reveal" functionality
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.papyrus.infra.widgets.providers.AbstractStaticContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.infra.widgets.util.IRevealSemanticElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.resource.UMLResource;

/**
 * A simple content provider that filters based on EClass.
 */
public class EClassContentProvider extends AbstractStaticContentProvider implements IStaticContentProvider, ITreeContentProvider, IRevealSemanticElement {

	protected Resource resource;

	protected EClass metaClass;

	protected TreeViewer viewer;

	/**
	 * Constructor.
	 *
	 * @param eObject
	 *            an eObject used to determine the resource
	 * @param metaClass
	 *            the meta-class to filter
	 */
	public EClassContentProvider(EObject eObject, EClass metaClass) {
		resource = eObject.eResource();
		if (resource == null) {
			Activator.log.debug("EObject passed to EClassContentProvider is not contained in a resource"); //$NON-NLS-1$
		}
		this.metaClass = metaClass;
	}

	/**
	 * Flat case - provide all elements
	 * 
	 * @see org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider#getElements()
	 *
	 * @return a list of elements
	 */
	@Override
	public Object[] getElements() {
		List<EObject> results = new ArrayList<EObject>();
		List<Namespace> visitedNSs = new ArrayList<Namespace>();
		for (Package pkg : getEligiblePkgs()) {
			getElements(pkg, results, visitedNSs);
		}
		return results.toArray();
	}

	/**
	 * Top level element for hierarchical way
	 *
	 * @see org.eclipse.papyrus.infra.widgets.providers.AbstractStaticContentProvider#getElements(java.lang.Object)
	 *
	 * @param input
	 * @return a list of elements
	 */
	@Override
	public Object[] getElements(Object input) {
		return getEligiblePkgs().toArray();
	}

	/**
	 * @return the list of top-level elements that are eligible for searching
	 */
	protected List<Package> getEligiblePkgs() {
		List<Package> pkgList = new ArrayList<Package>();
		ResourceSet rs = resource.getResourceSet();
		for (Resource resource : rs.getResources()) {
			if (resource instanceof UMLResource) {
				if (resource.getContents().size() > 0) {
					EObject topLevelElem = resource.getContents().get(0);
					// look into packages, but not profiles
					if (topLevelElem instanceof Package && !(topLevelElem instanceof Profile)) {
						pkgList.add((Package) topLevelElem);
					}
				}
			}
		}
		return pkgList;
	}

	protected void getElements(Namespace ns, List<EObject> results, List<Namespace> visitedNSs) {
		if (!visitedNSs.contains(ns)) {
			visitedNSs.add(ns);
			for (EObject eObj : ns.eContents()) {
				if (eObj instanceof Namespace) {
					getElements((Namespace) eObj, results, visitedNSs);
				}
				if (metaClass.isInstance(eObj)) {
					results.add(eObj);
				}
			}
		}
	}

	@Override
	public Object[] getChildren(Object parent) {
		List<Object> results = new ArrayList<Object>();
		if (parent instanceof Namespace)
			for (EObject eObj : ((Namespace) parent).eContents()) {
				if (metaClass.isInstance(eObj) || eObj instanceof Namespace) {
					results.add(eObj);
				}
			}
		return results.toArray();
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof EObject) {
			return ((EObject) element).eContainer();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object parent) {
		return getChildren(parent).length > 0;
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (viewer instanceof TreeViewer) {
			this.viewer = (TreeViewer) viewer;
		}
		super.inputChanged(viewer, oldInput, newInput);
	}

	/**
	 * Support "reveal", i.e. select currently chosen element (assume that only one
	 * element is actually selected
	 */
	@Override
	public void revealSemanticElement(List<?> elementList) {
		// ProviderHelper.selectReveal(eObjects, viewer) does not work for some reason
		// (while it does in case of the UMLContentProvider)
		for (Object object : elementList) {
			if (object instanceof EObject) {
				EObject eObject = (EObject) object;
				// history objects gets loaded into the current resource set
				Object path[] = {
						eObject
				};
				viewer.setSelection(new TreeSelection(new TreePath(path)), true);
				break;
			}
		}
	}
}
