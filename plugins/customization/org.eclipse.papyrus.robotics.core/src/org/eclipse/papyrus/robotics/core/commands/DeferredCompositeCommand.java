/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation (for bug 581833)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.IdentityCommand;

/**
 * An abstract superclass for composite commands which need to include a command whose
 * arguments are not known at command creation time, since the elements are only partly
 * created or need to be created first by previous commands that are not yet executed.
 * Technically, the composite command contains an IdentityCommand as first command. It
 * replaces it the the composite command is executed for the first time.
 */
abstract public class DeferredCompositeCommand extends CompositeCommand {

	boolean first = true;

	public DeferredCompositeCommand() {
		super("wrapper"); //$NON-NLS-1$
		add(IdentityCommand.INSTANCE);
	}

	/**
	 * Method that creates the deferred command. It is called at command execution
	 * time and needs to be implemented by subclasses
	 * 
	 * @return the command that is be executed at first command of the composite
	 */
	public abstract ICommand createDeferredCommand();

	@SuppressWarnings("unchecked") // due to access to getChildren().
	@Override
	public IStatus execute(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
		if (first) {
			ICommand command = createDeferredCommand();
			if (command != null) {
				// replace Identity command
				getChildren().set(0, command);
			}
			first = false;
		}
		return super.execute(progressMonitor, info);
	}
}