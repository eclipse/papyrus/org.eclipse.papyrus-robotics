/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper Advice for creating a new OutAttribute
 */
public class OutAttributeEditHelperAdvice extends AbstractEditHelperAdvice {

	/**
	 * Set parameter direction after creation
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Parameter)) {
			return super.getAfterConfigureCommand(request);
		}
		final Parameter outParam = (Parameter) newElement;
		
		final IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(outParam);

		SetRequest setDirReq = new SetRequest(outParam, UMLPackage.eINSTANCE.getParameter_Direction(), ParameterDirectionKind.OUT_LITERAL);
		ICommand setDirCmd = commandProvider.getEditCommand(setDirReq);
		return setDirCmd;
	}

	protected EObject source;
	protected EObject target;
}
