/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class InstanceUtils {

	/**
	 * @param system System class (composite with parts)
	 * @return list of component-instances (parts) within the system
	 */
	public static List<Property> getCompInstanceList(Class system) {
		EList<Property> instanceList = new UniqueEList<Property>();
		for (Property p : system.allAttributes()) {
			if (StereotypeUtil.isApplied(p, ComponentInstance.class)) {
				instanceList.add((Property) p);
			}
		}
		return instanceList;
	}

	/**
	 * @param system System class (composite with parts)
	 * @return list of components within the system
	 */
	public static List<Class> getComponentList(Class system) {
		EList<Class> compList = new UniqueEList<Class>();
		for (Property p : getCompInstanceList(system)) {
			if (p.getType() instanceof Class) {
				compList.add((Class) p.getType());
			}
		}
		return compList;
	}

	/**
	 * Get the first component definition from a package/model, assuming that a model contains exactly
	 * one component definition
	 * @param pkg
	 * @return the System class, if it exists, null otherwise
	 */
	public static Class getComponentFromPkg(Package pkg) {
		// no deployment plan, traverse all packages
		for (PackageableElement pe : pkg.getPackagedElements()) {
			if (pe instanceof Package) {
				Class candidate = getComponentFromPkg((Package) pe);
				if (candidate != null) {
					return candidate;
				}
			}
			else if (pe instanceof Class && StereotypeUtil.isApplied(pe, ComponentDefinition.class)) {
				return (Class) pe;
			}
		}
		return null;
	}

	/**
	 * Get the System class from a package/model, assuming that a model contains exactly
	 * one System class
	 * @param pkg
	 * @return the System class, if it exists, null otherwise
	 */
	public static Class getSystemFromPkg(Package pkg) {
		// no deployment plan, traverse all packages
		for (PackageableElement pe : pkg.getPackagedElements()) {
			if (pe instanceof Package) {
				Class candidate = getSystemFromPkg((Package) pe);
				if (candidate != null) {
					return candidate;
				}
			}
			else if (pe instanceof Class && StereotypeUtil.isApplied(pe, System.class)) {
				return (Class) pe;
			}
		}
		return null;
	}

	/**
	 * Get the System class from a deployment plan
	 * @param cdp
	 * @return the System class, if it exists, null otherwise
	 */
	public static Class getSystem(Package cdp) {
		if (!DepUtils.isDeploymentPlan(cdp)) {
			return getSystemFromPkg(cdp);
		}
		for (InstanceSpecification is : DepUtils.getTopLevelInstances(cdp)) {
			Classifier cl = DepUtils.getClassifier(is);
			if (cl instanceof Class && StereotypeUtil.isApplied(cl, System.class)) {
				return (Class) cl;
			}
		}
		return null;
	}

	/**
	 * return true, if the component represented by the part (instance) is a life-cycle node
	 */
	public static boolean isLifecycle(Property part) {
		if (part.getType() != null) {
			ComponentDefinition compDef = UMLUtil.getStereotypeApplication(part.getType(), ComponentDefinition.class);
			if (compDef != null) {
				return compDef.isLifecycle();
			}
		}
		return true;
	}
}
