/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation
 *    Ansgar Radermacher - Bug 582095 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.commands;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.designer.infra.base.StringUtils;
import org.eclipse.papyrus.robotics.core.utils.FunctionUtils;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * In the moment, a component port gets connected with an activity port, make sure that
 * there are associated handler functions.
 */
public class CreateFunctionsCommand extends RecordingCommand {

	protected Port componentPortUml;

	protected Port activityPortUml;

	protected ActivityPort activityPort;

	protected Activity activity;
	
	public CreateFunctionsCommand(Port componentPortUml, Port activityPortUml) {
		super(TransactionUtil.getEditingDomain(activityPortUml), "Create functions"); //$NON-NLS-1$
		this.activityPortUml = activityPortUml;
		this.componentPortUml = componentPortUml;
		activityPort = UMLUtil.getStereotypeApplication(activityPortUml, ActivityPort.class);
	}

	@Override
	protected void doExecute() {
		// add aspects ...
		Class activityCl = activityPortUml.getClass_();
		activity = UMLUtil.getStereotypeApplication(activityCl, Activity.class);
		CommunicationPattern cp = InteractionUtils.getCommunicationPattern(componentPortUml);
		if (componentPortUml.getProvideds().size() > 0) {
			// provided port
			if (InteractionUtils.isSend(cp) || InteractionUtils.isQuery(cp)) {
				// create send handler or query callback
				createFunction(FunctionUtils.HANDLER);
			}
			else if (InteractionUtils.isAction(cp)) {
				// create action handlers (provided side)
				createFunction(FunctionUtils.GOAL);
				createFunction(FunctionUtils.P_CANCEL);
				createFunction(FunctionUtils.P_ACCEPTED);
			}
		}
		else if (componentPortUml.getRequireds().size() > 0) {
			if (InteractionUtils.isPush(cp) || InteractionUtils.isPubSub(cp)) {
				createFunction(FunctionUtils.HANDLER);
			}
			else if (InteractionUtils.isQuery(cp)) {
				createFunction(FunctionUtils.R_RESULT);
			}
			else if (InteractionUtils.isAction(cp)) {
				// create action handlers (required side)
				createFunction(FunctionUtils.GOAL);
				createFunction(FunctionUtils.R_FEEDBACK);
				createFunction(FunctionUtils.R_RESULT);
			}
			
		}
	}
	
	protected void setName(Property fctAttr, String postFix) {
		Type function = fctAttr.getType();
		// use varName to assure that created function name is compatible with most programming languages 
		function.setName(StringUtils.varName(componentPortUml.getName()) + "_" + postFix); //$NON-NLS-1$
	}
	
	protected void createFunction(String postFix) {
		Behavior fctUml = FunctionUtils.getFunction(activityPort, FunctionKind.HANDLER, postFix);
		if (fctUml == null) {
			CreateFunctionCommand createFct = new CreateFunctionCommand(activity.getBase_Class());
			createFct.doExecute();
			Property fctAttr = createFct.getFunctionAttr();
			activity.getBase_Class().getOwnedAttributes().add(fctAttr);
			setName(fctAttr, postFix);
			fctUml = (Behavior) fctAttr.getType();
		}
		Function function = UMLUtil.getStereotypeApplication(fctUml, Function.class);
		// now check, if already associated with port
		if (function != null && function.getActivityPort() == null) {
			function.setActivityPort(activityPort);
		}
	}
}
