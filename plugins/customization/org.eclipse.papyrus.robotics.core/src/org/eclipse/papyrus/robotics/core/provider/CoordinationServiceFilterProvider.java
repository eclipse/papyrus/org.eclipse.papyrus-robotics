/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation (Bug #581690)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TemplateBinding;


/**
 * Filter coordination service elements depending on their interaction pattern.
 */
public class CoordinationServiceFilterProvider extends FilterStereotypes {

	protected String INTERACTION_PATTERN_QUALIF_NAME;

	public CoordinationServiceFilterProvider(IStaticContentProvider encapsulated, String pattern) {
		super(encapsulated, CoordinationService.class);
		INTERACTION_PATTERN_QUALIF_NAME = pattern;
	}

	public void setInteractionPatternQualifName(String pattern) {
		INTERACTION_PATTERN_QUALIF_NAME = pattern;
	}

	public String getInteractionPatternQualifName() {
		return INTERACTION_PATTERN_QUALIF_NAME;
	}

	@Override
	public boolean isValid(final Object object) {
		if (super.isValid(object)) {
			if (object instanceof Interface) {
				Interface service = (Interface) object;
				if (service.eResource() != null) {
					NamedElement interactionPatternNE = ElementUtils.getQualifiedElementFromRS(service, INTERACTION_PATTERN_QUALIF_NAME);
					if (interactionPatternNE instanceof Collaboration) {
						Collaboration interactionPatternCb = (Collaboration) interactionPatternNE;
						for (TemplateBinding binding : service.getTemplateBindings()) {
							if (binding.getTargets().contains(interactionPatternCb.getOwnedTemplateSignature())) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
}