/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.uml.tools.commands.ApplyProfileCommand;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;

/**
 * Edit Helper Advice for applying a stereotype and making sure to apply required (sub-) profiles before
 */
public abstract class AbstractApplyStereotypeEditHelperAdvice extends AbstractEditHelperAdvice {

	/**
	 * @return the qualified name of the subProfile. If null, apply main profile
	 */
	abstract protected URI getProfileURI();

	abstract protected Class<? extends EObject> stereotypeToApply();

	/**
	 * @return the qualified name of the subProfile. Default implementation: return null => apply main profile
	 */
	protected String getSubProfileQName() {
		return null;
	}

	/**
	 * Create additional ComponentService and type port with that service.
	 */
	@Override
	protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
		final EObject newEObject = request.getElementToConfigure();
		CompositeCommand compositeCommand = new CompositeCommand("Apply profile"); //$NON-NLS-1$
		ICommand superCmd = super.getAfterConfigureCommand(request);
		if (superCmd != null) {
			compositeCommand.add(superCmd);
		}
		if (newEObject instanceof Element) {
			final Element newElement = (Element) newEObject;

			Package root = PackageUtil.getRootPackage(newElement);
			Package profile = PackageUtil.loadPackage(getProfileURI(), newElement.eResource().getResourceSet());
			if (profile != null && getSubProfileQName() != null) {
				NamedElement subProfile = ElementUtils.getQualifiedElement(profile, getSubProfileQName());
				if (subProfile instanceof Profile) {
					profile = (Profile) subProfile;
				}
			}
			if (profile instanceof Profile) {
				TransactionalEditingDomain ed = TransactionUtil.getEditingDomain(newElement);
				RecordingCommand applyProfileCmd = new ApplyProfileCommand(root, (Profile) profile, ed, false);
				RecordingCommand applyStereoCmd = new RecordingCommand(ed) {
					@Override
					protected void doExecute() {
						StereotypeUtil.apply(newElement, stereotypeToApply());
					}
				};
				if (!newElement.getNearestPackage().isProfileApplied((Profile) profile)) {
					compositeCommand.add(EMFtoGMFCommandWrapper.wrap(applyProfileCmd));
				}
				compositeCommand.add(EMFtoGMFCommandWrapper.wrap(applyStereoCmd));
			}
		}

		return compositeCommand;
	}
}