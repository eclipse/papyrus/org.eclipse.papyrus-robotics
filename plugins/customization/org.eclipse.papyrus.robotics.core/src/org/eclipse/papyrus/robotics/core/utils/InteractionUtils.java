/*****************************************************************************
 * Copyright (c) 2018, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *    Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581690
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.ParameterableElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

public class InteractionUtils {

	public static final String QUERY = "Query"; //$NON-NLS-1$
	public static final String PUSH = "Push"; //$NON-NLS-1$
	public static final String PUB_SUB = "PubSub"; //$NON-NLS-1$
	public static final String SEND = "Send"; //$NON-NLS-1$
	public static final String EVENT = "Event"; //$NON-NLS-1$
	public static final String ACTION = "Action"; //$NON-NLS-1$

	/**
	 * Get the first communication object from a port
	 * 
	 * @param port
	 *            a port
	 * @return the communication object for that port or null
	 */
	public static List<Type> getCommObjects(Port port) {
		return getCommObjects(getTemplateBinding(port));
	}

	/**
	 * Get the first communication object from a connector
	 * 
	 * @param connector
	 *            a connector
	 * @return the communication object for that connector or null
	 */
	public static List<Type> getCommObjects(Connector connector) {
		return getCommObjects(getTemplateBinding(connector));
	}

	/**
	 * Get the first communication object from a port
	 * 
	 * @param port
	 *            a port
	 * @return the communication object for that port or null
	 */
	public static Type getCommObject(Port port) {
		return getCommObject(getTemplateBinding(port));
	}

	/**
	 * Get the first communication object from a connector
	 * 
	 * @param connector
	 *            a connector
	 * @return the communication object for that connector or null
	 */
	public static Type getCommObject(Connector connector) {
		return getCommObject(getTemplateBinding(connector));
	}

	/**
	 * Get the first communication object from a template binding (first template parameter)
	 * 
	 * @param tb
	 *            a template binding
	 * @return the communication object for that port
	 */
	public static Type getCommObject(TemplateBinding tb) {
		if (getCommObjects(tb).size() > 0) {
			return getCommObjects(tb).get(0);
		}
		return null;
	}

	/**
	 * Get the communication object from a template binding (all parameter substitutions)
	 * 
	 * @param tb
	 *            a template binding
	 * @return the communication object for that port
	 */
	public static List<Type> getCommObjects(TemplateBinding tb) {
		List<Type> commObjects = new ArrayList<Type>();
		if (tb != null) {
			for (TemplateParameterSubstitution tps : tb.getParameterSubstitutions()) {
				ParameterableElement pe = tps.getActual();
				if (pe instanceof Type) {
					commObjects.add((Type) pe);
				}
			}
		}
		return commObjects;
	}

	public static CommunicationPattern getCommunicationPattern(Connector connector) {
		TemplateBinding tb = getTemplateBinding(connector);
		return tb != null ? getCommunicationPattern(tb) : null;
	}

	public static CommunicationPattern getCommunicationPattern(Port port) {
		TemplateBinding tb = getTemplateBinding(port);
		return tb != null ? getCommunicationPattern(tb) : null;
	}

	public static CommunicationPattern getCommunicationPattern(Interface intf) {
		TemplateBinding tb = getTemplateBinding(intf);
		return tb != null ? getCommunicationPattern(tb) : null;
	}

	/**
	 * return a pattern from interface that is provided or required
	 * by a port
	 * 
	 * @param tb
	 *            a template binding
	 * @return a communication pattern
	 */
	public static CommunicationPattern getCommunicationPattern(TemplateBinding tb) {
		if (tb != null) {
			Element pattern = tb.getSignature().getOwner();
			if (pattern != null) {
				CommunicationPattern cp = UMLUtil.getStereotypeApplication(pattern, CommunicationPattern.class);
				if (cp != null) {
					return cp;
				}
			}
		}
		return null;
	}

	/**
	 * Return the name of a collaboration pattern
	 * 
	 * @param pattern
	 *            the pattern
	 * @return the name of the pattern
	 */
	public static String getPatternName(CommunicationPattern pattern) {
		if (pattern != null && pattern.getBase_Collaboration() != null) {
			return pattern.getBase_Collaboration().getName();
		}
		return null;
	}

	/**
	 * Return the template binding from a connector
	 * 
	 * @param connector
	 *            a connector
	 * @return the template binding
	 */
	public static TemplateBinding getTemplateBinding(Connector connector) {
		if (connector.getEnds().size() >= 2) {
			ConnectableElement role1 = connector.getEnds().get(0).getRole();
			ConnectableElement role2 = connector.getEnds().get(1).getRole();
			if (role1 instanceof Port && StereotypeUtil.isApplied(role1, ComponentPort.class)) {
				return getTemplateBinding((Port) role1);
			} else if (role2 instanceof Port && StereotypeUtil.isApplied(role2, ComponentPort.class)) {
				return getTemplateBinding((Port) role2);
			}
		}
		return null;
	}

	/**
	 * Return a template binding from a service definition
	 * (interface that is provided or required by a port)
	 * 
	 * @param intf
	 *            an interface
	 * @return the template binding
	 */
	public static TemplateBinding getTemplateBinding(Interface intf) {
		Element target = intf; // use template binding of interface
		if (intf.getClientDependencies().size() > 0) {
			// old way: use template binding of a comm. pattern definition
			// accessed via a client dependency
			Dependency d = intf.getClientDependencies().get(0);
			if (d.getTargets().size() > 0) {
				target = d.getTargets().get(0);
			}
		}
		if (target instanceof Classifier) {
			Classifier targetCl = (Classifier) target;
			if (targetCl.getTemplateBindings().size() > 0) {
				return targetCl.getTemplateBindings().get(0);
			}
		}
		return null;
	}

	public static TemplateBinding getTemplateBinding(Port port) {
		if (port.getProvideds().size() > 0) {
			return getTemplateBinding(port.getProvideds().get(0));
		} else if (port.getRequireds().size() > 0) {
			return getTemplateBinding(port.getRequireds().get(0));
		}
		return null;
	}

	public static String getTPName(TemplateParameter tp) {
		if (tp.getParameteredElement() instanceof NamedElement) {
			return ((NamedElement) tp.getParameteredElement()).getName();
		}
		return null;
	}

	/**
	 * return the owning service definition of a template binding
	 * TODO: will not work, if we pass by CommunicationPatternDefinition
	 * 
	 * @param tb
	 */
	public static Interface getServiceDefinition(TemplateBinding tb) {
		Element owner = tb.getOwner();
		return owner instanceof Interface ? (Interface) owner : null;
	}

	/**
	 * Return the service definition from a port
	 * 
	 * @param port
	 *            a UML2 Port (typed with a component service)
	 */
	public static Interface getServiceDefinition(Port port) {
		TemplateBinding tb = getTemplateBinding(port);
		if (tb != null) {
			return getServiceDefinition(tb);
		}
		return null;
	}

	/**
	 * Get the message definitions within a (i.e. packages with comm. objects)
	 *
	 * @param messagePkg
	 *            a message package (package with communication objects)
	 * @return the list of data types within that package, i.e. those related to Push/Send or no
	 *         pattern. Include data types that are not referenced by a service definition, as they could be
	 *         referenced by other data types or services
	 */
	public static List<DataType> getMessages(Package messagePkg) {
		List<DataType> dtList = new ArrayList<DataType>();
		for (PackageableElement pe : messagePkg.getPackagedElements()) {
			if (pe instanceof Package) {
				dtList.addAll(getMessages((Package) pe));
			} else if (pe instanceof DataType) {
				DataType dt = (DataType) pe;
				List<TemplateBinding> tbs = getTemplateBindings(dt);
				boolean pushOrSend = isPushOrSend(tbs);

				// treat "no pattern" (size of list = 0) as message (this is the case for
				// messages referenced by other messages)
				if (pushOrSend || tbs.size() == 0) {
					dtList.add(dt);
				}
			}
		}
		return dtList;
	}

	/**
	 * @param tbs
	 *            a list of template bindings (originating from a service definition)
	 * @return true, if the list of passed patterns contains a push or send one
	 */
	protected static boolean isPushOrSend(List<TemplateBinding> tbs) {
		for (TemplateBinding tb : tbs) {
			final CommunicationPattern pattern = getCommunicationPattern(tb);
			if (pattern != null) {
				boolean pushOrSend = isPush(pattern) || isSend(pattern) || isPubSub(pattern);
				if (pushOrSend) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Get the send definitions within a message package, i.e. service definitions for the Send pattern
	 *
	 * @param messagePkg
	 *            a message package (package with communication objects)
	 * @return the list of service definitions for data types with the Query pattern
	 */
	public static List<Interface> getSends(Package messagePkg) {
		return getServiceDefinitions(messagePkg, SEND);
	}

	/**
	 * Get the query definitions within a message package, i.e. service definitions for the Query pattern
	 *
	 * @param messagePkg
	 *            a message package (package with communication objects)
	 * @return the list of service definitions for data types with the Query pattern
	 */
	public static List<Interface> getQueries(Package messagePkg) {
		return getServiceDefinitions(messagePkg, QUERY);
	}

	/**
	 * Get the service definitions within a message package, i.e. service definitions for the ACTION pattern
	 *
	 * @param messagePkg
	 *            a message package (package with communication objects)
	 * @return the list of service definitions for data types with the Query pattern
	 */
	public static List<Interface> getActions(Package messagePkg) {
		return getServiceDefinitions(messagePkg, ACTION);
	}

	/**
	 * Get service definitions from a message package
	 * 
	 * @param messagePkg
	 *            a message package
	 * @param filterPattern
	 *            the patterns to look for (Query, Push, ...)
	 * @return the list of service definitions matching the filterPattern
	 */
	public static List<Interface> getServiceDefinitions(Package messagePkg, String filterPattern) {
		List<Interface> sdList = new ArrayList<Interface>();
		for (PackageableElement pe : messagePkg.getPackagedElements()) {
			if (pe instanceof Package) {
				sdList.addAll(getServiceDefinitions((Package) pe, filterPattern));
			} else if (pe instanceof Interface) {
				Interface sd = (Interface) pe;
				TemplateBinding tb = getTemplateBinding(sd);
				if (tb != null) {
					final CommunicationPattern pattern = getCommunicationPattern(tb);
					if (pattern != null) {
						String patternName = InteractionUtils.getPatternName(pattern);
						if (patternName.equals(filterPattern) && !sdList.contains(sd)) {
							sdList.add(sd);
						}
					}
				}
			}
		}
		return sdList;
	}

	/**
	 * @param cp
	 *            a communication pattern
	 * @return true, if the passed communication pattern is a push or publish-subscribe pattern
	 * 
	 */
	public static boolean isPush(CommunicationPattern cp) {
		return PUSH.equals(getPatternName(cp));
	}

	/**
	 * @param cp
	 *            a communication pattern
	 * @return true, if the passed communication pattern is a publish-subscribe pattern
	 * 
	 */
	public static boolean isPubSub(CommunicationPattern cp) {
		return PUB_SUB.equals(getPatternName(cp));
	}

	/**
	 * @param cp
	 *            a communication pattern
	 * @return true, if the passed communication pattern is a send pattern
	 */
	public static boolean isSend(CommunicationPattern cp) {
		return SEND.equals(getPatternName(cp));
	}

	/**
	 * @param cp
	 *            a communication pattern
	 * @return true, if the passed communication pattern is a query pattern
	 */
	public static boolean isQuery(CommunicationPattern cp) {
		return QUERY.equals(getPatternName(cp));
	}

	/**
	 * @param cp
	 *            a communication pattern
	 * @return true, if the passed communication pattern is an event pattern
	 */
	public static boolean isEvent(CommunicationPattern cp) {
		return EVENT.equals(getPatternName(cp));
	}

	/**
	 * @param cp
	 *            a communication pattern
	 * @return true, if the passed communication pattern is an event pattern
	 */
	public static boolean isAction(CommunicationPattern cp) {
		return ACTION.equals(getPatternName(cp));
	}

	/**
	 * Return the list of template bindings for a given communication object, i.e.
	 * the template bindings originating from a set of service definitions
	 * 
	 * @param commObject
	 *            a communication object
	 * @return the list of associate template bindings
	 */
	public static List<TemplateBinding> getTemplateBindings(DataType commObject) {
		List<TemplateBinding> tbs = new ArrayList<TemplateBinding>();
		for (Setting setting : UMLUtil.getNonNavigableInverseReferences(commObject)) {
			EObject eObj = setting.getEObject();
			if (eObj instanceof TemplateParameterSubstitution) {
				TemplateParameterSubstitution tps = (TemplateParameterSubstitution) eObj;
				tbs.add(tps.getTemplateBinding());
			}
		}
		return tbs;
	}

	/**
	 * return a connector label for a given connector. It provides the communication objects
	 * 
	 * @param connector
	 *            a connector
	 * @return the connector label
	 */
	public static String getConnectorLabel(Connector connector) {
		String names = null;
		TemplateBinding tb = getTemplateBinding(connector);
		if (tb != null) {
			for (TemplateParameterSubstitution tps : tb.getParameterSubstitutions()) {
				ParameterableElement pe = tps.getActual();
				if (pe instanceof NamedElement) {
					String name = ((NamedElement) pe).getName();
					names = (names == null) ? name : names + ", " + name; //$NON-NLS-1$
				}
			}
		}
		return (names != null) ? names : ""; //$NON-NLS-1$
	}

	/**
	 * Return the name of the service definition, without the optional prefix that
	 * is applied during reverse, i.e. without P_, Q_, S_, A_ and U_
	 * Check avoids specific prefixes defined in plugin oep.robotics.library
	 * (dependency is not possible, would be cyclic)
	 */
	public static String getNameWoPrefix(Interface sd) {
		String name = sd.getName();
		if (name != null && name.length() > 2) {
			// check, if name starts with an upperCase letter, followed by _
			if (Character.isUpperCase(name.charAt(0)) && name.charAt(1) == '_') {
				return name.substring(2);
			}
		}
		return name;
	}

}
