/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EReferenceTreeElement;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;


/**
 * Filter elements depending on their stereotypes, i.e. only provide elements that apply a certain
 * stereotype.
 */
public abstract class AbstractFilterProvider extends EncapsulatedContentProvider {

	/**
	 * Constructor.
	 *
	 * @param encapsulated
	 *            An existing content provider
	 * @param stereotypeFilter
	 *            a filter condition
	 */
	AbstractFilterProvider(IStaticContentProvider encapsulated) {
		super(encapsulated);
	}

	@Override
	public Object[] getElements() {
		// flat list case
		return filteredList(super.getElements());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		return filteredList(super.getElements(inputElement));
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		return filteredList(super.getChildren(parentElement));
	}

	@Override
	public boolean isValidValue(final Object element) {
		return super.isValidValue(element);
	}


	protected Object[] filteredList(Object[] list) {
		ArrayList<Object> filteredElements = new ArrayList<Object>();
		for (Object element : list) {
			EObject eObj = null;
			if (element instanceof EObject) {
				eObj = (EObject) element;
			} else if (element instanceof EObjectTreeElement) {
				eObj = ((EObjectTreeElement) element).getEObject();
			} else if (element instanceof EReferenceTreeElement) {
				// happens in case of advanced model explorer, add directly
				filteredElements.add(element);
			} else {
				Activator.log.debug("element must be either EObject, EObjectTreeElement or EReferenceTreeElement"); //$NON-NLS-1$
			}
			if (eObj != null) {
				if (hasChildren(element) && hasFilteredChildren(eObj)) {
					filteredElements.add(element);
				} else if (isValid(eObj)) {
					filteredElements.add(eObj);
				}
			}
		}
		return filteredElements.toArray();
	}

	protected boolean hasFilteredChildren(EObject eObj) {
		TreeIterator<EObject> treeIter = eObj.eAllContents();
		while (treeIter.hasNext()) {
			EObject child = treeIter.next();
			if (isValid(child)) {
				return true;
			}
		}
		return false;
	}

	abstract boolean isValid(Object object);
}
