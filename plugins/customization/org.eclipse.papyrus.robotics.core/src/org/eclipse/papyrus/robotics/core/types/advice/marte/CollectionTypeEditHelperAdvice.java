/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice.marte;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.MARTE.MARTE_Annexes.VSL.DataTypes.CollectionType;
import org.eclipse.papyrus.designer.transformation.base.utils.StdModelLibs;
import org.eclipse.papyrus.robotics.core.types.advice.AbstractApplyStereotypeEditHelperAdvice;
import org.eclipse.papyrus.robotics.core.utils.MarteUtils;

/**
 * Edit Helper Advice that applied MARTE NFPs sub-profile
 */
public class CollectionTypeEditHelperAdvice extends AbstractApplyStereotypeEditHelperAdvice {

	@Override
	protected URI getProfileURI() {
		return StdModelLibs.MARTE_PROFILE_URI;
	}
	
	@Override
	protected String getSubProfileQName() {
		return MarteUtils.MARTE_DATA_TYPES_SUBPROFILE;
	}

	@Override
	protected Class<? extends EObject> stereotypeToApply() {
		return CollectionType.class;
	}
}
