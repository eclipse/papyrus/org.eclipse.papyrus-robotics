/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.MoveRequest;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Type;

public class PortCommandUtils {
	public static void avoidDuplicatesCommand(Port port) {
		if (PortUtils.isCSDuplicate(port)) {
			// the CS is also the type of another port (e.g. due to copying the port)
			// => create a new CS for the current port
			try {
				PortUtils.associateCSCommand(port).execute(null, null);
			} catch (ExecutionException e) {
				Activator.log.error(e);
			}
		}
	}

	/**
	 * If the type of the port is not already a nested classifier of the class owning
	 * the port, move it towards this class (enforce conventions)
	 * A advantage of this convention is that moving the class around will automatically
	 * move this component service as well.
	 *
	 * @param cc
	 *            an existing composite command
	 * @param port
	 *            the port whose type should eventually be moved
	 */
	public static void addMoveCSCommand(CompositeCommand cc, Port port) {
		Type type = port.getType();
		if (type != null && StereotypeUtil.isApplied(type, ComponentService.class)) {
			if (port.getClass_() != type.getOwner()) {
				IElementEditService provider = ElementEditServiceUtils.getCommandProvider(port);
				IEditCommandRequest request = new MoveRequest(port.getClass_(), type);
				cc.add(provider.getEditCommand(request));
			}
		}
	}

	/**
	 * Add a removal command for existing client dependencies (usages) of the
	 * type of a port. This removes existing "required" relationships
	 *
	 * @param cc
	 *            an existing composite command
	 * @param port
	 *            the port whose "required" relationships should be cleared
	 */
	public static void addDeleteDependenciesCommand(CompositeCommand cc, Port port) {
		if (port.getType() instanceof org.eclipse.uml2.uml.Class) {
			IElementEditService provider = ElementEditServiceUtils.getCommandProvider(port);
			Class clazz = (Class) port.getType();
			for (Dependency dep : clazz.getClientDependencies()) {
				IEditCommandRequest request = new DestroyElementRequest(dep, false);
				cc.add(provider.getEditCommand(request));
			}
		}
	}
}
