/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.menu;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsLabelProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsMenuLabelProvider;
import org.eclipse.uml2.uml.Type;

/**
 * Simplify the creation of an enhanced popup menu
 */
public class MenuHelper {
	
	public static final String CREATE_NEW_TYPE = "Create new type"; //$NON-NLS-1$

	/**
	 * Create a list of menu items based on the result content provider
	 * @param cp a content provider
	 * @param title the title of the dialog
	 * @param allowNew if true, allow for the creation of a new type
	 * @return a list of menu items
	 */
	public static List<Object> createMenuItems(IStaticContentProvider cp, String title, boolean allowNew) {
		List<Object> menuItems = new ArrayList<Object>();

		menuItems.add(new EnhancedPopupMenu.Disabled(title));
		menuItems.add(new EnhancedPopupMenu.Separator());
		if (allowNew) {
			menuItems.add(CREATE_NEW_TYPE);
		}
		Object elements[] = cp.getElements();
		if (elements.length > 0) {
			boolean containsURI = false;
			for (Object element : elements) {
				if (element instanceof URI) {
					containsURI = true;
					break;
				}
			}
			if (elements.length < 5 && !containsURI) {
				if (elements[0] instanceof Type) {
					// display short info text, if the first element (and presumably the following)
					// are UML types
					menuItems.add(new EnhancedPopupMenu.Disabled("Existing types")); //$NON-NLS-1$
				}

				for (Object element : cp.getElements()) {
					menuItems.add(element);
				}
			} else {
				menuItems.add(new EnhancedPopupMenu.SubSelect(title, cp));
			}
		}
		return menuItems;
	}

	public static EnhancedPopupMenu createPopupMenu(IStaticContentProvider cp, String title, boolean allowNew) {
		ILabelProvider roboticsLP = new RoboticsMenuLabelProvider();
		return new EnhancedPopupMenu(createMenuItems(cp, title, allowNew), new RoboticsLabelProvider(roboticsLP));
	}

}
