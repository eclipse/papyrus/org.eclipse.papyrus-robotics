/*****************************************************************************
 * Copyright (c) 2019, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *    Ansgar Radermacher - bug 581833
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.core.commands.DeferredCompositeCommand;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;

public class ImportUtils {
	/**
	 * Convenience function that checks whether passed object is
	 * an Element, and calls createImport, if that is the case.
	 * 
	 * @param obj
	 *            eventually an Element
	 */
	public static void createImportFromObj(Object obj) {
		if (obj instanceof Element) {
			createImport((Element) obj);
		}
	}

	/**
	 * Create a package-import for the passed element, if
	 * (1) it is not part of the main model edited by the user
	 * (2) there is not already a package import
	 * 
	 * @param element
	 *            an element
	 */
	public static void createImport(Element element) {
		ResourceSet rs = element.eResource().getResourceSet();
		if (rs instanceof ModelSet) {
			ModelSet modelSet = (ModelSet) rs;
			Resource umlR = UmlUtils.getUmlResource(modelSet);
			Package root = (Package) umlR.getContents().get(0);
			Package pkgToImport = PackageUtil.getRootPackage((Element) element);


			if (pkgToImport != root && !root.getImportedPackages().contains(pkgToImport)) {
				// create package import
				CommandStack stack = modelSet.getTransactionalEditingDomain().getCommandStack();
				IElementEditService provider = ElementEditServiceUtils.getCommandProvider(root);

				// execute both commands in a composite
				CompositeCommand cc = new CompositeCommand("Create package import"); //$NON-NLS-1$

				CreateElementRequest createPiReq = new CreateElementRequest(root, UMLElementTypes.PACKAGE_IMPORT);
				ICommand createPiCmd = provider.getEditCommand(createPiReq);

				// use a deferred composite for 2nd command
				DeferredCompositeCommand wrapper = new DeferredCompositeCommand() {

					@Override
					public ICommand createDeferredCommand() {
						// add command now (command result is not known before)
						Object result = createPiCmd.getCommandResult().getReturnValue();
						if (result instanceof EObject) {
							SetRequest setPackageReq = new SetRequest((EObject) result, UMLPackage.eINSTANCE.getPackageImport_ImportedPackage(), pkgToImport);
							ICommand setPackageCmd = provider.getEditCommand(setPackageReq);
							return setPackageCmd;
						}
						return null;
					}
				};

				cc.add(createPiCmd);
				cc.add(wrapper);
				stack.execute(GMFtoEMFCommandWrapper.wrap(cc));
			}
		}
	}
}
