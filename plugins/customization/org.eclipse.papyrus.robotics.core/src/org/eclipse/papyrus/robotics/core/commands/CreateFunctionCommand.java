/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.commands;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;

/**
 * Create a function (opaque behavior) for an activity and an attribute typed with that function
 * The function gets created in the nearest package of the activity, the attribute is not associated
 * with any class, it needs to be added by the caller.
 */
public class CreateFunctionCommand extends RecordingCommand {

	protected Class activity;

	protected Property functionAttr;

	public CreateFunctionCommand(Class activity) {
		super(TransactionUtil.getEditingDomain(activity), "Add function"); //$NON-NLS-1$
		this.activity = activity;
	}

	@SuppressWarnings("nls")
	@Override
	protected void doExecute() {
		// create a new attribute and type that attribute with a new function (created by default
		// in the nearest package)
		functionAttr = (Property) UMLFactory.eINSTANCE.createProperty();
		functionAttr.setName(""); // don't use a name (avoid duplicate name warnings during validation)
		// add to operation and apply stereotype
		OpaqueBehavior function = UMLFactory.eINSTANCE.createOpaqueBehavior();
		function.getLanguages().add("C++");
		function.getBodies().add("");
		// use a valid C++ name
		function.setName("fNewFunction");
		activity.getNearestPackage().getPackagedElements().add(function);
		StereotypeUtil.apply(function, Function.class);
		functionAttr.setType(function);
	}

	/**
	 * @return the created function attribute. Must be called after execution of this command
	 */
	public Property getFunctionAttr() {
		return functionAttr;
	}
};
