/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types;

import org.eclipse.gmf.runtime.emf.type.core.AbstractElementTypeEnumerator;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;

/**
 * Static list of robotics specific element types
 */
public class RoboticElementTypesEnumerator extends AbstractElementTypeEnumerator implements IRoboticElementTypes {

	public static final IHintedType COMPONENT_SERVICE = (IHintedType) getElementType(COMPONENT_SERVICE_ID);

	public static final IHintedType IN_ATTRIBUTE = (IHintedType) getElementType(IN_ATTRIBUTE_ID);
	public static final IHintedType OUT_ATTRIBUTE = (IHintedType) getElementType(OUT_ATTRIBUTE_ID);
	public static final IHintedType SKILL_RESULT = (IHintedType) getElementType(SKILL_RESULT_ID);
	
	public static final IHintedType PARAM_ENTRY = (IHintedType) getElementType(PARAM_ENTRY_ID);
	public static final IHintedType ACTIVITY_INSTANCE = (IHintedType) getElementType(ACTIVITY_INSTANCE_ID);
}
