/*****************************************************************************
 * Copyright (c) 2019, 2020, 2021, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr>       - Bug 568720
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Bug 574543, Bug 581428
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr>       - Bug 581690 (reverted in bug 581961)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.utils.ScanUtils;
import org.eclipse.uml2.uml.Package;

/**
 * A (robotics) content provider that also takes workspace and plugin matches into account
 *
 */
public class RoboticsContentProvider extends EncapsulatedContentProvider implements IStaticContentProvider {

	private static final String EMPTY = ""; //$NON-NLS-1$

	protected static final String MODELS_VIA_PATHMAPS = "Models via pathmaps"; //$NON-NLS-1$

	protected static final String WORKSPACE_MATCHES = "Workspace matches"; //$NON-NLS-1$

	protected String extension;

	/**
	 * The resource-set use temporarily to load resources to display their contents
	 */
	protected ResourceSet tempRS;

	/**
	 * The resource-set in which the encapsulated content provider looks for matches.
	 */
	protected ResourceSet encapsulatedRS;

	/**
	 * A stereotype to filter. Might be null.
	 */
	protected Class<? extends EObject> stereoFilterClass;

	/**
	 *
	 * Constructor.
	 *
	 * @param context
	 *            a context object used to obtain the resource set
	 * @param encapsulated
	 *            an existing content provider
	 * @param stereotypeFilter
	 *            a stereotype a list of stereotype filters
	 * @param extension
	 *            a file extension to search for in workspace and plugins
	 */
	public RoboticsContentProvider(EObject context, IStaticContentProvider encapsulated, Class<? extends EObject> stereoFilterClass, String extension) {
		if (context.eResource() != null) {
			this.encapsulatedRS = context.eResource().getResourceSet();
		} else {
			Activator.log.warn("RoboticsContentProvider: passed context is not contained in a resource (proxy?)"); //$NON-NLS-1$
		}
		// the method "containsStereotype" uses the "stereotypeFilter" to select (or not)
		// a complete resource, additional filtering on element level is done via the
		// encapsulated provider
		if (stereoFilterClass != null) {
			this.encapsulated = new FilterStereotypes(encapsulated, stereoFilterClass);
		}
		else {
			this.encapsulated = encapsulated;
		}
		this.stereoFilterClass = stereoFilterClass;
		this.extension = extension;
		tempRS = new ResourceSetImpl();
	}

	/**
	 *
	 * Constructor.
	 *
	 * @param context
	 *            a context object used to obtain the resource set
	 * @param encapsulated
	 *            an existing content provider
	 * @param extension
	 *            a file extension to search for in workspace and plugin
	 */
	public RoboticsContentProvider(EObject context, IStaticContentProvider encapsulated, String extension) {
		this(context, encapsulated, null, extension);
	}

	protected Object[] getTopElements() {
		// Bug 574543 - don't use "Model matches" top level element, append existing
		// top-level elements
		Object[] topOrig = super.getElements(EMPTY);
		Object[] top = new Object[topOrig.length+2];
		top[0] = MODELS_VIA_PATHMAPS;
		top[1] = WORKSPACE_MATCHES;
		for (int i = 0; i<topOrig.length; i++) {
			top[i+2] = topOrig[i];
		}
		return top;
	}

	@Override
	public Object[] getElements(Object parent) {
		List<Object> results = new ArrayList<Object>();
		List<URI> availableURIs = null;
		if (parent.equals(EMPTY)) {
			return getTopElements();
		}
		if (parent == WORKSPACE_MATCHES) {
			availableURIs = ScanUtils.modelURIsInWorkspace(extension);
			results.addAll(availableURIs);
		} else if (parent == MODELS_VIA_PATHMAPS) {
			availableURIs = ScanUtils.pathmapsWithModels(extension);
			results.addAll(availableURIs);
		}
		return results.toArray();
	}

	/**
	 * Check, whether a resource contains searched stereotypes (used in the hasChildren) dialog
	 */
	protected boolean containsStereotype(Resource r) {
		if (stereoFilterClass == null) {
			return true;
		}
		for (EObject obj : r.getContents()) {
			if (stereoFilterClass.isInstance(obj)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the resource set handled by the encapsulated content provider
	 */
	public ResourceSet getResourceSet() {
		return encapsulatedRS;
	}

	/** handle "flat" case in pop-up menu => return all elements that are eligible */
	@Override
	public Object[] getElements() {
		List<Object> results = new ArrayList<Object>();
		for (Object obj : super.getElements()) {
			// replace tree elements with contained EObject
			if (obj instanceof EObjectTreeElement) {
				obj = ((EObjectTreeElement) obj).getEObject();
			}
			results.add(obj);
		}
		// add URIs (it's not useful to expand the complete (filtered) content, as it
		// would imply to we need to load all models first
		// The pop-up menu will open the hierarchical dialog, if the menu contains URIs
		results.addAll(ScanUtils.modelURIsInWorkspace(extension));
		results.addAll(ScanUtils.allPathmapModels(extension));
		return results.toArray();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof String) {
			// on of the three model elements
			return getElements(parentElement);
		} else if (parentElement instanceof URI) {
			URI uri = (URI) parentElement;
			if (ScanUtils.matchesExtension(uri.path(), extension)) {
				// URI of model, either via a pathmap or via within a project
				Resource r = tempRS.getResource(uri, true);
				if (r != null && r.getContents().size() > 0) {
					Package subRoot = (Package) r.getContents().get(0);
					return getChildren(subRoot);
				}
			} else if (uri.segmentCount() == 1) {
				// handle case of single project encoded in an URI
				IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(uri.segment(0));
				List<URI> uriList = new ArrayList<URI>();
				try {
					ScanUtils.processContainer(project, extension, uriList);
				} catch (CoreException e) {
					Activator.log.error(e);
				}
				return uriList.toArray();
			} else {
				// URI of plugin or pathmap
				return ScanUtils.modelsFromPath(uri, extension).toArray();
			}
		}
		if (encapsulated instanceof ITreeContentProvider) {
			return ((ITreeContentProvider) encapsulated).getChildren(parentElement);
		} else {
			return new Object[0];
		}
	}
}
