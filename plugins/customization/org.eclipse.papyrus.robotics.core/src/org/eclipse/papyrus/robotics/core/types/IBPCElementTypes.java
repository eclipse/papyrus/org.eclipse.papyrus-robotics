/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   Ansgar Radermacher
 *   
 *****************************************************************************/
package org.eclipse.papyrus.robotics.core.types;

/**
 * Declaration of all constants for UCM Element Types.
 */
public interface IBPCElementTypes {

	public static final String PREFIX = "org.eclipse.papyrus.robotics.core.bpc."; //$NON-NLS-1$

	public static final String ENTITY_ID = PREFIX + "Block"; //$NON-NLS-1$

	public static final String BLOCK_ID = PREFIX + "Block"; //$NON-NLS-1$

}
