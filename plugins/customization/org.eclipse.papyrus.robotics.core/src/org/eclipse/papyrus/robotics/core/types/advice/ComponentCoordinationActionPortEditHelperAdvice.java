/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation (Bug #581690)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

/**
 * Edit Helper Advice for creating a new type based on coordination services and typing the port with this type
 */
public class ComponentCoordinationActionPortEditHelperAdvice extends AbstractComponentCoordinationPortEditHelperAdvice {

	public static final String ACTION_PATTERN = "robotics::commpatterns::Action";

	public static final String COORDINATION_KIND = "Action";

	@Override
	protected String getInteractionPatternQualifName() {
		return ACTION_PATTERN;
	}

	@Override
	protected String getCoordinationServiceKind() {
		return COORDINATION_KIND;
	}
}
