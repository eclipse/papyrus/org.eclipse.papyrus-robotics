/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types;

import org.eclipse.gmf.runtime.emf.type.core.AbstractElementTypeEnumerator;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;

/**
 * Static list of UCM specific element types
 */
public class BPCElementTypesEnumerator extends AbstractElementTypeEnumerator implements IBPCElementTypes {

	public static final IHintedType BLOCK = (IHintedType) getElementType(BLOCK_ID);

	public static final IHintedType ENTITY = (IHintedType) getElementType(ENTITY_ID);
}
