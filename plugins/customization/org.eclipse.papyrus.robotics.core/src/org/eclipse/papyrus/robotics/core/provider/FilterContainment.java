/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;


/**
 * Filter elements depending on their stereotypes, i.e. only provide elements that apply a certain
 * stereotype.
 */
public class FilterContainment extends AbstractFilterProvider {

	/**
	 * Constructor.
	 *
	 * @param encapsulated An existing content provider
	 */
	public FilterContainment(IStaticContentProvider encapsulated) {
		super(encapsulated);
	}

	/**
	 * Set parent object for containment
	 * @param parent
	 */
	public void setParent(EObject parent) {
		this.parent = parent;
	}
	
	EObject parent;

	@Override
	public boolean isValid(final Object object) {
		if (object instanceof EObject) {
			return isParent(parent, (EObject) object);
		}
		else {
			return false;
		}
	}

	protected boolean isParent(EObject parent, EObject tstObject) {
		EObject tstParent = tstObject.eContainer();
		if (tstObject.eContainer() == parent) {
			return true;
		}
		else if (tstParent != null) {
			return isParent(parent, tstParent);
		}
		return false;
	}
}
