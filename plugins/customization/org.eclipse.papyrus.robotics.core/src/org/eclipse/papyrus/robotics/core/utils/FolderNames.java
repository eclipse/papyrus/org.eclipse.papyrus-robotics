/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

public class FolderNames {
	public static final String MODELS = "models"; //$NON-NLS-1$

	public static final String TASKS = "tasks"; //$NON-NLS-1$

	public static final String SKILLS = "skills"; //$NON-NLS-1$

	public static final String SYSTEM = "system";  //$NON-NLS-1$

	public static final String SERVICES = "services";  //$NON-NLS-1$

	public static final String COMPONENTS = "components";  //$NON-NLS-1$

	public static final String TASKBASEDHARA = "taskbased-hara";  //$NON-NLS-1$
}
