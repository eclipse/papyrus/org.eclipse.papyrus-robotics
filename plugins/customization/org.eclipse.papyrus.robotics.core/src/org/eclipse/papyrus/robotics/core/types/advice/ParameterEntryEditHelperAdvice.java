/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu.SubSelect;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper Advice for creating a new type and typing the port with this type
 */
public class ParameterEntryEditHelperAdvice extends AbstractEditHelperAdvice {

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateElementRequest) {
				CreateElementRequest createReq = (CreateElementRequest) context.getEditCommandRequest();
				if (createReq.getContainer() instanceof Classifier) {
					return StereotypeUtil.isApplied((Classifier) createReq.getContainer(), Parameter.class);
				}
				return false;
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Create additional ComponentService and type port with that service.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Property)) {
			return super.getAfterConfigureCommand(request);
		}
		final Property property = (Property) newElement;

		CompositeCommand compositeCommand = new CompositeCommand("Set property type"); //$NON-NLS-1$

		EncapsulatedContentProvider cpWithWS = new UMLContentProvider(property, UMLPackage.eINSTANCE.getTypedElement_Type());
		SubSelect serviceDef = new EnhancedPopupMenu.SubSelect("Select parameter type", cpWithWS); //$NON-NLS-1$
		Object value = serviceDef.getResult();

		if (value instanceof Classifier) {
			final Classifier newType = (Classifier) value;

			SetRequest setNameRequest = new SetRequest(property, UMLPackage.eINSTANCE.getTypedElement_Type(), newType);
			ICommand setNameCmd = ElementEditServiceUtils.getCommandProvider(property).getEditCommand(setNameRequest);
			compositeCommand.add(setNameCmd);

		} else {
			return new CancelCommand(property);
		}

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	protected EObject source;
	protected EObject target;
}
