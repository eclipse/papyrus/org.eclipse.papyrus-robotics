/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

public class MarteUtils {
	public static final String MARTE_DATA_TYPES_SUBPROFILE = "MARTE::MARTE_Annexes::VSL::DataTypes"; //$NON-NLS-1$
	public static final String MARTE_NFPS_SUBPROFILE = "MARTE::MARTE_Foundations::NFPs"; //$NON-NLS-1$
}
