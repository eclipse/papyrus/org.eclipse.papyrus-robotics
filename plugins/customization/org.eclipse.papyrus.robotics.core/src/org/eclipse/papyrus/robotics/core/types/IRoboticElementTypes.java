/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   Ansgar Radermacher
 *   
 *****************************************************************************/
package org.eclipse.papyrus.robotics.core.types;

/**
 * Declaration of all constants for robotic element types.
 */
@SuppressWarnings("nls")
public interface IRoboticElementTypes {

	public static final String PREFIX = "org.eclipse.papyrus.robotics.";

	public static final String COMPONENT_SERVICE_ID = PREFIX + "ComponentService";

	public static final String IN_ATTRIBUTE_ID = PREFIX + "InAttribute";
	public static final String OUT_ATTRIBUTE_ID = PREFIX + "OutAttribute";
	public static final String SKILL_RESULT_ID = PREFIX + "SkillResult";

	public static final String PARAM_ENTRY_ID = PREFIX + "ParameterEntry";
	public static final String ACTIVITY_INSTANCE_ID = PREFIX + "ActivityInstance";
}
