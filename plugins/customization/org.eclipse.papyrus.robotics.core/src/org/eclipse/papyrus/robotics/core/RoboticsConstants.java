package org.eclipse.papyrus.robotics.core;

@SuppressWarnings("nls")
public class RoboticsConstants {
	public static final String ROBOTICS_MODELING_CONTEXT = "org.eclipse.papyrus.robotics.architecture";
	
	public static final String ROBOTICS_COMPONENT_DEV_VIEWPOINT_ID = "org.eclipse.papyrus.robotics.viewpoint.ComponentDevelopment";
	
	public static final String ROBOTICS_SERVICE_DESIGN_VIEWPOINT_ID = "org.eclipse.papyrus.robotics.viewpoint.ServiceDesign";
	
	public static final String ROBOTICS_SERVICE_FULFILLMENT_VIEWPOINT_ID = "org.eclipse.papyrus.robotics.viewpoint.ServiceFulfillment";
	
	public static final String ROBOTICS_SYSTEM_CONFIGURATION_VIEWPOINT_ID = "org.eclipse.papyrus.robotics.viewpoint.SystemConfiguration";
}
