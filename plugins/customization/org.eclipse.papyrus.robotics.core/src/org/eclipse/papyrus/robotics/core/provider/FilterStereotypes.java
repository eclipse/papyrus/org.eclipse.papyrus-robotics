/*****************************************************************************
 * Copyright (c) 2017-2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr>       - Bug 568720
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;


/**
 * Filter elements depending on their stereotypes, i.e. only provide elements that apply a certain
 * stereotype.
 */
public class FilterStereotypes extends AbstractFilterProvider {

	protected Class<? extends EObject> stereotypeFilter;

	/**
	 * Constructor.
	 *
	 * @param encapsulated
	 *            An existing content provider
	 * @param stereotypeFilter
	 *            the class representing a stereotype.
	 */
	public FilterStereotypes(IStaticContentProvider encapsulated, Class<? extends EObject> stereotypeFilter) {
		super(encapsulated);
		this.stereotypeFilter = stereotypeFilter;
	}

	/**
	 * Read access to the filter condition.
	 *
	 * @return stereotypeFilter a filter condition
	 */
	public Class<? extends EObject> getStereotypeFilter() {
		return stereotypeFilter;
	}

	@Override
	public boolean isValid(final Object object) {
		if (object instanceof Element) {
			return StereotypeUtil.isApplied((Element) object, stereotypeFilter);
		}
		return false;
	}
}
