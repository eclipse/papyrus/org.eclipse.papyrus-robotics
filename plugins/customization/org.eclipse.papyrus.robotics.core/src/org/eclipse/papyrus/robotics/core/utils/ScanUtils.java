package org.eclipse.papyrus.robotics.core.utils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.papyrus.robotics.core.Activator;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.osgi.framework.Bundle;

public class ScanUtils {
	private static final String ORG_ECLIPSE_EMF_ECORE_URI_MAPPING = "org.eclipse.emf.ecore.uri_mapping"; //$NON-NLS-1$

	/**
	 * @param extension filter models having this extension (a "|" allows to check
	 *                  for multiple extensions)
	 * @return a list of URIs of the projects that contain a model with the searched
	 *         extension
	 */
	public static List<URI> modelURIsInWorkspace(String extension) {
		List<URI> list = new ArrayList<URI>();
		// get all projects within the workspace
		for (IProject project : ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
			try {
				if (project.isOpen()) {
					List<URI> checkURIs = new ArrayList<URI>();
					processContainer(project, extension, checkURIs);
					if (checkURIs.size() > 0) {
						URI uri = URI.createURI(project.getName());
						list.add(uri);
					}
				}
			} catch (CoreException e) {
				Activator.log.error(e);
			}
		}
		return list;
	}

	/**
	 * @param fileNameEnd end of file name used for matching (a "|" allows to check
	 *                    for multiple names)
	 * @return the first model with a matching URI
	 */
	public static URI firstModelInWorkspace(String fileNameEnd) {
		// get all projects within the workspace
		for (IProject project : ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
			try {
				if (project.isOpen()) {
					List<URI> checkURIs = new ArrayList<URI>();
					processContainer(project, fileNameEnd, checkURIs);
					if (checkURIs.size() > 0) {
						return checkURIs.get(0);
					}
				}
			} catch (CoreException e) {
				Activator.log.error(e);
			}
		}
		return null;
	}

	/**
	 * Return pathmaps from which models with the right extensions are accessible.
	 * Note that this function only returns the source URI of the pathmap, not the
	 * URIs of the models contained.
	 * 
	 * @param extension an extension (a "|" allows to check for multiple extensions)
	 * @return list of URIs
	 */
	public static List<URI> pathmapsWithModels(String extension) {
		List<URI> list = new ArrayList<URI>();
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] configElements = reg.getConfigurationElementsFor(ORG_ECLIPSE_EMF_ECORE_URI_MAPPING);

		for (IConfigurationElement configElement : configElements) {
			final String source = configElement.getAttribute("source"); //$NON-NLS-1$
			final String target = configElement.getAttribute("target"); //$NON-NLS-1$
			URI uri = URI.createURI(target);
			if (uri.isPlatformPlugin()) {
				String pluginId = uri.segment(1);
				Bundle bundle = Platform.getBundle(pluginId);
				String path = uri.toPlatformString(false).substring(pluginId.length() + 2);
				if (bundle != null) {
					if (hasBundleModels(bundle, path, extension)) {
						list.add(URI.createURI(source));
					}
				}
			}
		}
		return list;
	}

	/**
	 * return the URIs of models accessible from a given path
	 * 
	 * @param uri       the URI of a pathmap - all URIs accessible via that pathmap
	 *                  are returned
	 * @param extension the file extension to scan for (a "|" allows to check for
	 *                  multiple extensions)
	 * @return list of model URIs
	 */
	public static List<URI> modelsFromPath(URI uri, String extension) {
		List<URI> list = new ArrayList<URI>();
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		String uriStr = uri.toString();
		IConfigurationElement[] configElements = reg.getConfigurationElementsFor(ORG_ECLIPSE_EMF_ECORE_URI_MAPPING);
		for (IConfigurationElement configElement : configElements) {
			final String source = configElement.getAttribute("source"); //$NON-NLS-1$
			final String target = configElement.getAttribute("target"); //$NON-NLS-1$
			if (source.equals(uriStr)) {
				URI targetUri = URI.createURI(target);
				if (targetUri.isPlatformPlugin()) {
					String pluginId = targetUri.segment(1);
					Bundle bundle = Platform.getBundle(pluginId);
					String path = targetUri.toPlatformString(false).substring(pluginId.length() + 1);
					if (bundle != null) {
						scanBundle(bundle, source, path, path, extension, list);
					}
				}
			}
		}
		return list;
	}

	public static List<URI> allPathmapModels(String extension) {
		List<URI> list = new ArrayList<URI>();
		for (URI uri : pathmapsWithModels(extension)) {
			list.addAll(modelsFromPath(uri, extension));
		}
		return list;
	}

	/**
	 * check, whether a bundle contains models with a given extension.
	 * 
	 * @param bundle    a bundle
	 * @param path      an initial path within the bundle
	 * @param extension an extension (a "|" allows to check for multiple extensions)
	 * @return true, if the bundle contains models with the given extension
	 */
	protected static boolean hasBundleModels(Bundle bundle, String path, String extension) {
		Enumeration<URL> urlEnum;
		urlEnum = bundle.findEntries(path, "*", false); //$NON-NLS-1$
		if (urlEnum != null) {
			while (urlEnum.hasMoreElements()) {
				URL url = urlEnum.nextElement();
				if (matchesExtension(url.getPath(), extension)) {
					return true;
				} else {
					boolean found = hasBundleModels(bundle, url.getPath(), extension);
					if (found) {
						return true;
					}
				}
			}
		}
		return false;
	}

	protected static void scanBundle(Bundle bundle, String pathmap, String initialPath, String path, String extension,
			List<URI> list) {
		Enumeration<URL> urlEnum;
		urlEnum = bundle.findEntries(path, "*", false); //$NON-NLS-1$
		if (urlEnum != null) {
			while (urlEnum.hasMoreElements()) {
				URL url = urlEnum.nextElement();
				if (matchesExtension(url.getPath(), extension)) {
					String modelPath = pathmap + url.getPath().replaceFirst(initialPath, ""); //$NON-NLS-1$
					URI uri = URI.createURI(modelPath);
					list.add(uri);

				} else {
					scanBundle(bundle, pathmap, initialPath, url.getPath(), extension, list);
				}
			}
		}
	}

	/**
	 * Move the passed eObject into the target resource set.
	 * 
	 * @param targetRS a resource set into which the value should be loaded
	 * @param value    an EObject
	 * @return the EObject within the target resource set
	 */
	public static EObject moveIntoRS(ResourceSet targetRS, EObject value) {
		Resource sourceRes = value.eResource();
		if (sourceRes.getResourceSet() != targetRS) {
			URI sourceURI = sourceRes.getURI();
			Resource targetRes = targetRS.getResource(sourceURI, true);
			return targetRes.getEObject(sourceRes.getURIFragment(value));
		}
		return value;
	}

	public static Class getCompFromResource(Resource r) {
		Iterator<EObject> iter = r.getAllContents();
		while (iter.hasNext()) {
			EObject candidate = iter.next();
			if (candidate instanceof Class && StereotypeUtil.isApplied((Class) candidate, ComponentDefinition.class)) {
				return (Class) candidate;
			}
		}
		return null;
	}

	public static Class getSystemFromResource(Resource r) {
		Iterator<EObject> iter = r.getAllContents();
		while (iter.hasNext()) {
			EObject candidate = iter.next();
			if (candidate instanceof Class && StereotypeUtil.isApplied((Class) candidate, System.class)) {
				return (Class) candidate;
			}
		}
		return null;
	}

	/**
	 * Collect all models with a given extension (compdef, ...) recursively from a
	 * container
	 * 
	 * @param container an Eclipse container
	 * @param extension an extension such as compdef
	 * @param list      the list to collect results
	 * @throws CoreException
	 */
	public static void processContainer(IContainer container, String extension, List<URI> list) throws CoreException {
		IResource[] members = container.members();

		for (IResource member : members) {
			if (member instanceof IContainer) {
				processContainer((IContainer) member, extension, list);
			} else if (member instanceof IFile) {
				if (matchesExtension(member.getName(), extension)) {
					IFile file = (IFile) member;
					URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
					list.add(uri);
				}
			}
		}
	}

	/*
	 * check whether the extension matches. An "|" operator is support to check for
	 * multiple extensions
	 */
	public static boolean matchesExtension(String str, String extension) {
		for (String ext : extension.split("\\|")) { //$NON-NLS-1$
			if (str.endsWith(ext)) {
				return true;
			}
		}
		return false;
	}
}
