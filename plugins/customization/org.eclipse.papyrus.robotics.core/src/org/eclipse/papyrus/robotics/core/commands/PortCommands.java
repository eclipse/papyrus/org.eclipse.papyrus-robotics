/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.commands;

import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.core.utils.PortCommandUtils;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.UMLPackage;

public class PortCommands {
	
	private static final String DEFAULT_PNAME = "Port"; //$NON-NLS-1$
	private static final String UPDATE_PROVIDED_REQUIRED = "Update provided/required"; //$NON-NLS-1$

	public static ICommand addProvided(Port port, Interface target) {
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(port);
		
		CompositeCommand cc = new CompositeCommand(UPDATE_PROVIDED_REQUIRED);
		PortCommandUtils.avoidDuplicatesCommand(port);
		PortCommandUtils.addMoveCSCommand(cc, port);
		if (port.getName() == null || port.getName().equals(DEFAULT_PNAME)) {
			IEditCommandRequest setName = new SetRequest(port, UMLPackage.eINSTANCE.getNamedElement_Name(), PortUtils.calcPortName(target));
			cc.add(provider.getEditCommand(setName));
		}

		PortCommandUtils.addDeleteDependenciesCommand(cc, port);
		// add interface
		IEditCommandRequest request = new CreateRelationshipRequest(port.getType(), port.getType(), target,
				ElementTypeRegistry.getInstance().getType("org.eclipse.papyrus.uml.InterfaceRealization")); //$NON-NLS-1$
		cc.add(provider.getEditCommand(request));
		// TODO: would be cleaner to apply stereotype on created relationships here, but it's not trivial since we cannot
		// apply a stereotype before the relationship.
		// new ApplyStereotypeRequest(element, StereotypeUtil.getStereoName(types).apply(dep, Realizes.class);
		return cc;
	}

	public static ICommand addRequired(Port port, Interface target) {
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(port);

		CompositeCommand cc = new CompositeCommand(UPDATE_PROVIDED_REQUIRED);
		PortCommandUtils.avoidDuplicatesCommand(port);
		PortCommandUtils.addMoveCSCommand(cc, port);
		PortCommandUtils.addDeleteDependenciesCommand(cc, port);
		if (port.getName() == null || port.getName().equals(DEFAULT_PNAME)) {
			IEditCommandRequest setName = new SetRequest(port, UMLPackage.eINSTANCE.getNamedElement_Name(), PortUtils.calcPortName(target));
			cc.add(provider.getEditCommand(setName));
		}

		// add interface
		IEditCommandRequest request = new CreateRelationshipRequest(port.getType().getNearestPackage(), port.getType(),
				target, ElementTypeRegistry.getInstance().getType("org.eclipse.papyrus.uml.Usage")); //$NON-NLS-1$
		cc.add(provider.getEditCommand(request));
	
		return cc;
	}
	
	public static ICommand removeProvReq(Port port) {
		CompositeCommand cc = new CompositeCommand(UPDATE_PROVIDED_REQUIRED);
		PortCommandUtils.addDeleteDependenciesCommand(cc, port);// Sort input values
		return cc;
	}
}
