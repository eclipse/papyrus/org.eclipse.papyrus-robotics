/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/


package org.eclipse.papyrus.robotics.core.provider;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.infra.emf.utils.HistoryUtil;
import org.eclipse.papyrus.infra.ui.emf.providers.EMFGraphicalContentProvider;

/**
 * A content provider filtering based on EClass. It provides history via it's superclass
 * EMFGraphicalContentProvider
 */
public class EClassGraphicalContentProvider extends EMFGraphicalContentProvider {

	/**
	 * History ID when only providing a meta-class
	 */
	private static final String HISTORY_MC_S = "history_mc_%s"; //$NON-NLS-1$

	/**
	 * Constructor.
	 *
	 * @param eObject
	 *            an eObject used to determine the resource
	 * @param metaClass
	 *            meta class
	 * @param feature
	 *            the feature, only used for history calculation
	 */
	public EClassGraphicalContentProvider(EObject eObject, EClass metaClass, EStructuralFeature feature) {
		super(new EClassContentProvider(eObject, metaClass), eObject.eResource().getResourceSet(),
				HistoryUtil.getHistoryID(eObject, feature));
	}

	/**
	 * 
	 * Constructor, history is only based on meta-class (and not meta-class + feature)
	 *
	 * @param eObject
	 *            an eObject used to determine the resource
	 * @param metaClass
	 *            meta class
	 */
	public EClassGraphicalContentProvider(EObject eObject, EClass metaClass) {
		super(new EClassContentProvider(eObject, metaClass), eObject.eResource().getResourceSet(),
				String.format(HISTORY_MC_S, metaClass.getName()));
	}
}
