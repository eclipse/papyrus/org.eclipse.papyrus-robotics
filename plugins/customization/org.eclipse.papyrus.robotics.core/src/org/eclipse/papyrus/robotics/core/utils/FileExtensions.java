/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;

/**
 * Manage file extensions and associated image paths
 */
public class FileExtensions {
	// component definition
	public static final String COMPDEF_UML = ".compdef.uml"; //$NON-NLS-1$

	// service definition
	public static final String SERVICEDEF_UML = ".servicedef.uml"; //$NON-NLS-1$

	// system definition (assembly)
	public static final String SYSTEM_UML = ".system.uml"; //$NON-NLS-1$

	// skills & behavior
	public static final String SKILLS_UML = ".skills.uml"; //$NON-NLS-1$

	// behavior-tree
	public static final String BT_UML = ".bt.uml"; //$NON-NLS-1$

	// task-based HARA
	public static final String THARA_UML = ".thara.uml"; //$NON-NLS-1$

	public static final String COMPDEF_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.profile/icons/ComponentDefinition.png"; //$NON-NLS-1$

	public static final String SYSTEM_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.diagrams/icons/Diagram_Assembly.png"; //$NON-NLS-1$

	public static final String SKILLS_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.diagrams/icons/Diagram_Skill.png"; //$NON-NLS-1$

	public static final String SERVICEDEF_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.profile/icons/ServiceDefinition.png"; //$NON-NLS-1$

	public static final String BT_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.bt.ui/images/subtree.png"; //$NON-NLS-1$

	public static final String THARA_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.safety.riskanalysis.ui/icons/hara.png"; //$NON-NLS-1$

	private static Map<String, String> extPath;

	static {
		extPath = new HashMap<String, String>();
		extPath.put(COMPDEF_UML, COMPDEF_ICON);
		extPath.put(SERVICEDEF_UML, SERVICEDEF_ICON);
		extPath.put(SYSTEM_UML, SYSTEM_ICON);
		extPath.put(SKILLS_UML, SKILLS_ICON);
		extPath.put(BT_UML, BT_ICON);
		extPath.put(THARA_UML, THARA_ICON);
	}

	public static String getExtension(URI path) {
		for (String extension : extPath.keySet()) {
			if (path.path().endsWith(extension)) {
				return extension;
			}
		}
		return null;
	}

	public static String getImagePath(URI path) {
		String extension = getExtension(path);
		if (extension != null) {
			return extPath.get(extension);
		}
		return null;
	}
}
