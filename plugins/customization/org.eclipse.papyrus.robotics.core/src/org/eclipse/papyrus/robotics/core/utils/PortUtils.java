/*****************************************************************************
 * Copyright (c) 2018-2020 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation
 *    Matteo Morelli     - Bug 565827
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.EncapsulatedClassifier;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Utility class around component ports
 */
public class PortUtils {

	/**
	 * Returns all ports (including inherited ones) for an encapsulated classifier
	 * It will also flatten extended ports
	 *
	 * @param ec
	 *            an encapsulated classifier
	 * @return the list of ports, including inherited ones
	 */
	public static EList<Port> getAllPorts(EncapsulatedClassifier ec) {
		EList<Port> ports = new BasicEList<Port>();
		for (Property attribute : ec.getAllAttributes()) {
			if (attribute instanceof Port) {
				ports.add((Port) attribute);
			}
		}
		return ports;
	}

	public static List<ComponentPort> getPortList(Class clazz) {
		List<ComponentPort> cpList = new ArrayList<ComponentPort>();
		for (Port port : clazz.getOwnedPorts()) {
			ComponentPort cp = UMLUtil.getStereotypeApplication(port, ComponentPort.class);
			if (cp != null) {
				cpList.add(cp);
			}
		}
		return cpList;
	}

	/**
	 * Create a new component service and type the passed port with that service
	 *
	 * @param port
	 *            a component port
	 * @return the creation/set command
	 */
	public static ICommand associateCSCommand(Port port) {
		// use a recoding command instead of ElementService, since the latter is not available (at least not for robotics domain)
		// before a Papyrus editor is open. Since this command is used by reversal tools, it might be called before a Papyrus
		// editor has been openend.
		RecordingCommand associateCS = new RecordingCommand(TransactionUtil.getEditingDomain(port), "Associate component-service command") { //$NON-NLS-1$

			@Override
			protected void doExecute() {
				Class clazz = port.getClass_();
				Class cs = (Class) clazz.createNestedClassifier(null, UMLPackage.eINSTANCE.getClass_());
				StereotypeUtil.apply(cs, ComponentService.class);
				port.setType(cs);
			}
		};
		return EMFtoGMFCommandWrapper.wrap(associateCS);
	}

	/**
	 * Set the part-with-port attribute of a connector end
	 *
	 * @param end
	 *            a connector end whose part-with-port should be set
	 * @return the set command
	 */
	public static ICommand setPartWithPortCommand(ConnectorEnd end) {
		final IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(end);
		if (end.getRole() instanceof Port) {
			Class activity = ((Port) end.getRole()).getClass_();
			Property activityInstance = getActivityInstance(activity);
			if (activityInstance != null) {
				SetRequest setPWPReq = new SetRequest(end, UMLPackage.eINSTANCE.getConnectorEnd_PartWithPort(), activityInstance);
				ICommand setPWPCmd = commandProvider.getEditCommand(setPWPReq);
				return setPWPCmd;
			}
		}
		return null;
	}

	/**
	 * Return an activity instance for a given activity
	 *
	 * @param activity
	 *            an activity
	 * @return the associated activity instance
	 */
	public static Property getActivityInstance(Class activity) {
		Class component = (Class) activity.getOwner();
		for (Property part : component.getOwnedAttributes()) {
			if (part.getType() == activity) {
				return part;
			}
		}
		return null;
	}

	/**
	 * Calculate a label for the port that indicates provided and required service
	 *
	 * @param port
	 *            a (RMS) port
	 * @return the port label
	 */
	public static String getPortLabel(Port port) {
		String label = port.getName();
		if (port.getProvideds().size() > 0) {
			label += " provides " + port.getProvideds().get(0).getName(); //$NON-NLS-1$
		}
		if (port.getRequireds().size() > 0) {
			label += " requires " + port.getRequireds().get(0).getName(); //$NON-NLS-1$
		}
		return label;
	}

	/**
	 * @param port
	 * @return if the typing component service is used by more than one port
	 */
	public static boolean isCSDuplicate(Port port) {
		Type type = port.getType();
		if (type != null && StereotypeUtil.isApplied(type, ComponentService.class)) {
			for (Setting setting : UMLUtil.getNonNavigableInverseReferences(type)) {
				EObject eObj = setting.getEObject();
				if (eObj instanceof Port && eObj != port) {
					// the CS is also the type of another port (e.g. due to copying the port)
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param port
	 * @return if the port is used as coordination port
	 */
	public static boolean isCoordinationPort(Port port) {
		ComponentPort cp = UMLUtil.getStereotypeApplication(port, ComponentPort.class);
		if (cp != null) {
			return cp.isCoordinationPort();
		}
		return false;
	}

	/**
	 * Calculate a name of a port from a given service description, by default
	 * use service name without prefix + lowerCase on first letter.
	 * 
	 * @param sd
	 *            a service description
	 * @return the default name after creation
	 */
	public static String calcPortName(Interface sd) {
		String sdName = InteractionUtils.getNameWoPrefix(sd);
		if (sdName != null && sdName.length() > 1) {
			return sdName.substring(0, 1).toLowerCase() + sdName.substring(1);
		}
		return sdName;
	}
}
