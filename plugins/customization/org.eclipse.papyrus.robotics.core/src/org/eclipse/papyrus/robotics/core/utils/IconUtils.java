/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.papyrus.commands.Activator;
import org.eclipse.swt.graphics.Image;

public class IconUtils {

	public static final String workspacePath = "platform:/plugin/org.eclipse.papyrus.robotics.diagrams/icons/toplevel/workspace.png"; //$NON-NLS-1$

	public static final String pluginsPath = "platform:/plugin/org.eclipse.papyrus.robotics.diagrams/icons/toplevel/plugins.png"; //$NON-NLS-1$

	public static final String modelPath = "platform:/plugin/org.eclipse.papyrus.robotics.diagrams/icons/toplevel/model.png"; //$NON-NLS-1$

	public static final URL getURL(String urlStr) {
		try {
			return new URL(urlStr);
		} catch (MalformedURLException e) {
			Activator.log.error(e);
		}
		return null;
	}

	/**
	 * Get the image for a given icon path in String form
	 * 
	 * @param iconPath
	 *            an icon path
	 * @return the image or null
	 */
	public static final Image getIcon(String iconPath) {
		return getIcon(getURL(iconPath));
	}

	/**
	 * Get the image for a given icon path given as a URL
	 * 
	 * @param iconURL
	 *            an icon URL
	 * @return the image or null
	 */
	public static final Image getIcon(URL iconURL) {
		return ExtendedImageRegistry.getInstance().getImage(iconURL);
	}
}
