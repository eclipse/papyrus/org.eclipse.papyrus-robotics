/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;

/**
 * Edit Helper Advice for realization link between an technical policy definition and an advice
 */
public class EntityEditHelperAdvice extends AbstractEditHelperAdvice {

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getBeforeConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest)
	 */
	@Override
	protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
		return super.getBeforeConfigureCommand(request);
	}


	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 * Add data-type.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Entity configuration command"); //$NON-NLS-1$
		EObject newElement = request.getElementToConfigure();
		if (newElement == null) {
			return super.getAfterConfigureCommand(request);
		}
		RecordingCommand addMetaData = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
			@Override
			protected void doExecute() {
				
				// code was placed here fore testing purposes, but has been moved into class generated
				// from profile (EntityImpl) instead, since the latter is executed for all sub-classes
				// of Entity whereas the advice helper is only executed for exact matches 
				/*
				// get applied stereotype. Assure that stereotype advice is executed before EntityEditHelperAdvice
				Entity entity = UMLUtil.getStereotypeApplication((Element) newElement,  Entity.class);
				
				IdMetaData metaData = BPCFactory.eINSTANCE.createIdMetaData();
				
				metaData.setInstance_uid("test");
				if (entity != null) {
					entity.setId(metaData);
				}
				*/
			}
		};
		
		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(addMetaData));
		
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}
	
	protected EObject source;
	protected EObject target;
}
