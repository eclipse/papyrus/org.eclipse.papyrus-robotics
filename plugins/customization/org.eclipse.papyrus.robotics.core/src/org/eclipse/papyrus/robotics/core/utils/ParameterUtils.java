/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;

/**
 * Utilities around parameter classes
 */
public class ParameterUtils {
	/**
	 * @param clazz a component definition
	 * @return all parameters of a component definition
	 */
	public static List<Property> getAllParameters(Class clazz) {
		List<Property> parameterList = new ArrayList<Property>();
		for (Class superClass : clazz.getSuperClasses()) {
			StructuredClassifier paramClass = ParameterUtils.getParameterClass(superClass);
			if (paramClass != null) {
				parameterList.addAll(paramClass.getOwnedAttributes());
			}
		}
		StructuredClassifier paramClass = ParameterUtils.getParameterClass(clazz);
		if (paramClass != null) {
			parameterList.addAll(paramClass.getOwnedAttributes());
		}
		return parameterList;
	}

	/**
	 * Return the nested classifier within a component that is holding configuration parameters
	 * Return null, if not found
	 */
	public static Class getParameterClass(Class component) {
		for (Classifier cl : component.getNestedClassifiers()) {
			if (cl instanceof Class && StereotypeUtil.isApplied(cl, Parameter.class)) {
				return (Class) cl;
			}
		}
		return null;
	}

	/**
	 * Return the part within a component that is typed with the passed nested classifier
	 */
	public static Property getParameterProperty(Class nestedClassifier) {
		Element owner = nestedClassifier.getOwner();
		if (owner instanceof Class) {
			for (Property p : ((Class) owner).getOwnedAttributes()) {
				if (p.getType() == nestedClassifier) {
					return p;
				}
			}
		}
		return null;
	}
}
