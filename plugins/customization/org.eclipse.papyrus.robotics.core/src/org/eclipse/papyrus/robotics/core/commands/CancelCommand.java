/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;

public class CancelCommand extends AbstractTransactionalCommand {
	/**
	 * Create a new cancel command
	 * Constructor.
	 *
	 * @param anElement
	 *            an element (only used to determine the editing domain)
	 */
	public CancelCommand(EObject anElement) {
		super(TransactionUtil.getEditingDomain(anElement), "cancel", null); //$NON-NLS-1$
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {
		return CommandResult.newCancelledCommandResult();
	}
}
