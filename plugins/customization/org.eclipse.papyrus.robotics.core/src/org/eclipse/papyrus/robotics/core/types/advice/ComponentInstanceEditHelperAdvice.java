/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper Advice for creating a new type and typing the port with this type
 */
public class ComponentInstanceEditHelperAdvice extends AbstractEditHelperAdvice {

	/**
	 * Create additional ComponentService and type port with that service.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Property)) {
			return super.getAfterConfigureCommand(request);
		}
		final Property part = (Property) newElement;

		EncapsulatedContentProvider cpWithWS = new RoboticsContentProvider(part,
				new UMLContentProvider(part, UMLPackage.eINSTANCE.getTypedElement_Type()),
				ComponentOrSystem.class, FileExtensions.COMPDEF_UML + "|" + FileExtensions.SYSTEM_UML); //$NON-NLS-1$

		EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(cpWithWS, "Choose component type", false);

		CompositeCommand compositeCommand = new CompositeCommand("Update component instance");
		if (popupMenuState.show(Display.getDefault().getActiveShell())) {
			Object menuResult = popupMenuState.getSubResult();
			final Classifier newType = menuResult instanceof Classifier ? (Classifier) menuResult : null;

			SetRequest setRequest = new SetRequest(part, UMLPackage.eINSTANCE.getTypedElement_Type(), newType);
			ICommand setTypeCmd = ElementEditServiceUtils.getCommandProvider(part).getEditCommand(setRequest);
			compositeCommand.add(setTypeCmd);
		}
		else {
			return new CancelCommand(newElement);
		}

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	@Override
	protected ICommand getAfterDuplicateCommand(DuplicateElementsRequest request) {
		return null;
	}

	protected EObject source;
	protected EObject target;
}
