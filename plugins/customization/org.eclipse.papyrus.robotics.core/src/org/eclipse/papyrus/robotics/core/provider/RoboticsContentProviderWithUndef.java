/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.provider;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;

/**
 * Variant of RoboticsContentProvider content provider that adds an explicit "undefined" field
 */
public class RoboticsContentProviderWithUndef extends RoboticsContentProvider implements IStaticContentProvider {

	public RoboticsContentProviderWithUndef(EObject context, IStaticContentProvider encapsulated, Class<? extends EObject> stereotypeFilter, String extension) {
		super(context, encapsulated, stereotypeFilter, extension);
	}

	public RoboticsContentProviderWithUndef(EObject context, IStaticContentProvider encapsulated, String extension) {
		super(context, encapsulated, extension);
	}
	
	@Override
	public Object[] getElements() {
		List<Object> results = new ArrayList<Object>();
		results.add(getUndef());
		for (Object element : super.getElements()) {
			results.add(element);
		}
		return results.toArray();
	}
	
	public static Type getUndef() {
		if (undef == null) {
			undef = UMLFactory.eINSTANCE.createClass();
			undef.setName("<undefined>"); //$NON-NLS-1$
		}
		return undef;
	}
	
	static Type undef = null;
}
