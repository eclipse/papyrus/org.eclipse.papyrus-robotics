/*****************************************************************************
 * Copyright (c) 2020, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from Ansgar Radermacher)
 *    Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581690
 *    Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - bug 581833
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.core.types.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.commands.DeferredCompositeCommand;
import org.eclipse.papyrus.robotics.core.commands.PortCommands;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu.SubSelect;
import org.eclipse.papyrus.robotics.core.provider.CoordinationServiceFilterProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper Advice for creating a new type based on coordination services and typing the port with this type
 */
public abstract class AbstractComponentCoordinationPortEditHelperAdvice extends AbstractEditHelperAdvice {

	private static final String IS_COORD_PORT_ATTRIBUTE = "isCoordinationPort"; //$NON-NLS-1$

	protected abstract String getInteractionPatternQualifName();

	protected abstract String getCoordinationServiceKind();

	/**
	 * Create additional ComponentService and type port with that (coordination) service.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// get element to configure and prepare the command
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Port)) {
			return super.getAfterConfigureCommand(request);
		}
		final Port port = (Port) newElement;
		CompositeCommand compositeCommand = new CompositeCommand("Update component coordination port"); //$NON-NLS-1$

		// Apply the ComponentPort stereotype to the new element and set its isCoordinationPort to true (different from default)
		RecordingCommand applyStereotypeCommand = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
			@Override
			protected void doExecute() {
				Stereotype cp = StereotypeUtil.apply(port, ComponentPort.class);
				port.setValue(cp, IS_COORD_PORT_ATTRIBUTE, true);
			}
		};
		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(applyStereotypeCommand));

		// Eventually create a ComponentService (port type)
		ICommand associateCSCommand = PortUtils.associateCSCommand(port);
		if (associateCSCommand != null) {
			compositeCommand.add(associateCSCommand);
		}

		// "Point" the CS to a coordination service (to be selected by the user)
		// Coordination ports in component definitions always PROVIDE the coordination interface (slave interface)
		// (the master interface plays the role of client (REQUIRE the coordination interface) -- the master interface
		// belongs to the coordinator component (sequencer))
		EncapsulatedContentProvider cpWithWS = new RoboticsContentProvider(
														port,
														new CoordinationServiceFilterProvider(
																new UMLContentProvider(port, UMLPackage.eINSTANCE.getPort_Provided()),
																getInteractionPatternQualifName()
															),
														CoordinationService.class,
														FileExtensions.SERVICEDEF_UML
													);
		SubSelect serviceDef = new EnhancedPopupMenu.SubSelect("Choose Coordination " + getCoordinationServiceKind() + " Service", cpWithWS); //$NON-NLS-1$
		Object value = serviceDef.getResult();

		// Create the port except when the user selects Cancel
		if (value instanceof Classifier) {
			final Classifier newType = (Classifier) value;
			SetRequest setNameRequest = new SetRequest(port, UMLPackage.eINSTANCE.getNamedElement_Name(), "Port"); //$NON-NLS-1$
			ICommand setNameCmd = ElementEditServiceUtils.getCommandProvider(port).getEditCommand(setNameRequest);
			compositeCommand.add(setNameCmd);

			DeferredCompositeCommand wrapper = new DeferredCompositeCommand() {

				@Override
				public ICommand createDeferredCommand() {
					return PortCommands.addProvided(port, (Interface) newType);
				}
			};
			compositeCommand.add(wrapper);
		}
		else {
			return new CancelCommand(port);
		}

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	/**
	 * Remove created CS and its relationships, if port is destroyed
	 */
	@Override
	protected ICommand getAfterDestroyDependentsCommand(DestroyDependentsRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Port destruction command"); //$NON-NLS-1$
		EObject destroyElement = request.getElementToDestroy();
		if (!(destroyElement instanceof Port)) {
			return super.getAfterDestroyDependentsCommand(request);
		}
		final Port port = (Port) destroyElement;
		Type cs = port.getType();

		final IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(port);

		if (cs != null && StereotypeUtil.isApplied(cs, ComponentService.class) && !PortUtils.isCSDuplicate(port)) {
			// destroy relationships (usage, interface realization)
			for (Dependency dep : cs.getClientDependencies()) {
				DestroyElementRequest destroyDepReq = new DestroyElementRequest(dep, false);
				ICommand destroyDepCmd = commandProvider.getEditCommand(destroyDepReq);
				compositeCommand.add(destroyDepCmd);
			}

			DestroyElementRequest destroyCSReq = new DestroyElementRequest(cs, false);
			ICommand destroyCSCmd = commandProvider.getEditCommand(destroyCSReq);
			compositeCommand.add(destroyCSCmd);
		}

		return compositeCommand.isEmpty() ? super.getAfterDestroyDependentsCommand(request) : compositeCommand;
	}

	@Override
	protected ICommand getAfterDuplicateCommand(DuplicateElementsRequest request) {
		return null;
	}

	protected EObject source;
	protected EObject target;
}
