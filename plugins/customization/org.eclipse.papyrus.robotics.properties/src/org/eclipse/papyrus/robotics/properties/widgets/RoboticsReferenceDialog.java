/*****************************************************************************
 * Copyright (c) 2019, 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsLabelProvider;
import org.eclipse.papyrus.robotics.core.utils.ImportUtils;
import org.eclipse.papyrus.robotics.core.utils.ScanUtils;
import org.eclipse.swt.widgets.Composite;

/**
 * Robotics reference dialog. Supposed to be overridden by sub-classes
 * that define the extension and stereotype to look for.
 * Attention wrt. naming: this is rather an editor, not a dialog referenced from XWT.
 */
public class RoboticsReferenceDialog extends ReferenceDialog {

	protected Class<? extends EObject> stereotypeFilter;

	protected String extension;

	protected EObject context;

	/**
	 * 
	 * Constructor.
	 *
	 * @param parent
	 *            the graphical parent composite
	 * @param style
	 *            the SWT style
	 * @param stereotypeFilter
	 *            an optional stereotype filter (maybe null)
	 * @param extension
	 *            the file extension to scan for
	 */
	public RoboticsReferenceDialog(Composite parent, int style, Class<? extends EObject> stereotypeFilter, String extension) {
		super(parent, style);
		this.stereotypeFilter = stereotypeFilter;
		this.extension = extension;
	}

	@Override
	public void setModelObservable(@SuppressWarnings("rawtypes") IObservableValue modelProperty) {
		super.setModelObservable(modelProperty);
		// get context (extracted from model Property, i.e. not known before)
		Object contextObj = getContextElement();
		if (contextObj instanceof EObject) {
			context = (EObject) contextObj;
			// create robotics content provider
			EncapsulatedContentProvider roboticsProvider;
			if (stereotypeFilter != null) {
				roboticsProvider = new RoboticsContentProvider(
						context, contentProvider, stereotypeFilter, extension);
			} else {
				roboticsProvider = new RoboticsContentProvider(
						context, contentProvider, extension);
			}
			setContentProvider(roboticsProvider);
		}
	}

	@Override
	public void setValue(Object valueObj) {
		if (valueObj instanceof EObject) {
			EObject value = (EObject) valueObj;
			valueObj = ScanUtils.moveIntoRS(context.eResource().getResourceSet(), value);
			ImportUtils.createImportFromObj(valueObj);
		}
		super.setValue(valueObj);
	}

	@Override
	public void setLabelProvider(final ILabelProvider provider) {
		super.setLabelProvider(new RoboticsLabelProvider(provider));
	}
}
