/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.papyrus.infra.properties.ui.widgets.MultiReference;
import org.eclipse.papyrus.robotics.core.provider.RoboticsLabelProvider;
import org.eclipse.swt.widgets.Composite;

/**
 * Variant of multi reference, using the Robotics Label provider
 */
public class RobMultiReference extends MultiReference {

	public RobMultiReference(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected void doBinding() {
		super.doBinding();
		editor.setLabelProvider(new RoboticsLabelProvider(input.getLabelProvider(propertyPath)));
	}
 }
