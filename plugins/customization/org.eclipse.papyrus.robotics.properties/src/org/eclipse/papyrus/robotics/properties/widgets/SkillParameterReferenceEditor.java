/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Made Up/Down buttons invisible
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.papyrus.infra.widgets.editors.MultipleReferenceEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;

/**
 * Editor for skill parameters, performing specific filtering
 */
public class SkillParameterReferenceEditor extends MultipleReferenceEditor {

	ParameterDirectionKind direction;

	/**
	 * Constructor.
	 *
	 * @param parent a parent composite
	 * @param style
	 */
	public SkillParameterReferenceEditor(Composite parent, int style, ParameterDirectionKind direction) {
		super(parent, style);
		this.direction = direction;
	}

	@Override
	public void updateButtons() {
		super.updateButtons();
		up.setVisible(false);
		down.setVisible(false);
	}

	@Override
	protected void setInput(@SuppressWarnings("rawtypes") IObservableList modelProperty) {
		super.setInput(modelProperty);
		treeViewer.setFilters(new ViewerFilter() {
			
			public boolean select(Viewer viewer, Object observable, Object data) {
				Parameter parameter = (Parameter) data;
				return parameter.getDirection() == direction;
			}
		});
	}
}
