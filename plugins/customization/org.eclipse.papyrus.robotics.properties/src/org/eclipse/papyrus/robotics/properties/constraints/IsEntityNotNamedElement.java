/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.properties.constraints;

import org.eclipse.papyrus.infra.constraints.constraints.AbstractConstraint;
import org.eclipse.papyrus.infra.constraints.constraints.Constraint;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

/**
 * A Constraint to test if the given object is an entity but not a named element
 */
public class IsEntityNotNamedElement extends AbstractConstraint {

	@Override
	public boolean match(Object selection) {
		Element umlSemantic = UMLUtil.resolveUMLElement(selection);
		if (umlSemantic == null || umlSemantic instanceof NamedElement) {
			return false;
		}
		return StereotypeUtil.isApplied(umlSemantic, Entity.class);
	}

	@Override
	protected boolean equivalent(Constraint constraint) {
		return (constraint instanceof IsEntityNotNamedElement);
	}
}
