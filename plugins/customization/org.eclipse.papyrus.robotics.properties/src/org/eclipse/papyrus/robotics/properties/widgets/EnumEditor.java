/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.papyrus.infra.properties.ui.modelelement.ModelElement;
import org.eclipse.papyrus.robotics.xtext.util.TransactionalXtextEditor;
import org.eclipse.papyrus.uml.properties.xtext.widget.property.AbstractXtextPropertyEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * A Property editor for DAtaTypes
 * 
 * @see AbstractXtextPropertyEditor
 */
public class EnumEditor extends TransactionalXtextEditor {

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            The composite in which the widget is created
	 * @param style
	 *            The style of the editor
	 */
	public EnumEditor(Composite parent, int style) {
		super("Enum editor", parent, style);	//$NON-NLS-1$
		// use all available size
		LayoutUtil.fillVSpace(parent);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(xtextEditor.getTextControl());
	}

	@SuppressWarnings("nls")
	@Override
	protected void registerChangeListeners(ModelElement element) {
		registerObservable(element, "name");
		registerObservable(element, "ownedAttribute");
	}
}
