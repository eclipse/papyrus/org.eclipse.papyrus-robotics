/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by implementation of ProvidedInterfaceObservableList)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.ui.databinding.PapyrusObservableList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * An IObservableList to edit the UML derived feature, in/out/res parameters.
 */
public class FunctionObservable extends PapyrusObservableList {

	/**
	 * Instantiates a new provided interface observable list.
	 *
	 * @param activity
	 *            the operation
	 * @param domain
	 *            the domain
	 */
	public FunctionObservable(Class activity, EditingDomain domain) {
		super(activity.getOwnedAttributes(), domain, activity, UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute(),
				GMFtoEMFCommandWrapper::wrap);
	}
	
}
