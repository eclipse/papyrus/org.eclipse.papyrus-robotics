/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by UML/RT implementation)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.EClassGraphicalContentProvider;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElement;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * A ModelElement provider for robotic stereotypes, currently used for ComponentPort only
 */
public class RoboticsStereotypeME extends StereotypeModelElement {

	final static String PROVIDES = "provides";  //$NON-NLS-1$

	final static String REQUIRES = "requires";  //$NON-NLS-1$
	
	/**
	 * Constructor.
	 */
	public RoboticsStereotypeME(EObject stereotypeApplication, Stereotype stereotype) {
		this(stereotypeApplication, stereotype, TransactionUtil.getEditingDomain(stereotypeApplication));
	}

	/**
	 * Constructor.
	 */
	public RoboticsStereotypeME(EObject stereotypeApplication, Stereotype stereotype, EditingDomain domain) {
		super(stereotypeApplication, stereotype, domain);
	}

	public void updateSource(EObject source) {
		this.source = source;
	}

	@Override
	public IStaticContentProvider getContentProvider(String propertyPath) {
		if (propertyPath.equals(PROVIDES) || (propertyPath.equals(REQUIRES))) {
			// TODO: not clear, why it is necessary to return a simpler
			// content provider
			EStructuralFeature feature = getFeature(propertyPath);
			return new EClassGraphicalContentProvider(source, UMLPackage.eINSTANCE.getInterface(), feature);
		}
		return super.getContentProvider(propertyPath);
	}

	@Override
	public IObservable doGetObservable(String propertyPath) {
		IObservable observable = null;

		if (source instanceof ComponentPort) {
			if (propertyPath.equals(PROVIDES)) {
				Port port = ((ComponentPort) source).getBase_Port();
				if (port.getType() == null || !StereotypeUtil.isApplied(port.getType(), ComponentService.class)) {
					boolean assign = MessageDialog.openConfirm(Display.getCurrent().getActiveShell(),
							"Create component service?", String.format(
									"In RobMoSys compliant models, ports are typed with a component service. Should the current type (%s) be replaced with a component service?",
									port.getType() != null ? port.getType().getName() : "unset"));
					if (assign) {
						org.eclipse.gmf.runtime.common.core.command.ICommand assocCmd = PortUtils.associateCSCommand(port);
						TransactionUtil.getEditingDomain(port).getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(assocCmd));
					}
				}
				observable = new ProvidedInterfaceObservable(port, domain);
			} else if (propertyPath.equals(REQUIRES)) {
				Port port = ((ComponentPort) source).getBase_Port();
				observable = new RequiredInterfaceObservable(port, domain);
			} else {
				observable = super.doGetObservable(propertyPath);
			}
		}
		else {
			observable = super.doGetObservable(propertyPath);
		}

		return observable;
	}

	@Override
	protected boolean isFeatureEditable(String propertyPath) {
		if (source instanceof ComponentPort) {
			return true;
		}
		return super.isFeatureEditable(propertyPath);
	}


	@Override
	public boolean getDirectCreation(final String propertyPath) {
		return super.getDirectCreation(propertyPath);
	}
}
