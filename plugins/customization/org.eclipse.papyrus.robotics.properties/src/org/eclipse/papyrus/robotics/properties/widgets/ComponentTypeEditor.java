/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.swt.widgets.Composite;

/**
 * A dialog that filters available selections: only ComponentDefinitions are shown
 */
public class ComponentTypeEditor extends RoboticsReferenceDialog {

	/**
	 * 
	 * Constructor.
	 *
	 * @param parent
	 *            Parent Composite owning this Dialog
	 * @param style
	 *            The Style of the Composite
	 */
	public ComponentTypeEditor(final Composite parent, final int style) {
		super(parent, style, ComponentDefinition.class, FileExtensions.COMPDEF_UML);
	}
}
