/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.infra.widgets.selectors.ReferenceSelector;
import org.eclipse.swt.widgets.Composite;

/**
 * A variant of the standard reference selector that takes care of using the passed
 * robotics content provider in the treeViewer of the selection 
 */
public class RoboticsElementSelector extends ReferenceSelector {

	IStaticContentProvider roboticsCP;

	public void setProviders(IStaticContentProvider roboticsCP, ILabelProvider roboticsLP) {

		this.roboticsCP = roboticsCP;
		setContentProvider(roboticsCP);
		labelProvider = roboticsLP;
	}


	@Override
	public void createControls(Composite parent) {
		super.createControls(parent);
		// overwrite tree viewer content provider (the superclass always creates an
		// EncapsulatedContent provider, when calling setContentProvider)
		treeViewer.setContentProvider(roboticsCP);
	}
}
