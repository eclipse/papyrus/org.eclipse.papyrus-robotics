/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.swt.widgets.Composite;

/**
 * Papyrus4robotics currently uses template bindings are exclusively for binding
 * communication objects. This dialog filters accordingly.
 */
public class TPSActualReferenceDialog extends ReferenceWoAddDialog {

	public TPSActualReferenceDialog(Composite parent, int style) {
		super(parent, style);
	}
	
	@Override
	protected org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog createReferenceDialog(Composite parent, int style) {
		return new TPSActualReferenceEditor(parent, style);
	}
	
	@Override
    protected void doBinding() {
		super.doBinding();
		// editor.setValueFactory(null);
    }
}
