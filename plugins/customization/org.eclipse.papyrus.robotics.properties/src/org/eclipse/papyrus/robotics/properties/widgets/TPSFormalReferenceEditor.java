/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import java.util.Collections;

import org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.FilterContainment;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;

/**
 * restrict formal choice to elements in signature
 */
public class TPSFormalReferenceEditor extends ReferenceDialog {

	FilterContainment providerForDialog;
	
	public TPSFormalReferenceEditor(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog#setContentProvider(org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider)
	 */
	@Override
	public void setContentProvider(final IStaticContentProvider provider) {
		providerForDialog = new FilterContainment(provider);
		dialog.setContentProvider(providerForDialog);
		if (null != getValue()) {
			setInitialSelection(Collections.singletonList(getValue()));
		}
		this.contentProvider = provider;
	}
	
	public void updateContainment() {
		// Limit available choices to Communication objects
		final Object context = getContextElement();
		if (context instanceof TemplateParameterSubstitution) {
			TemplateParameterSubstitution tps = (TemplateParameterSubstitution) context;
			if (tps.getOwner() instanceof TemplateBinding) {
				TemplateBinding binding = (TemplateBinding) tps.getOwner();
				providerForDialog.setParent(binding.getSignature());		
			}
		}
	}
}
