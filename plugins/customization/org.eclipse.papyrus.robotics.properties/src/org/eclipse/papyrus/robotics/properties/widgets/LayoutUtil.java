/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.papyrus.infra.widgets.editors.EditorParentComposite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

public class LayoutUtil {

	/**
	 * Use all available vertical space by changing a layout option
	 * in the parent
	 */
	public static void fillVSpace(Composite parent) {
		GridDataFactory.fillDefaults().grab(true, true).applyTo(parent);
		while (parent != null) {
			if (parent instanceof EditorParentComposite) {
				// fix grid layout data, which does not use the available vertical space
				Object layoutData = parent.getParent().getLayoutData();
				if (layoutData instanceof GridData) {
					GridData gd = (GridData) layoutData;
					gd.grabExcessVerticalSpace = true;
					gd.verticalAlignment = SWT.FILL;
				}
				break;
			}
			parent = parent.getParent();
		}
	}
}