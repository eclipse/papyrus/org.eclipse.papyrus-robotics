/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.widgets.editors.AbstractEditor;
import org.eclipse.papyrus.infra.widgets.editors.ICommitListener;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.generics.Realizes;
import org.eclipse.papyrus.robotics.profile.robotics.generics.Uses;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Usage;

/**
 * Editor for selecting the service definitions that are provided or required by a port
 * It assures that
 * 1. Only ServiceDefinitions can be selected
 * 2. RobMoSys stereotypes are applied on relationships
 */
public class PortReferenceEditor extends RoboticsReferenceDialog {

	ICommitListener applyStereoListener;

	protected EObject context;

	/**
	 * Constructor.
	 *
	 * @param parent a parent composite
	 * @param style
	 */
	public PortReferenceEditor(Composite parent, int style) {
		super(parent, style, ServiceDefinition.class, FileExtensions.SERVICEDEF_UML);

		applyStereoListener = new ICommitListener() {

			@Override
			public void commit(AbstractEditor editor) {
				final Object context = getContextElement();

				if (context instanceof Port) {
					Port port = (Port) context;
					if (port.getType() != null) {
						// assure that RobMoSys stereotypes get applied
						for (Dependency dep : port.getType().getClientDependencies()) {
							if (dep instanceof InterfaceRealization) {
								StereotypeUtil.apply(dep, Realizes.class);
							} else if (dep instanceof Usage) {
								StereotypeUtil.apply(dep, Uses.class);
							}
						}
					}
				}
			}
		};
	}
}
