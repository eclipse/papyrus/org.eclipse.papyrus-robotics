/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by implementation of ProvidedInterfaceObservableList)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.ui.databinding.PapyrusObservableValue;
import org.eclipse.papyrus.robotics.core.commands.PortCommands;
import org.eclipse.papyrus.uml.tools.adapters.PortTypeAdapter;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * An IObservableList to edit the UML Derived feature {@link Port#getProvideds()}.
 */
public class ProvidedInterfaceObservable extends PapyrusObservableValue {

	/** The port. */
	protected Port port = null;

	/** Adapter for port type. */
	private Adapter portTypeAdapter = null;

	/**
	 * Instantiates a new provided interface observable list.
	 *
	 * @param source
	 *            the source
	 * @param domain
	 *            the domain
	 */
	public ProvidedInterfaceObservable(Port source, EditingDomain domain) {
		super(source, UMLPackage.eINSTANCE.getPort_Provided(), domain, null);
		this.port = source;
		port.eAdapters().add(portTypeAdapter = new PortTypeAdapter(port, UMLPackage.eINSTANCE.getPort_Provided(), UMLPackage.Literals.BEHAVIORED_CLASSIFIER__INTERFACE_REALIZATION));
	}

	@Override
	public Object doGetValue() {
		if (port.getProvideds().size() > 0) {
			return port.getProvideds().get(0);
		}
		return null;
	}
	
	/**
	 * Return the set command (that will set or remove a provided interface).
	 *
	 * @param value
	 *            the new value
	 * @return the set command
	 */
	@Override
	public Command getCommand(Object value) {
		if (value instanceof Interface) {
			return new GMFtoEMFCommandWrapper(PortCommands.addProvided(port, (Interface) value));
		}
		else {
			return new GMFtoEMFCommandWrapper(PortCommands.removeProvReq(port));
		}
	}

	/**
	 * @see org.eclipse.papyrus.infra.ui.emf.databinding.EMFObservableList#dispose()
	 *
	 */
	@Override
	public synchronized void dispose() {
		port.eAdapters().remove(portTypeAdapter);
		super.dispose();
	}
}
