/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.papyrus.infra.gmfdiag.common.databinding.GMFObservableValue;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * An observable that "forwards" to the operational state and observes the skill
 * compInterface attribute
 */
public class SkillSemanticObservableCS extends GMFObservableValue {

	public SkillSemanticObservableCS(SkillDefinition sd, EditingDomain domain) {
		super(getOperationalState(sd),
				SkillsPackage.eINSTANCE.getSkillOperationalState_CompInterface(), domain);
	}

	public static EObject getOperationalState(SkillDefinition sd) {
		SkillSemantic semantic = sd.getDefaultSemantic();
		if (semantic == null || semantic.getBase_StateMachine() == null) {
			return sd;
		}
		StateMachine sm = semantic.getBase_StateMachine();
		if (sm.getRegions().size() > 0) {
			Region r = sm.getRegions().get(0);
			for (Vertex state : r.getSubvertices()) {
				SkillOperationalState sos = UMLUtil.getStereotypeApplication(state, SkillOperationalState.class);
				if (sos != null) {
					return sos;
				}
			}
		}
		return sd;
	}
}
