/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from Ansgar Radermacher) - Bug #581690
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.swt.widgets.Composite;

public class CoordinationServiceReference extends ReferenceWoAddDialog {

	CoordinationServiceReferenceEditor csEditor;

	public CoordinationServiceReference(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog createReferenceDialog(Composite parent, int style) {
		csEditor = new CoordinationServiceReferenceEditor(parent, style);
		return csEditor;
	}
}
