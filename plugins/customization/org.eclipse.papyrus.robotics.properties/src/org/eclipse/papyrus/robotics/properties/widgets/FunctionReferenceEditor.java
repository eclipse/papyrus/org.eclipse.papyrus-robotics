/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.papyrus.infra.widgets.editors.MultipleReferenceEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Port;

/**
 * Editor for function (properties), i.e. filtering (Activity-) ports
 */
public class FunctionReferenceEditor extends MultipleReferenceEditor {

	ParameterDirectionKind direction;

	/**
	 * Constructor.
	 *
	 * @param parent a parent composite
	 * @param style
	 */
	public FunctionReferenceEditor(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected void setInput(@SuppressWarnings("rawtypes") IObservableList modelProperty) {
		super.setInput(modelProperty);
		treeViewer.setFilters(new ViewerFilter() {
			
			public boolean select(Viewer viewer, Object observable, Object data) {
				return !(data instanceof Port);
			}
		});
	}
}
