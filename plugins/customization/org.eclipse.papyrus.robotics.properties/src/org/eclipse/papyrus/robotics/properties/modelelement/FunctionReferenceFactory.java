/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.core.commands.CreateFunctionCommand;
import org.eclipse.papyrus.uml.properties.creation.UMLPropertyEditorFactory;
import org.eclipse.swt.widgets.Control;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;

public class FunctionReferenceFactory extends UMLPropertyEditorFactory {

	protected Class activity;

	public FunctionReferenceFactory(Class activity, EReference reference) {
		super(reference);
		this.activity = activity;
	}

	@Override
	protected EObject simpleCreateObject(Control widget) {
		this.className = "Function";
		CreateFunctionCommand cmd = new CreateFunctionCommand(activity);
		cmd.execute();

		return cmd.getFunctionAttr();
	}
	
	@Override
	protected Object doCreateObject(Control widget, Object context) {
		Object instance;

		if (referenceIn.isContainment()) {
			instance = simpleCreateObject(widget);
		} else {
			instance = createObjectInDifferentContainer(widget);
		}
		// unlike in superclass, do not call createObject which would open a dialog
		// to edit the property. But we want to edit rather the function.
		return instance;
	}
	
	@Override
	public Object edit(Control widget, Object source) {
		if (source instanceof Property) {
			return super.edit(widget, ((Property) source).getType());
		}
		return super.edit(widget,  source);
	}
}
