/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.papyrus.infra.widgets.editors.MultipleReferenceEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.TemplateBinding;

/**
 * Overload editor to switch from template binding to parameter substitutions, when invoked
 * on a service-definition (while it is possible to use nested property path, such as
 * templateBinding.parameterSubstitution, the latter does work, since templateBinding returns a list)
 * TODO: better use a custom model element factory, as in UML/RT
 */
public class TemplateBindingEditor extends MultipleReferenceEditor {

	public TemplateBindingEditor(Composite parent, int style) {
		super(parent, style);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void setModelObservable(IObservableList modelProperty) {
		super.setModelObservable(modelProperty);
		if (switchProperty()) {
			modelProperty.addChangeListener(new IChangeListener() {
				
				@Override
				public void handleChange(ChangeEvent arg0) {
					switchProperty();
				}
			});
		}
	}
	
	@SuppressWarnings("unchecked")
	protected boolean switchProperty() {
		if (modelProperty.size() > 0 && modelProperty.get(0) instanceof TemplateBinding) {
			TemplateBinding tb = (TemplateBinding) modelProperty.get(0);
			// switch observable from template binding to parameter substitutions
			// (enable direct use from ServiceDefinitions)
			modelProperty.remove(tb);
			modelProperty.addAll(tb.getParameterSubstitutions());
			return true;
		}
		return false;
	}
}
