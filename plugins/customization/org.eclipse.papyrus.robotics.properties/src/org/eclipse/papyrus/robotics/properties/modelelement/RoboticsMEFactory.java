/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by UML/RT implementation)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.properties.contexts.DataContextElement;
import org.eclipse.papyrus.infra.properties.ui.modelelement.EMFModelElement;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.properties.Activator;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElementFactory;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

public class RoboticsMEFactory extends StereotypeModelElementFactory {

	@Override
	protected EMFModelElement doCreateFromSource(Object sourceElement, DataContextElement context) {
		EObject source = EMFHelper.getEObject(sourceElement);
		EMFModelElement modelElement = null;
		if (source != null) {
			if (source instanceof Port && StereotypeUtil.isApplied((Port) source, ComponentPort.class)) {
				Port port = (Port) source;
				Stereotype st = port.getAppliedStereotypes().get(0);
				ComponentPort compPort = UMLUtil.getStereotypeApplication(port, ComponentPort.class);
				// use stereotype ME, otherwise the QoS would not be detected.
				modelElement = new RoboticsStereotypeME(compPort, st);
			}
			else if (source instanceof Operation && StereotypeUtil.isApplied((Operation) source, SkillDefinition.class)) {
				modelElement = new RoboticsME(source);
			}
			else if (source instanceof Class && StereotypeUtil.isApplied((Class) source, Activity.class)) {
				modelElement = new RoboticsME(source);
			}
			else {
				modelElement = super.doCreateFromSource(sourceElement, context);
			}
		} else {
			Activator.log.warn("Unable to resolve the selected element to an EObject"); //$NON-NLS-1$
		}

		return modelElement;
	}

	protected void updateModelElement(EMFModelElement modelElement, Object newSourceElement) {
		if (modelElement instanceof RoboticsME) {
			EObject source = EMFHelper.getEObject(newSourceElement);
			((RoboticsME) modelElement).updateSource(source);
		}
		else {
			super.updateModelElement(modelElement, newSourceElement);
		}
	}
}
