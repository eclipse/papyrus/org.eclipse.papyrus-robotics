/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.uml.properties.creation.UMLPropertyEditorFactory;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Control;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;

public class SkillReferenceFactory extends UMLPropertyEditorFactory {

	protected ParameterDirectionKind direction;

	Operation operation;

	public SkillReferenceFactory(Operation operation, EReference reference, ParameterDirectionKind direction) {
		super(reference);
		this.operation = operation;
		this.direction = direction;
	}

	@Override
	protected EObject simpleCreateObject(Control widget) {
		// passing by an element type CreateElementRequest does not work
		// for some reason
		Parameter parameter = (Parameter) super.simpleCreateObject(widget);
		parameter.setDirection(direction);
		RecordingCommand cmd = new RecordingCommand(TransactionUtil.getEditingDomain(operation), "Create skill parameter") {

			@Override
			protected void doExecute() {
				// add to operation and apply stereotype
				operation.getOwnedParameters().add(parameter);
				if (direction == ParameterDirectionKind.IN_LITERAL) {
					StereotypeUtil.apply(parameter, InAttribute.class);
				} else if (direction == ParameterDirectionKind.OUT_LITERAL) {
					StereotypeUtil.apply(parameter, OutAttribute.class);
				} else {
					StereotypeUtil.apply(parameter, SkillResult.class);
				}
			}
		};
		cmd.execute();
		return parameter;
	}
}
