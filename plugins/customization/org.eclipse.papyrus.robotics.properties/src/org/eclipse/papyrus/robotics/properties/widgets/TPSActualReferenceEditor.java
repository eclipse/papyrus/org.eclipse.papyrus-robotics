/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import java.util.Collections;

import org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.FilterStereotypes;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.swt.widgets.Composite;

/**
 * Papyrus4robotics currently uses template bindings are exclusively for binding
 * communication objects. This dialog filters accordingly.
 */
public class TPSActualReferenceEditor extends ReferenceDialog {

	public TPSActualReferenceEditor(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog#setContentProvider(org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider)
	 */
	@Override
	public void setContentProvider(final IStaticContentProvider provider) {
		// Limit available choices to Communication objects
		EncapsulatedContentProvider providerForDialog = new FilterStereotypes(provider, CommunicationObject.class);
		dialog.setContentProvider(providerForDialog);
		if (null != getValue()) {
			setInitialSelection(Collections.singletonList(getValue()));
		}
		this.contentProvider = provider;
	}
}
