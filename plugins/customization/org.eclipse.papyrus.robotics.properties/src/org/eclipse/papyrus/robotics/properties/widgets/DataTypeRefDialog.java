/*****************************************************************************
 * Copyright (c) 2021, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *  Ansgar Radermacher - Bug 581428
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.swt.widgets.Composite;

/**
 * A dialog for datatype attributes, currently shows all available types (see bug 581428)
 */
public class DataTypeRefDialog extends ReferenceWoAddDialog {

	public DataTypeRefDialog(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog createReferenceDialog(Composite parent, int style) {
		return new RoboticsReferenceDialog(parent, style, null, FileExtensions.SERVICEDEF_UML);
	}
}
