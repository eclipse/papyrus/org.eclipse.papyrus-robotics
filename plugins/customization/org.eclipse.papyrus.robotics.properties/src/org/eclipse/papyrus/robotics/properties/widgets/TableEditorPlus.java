package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.nebula.widgets.nattable.grid.layer.GridLayer;
import org.eclipse.nebula.widgets.nattable.layer.ILayer;
import org.eclipse.nebula.widgets.nattable.selection.SelectionLayer;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.layerstack.BodyLayerStack;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.widgets.Activator;
import org.eclipse.papyrus.infra.widgets.messages.Messages;
import org.eclipse.papyrus.uml.properties.widgets.NattablePropertyEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

abstract public class TableEditorPlus extends NattablePropertyEditor implements SelectionListener {

	public static String ICON_PATH = "platform:/plugin/org.eclipse.papyrus.infra.widgets/icons/";

	protected Button add;

	protected Button remove;

	protected Composite controlsSection;

	protected EObject context;

	public TableEditorPlus(Composite parent, int style) {
		 super(parent, style);

		// use all available size
		LayoutUtil.fillVSpace(parent);
	}

	/**
	 * This allow to create the widgets displayed before the table widget.
	 */
	@Override
	protected void createPreviousWidgets(final EObject sourceElement, final EStructuralFeature feature) {
		// change to grid layout and store context
		self.setLayout(new GridLayout(2, false));
		context = sourceElement;
	}

	/**
	 * This allow to create the widgets displayed after the table widget.
	 */
	@Override
	protected void createFollowingWidgets(final EObject sourceElement, final EStructuralFeature feature) {
		controlsSection = new Composite(self, SWT.NONE);

		controlsSection.setLayout(new FillLayout());
		controlsSection.setLayoutData(new GridData(SWT.END, SWT.TOP, false, false));
		createListControls();

		// assure sufficient space for initially small tables
		Control tableWidget = self.getChildren()[0];
		GridDataFactory gdf = GridDataFactory.fillDefaults().grab(true, true);
		gdf.applyTo(tableWidget);
	}

	/**
	 * Creates the Add/Remove controls,
	 * and the Up/Down controls if the collection is ordered
	 *
	 * @param ordered
	 */
	protected void createListControls() {
		add = createButton(Activator.getDefault().getImage(ICON_PATH + "Add_12x12.gif"), Messages.MultipleValueEditor_AddElements); //$NON-NLS-1$
		remove = createButton(Activator.getDefault().getImage(ICON_PATH + "Delete_12x12.gif"), Messages.MultipleValueEditor_RemoveSelectedElements); //$NON-NLS-1$
	}

	protected Button createButton(Image image, String toolTipText) {
		Button button = new Button(controlsSection, SWT.PUSH);
		button.setImage(image);
		button.addSelectionListener(this);
		button.setToolTipText(toolTipText);
		return button;
	}

	public abstract AbstractEditCommandRequest createElementRequest();

	public abstract AbstractEditCommandRequest removeElementRequest(Object object);

	@Override
	public void widgetSelected(SelectionEvent e) {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(context);

		if (e.getSource() == add) {
			ICommand cmd = ElementEditServiceUtils.getCommandProvider(context).getEditCommand(
					createElementRequest());
			if (cmd != null) {
				domain.getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(cmd));
			}
		} else if (e.getSource() == remove) {
			// obtain selected row
			ILayer gridLayer = natTableWidget.getLayer();
			ILayer bodyLayer = ((GridLayer) gridLayer).getBodyLayer();
			SelectionLayer sl = ((BodyLayerStack) bodyLayer).getSelectionLayer();
			int row = sl.getSelectionAnchor().rowPosition;
			if (row >= 0) {
				Object object = nattableManager.getRowElement(row);
				ICommand cmd = ElementEditServiceUtils.getCommandProvider(context).getEditCommand(
						removeElementRequest(object));
				if (cmd != null) {
					domain.getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(cmd));
				}
			}
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
	}
}
