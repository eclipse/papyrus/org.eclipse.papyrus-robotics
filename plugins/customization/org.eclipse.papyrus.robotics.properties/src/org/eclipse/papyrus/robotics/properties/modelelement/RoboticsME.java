/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by UML/RT implementation)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.modelelement;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.infra.widgets.creation.ReferenceValueFactory;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.FilterStereotypes;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.papyrus.robotics.profile.robotics.skills.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.papyrus.uml.properties.modelelement.UMLModelElement;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * A ModelElement provider for robotics model elements
 */
public class RoboticsME extends UMLModelElement {

	final static String QOS = "qos"; //$NON-NLS-1$

	final static String COORDINATION_SVC = "coordinationSvc"; //$NON-NLS-1$

	final static String OUTS = "outs"; //$NON-NLS-1$

	final static String INS = "ins"; //$NON-NLS-1$

	final static String RES = "res"; //$NON-NLS-1$

	final static String FUNCTIONS = "functions"; //$NON-NLS-1$

	/**
	 * Constructor.
	 */
	public RoboticsME(EObject source) {
		this(source, TransactionUtil.getEditingDomain(source));
	}

	/**
	 * Constructor.
	 */
	public RoboticsME(EObject source, EditingDomain domain) {
		super(source, domain);
	}

	public void updateSource(EObject source) {
		this.source = source;
	}

	@Override
	public IStaticContentProvider getContentProvider(String propertyPath) {
		if (propertyPath.equals(COORDINATION_SVC)) {
			SkillDefinition sd = UMLUtil.getStereotypeApplication((Operation) source, SkillDefinition.class);
			EObject oState = SkillSemanticObservableCS.getOperationalState(sd);
			return new RoboticsContentProvider(source,
					new UMLContentProvider(oState, SkillsPackage.eINSTANCE.getSkillOperationalState_CompInterface()),
					CoordinationService.class, FileExtensions.SERVICEDEF_UML);
		}
		if (propertyPath.equals(INS)) {
			return new FilterStereotypes(super.getContentProvider(propertyPath), InAttribute.class);
		} else if (propertyPath.equals(OUTS)) {
			return new FilterStereotypes(super.getContentProvider(propertyPath), OutAttribute.class);
		} else if (propertyPath.endsWith(RES)) {
			return new FilterStereotypes(super.getContentProvider(propertyPath), SkillResult.class);
		}
		return super.getContentProvider(propertyPath);
	}

	@Override
	public IObservable doGetObservable(String propertyPath) {
		IObservable observable = null;

		if (propertyPath.equals(COORDINATION_SVC)) {
			SkillDefinition sd = UMLUtil.getStereotypeApplication((Operation) source, SkillDefinition.class);
			observable = new SkillSemanticObservableCS(sd, domain);
		} else if (isSkillParameter(propertyPath)) {
			observable = new SkillParameterObservable((Operation) source, domain);
		} else if (propertyPath.equals(FUNCTIONS)) {
			observable = new FunctionObservable((Class) source, domain);
		} else if (propertyPath.equals(QOS)) {
			observable = new FunctionObservable((Class) source, domain);
		} else {
			observable = super.doGetObservable(propertyPath);
		}

		return observable;
	}

	protected boolean isSkillParameter(String propertyPath) {
		return propertyPath.equals(INS) ||
				propertyPath.equals(OUTS) ||
				propertyPath.equals(RES);
	}

	@Override
	protected boolean isFeatureEditable(String propertyPath) {
		if (source instanceof Operation) {
			if (isSkillParameter(propertyPath)) {
				return true;
			} else if (propertyPath.equals(COORDINATION_SVC)) {
				return true;
			}
		} else if (source instanceof Class) {
			if (propertyPath.equals(FUNCTIONS)) {
				return true;
			}
		}
		return super.isFeatureEditable(propertyPath);
	}


	@Override
	public ReferenceValueFactory getValueFactory(final String propertyPath) {
		final EReference paramFeature = UMLPackage.eINSTANCE.getBehavioralFeature_OwnedParameter();
		final EReference attrFeature = UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute();
		if (propertyPath.equals(INS)) {
			return new SkillReferenceFactory((Operation) source, paramFeature, ParameterDirectionKind.IN_LITERAL);
		} else if (propertyPath.equals(OUTS)) {
			return new SkillReferenceFactory((Operation) source, paramFeature, ParameterDirectionKind.OUT_LITERAL);
		} else if (propertyPath.equals(RES)) {
			return new SkillReferenceFactory((Operation) source, paramFeature, ParameterDirectionKind.RETURN_LITERAL);
		} else if (propertyPath.equals(FUNCTIONS)) {
			return new FunctionReferenceFactory((Class) source, attrFeature);
		}
		return super.getValueFactory(propertyPath);
	}

	@Override
	public boolean getDirectCreation(final String propertyPath) {
		if (isSkillParameter(propertyPath)) {
			return true;
		} else if (propertyPath.equals(FUNCTIONS)) {
			return true;
		}
		return super.getDirectCreation(propertyPath);
	}
}