/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.papyrus.infra.widgets.editors.MultipleReferenceEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * Multi reference for tasks
 */
public class TaskMultiReference extends RobMultiReference {

	TaskReferenceEditor taskEditor;

	public TaskMultiReference(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected MultipleReferenceEditor createMultipleReferenceEditor(Composite parent, int style) {
		taskEditor = new TaskReferenceEditor(parent, style);
		return taskEditor;
	}

 }
