/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.papyrus.robotics.properties.widgets.ReferenceWoAddDialog;
import org.eclipse.swt.widgets.Composite;

/**
 * A dialog that filters available selections: only ComponentDefinitions are shown
 */
public class ActionSkillDialog extends ReferenceWoAddDialog {

	public ActionSkillDialog(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected org.eclipse.papyrus.infra.widgets.editors.ReferenceDialog createReferenceDialog(Composite parent, int style) {
		return new ActionSkillEditor(parent, style);
	}
 }
