/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.papyrus.infra.widgets.editors.MultipleReferenceEditor;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsLabelProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.swt.widgets.Composite;

/**
 * Editor for tasks
 */
public class TaskReferenceEditor extends MultipleReferenceEditor {

	/**
	 * Constructor.
	 *
	 * @param parent a parent composite
	 * @param style
	 */
	public TaskReferenceEditor(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	protected void setInput(@SuppressWarnings("rawtypes") IObservableList modelProperty) {
		super.setInput(modelProperty);
		Object contextObj = getContextElement();
		if (contextObj instanceof EObject) {
			EObject context = (EObject) contextObj;
			// create robotics content and label provider
			IStaticContentProvider roboticsCP = new RoboticsContentProvider(context, contentProvider, Task.class, FileExtensions.BT_UML);
			ILabelProvider roboticsLP = new RoboticsLabelProvider((ILabelProvider) getLabelProvider());
			// use a new selector (it's not possible to override the selector creation in the super class MultipleReferenceEditor, since
			// no constructor enables to pass a selector)
			RoboticsElementSelector roboticsSelector = new RoboticsElementSelector();
			setSelector(roboticsSelector);
			roboticsSelector.setProviders(roboticsCP, roboticsLP);
		}
	}

	@Override
	public void setProviders(IStaticContentProvider contentProvider, ILabelProvider labelProvider) {
		// don't use superclass method, as it resets selector providers
		this.contentProvider = contentProvider;
		super.setLabelProvider(labelProvider);
	}
}
