/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by implementation of ProvidedInterfaceObservableList)
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Workaround for bug https://git.eclipse.org/r/#/c/153602/
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.modelelement;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.infra.services.edit.ui.databinding.PapyrusObservableList;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * An IObservableList to edit the UML derived feature, in/out/res parameters.
 */
public class SkillParameterObservable extends PapyrusObservableList {

	/**
	 * Instantiates a new provided interface observable list.
	 *
	 * @param operation
	 *            the operation
	 * @param domain
	 *            the domain
	 */
	public SkillParameterObservable(Operation operation,EditingDomain domain) {
		super(operation.getOwnedParameters(), domain, operation, UMLPackage.eINSTANCE.getBehavioralFeature_OwnedParameter(),
				GMFtoEMFCommandWrapper::wrap);
	}

	/**
	 * Override getAddCommand to always access the operation's complete parameter list
	 * (see bug at https://git.eclipse.org/r/#/c/153602/)
	 */
	@Override
	public Command getAddCommand(Object value) {
		IElementEditService provider = getProvider();
		if (provider != null) {
			List<Object> values = new LinkedList<Object>( ((Operation) this.source).getOwnedParameters() );
			values.add(value);
			return getCommandFromRequests(provider, getRequests(values, null));
		}

		return super.getAddCommand(value);
	}

	/**
	 * Override getRemoveCommand to always access the operation's complete parameter list
	 * (see bug at https://git.eclipse.org/r/#/c/153602/)
	 */
	@Override
	public Command getRemoveCommand(final Object value) {
		IElementEditService provider = getProvider();

		if (provider != null) {
			List<Object> values = new LinkedList<Object>( ((Operation) this.source).getOwnedParameters() );
			values.remove(value);
			return getCommandFromRequests(provider, getRequests(values, Collections.singletonList(value)));
		}

		return super.getRemoveCommand(value);
	}
}
