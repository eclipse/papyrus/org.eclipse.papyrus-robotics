/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *                                from Ansgar Radermacher) - Bug #581690
 *  Ansgar Radermacher (CEA LIST), Bug #582127
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.properties.widgets;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.widgets.editors.ICommitListener;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.provider.CoordinationServiceFilterProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Editor for selecting the service definitions that are provided or required by a port
 * It assures that
 * 1. Only ServiceDefinitions can be selected
 * 2. RobMoSys stereotypes are applied on relationships
 */
public class CoordinationServiceReferenceEditor extends RoboticsReferenceDialog {

	ICommitListener applyStereoListener;
	protected EObject context;

	/**
	 * Constructor.
	 *
	 * @param parent a parent composite
	 * @param style
	 */
	public CoordinationServiceReferenceEditor(Composite parent, int style) {
		super(parent, style, CoordinationService.class, FileExtensions.SERVICEDEF_UML);
	}

	@Override
	public void setModelObservable(@SuppressWarnings("rawtypes") IObservableValue modelProperty) {
		super.setModelObservable(modelProperty);
		// get context (extracted from model Property, i.e. not known before)
		Object contextObj = getContextElement();
		if (contextObj instanceof SkillOperationalState) {
			SkillOperationalState oState = (SkillOperationalState) contextObj;
			SkillSemantic skSem = oState.getFsm();
			BehavioralFeature bf  = skSem.getBase_StateMachine().getSpecification();
			SkillDefinition skDef = UMLUtil.getStereotypeApplication(bf, SkillDefinition.class);
			if (skDef != null) {
				// decide whether to filter for coordination service actions or queries
				String pattern = "robotics::commpatterns::Query"; //$NON-NLS-1$
				for (SkillResult res : skDef.getRes()) {
					if (res.getKind() == SkillResultKind.RUNNING) {
						pattern = "robotics::commpatterns::Action"; //$NON-NLS-1$
						break;
					}
				}
				// create robotics content provider
				EncapsulatedContentProvider roboticsProvider = new RoboticsContentProvider(
						bf,
						new CoordinationServiceFilterProvider(
								contentProvider,
								pattern
							),
						CoordinationService.class,
						FileExtensions.SERVICEDEF_UML
					);
				setContentProvider(roboticsProvider);
			}
		}
	}
}
