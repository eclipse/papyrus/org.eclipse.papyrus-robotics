/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bpc.profile;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;

/**
 * Derived attribute implementation for Collection stereotype
 */
public class CollectionOperations {

	public static EList<Relation> getRelation() {
		// return an empty list (refined in sub-classes)
		return new BasicEList<Relation>();
	}

	public static EList<Entity> getEntity() {
		// return an empty list (refined in sub-classes)
		return new BasicEList<Entity>();
	}
}
