/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bpc.profile;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Property;
import org.eclipse.papyrus.robotics.bpc.profile.util.BPCResource;

/**
 * Derived attribute implementation for Entity stereotype
 */
public class EntityOperations {

	protected static EList<Property> emptyList = new BasicEList<Property>();

	/**
	 * @see #EntityImpl.getProperty()
	 */
	public static EList<Property> getProperty() {
		// Return empty list: it is up to subclasses to provide a meaningful implementation of
		// this method, based on what UML metaclass they do extend
		return emptyList;
	}

	/**
	 * The default UID is the URI fragment
	 */
	public static String getDefault_uid(Entity entity) {
		if (entity.eResource() != null) {
			return entity.eResource().getURIFragment(entity);
		}
		return null;
	}

	/**
	 * @see #EntityImpl.getInstance_uid(Entity, String)
	 */
	public static String getInstance_uid(Entity entity, String instance_uid) {
		if (instance_uid == null || instance_uid.length() == 0) {
			return getDefault_uid(entity);
		}
		return instance_uid;
	}

	/**
	 * @return the instance-uid to use for setting
	 */
	public static String getSetInstance_uid(Entity entity, String newInstance_uid) {
		if (newInstance_uid != null && newInstance_uid.equals(getDefault_uid(entity))) {
			// unset uid, if its corresponds to default_uid
			return null;
		}
		return newInstance_uid;
	}

	/**
	 * @see #EntityImpl.getModel_uid(Entity)
	 */
	public static String getModel_uid(Entity entity) {
		String instId = "unknown";
		if (entity.eResource() != null && entity.eResource().getContents() != null) {
			EList<EObject> contents = entity.eResource().getContents();
			if (contents.size() > 0) {
				instId = entity.eResource().getURIFragment(contents.get(0));
			}
		}
		return instId;
	}

	/**
	 * @see #EntityImpl.getMetamodel_uid()
	 */
	public static String getMetamodel_uid() {
		return BPCResource.PROFILE_URI;
	}
}
