/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bpc.profile;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Derived attribute implementation for Block stereotype
 */
public class BlockOperations {
	/**
	 */
	public static EList<Port> getPort(Block block) {
		EList<Port> portList = new UniqueEList<Port>();
		// check is required, since not all blocks might have a base class (e.g. RealizeImpl inherits from Block)
		if (block.getBase_Class() != null) {
			for (org.eclipse.uml2.uml.Port port : block.getBase_Class().getOwnedPorts()) {
				Port service = UMLUtil.getStereotypeApplication(port, Port.class);
				if (service != null) {
					portList.add(service);
				}
			}
		}
		return portList;
	}

	/**
	 */
	public static EList<Connector> getConnector(Block block) {
		EList<Connector> connectorList = new UniqueEList<Connector>();
		if (block.getBase_Class() != null) {
			for (org.eclipse.uml2.uml.Connector umlConnector : block.getBase_Class().getOwnedConnectors()) {
				if (umlConnector != null) {
					Connector connector = UMLUtil.getStereotypeApplication(umlConnector, Connector.class);
					if (connector != null) {
						connectorList.add(connector);
					}
				}
			}
		}
		return connectorList;
	}
	
	/**
	 */
	public static EList<org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection> getCollection(Block block) {
		EList<org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection> collectionList = new UniqueEList<org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection>();
		if (block.getBase_Class() != null) {
			for (Classifier cl : block.getBase_Class().getNestedClassifiers()) {
				if (cl != null) {
					org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection collection = UMLUtil.getStereotypeApplication(cl, org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection.class);
					if (collection != null) {
						collectionList.add(collection);
					}
				}
			}
		}
		return collectionList;
	}

	/**
	 */
	public static EList<Block> getBlock(Block block) {
		EList<Block> blockList = new UniqueEList<Block>();
		if (block.getBase_Class() != null) {
			for (Classifier cl : block.getBase_Class().getNestedClassifiers()) {
				if (cl != null) {
					Block nestedBlock = UMLUtil.getStereotypeApplication(cl, Block.class);
					if (block != null) {
						blockList.add(nestedBlock);
					}
				}
			}
		}
		return blockList;
	}

	/**
	 */
	public static EList<Relation> getRelation(Block block) {
		EList<Relation> relationList = new UniqueEList<Relation>();
		if (block.getBase_Class() != null) {
			for (DirectedRelationship dr : block.getBase_Class().getSourceDirectedRelationships()) {
				if (dr != null) {
					Relation relation = UMLUtil.getStereotypeApplication(dr, Relation.class);
					if (relation != null) {
						relationList.add(relation);
					}
				}
			}
		}
		return relationList;
	}
}
