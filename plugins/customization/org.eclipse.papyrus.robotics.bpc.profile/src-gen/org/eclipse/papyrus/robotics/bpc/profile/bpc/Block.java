/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The relationships collection, relation, connector ad port are realized on UML level in form of nested classifiers (other blocks or collections), owned relationships, owned connectors. and owned ports respectively. Therefore, they are derived on stereotype level.
 * Port-ownership concept reused from UML, which implies port is derived attribute on stereotype level
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getPort <em>Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getConnector <em>Connector</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getCollection <em>Collection</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBlock <em>Block</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getRelation <em>Relation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBase_Class <em>Base Class</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock()
 * @model
 * @generated
 */
public interface Block extends Entity {
	/**
	 * Returns the value of the '<em><b>Port</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Port}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock_Port()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Port> getPort();

	/**
	 * Returns the value of the '<em><b>Connector</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connector</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connector</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock_Connector()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Connector> getConnector();

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock_Collection()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Collection> getCollection();

	/**
	 * Returns the value of the '<em><b>Block</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock_Block()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Block> getBlock();

	/**
	 * Returns the value of the '<em><b>Relation</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock_Relation()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Relation> getRelation();

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getBlock_Base_Class()
	 * @model ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

} // Block
