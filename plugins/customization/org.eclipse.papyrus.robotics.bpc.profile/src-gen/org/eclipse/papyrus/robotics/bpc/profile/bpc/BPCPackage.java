/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCFactory
 * @model kind="package"
 * @generated
 */
public interface BPCPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bpc"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/bpc/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "bpc"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BPCPackage eINSTANCE = org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__INSTANCE_UID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__AUTHORSHIP = 3;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__PROVENANCE = 4;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__MODEL_UID = 5;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__METAMODEL_UID = 6;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PortImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.VersionMetaDataImpl <em>Version Meta Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.VersionMetaDataImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getVersionMetaData()
	 * @generated
	 */
	int VERSION_META_DATA = 9;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl <em>Id Meta Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getIdMetaData()
	 * @generated
	 */
	int ID_META_DATA = 8;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PropertyImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.RelationImpl <em>Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.RelationImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getRelation()
	 * @generated
	 */
	int RELATION = 6;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getBlock()
	 * @generated
	 */
	int BLOCK = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PROPERTY = ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__INSTANCE_UID = ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__AUTHORSHIP = ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PROVENANCE = ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__MODEL_UID = ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__METAMODEL_UID = ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__PORT = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__CONNECTOR = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__COLLECTION = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__BLOCK = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__RELATION = ENTITY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__BASE_CLASS = ENTITY_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__PROPERTY = ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__INSTANCE_UID = ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__AUTHORSHIP = ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__PROVENANCE = ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__MODEL_UID = ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__METAMODEL_UID = ENTITY__METAMODEL_UID;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PROPERTY = ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__INSTANCE_UID = ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__AUTHORSHIP = ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__PROVENANCE = ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__MODEL_UID = ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__METAMODEL_UID = ENTITY__METAMODEL_UID;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.CollectionImpl <em>Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.CollectionImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getCollection()
	 * @generated
	 */
	int COLLECTION = 5;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ContainsImpl <em>Contains</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ContainsImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getContains()
	 * @generated
	 */
	int CONTAINS = 10;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Has_aImpl <em>Has a</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Has_aImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getHas_a()
	 * @generated
	 */
	int HAS_A = 11;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectsImpl <em>Connects</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectsImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConnects()
	 * @generated
	 */
	int CONNECTS = 12;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectorImpl <em>Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectorImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConnector()
	 * @generated
	 */
	int CONNECTOR = 4;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__PROPERTY = ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__INSTANCE_UID = ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__AUTHORSHIP = ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__PROVENANCE = ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__MODEL_UID = ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR__METAMODEL_UID = ENTITY__METAMODEL_UID;

	/**
	 * The number of structural features of the '<em>Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTOR_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__PROPERTY = ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__INSTANCE_UID = ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__AUTHORSHIP = ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__PROVENANCE = ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__MODEL_UID = ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__METAMODEL_UID = ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__RELATION = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION__ENTITY = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLECTION_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__PROPERTY = BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__INSTANCE_UID = BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__DESCRIPTION = BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__AUTHORSHIP = BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__PROVENANCE = BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__MODEL_UID = BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__METAMODEL_UID = BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__PORT = BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__CONNECTOR = BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__COLLECTION = BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__BLOCK = BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__RELATION = BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__BASE_CLASS = BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION__REIFICATION = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATION_OPERATION_COUNT = BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ReificationMetaDataImpl <em>Reification Meta Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ReificationMetaDataImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getReificationMetaData()
	 * @generated
	 */
	int REIFICATION_META_DATA = 7;

	/**
	 * The feature id for the '<em><b>Parents eref name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REIFICATION_META_DATA__PARENTS_EREF_NAME = 0;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REIFICATION_META_DATA__ENTITY = 1;

	/**
	 * The number of structural features of the '<em>Reification Meta Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REIFICATION_META_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Reification Meta Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REIFICATION_META_DATA_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_META_DATA__INSTANCE_UID = 0;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_META_DATA__MODEL_UID = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_META_DATA__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_META_DATA__METAMODEL_UID = 3;

	/**
	 * The number of structural features of the '<em>Id Meta Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_META_DATA_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Id Meta Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ID_META_DATA_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_META_DATA__PROVENANCE = 0;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_META_DATA__AUTHORSHIP = 1;

	/**
	 * The number of structural features of the '<em>Version Meta Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_META_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Version Meta Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERSION_META_DATA_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Contains</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Contains</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Has a</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Has a</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAS_A_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Connects</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connects</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTS_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Conforms_toImpl <em>Conforms to</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Conforms_toImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConforms_to()
	 * @generated
	 */
	int CONFORMS_TO = 13;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Conforms to</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Conforms to</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFORMS_TO_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.DockImpl <em>Dock</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.DockImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getDock()
	 * @generated
	 */
	int DOCK = 14;

	/**
	 * The number of structural features of the '<em>Dock</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCK_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Dock</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCK_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConstraintsImpl <em>Constraints</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConstraintsImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConstraints()
	 * @generated
	 */
	int CONSTRAINTS = 15;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Constraints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Constraints</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINTS_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Instance_ofImpl <em>Instance of</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Instance_ofImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getInstance_of()
	 * @generated
	 */
	int INSTANCE_OF = 16;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Instance of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Instance of</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCE_OF_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Is_aImpl <em>Is a</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Is_aImpl
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getIs_a()
	 * @generated
	 */
	int IS_A = 17;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__PROPERTY = RELATION__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__INSTANCE_UID = RELATION__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__DESCRIPTION = RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__AUTHORSHIP = RELATION__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__PROVENANCE = RELATION__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__MODEL_UID = RELATION__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__METAMODEL_UID = RELATION__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__PORT = RELATION__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__CONNECTOR = RELATION__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__COLLECTION = RELATION__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__BLOCK = RELATION__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__RELATION = RELATION__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__BASE_CLASS = RELATION__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A__REIFICATION = RELATION__REIFICATION;

	/**
	 * The number of structural features of the '<em>Is a</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A_FEATURE_COUNT = RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Is a</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_A_OPERATION_COUNT = RELATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Property</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProperty()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_Property();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getInstance_uid <em>Instance uid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instance uid</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getInstance_uid()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Instance_uid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getModel_uid <em>Model uid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model uid</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getModel_uid()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Model_uid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getDescription()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getMetamodel_uid <em>Metamodel uid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel uid</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getMetamodel_uid()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Metamodel_uid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProvenance <em>Provenance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Provenance</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProvenance()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Provenance();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getAuthorship <em>Authorship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Authorship</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getAuthorship()
	 * @see #getEntity()
	 * @generated
	 */
	EAttribute getEntity_Authorship();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData <em>Version Meta Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Version Meta Data</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData
	 * @generated
	 */
	EClass getVersionMetaData();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getProvenance <em>Provenance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Provenance</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getProvenance()
	 * @see #getVersionMetaData()
	 * @generated
	 */
	EAttribute getVersionMetaData_Provenance();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getAuthorship <em>Authorship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Authorship</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getAuthorship()
	 * @see #getVersionMetaData()
	 * @generated
	 */
	EAttribute getVersionMetaData_Authorship();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData <em>Id Meta Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Id Meta Data</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData
	 * @generated
	 */
	EClass getIdMetaData();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getInstance_uid <em>Instance uid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instance uid</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getInstance_uid()
	 * @see #getIdMetaData()
	 * @generated
	 */
	EAttribute getIdMetaData_Instance_uid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getModel_uid <em>Model uid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model uid</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getModel_uid()
	 * @see #getIdMetaData()
	 * @generated
	 */
	EAttribute getIdMetaData_Model_uid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getDescription()
	 * @see #getIdMetaData()
	 * @generated
	 */
	EAttribute getIdMetaData_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getMetamodel_uid <em>Metamodel uid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel uid</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getMetamodel_uid()
	 * @see #getIdMetaData()
	 * @generated
	 */
	EAttribute getIdMetaData_Metamodel_uid();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relation</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation
	 * @generated
	 */
	EClass getRelation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation#getReification <em>Reification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reification</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation#getReification()
	 * @see #getRelation()
	 * @generated
	 */
	EReference getRelation_Reification();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getPort()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Port();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getConnector <em>Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connector</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getConnector()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Connector();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Collection</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getCollection()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Collection();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBlock <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Block</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBlock()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Block();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Relation</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getRelation()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Relation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Block#getBase_Class()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collection</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection
	 * @generated
	 */
	EClass getCollection();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection#getRelation <em>Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Relation</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection#getRelation()
	 * @see #getCollection()
	 * @generated
	 */
	EReference getCollection_Relation();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Entity</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection#getEntity()
	 * @see #getCollection()
	 * @generated
	 */
	EReference getCollection_Entity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData <em>Reification Meta Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reification Meta Data</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData
	 * @generated
	 */
	EClass getReificationMetaData();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getParents_eref_name <em>Parents eref name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parents eref name</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getParents_eref_name()
	 * @see #getReificationMetaData()
	 * @generated
	 */
	EAttribute getReificationMetaData_Parents_eref_name();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getEntity()
	 * @see #getReificationMetaData()
	 * @generated
	 */
	EReference getReificationMetaData_Entity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Contains <em>Contains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contains</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Contains
	 * @generated
	 */
	EClass getContains();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Has_a <em>Has a</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Has a</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Has_a
	 * @generated
	 */
	EClass getHas_a();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Connects <em>Connects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connects</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Connects
	 * @generated
	 */
	EClass getConnects();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Conforms_to <em>Conforms to</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conforms to</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Conforms_to
	 * @generated
	 */
	EClass getConforms_to();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector <em>Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connector</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector
	 * @generated
	 */
	EClass getConnector();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Dock <em>Dock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dock</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Dock
	 * @generated
	 */
	EClass getDock();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Constraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraints</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Constraints
	 * @generated
	 */
	EClass getConstraints();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Instance_of <em>Instance of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance of</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Instance_of
	 * @generated
	 */
	EClass getInstance_of();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Is_a <em>Is a</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is a</em>'.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.Is_a
	 * @generated
	 */
	EClass getIs_a();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BPCFactory getBPCFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PortImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__PROPERTY = eINSTANCE.getEntity_Property();

		/**
		 * The meta object literal for the '<em><b>Instance uid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__INSTANCE_UID = eINSTANCE.getEntity_Instance_uid();

		/**
		 * The meta object literal for the '<em><b>Model uid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__MODEL_UID = eINSTANCE.getEntity_Model_uid();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__DESCRIPTION = eINSTANCE.getEntity_Description();

		/**
		 * The meta object literal for the '<em><b>Metamodel uid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__METAMODEL_UID = eINSTANCE.getEntity_Metamodel_uid();

		/**
		 * The meta object literal for the '<em><b>Provenance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__PROVENANCE = eINSTANCE.getEntity_Provenance();

		/**
		 * The meta object literal for the '<em><b>Authorship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY__AUTHORSHIP = eINSTANCE.getEntity_Authorship();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.VersionMetaDataImpl <em>Version Meta Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.VersionMetaDataImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getVersionMetaData()
		 * @generated
		 */
		EClass VERSION_META_DATA = eINSTANCE.getVersionMetaData();

		/**
		 * The meta object literal for the '<em><b>Provenance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VERSION_META_DATA__PROVENANCE = eINSTANCE.getVersionMetaData_Provenance();

		/**
		 * The meta object literal for the '<em><b>Authorship</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VERSION_META_DATA__AUTHORSHIP = eINSTANCE.getVersionMetaData_Authorship();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl <em>Id Meta Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getIdMetaData()
		 * @generated
		 */
		EClass ID_META_DATA = eINSTANCE.getIdMetaData();

		/**
		 * The meta object literal for the '<em><b>Instance uid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_META_DATA__INSTANCE_UID = eINSTANCE.getIdMetaData_Instance_uid();

		/**
		 * The meta object literal for the '<em><b>Model uid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_META_DATA__MODEL_UID = eINSTANCE.getIdMetaData_Model_uid();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_META_DATA__DESCRIPTION = eINSTANCE.getIdMetaData_Description();

		/**
		 * The meta object literal for the '<em><b>Metamodel uid</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ID_META_DATA__METAMODEL_UID = eINSTANCE.getIdMetaData_Metamodel_uid();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PropertyImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.RelationImpl <em>Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.RelationImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getRelation()
		 * @generated
		 */
		EClass RELATION = eINSTANCE.getRelation();

		/**
		 * The meta object literal for the '<em><b>Reification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATION__REIFICATION = eINSTANCE.getRelation_Reification();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getBlock()
		 * @generated
		 */
		EClass BLOCK = eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__PORT = eINSTANCE.getBlock_Port();

		/**
		 * The meta object literal for the '<em><b>Connector</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__CONNECTOR = eINSTANCE.getBlock_Connector();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__COLLECTION = eINSTANCE.getBlock_Collection();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__BLOCK = eINSTANCE.getBlock_Block();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__RELATION = eINSTANCE.getBlock_Relation();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__BASE_CLASS = eINSTANCE.getBlock_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.CollectionImpl <em>Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.CollectionImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getCollection()
		 * @generated
		 */
		EClass COLLECTION = eINSTANCE.getCollection();

		/**
		 * The meta object literal for the '<em><b>Relation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION__RELATION = eINSTANCE.getCollection_Relation();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLECTION__ENTITY = eINSTANCE.getCollection_Entity();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ReificationMetaDataImpl <em>Reification Meta Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ReificationMetaDataImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getReificationMetaData()
		 * @generated
		 */
		EClass REIFICATION_META_DATA = eINSTANCE.getReificationMetaData();

		/**
		 * The meta object literal for the '<em><b>Parents eref name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REIFICATION_META_DATA__PARENTS_EREF_NAME = eINSTANCE.getReificationMetaData_Parents_eref_name();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REIFICATION_META_DATA__ENTITY = eINSTANCE.getReificationMetaData_Entity();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ContainsImpl <em>Contains</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ContainsImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getContains()
		 * @generated
		 */
		EClass CONTAINS = eINSTANCE.getContains();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Has_aImpl <em>Has a</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Has_aImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getHas_a()
		 * @generated
		 */
		EClass HAS_A = eINSTANCE.getHas_a();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectsImpl <em>Connects</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectsImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConnects()
		 * @generated
		 */
		EClass CONNECTS = eINSTANCE.getConnects();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Conforms_toImpl <em>Conforms to</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Conforms_toImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConforms_to()
		 * @generated
		 */
		EClass CONFORMS_TO = eINSTANCE.getConforms_to();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectorImpl <em>Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectorImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConnector()
		 * @generated
		 */
		EClass CONNECTOR = eINSTANCE.getConnector();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.DockImpl <em>Dock</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.DockImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getDock()
		 * @generated
		 */
		EClass DOCK = eINSTANCE.getDock();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConstraintsImpl <em>Constraints</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConstraintsImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getConstraints()
		 * @generated
		 */
		EClass CONSTRAINTS = eINSTANCE.getConstraints();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Instance_ofImpl <em>Instance of</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Instance_ofImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getInstance_of()
		 * @generated
		 */
		EClass INSTANCE_OF = eINSTANCE.getInstance_of();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Is_aImpl <em>Is a</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.Is_aImpl
		 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BPCPackageImpl#getIs_a()
		 * @generated
		 */
		EClass IS_A = eINSTANCE.getIs_a();

	}

} //BPCPackage
