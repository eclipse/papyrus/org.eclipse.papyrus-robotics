/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.papyrus.robotics.bpc.profile.EntityOperations;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getInstance_uid <em>Instance uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getAuthorship <em>Authorship</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getProvenance <em>Provenance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getModel_uid <em>Model uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl#getMetamodel_uid <em>Metamodel uid</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntityImpl extends MinimalEObjectImpl.Container implements Entity {
	/**
	 * The default value of the '{@link #getInstance_uid() <em>Instance uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance_uid()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCE_UID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstance_uid() <em>Instance uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance_uid()
	 * @generated
	 * @ordered
	 */
	protected String instance_uid = INSTANCE_UID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getAuthorship() <em>Authorship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorship()
	 * @generated
	 * @ordered
	 */
	protected static final String AUTHORSHIP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAuthorship() <em>Authorship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorship()
	 * @generated
	 * @ordered
	 */
	protected String authorship = AUTHORSHIP_EDEFAULT;

	/**
	 * The default value of the '{@link #getProvenance() <em>Provenance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvenance()
	 * @generated
	 * @ordered
	 */
	protected static final String PROVENANCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProvenance() <em>Provenance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvenance()
	 * @generated
	 * @ordered
	 */
	protected String provenance = PROVENANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getModel_uid() <em>Model uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel_uid()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_UID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getMetamodel_uid() <em>Metamodel uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetamodel_uid()
	 * @generated
	 * @ordered
	 */
	protected static final String METAMODEL_UID_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BPCPackage.Literals.ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Property> getProperty() {
		return EntityOperations.getProperty();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getInstance_uid() {
		return EntityOperations.getInstance_uid(this, instance_uid);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstance_uid(String newInstance_uid) {
		String oldInstance_uid = instance_uid;
		instance_uid = newInstance_uid;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.ENTITY__INSTANCE_UID, oldInstance_uid, instance_uid));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.ENTITY__DESCRIPTION, oldDescription, description));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAuthorship() {
		return authorship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAuthorship(String newAuthorship) {
		String oldAuthorship = authorship;
		authorship = newAuthorship;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.ENTITY__AUTHORSHIP, oldAuthorship, authorship));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProvenance() {
		return provenance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProvenance(String newProvenance) {
		String oldProvenance = provenance;
		provenance = newProvenance;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.ENTITY__PROVENANCE, oldProvenance, provenance));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getModel_uid() {
		return EntityOperations.getModel_uid(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getMetamodel_uid() {
		return EntityOperations.getMetamodel_uid();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BPCPackage.ENTITY__PROPERTY:
				return getProperty();
			case BPCPackage.ENTITY__INSTANCE_UID:
				return getInstance_uid();
			case BPCPackage.ENTITY__DESCRIPTION:
				return getDescription();
			case BPCPackage.ENTITY__AUTHORSHIP:
				return getAuthorship();
			case BPCPackage.ENTITY__PROVENANCE:
				return getProvenance();
			case BPCPackage.ENTITY__MODEL_UID:
				return getModel_uid();
			case BPCPackage.ENTITY__METAMODEL_UID:
				return getMetamodel_uid();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BPCPackage.ENTITY__PROPERTY:
				getProperty().clear();
				getProperty().addAll((Collection<? extends Property>)newValue);
				return;
			case BPCPackage.ENTITY__INSTANCE_UID:
				setInstance_uid((String)newValue);
				return;
			case BPCPackage.ENTITY__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case BPCPackage.ENTITY__AUTHORSHIP:
				setAuthorship((String)newValue);
				return;
			case BPCPackage.ENTITY__PROVENANCE:
				setProvenance((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BPCPackage.ENTITY__PROPERTY:
				getProperty().clear();
				return;
			case BPCPackage.ENTITY__INSTANCE_UID:
				setInstance_uid(INSTANCE_UID_EDEFAULT);
				return;
			case BPCPackage.ENTITY__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case BPCPackage.ENTITY__AUTHORSHIP:
				setAuthorship(AUTHORSHIP_EDEFAULT);
				return;
			case BPCPackage.ENTITY__PROVENANCE:
				setProvenance(PROVENANCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BPCPackage.ENTITY__PROPERTY:
				return !getProperty().isEmpty();
			case BPCPackage.ENTITY__INSTANCE_UID:
				return INSTANCE_UID_EDEFAULT == null ? instance_uid != null : !INSTANCE_UID_EDEFAULT.equals(instance_uid);
			case BPCPackage.ENTITY__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case BPCPackage.ENTITY__AUTHORSHIP:
				return AUTHORSHIP_EDEFAULT == null ? authorship != null : !AUTHORSHIP_EDEFAULT.equals(authorship);
			case BPCPackage.ENTITY__PROVENANCE:
				return PROVENANCE_EDEFAULT == null ? provenance != null : !PROVENANCE_EDEFAULT.equals(provenance);
			case BPCPackage.ENTITY__MODEL_UID:
				return MODEL_UID_EDEFAULT == null ? getModel_uid() != null : !MODEL_UID_EDEFAULT.equals(getModel_uid());
			case BPCPackage.ENTITY__METAMODEL_UID:
				return METAMODEL_UID_EDEFAULT == null ? getMetamodel_uid() != null : !METAMODEL_UID_EDEFAULT.equals(getMetamodel_uid());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (instance_uid: "); //$NON-NLS-1$
		result.append(instance_uid);
		result.append(", description: "); //$NON-NLS-1$
		result.append(description);
		result.append(", authorship: "); //$NON-NLS-1$
		result.append(authorship);
		result.append(", provenance: "); //$NON-NLS-1$
		result.append(provenance);
		result.append(')');
		return result.toString();
	}

} //EntityImpl
