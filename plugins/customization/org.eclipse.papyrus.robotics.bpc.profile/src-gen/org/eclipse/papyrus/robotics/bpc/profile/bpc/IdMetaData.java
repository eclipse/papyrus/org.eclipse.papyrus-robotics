/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Id Meta Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The IdMetaData and VersionMetaData types are not directly used, but integrated directly in the entity data type.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getInstance_uid <em>Instance uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getModel_uid <em>Model uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getMetamodel_uid <em>Metamodel uid</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getIdMetaData()
 * @model
 * @generated
 */
public interface IdMetaData extends EObject {
	/**
	 * Returns the value of the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance uid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance uid</em>' attribute.
	 * @see #setInstance_uid(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getIdMetaData_Instance_uid()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getInstance_uid();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getInstance_uid <em>Instance uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance uid</em>' attribute.
	 * @see #getInstance_uid()
	 * @generated
	 */
	void setInstance_uid(String value);

	/**
	 * Returns the value of the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model uid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model uid</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getIdMetaData_Model_uid()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	String getModel_uid();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getIdMetaData_Description()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel uid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel uid</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getIdMetaData_Metamodel_uid()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	String getMetamodel_uid();

} // IdMetaData
