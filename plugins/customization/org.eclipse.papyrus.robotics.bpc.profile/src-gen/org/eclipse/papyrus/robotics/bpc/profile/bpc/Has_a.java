/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has a</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getHas_a()
 * @model
 * @generated
 */
public interface Has_a extends Relation {
} // Has_a
