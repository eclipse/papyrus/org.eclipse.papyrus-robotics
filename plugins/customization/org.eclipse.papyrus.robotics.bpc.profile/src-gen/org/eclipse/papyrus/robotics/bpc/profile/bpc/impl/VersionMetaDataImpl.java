/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Version Meta Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.VersionMetaDataImpl#getProvenance <em>Provenance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.VersionMetaDataImpl#getAuthorship <em>Authorship</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VersionMetaDataImpl extends MinimalEObjectImpl.Container implements VersionMetaData {
	/**
	 * The default value of the '{@link #getProvenance() <em>Provenance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvenance()
	 * @generated
	 * @ordered
	 */
	protected static final String PROVENANCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProvenance() <em>Provenance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvenance()
	 * @generated
	 * @ordered
	 */
	protected String provenance = PROVENANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAuthorship() <em>Authorship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorship()
	 * @generated
	 * @ordered
	 */
	protected static final String AUTHORSHIP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAuthorship() <em>Authorship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorship()
	 * @generated
	 * @ordered
	 */
	protected String authorship = AUTHORSHIP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VersionMetaDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BPCPackage.Literals.VERSION_META_DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProvenance() {
		return provenance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProvenance(String newProvenance) {
		String oldProvenance = provenance;
		provenance = newProvenance;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.VERSION_META_DATA__PROVENANCE, oldProvenance, provenance));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAuthorship() {
		return authorship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAuthorship(String newAuthorship) {
		String oldAuthorship = authorship;
		authorship = newAuthorship;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.VERSION_META_DATA__AUTHORSHIP, oldAuthorship, authorship));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BPCPackage.VERSION_META_DATA__PROVENANCE:
				return getProvenance();
			case BPCPackage.VERSION_META_DATA__AUTHORSHIP:
				return getAuthorship();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BPCPackage.VERSION_META_DATA__PROVENANCE:
				setProvenance((String)newValue);
				return;
			case BPCPackage.VERSION_META_DATA__AUTHORSHIP:
				setAuthorship((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BPCPackage.VERSION_META_DATA__PROVENANCE:
				setProvenance(PROVENANCE_EDEFAULT);
				return;
			case BPCPackage.VERSION_META_DATA__AUTHORSHIP:
				setAuthorship(AUTHORSHIP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BPCPackage.VERSION_META_DATA__PROVENANCE:
				return PROVENANCE_EDEFAULT == null ? provenance != null : !PROVENANCE_EDEFAULT.equals(provenance);
			case BPCPackage.VERSION_META_DATA__AUTHORSHIP:
				return AUTHORSHIP_EDEFAULT == null ? authorship != null : !AUTHORSHIP_EDEFAULT.equals(authorship);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (provenance: "); //$NON-NLS-1$
		result.append(provenance);
		result.append(", authorship: "); //$NON-NLS-1$
		result.append(authorship);
		result.append(')');
		return result.toString();
	}

} //VersionMetaDataImpl
