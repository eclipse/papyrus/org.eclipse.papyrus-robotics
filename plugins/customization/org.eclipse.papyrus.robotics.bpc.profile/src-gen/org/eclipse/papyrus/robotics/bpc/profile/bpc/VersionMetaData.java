/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version Meta Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The IdMetaData and VersionMetaData types are not directly used, but integrated directly in the entity data type.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getProvenance <em>Provenance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getAuthorship <em>Authorship</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getVersionMetaData()
 * @model
 * @generated
 */
public interface VersionMetaData extends EObject {
	/**
	 * Returns the value of the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provenance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provenance</em>' attribute.
	 * @see #setProvenance(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getVersionMetaData_Provenance()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getProvenance();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getProvenance <em>Provenance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provenance</em>' attribute.
	 * @see #getProvenance()
	 * @generated
	 */
	void setProvenance(String value);

	/**
	 * Returns the value of the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorship</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorship</em>' attribute.
	 * @see #setAuthorship(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getVersionMetaData_Authorship()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getAuthorship();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.VersionMetaData#getAuthorship <em>Authorship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Authorship</em>' attribute.
	 * @see #getAuthorship()
	 * @generated
	 */
	void setAuthorship(String value);

} // VersionMetaData
