/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.IdMetaData;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Id Meta Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl#getInstance_uid <em>Instance uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl#getModel_uid <em>Model uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.IdMetaDataImpl#getMetamodel_uid <em>Metamodel uid</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IdMetaDataImpl extends MinimalEObjectImpl.Container implements IdMetaData {
	/**
	 * The default value of the '{@link #getInstance_uid() <em>Instance uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance_uid()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCE_UID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstance_uid() <em>Instance uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance_uid()
	 * @generated
	 * @ordered
	 */
	protected String instance_uid = INSTANCE_UID_EDEFAULT;

	/**
	 * The default value of the '{@link #getModel_uid() <em>Model uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel_uid()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_UID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMetamodel_uid() <em>Metamodel uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetamodel_uid()
	 * @generated
	 * @ordered
	 */
	protected static final String METAMODEL_UID_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdMetaDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BPCPackage.Literals.ID_META_DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInstance_uid() {
		return instance_uid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstance_uid(String newInstance_uid) {
		String oldInstance_uid = instance_uid;
		instance_uid = newInstance_uid;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.ID_META_DATA__INSTANCE_UID, oldInstance_uid, instance_uid));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getModel_uid() {
		// TODO: implement this method to return the 'Model uid' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.ID_META_DATA__DESCRIPTION, oldDescription, description));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMetamodel_uid() {
		// TODO: implement this method to return the 'Metamodel uid' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BPCPackage.ID_META_DATA__INSTANCE_UID:
				return getInstance_uid();
			case BPCPackage.ID_META_DATA__MODEL_UID:
				return getModel_uid();
			case BPCPackage.ID_META_DATA__DESCRIPTION:
				return getDescription();
			case BPCPackage.ID_META_DATA__METAMODEL_UID:
				return getMetamodel_uid();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BPCPackage.ID_META_DATA__INSTANCE_UID:
				setInstance_uid((String)newValue);
				return;
			case BPCPackage.ID_META_DATA__DESCRIPTION:
				setDescription((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BPCPackage.ID_META_DATA__INSTANCE_UID:
				setInstance_uid(INSTANCE_UID_EDEFAULT);
				return;
			case BPCPackage.ID_META_DATA__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BPCPackage.ID_META_DATA__INSTANCE_UID:
				return INSTANCE_UID_EDEFAULT == null ? instance_uid != null : !INSTANCE_UID_EDEFAULT.equals(instance_uid);
			case BPCPackage.ID_META_DATA__MODEL_UID:
				return MODEL_UID_EDEFAULT == null ? getModel_uid() != null : !MODEL_UID_EDEFAULT.equals(getModel_uid());
			case BPCPackage.ID_META_DATA__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case BPCPackage.ID_META_DATA__METAMODEL_UID:
				return METAMODEL_UID_EDEFAULT == null ? getMetamodel_uid() != null : !METAMODEL_UID_EDEFAULT.equals(getMetamodel_uid());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (instance_uid: "); //$NON-NLS-1$
		result.append(instance_uid);
		result.append(", description: "); //$NON-NLS-1$
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //IdMetaDataImpl
