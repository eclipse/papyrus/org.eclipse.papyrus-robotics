/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reification Meta Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getParents_eref_name <em>Parents eref name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getEntity <em>Entity</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getReificationMetaData()
 * @model
 * @generated
 */
public interface ReificationMetaData extends EObject {
	/**
	 * Returns the value of the '<em><b>Parents eref name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parents eref name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parents eref name</em>' attribute.
	 * @see #setParents_eref_name(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getReificationMetaData_Parents_eref_name()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getParents_eref_name();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getParents_eref_name <em>Parents eref name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parents eref name</em>' attribute.
	 * @see #getParents_eref_name()
	 * @generated
	 */
	void setParents_eref_name(String value);

	/**
	 * Returns the value of the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' reference.
	 * @see #setEntity(Entity)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getReificationMetaData_Entity()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Entity getEntity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData#getEntity <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(Entity value);

} // ReificationMetaData
