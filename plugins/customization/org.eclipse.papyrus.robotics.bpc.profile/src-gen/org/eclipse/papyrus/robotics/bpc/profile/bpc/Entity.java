/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A BPC Entity extends the UML Element foundational concept.
 * A BPC Property specializes a BPC Entity. It is related with Entity through the UML ownedElement relationship. Has Id and Version metadata - which have been flattened for technical reasons.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProperty <em>Property</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getInstance_uid <em>Instance uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getAuthorship <em>Authorship</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProvenance <em>Provenance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getModel_uid <em>Model uid</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getMetamodel_uid <em>Metamodel uid</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity()
 * @model
 * @generated
 */
public interface Entity extends EObject {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Property()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Property> getProperty();

	/**
	 * Returns the value of the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance uid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance uid</em>' attribute.
	 * @see #setInstance_uid(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Instance_uid()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getInstance_uid();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getInstance_uid <em>Instance uid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance uid</em>' attribute.
	 * @see #getInstance_uid()
	 * @generated
	 */
	void setInstance_uid(String value);

	/**
	 * Returns the value of the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model uid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model uid</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Model_uid()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	String getModel_uid();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Description()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel uid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel uid</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Metamodel_uid()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	String getMetamodel_uid();

	/**
	 * Returns the value of the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provenance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provenance</em>' attribute.
	 * @see #setProvenance(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Provenance()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getProvenance();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getProvenance <em>Provenance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provenance</em>' attribute.
	 * @see #getProvenance()
	 * @generated
	 */
	void setProvenance(String value);

	/**
	 * Returns the value of the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorship</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorship</em>' attribute.
	 * @see #setAuthorship(String)
	 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getEntity_Authorship()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	String getAuthorship();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity#getAuthorship <em>Authorship</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Authorship</em>' attribute.
	 * @see #getAuthorship()
	 * @generated
	 */
	void setAuthorship(String value);

} // Entity
