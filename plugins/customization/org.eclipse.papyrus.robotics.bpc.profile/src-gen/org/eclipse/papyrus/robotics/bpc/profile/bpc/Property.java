/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A BPC Entity extends the UML Element foundational concept.
 * A BPC Property specializes a BPC Entity. It is related with Entity through the UML ownedElement relationship. Has Id and Version metadata - which have been flattened for technical reasons.
 * <!-- end-model-doc -->
 *
 *
 * @see org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage#getProperty()
 * @model
 * @generated
 */
public interface Property extends Entity {

} // Property
