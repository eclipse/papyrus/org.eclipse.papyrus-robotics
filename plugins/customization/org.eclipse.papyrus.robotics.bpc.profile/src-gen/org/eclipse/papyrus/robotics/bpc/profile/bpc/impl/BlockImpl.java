/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.papyrus.robotics.bpc.profile.BlockOperations;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Collection;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl#getPort <em>Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl#getConnector <em>Connector</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl#getCollection <em>Collection</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl#getBlock <em>Block</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl#getRelation <em>Relation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl#getBase_Class <em>Base Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlockImpl extends EntityImpl implements Block {
	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BPCPackage.Literals.BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Port> getPort() {
		return BlockOperations.getPort(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Connector> getConnector() {
		return BlockOperations.getConnector(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Collection> getCollection() {
		return BlockOperations.getCollection(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Block> getBlock() {
		return BlockOperations.getBlock(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Relation> getRelation() {
		return BlockOperations.getRelation(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject)base_Class;
			base_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired()) {
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BPCPackage.BLOCK__BASE_CLASS, oldBase_Class, base_Class));
				}
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.BLOCK__BASE_CLASS, oldBase_Class, base_Class));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BPCPackage.BLOCK__PORT:
				return getPort();
			case BPCPackage.BLOCK__CONNECTOR:
				return getConnector();
			case BPCPackage.BLOCK__COLLECTION:
				return getCollection();
			case BPCPackage.BLOCK__BLOCK:
				return getBlock();
			case BPCPackage.BLOCK__RELATION:
				return getRelation();
			case BPCPackage.BLOCK__BASE_CLASS:
				if (resolve) {
					return getBase_Class();
				}
				return basicGetBase_Class();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BPCPackage.BLOCK__PORT:
				getPort().clear();
				getPort().addAll((java.util.Collection<? extends Port>)newValue);
				return;
			case BPCPackage.BLOCK__CONNECTOR:
				getConnector().clear();
				getConnector().addAll((java.util.Collection<? extends Connector>)newValue);
				return;
			case BPCPackage.BLOCK__COLLECTION:
				getCollection().clear();
				getCollection().addAll((java.util.Collection<? extends Collection>)newValue);
				return;
			case BPCPackage.BLOCK__BLOCK:
				getBlock().clear();
				getBlock().addAll((java.util.Collection<? extends Block>)newValue);
				return;
			case BPCPackage.BLOCK__RELATION:
				getRelation().clear();
				getRelation().addAll((java.util.Collection<? extends Relation>)newValue);
				return;
			case BPCPackage.BLOCK__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BPCPackage.BLOCK__PORT:
				getPort().clear();
				return;
			case BPCPackage.BLOCK__CONNECTOR:
				getConnector().clear();
				return;
			case BPCPackage.BLOCK__COLLECTION:
				getCollection().clear();
				return;
			case BPCPackage.BLOCK__BLOCK:
				getBlock().clear();
				return;
			case BPCPackage.BLOCK__RELATION:
				getRelation().clear();
				return;
			case BPCPackage.BLOCK__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BPCPackage.BLOCK__PORT:
				return !getPort().isEmpty();
			case BPCPackage.BLOCK__CONNECTOR:
				return !getConnector().isEmpty();
			case BPCPackage.BLOCK__COLLECTION:
				return !getCollection().isEmpty();
			case BPCPackage.BLOCK__BLOCK:
				return !getBlock().isEmpty();
			case BPCPackage.BLOCK__RELATION:
				return !getRelation().isEmpty();
			case BPCPackage.BLOCK__BASE_CLASS:
				return base_Class != null;
		}
		return super.eIsSet(featureID);
	}

} //BlockImpl
