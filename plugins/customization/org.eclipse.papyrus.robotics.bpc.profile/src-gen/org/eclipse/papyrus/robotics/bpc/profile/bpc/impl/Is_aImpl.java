/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Is_a;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is a</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Is_aImpl extends RelationImpl implements Is_a {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Is_aImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BPCPackage.Literals.IS_A;
	}

} //Is_aImpl
