/**
 * Copyright (c) 2017 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
package org.eclipse.papyrus.robotics.bpc.profile.bpc.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.ReificationMetaData;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reification Meta Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ReificationMetaDataImpl#getParents_eref_name <em>Parents eref name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ReificationMetaDataImpl#getEntity <em>Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReificationMetaDataImpl extends MinimalEObjectImpl.Container implements ReificationMetaData {
	/**
	 * The default value of the '{@link #getParents_eref_name() <em>Parents eref name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParents_eref_name()
	 * @generated
	 * @ordered
	 */
	protected static final String PARENTS_EREF_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParents_eref_name() <em>Parents eref name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParents_eref_name()
	 * @generated
	 * @ordered
	 */
	protected String parents_eref_name = PARENTS_EREF_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEntity() <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntity()
	 * @generated
	 * @ordered
	 */
	protected Entity entity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReificationMetaDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BPCPackage.Literals.REIFICATION_META_DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getParents_eref_name() {
		return parents_eref_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParents_eref_name(String newParents_eref_name) {
		String oldParents_eref_name = parents_eref_name;
		parents_eref_name = newParents_eref_name;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.REIFICATION_META_DATA__PARENTS_EREF_NAME, oldParents_eref_name, parents_eref_name));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Entity getEntity() {
		if (entity != null && entity.eIsProxy()) {
			InternalEObject oldEntity = (InternalEObject)entity;
			entity = (Entity)eResolveProxy(oldEntity);
			if (entity != oldEntity) {
				if (eNotificationRequired()) {
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BPCPackage.REIFICATION_META_DATA__ENTITY, oldEntity, entity));
				}
			}
		}
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entity basicGetEntity() {
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEntity(Entity newEntity) {
		Entity oldEntity = entity;
		entity = newEntity;
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, BPCPackage.REIFICATION_META_DATA__ENTITY, oldEntity, entity));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BPCPackage.REIFICATION_META_DATA__PARENTS_EREF_NAME:
				return getParents_eref_name();
			case BPCPackage.REIFICATION_META_DATA__ENTITY:
				if (resolve) {
					return getEntity();
				}
				return basicGetEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BPCPackage.REIFICATION_META_DATA__PARENTS_EREF_NAME:
				setParents_eref_name((String)newValue);
				return;
			case BPCPackage.REIFICATION_META_DATA__ENTITY:
				setEntity((Entity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BPCPackage.REIFICATION_META_DATA__PARENTS_EREF_NAME:
				setParents_eref_name(PARENTS_EREF_NAME_EDEFAULT);
				return;
			case BPCPackage.REIFICATION_META_DATA__ENTITY:
				setEntity((Entity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BPCPackage.REIFICATION_META_DATA__PARENTS_EREF_NAME:
				return PARENTS_EREF_NAME_EDEFAULT == null ? parents_eref_name != null : !PARENTS_EREF_NAME_EDEFAULT.equals(parents_eref_name);
			case BPCPackage.REIFICATION_META_DATA__ENTITY:
				return entity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) {
			return super.toString();
		}

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (parents_eref_name: "); //$NON-NLS-1$
		result.append(parents_eref_name);
		result.append(')');
		return result.toString();
	}

} //ReificationMetaDataImpl
