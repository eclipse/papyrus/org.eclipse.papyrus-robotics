/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * BASED ON THE WORK OF
 * 		Jeremie Tatibouet (CEA LIST) <jeremie.tatibouet@cea.fr>
 * BASED ON THE CODE IN
 * 		org.eclipse.papyrus.moka.fmi.ui.commands.DropFMUAsPartCommand
 * 
 * Adapted for use within RobMoSys by:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial Adaptation to RobMoSys
 *****************************************************************************/

package org.eclipse.papyrus.robotics.diagrams.dnd.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetBoundsCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.uml.diagram.composite.custom.edit.command.CreateViewCommand;
import org.eclipse.papyrus.uml.diagram.composite.part.UMLVisualIDRegistry;
import org.eclipse.papyrus.uml.diagram.composite.providers.UMLElementTypes;
import org.eclipse.papyrus.uml.tools.utils.NamedElementUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class DropComponentDefinitionAsComponentInstanceCommand extends AbstractTransactionalCommand {

	protected static final int PORT_DEFAULT_HEIGHT = 20;
	protected static final int WIDTH_FACTOR = 11;
	protected static final int MIN_WIDTH = 150;
	protected static final int MIN_HEIGHT = 150;

	protected static final double X_FONT_HEIGHT_FACTOR = 0.9;
	protected static final double Y_FONT_HEIGHT_FACTOR = 0.85;

	protected static final double X_WEST_OFFSET = 8;
	protected static final int Y_LABEL_OFFSET = 20;

	protected Class target;
	protected Class source;
	protected DropObjectsRequest request;
	protected Property newPart;
	protected GraphicalEditPart targetGraphicalEditPart;

	protected View targetView;

	protected TransactionalEditingDomain domain;

	enum PositionKind {
		EAST, WEST
	};

	public DropComponentDefinitionAsComponentInstanceCommand(DropObjectsRequest request, TransactionalEditingDomain domain,
			Class containerClass, Class sourceType, GraphicalEditPart targetEditPart, View targetView) {
		super(domain, "Create a composite part (ComponentInstance) from ComponentDefinition and create graphical view", //$NON-NLS-1$
				getWorkspaceFiles(containerClass));
		this.target = containerClass;
		this.source = sourceType;
		this.request = request;
		this.targetGraphicalEditPart = targetEditPart;
		this.targetView = targetView;
		this.domain = domain;
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		// create a new part
		createNewPart();
		// apply ActivityInstance stereotype to the part
		StereotypeUtil.apply(newPart, ComponentInstance.class);
		
		createGraphicalViews();

		request.setResult(newPart);
		return CommandResult.newOKCommandResult(newPart);
	}

	protected void createNewPart() {
		String partName = source.getName();	
		newPart = target.createOwnedAttribute(partName, source);
		newPart.setName(NamedElementUtil.getDefaultNameWithIncrementFromBase(partName,target.getOwnedAttributes()));
		newPart.setAggregation(AggregationKind.COMPOSITE_LITERAL);
	}

	protected void createGraphicalViews() {
		List<Port> allPorts = collectPorts(newPart);

		int figureWidth = Math.max(MIN_WIDTH, WIDTH_FACTOR * newPart.getName().length());

		int maxPortOnOneSide = allPorts.size() % 2 == 0 ? allPorts.size() / 2 : (allPorts.size() / 2) + 1;
		int figureHeight = Math.max(MIN_HEIGHT, (2 * maxPortOnOneSide + 1) * PORT_DEFAULT_HEIGHT);

		ViewDescriptor descriptor = new ViewDescriptor(new EObjectAdapter(newPart), Node.class,
				((IHintedType) UMLElementTypes.getElementType(UMLVisualIDRegistry.getNodeVisualID(targetView, newPart)))
				.getSemanticHint(),
				targetGraphicalEditPart.getDiagramPreferencesHint());
		CreateViewCommand createCommand = new CreateViewCommand(domain, descriptor, targetView);

		try {
			createCommand.execute(null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		Point location = request.getLocation().getCopy();
		targetGraphicalEditPart.getContentPane().translateToRelative(location);
		Dimension dimension = new Dimension(figureWidth, figureHeight);
		Rectangle partRectangle = new Rectangle(location, dimension);
		SetBoundsCommand setBoundsCommand = new SetBoundsCommand(domain, "move", //$NON-NLS-1$
				(IAdaptable) createCommand.getCommandResult().getReturnValue(), partRectangle);

		try {
			setBoundsCommand.execute(null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		createPorts(allPorts.subList(0, maxPortOnOneSide), allPorts.subList(maxPortOnOneSide,allPorts.size()), partRectangle, (IAdaptable) createCommand.getCommandResult().getReturnValue());

	}

	private void createPorts(List<Port> inPorts, List<Port> outPorts, Rectangle rectangle, IAdaptable figureAdapter) {

		View view = (View) figureAdapter.getAdapter(View.class);

		for (int index = 0; index < inPorts.size(); index++) {
			Port port = inPorts.get(index);
			Point proposedLocation = new Point(0, (index + 1) * rectangle.height / (inPorts.size() + 1));
			createPortView(port, proposedLocation, view, PositionKind.EAST);
		}

		for (int index = 0; index < outPorts.size(); index++) {
			Port port = outPorts.get(index);
			Point proposedLocation = new Point(rectangle.width, (index + 1) * rectangle.height / (outPorts.size() + 1));
			createPortView(port, proposedLocation, view, PositionKind.WEST);

		}

	}

	private void createPortView(Port port, Point location, View view, PositionKind position) {
		ViewDescriptor descriptor = new ViewDescriptor(new EObjectAdapter(port), Node.class,
				((IHintedType) UMLElementTypes.getElementType(UMLVisualIDRegistry.getNodeVisualID(view, port))).getSemanticHint(),
				targetGraphicalEditPart.getDiagramPreferencesHint());

		CreateViewCommand createCommand = new CreateViewCommand(domain, descriptor, view);

		try {
			createCommand.execute(null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		SetBoundsCommand setBoundsCommand = new SetBoundsCommand(domain, "move", //$NON-NLS-1$
				(IAdaptable) createCommand.getCommandResult().getReturnValue(), location);

		try {
			setBoundsCommand.execute(null, null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	private List<Port> collectPorts(Property newPart) {
		List<Port> ret = new ArrayList<Port>();
		Class partType = (Class) newPart.getType();
		for (Port port : partType.getOwnedPorts()) {
			if (StereotypeUtil.isApplied((Port) port, ComponentPort.class)) {
				ret.add(port);
			}
		}
		return ret;
	}
}
