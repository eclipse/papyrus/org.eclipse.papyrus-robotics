package org.eclipse.papyrus.robotics.diagrams.dnd.strategies;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.papyrus.infra.gmfdiag.dnd.strategy.TransactionalDropStrategy;
import org.eclipse.papyrus.robotics.diagrams.dnd.Activator;
import org.eclipse.papyrus.robotics.diagrams.dnd.commands.DropComponentDefinitionAsComponentInstanceCommand;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.graphics.Image;
import org.eclipse.uml2.uml.Class;

public class ComponentDefinitionToComponentInstanceDropStrategy extends TransactionalDropStrategy {

	public static String ID =Activator.PLUGIN_ID + ".strategies.DropComponentDefinitionAsComponentInstance"; //$NON-NLS-1$

	@Override
	public String getLabel() {
		return "Drop the ComponentDefinition item as ComponentInstance"; //$NON-NLS-1$
	}

	@SuppressWarnings("nls")
	@Override
	public String getDescription() {
		return "Drop a model item (class) stereotyped as ComponentDefinition into the assembly diagram (composite diagram) of a System (class) used by system builders."
				+ " This will create a new Part, typed by the dropped classifier."
                + " The new Part into the destination class will be stereotyped as ComponentInstance.";
	}

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public String getID() {
		return ID;
	}

	@Override
	public int getPriority() {
		return 0;
	}

	@Override
	protected Command doGetCommand(Request request, EditPart targetEditPart) {
		if (request instanceof DropObjectsRequest) {
			DropObjectsRequest dropRequest = (DropObjectsRequest) request;
			EObject targetSemanticElement = getTargetSemanticElement(targetEditPart);
			if (
					(dropRequest.getLocation() != null) &&
                    (targetEditPart instanceof GraphicalEditPart) &&
                    (targetSemanticElement instanceof Class) && (StereotypeUtil.isApplied((Class) targetSemanticElement, System.class))
               ) {
				final GraphicalEditPart targetGraphicalEditPart = (GraphicalEditPart) targetEditPart;
				final Class targetSystem = (Class) targetSemanticElement;
				List<EObject> sourceElements = getSourceEObjects(request);
				// The only supported case is "Drop a single ComponentDefinition into a System"
                if (sourceElements.size() == 1) {
                	final EObject sourceElement = sourceElements.get(0);
                	if ( (sourceElement instanceof Class) && (StereotypeUtil.isApplied((Class) sourceElement, ComponentDefinition.class)) ) {
                		final Class sourceType = (Class) sourceElement;
                		Command resultCommand = new ICommandProxy(
                					new DropComponentDefinitionAsComponentInstanceCommand(
                							dropRequest,
                							getTransactionalEditingDomain(targetGraphicalEditPart),
                							targetSystem,
                							sourceType,
                							targetGraphicalEditPart, getTargetView(targetGraphicalEditPart)
                						)
                					);
                		return resultCommand;
                	}
                }
			}
		}
		return null;
	}

}
