/*****************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (based on code from Francois Le Fevre)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.css;

import org.eclipse.e4.ui.css.core.engine.CSSEngine;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.gmfdiag.css.engine.ExtendedCSSEngine;
import org.eclipse.papyrus.infra.gmfdiag.css.provider.IPapyrusElementProvider;
import org.w3c.dom.Element;

/**
 * An IElementProvider for RobMoSys-specific CSS concepts
 *
 * Provides a specialization of GMFElementAdapter for RobMoSys elements
 */
@SuppressWarnings("restriction")
public class GMFRMSElementProvider implements IPapyrusElementProvider {

	@Override
	public Element getElement(Object element, CSSEngine engine) {

		if (!(element instanceof View)) {
			throw new IllegalArgumentException("Unknown element: " + element);//$NON-NLS-1$
		}

		if (!(engine instanceof ExtendedCSSEngine)) {
			throw new IllegalArgumentException("Invalid CSS Engine: " + engine);//$NON-NLS-1$
		}

		return new GMFRMSElementAdapter((View) element, (ExtendedCSSEngine) engine);
	}

	/**
	 * @see org.eclipse.papyrus.infra.gmfdiag.css.provider.IPapyrusElementProvider#getPrimaryView(org.eclipse.emf.ecore.EObject)
	 *
	 * @param notationElement
	 * @return the primary view
	 */
	@Override
	public View getPrimaryView(EObject notationElement) {
		return CSSDOMRMSSemanticElementHelper.getInstance().findPrimaryView(notationElement);
	}
}
