package org.eclipse.papyrus.robotics.safety;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.profile.esfcore.ICustomUUID;

public class RoboticUUID implements ICustomUUID {

	public boolean showUUID(Element element) {
		return getUUID(element) == null;
	}

	public String getUUID(Element element) {
		Entity entity = UMLUtil.getStereotypeApplication(element, Entity.class);
		if (entity != null) {
			return entity.getInstance_uid();
		}
		return null;
	}
}
