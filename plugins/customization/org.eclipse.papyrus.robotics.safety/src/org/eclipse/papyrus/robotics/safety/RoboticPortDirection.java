package org.eclipse.papyrus.robotics.safety;

import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.profile.robotics.commpattern.CommunicationPattern;
import org.eclipse.uml2.uml.Port;
import org.polarsys.esf.core.profile.esfarchitectureconcepts.IGetPortDirection;
import org.polarsys.esf.esfarchitectureconcepts.SDirection;

public class RoboticPortDirection implements IGetPortDirection {

	@Override
	public SDirection getSDirectionLAnalysis(Port port) {
		CommunicationPattern pattern = InteractionUtils.getCommunicationPattern(port);
		
		if (InteractionUtils.isPush(pattern) || InteractionUtils.isPubSub(pattern))  {
			if (port.getProvideds().size() > 0 && port.getRequireds().size() > 0) {
				return SDirection.INOUT;
			} else if (port.getProvideds().size() > 0) {
				return SDirection.OUT;
			} else if (port.getRequireds().size() > 0) {
				return SDirection.IN;
			}
		}
		
		else if (InteractionUtils.isSend(pattern)) {
			if (port.getProvideds().size() > 0 && port.getRequireds().size() > 0) {
				return SDirection.INOUT;
			} else if (port.getProvideds().size() > 0) {
				return SDirection.IN;
			} else if (port.getRequireds().size() > 0) {
				return SDirection.OUT;
			}
		}
		return SDirection.UNDEFINED;
	}

}
