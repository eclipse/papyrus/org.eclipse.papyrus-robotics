/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.EClassGraphicalContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.UMLPackage;

public class EventCommPatternAdvice extends AbstractEditHelperAdvice {

	/**
	 * qualified name of the event pattern within the robotics library
	 */
	public static final String QNAME = "robotics::commpatterns::Event"; //$NON-NLS-1$

	/**
	 * Prefix for default name of the configured service definition
	 */
	public static final String EVENT_PREFIX = "E_"; //$NON-NLS-1$

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      configure a service definition for the event interaction pattern
	 */
	@SuppressWarnings("nls")
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Event pattern configuration command");
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Classifier)) {
			return super.getAfterConfigureCommand(request);
		}
		Classifier classifier = (Classifier) newElement;

		RoboticsContentProvider cp = new RoboticsContentProvider(classifier,
				new EClassGraphicalContentProvider(classifier, UMLPackage.eINSTANCE.getDataType()),
				CommunicationObject.class, FileExtensions.SERVICEDEF_UML);

		EnhancedPopupMenu popupMenuEvent = MenuHelper.createPopupMenu(cp, "Choose CO for event", false);

		Object menuResult = null;
		if (popupMenuEvent.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuEvent.getSubResult();
		}
		final DataType event = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (event == null) {
			return new CancelCommand(classifier);
		}
	
		EnhancedPopupMenu popupMenuParam = MenuHelper.createPopupMenu(cp, "Choose CO for parameter", false);

		if (popupMenuParam.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuParam.getSubResult();
		}
		final DataType parameter = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (parameter == null) {
			return new CancelCommand(classifier);
		}
	
		EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(cp, "Choose CO for state", false);

		if (popupMenuState.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuState.getSubResult();
		}
		final DataType state = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (state == null) {
			return new CancelCommand(classifier);
		}
	
		RecordingCommand templateBinding = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
			@Override
			protected void doExecute() {

				Classifier eventCommPattern = AdviceUtil.getPattern(classifier, QNAME);
				if (eventCommPattern != null) {
					TemplateSignature signature = eventCommPattern.getOwnedTemplateSignature();
					TemplateBinding binding = classifier.createTemplateBinding(signature);

					String name = EVENT_PREFIX.substring(0, 1);
					// loop on template parameters;
					for (TemplateParameter tp : signature.getOwnedParameters()) {

						TemplateParameterSubstitution substitution = binding.createParameterSubstitution();
						substitution.setFormal(tp);

						// now obtain actual from user
						String formalName = ((NamedElement) tp.getParameteredElement()).getName();
						// code is based on assumption that the formal parameters are "named-elements" whose name is not null
						// (which is valid for the RobMoSys library)
						if (formalName.equals("Event")) {
							substitution.setActual(event);
							name += "_" + event.getName();
						} else if (formalName.equals("Parameter")) {
							substitution.setActual(parameter);
							name += "_" + parameter.getName();
						} else if (formalName.equals("State")) {
							substitution.setActual(state);
							name += "_" + state.getName();
						}
					}
					classifier.setName(name);
				}
				// simplification: use directly ServiceDefinition instead of CommunicationPatternDefinition
				StereotypeUtil.apply(classifier, ServiceDefinition.class);
			}
		};

		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(templateBinding));

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

}
