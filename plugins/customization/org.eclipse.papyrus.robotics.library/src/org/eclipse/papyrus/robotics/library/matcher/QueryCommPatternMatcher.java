/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.matcher;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementMatcher;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.robotics.library.advice.QueryCommPatternAdvice;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TemplateBinding;

public class QueryCommPatternMatcher implements IElementMatcher {

	@Override
	public boolean matches(EObject eObject) {
		if (eObject instanceof Class) {
			Class clazz = (Class) eObject;
			if (clazz.eResource() != null) {
				NamedElement queryCommPatternNE = ElementUtils.getQualifiedElementFromRS(clazz, QueryCommPatternAdvice.QNAME);

				for (TemplateBinding binding : clazz.getTemplateBindings()) {
					if (binding.getTargets().contains(queryCommPatternNE)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
