/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.matcher;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementMatcher;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;

public abstract class AbstractTypedElementMatcher implements IElementMatcher {

	public boolean matches(EObject eObject) {
		if (eObject instanceof Property) {
			Property property = (Property) eObject;
			Type type = property.getType();
			if (type != null) {
				String qName = type.getQualifiedName();
				if (qName != null) {
					return checkType(qName);
				}
			}
        }
        return false;
	}

	protected abstract boolean checkType(String qName);
}
