/*****************************************************************************
 * Copyright (c) 2019, 2021, 2023 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581690
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;

public class QueryCommPatternAdvice extends AbstractQueryPatternAdvice {

	@Override
	public void applyServiceStereotype(Classifier cl) {
		StereotypeUtil.apply(cl, ServiceDefinition.class);
	}

}
