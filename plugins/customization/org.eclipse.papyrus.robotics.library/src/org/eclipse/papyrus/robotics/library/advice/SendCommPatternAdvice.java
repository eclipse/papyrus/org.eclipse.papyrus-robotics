/*****************************************************************************
 * Copyright (c) 2019, 2021  CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.EClassGraphicalContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.UMLPackage;

public class SendCommPatternAdvice extends AbstractEditHelperAdvice {

	/**
	 * qualified name of the send pattern within the robotics library
	 */
	public static final String QNAME = "robotics::commpatterns::Send"; //$NON-NLS-1$

	/**
	 * Prefix for default name of the configured service definition
	 */
	public static final String PREFIX = "S_"; //$NON-NLS-1$

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      configure a service definition for the send interaction pattern
	 */
	@SuppressWarnings("nls")
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Send pattern configuration command");
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Classifier)) {
			return super.getAfterConfigureCommand(request);
		}
		Classifier classifier = (Classifier) newElement;

		RoboticsContentProvider cp = new RoboticsContentProvider(classifier,
				new EClassGraphicalContentProvider(classifier, UMLPackage.eINSTANCE.getDataType()),
				CommunicationObject.class, FileExtensions.SERVICEDEF_UML);

		EnhancedPopupMenu popupMenu = MenuHelper.createPopupMenu(cp, "Choose CO for message", false);

		Object menuResult = null;
		if (popupMenu.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenu.getSubResult();
		}
		final DataType message = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (message == null) {
			return new CancelCommand(classifier);
		}

		RecordingCommand templateBinding = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
			@Override
			protected void doExecute() {

				Classifier sendCommPattern = AdviceUtil.getPattern(classifier, QNAME);
				if (sendCommPattern != null) {
					TemplateSignature signature = sendCommPattern.getOwnedTemplateSignature();
					TemplateBinding binding = classifier.createTemplateBinding(signature);

					// pattern has exactly one template parameter
					TemplateParameter parameter = signature.getOwnedParameters().get(0);

					TemplateParameterSubstitution substitution = binding.createParameterSubstitution();
					substitution.setFormal(parameter);

					// now obtain actual from user (code is based on assumption that there is a single message parameter)
					substitution.setActual(message);
					classifier.setName(PREFIX + message.getName());
				}
				// simplification: use directly ServiceDefinition instead of CommunicationPatternDefinition
				StereotypeUtil.apply(classifier, ServiceDefinition.class);
			}
		};

		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(templateBinding));

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}
}
