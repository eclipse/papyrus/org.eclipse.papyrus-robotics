/*****************************************************************************
 * Copyright (c) 2023 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from Ansgar Radermacher) - Bug #581690
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.EClassGraphicalContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.UMLPackage;

public abstract class AbstractQueryPatternAdvice extends AbstractEditHelperAdvice {

	/**
	 * qualified name of the query pattern within the robotics library
	 */
	public static final String QNAME = "robotics::commpatterns::Query"; //$NON-NLS-1$

    /**
	 * Prefix for default name of the configured service definition
	 */
	public static final String PREFIX = "Q_"; //$NON-NLS-1$

	/**
	 * 
	 */
	public abstract void applyServiceStereotype(Classifier cl);

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      configure a service definition for the query interaction pattern
	 */
	@SuppressWarnings("nls")
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Query pattern configuration command");
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Classifier)) {
			return super.getAfterConfigureCommand(request);
		}
		Classifier classifier = (Classifier) newElement;

		RoboticsContentProvider cp = new RoboticsContentProvider(classifier,
				new EClassGraphicalContentProvider(classifier, UMLPackage.eINSTANCE.getDataType()),
				CommunicationObject.class, FileExtensions.SERVICEDEF_UML);

		EnhancedPopupMenu popupMenuRequest = MenuHelper.createPopupMenu(cp, "Choose CO for request", false);

		Object menuResult = null;
		if (popupMenuRequest.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuRequest.getSubResult();
		}
		final DataType _request = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (_request == null) {
			return new CancelCommand(classifier);
		}
	
		EnhancedPopupMenu popupMenuResponse = MenuHelper.createPopupMenu(cp, "Choose CO for response", false);

		if (popupMenuResponse.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuResponse.getSubResult();
		}
		final DataType response = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (response == null) {
			return new CancelCommand(classifier);
		}
	
		RecordingCommand templateBinding = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
			@Override
			protected void doExecute() {

				Classifier queryCommPattern = AdviceUtil.getPattern(classifier, QNAME);
				if (queryCommPattern != null) {
					TemplateSignature signature = queryCommPattern.getOwnedTemplateSignature();
					TemplateBinding binding = classifier.createTemplateBinding(signature);

					String name = PREFIX.substring(0, 1);
					// loop on template parameters;
					for (TemplateParameter parameter : signature.getOwnedParameters()) {

						TemplateParameterSubstitution substitution = binding.createParameterSubstitution();
						substitution.setFormal(parameter);

						// now obtain actual from user (code is based on assumption that there is a single message parameter)
						String formalName = ((NamedElement) parameter.getParameteredElement()).getName();
						// code is based on assumption that the formal parameters are "named-elements" whose name is not null
						// (which is valid for the RobMoSys library)
						if (formalName.equals("Request")) {
							substitution.setActual(_request);
							name += "_" + _request.getName();
						}
						else if (formalName.equals("Response")) {
							substitution.setActual(response);
							name += "_" + response.getName();
						}
					}
					classifier.setName(name);
				}
				// simplification: use directly ServiceDefinition instead of CommunicationPatternDefinition
				applyServiceStereotype(classifier);
			}
		};

		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(templateBinding));

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}
}
