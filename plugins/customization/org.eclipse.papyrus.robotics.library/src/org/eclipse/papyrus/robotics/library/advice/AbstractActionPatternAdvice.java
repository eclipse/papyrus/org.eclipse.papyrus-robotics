/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.EClassGraphicalContentProvider;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.commobject.CommunicationObject;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.UMLPackage;

public abstract class AbstractActionPatternAdvice extends AbstractEditHelperAdvice {

	/**
	 * qualified name of the action pattern within the RobMoSys library
	 */
	public static final String QNAME = "robotics::commpatterns::Action"; //$NON-NLS-1$

	/**
	 * Prefix for default name of the configured service definition
	 */
	public static final String PREFIX = "A_"; //$NON-NLS-1$

	/**
	 * 
	 */
	public abstract void applyServiceStereotype(Classifier cl);

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      configure a service definition for the action interaction pattern
	 */
	@SuppressWarnings("nls")
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Action pattern configuration command");
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Classifier)) {
			return super.getAfterConfigureCommand(request);
		}
		Classifier classifier = (Classifier) newElement;

		RoboticsContentProvider cp = new RoboticsContentProvider(classifier,
				new EClassGraphicalContentProvider(classifier, UMLPackage.eINSTANCE.getDataType()),
				CommunicationObject.class, FileExtensions.SERVICEDEF_UML);

		EnhancedPopupMenu popupMenuEvent = MenuHelper.createPopupMenu(cp, "Choose CO for goal", false);

		Object menuResult = null;
		if (popupMenuEvent.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuEvent.getSubResult();
		}
		final DataType goal = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (goal == null) {
			return new CancelCommand(classifier);
		}

		EnhancedPopupMenu popupMenuParam = MenuHelper.createPopupMenu(cp, "Choose CO for response", false);

		if (popupMenuParam.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuParam.getSubResult();
		}
		final DataType response = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (response == null) {
			return new CancelCommand(classifier);
		}

		EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(cp, "Choose CO for feedback", false);

		if (popupMenuState.show(Display.getDefault().getActiveShell())) {
			menuResult = popupMenuState.getSubResult();
		}
		final DataType feedback = menuResult instanceof DataType ? (DataType) menuResult : null;
		if (feedback == null) {
			return new CancelCommand(classifier);
		}

		RecordingCommand templateBinding = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
			@Override
			protected void doExecute() {

				Classifier eventCommPattern = AdviceUtil.getPattern(classifier, QNAME);
				if (eventCommPattern != null) {
					TemplateSignature signature = eventCommPattern.getOwnedTemplateSignature();
					TemplateBinding binding = classifier.createTemplateBinding(signature);

					String name = PREFIX.substring(0, 1);
					// loop on template parameters;
					for (TemplateParameter tp : signature.getOwnedParameters()) {

						TemplateParameterSubstitution substitution = binding.createParameterSubstitution();
						substitution.setFormal(tp);

						// now obtain actual from user
						String formalName = ((NamedElement) tp.getParameteredElement()).getName();
						// code is based on assumption that the formal parameters are "named-elements" whose name is not null
						// (which is valid for the RobMoSys library)
						if (formalName.equals("Goal")) {
							substitution.setActual(goal);
							name += "_" + goal.getName();
						} else if (formalName.equals("Response")) {
							substitution.setActual(response);
							name += "_" + response.getName();
						} else if (formalName.equals("Feedback")) {
							substitution.setActual(feedback);
							name += "_" + feedback.getName();
						}
					}
					classifier.setName(name);
				}
				// simplification: use directly ServiceDefinition instead of CommunicationPatternDefinition
				applyServiceStereotype(classifier);
			}
		};

		compositeCommand.add(EMFtoGMFCommandWrapper.wrap(templateBinding));

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

}
