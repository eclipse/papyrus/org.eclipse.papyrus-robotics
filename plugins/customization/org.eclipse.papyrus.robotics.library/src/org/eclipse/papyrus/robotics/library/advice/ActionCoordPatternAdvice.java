/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.papyrus.robotics.profile.robotics.services.CoordinationService;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Classifier;

public class ActionCoordPatternAdvice extends AbstractActionPatternAdvice {

	@Override
	public void applyServiceStereotype(Classifier cl) {
		StereotypeUtil.apply(cl, CoordinationService.class);
	}

}
