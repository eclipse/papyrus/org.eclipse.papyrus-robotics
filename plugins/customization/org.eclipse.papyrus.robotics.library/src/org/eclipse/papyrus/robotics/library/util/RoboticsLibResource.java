/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher
 *  	- Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.robotics.library.util;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get informations about robotics library resources
 *
 */
public final class RoboticsLibResource {

	public static final String ROBOTICS_LIB_PATHMAP = "pathmap://ROBOTICS_LIBRARIES/"; //$NON-NLS-1$	
	
	public static final String ROBOTICS_LIB_PATH = ROBOTICS_LIB_PATHMAP + "robotics.library.uml"; //$NON-NLS-1$

	public static final URI ROBOTICS_LIB_URI = URI.createURI(ROBOTICS_LIB_PATH);
}
