/*****************************************************************************
 * Copyright (c) 2019 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.library.advice;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.robotics.library.util.RoboticsLibResource;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

public class AdviceUtil {
	/**
	 * Retrieve the communication pattern from the resource set, warn, if not found
	 *
	 * @param anElement
	 *            an Element within the resource set
	 * @param patternQN
	 *            the qualified name of the communication pattern
	 * @return the communication, if found, null otherwise
	 */
	public static Classifier getPattern(Element anElement, String patternQN) {
		Classifier pattern = getPatternWoMessage(anElement, patternQN);
		if (pattern == null) {
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					MessageDialog.openWarning(Display.getCurrent().getActiveShell(),
							"Can not find communication pattern", //$NON-NLS-1$
							String.format("Cannot find communication pattern %s.", patternQN)); //$NON-NLS-1$
				}
			});
		}
		return pattern;
	}

	/**
	 * Get pattern, but do not retry or invoke a message if not found
	 *
	 * @param anElement
	 *            an Element within the resource set
	 * @param patternQN
	 *            the qualified name of the communication pattern
	 * @return the communication, if found, null otherwise
	 */
	public static Classifier getPatternWoMessage(Element anElement, String patternQN) {
		NamedElement pattern = ElementUtils.getQualifiedElementFromRS(anElement, RoboticsLibResource.ROBOTICS_LIB_URI, patternQN);
		if (pattern instanceof Classifier) {
			return (Classifier) pattern;
		}
		return null;
	}
}