/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * Open Handler
 * 
 * @author jabier.martinez
 */
public class OpenHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow ww = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IStorage storage = new NullStorage();
		IEditorInput input = new NullEditorInput(storage);
		try {
			ww.getActivePage().openEditor(input, DashboardEditor.ID);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * A fake storage and input editor to open the editor in read only
	 */
	class NullStorage implements IStorage {

		public InputStream getContents() throws CoreException {
			return new ByteArrayInputStream("".getBytes()); //$NON-NLS-1$
		}

		public IPath getFullPath() {
			return null;
		}

		public <T> T getAdapter(Class<T> adapter) {
			return null;
		}

		public String getName() {
			return null;
		}

		public boolean isReadOnly() {
			return true;
		}
	}

	class NullEditorInput implements IEditorInput {
		private IStorage storage;

		NullEditorInput(IStorage storage) {
			this.storage = storage;
		}

		public boolean exists() {
			return true;
		}

		public ImageDescriptor getImageDescriptor() {
			return null;
		}

		public String getName() {
			return storage.getName();
		}

		public IPersistableElement getPersistable() {
			return null;
		}

		public IStorage getStorage() {
			return storage;
		}

		public String getToolTipText() {
			return null;
		}

		public <T> T getAdapter(Class<T> adapter) {
			return null;
		}

	}

}
