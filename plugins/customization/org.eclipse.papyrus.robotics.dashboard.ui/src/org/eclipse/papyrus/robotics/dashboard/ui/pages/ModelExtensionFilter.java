/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 *   Alejandra Ruiz, Tecnalia - Initial implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.pages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

@SuppressWarnings("nls")
public class ModelExtensionFilter extends ViewerFilter {

	private List<String> fTargetExtension;

	public ModelExtensionFilter(String targetExtension) {
		fTargetExtension = new ArrayList<String>();
		fTargetExtension.add(targetExtension);
	}

	public ModelExtensionFilter(List<String> targetExtension) {
		fTargetExtension = new ArrayList<String>();
		fTargetExtension.addAll(targetExtension);
	}

	@Override
	public boolean select(Viewer viewer, Object parent, Object element) {
		if (element instanceof IFile) {
			// return ((IFile)element).getName().toLowerCase().endsWith("." + fTargetExtension);
			for (String extension : fTargetExtension) {
				if (((IFile) element).getName().toLowerCase().contains("." + extension)) {
					return true;
				}
			}
			return false;
		}

		if (element instanceof IProject && !((IProject) element).isOpen())
			return false;

		if (element instanceof IContainer) { // i.e. IProject, IFolder
			try {
				IResource[] resources = ((IContainer) element).members();
				for (IResource resource : resources) {
					if (select(viewer, parent, resource))
						return true;
				}
			} catch (CoreException e) {
				// Ignore
			}
		}

		return false;
	}
}
