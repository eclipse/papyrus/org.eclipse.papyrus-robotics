/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Angel Lopez, Tecnalia - Initial creation
 * 	 Jabier Martinez, Tecnalia - Initial creation
 *   Alejandra Ruiz, Tecnalia - provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.pages;

import java.net.URL;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.papyrus.robotics.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.ros2.reverse.fromfile.ReverseNodeFromSource;
import org.eclipse.papyrus.robotics.wizards.WizardConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.wizards.IWizardDescriptor;
import org.osgi.framework.Bundle;

@SuppressWarnings("nls")
public class ComponentDesignPage {

	public static final String ID = "Component Design";

	public ScrolledForm getPageBody(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		ScrolledForm composite = toolkit.createScrolledForm(container);
		composite.setText("Papyrus4Robotics Compositional Development Process");

		String path_mision = "icons/robotics_component.png";
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		URL url_comp = FileLocator.find(bundle, new Path(path_mision), null);
		ImageDescriptor imageDescComp = ImageDescriptor.createFromURL(url_comp);
		Image compImage = imageDescComp.createImage();
		composite.setImage(compImage);

		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		composite.getBody().setLayout(layout);

		toolkit.createLabel(composite.getBody(), ID);
		toolkit.createLabel(composite.getBody(), "");

		// Section component creation
		Section section1 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
		td.colspan = 2;
		section1.setLayoutData(td);
		section1.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section1.setText("Component Creation");
		section1.setDescription(
				"A component supplier creates software components to offer them as units of composition that provide or require services (service-level) and contain functions.");
		Composite sectionClient = toolkit.createComposite(section1);
		sectionClient.setLayout(new GridLayout());

		P4RProjectModelHyperlink.create(toolkit, sectionClient, "Create a new component definition project",
				PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD),
				WizardConstants.COMPONENT_DEVELOPMENT_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, sectionClient, "Create a new component definition model",
				PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD),
				WizardConstants.COMPONENT_DEVELOPMENT_VIEWPOINT, false);

		ImageHyperlink i3 = toolkit.createImageHyperlink(sectionClient, SWT.WRAP);
		URL urlCode = FileLocator.find(bundle, new Path("icons/Code.png"), null);
		ImageDescriptor imageDescCode = ImageDescriptor.createFromURL(urlCode);
		i3.setImage(imageDescCode.createImage());
		i3.setText("Reverse engineering a component from a C code");
		
		i3.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(final HyperlinkEvent e) {

				ElementTreeSelectionDialog dialogOpenComponent = new ElementTreeSelectionDialog(
						Display.getCurrent().getActiveShell(), new WorkbenchLabelProvider(),
						new BaseWorkbenchContentProvider());
				dialogOpenComponent.addFilter(new ModelExtensionFilter("cpp"));
				dialogOpenComponent.setTitle("Code origin selection");
				dialogOpenComponent.setMessage("Select the C++ file from the tree:");
				dialogOpenComponent.setInput(ResourcesPlugin.getWorkspace().getRoot());
				if (dialogOpenComponent.open() == Window.OK) {
					IFile file = (IFile) dialogOpenComponent.getFirstResult();
					Job job = new Job("Reverse node from file") { //$NON-NLS-1$
						@Override
						protected IStatus run(final IProgressMonitor monitor) {
							ReverseNodeFromSource reverseFromSource =
									new ReverseNodeFromSource(file);
							reverseFromSource.reverseNode(monitor);
							return Status.OK_STATUS;
						}
					};
					job.schedule();
				}
			}
		});

		ImageHyperlink i4 = toolkit.createImageHyperlink(sectionClient, SWT.WRAP);
		i4.setImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ADD));
		i4.setText("Import a component project");

		i4.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(final HyperlinkEvent e) {
				String id = "org.eclipse.ui.wizards.import.ExternalProject";
				IWizardDescriptor descriptor = PlatformUI.getWorkbench().getImportWizardRegistry().findWizard(id);
				IWizard wizard;
				try {
					wizard = descriptor.createWizard();
					WizardDialog wd = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);
					wd.setTitle(wizard.getWindowTitle());
					wd.open();
				} catch (CoreException e1) {
					e1.printStackTrace();
				}
			}
		});
		section1.setClient(sectionClient);

////Section Specify a component
		Section section2 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td2 = new TableWrapData(TableWrapData.FILL_GRAB);
		td2.colspan = 2;
		section2.setLayoutData(td2);
		section2.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section2.setText("Specify a Component");
		section2.setDescription(
				"The component supplier models the component by using existing service definitions and functions. He/she therefore uses models from the roles service designer and function developer using the component development view.");
		Composite section2Client = toolkit.createComposite(section2);
		section2Client.setLayout(new GridLayout());

		Image editCompImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD);
		P4RProjectModelHyperlink.open(toolkit, section2Client, "Edit an existing Component model", editCompImage,
				"compdef.di");

		P4RProjectModelHyperlink.open(toolkit, section2Client, "Show available services (Show a service list)", editCompImage,
				"servicedef");
		section2.setClient(section2Client);

// Section Component Usage
		Section section3 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td3 = new TableWrapData(TableWrapData.FILL_GRAB);
		td3.colspan = 2;
		section3.setLayoutData(td3);
		section3.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section3.setText("Services modeling");
		section3.setDescription(
				"The service designer specified a service which consists of service-properties and a communication-pattern-usage. The communication-pattern-usage selects a certain Communication Pattern with a pattern-specific selection of according number of communicated data-structures");
		Composite section3Client = toolkit.createComposite(section3);
		section3Client.setLayout(new GridLayout());
		
		String path3 = "icons/tree-16.png";
		URL url3 = FileLocator.find(bundle, new Path(path3), null);
		ImageDescriptor imageDesc3 = ImageDescriptor.createFromURL(url3);
		Image image3 = imageDesc3.createImage();
		P4RProjectModelHyperlink.create(toolkit, section3Client, "Service definition project creation", image3,
				WizardConstants.COMPONENT_DEVELOPMENT_VIEWPOINT, true);
		
		section3.setClient(section3Client);

		return composite;
	}

}
