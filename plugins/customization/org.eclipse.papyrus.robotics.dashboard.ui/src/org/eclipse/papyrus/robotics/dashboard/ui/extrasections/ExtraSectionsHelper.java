/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.extrasections;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

public class ExtraSectionsHelper {

	public static final String SECTIONS_EXTENSIONPOINT = "org.eclipse.papyrus.robotics.dashboard.ui.sections"; //$NON-NLS-1$

	public static List<IDashboardSection> getAllExtraDashboardSections() {
		List<IDashboardSection> extraSections = new ArrayList<IDashboardSection>();
		IConfigurationElement[] extensionPoints = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(SECTIONS_EXTENSIONPOINT);
		for (IConfigurationElement extensionPoint : extensionPoints) {
			try {
				extraSections.add((IDashboardSection) extensionPoint.createExecutableExtension("class")); //$NON-NLS-1$
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		return extraSections;
	}

}
