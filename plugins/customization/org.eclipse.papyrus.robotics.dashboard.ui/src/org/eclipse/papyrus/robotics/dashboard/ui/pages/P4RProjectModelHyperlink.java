/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation
 *   Alejandra Ruiz, Tecnalia - provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.pages;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.papyrus.robotics.wizards.wizards.NewRoboticsModelWizard;
import org.eclipse.papyrus.robotics.wizards.wizards.NewRoboticsProjectWizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.wizards.IWizardDescriptor;

@SuppressWarnings("nls")
public class P4RProjectModelHyperlink {

	/**
	 * Create hyperlink for the creation of P4R projects or models
	 * 
	 * @param toolkit
	 * @param sectionClient
	 * @param wizardConstant
	 * @param isProject
	 *            true for project creation, false for model creation
	 */
	public static void create(FormToolkit toolkit, Composite sectionClient, String text, Image image,
			String wizardConstant, boolean isProject) {
		ImageHyperlink i = toolkit.createImageHyperlink(sectionClient, SWT.WRAP);
		i.setImage(image);
		i.setText(text);
		i.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(final HyperlinkEvent e) {
				String id = null;
				if (isProject) {
					id = NewRoboticsProjectWizard.WIZARD_ID;
				} else {
					id = NewRoboticsModelWizard.WIZARD_ID;
				}
				IWizardDescriptor descriptor = PlatformUI.getWorkbench().getNewWizardRegistry().findWizard(id);
				IWizard wizard;
				try {
					wizard = descriptor.createWizard();
					NewRoboticsModelWizard wi = ((NewRoboticsModelWizard) wizard);
					IWorkbench vWorkbench = PlatformUI.getWorkbench();
					wi.init(vWorkbench, StructuredSelection.EMPTY);
					WizardDialog wd = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);
					wd.setTitle(wizard.getWindowTitle());
					wd.create();
					wi.setViewpoint(wizardConstant);
					wd.showPage(wd.getCurrentPage().getNextPage());
					wd.open();
				} catch (CoreException e1) {
					e1.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create hyperlink for opening specific P4R projects or models
	 * 
	 * @param toolkit
	 * @param sectionClient
	 */
	public static void open(FormToolkit toolkit, Composite sectionClient, String text, Image image,
			String fileExtensionConstant) {
		ImageHyperlink i = toolkit.createImageHyperlink(sectionClient, SWT.WRAP);
		i.setImage(image);
		i.setText(text);
		i.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(final HyperlinkEvent e) {

				ElementTreeSelectionDialog dialogOpenComponent = new ElementTreeSelectionDialog(
						Display.getCurrent().getActiveShell(), new WorkbenchLabelProvider(),
						new BaseWorkbenchContentProvider());
				dialogOpenComponent.addFilter(new ModelExtensionFilter(fileExtensionConstant));
				dialogOpenComponent.setTitle("Model Selection");
				dialogOpenComponent.setMessage("Select the element from the tree:");
				dialogOpenComponent.setInput(ResourcesPlugin.getWorkspace().getRoot());
				if (dialogOpenComponent.open() == Window.OK) {
					IFile file = (IFile) dialogOpenComponent.getFirstResult();
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry()
							.getDefaultEditor(file.getName());
					try {
						page.openEditor(new FileEditorInput(file), desc.getId());
					} catch (PartInitException e1) {
						e1.printStackTrace();
					}

				}
			}
		});
	}

	/**
	 * Create hyperlink for opening specific P4R projects or models
	 * 
	 * @param toolkit
	 * @param sectionClient
	 */
	public static void openAndShowMessage(FormToolkit toolkit, Composite sectionClient, String text, Image image,
			String FileExtensionConstant, String msg_text) {
		ImageHyperlink i = toolkit.createImageHyperlink(sectionClient, SWT.WRAP);
		i.setImage(image);
		i.setText(text);
		i.addHyperlinkListener(new HyperlinkAdapter() {

			@Override
			public void linkActivated(final HyperlinkEvent e) {

				ElementTreeSelectionDialog dialogOpenComponent = new ElementTreeSelectionDialog(
						Display.getCurrent().getActiveShell(), new WorkbenchLabelProvider(),
						new BaseWorkbenchContentProvider());
				dialogOpenComponent.addFilter(new ModelExtensionFilter(FileExtensionConstant));
				dialogOpenComponent.setTitle("Model Selection");
				dialogOpenComponent.setMessage("Select the element from the tree:");
				dialogOpenComponent.setInput(ResourcesPlugin.getWorkspace().getRoot());
				if (dialogOpenComponent.open() == Window.OK) {
					IFile file = (IFile) dialogOpenComponent.getFirstResult();
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry()
							.getDefaultEditor(file.getName());
					MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Info", msg_text);
					try {
						page.openEditor(new FileEditorInput(file), desc.getId());
					} catch (PartInitException e1) {
						e1.printStackTrace();
					}

				}
			}
		});
	}
}
