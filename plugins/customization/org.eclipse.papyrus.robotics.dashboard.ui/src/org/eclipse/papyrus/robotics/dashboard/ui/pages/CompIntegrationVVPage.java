/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation
 *   Alejandra Ruiz, Tecnalia - provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.pages;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.wizards.WizardConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.osgi.framework.Bundle;

@SuppressWarnings("nls")
public class CompIntegrationVVPage {

	public static final String ID = "Validation && Verification";

	public ScrolledForm getPageBody(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		ScrolledForm composite = toolkit.createScrolledForm(container);
		composite.setText("Papyrus for Robotics Compositional Development Process");

		String path_vv = "icons/robotics_VV.png";
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		URL url_vv = FileLocator.find(bundle, new Path(path_vv), null);
		ImageDescriptor imageDesc_vv = ImageDescriptor.createFromURL(url_vv);
		Image vvImage = imageDesc_vv.createImage();
		composite.setImage(vvImage);

		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		composite.getBody().setLayout(layout);

		toolkit.createLabel(composite.getBody(), ID);
		toolkit.createLabel(composite.getBody(), "");

//Section eITUS
		Section section2 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td2 = new TableWrapData(TableWrapData.FILL_GRAB);
		td2.colspan = 2;
		section2.setLayoutData(td2);
		section2.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section2.setText("Component's nominal and faulty behaviour definition");
		section2.setDescription("The safety engineer performs a safety assessment");
		Composite sectionClient2 = toolkit.createComposite(section2);
		sectionClient2.setLayout(new GridLayout());

		URL urlHARA = FileLocator.find(bundle, new Path("icons/hazard.png"), null);
		ImageDescriptor imageDescHARA = ImageDescriptor.createFromURL(urlHARA);

		P4RProjectModelHyperlink.create(toolkit, sectionClient2, "Create a new Task-Based HARA project",
				imageDescHARA.createImage(), WizardConstants.TASKBASED_HARA_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, sectionClient2, "Create a new Task-Based HARA model",
				imageDescHARA.createImage(), WizardConstants.TASKBASED_HARA_VIEWPOINT, false);

		ImageHyperlink is2_2 = toolkit.createImageHyperlink(sectionClient2, SWT.WRAP);
		URL urlFault = FileLocator.find(bundle, new Path("icons/fault.gif"), null);
		ImageDescriptor imageDescFault = ImageDescriptor.createFromURL(urlFault);
		is2_2.setImage(imageDescFault.createImage());
		is2_2.setText("Fault Injection experiments definition (eITUS)");
		is2_2.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(final HyperlinkEvent e) {
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Fault Injection",
						"You can create fault injection experiments using the model explorer or the diagrams doing a right-click in the model elements. You will see a Fault Injection menu with the available actions.");
			}
		});

		section2.setClient(sectionClient2);

		return composite;
	}

}
