/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.extrasections;

import org.eclipse.swt.widgets.Composite;

public interface IDashboardSection {

	/**
	 * To be shown in the section title
	 * 
	 * @return name
	 */
	public String getName();

	/**
	 * To be shown in the section description
	 * 
	 * @return description
	 */
	public String getDescription();

	/**
	 * The ID of the page that we want to extend
	 * 
	 * @return page id
	 */
	public String getExtendedPageID();

	/**
	 * This is to create the buttons or the widgets you may want to add. Example:
	 * FormToolkit toolkit = new FormToolkit(container.getDisplay()); ImageHyperlink
	 * i = toolkit.createImageHyperlink(container, SWT.WRAP);
	 * i.setImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD));
	 * i.setText("New action");
	 * 
	 * @param container
	 */
	public void createComposite(Composite container);

}
