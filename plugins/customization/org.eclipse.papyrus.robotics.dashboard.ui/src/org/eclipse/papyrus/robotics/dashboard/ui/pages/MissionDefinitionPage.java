/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation
 *   Alejandra Ruiz, Tecnalia - provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.pages;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.wizards.WizardConstants;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.osgi.framework.Bundle;

@SuppressWarnings("nls")
public class MissionDefinitionPage {

	public static final String ID = "Robot Mission Definition";

	public ScrolledForm getPageBody(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		ScrolledForm composite = toolkit.createScrolledForm(container);
		composite.setText("Papyrus4Robotics Compositional Development Process");

		String path_mision = "icons/robotics_mision.png";
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		URL url_mision = FileLocator.find(bundle, new Path(path_mision), null);
		ImageDescriptor imageDescMision = ImageDescriptor.createFromURL(url_mision);
		Image misionImage = imageDescMision.createImage();
		composite.setImage(misionImage);

		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		composite.getBody().setLayout(layout);

		toolkit.createLabel(composite.getBody(), ID);
		toolkit.createLabel(composite.getBody(), "");

		Section section1 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
		td.colspan = 2;
		section1.setLayoutData(td);
		section1.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section1.setText("Skill Definition");
		section1.setDescription(
				"The Skill designer (a domain expert) is able to define skill definitions modelling the interface of skills on tier 2, not yet deciding how a skill is realized. As a result, the skill definition is any component that realizes it.");
		Composite sectionClient = toolkit.createComposite(section1);
		sectionClient.setLayout(new GridLayout());

		ImageDescriptor imageDescSkill = ImageDescriptor
				.createFromURL(FileLocator.find(bundle, new Path("icons/skills.png"), null));
		Image skillImage = imageDescSkill.createImage();

		P4RProjectModelHyperlink.create(toolkit, sectionClient, "Create a new skills definition project", skillImage,
				WizardConstants.BEHAVIOR_DESIGN_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, sectionClient, "Create a new skills definition model", skillImage,
				WizardConstants.BEHAVIOR_DESIGN_VIEWPOINT, false);

		section1.setClient(sectionClient);

		Section section2 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td2 = new TableWrapData(TableWrapData.FILL_GRAB);
		td2.colspan = 2;
		section2.setLayoutData(td2);
		section2.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section2.setText("Behaviour Definition");
		section2.setDescription(
				"A behaviour tree is capable of modelling task-plots of a robot. Task-plots define sequences of tasks required to achieve certain goals at run-time.");
		Composite section2Client = toolkit.createComposite(section2);
		section2Client.setLayout(new GridLayout());

		ImageDescriptor imageDescBehaviour = ImageDescriptor
				.createFromURL(FileLocator.find(bundle, new Path("icons/tree-16.png"), null));
		Image behaviourImage = imageDescBehaviour.createImage();

		P4RProjectModelHyperlink.create(toolkit, section2Client, "Create a new behaviour tree project", behaviourImage,
				WizardConstants.BEHAVIOR_TREE_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, section2Client, "Create a new behaviour tree model", behaviourImage,
				WizardConstants.BEHAVIOR_TREE_VIEWPOINT, false);

		section2.setClient(section2Client);

		// Services modeling
		Section section3 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td3 = new TableWrapData(TableWrapData.FILL_GRAB);
		td3.colspan = 2;
		section3.setLayoutData(td3);
		section3.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section3.setText("Services modeling");
		section3.setDescription(
				"The service designer defines a Service which consists of service-properties and a communication-pattern-usage. The communication-pattern-usage selects a certain Communication Pattern with a pattern-specific selection of according number of communicated data-structures");
		Composite section3Client = toolkit.createComposite(section3);
		section3Client.setLayout(new GridLayout());

		String path3 = "icons/tree-16.png";
		URL url3 = FileLocator.find(bundle, new Path(path3), null);
		ImageDescriptor imageDesc3 = ImageDescriptor.createFromURL(url3);
		Image image3 = imageDesc3.createImage();

		P4RProjectModelHyperlink.create(toolkit, section3Client, "Create a new services definition project", image3,
				WizardConstants.SERVICE_DESIGN_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, section3Client, "Create a new services definition model", image3,
				WizardConstants.SERVICE_DESIGN_VIEWPOINT, false);

		section3.setClient(section3Client);

		// Risk Assessment
		Section section4 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td4 = new TableWrapData(TableWrapData.FILL_GRAB);
		td4.colspan = 2;
		section4.setLayoutData(td4);
		section4.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section4.setText("Risk Assessment");
		section4.setDescription("The safety engineer performs a risk assessment");
		Composite section4Client = toolkit.createComposite(section4);
		section4Client.setLayout(new GridLayout());

		String path4 = "icons/hazard.png";
		URL url4 = FileLocator.find(bundle, new Path(path4), null);
		ImageDescriptor imageDesc4 = ImageDescriptor.createFromURL(url4);
		Image image4 = imageDesc4.createImage();

		P4RProjectModelHyperlink.create(toolkit, section4Client, "Create a new Task-Based HARA project", image4,
				WizardConstants.TASKBASED_HARA_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, section4Client, "Create a new Task-Based HARA model", image4,
				WizardConstants.TASKBASED_HARA_VIEWPOINT, false);

		section4.setClient(section4Client);

		return composite;
	}

}

