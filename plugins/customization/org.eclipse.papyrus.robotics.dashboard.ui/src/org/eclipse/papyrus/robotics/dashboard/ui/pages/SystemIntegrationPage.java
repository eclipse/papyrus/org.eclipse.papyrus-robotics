/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation
 *   Alejandra Ruiz, Tecnalia - provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui.pages;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.wizards.WizardConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.osgi.framework.Bundle;

@SuppressWarnings("nls")
public class SystemIntegrationPage {
	
	public static final String ID = "System Composition";

	public ScrolledForm getPageBody(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		ScrolledForm composite = toolkit.createScrolledForm(container);
		composite.setText("Papyrus4Robotics Compositional Development Process");		

		String path = "icons/robotics_system.png";
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		URL url = FileLocator.find(bundle, new Path(path), null);
		ImageDescriptor imageDesc = ImageDescriptor.createFromURL(url);
		Image systemImage = imageDesc.createImage();
		composite.setImage(systemImage);
		
		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		composite.getBody().setLayout(layout);
		
//		toolkit.createLabel(composite.getBody(), "System Integration");
		toolkit.createLabel(composite.getBody(), ID);
		toolkit.createLabel(composite.getBody(), "");

		
//Section System Creation
		Section section1 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
		td.colspan = 2;
		section1.setLayoutData(td);
		section1.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section1.setText("System Creation");
		section1.setDescription(
				"The system builder puts together systems from building blocks (i.e. software components).");
		Composite sectionClient = toolkit.createComposite(section1);
		sectionClient.setLayout(new GridLayout());
				
		P4RProjectModelHyperlink.create(toolkit, sectionClient, "Create a new system definition project",
				PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD),
				WizardConstants.SYSTEM_CONFIG_VIEWPOINT, true);

		P4RProjectModelHyperlink.create(toolkit, sectionClient, "Create a new system definition model",
				PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD),
				WizardConstants.SYSTEM_CONFIG_VIEWPOINT, false);


		section1.setClient(sectionClient);
// Section Specify system
		Section section2 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td2 = new TableWrapData(TableWrapData.FILL_GRAB);
		td2.colspan = 2;
		section2.setLayoutData(td2);
		section2.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section2.setText("Specify System");
		section2.setDescription(
				"The system builder selects components (provided by component suppliers) from the ecosystem that performs the needed services. Matchmaking must be made on the basis of offered services and on other properties, e.g. the required accuracy.");
		Composite sectionClient2 = toolkit.createComposite(section2);
		sectionClient2.setLayout(new GridLayout());
		
		P4RProjectModelHyperlink.open(toolkit, sectionClient2, "Show available components", 
				PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD),
				"compdef.di");
		
		P4RProjectModelHyperlink.open(toolkit, sectionClient2, "Edit an existing system model", 
				PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD),
				"system");
		
		section2.setClient(sectionClient2);
		
//Here it should go the assertions but we might use the extension point
		
//Section System usage		
		Section section3 = toolkit.createSection(composite.getBody(),
				Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		TableWrapData td3 = new TableWrapData(TableWrapData.FILL_GRAB);
		td3.colspan = 2;
		section3.setLayoutData(td3);
		section3.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				composite.reflow(true);
			}
		});
		section3.setText("System Usage");
		section3.setDescription(
				"The system builder selects to use the system.");
		Composite sectionClient3 = toolkit.createComposite(section3);
		sectionClient3.setLayout(new GridLayout());
		
		ImageHyperlink is3_1 = toolkit.createImageHyperlink(sectionClient3, SWT.WRAP);
		Image imageCode = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_NEW_WIZARD);
		is3_1.setText("Code generation");
		
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, sectionClient3, "The programmer from the system integrator selects to use the system.", 
				imageCode, ".system", "Right click on the system you want to generade code from, select Robotics menu and then select Generate ROS 2 code");
		
		
		section3.setClient(sectionClient3);
		return composite;
	}
	
}
