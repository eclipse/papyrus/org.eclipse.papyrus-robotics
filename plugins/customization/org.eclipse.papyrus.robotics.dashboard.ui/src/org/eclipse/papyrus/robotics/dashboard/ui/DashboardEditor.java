/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.dashboard.ui;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.dashboard.ui.extrasections.ExtraSectionsHelper;
import org.eclipse.papyrus.robotics.dashboard.ui.extrasections.IDashboardSection;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.CompIntegrationVVPage;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.ComponentDesignPage;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.MissionDefinitionPage;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.SystemIntegrationPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.editor.SharedHeaderFormEditor;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.osgi.framework.Bundle;

/**
 * Dashboard editor
 */
@SuppressWarnings("nls")
public class DashboardEditor extends SharedHeaderFormEditor {

	public static final String ID = "org.eclipse.papyrus.robotics.dashboard.ui.editor"; //$NON-NLS-1$

	Map<String, Integer> pageIdToPageIndexMap = new HashMap<String, Integer>();

	@Override
	protected void addPages() {
		createGlobalPage();
		createPage1();
		createPage2();
		createPage3();
		createPage4();

		List<IDashboardSection> extraSections = ExtraSectionsHelper.getAllExtraDashboardSections();
		for (IDashboardSection extraSection : extraSections) {
			String pageID = extraSection.getExtendedPageID();
			Integer index = pageIdToPageIndexMap.get(pageID);
			if (index == null) {
				System.err.println(String.format("Extra section for P4R Dashboard: %s not found", pageID));
				continue;
			}
			ScrolledForm composite = (ScrolledForm) pages.get(index);
			FormToolkit toolkit = new FormToolkit(composite.getDisplay());

			Section section = toolkit.createSection(composite.getBody(),
					Section.DESCRIPTION | Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
			TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
			td.colspan = 2;
			section.setLayoutData(td);
			section.addExpansionListener(new ExpansionAdapter() {
				public void expansionStateChanged(ExpansionEvent e) {
					composite.reflow(true);
				}
			});
			section.setText(extraSection.getName());
			section.setDescription(extraSection.getDescription());
			Composite sectionClient = toolkit.createComposite(section);
			sectionClient.setLayout(new GridLayout());
			extraSection.createComposite(sectionClient);
			section.setClient(sectionClient);
		}
	}

	private void createPage1() {
		MissionDefinitionPage page = new MissionDefinitionPage();
		int index = addPage(page.getPageBody(getContainer()));
		setPageText(index, MissionDefinitionPage.ID);
		pageIdToPageIndexMap.put(MissionDefinitionPage.ID, index);
	}

	private void createPage2() {
		ComponentDesignPage page = new ComponentDesignPage();
		int index = addPage(page.getPageBody(getContainer()));
		setPageText(index, ComponentDesignPage.ID);
		pageIdToPageIndexMap.put(ComponentDesignPage.ID, index);
	}

	private void createPage3() {
		SystemIntegrationPage page = new SystemIntegrationPage();
		int index = addPage(page.getPageBody(getContainer()));
		setPageText(index, SystemIntegrationPage.ID);
		pageIdToPageIndexMap.put(SystemIntegrationPage.ID, index);
	}

	private void createPage4() {
		CompIntegrationVVPage page = new CompIntegrationVVPage();
		int index = addPage(page.getPageBody(getContainer()));
		setPageText(index, CompIntegrationVVPage.ID);
		pageIdToPageIndexMap.put(CompIntegrationVVPage.ID, index);
	}

	private void createGlobalPage() {
		FormToolkit toolkit = new FormToolkit(getContainer().getDisplay());
		ScrolledForm composite = toolkit.createScrolledForm(getContainer());
		composite.setText("Papyrus for Robotics Compositional Development Process");

		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		composite.getBody().setLayout(layout);

		// space
		toolkit.createLabel(composite.getBody(), "");
		toolkit.createLabel(composite.getBody(), "");

		ImageHyperlink goTo1 = toolkit.createImageHyperlink(composite.getBody(), SWT.CENTER);
		String path = "icons/robotics_mision.png";
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		URL url = FileLocator.find(bundle, new Path(path), null);
		ImageDescriptor imageDesc = ImageDescriptor.createFromURL(url);
		Image goTo1Image = imageDesc.createImage();

		goTo1.setImage(goTo1Image);
		goTo1.setText("Robot Mission Definition");
		goTo1.setUnderlined(false);
		goTo1.addHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				DashboardEditor.this.setActivePage(1);
			}
		});

		toolkit.createLabel(composite.getBody(),
				"Define the services and skills the robot should perform to execute a mission", SWT.WRAP);

		// space
		toolkit.createLabel(composite.getBody(), "");
		toolkit.createLabel(composite.getBody(), "");

		ImageHyperlink goTo2 = toolkit.createImageHyperlink(composite.getBody(), SWT.WRAP);
		String path2 = "icons/robotics_component.png";
		URL url2 = FileLocator.find(bundle, new Path(path2), null);
		ImageDescriptor imageDesc2 = ImageDescriptor.createFromURL(url2);
		Image goTo2Image = imageDesc2.createImage();
		goTo2.setImage(goTo2Image);
		goTo2.setText("Component Design");
		goTo2.setUnderlined(false);
		goTo2.addHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				DashboardEditor.this.setActivePage(2);
			}
		});

		toolkit.createLabel(composite.getBody(),
				"A component supplier (Tier 3) creates software components as units of composition that provide or require services (service-level) and contain functions. He/she models the component by using service definitions and functions. Implement a skill that lifts the abstraction of a component from the service level to the task level",
				SWT.WRAP);

		// space
		toolkit.createLabel(composite.getBody(), "");
		toolkit.createLabel(composite.getBody(), "");

		ImageHyperlink goTo3 = toolkit.createImageHyperlink(composite.getBody(), SWT.WRAP);
		String path3 = "icons/robotics_system.png";
		URL url3 = FileLocator.find(bundle, new Path(path3), null);
		ImageDescriptor imageDesc3 = ImageDescriptor.createFromURL(url3);
		Image goTo3Image = imageDesc3.createImage();
		// Image goTo3Image = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		goTo3.setImage(goTo3Image);
		goTo3.setText("System Composition");
		goTo3.setUnderlined(false);
		goTo3.addHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				DashboardEditor.this.setActivePage(3);
			}
		});

		toolkit.createLabel(composite.getBody(),
				"Define the scope of the system, define the system Architecture, elicit system assertion", SWT.WRAP);

		// space
		toolkit.createLabel(composite.getBody(), "");
		toolkit.createLabel(composite.getBody(), "");

		ImageHyperlink goTo4 = toolkit.createImageHyperlink(composite.getBody(), SWT.WRAP);
		String path4 = "icons/robotics_VV.png";
		URL url4 = FileLocator.find(bundle, new Path(path4), null);
		ImageDescriptor imageDesc4 = ImageDescriptor.createFromURL(url4);
		Image goTo4Image = imageDesc4.createImage();
		// Image goTo4Image = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		goTo4.setImage(goTo4Image);
		goTo4.setText("Validation & Verification");
		goTo4.setUnderlined(false);
		goTo4.addHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				DashboardEditor.this.setActivePage(4);
			}
		});

		toolkit.createLabel(composite.getBody(), "Validate component Contracts and execute Safety/Security Analyses",
				SWT.WRAP);

		int index = addPage(composite);
		setPageText(index, "Global View");
		setPageImage(index, PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_HOME_NAV));
	}

	@Override
	public void doSave(IProgressMonitor monitor) {

	}

	@Override
	public void doSaveAs() {

	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

}
