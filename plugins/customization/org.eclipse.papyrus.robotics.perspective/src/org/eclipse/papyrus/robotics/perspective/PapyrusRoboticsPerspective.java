/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.perspective;

import org.eclipse.papyrus.robotics.wizards.wizards.NewRoboticsModelWizard;
import org.eclipse.papyrus.robotics.wizards.wizards.NewRoboticsProjectWizard;
import org.eclipse.papyrus.uml.perspective.PapyrusPerspective;
import org.eclipse.papyrus.views.validation.internal.ModelValidationView;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Papyrus for Robotics perspective
 */
public class PapyrusRoboticsPerspective extends PapyrusPerspective implements IPerspectiveFactory {

	/**
	 * Add actions into the workbench UI.
	 *
	 * @param layout
	 *            the page layout
	 *
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	@Override
	public void defineActions(IPageLayout layout) {
		// Add "new wizards".
		layout.addNewWizardShortcut(NewRoboticsModelWizard.WIZARD_ID);
		layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder"); //$NON-NLS-1$
		layout.addNewWizardShortcut(NewRoboticsProjectWizard.WIZARD_ID);
		// Add "show views".
		layout.addShowViewShortcut(IPageLayout.ID_PROJECT_EXPLORER);
		layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
		layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);
		layout.addShowViewShortcut(ID_MODELEXPLORER);
		layout.addShowViewShortcut(ModelValidationView.VIEW_ID);
		layout.addShowViewShortcut(IPageLayout.ID_PROBLEM_VIEW);
		layout.addShowViewShortcut("org.eclipse.pde.runtime.LogView"); // Error log. //A constant doesn't seem to exist for this ID //$NON-NLS-1$

		layout.addActionSet("org.eclipse.debug.ui.launchActionSet"); //$NON-NLS-1$

		// add perspectives
		layout.addPerspectiveShortcut("org.eclipse.ui.resourcePerspective"); //$NON-NLS-1$
		layout.addPerspectiveShortcut("org.eclipse.jdt.ui.JavaPerspective"); //$NON-NLS-1$
	}
}
