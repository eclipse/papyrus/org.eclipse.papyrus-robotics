/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.parameters.instancevalue;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;

public class InstanceValueTableRowAxisManager extends AbstractUMLSynchronizedOnFeatureAxisManager {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected List<Object> getFeaturesValue() {
		EObject context = getTableContext();
		if (context instanceof Class) {
			return (List) ParameterUtils.getAllParameters((Class) context); 
		}
		else if (context instanceof Property) {
			Type type = ((Property) context).getType();
			return (List) ParameterUtils.getAllParameters((Class) type); 
		}
		return null;
	}

	@Override
	protected Collection<EStructuralFeature> getListenFeatures() {
		Collection<EStructuralFeature> listenedFeatures = super.getListenFeatures();
		listenedFeatures.add(UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute());
		return listenedFeatures;
	}
}
