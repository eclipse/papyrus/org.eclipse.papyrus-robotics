package org.eclipse.papyrus.robotics.parameters.defaultvalue;

import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.edit.EditConfigAttributes;
import org.eclipse.nebula.widgets.nattable.edit.editor.ICellEditor;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.infra.nattable.celleditor.TextCellEditor;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.uml2.uml.UMLPackage;

public class DefaultValueTableCellAxisConfiguration implements ICellAxisConfiguration {

	public DefaultValueTableCellAxisConfiguration() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getConfigurationId() {
		return "DefaultValueTable.id"; //$NON-NLS-1$
	}

	@Override
	public String getConfigurationDescription() {
		return "Default value table"; //$NON-NLS-1$
	}

	@Override
	public boolean handles(Table table, Object axisElement) {
		String type = table.getTableConfiguration().getType();
		Object represents = AxisUtils.getRepresentedElement(axisElement);
		boolean handle = (type.equals(DefaultValueTableCellManager.DEFAULT_VALUE_TABLE) &&
				represents.equals(UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute()));
		return handle;
	}

	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axis, String configLabel) {
		final Object axisElement = AxisUtils.getRepresentedElement(axis);
		// configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, getCellPainter(configRegistry, axisElement, configLabel), DisplayMode.NORMAL, configLabel);
		configRegistry.registerConfigAttribute(EditConfigAttributes.CELL_EDITOR, getCellEditor(configRegistry, axisElement, configLabel), DisplayMode.EDIT, configLabel);
	}

	/**
	 * This allows to get the cell editor to use for the table.
	 * 
	 * @return The cell editor.
	 * 
	 * @since 3.0
	 */
	protected ICellEditor getCellEditor(final IConfigRegistry configRegistry, final Object axis, final String configLabel) {
		return new TextCellEditor(true);
	}
}
