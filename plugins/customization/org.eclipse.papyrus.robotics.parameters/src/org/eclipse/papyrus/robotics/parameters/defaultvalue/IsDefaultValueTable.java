/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.parameters.defaultvalue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.constraints.constraints.JavaQuery;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;

public class IsDefaultValueTable implements JavaQuery {

	@Override
	public boolean match(Object selection) {
		boolean matches = false;
		if (selection != null) {
			final EObject modelElement = EMFHelper.getEObject(selection);
			// table with default values
			matches = modelElement instanceof Class &&
					StereotypeUtil.isApplied((Class) modelElement, Parameter.class);
			if (matches == false) {
				matches = modelElement instanceof Class
						&& StereotypeUtil.isApplied((Class) modelElement, ComponentDefinition.class)
						&& ParameterUtils.getParameterClass((Class) modelElement) != null;
			}
		}
		return matches;
	}
}
