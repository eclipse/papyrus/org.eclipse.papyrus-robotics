/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.parameters.defaultvalue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.papyrus.robotics.core.types.RoboticElementTypesEnumerator;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.robotics.properties.widgets.TableEditorPlus;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.UMLPackage;

public class DefaultValueTable extends TableEditorPlus {

	public DefaultValueTable(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public AbstractEditCommandRequest createElementRequest() {
		Class paramClass;
		if (StereotypeUtil.isApplied((Class) context, Parameter.class)) {
			paramClass = (Class) context;
		}
		else {
			paramClass = ParameterUtils.getParameterClass((Class) context);
		}
		if (paramClass != null) {
			CreateElementRequest createParameterRequest = new CreateElementRequest(paramClass,
					RoboticElementTypesEnumerator.PARAM_ENTRY, UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute());

			return createParameterRequest;
		}
		return null;
	}
	
	@Override
	public AbstractEditCommandRequest removeElementRequest(Object object) {
		return new DestroyElementRequest((EObject) object, false);
	}
}
