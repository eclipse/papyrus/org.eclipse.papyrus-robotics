/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.parameters.instancevalue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.constraints.constraints.JavaQuery;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.parameters.utils.ParameterTableUtils;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;

public class IsInstanceValueTable implements JavaQuery {

	@Override
	public boolean match(Object selection) {
		boolean matches = false;
		if (selection != null) {
			final EObject modelElement = EMFHelper.getEObject(selection);
			if (ParameterTableUtils.isProperty(modelElement)) {
				Property property = (Property) modelElement;
				Type type = property.getType();
				if (type instanceof Class) {
					matches = ParameterUtils.getParameterClass((Class) type) != null;
				}
			}
			else if (modelElement instanceof Class) {
				Class parameterClass = (Class) modelElement;
				// check, if actually a parameter block
				if (StereotypeUtil.isApplied(parameterClass, Parameter.class)) {
					Property property = ParameterUtils.getParameterProperty(parameterClass);
					matches = property != null;
				}
			}
		}
		return matches;
	}
}
