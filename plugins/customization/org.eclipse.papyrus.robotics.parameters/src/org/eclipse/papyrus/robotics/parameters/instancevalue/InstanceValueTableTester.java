/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.parameters.instancevalue;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.papyrus.infra.nattable.tester.ITableTester;
import org.eclipse.papyrus.robotics.parameters.utils.ParameterTableUtils;
import org.eclipse.uml2.uml.Property;

public class InstanceValueTableTester implements ITableTester {

	@Override
	public IStatus isAllowed(Object context) {
		IStatus status = null;
		if(ParameterTableUtils.isProperty(context)){
			Property property = (Property) context;
			if(ParameterTableUtils.hasDefaultValue(property)){
				status = Status.OK_STATUS;
			}
		}
		return status;
	}
}
