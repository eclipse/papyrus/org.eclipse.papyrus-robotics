/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.parameters.defaultvalue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.Parameter;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.UMLPackage;

public class DefaultValueTableRowAxisManager extends AbstractUMLSynchronizedOnFeatureAxisManager {

	protected Adapter adapter;
	
	@Override
	protected List<Object> getFeaturesValue() {
		EObject context = getTableContext();
		List<Object> featureValue = new ArrayList<Object>();
		if (context instanceof Class) {
			Class contextCl = (Class) context;
			if (StereotypeUtil.isApplied(contextCl, ComponentDefinition.class)) {
				contextCl = ParameterUtils.getParameterClass(contextCl);
				// adapter must be on parameter class, not on component definition.
				adapter = new AdapterImpl() {

					@Override
					public void notifyChanged(Notification notification) {
						if (notification.getFeature() == UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute()) {
							featureValueHasChanged(notification);
						}
					}
				};
				contextCl.eAdapters().add(adapter);
				featureValue.addAll(contextCl.getOwnedAttributes());
			}
			else if (StereotypeUtil.isApplied(contextCl, Parameter.class)) {
				featureValue.addAll(contextCl.getOwnedAttributes());
			}
		}
		return featureValue;
	}

	@Override
	protected void removeListeners() {
		super.removeListeners();
		EObject context = getTableContext();
		if (adapter != null && context instanceof Class) {
			Class contextCl = ParameterUtils.getParameterClass((Class) context);
			contextCl.eAdapters().remove(adapter);
			adapter = null;
		}
	}

	@Override
	protected Collection<EStructuralFeature> getListenFeatures() {
		Collection<EStructuralFeature> listenedFeatures = super.getListenFeatures();
		listenedFeatures.add(UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute());
		return listenedFeatures;
	}
}
