package org.eclipse.papyrus.robotics.parameters.instancevalue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.nebula.widgets.nattable.config.CellConfigAttributes;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.edit.EditConfigAttributes;
import org.eclipse.nebula.widgets.nattable.edit.editor.ICellEditor;
import org.eclipse.nebula.widgets.nattable.layer.cell.ILayerCell;
import org.eclipse.nebula.widgets.nattable.painter.cell.TextPainter;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.infra.nattable.celleditor.TextCellEditor;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.uml2.uml.Property;

public class InstanceValueTableCellAxisConfiguration implements ICellAxisConfiguration {

	public InstanceValueTableCellAxisConfiguration() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getConfigurationId() {
		return "InstanceValueTable.id"; //$NON-NLS-1$
	}

	@Override
	public String getConfigurationDescription() {
		return "Instance value table"; //$NON-NLS-1$
	}

	@Override
	public boolean handles(Table table, Object axisElement) {
		String type = table.getTableConfiguration().getType();
		return type.equals(InstanceValueTableCellManager.INSTANCE_VALUE_TABLE);
	}

	private class InstanceValuePainter extends TextPainter {

		@Override
		public void paintCell(ILayerCell cell, GC gc, Rectangle bounds, IConfigRegistry configRegistry) {
			Object dataValue = cell.getDataValue();
			boolean defaultValue = true;
			if (dataValue instanceof EObject) {
				// value is contained in a property, is a default value
				defaultValue = ((EObject) dataValue).eContainer() instanceof Property;
			}
			if (!defaultValue) {
				Color originalBackground = gc.getBackground();
				int originalAlpha = gc.getAlpha();
				super.paintCell(cell, gc, bounds, configRegistry);
				gc.setBackground(new Color(null, 255, 255, 0));
				gc.setAlpha(100);
				gc.fillRectangle(bounds);
				gc.setAlpha(originalAlpha);
				gc.setBackground(originalBackground);
			} else {
				super.paintCell(cell, gc, bounds, configRegistry);
			}
		}
	}

	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axis, String configLabel) {
		final Object axisElement = AxisUtils.getRepresentedElement(axis);
		// configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, getCellPainter(configRegistry, axisElement, configLabel), DisplayMode.NORMAL, configLabel);
		configRegistry.registerConfigAttribute(EditConfigAttributes.CELL_EDITOR, getCellEditor(configRegistry, axisElement, configLabel), DisplayMode.EDIT, configLabel);
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, new InstanceValuePainter(), DisplayMode.NORMAL, configLabel);
	}

	/**
	 * This allows to get the cell editor to use for the table.
	 * 
	 * @return The cell editor.
	 * 
	 * @since 3.0
	 */
	protected ICellEditor getCellEditor(final IConfigRegistry configRegistry, final Object axis, final String configLabel) {
		return new TextCellEditor(true);
	}
}
