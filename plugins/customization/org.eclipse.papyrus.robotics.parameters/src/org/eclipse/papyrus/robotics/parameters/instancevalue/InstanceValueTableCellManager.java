/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.parameters.instancevalue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.IAxis;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.parameters.utils.ParameterTableUtils;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterInstance;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.LiteralSpecification;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecification;

public class InstanceValueTableCellManager extends AbstractCellManager {

	// Table type handled by the cell manager
	public static final String INSTANCE_VALUE_TABLE = "InstanceValueTable"; //$NON-NLS-1$

	public static final String INSTANCES = "instances"; //$NON-NLS-1$
	
	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// Check if the table type is PHYSYSTEM_TABLE_TYPE. If not, the table cannot
		// be handled by this cell manager.
		if (columnElement instanceof IAxis) {
			EObject parent = ((IAxis) columnElement).eContainer(); // SlaveObjectAxisProvider
			if (null != parent) {
				parent = parent.eContainer();
				if (parent instanceof Table && ((Table) parent).getTableConfiguration().getType().equals(INSTANCE_VALUE_TABLE)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isCellEditable(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// The cell can be edited as soon as the element displayed in the row
		// is a slot and the defining feature is only allowed to have at most
		// a single value.
		boolean isEditable = false;
		if (rowElement instanceof Property) {
			Property p = (Property) rowElement;
			if (p.upperBound() == 1) {
				isEditable = true;
			}
		}
		return isEditable;
	}

	protected Slot getSlot(EObject context, Property p) {
		if (context instanceof Property) {
			ValueSpecification v = ((Property) context).getDefaultValue();
			if (v instanceof InstanceValue) {
				for (Slot slot : ((InstanceValue) v).getInstance().getSlots()) {
					if (slot.getDefiningFeature() == p) {
						return slot;
					}
				}
			}
		}
		return null;
	}
	
	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// Provide the value to be edited. The value to be edited is always a literal specification.
		// Below are the possible origins for the literal specifications:
		// - (1) The defining feature has no associated values. In that case, an empty
		// literal specification gets generated. This specification has no value.
		// - (2) The defining feature has an associated value and this value is a literal
		// specification. In that case, the literal specification is provided.
		// - (3) The defining feature has an associated value and this latter is an
		// an opaque expression. In this situation an literal string gets generated
		// and its value is the content of the first body of the opaque expression.
		Object value = null;
		EObject context = tableManager.getTable().getContext();
		if (context instanceof Class) {
			context = ParameterUtils.getParameterProperty((Class) context);
		}
		if (rowElement instanceof Property) {
			Property p = (Property) rowElement;
			Slot slot = getSlot(context, p);
			if (slot != null && !slot.getValues().isEmpty()) {
				if (slot.getDefiningFeature().upperBound() == 1) {
					ValueSpecification specification = slot.getValues().iterator().next();
	
					if (specification == null) {
						// no instance value, try default value on defining feature
						specification = p.getDefaultValue();
					}
					if (specification == null) {
						// still null, no default value on defining feature, create new specification
						value = ParameterTableUtils.createValueSpecification((Element) rowElement, slot, null);
					} else if (specification instanceof LiteralSpecification) {
						value = specification;
					} else if (specification instanceof OpaqueExpression) {
						OpaqueExpression expression = (OpaqueExpression) specification;
						List<String> bodies = expression.getBodies();
						if (!bodies.isEmpty()) {
							LiteralString literalString = UMLFactory.eINSTANCE.createLiteralString();
							literalString.setValue(bodies.iterator().next());
							value = literalString;
						}
					}
				}
			}
			else if (p != null && p.getDefault() != null) {
				return p.getDefaultValue();
			}
		}
		return value;
	}

	/**
	 * Create a slot for the property p and the associated instance specification/default value, if required
	 * @param editingCommand
	 * @param context
	 * @param p
	 * @return
	 */
	protected Slot createSlot(CompositeCommand editingCommand, EObject context, Property p) {
		if (context instanceof Property) {
			final Property instance = (Property) context;
			ValueSpecification defaultValue = instance.getDefaultValue();
			IElementEditService provider = ElementEditServiceUtils.getCommandProvider(instance);

			if (defaultValue == null) {
				// no default value, create it and assume that instance specification does not exist neither
				InstanceValue iv = UMLFactory.eINSTANCE.createInstanceValue();
				InstanceSpecification is = UMLFactory.eINSTANCE.createInstanceSpecification();
				Slot slot = is.createSlot();
				slot.setDefiningFeature(p);
				iv.setInstance(is);

				AbstractEditCommandRequest setDefaultValue = new SetRequest(instance, UMLPackage.eINSTANCE.getProperty_DefaultValue(), iv);
				editingCommand.compose(provider.getEditCommand(setDefaultValue));
				
				Package root = PackageUtil.getRootPackage(instance);
				Package instances = (Package) root.getMember(INSTANCES);
				if (instances == null) {
					// instances package does not exists, create it, add instance specification and then add it to the
					// existing model
					instances = UMLFactory.eINSTANCE.createPackage();
					instances.setName(INSTANCES);
					instances.getPackagedElements().add(is);
					List<PackageableElement> peList = new ArrayList<PackageableElement>(root.getPackagedElements());
					peList.add((Package) instances);
					AbstractEditCommandRequest addPackage = new SetRequest(root, UMLPackage.eINSTANCE.getPackage_PackagedElement(), peList);
					editingCommand.compose(provider.getEditCommand(addPackage));
				}
				else {
					// instances package exists already, add instance value
					List<PackageableElement> isList = new ArrayList<PackageableElement>(instances.getPackagedElements());
					isList.add(is);
					AbstractEditCommandRequest addInstanceSpec = new SetRequest(instances, UMLPackage.eINSTANCE.getPackage_PackagedElement(), isList);
					editingCommand.compose(provider.getEditCommand(addInstanceSpec));
				}

				RecordingCommand rc = new RecordingCommand(TransactionUtil.getEditingDomain(p)) {
					protected void doExecute() {
						StereotypeUtil.apply(is, ParameterInstance.class);
					}
				};
				editingCommand.compose(EMFtoGMFCommandWrapper.wrap(rc)); 

				return slot;
			}
			else if (defaultValue instanceof InstanceValue) {
				InstanceSpecification is = ((InstanceValue) defaultValue).getInstance(); 
				for (Slot slot : is.getSlots()) {
					if (slot.getDefiningFeature() == p) {
						// already exists
						return slot;
					}
				}
				Slot slot = UMLFactory.eINSTANCE.createSlot();
				slot.setDefiningFeature(p);
				ArrayList<Slot> slotList = new ArrayList<Slot>(is.getSlots());
				slotList.add(slot);
				AbstractEditCommandRequest addSlot = new SetRequest(is, UMLPackage.eINSTANCE.getInstanceSpecification_Slot(), slotList);
				editingCommand.compose(provider.getEditCommand(addSlot));
				return slot;
			}
			// else case should not happen
		}
		return null;
	}
	
	@Override
	public Command getSetValueCommand(TransactionalEditingDomain domain, Object columnElement, Object rowElement,
			Object newValue, INattableModelManager tableManager) {
		// The text provided by the cell editor in usage is retrieved. If the value associated
		// to the defining feature was an opaque expression then the first body of this opaque
		// expression is
		EObject context = tableManager.getTable().getContext();
		if (context instanceof Class) {
			context = ParameterUtils.getParameterProperty((Class) context);
		}
		CompositeCommand editingCommand = new CompositeCommand(null);
		if (newValue instanceof String) {
			String text = (String) newValue;
			Slot slot;
			if (rowElement instanceof Property) {
				// default value, need to create slot first.
				slot = createSlot(editingCommand, context, (Property) rowElement);
				// return new GMFtoEMFCommandWrapper(editingCommand);
			}
			else {
				slot = (Slot) rowElement;
			}
			ValueSpecification defaultValue = null;
			if (slot.getDefiningFeature() != null) {
				defaultValue = ((Property) slot.getDefiningFeature()).getDefaultValue();
			}
			if (text != null) {
				text = text.replaceAll("\"", "");  //$NON-NLS-1$//$NON-NLS-2$
				if (text.length() == 0 || (defaultValue != null && text.equals(defaultValue.stringValue()))) {
					// value empty identical with default value, remove value specification from slot
					IElementEditService provider = ElementEditServiceUtils.getCommandProvider(context);
					AbstractEditCommandRequest request = new DestroyElementRequest(domain, slot, false);
					editingCommand.compose(provider.getEditCommand(request));
				}
				else if (ParameterTableUtils.isSlotValueAnOpaqueExpression(slot)) {
					OpaqueExpression expression = ParameterTableUtils.getSlotValue(slot);
					IElementEditService provider = ElementEditServiceUtils.getCommandProvider(expression);
					if (provider != null) {
						List<String> bodies = expression.getBodies();
						Collection<String> newBodies = new ArrayList<String>();
						newBodies.add(text);
						if (!bodies.isEmpty()) {
							newBodies.addAll(bodies.subList(1, bodies.size()));
						} else {
							Collection<String> newLanguages = new ArrayList<String>();
							newLanguages.add("VSL"); //$NON-NLS-1$
							AbstractEditCommandRequest languageRequest = new SetRequest(domain, expression, UMLPackage.eINSTANCE.getOpaqueExpression_Language(), newLanguages);
							editingCommand.compose(provider.getEditCommand(languageRequest));
						}
						AbstractEditCommandRequest bodiesRequest = new SetRequest(domain, expression, UMLPackage.eINSTANCE.getOpaqueExpression_Body(), newBodies);
						editingCommand.compose(provider.getEditCommand(bodiesRequest));
					}
				} else {
					ValueSpecification specification = ParameterTableUtils.createValueSpecification((Element) rowElement, slot, text);
					IElementEditService provider = ElementEditServiceUtils.getCommandProvider(slot);
					if (specification != null && provider != null) {
						Collection<ValueSpecification> values = new ArrayList<ValueSpecification>();
						values.add(specification);
						AbstractEditCommandRequest request = new SetRequest(domain, slot, UMLPackage.eINSTANCE.getSlot_Value(), values);
						editingCommand.compose(provider.getEditCommand(request));
					}
				}
			}
		}
		return new GMFtoEMFCommandWrapper(editingCommand);
	}
}
