/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.modelexplorer.queries;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.papyrus.robotics.core.provider.RoboticsMenuLabelProvider;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.uml.modelexplorer.queries.GetComplexName;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Port;

/**
 * Query to get names for elements displayed in the model explorer. It delegates to the {@link RoboticsMenuLabelProvider}.
 * CAVEAT: The DerivedRMSNames query must be placed before the StereotypeDisplay query in the ME configuration.
 */
public class GetLabelQuery extends GetComplexName {

	/**
	 * Constructor.
	 */
	public GetLabelQuery() {
		// empty
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String evaluate(final NamedElement context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {
		String result = null;

		if (context instanceof Connector) {
			result = InteractionUtils.getConnectorLabel((Connector) context);
		} 
		else if (context instanceof Port) {
			result = PortUtils.getPortLabel((Port) context);
		}
		else {
			result = super.evaluate(context, parameterValues, facetManager);
		}
		return result;
	}
}
