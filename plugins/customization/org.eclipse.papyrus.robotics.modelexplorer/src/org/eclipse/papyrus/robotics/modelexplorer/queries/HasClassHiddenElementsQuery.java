/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.modelexplorer.queries;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Class;

/**
 * Query that returns <code>true</code> if the specified Package context contains a protocolContainer Package.
 */
public class HasClassHiddenElementsQuery implements IJavaQuery2<Class, Boolean> {

	/**
	 * Constructor.
	 */
	public HasClassHiddenElementsQuery() {
		// empty constructor
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean evaluate(final Class context, final IParameterValueList2 parameterValues, final IFacetManager facetManager) throws DerivedTypedElementException {
		return CustomNestedClassifier.getFilteredElements(context).size() < context.getNestedClassifiers().size();
	}
}




