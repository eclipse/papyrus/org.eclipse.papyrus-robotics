/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.modelexplorer.queries;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.papyrus.uml.modelexplorer.queries.GetVisibleUMLReferencesQuery;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Query that returns all usual UML references, except Element::OWNED_ELEMENT.
 */
public class GetVisibleClassUMLReferencesQuery extends GetVisibleUMLReferencesQuery {

	/** excluded reference set. */
	private static final Set<EReference> EXCLUDED_REFERENCES = getExcludedReferences();

	/**
	 * Constructor.
	 */
	public GetVisibleClassUMLReferencesQuery() {
		// empty
	}

	/**
	 * @return the list of Excluded references from this query.
	 */
	private static Set<EReference> getExcludedReferences() {
		final Set<EReference> result = new HashSet<>(1);
		result.add(UMLPackage.Literals.CLASS__NESTED_CLASSIFIER);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EReference> evaluate(final EObject source, final IParameterValueList2 parameterValues, final IFacetManager facetManager) throws DerivedTypedElementException {
		List<EReference> result = new ArrayList<>(super.evaluate(source, parameterValues, facetManager));
		result.removeAll(EXCLUDED_REFERENCES);
		return result;
	}
}
