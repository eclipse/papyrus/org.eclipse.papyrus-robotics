/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.modelexplorer.queries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Usage;


/**
 * Variant of packaged elements query that hides component services (to be extended)
 */
public class CustomPackagedElement implements IJavaQuery2<Element, List<PackageableElement>> {

	public List<PackageableElement> evaluate(final Element context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {

		if (!(context instanceof Package)) {
			return Collections.emptyList();
		}
		return getFilteredElements((Package) context);
	}
	
	public static List<PackageableElement> getFilteredElements(Package pkg) {
		ArrayList<PackageableElement> result = new ArrayList<PackageableElement>();
		for (PackageableElement pe : pkg.getPackagedElements()) {
			if (StereotypeUtil.isApplied(pe, ComponentService.class)) {
				// hide component services
			}
			else if (pe instanceof Usage) {
				// hide Usage relationships originating from ComponentServices
				Usage usage = (Usage) pe;
				boolean csSource = false;
				if (usage.getClients().size() > 0) {
					NamedElement client = usage.getClients().get(0);
					if (StereotypeUtil.isApplied(client, ComponentService.class)) {
						csSource = true;
					}
				}
				if (!csSource) {
					result.add(pe);
				}
			}
			else {
				result.add(pe);
			}
		}
		return result;
		
	}
}

