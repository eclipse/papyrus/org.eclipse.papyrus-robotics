/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.modelexplorer.queries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentService;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;


/**
 * Variant of packaged elements query that hides component services (to be extended)
 */
public class CustomNestedClassifier implements IJavaQuery2<Element, List<Classifier>> {

	public List<Classifier> evaluate(final Element context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {

		if (!(context instanceof Class)) {
			return Collections.emptyList();
		}
		return getFilteredElements((Class) context);
	}
	
	public static List<Classifier> getFilteredElements(Class cl) {
		ArrayList<Classifier> result = new ArrayList<Classifier>();
		for (Classifier nCL : cl.getNestedClassifiers()) {
			if (StereotypeUtil.isApplied(nCL, ComponentService.class)) {
				// hide component services
			}
			else {
				result.add(nCL);
			}
		}
		return result;
		
	}
}

