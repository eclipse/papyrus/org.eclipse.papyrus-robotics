/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.architecture.commands;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.infra.gmfdiag.common.helper.DiagramPrototype;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.DiagramUtils;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.papyrus.uml.diagram.activity.CreateActivityDiagramCommand;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.BehavioredClassifier;

public class CreateBTSystemActivityDiagramCommand extends CreateActivityDiagramCommand {

	protected Diagram dgr;

	public Diagram getDiagram() {
		return dgr;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Diagram doCreateDiagram(Resource diagramResource, EObject owner, EObject element, DiagramPrototype prototype, String name) {
		Diagram diagram = null;
		if (element instanceof org.eclipse.uml2.uml.Package) {
			diagram = ViewService.createDiagram(element, getDiagramNotationID(), getPreferenceHint());
		} else if (element instanceof BehavioredClassifier) {
			diagram = ViewService.createDiagram(((BehavioredClassifier) element).getNearestPackage(), getDiagramNotationID(), getPreferenceHint());
		}
		// create diagram
		if (diagram != null) {
			if (element instanceof Activity) {
				Activity activity = (Activity) element;
				StereotypeUtil.apply(activity, Task.class);
				StereotypeUtil.apply(activity, TreeRoot.class);
			}
			setName(name);
			diagram.setElement(element);
			DiagramUtils.setOwner(diagram, owner);
			DiagramUtils.setPrototype(diagram, prototype);
			initializeModel(element);
			initializeDiagram(diagram);
			diagramResource.getContents().add(diagram);
		}
		dgr = diagram;
		return diagram;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getDefaultDiagramName() {
		return "Behavior tree diagram"; //$NON-NLS-1$
	}
}
