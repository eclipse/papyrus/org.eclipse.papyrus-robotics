/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  							from Ansgar Radermacher)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.architecture.commands;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.papyrus.infra.architecture.commands.IModelCreationCommand;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.robotics.bpc.profile.util.BPCResource;
import org.eclipse.papyrus.robotics.bt.architecture.Activator;
import org.eclipse.papyrus.robotics.library.util.RoboticsLibResource;
import org.eclipse.papyrus.robotics.profile.robotics.generics.Package;
import org.eclipse.papyrus.robotics.profile.util.RoboticsResource;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;

public class CreateBTMLModelCommand implements IModelCreationCommand {
	private static final String TEMPLATE_URI = "platform:/plugin/org.eclipse.papyrus.robotics.bt.architecture/templates/btmodel-template.uml"; //$NON-NLS-1$

	@Override
	public void createModel(final ModelSet modelSet) {
		runAsTransaction(modelSet);
	}

	protected void runAsTransaction(final ModelSet modelSet) {
		// Get the UML element to which the newly created diagram will be
		// attached.
		// Create the diagram
		final Resource modelResource = UmlUtils.getUmlResource(modelSet);
		TransactionalEditingDomain editingDomain = modelSet.getTransactionalEditingDomain();

		URI origURI = modelResource.getURI();
		try {
			modelResource.setURI(URI.createURI(TEMPLATE_URI));
			modelResource.load(null);
		}
		catch (Exception e) {
			Activator.log.error(e);
			// template loading failed, fall-back to programmatic creation
			AbstractTransactionalCommand command = new AbstractTransactionalCommand(editingDomain, "Initialize model", Collections.EMPTY_LIST) { //$NON-NLS-1$

				@Override
				protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
					EObject model = getRootElement(modelResource);
					attachModelToResource(model, modelResource);
					initializeModel(model);
					return CommandResult.newOKCommandResult();
				}
			};
			editingDomain.getCommandStack().execute(new GMFtoEMFCommandWrapper(command));
		}
		finally {
			modelResource.setURI(origURI);
		}
	}

	// methods for programmatic creation in case of failure of template loading
	// ========================================================================
	protected EObject getRootElement(Resource modelResource) {
		EObject rootElement = null;
		if (modelResource != null && modelResource.getContents() != null && modelResource.getContents().size() > 0) {
			Object root = modelResource.getContents().get(0);
			if (root instanceof EObject) {
				rootElement = (EObject) root;
			}
		} else {
			rootElement = createRootElement();
		}
		return rootElement;
	}

	protected EObject createRootElement() {
		return UMLFactory.eINSTANCE.createModel();
	}

	protected void attachModelToResource(EObject root, Resource resource) {
		resource.getContents().add(root);
	}

	protected void initializeModel(EObject owner) {
		org.eclipse.uml2.uml.Package packageOwner = (org.eclipse.uml2.uml.Package) owner;

		// Retrieve profiles and apply
		Profile bpc = (Profile) PackageUtil.loadPackage(BPCResource.PROFILE_PATH_URI, owner.eResource().getResourceSet());
		if (bpc != null) {
			PackageUtil.applyProfile(packageOwner, bpc, true);
		}
		Profile robotics = (Profile) PackageUtil.loadPackage(RoboticsResource.PROFILE_PATH_URI, owner.eResource().getResourceSet());
		if (robotics != null) {
			List<String> profileNames = Arrays.asList("generics", "skills", "behavior");
			for(String name : profileNames) {
				Profile profile = (Profile) robotics.getOwnedMember(name);
				if (profile != null) {
					PackageUtil.applyProfile(packageOwner, profile, true);
				}
			}
		}
		Profile bt = (Profile) PackageUtil.loadPackage(URI.createURI("pathmap://Robotics_PROFILES_BT/bt.profile.uml"), owner.eResource().getResourceSet());
		if (bt != null) {
			PackageUtil.applyProfile(packageOwner, bt, true);
		}

		org.eclipse.uml2.uml.Package roboticsLib = PackageUtil.loadPackage(RoboticsLibResource.ROBOTICS_LIB_URI, owner.eResource().getResourceSet());
		if (roboticsLib != null) {
			org.eclipse.uml2.uml.Package skillLib = (org.eclipse.uml2.uml.Package) roboticsLib.getOwnedMember("skills");
			if (roboticsLib != null) {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(skillLib);
				packageOwner.getPackageImports().add(pi);
			}
		}

		// Model name and stereotype
		packageOwner.setName(getModelName());
		StereotypeUtil.apply(packageOwner, Package.class);
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected String getModelName() {
		return "BtModel"; //$NON-NLS-1$
	}
}
