/**
 * Copyright (c) 2019, 2023 CEA LIST.
 * Copyright (c) 2014-2018 Michele Colledanchise Copyright (c) 2018-2019 Davide Faconti
 */
package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.util.BehaviortreeSchemaResourceFactoryImpl
 * @generated
 */
public class BehaviortreeSchemaResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public BehaviortreeSchemaResourceImpl(URI uri) {
		super(uri);
	}

} //BehaviortreeSchemaResourceImpl
