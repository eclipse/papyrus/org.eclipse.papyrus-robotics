/**
 * Copyright (c) 2019, 2023 CEA LIST.
 * Copyright (c) 2014-2018 Michele Colledanchise Copyright (c) 2018-2019 Davide Faconti
 */
package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timeout Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getControl <em>Control</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getDecorator <em>Decorator</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSubtreeplus <em>Subtreeplus</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSequence <em>Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveSequence <em>Reactive Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getFallback <em>Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveFallback <em>Reactive Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getParallel <em>Parallel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getInverter <em>Inverter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRetryUntilSuccessful <em>Retry Until Successful</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRepeat <em>Repeat</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getTimeout <em>Timeout</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceSucces <em>Force Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceFailure <em>Force Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysSucces <em>Always Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysFailure <em>Always Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getMsec <em>Msec</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType()
 * @model extendedMetaData="name='TimeoutType' kind='elementOnly'"
 * @generated
 */
public interface TimeoutType extends EObject {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference.
	 * @see #setAction(ActionType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Action()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Action' namespace='##targetNamespace'"
	 * @generated
	 */
	ActionType getAction();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAction <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' containment reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(ActionType value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(ConditionType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Condition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Condition' namespace='##targetNamespace'"
	 * @generated
	 */
	ConditionType getCondition();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(ConditionType value);

	/**
	 * Returns the value of the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Control</em>' containment reference.
	 * @see #setControl(ControlType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Control()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Control' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlType getControl();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getControl <em>Control</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Control</em>' containment reference.
	 * @see #getControl()
	 * @generated
	 */
	void setControl(ControlType value);

	/**
	 * Returns the value of the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decorator</em>' containment reference.
	 * @see #setDecorator(DecoratorType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Decorator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Decorator' namespace='##targetNamespace'"
	 * @generated
	 */
	DecoratorType getDecorator();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getDecorator <em>Decorator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decorator</em>' containment reference.
	 * @see #getDecorator()
	 * @generated
	 */
	void setDecorator(DecoratorType value);

	/**
	 * Returns the value of the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtreeplus</em>' containment reference.
	 * @see #setSubtreeplus(SubTreePlusType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Subtreeplus()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SubTreePlus' namespace='##targetNamespace'"
	 * @generated
	 */
	SubTreePlusType getSubtreeplus();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSubtreeplus <em>Subtreeplus</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtreeplus</em>' containment reference.
	 * @see #getSubtreeplus()
	 * @generated
	 */
	void setSubtreeplus(SubTreePlusType value);

	/**
	 * Returns the value of the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence</em>' containment reference.
	 * @see #setSequence(SequenceType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Sequence()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Sequence' namespace='##targetNamespace'"
	 * @generated
	 */
	SequenceType getSequence();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSequence <em>Sequence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence</em>' containment reference.
	 * @see #getSequence()
	 * @generated
	 */
	void setSequence(SequenceType value);

	/**
	 * Returns the value of the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reactive Sequence</em>' containment reference.
	 * @see #setReactiveSequence(ReactiveSequenceType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_ReactiveSequence()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ReactiveSequence' namespace='##targetNamespace'"
	 * @generated
	 */
	ReactiveSequenceType getReactiveSequence();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveSequence <em>Reactive Sequence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reactive Sequence</em>' containment reference.
	 * @see #getReactiveSequence()
	 * @generated
	 */
	void setReactiveSequence(ReactiveSequenceType value);

	/**
	 * Returns the value of the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fallback</em>' containment reference.
	 * @see #setFallback(FallbackType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Fallback()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Fallback' namespace='##targetNamespace'"
	 * @generated
	 */
	FallbackType getFallback();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getFallback <em>Fallback</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fallback</em>' containment reference.
	 * @see #getFallback()
	 * @generated
	 */
	void setFallback(FallbackType value);

	/**
	 * Returns the value of the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reactive Fallback</em>' containment reference.
	 * @see #setReactiveFallback(ReactiveFallbackType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_ReactiveFallback()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ReactiveFallback' namespace='##targetNamespace'"
	 * @generated
	 */
	ReactiveFallbackType getReactiveFallback();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveFallback <em>Reactive Fallback</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reactive Fallback</em>' containment reference.
	 * @see #getReactiveFallback()
	 * @generated
	 */
	void setReactiveFallback(ReactiveFallbackType value);

	/**
	 * Returns the value of the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parallel</em>' containment reference.
	 * @see #setParallel(ParallelType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Parallel()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Parallel' namespace='##targetNamespace'"
	 * @generated
	 */
	ParallelType getParallel();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getParallel <em>Parallel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parallel</em>' containment reference.
	 * @see #getParallel()
	 * @generated
	 */
	void setParallel(ParallelType value);

	/**
	 * Returns the value of the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverter</em>' containment reference.
	 * @see #setInverter(InverterType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Inverter()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Inverter' namespace='##targetNamespace'"
	 * @generated
	 */
	InverterType getInverter();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getInverter <em>Inverter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inverter</em>' containment reference.
	 * @see #getInverter()
	 * @generated
	 */
	void setInverter(InverterType value);

	/**
	 * Returns the value of the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Retry Until Successful</em>' containment reference.
	 * @see #setRetryUntilSuccessful(RetryType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_RetryUntilSuccessful()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RetryUntilSuccessful' namespace='##targetNamespace'"
	 * @generated
	 */
	RetryType getRetryUntilSuccessful();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRetryUntilSuccessful <em>Retry Until Successful</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Retry Until Successful</em>' containment reference.
	 * @see #getRetryUntilSuccessful()
	 * @generated
	 */
	void setRetryUntilSuccessful(RetryType value);

	/**
	 * Returns the value of the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repeat</em>' containment reference.
	 * @see #setRepeat(RepeatType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Repeat()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Repeat' namespace='##targetNamespace'"
	 * @generated
	 */
	RepeatType getRepeat();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRepeat <em>Repeat</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repeat</em>' containment reference.
	 * @see #getRepeat()
	 * @generated
	 */
	void setRepeat(RepeatType value);

	/**
	 * Returns the value of the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeout</em>' containment reference.
	 * @see #setTimeout(TimeoutType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Timeout()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Timeout' namespace='##targetNamespace'"
	 * @generated
	 */
	TimeoutType getTimeout();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getTimeout <em>Timeout</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timeout</em>' containment reference.
	 * @see #getTimeout()
	 * @generated
	 */
	void setTimeout(TimeoutType value);

	/**
	 * Returns the value of the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Force Succes</em>' containment reference.
	 * @see #setForceSucces(ForceSuccesType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_ForceSucces()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ForceSucces' namespace='##targetNamespace'"
	 * @generated
	 */
	ForceSuccesType getForceSucces();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceSucces <em>Force Succes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Force Succes</em>' containment reference.
	 * @see #getForceSucces()
	 * @generated
	 */
	void setForceSucces(ForceSuccesType value);

	/**
	 * Returns the value of the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Force Failure</em>' containment reference.
	 * @see #setForceFailure(ForceFailureType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_ForceFailure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ForceFailure' namespace='##targetNamespace'"
	 * @generated
	 */
	ForceFailureType getForceFailure();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceFailure <em>Force Failure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Force Failure</em>' containment reference.
	 * @see #getForceFailure()
	 * @generated
	 */
	void setForceFailure(ForceFailureType value);

	/**
	 * Returns the value of the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Always Succes</em>' containment reference.
	 * @see #setAlwaysSucces(AlwaysSuccesType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_AlwaysSucces()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AlwaysSucces' namespace='##targetNamespace'"
	 * @generated
	 */
	AlwaysSuccesType getAlwaysSucces();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysSucces <em>Always Succes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Always Succes</em>' containment reference.
	 * @see #getAlwaysSucces()
	 * @generated
	 */
	void setAlwaysSucces(AlwaysSuccesType value);

	/**
	 * Returns the value of the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Always Failure</em>' containment reference.
	 * @see #setAlwaysFailure(AlwaysFailureType)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_AlwaysFailure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AlwaysFailure' namespace='##targetNamespace'"
	 * @generated
	 */
	AlwaysFailureType getAlwaysFailure();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysFailure <em>Always Failure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Always Failure</em>' containment reference.
	 * @see #getAlwaysFailure()
	 * @generated
	 */
	void setAlwaysFailure(AlwaysFailureType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Msec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msec</em>' attribute.
	 * @see #setMsec(String)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getTimeoutType_Msec()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='msec' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMsec();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getMsec <em>Msec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msec</em>' attribute.
	 * @see #getMsec()
	 * @generated
	 */
	void setMsec(String value);

} // TimeoutType
