/**
 * Copyright (c) 2019, 2023 CEA LIST.
 * Copyright (c) 2014-2018 Michele Colledanchise Copyright (c) 2018-2019 Davide Faconti
 */
package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reactive Fallback Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getControl <em>Control</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getDecorator <em>Decorator</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getSubtreeplus <em>Subtreeplus</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getSequence <em>Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getReactiveSequence <em>Reactive Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getFallback <em>Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getReactiveFallback <em>Reactive Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getParallel <em>Parallel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getInverter <em>Inverter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getRetryUntilSuccessful <em>Retry Until Successful</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getRepeat <em>Repeat</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getTimeout <em>Timeout</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getForceSucces <em>Force Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getForceFailure <em>Force Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getAlwaysSucces <em>Always Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getAlwaysFailure <em>Always Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReactiveFallbackTypeImpl extends MinimalEObjectImpl.Container implements ReactiveFallbackType {
	/**
	 * The cached value of the '{@link #getBuiltInMultipleTypes() <em>Built In Multiple Types</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuiltInMultipleTypes()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap builtInMultipleTypes;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReactiveFallbackTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getBuiltInMultipleTypes() {
		if (builtInMultipleTypes == null) {
			builtInMultipleTypes = new BasicFeatureMap(this, BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES);
		}
		return builtInMultipleTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActionType> getAction() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConditionType> getCondition() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__CONDITION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlType> getControl() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__CONTROL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DecoratorType> getDecorator() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__DECORATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubTreePlusType> getSubtreeplus() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__SUBTREEPLUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SequenceType> getSequence() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__SEQUENCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReactiveSequenceType> getReactiveSequence() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FallbackType> getFallback() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__FALLBACK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReactiveFallbackType> getReactiveFallback() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParallelType> getParallel() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__PARALLEL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InverterType> getInverter() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__INVERTER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RetryType> getRetryUntilSuccessful() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RepeatType> getRepeat() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__REPEAT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TimeoutType> getTimeout() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__TIMEOUT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForceSuccesType> getForceSucces() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__FORCE_SUCCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForceFailureType> getForceFailure() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__FORCE_FAILURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlwaysSuccesType> getAlwaysSucces() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlwaysFailureType> getAlwaysFailure() {
		return getBuiltInMultipleTypes().list(BehaviortreeSchemaPackage.Literals.REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES:
				return ((InternalEList<?>)getBuiltInMultipleTypes()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ACTION:
				return ((InternalEList<?>)getAction()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONDITION:
				return ((InternalEList<?>)getCondition()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONTROL:
				return ((InternalEList<?>)getControl()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__DECORATOR:
				return ((InternalEList<?>)getDecorator()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SUBTREEPLUS:
				return ((InternalEList<?>)getSubtreeplus()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SEQUENCE:
				return ((InternalEList<?>)getSequence()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE:
				return ((InternalEList<?>)getReactiveSequence()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FALLBACK:
				return ((InternalEList<?>)getFallback()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK:
				return ((InternalEList<?>)getReactiveFallback()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__PARALLEL:
				return ((InternalEList<?>)getParallel()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__INVERTER:
				return ((InternalEList<?>)getInverter()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL:
				return ((InternalEList<?>)getRetryUntilSuccessful()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REPEAT:
				return ((InternalEList<?>)getRepeat()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__TIMEOUT:
				return ((InternalEList<?>)getTimeout()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_SUCCES:
				return ((InternalEList<?>)getForceSucces()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_FAILURE:
				return ((InternalEList<?>)getForceFailure()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES:
				return ((InternalEList<?>)getAlwaysSucces()).basicRemove(otherEnd, msgs);
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE:
				return ((InternalEList<?>)getAlwaysFailure()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES:
				if (coreType) return getBuiltInMultipleTypes();
				return ((FeatureMap.Internal)getBuiltInMultipleTypes()).getWrapper();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ACTION:
				return getAction();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONDITION:
				return getCondition();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONTROL:
				return getControl();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__DECORATOR:
				return getDecorator();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SUBTREEPLUS:
				return getSubtreeplus();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SEQUENCE:
				return getSequence();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE:
				return getReactiveSequence();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FALLBACK:
				return getFallback();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK:
				return getReactiveFallback();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__PARALLEL:
				return getParallel();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__INVERTER:
				return getInverter();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL:
				return getRetryUntilSuccessful();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REPEAT:
				return getRepeat();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__TIMEOUT:
				return getTimeout();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_SUCCES:
				return getForceSucces();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_FAILURE:
				return getForceFailure();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES:
				return getAlwaysSucces();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE:
				return getAlwaysFailure();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES:
				((FeatureMap.Internal)getBuiltInMultipleTypes()).set(newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ACTION:
				getAction().clear();
				getAction().addAll((Collection<? extends ActionType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection<? extends ConditionType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONTROL:
				getControl().clear();
				getControl().addAll((Collection<? extends ControlType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__DECORATOR:
				getDecorator().clear();
				getDecorator().addAll((Collection<? extends DecoratorType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SUBTREEPLUS:
				getSubtreeplus().clear();
				getSubtreeplus().addAll((Collection<? extends SubTreePlusType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SEQUENCE:
				getSequence().clear();
				getSequence().addAll((Collection<? extends SequenceType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE:
				getReactiveSequence().clear();
				getReactiveSequence().addAll((Collection<? extends ReactiveSequenceType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FALLBACK:
				getFallback().clear();
				getFallback().addAll((Collection<? extends FallbackType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK:
				getReactiveFallback().clear();
				getReactiveFallback().addAll((Collection<? extends ReactiveFallbackType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__PARALLEL:
				getParallel().clear();
				getParallel().addAll((Collection<? extends ParallelType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__INVERTER:
				getInverter().clear();
				getInverter().addAll((Collection<? extends InverterType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL:
				getRetryUntilSuccessful().clear();
				getRetryUntilSuccessful().addAll((Collection<? extends RetryType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REPEAT:
				getRepeat().clear();
				getRepeat().addAll((Collection<? extends RepeatType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__TIMEOUT:
				getTimeout().clear();
				getTimeout().addAll((Collection<? extends TimeoutType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_SUCCES:
				getForceSucces().clear();
				getForceSucces().addAll((Collection<? extends ForceSuccesType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_FAILURE:
				getForceFailure().clear();
				getForceFailure().addAll((Collection<? extends ForceFailureType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES:
				getAlwaysSucces().clear();
				getAlwaysSucces().addAll((Collection<? extends AlwaysSuccesType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE:
				getAlwaysFailure().clear();
				getAlwaysFailure().addAll((Collection<? extends AlwaysFailureType>)newValue);
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES:
				getBuiltInMultipleTypes().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ACTION:
				getAction().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONDITION:
				getCondition().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONTROL:
				getControl().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__DECORATOR:
				getDecorator().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SUBTREEPLUS:
				getSubtreeplus().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SEQUENCE:
				getSequence().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE:
				getReactiveSequence().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FALLBACK:
				getFallback().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK:
				getReactiveFallback().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__PARALLEL:
				getParallel().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__INVERTER:
				getInverter().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL:
				getRetryUntilSuccessful().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REPEAT:
				getRepeat().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__TIMEOUT:
				getTimeout().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_SUCCES:
				getForceSucces().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_FAILURE:
				getForceFailure().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES:
				getAlwaysSucces().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE:
				getAlwaysFailure().clear();
				return;
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES:
				return builtInMultipleTypes != null && !builtInMultipleTypes.isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ACTION:
				return !getAction().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONDITION:
				return !getCondition().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__CONTROL:
				return !getControl().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__DECORATOR:
				return !getDecorator().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SUBTREEPLUS:
				return !getSubtreeplus().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__SEQUENCE:
				return !getSequence().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE:
				return !getReactiveSequence().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FALLBACK:
				return !getFallback().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK:
				return !getReactiveFallback().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__PARALLEL:
				return !getParallel().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__INVERTER:
				return !getInverter().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL:
				return !getRetryUntilSuccessful().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__REPEAT:
				return !getRepeat().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__TIMEOUT:
				return !getTimeout().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_SUCCES:
				return !getForceSucces().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__FORCE_FAILURE:
				return !getForceFailure().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES:
				return !getAlwaysSucces().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE:
				return !getAlwaysFailure().isEmpty();
			case BehaviortreeSchemaPackage.REACTIVE_FALLBACK_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (builtInMultipleTypes: ");
		result.append(builtInMultipleTypes);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ReactiveFallbackTypeImpl
