/**
 * Copyright (c) 2019, 2023 CEA LIST.
 * Copyright (c) 2014-2018 Michele Colledanchise Copyright (c) 2018-2019 Davide Faconti
 */
package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType;
import org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Behavior Tree Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getControl <em>Control</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getDecorator <em>Decorator</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getSubtreeplus <em>Subtreeplus</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getSequence <em>Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getReactiveSequence <em>Reactive Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getFallback <em>Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getReactiveFallback <em>Reactive Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getParallel <em>Parallel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getInverter <em>Inverter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getRetryUntilSuccessful <em>Retry Until Successful</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getRepeat <em>Repeat</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getTimeout <em>Timeout</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getForceSucces <em>Force Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getForceFailure <em>Force Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getAlwaysSucces <em>Always Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getAlwaysFailure <em>Always Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl#getID <em>ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BehaviorTreeTypeImpl extends MinimalEObjectImpl.Container implements BehaviorTreeType {
	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected ActionType action;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected ConditionType condition;

	/**
	 * The cached value of the '{@link #getControl() <em>Control</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControl()
	 * @generated
	 * @ordered
	 */
	protected ControlType control;

	/**
	 * The cached value of the '{@link #getDecorator() <em>Decorator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecorator()
	 * @generated
	 * @ordered
	 */
	protected DecoratorType decorator;

	/**
	 * The cached value of the '{@link #getSubtreeplus() <em>Subtreeplus</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtreeplus()
	 * @generated
	 * @ordered
	 */
	protected SubTreePlusType subtreeplus;

	/**
	 * The cached value of the '{@link #getSequence() <em>Sequence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequence()
	 * @generated
	 * @ordered
	 */
	protected SequenceType sequence;

	/**
	 * The cached value of the '{@link #getReactiveSequence() <em>Reactive Sequence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReactiveSequence()
	 * @generated
	 * @ordered
	 */
	protected ReactiveSequenceType reactiveSequence;

	/**
	 * The cached value of the '{@link #getFallback() <em>Fallback</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFallback()
	 * @generated
	 * @ordered
	 */
	protected FallbackType fallback;

	/**
	 * The cached value of the '{@link #getReactiveFallback() <em>Reactive Fallback</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReactiveFallback()
	 * @generated
	 * @ordered
	 */
	protected ReactiveFallbackType reactiveFallback;

	/**
	 * The cached value of the '{@link #getParallel() <em>Parallel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParallel()
	 * @generated
	 * @ordered
	 */
	protected ParallelType parallel;

	/**
	 * The cached value of the '{@link #getInverter() <em>Inverter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInverter()
	 * @generated
	 * @ordered
	 */
	protected InverterType inverter;

	/**
	 * The cached value of the '{@link #getRetryUntilSuccessful() <em>Retry Until Successful</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRetryUntilSuccessful()
	 * @generated
	 * @ordered
	 */
	protected RetryType retryUntilSuccessful;

	/**
	 * The cached value of the '{@link #getRepeat() <em>Repeat</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepeat()
	 * @generated
	 * @ordered
	 */
	protected RepeatType repeat;

	/**
	 * The cached value of the '{@link #getTimeout() <em>Timeout</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeout()
	 * @generated
	 * @ordered
	 */
	protected TimeoutType timeout;

	/**
	 * The cached value of the '{@link #getForceSucces() <em>Force Succes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForceSucces()
	 * @generated
	 * @ordered
	 */
	protected ForceSuccesType forceSucces;

	/**
	 * The cached value of the '{@link #getForceFailure() <em>Force Failure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForceFailure()
	 * @generated
	 * @ordered
	 */
	protected ForceFailureType forceFailure;

	/**
	 * The cached value of the '{@link #getAlwaysSucces() <em>Always Succes</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlwaysSucces()
	 * @generated
	 * @ordered
	 */
	protected AlwaysSuccesType alwaysSucces;

	/**
	 * The cached value of the '{@link #getAlwaysFailure() <em>Always Failure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlwaysFailure()
	 * @generated
	 * @ordered
	 */
	protected AlwaysFailureType alwaysFailure;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BehaviorTreeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BehaviortreeSchemaPackage.Literals.BEHAVIOR_TREE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionType getAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAction(ActionType newAction, NotificationChain msgs) {
		ActionType oldAction = action;
		action = newAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION, oldAction, newAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(ActionType newAction) {
		if (newAction != action) {
			NotificationChain msgs = null;
			if (action != null)
				msgs = ((InternalEObject)action).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION, null, msgs);
			if (newAction != null)
				msgs = ((InternalEObject)newAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION, null, msgs);
			msgs = basicSetAction(newAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION, newAction, newAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionType getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(ConditionType newCondition, NotificationChain msgs) {
		ConditionType oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(ConditionType newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlType getControl() {
		return control;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControl(ControlType newControl, NotificationChain msgs) {
		ControlType oldControl = control;
		control = newControl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL, oldControl, newControl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControl(ControlType newControl) {
		if (newControl != control) {
			NotificationChain msgs = null;
			if (control != null)
				msgs = ((InternalEObject)control).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL, null, msgs);
			if (newControl != null)
				msgs = ((InternalEObject)newControl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL, null, msgs);
			msgs = basicSetControl(newControl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL, newControl, newControl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecoratorType getDecorator() {
		return decorator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDecorator(DecoratorType newDecorator, NotificationChain msgs) {
		DecoratorType oldDecorator = decorator;
		decorator = newDecorator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR, oldDecorator, newDecorator);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDecorator(DecoratorType newDecorator) {
		if (newDecorator != decorator) {
			NotificationChain msgs = null;
			if (decorator != null)
				msgs = ((InternalEObject)decorator).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR, null, msgs);
			if (newDecorator != null)
				msgs = ((InternalEObject)newDecorator).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR, null, msgs);
			msgs = basicSetDecorator(newDecorator, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR, newDecorator, newDecorator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubTreePlusType getSubtreeplus() {
		return subtreeplus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtreeplus(SubTreePlusType newSubtreeplus, NotificationChain msgs) {
		SubTreePlusType oldSubtreeplus = subtreeplus;
		subtreeplus = newSubtreeplus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS, oldSubtreeplus, newSubtreeplus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtreeplus(SubTreePlusType newSubtreeplus) {
		if (newSubtreeplus != subtreeplus) {
			NotificationChain msgs = null;
			if (subtreeplus != null)
				msgs = ((InternalEObject)subtreeplus).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS, null, msgs);
			if (newSubtreeplus != null)
				msgs = ((InternalEObject)newSubtreeplus).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS, null, msgs);
			msgs = basicSetSubtreeplus(newSubtreeplus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS, newSubtreeplus, newSubtreeplus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceType getSequence() {
		return sequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSequence(SequenceType newSequence, NotificationChain msgs) {
		SequenceType oldSequence = sequence;
		sequence = newSequence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE, oldSequence, newSequence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSequence(SequenceType newSequence) {
		if (newSequence != sequence) {
			NotificationChain msgs = null;
			if (sequence != null)
				msgs = ((InternalEObject)sequence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE, null, msgs);
			if (newSequence != null)
				msgs = ((InternalEObject)newSequence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE, null, msgs);
			msgs = basicSetSequence(newSequence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE, newSequence, newSequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReactiveSequenceType getReactiveSequence() {
		return reactiveSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReactiveSequence(ReactiveSequenceType newReactiveSequence, NotificationChain msgs) {
		ReactiveSequenceType oldReactiveSequence = reactiveSequence;
		reactiveSequence = newReactiveSequence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE, oldReactiveSequence, newReactiveSequence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReactiveSequence(ReactiveSequenceType newReactiveSequence) {
		if (newReactiveSequence != reactiveSequence) {
			NotificationChain msgs = null;
			if (reactiveSequence != null)
				msgs = ((InternalEObject)reactiveSequence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE, null, msgs);
			if (newReactiveSequence != null)
				msgs = ((InternalEObject)newReactiveSequence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE, null, msgs);
			msgs = basicSetReactiveSequence(newReactiveSequence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE, newReactiveSequence, newReactiveSequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FallbackType getFallback() {
		return fallback;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFallback(FallbackType newFallback, NotificationChain msgs) {
		FallbackType oldFallback = fallback;
		fallback = newFallback;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK, oldFallback, newFallback);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFallback(FallbackType newFallback) {
		if (newFallback != fallback) {
			NotificationChain msgs = null;
			if (fallback != null)
				msgs = ((InternalEObject)fallback).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK, null, msgs);
			if (newFallback != null)
				msgs = ((InternalEObject)newFallback).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK, null, msgs);
			msgs = basicSetFallback(newFallback, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK, newFallback, newFallback));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReactiveFallbackType getReactiveFallback() {
		return reactiveFallback;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReactiveFallback(ReactiveFallbackType newReactiveFallback, NotificationChain msgs) {
		ReactiveFallbackType oldReactiveFallback = reactiveFallback;
		reactiveFallback = newReactiveFallback;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK, oldReactiveFallback, newReactiveFallback);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReactiveFallback(ReactiveFallbackType newReactiveFallback) {
		if (newReactiveFallback != reactiveFallback) {
			NotificationChain msgs = null;
			if (reactiveFallback != null)
				msgs = ((InternalEObject)reactiveFallback).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK, null, msgs);
			if (newReactiveFallback != null)
				msgs = ((InternalEObject)newReactiveFallback).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK, null, msgs);
			msgs = basicSetReactiveFallback(newReactiveFallback, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK, newReactiveFallback, newReactiveFallback));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParallelType getParallel() {
		return parallel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParallel(ParallelType newParallel, NotificationChain msgs) {
		ParallelType oldParallel = parallel;
		parallel = newParallel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL, oldParallel, newParallel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParallel(ParallelType newParallel) {
		if (newParallel != parallel) {
			NotificationChain msgs = null;
			if (parallel != null)
				msgs = ((InternalEObject)parallel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL, null, msgs);
			if (newParallel != null)
				msgs = ((InternalEObject)newParallel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL, null, msgs);
			msgs = basicSetParallel(newParallel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL, newParallel, newParallel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InverterType getInverter() {
		return inverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInverter(InverterType newInverter, NotificationChain msgs) {
		InverterType oldInverter = inverter;
		inverter = newInverter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER, oldInverter, newInverter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInverter(InverterType newInverter) {
		if (newInverter != inverter) {
			NotificationChain msgs = null;
			if (inverter != null)
				msgs = ((InternalEObject)inverter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER, null, msgs);
			if (newInverter != null)
				msgs = ((InternalEObject)newInverter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER, null, msgs);
			msgs = basicSetInverter(newInverter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER, newInverter, newInverter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RetryType getRetryUntilSuccessful() {
		return retryUntilSuccessful;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRetryUntilSuccessful(RetryType newRetryUntilSuccessful, NotificationChain msgs) {
		RetryType oldRetryUntilSuccessful = retryUntilSuccessful;
		retryUntilSuccessful = newRetryUntilSuccessful;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL, oldRetryUntilSuccessful, newRetryUntilSuccessful);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRetryUntilSuccessful(RetryType newRetryUntilSuccessful) {
		if (newRetryUntilSuccessful != retryUntilSuccessful) {
			NotificationChain msgs = null;
			if (retryUntilSuccessful != null)
				msgs = ((InternalEObject)retryUntilSuccessful).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL, null, msgs);
			if (newRetryUntilSuccessful != null)
				msgs = ((InternalEObject)newRetryUntilSuccessful).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL, null, msgs);
			msgs = basicSetRetryUntilSuccessful(newRetryUntilSuccessful, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL, newRetryUntilSuccessful, newRetryUntilSuccessful));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RepeatType getRepeat() {
		return repeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepeat(RepeatType newRepeat, NotificationChain msgs) {
		RepeatType oldRepeat = repeat;
		repeat = newRepeat;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT, oldRepeat, newRepeat);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepeat(RepeatType newRepeat) {
		if (newRepeat != repeat) {
			NotificationChain msgs = null;
			if (repeat != null)
				msgs = ((InternalEObject)repeat).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT, null, msgs);
			if (newRepeat != null)
				msgs = ((InternalEObject)newRepeat).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT, null, msgs);
			msgs = basicSetRepeat(newRepeat, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT, newRepeat, newRepeat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeoutType getTimeout() {
		return timeout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimeout(TimeoutType newTimeout, NotificationChain msgs) {
		TimeoutType oldTimeout = timeout;
		timeout = newTimeout;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT, oldTimeout, newTimeout);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeout(TimeoutType newTimeout) {
		if (newTimeout != timeout) {
			NotificationChain msgs = null;
			if (timeout != null)
				msgs = ((InternalEObject)timeout).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT, null, msgs);
			if (newTimeout != null)
				msgs = ((InternalEObject)newTimeout).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT, null, msgs);
			msgs = basicSetTimeout(newTimeout, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT, newTimeout, newTimeout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForceSuccesType getForceSucces() {
		return forceSucces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForceSucces(ForceSuccesType newForceSucces, NotificationChain msgs) {
		ForceSuccesType oldForceSucces = forceSucces;
		forceSucces = newForceSucces;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES, oldForceSucces, newForceSucces);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForceSucces(ForceSuccesType newForceSucces) {
		if (newForceSucces != forceSucces) {
			NotificationChain msgs = null;
			if (forceSucces != null)
				msgs = ((InternalEObject)forceSucces).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES, null, msgs);
			if (newForceSucces != null)
				msgs = ((InternalEObject)newForceSucces).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES, null, msgs);
			msgs = basicSetForceSucces(newForceSucces, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES, newForceSucces, newForceSucces));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForceFailureType getForceFailure() {
		return forceFailure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForceFailure(ForceFailureType newForceFailure, NotificationChain msgs) {
		ForceFailureType oldForceFailure = forceFailure;
		forceFailure = newForceFailure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE, oldForceFailure, newForceFailure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForceFailure(ForceFailureType newForceFailure) {
		if (newForceFailure != forceFailure) {
			NotificationChain msgs = null;
			if (forceFailure != null)
				msgs = ((InternalEObject)forceFailure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE, null, msgs);
			if (newForceFailure != null)
				msgs = ((InternalEObject)newForceFailure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE, null, msgs);
			msgs = basicSetForceFailure(newForceFailure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE, newForceFailure, newForceFailure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlwaysSuccesType getAlwaysSucces() {
		return alwaysSucces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlwaysSucces(AlwaysSuccesType newAlwaysSucces, NotificationChain msgs) {
		AlwaysSuccesType oldAlwaysSucces = alwaysSucces;
		alwaysSucces = newAlwaysSucces;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES, oldAlwaysSucces, newAlwaysSucces);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlwaysSucces(AlwaysSuccesType newAlwaysSucces) {
		if (newAlwaysSucces != alwaysSucces) {
			NotificationChain msgs = null;
			if (alwaysSucces != null)
				msgs = ((InternalEObject)alwaysSucces).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES, null, msgs);
			if (newAlwaysSucces != null)
				msgs = ((InternalEObject)newAlwaysSucces).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES, null, msgs);
			msgs = basicSetAlwaysSucces(newAlwaysSucces, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES, newAlwaysSucces, newAlwaysSucces));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlwaysFailureType getAlwaysFailure() {
		return alwaysFailure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlwaysFailure(AlwaysFailureType newAlwaysFailure, NotificationChain msgs) {
		AlwaysFailureType oldAlwaysFailure = alwaysFailure;
		alwaysFailure = newAlwaysFailure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE, oldAlwaysFailure, newAlwaysFailure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlwaysFailure(AlwaysFailureType newAlwaysFailure) {
		if (newAlwaysFailure != alwaysFailure) {
			NotificationChain msgs = null;
			if (alwaysFailure != null)
				msgs = ((InternalEObject)alwaysFailure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE, null, msgs);
			if (newAlwaysFailure != null)
				msgs = ((InternalEObject)newAlwaysFailure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE, null, msgs);
			msgs = basicSetAlwaysFailure(newAlwaysFailure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE, newAlwaysFailure, newAlwaysFailure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION:
				return basicSetAction(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION:
				return basicSetCondition(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL:
				return basicSetControl(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR:
				return basicSetDecorator(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS:
				return basicSetSubtreeplus(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE:
				return basicSetSequence(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE:
				return basicSetReactiveSequence(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK:
				return basicSetFallback(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK:
				return basicSetReactiveFallback(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL:
				return basicSetParallel(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER:
				return basicSetInverter(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL:
				return basicSetRetryUntilSuccessful(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT:
				return basicSetRepeat(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT:
				return basicSetTimeout(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES:
				return basicSetForceSucces(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE:
				return basicSetForceFailure(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES:
				return basicSetAlwaysSucces(null, msgs);
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE:
				return basicSetAlwaysFailure(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION:
				return getAction();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION:
				return getCondition();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL:
				return getControl();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR:
				return getDecorator();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS:
				return getSubtreeplus();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE:
				return getSequence();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE:
				return getReactiveSequence();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK:
				return getFallback();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK:
				return getReactiveFallback();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL:
				return getParallel();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER:
				return getInverter();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL:
				return getRetryUntilSuccessful();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT:
				return getRepeat();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT:
				return getTimeout();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES:
				return getForceSucces();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE:
				return getForceFailure();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES:
				return getAlwaysSucces();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE:
				return getAlwaysFailure();
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ID:
				return getID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION:
				setAction((ActionType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION:
				setCondition((ConditionType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL:
				setControl((ControlType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR:
				setDecorator((DecoratorType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS:
				setSubtreeplus((SubTreePlusType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE:
				setSequence((SequenceType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE:
				setReactiveSequence((ReactiveSequenceType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK:
				setFallback((FallbackType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK:
				setReactiveFallback((ReactiveFallbackType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL:
				setParallel((ParallelType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER:
				setInverter((InverterType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL:
				setRetryUntilSuccessful((RetryType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT:
				setRepeat((RepeatType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT:
				setTimeout((TimeoutType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES:
				setForceSucces((ForceSuccesType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE:
				setForceFailure((ForceFailureType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES:
				setAlwaysSucces((AlwaysSuccesType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE:
				setAlwaysFailure((AlwaysFailureType)newValue);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ID:
				setID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION:
				setAction((ActionType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION:
				setCondition((ConditionType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL:
				setControl((ControlType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR:
				setDecorator((DecoratorType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS:
				setSubtreeplus((SubTreePlusType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE:
				setSequence((SequenceType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE:
				setReactiveSequence((ReactiveSequenceType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK:
				setFallback((FallbackType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK:
				setReactiveFallback((ReactiveFallbackType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL:
				setParallel((ParallelType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER:
				setInverter((InverterType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL:
				setRetryUntilSuccessful((RetryType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT:
				setRepeat((RepeatType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT:
				setTimeout((TimeoutType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES:
				setForceSucces((ForceSuccesType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE:
				setForceFailure((ForceFailureType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES:
				setAlwaysSucces((AlwaysSuccesType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE:
				setAlwaysFailure((AlwaysFailureType)null);
				return;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ID:
				setID(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ACTION:
				return action != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONDITION:
				return condition != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__CONTROL:
				return control != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__DECORATOR:
				return decorator != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SUBTREEPLUS:
				return subtreeplus != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__SEQUENCE:
				return sequence != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE:
				return reactiveSequence != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FALLBACK:
				return fallback != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK:
				return reactiveFallback != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__PARALLEL:
				return parallel != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__INVERTER:
				return inverter != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL:
				return retryUntilSuccessful != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__REPEAT:
				return repeat != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__TIMEOUT:
				return timeout != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_SUCCES:
				return forceSucces != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__FORCE_FAILURE:
				return forceFailure != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES:
				return alwaysSucces != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE:
				return alwaysFailure != null;
			case BehaviortreeSchemaPackage.BEHAVIOR_TREE_TYPE__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iD: ");
		result.append(iD);
		result.append(')');
		return result.toString();
	}

} //BehaviorTreeTypeImpl
