/**
 * Copyright (c) 2019, 2023 CEA LIST.
 * Copyright (c) 2014-2018 Michele Colledanchise Copyright (c) 2018-2019 Davide Faconti
 */
package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fallback Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAction <em>Action</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getCondition <em>Condition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getControl <em>Control</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getDecorator <em>Decorator</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getSubtreeplus <em>Subtreeplus</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getSequence <em>Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getReactiveSequence <em>Reactive Sequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getFallback <em>Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getReactiveFallback <em>Reactive Fallback</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getParallel <em>Parallel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getInverter <em>Inverter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getRetryUntilSuccessful <em>Retry Until Successful</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getRepeat <em>Repeat</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getTimeout <em>Timeout</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getForceSucces <em>Force Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getForceFailure <em>Force Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAlwaysSucces <em>Always Succes</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAlwaysFailure <em>Always Failure</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType()
 * @model extendedMetaData="name='FallbackType' kind='elementOnly'"
 * @generated
 */
public interface FallbackType extends EObject {
	/**
	 * Returns the value of the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Built In Multiple Types</em>' attribute list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_BuiltInMultipleTypes()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	FeatureMap getBuiltInMultipleTypes();

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Action()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Action' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ActionType> getAction();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Condition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Condition' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ConditionType> getCondition();

	/**
	 * Returns the value of the '<em><b>Control</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Control</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Control()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Control' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ControlType> getControl();

	/**
	 * Returns the value of the '<em><b>Decorator</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decorator</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Decorator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Decorator' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<DecoratorType> getDecorator();

	/**
	 * Returns the value of the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtreeplus</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Subtreeplus()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SubTreePlus' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<SubTreePlusType> getSubtreeplus();

	/**
	 * Returns the value of the '<em><b>Sequence</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Sequence()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Sequence' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<SequenceType> getSequence();

	/**
	 * Returns the value of the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reactive Sequence</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_ReactiveSequence()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ReactiveSequence' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ReactiveSequenceType> getReactiveSequence();

	/**
	 * Returns the value of the '<em><b>Fallback</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fallback</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Fallback()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Fallback' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<FallbackType> getFallback();

	/**
	 * Returns the value of the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reactive Fallback</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_ReactiveFallback()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ReactiveFallback' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ReactiveFallbackType> getReactiveFallback();

	/**
	 * Returns the value of the '<em><b>Parallel</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parallel</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Parallel()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Parallel' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ParallelType> getParallel();

	/**
	 * Returns the value of the '<em><b>Inverter</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inverter</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Inverter()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Inverter' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<InverterType> getInverter();

	/**
	 * Returns the value of the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Retry Until Successful</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_RetryUntilSuccessful()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RetryUntilSuccessful' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<RetryType> getRetryUntilSuccessful();

	/**
	 * Returns the value of the '<em><b>Repeat</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repeat</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Repeat()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Repeat' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<RepeatType> getRepeat();

	/**
	 * Returns the value of the '<em><b>Timeout</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timeout</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Timeout()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Timeout' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<TimeoutType> getTimeout();

	/**
	 * Returns the value of the '<em><b>Force Succes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Force Succes</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_ForceSucces()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ForceSucces' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ForceSuccesType> getForceSucces();

	/**
	 * Returns the value of the '<em><b>Force Failure</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Force Failure</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_ForceFailure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ForceFailure' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<ForceFailureType> getForceFailure();

	/**
	 * Returns the value of the '<em><b>Always Succes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Always Succes</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_AlwaysSucces()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AlwaysSucces' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<AlwaysSuccesType> getAlwaysSucces();

	/**
	 * Returns the value of the '<em><b>Always Failure</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Always Failure</em>' containment reference list.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_AlwaysFailure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='AlwaysFailure' namespace='##targetNamespace' group='BuiltInMultipleTypes:0'"
	 * @generated
	 */
	EList<AlwaysFailureType> getAlwaysFailure();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaPackage#getFallbackType_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // FallbackType
