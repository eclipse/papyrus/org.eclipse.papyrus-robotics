/**
 * Copyright (c) 2019, 2023 CEA LIST.
 * Copyright (c) 2014-2018 Michele Colledanchise Copyright (c) 2018-2019 Davide Faconti
 */
package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviortreeSchemaFactory
 * @model kind="package"
 *        extendedMetaData="qualified='false'"
 * @generated
 */
public interface BehaviortreeSchemaPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "BehaviortreeSchema";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://github.com/BehaviorTree/BehaviorTree.CPP/behaviortree_schema.xsd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "BehaviortreeSchema";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BehaviortreeSchemaPackage eINSTANCE = org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ActionTypeImpl <em>Action Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ActionTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getActionType()
	 * @generated
	 */
	int ACTION_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_TYPE__ID = 1;

	/**
	 * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_TYPE__ANY_ATTRIBUTE = 2;

	/**
	 * The number of structural features of the '<em>Action Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Action Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysFailureTypeImpl <em>Always Failure Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysFailureTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getAlwaysFailureType()
	 * @generated
	 */
	int ALWAYS_FAILURE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALWAYS_FAILURE_TYPE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Always Failure Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALWAYS_FAILURE_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Always Failure Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALWAYS_FAILURE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysSuccesTypeImpl <em>Always Succes Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysSuccesTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getAlwaysSuccesType()
	 * @generated
	 */
	int ALWAYS_SUCCES_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALWAYS_SUCCES_TYPE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Always Succes Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALWAYS_SUCCES_TYPE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Always Succes Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALWAYS_SUCCES_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl <em>Behavior Tree Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getBehaviorTreeType()
	 * @generated
	 */
	int BEHAVIOR_TREE_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE__ID = 18;

	/**
	 * The number of structural features of the '<em>Behavior Tree Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE_FEATURE_COUNT = 19;

	/**
	 * The number of operations of the '<em>Behavior Tree Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TREE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ConditionTypeImpl <em>Condition Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ConditionTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getConditionType()
	 * @generated
	 */
	int CONDITION_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TYPE__ID = 1;

	/**
	 * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TYPE__ANY_ATTRIBUTE = 2;

	/**
	 * The number of structural features of the '<em>Condition Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Condition Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ControlTypeImpl <em>Control Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ControlTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getControlType()
	 * @generated
	 */
	int CONTROL_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__BUILT_IN_MULTIPLE_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__CONTROL = 3;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__DECORATOR = 4;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__SUBTREEPLUS = 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__REACTIVE_SEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__REACTIVE_FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__PARALLEL = 10;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__INVERTER = 11;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__RETRY_UNTIL_SUCCESSFUL = 12;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__REPEAT = 13;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__TIMEOUT = 14;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__FORCE_SUCCES = 15;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__FORCE_FAILURE = 16;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__ALWAYS_SUCCES = 17;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__ALWAYS_FAILURE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__NAME = 19;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__ID = 20;

	/**
	 * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE__ANY_ATTRIBUTE = 21;

	/**
	 * The number of structural features of the '<em>Control Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE_FEATURE_COUNT = 22;

	/**
	 * The number of operations of the '<em>Control Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DecoratorTypeImpl <em>Decorator Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DecoratorTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getDecoratorType()
	 * @generated
	 */
	int DECORATOR_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__NAME = 18;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__ID = 19;

	/**
	 * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE__ANY_ATTRIBUTE = 20;

	/**
	 * The number of structural features of the '<em>Decorator Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE_FEATURE_COUNT = 21;

	/**
	 * The number of operations of the '<em>Decorator Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DocumentRootImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 7;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__ROOT = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl <em>Reactive Fallback Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getReactiveFallbackType()
	 * @generated
	 */
	int REACTIVE_FALLBACK_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__CONTROL = 3;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__DECORATOR = 4;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__SUBTREEPLUS = 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__PARALLEL = 10;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__INVERTER = 11;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL = 12;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__REPEAT = 13;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__TIMEOUT = 14;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__FORCE_SUCCES = 15;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__FORCE_FAILURE = 16;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES = 17;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE__NAME = 19;

	/**
	 * The number of structural features of the '<em>Reactive Fallback Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Reactive Fallback Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.FallbackTypeImpl <em>Fallback Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.FallbackTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getFallbackType()
	 * @generated
	 */
	int FALLBACK_TYPE = 9;

	/**
	 * The feature id for the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__CONTROL = 3;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__DECORATOR = 4;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__SUBTREEPLUS = 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__REACTIVE_SEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__REACTIVE_FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__PARALLEL = 10;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__INVERTER = 11;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL = 12;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__REPEAT = 13;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__TIMEOUT = 14;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__FORCE_SUCCES = 15;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__FORCE_FAILURE = 16;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__ALWAYS_SUCCES = 17;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__ALWAYS_FAILURE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE__NAME = 19;

	/**
	 * The number of structural features of the '<em>Fallback Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Fallback Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceFailureTypeImpl <em>Force Failure Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceFailureTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getForceFailureType()
	 * @generated
	 */
	int FORCE_FAILURE_TYPE = 10;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE__NAME = 18;

	/**
	 * The number of structural features of the '<em>Force Failure Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE_FEATURE_COUNT = 19;

	/**
	 * The number of operations of the '<em>Force Failure Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_FAILURE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceSuccesTypeImpl <em>Force Succes Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceSuccesTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getForceSuccesType()
	 * @generated
	 */
	int FORCE_SUCCES_TYPE = 11;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE__NAME = 18;

	/**
	 * The number of structural features of the '<em>Force Succes Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE_FEATURE_COUNT = 19;

	/**
	 * The number of operations of the '<em>Force Succes Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORCE_SUCCES_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.InverterTypeImpl <em>Inverter Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.InverterTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getInverterType()
	 * @generated
	 */
	int INVERTER_TYPE = 12;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE__NAME = 18;

	/**
	 * The number of structural features of the '<em>Inverter Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE_FEATURE_COUNT = 19;

	/**
	 * The number of operations of the '<em>Inverter Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ParallelTypeImpl <em>Parallel Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ParallelTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getParallelType()
	 * @generated
	 */
	int PARALLEL_TYPE = 13;

	/**
	 * The feature id for the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__BUILT_IN_MULTIPLE_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__CONTROL = 3;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__DECORATOR = 4;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__SUBTREEPLUS = 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__REACTIVE_SEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__REACTIVE_FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__PARALLEL = 10;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__INVERTER = 11;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__RETRY_UNTIL_SUCCESSFUL = 12;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__REPEAT = 13;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__TIMEOUT = 14;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__FORCE_SUCCES = 15;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__FORCE_FAILURE = 16;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__ALWAYS_SUCCES = 17;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__ALWAYS_FAILURE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__NAME = 19;

	/**
	 * The feature id for the '<em><b>Success Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__SUCCESS_THRESHOLD = 20;

	/**
	 * The feature id for the '<em><b>Failure Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE__FAILURE_THRESHOLD = 21;

	/**
	 * The number of structural features of the '<em>Parallel Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE_FEATURE_COUNT = 22;

	/**
	 * The number of operations of the '<em>Parallel Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RepeatTypeImpl <em>Repeat Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RepeatTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getRepeatType()
	 * @generated
	 */
	int REPEAT_TYPE = 14;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__NAME = 18;

	/**
	 * The feature id for the '<em><b>Num Cycles</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE__NUM_CYCLES = 19;

	/**
	 * The number of structural features of the '<em>Repeat Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Repeat Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RetryTypeImpl <em>Retry Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RetryTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getRetryType()
	 * @generated
	 */
	int RETRY_TYPE = 15;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__NAME = 18;

	/**
	 * The feature id for the '<em><b>Num Attempts</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE__NUM_ATTEMPTS = 19;

	/**
	 * The number of structural features of the '<em>Retry Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Retry Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RootTypeImpl <em>Root Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RootTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getRootType()
	 * @generated
	 */
	int ROOT_TYPE = 16;

	/**
	 * The feature id for the '<em><b>Behavior Tree</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_TYPE__BEHAVIOR_TREE = 0;

	/**
	 * The feature id for the '<em><b>Tree Node Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_TYPE__TREE_NODE_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Main Tree To Execute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_TYPE__MAIN_TREE_TO_EXECUTE = 2;

	/**
	 * The number of structural features of the '<em>Root Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_TYPE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Root Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveSequenceTypeImpl <em>Reactive Sequence Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveSequenceTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getReactiveSequenceType()
	 * @generated
	 */
	int REACTIVE_SEQUENCE_TYPE = 17;

	/**
	 * The feature id for the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__BUILT_IN_MULTIPLE_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__CONTROL = 3;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__DECORATOR = 4;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__SUBTREEPLUS = 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__REACTIVE_SEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__REACTIVE_FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__PARALLEL = 10;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__INVERTER = 11;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__RETRY_UNTIL_SUCCESSFUL = 12;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__REPEAT = 13;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__TIMEOUT = 14;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__FORCE_SUCCES = 15;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__FORCE_FAILURE = 16;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__ALWAYS_SUCCES = 17;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__ALWAYS_FAILURE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE__NAME = 19;

	/**
	 * The number of structural features of the '<em>Reactive Sequence Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Reactive Sequence Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SequenceTypeImpl <em>Sequence Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SequenceTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getSequenceType()
	 * @generated
	 */
	int SEQUENCE_TYPE = 18;

	/**
	 * The feature id for the '<em><b>Built In Multiple Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__BUILT_IN_MULTIPLE_TYPES = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__CONTROL = 3;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__DECORATOR = 4;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__SUBTREEPLUS = 5;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__REACTIVE_SEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__REACTIVE_FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__PARALLEL = 10;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__INVERTER = 11;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__RETRY_UNTIL_SUCCESSFUL = 12;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__REPEAT = 13;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__TIMEOUT = 14;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__FORCE_SUCCES = 15;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__FORCE_FAILURE = 16;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__ALWAYS_SUCCES = 17;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__ALWAYS_FAILURE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE__NAME = 19;

	/**
	 * The number of structural features of the '<em>Sequence Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Sequence Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SubTreePlusTypeImpl <em>Sub Tree Plus Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SubTreePlusTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getSubTreePlusType()
	 * @generated
	 */
	int SUB_TREE_PLUS_TYPE = 19;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__NAME = 18;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__ID = 19;

	/**
	 * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE__ANY_ATTRIBUTE = 20;

	/**
	 * The number of structural features of the '<em>Sub Tree Plus Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE_FEATURE_COUNT = 21;

	/**
	 * The number of operations of the '<em>Sub Tree Plus Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_PLUS_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TimeoutTypeImpl <em>Timeout Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TimeoutTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getTimeoutType()
	 * @generated
	 */
	int TIMEOUT_TYPE = 20;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__DECORATOR = 3;

	/**
	 * The feature id for the '<em><b>Subtreeplus</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__SUBTREEPLUS = 4;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__SEQUENCE = 5;

	/**
	 * The feature id for the '<em><b>Reactive Sequence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__REACTIVE_SEQUENCE = 6;

	/**
	 * The feature id for the '<em><b>Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__FALLBACK = 7;

	/**
	 * The feature id for the '<em><b>Reactive Fallback</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__REACTIVE_FALLBACK = 8;

	/**
	 * The feature id for the '<em><b>Parallel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__PARALLEL = 9;

	/**
	 * The feature id for the '<em><b>Inverter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__INVERTER = 10;

	/**
	 * The feature id for the '<em><b>Retry Until Successful</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__RETRY_UNTIL_SUCCESSFUL = 11;

	/**
	 * The feature id for the '<em><b>Repeat</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__REPEAT = 12;

	/**
	 * The feature id for the '<em><b>Timeout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__TIMEOUT = 13;

	/**
	 * The feature id for the '<em><b>Force Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__FORCE_SUCCES = 14;

	/**
	 * The feature id for the '<em><b>Force Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__FORCE_FAILURE = 15;

	/**
	 * The feature id for the '<em><b>Always Succes</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__ALWAYS_SUCCES = 16;

	/**
	 * The feature id for the '<em><b>Always Failure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__ALWAYS_FAILURE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__NAME = 18;

	/**
	 * The feature id for the '<em><b>Msec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE__MSEC = 19;

	/**
	 * The number of structural features of the '<em>Timeout Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE_FEATURE_COUNT = 20;

	/**
	 * The number of operations of the '<em>Timeout Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMEOUT_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TreeNodeModelTypeImpl <em>Tree Node Model Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TreeNodeModelTypeImpl
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getTreeNodeModelType()
	 * @generated
	 */
	int TREE_NODE_MODEL_TYPE = 21;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_MODEL_TYPE__ACTION = 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_MODEL_TYPE__CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Decorator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_MODEL_TYPE__DECORATOR = 2;

	/**
	 * The feature id for the '<em><b>Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_MODEL_TYPE__CONTROL = 3;

	/**
	 * The number of structural features of the '<em>Tree Node Model Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_MODEL_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Tree Node Model Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_MODEL_TYPE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType
	 * @generated
	 */
	EClass getActionType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType#getName()
	 * @see #getActionType()
	 * @generated
	 */
	EAttribute getActionType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType#getID()
	 * @see #getActionType()
	 * @generated
	 */
	EAttribute getActionType_ID();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType#getAnyAttribute <em>Any Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ActionType#getAnyAttribute()
	 * @see #getActionType()
	 * @generated
	 */
	EAttribute getActionType_AnyAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType <em>Always Failure Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Always Failure Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType
	 * @generated
	 */
	EClass getAlwaysFailureType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysFailureType#getName()
	 * @see #getAlwaysFailureType()
	 * @generated
	 */
	EAttribute getAlwaysFailureType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType <em>Always Succes Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Always Succes Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType
	 * @generated
	 */
	EClass getAlwaysSuccesType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.AlwaysSuccesType#getName()
	 * @see #getAlwaysSuccesType()
	 * @generated
	 */
	EAttribute getAlwaysSuccesType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType <em>Behavior Tree Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behavior Tree Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType
	 * @generated
	 */
	EClass getBehaviorTreeType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getAction()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getCondition()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getControl()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getDecorator()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getSubtreeplus()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getSequence()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getReactiveSequence()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getFallback()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getReactiveFallback()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getParallel()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getInverter()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getRetryUntilSuccessful()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getRepeat()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getTimeout()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getForceSucces()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getForceFailure()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getAlwaysSucces()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getAlwaysFailure()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EReference getBehaviorTreeType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.BehaviorTreeType#getID()
	 * @see #getBehaviorTreeType()
	 * @generated
	 */
	EAttribute getBehaviorTreeType_ID();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType <em>Condition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType
	 * @generated
	 */
	EClass getConditionType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType#getName()
	 * @see #getConditionType()
	 * @generated
	 */
	EAttribute getConditionType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType#getID()
	 * @see #getConditionType()
	 * @generated
	 */
	EAttribute getConditionType_ID();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType#getAnyAttribute <em>Any Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ConditionType#getAnyAttribute()
	 * @see #getConditionType()
	 * @generated
	 */
	EAttribute getConditionType_AnyAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType <em>Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType
	 * @generated
	 */
	EClass getControlType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Built In Multiple Types</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getBuiltInMultipleTypes()
	 * @see #getControlType()
	 * @generated
	 */
	EAttribute getControlType_BuiltInMultipleTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAction()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getCondition()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getControl()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Control();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getDecorator()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Decorator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getSubtreeplus()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getSequence()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Sequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getReactiveSequence()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getFallback()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Fallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getReactiveFallback()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getParallel()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Parallel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getInverter()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Inverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getRetryUntilSuccessful()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getRepeat()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Repeat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getTimeout()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_Timeout();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getForceSucces()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getForceFailure()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAlwaysSucces()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAlwaysFailure()
	 * @see #getControlType()
	 * @generated
	 */
	EReference getControlType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getName()
	 * @see #getControlType()
	 * @generated
	 */
	EAttribute getControlType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getID()
	 * @see #getControlType()
	 * @generated
	 */
	EAttribute getControlType_ID();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAnyAttribute <em>Any Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ControlType#getAnyAttribute()
	 * @see #getControlType()
	 * @generated
	 */
	EAttribute getControlType_AnyAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType <em>Decorator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decorator Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType
	 * @generated
	 */
	EClass getDecoratorType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAction()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getCondition()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getControl()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getDecorator()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getSubtreeplus()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getSequence()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getReactiveSequence()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getFallback()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getReactiveFallback()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getParallel()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getInverter()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getRetryUntilSuccessful()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getRepeat()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getTimeout()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getForceSucces()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getForceFailure()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAlwaysSucces()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAlwaysFailure()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EReference getDecoratorType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getName()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EAttribute getDecoratorType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getID()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EAttribute getDecoratorType_ID();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAnyAttribute <em>Any Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DecoratorType#getAnyAttribute()
	 * @see #getDecoratorType()
	 * @generated
	 */
	EAttribute getDecoratorType_AnyAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.DocumentRoot#getRoot()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Root();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType <em>Reactive Fallback Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reactive Fallback Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType
	 * @generated
	 */
	EClass getReactiveFallbackType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Built In Multiple Types</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getBuiltInMultipleTypes()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EAttribute getReactiveFallbackType_BuiltInMultipleTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getAction()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getCondition()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getControl()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Control();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getDecorator()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Decorator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getSubtreeplus()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getSequence()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Sequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getReactiveSequence()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getFallback()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Fallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getReactiveFallback()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getParallel()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Parallel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getInverter()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Inverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getRetryUntilSuccessful()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getRepeat()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Repeat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getTimeout()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_Timeout();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getForceSucces()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getForceFailure()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getAlwaysSucces()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getAlwaysFailure()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EReference getReactiveFallbackType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveFallbackType#getName()
	 * @see #getReactiveFallbackType()
	 * @generated
	 */
	EAttribute getReactiveFallbackType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType <em>Fallback Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fallback Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType
	 * @generated
	 */
	EClass getFallbackType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Built In Multiple Types</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getBuiltInMultipleTypes()
	 * @see #getFallbackType()
	 * @generated
	 */
	EAttribute getFallbackType_BuiltInMultipleTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAction()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getCondition()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getControl()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Control();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getDecorator()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Decorator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getSubtreeplus()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getSequence()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Sequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getReactiveSequence()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getFallback()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Fallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getReactiveFallback()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getParallel()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Parallel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getInverter()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Inverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getRetryUntilSuccessful()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getRepeat()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Repeat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getTimeout()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_Timeout();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getForceSucces()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getForceFailure()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAlwaysSucces()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getAlwaysFailure()
	 * @see #getFallbackType()
	 * @generated
	 */
	EReference getFallbackType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.FallbackType#getName()
	 * @see #getFallbackType()
	 * @generated
	 */
	EAttribute getFallbackType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType <em>Force Failure Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Force Failure Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType
	 * @generated
	 */
	EClass getForceFailureType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getAction()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getCondition()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getControl()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getDecorator()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getSubtreeplus()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getSequence()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getReactiveSequence()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getFallback()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getReactiveFallback()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getParallel()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getInverter()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getRetryUntilSuccessful()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getRepeat()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getTimeout()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getForceSucces()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getForceFailure()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getAlwaysSucces()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getAlwaysFailure()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EReference getForceFailureType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceFailureType#getName()
	 * @see #getForceFailureType()
	 * @generated
	 */
	EAttribute getForceFailureType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType <em>Force Succes Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Force Succes Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType
	 * @generated
	 */
	EClass getForceSuccesType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getAction()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getCondition()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getControl()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getDecorator()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getSubtreeplus()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getSequence()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getReactiveSequence()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getFallback()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getReactiveFallback()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getParallel()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getInverter()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getRetryUntilSuccessful()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getRepeat()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getTimeout()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getForceSucces()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getForceFailure()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getAlwaysSucces()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getAlwaysFailure()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EReference getForceSuccesType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ForceSuccesType#getName()
	 * @see #getForceSuccesType()
	 * @generated
	 */
	EAttribute getForceSuccesType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType <em>Inverter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inverter Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType
	 * @generated
	 */
	EClass getInverterType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getAction()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getCondition()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getControl()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getDecorator()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getSubtreeplus()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getSequence()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getReactiveSequence()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getFallback()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getReactiveFallback()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getParallel()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getInverter()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getRetryUntilSuccessful()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getRepeat()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getTimeout()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getForceSucces()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getForceFailure()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getAlwaysSucces()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getAlwaysFailure()
	 * @see #getInverterType()
	 * @generated
	 */
	EReference getInverterType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.InverterType#getName()
	 * @see #getInverterType()
	 * @generated
	 */
	EAttribute getInverterType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType <em>Parallel Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parallel Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType
	 * @generated
	 */
	EClass getParallelType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Built In Multiple Types</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getBuiltInMultipleTypes()
	 * @see #getParallelType()
	 * @generated
	 */
	EAttribute getParallelType_BuiltInMultipleTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getAction()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getCondition()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getControl()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Control();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getDecorator()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Decorator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getSubtreeplus()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getSequence()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Sequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getReactiveSequence()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getFallback()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Fallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getReactiveFallback()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getParallel()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Parallel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getInverter()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Inverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getRetryUntilSuccessful()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getRepeat()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Repeat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getTimeout()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_Timeout();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getForceSucces()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getForceFailure()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getAlwaysSucces()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getAlwaysFailure()
	 * @see #getParallelType()
	 * @generated
	 */
	EReference getParallelType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getName()
	 * @see #getParallelType()
	 * @generated
	 */
	EAttribute getParallelType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getSuccessThreshold <em>Success Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Success Threshold</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getSuccessThreshold()
	 * @see #getParallelType()
	 * @generated
	 */
	EAttribute getParallelType_SuccessThreshold();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getFailureThreshold <em>Failure Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Failure Threshold</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ParallelType#getFailureThreshold()
	 * @see #getParallelType()
	 * @generated
	 */
	EAttribute getParallelType_FailureThreshold();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType <em>Repeat Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repeat Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType
	 * @generated
	 */
	EClass getRepeatType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getAction()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getCondition()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getControl()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getDecorator()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getSubtreeplus()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getSequence()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getReactiveSequence()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getFallback()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getReactiveFallback()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getParallel()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getInverter()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getRetryUntilSuccessful()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getRepeat()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getTimeout()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getForceSucces()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getForceFailure()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getAlwaysSucces()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getAlwaysFailure()
	 * @see #getRepeatType()
	 * @generated
	 */
	EReference getRepeatType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getName()
	 * @see #getRepeatType()
	 * @generated
	 */
	EAttribute getRepeatType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getNumCycles <em>Num Cycles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Cycles</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RepeatType#getNumCycles()
	 * @see #getRepeatType()
	 * @generated
	 */
	EAttribute getRepeatType_NumCycles();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType <em>Retry Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Retry Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType
	 * @generated
	 */
	EClass getRetryType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getAction()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getCondition()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getControl()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getDecorator()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getSubtreeplus()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getSequence()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getReactiveSequence()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getFallback()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getReactiveFallback()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getParallel()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getInverter()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getRetryUntilSuccessful()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getRepeat()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getTimeout()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getForceSucces()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getForceFailure()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getAlwaysSucces()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getAlwaysFailure()
	 * @see #getRetryType()
	 * @generated
	 */
	EReference getRetryType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getName()
	 * @see #getRetryType()
	 * @generated
	 */
	EAttribute getRetryType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getNumAttempts <em>Num Attempts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Attempts</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RetryType#getNumAttempts()
	 * @see #getRetryType()
	 * @generated
	 */
	EAttribute getRetryType_NumAttempts();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType <em>Root Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Root Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType
	 * @generated
	 */
	EClass getRootType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType#getBehaviorTree <em>Behavior Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Behavior Tree</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType#getBehaviorTree()
	 * @see #getRootType()
	 * @generated
	 */
	EReference getRootType_BehaviorTree();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType#getTreeNodeModel <em>Tree Node Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tree Node Model</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType#getTreeNodeModel()
	 * @see #getRootType()
	 * @generated
	 */
	EReference getRootType_TreeNodeModel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType#getMainTreeToExecute <em>Main Tree To Execute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Main Tree To Execute</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.RootType#getMainTreeToExecute()
	 * @see #getRootType()
	 * @generated
	 */
	EAttribute getRootType_MainTreeToExecute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType <em>Reactive Sequence Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reactive Sequence Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType
	 * @generated
	 */
	EClass getReactiveSequenceType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Built In Multiple Types</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getBuiltInMultipleTypes()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EAttribute getReactiveSequenceType_BuiltInMultipleTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getAction()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getCondition()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getControl()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Control();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getDecorator()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Decorator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getSubtreeplus()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getSequence()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Sequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getReactiveSequence()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getFallback()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Fallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getReactiveFallback()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getParallel()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Parallel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getInverter()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Inverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getRetryUntilSuccessful()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getRepeat()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Repeat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getTimeout()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_Timeout();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getForceSucces()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getForceFailure()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getAlwaysSucces()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getAlwaysFailure()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EReference getReactiveSequenceType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.ReactiveSequenceType#getName()
	 * @see #getReactiveSequenceType()
	 * @generated
	 */
	EAttribute getReactiveSequenceType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType <em>Sequence Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType
	 * @generated
	 */
	EClass getSequenceType();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getBuiltInMultipleTypes <em>Built In Multiple Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Built In Multiple Types</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getBuiltInMultipleTypes()
	 * @see #getSequenceType()
	 * @generated
	 */
	EAttribute getSequenceType_BuiltInMultipleTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getAction()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Action();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getCondition()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getControl()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Control();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getDecorator()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Decorator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getSubtreeplus()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getSequence()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Sequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getReactiveSequence()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getFallback()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Fallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getReactiveFallback()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getParallel()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Parallel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getInverter()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Inverter();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getRetryUntilSuccessful()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getRepeat()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Repeat();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getTimeout()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_Timeout();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getForceSucces()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getForceFailure()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getAlwaysSucces()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getAlwaysFailure()
	 * @see #getSequenceType()
	 * @generated
	 */
	EReference getSequenceType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SequenceType#getName()
	 * @see #getSequenceType()
	 * @generated
	 */
	EAttribute getSequenceType_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType <em>Sub Tree Plus Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Tree Plus Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType
	 * @generated
	 */
	EClass getSubTreePlusType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAction()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getCondition()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getControl()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getDecorator()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getSubtreeplus()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getSequence()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getReactiveSequence()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getFallback()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getReactiveFallback()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getParallel()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getInverter()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getRetryUntilSuccessful()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getRepeat()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getTimeout()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getForceSucces()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getForceFailure()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAlwaysSucces()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAlwaysFailure()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EReference getSubTreePlusType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getName()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EAttribute getSubTreePlusType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getID()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EAttribute getSubTreePlusType_ID();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAnyAttribute <em>Any Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Any Attribute</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.SubTreePlusType#getAnyAttribute()
	 * @see #getSubTreePlusType()
	 * @generated
	 */
	EAttribute getSubTreePlusType_AnyAttribute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType <em>Timeout Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timeout Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType
	 * @generated
	 */
	EClass getTimeoutType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAction()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getCondition()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getControl()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Control();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getDecorator()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSubtreeplus <em>Subtreeplus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subtreeplus</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSubtreeplus()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Subtreeplus();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getSequence()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Sequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveSequence()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_ReactiveSequence();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getFallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getFallback()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Fallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getReactiveFallback()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_ReactiveFallback();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getParallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parallel</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getParallel()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Parallel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getInverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getInverter()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Inverter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRetryUntilSuccessful()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_RetryUntilSuccessful();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRepeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getRepeat()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Repeat();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getTimeout <em>Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timeout</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getTimeout()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_Timeout();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceSucces <em>Force Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceSucces()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_ForceSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceFailure <em>Force Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Force Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getForceFailure()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_ForceFailure();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysSucces <em>Always Succes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Succes</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysSucces()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_AlwaysSucces();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysFailure <em>Always Failure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Always Failure</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getAlwaysFailure()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EReference getTimeoutType_AlwaysFailure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getName()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EAttribute getTimeoutType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getMsec <em>Msec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Msec</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TimeoutType#getMsec()
	 * @see #getTimeoutType()
	 * @generated
	 */
	EAttribute getTimeoutType_Msec();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType <em>Tree Node Model Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tree Node Model Type</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType
	 * @generated
	 */
	EClass getTreeNodeModelType();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getAction()
	 * @see #getTreeNodeModelType()
	 * @generated
	 */
	EReference getTreeNodeModelType_Action();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getCondition()
	 * @see #getTreeNodeModelType()
	 * @generated
	 */
	EReference getTreeNodeModelType_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getDecorator <em>Decorator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decorator</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getDecorator()
	 * @see #getTreeNodeModelType()
	 * @generated
	 */
	EReference getTreeNodeModelType_Decorator();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Control</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.TreeNodeModelType#getControl()
	 * @see #getTreeNodeModelType()
	 * @generated
	 */
	EReference getTreeNodeModelType_Control();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BehaviortreeSchemaFactory getBehaviortreeSchemaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ActionTypeImpl <em>Action Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ActionTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getActionType()
		 * @generated
		 */
		EClass ACTION_TYPE = eINSTANCE.getActionType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_TYPE__NAME = eINSTANCE.getActionType_Name();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_TYPE__ID = eINSTANCE.getActionType_ID();

		/**
		 * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_TYPE__ANY_ATTRIBUTE = eINSTANCE.getActionType_AnyAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysFailureTypeImpl <em>Always Failure Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysFailureTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getAlwaysFailureType()
		 * @generated
		 */
		EClass ALWAYS_FAILURE_TYPE = eINSTANCE.getAlwaysFailureType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALWAYS_FAILURE_TYPE__NAME = eINSTANCE.getAlwaysFailureType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysSuccesTypeImpl <em>Always Succes Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.AlwaysSuccesTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getAlwaysSuccesType()
		 * @generated
		 */
		EClass ALWAYS_SUCCES_TYPE = eINSTANCE.getAlwaysSuccesType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALWAYS_SUCCES_TYPE__NAME = eINSTANCE.getAlwaysSuccesType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl <em>Behavior Tree Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviorTreeTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getBehaviorTreeType()
		 * @generated
		 */
		EClass BEHAVIOR_TREE_TYPE = eINSTANCE.getBehaviorTreeType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__ACTION = eINSTANCE.getBehaviorTreeType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__CONDITION = eINSTANCE.getBehaviorTreeType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__CONTROL = eINSTANCE.getBehaviorTreeType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__DECORATOR = eINSTANCE.getBehaviorTreeType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__SUBTREEPLUS = eINSTANCE.getBehaviorTreeType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__SEQUENCE = eINSTANCE.getBehaviorTreeType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getBehaviorTreeType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__FALLBACK = eINSTANCE.getBehaviorTreeType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__REACTIVE_FALLBACK = eINSTANCE.getBehaviorTreeType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__PARALLEL = eINSTANCE.getBehaviorTreeType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__INVERTER = eINSTANCE.getBehaviorTreeType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getBehaviorTreeType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__REPEAT = eINSTANCE.getBehaviorTreeType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__TIMEOUT = eINSTANCE.getBehaviorTreeType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__FORCE_SUCCES = eINSTANCE.getBehaviorTreeType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__FORCE_FAILURE = eINSTANCE.getBehaviorTreeType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__ALWAYS_SUCCES = eINSTANCE.getBehaviorTreeType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TREE_TYPE__ALWAYS_FAILURE = eINSTANCE.getBehaviorTreeType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEHAVIOR_TREE_TYPE__ID = eINSTANCE.getBehaviorTreeType_ID();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ConditionTypeImpl <em>Condition Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ConditionTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getConditionType()
		 * @generated
		 */
		EClass CONDITION_TYPE = eINSTANCE.getConditionType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_TYPE__NAME = eINSTANCE.getConditionType_Name();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_TYPE__ID = eINSTANCE.getConditionType_ID();

		/**
		 * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_TYPE__ANY_ATTRIBUTE = eINSTANCE.getConditionType_AnyAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ControlTypeImpl <em>Control Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ControlTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getControlType()
		 * @generated
		 */
		EClass CONTROL_TYPE = eINSTANCE.getControlType();

		/**
		 * The meta object literal for the '<em><b>Built In Multiple Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_TYPE__BUILT_IN_MULTIPLE_TYPES = eINSTANCE.getControlType_BuiltInMultipleTypes();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__ACTION = eINSTANCE.getControlType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__CONDITION = eINSTANCE.getControlType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__CONTROL = eINSTANCE.getControlType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__DECORATOR = eINSTANCE.getControlType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__SUBTREEPLUS = eINSTANCE.getControlType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__SEQUENCE = eINSTANCE.getControlType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getControlType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__FALLBACK = eINSTANCE.getControlType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__REACTIVE_FALLBACK = eINSTANCE.getControlType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__PARALLEL = eINSTANCE.getControlType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__INVERTER = eINSTANCE.getControlType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getControlType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__REPEAT = eINSTANCE.getControlType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__TIMEOUT = eINSTANCE.getControlType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__FORCE_SUCCES = eINSTANCE.getControlType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__FORCE_FAILURE = eINSTANCE.getControlType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__ALWAYS_SUCCES = eINSTANCE.getControlType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_TYPE__ALWAYS_FAILURE = eINSTANCE.getControlType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_TYPE__NAME = eINSTANCE.getControlType_Name();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_TYPE__ID = eINSTANCE.getControlType_ID();

		/**
		 * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_TYPE__ANY_ATTRIBUTE = eINSTANCE.getControlType_AnyAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DecoratorTypeImpl <em>Decorator Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DecoratorTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getDecoratorType()
		 * @generated
		 */
		EClass DECORATOR_TYPE = eINSTANCE.getDecoratorType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__ACTION = eINSTANCE.getDecoratorType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__CONDITION = eINSTANCE.getDecoratorType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__CONTROL = eINSTANCE.getDecoratorType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__DECORATOR = eINSTANCE.getDecoratorType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__SUBTREEPLUS = eINSTANCE.getDecoratorType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__SEQUENCE = eINSTANCE.getDecoratorType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getDecoratorType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__FALLBACK = eINSTANCE.getDecoratorType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__REACTIVE_FALLBACK = eINSTANCE.getDecoratorType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__PARALLEL = eINSTANCE.getDecoratorType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__INVERTER = eINSTANCE.getDecoratorType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getDecoratorType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__REPEAT = eINSTANCE.getDecoratorType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__TIMEOUT = eINSTANCE.getDecoratorType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__FORCE_SUCCES = eINSTANCE.getDecoratorType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__FORCE_FAILURE = eINSTANCE.getDecoratorType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__ALWAYS_SUCCES = eINSTANCE.getDecoratorType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_TYPE__ALWAYS_FAILURE = eINSTANCE.getDecoratorType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATOR_TYPE__NAME = eINSTANCE.getDecoratorType_Name();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATOR_TYPE__ID = eINSTANCE.getDecoratorType_ID();

		/**
		 * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECORATOR_TYPE__ANY_ATTRIBUTE = eINSTANCE.getDecoratorType_AnyAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.DocumentRootImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__ROOT = eINSTANCE.getDocumentRoot_Root();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl <em>Reactive Fallback Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveFallbackTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getReactiveFallbackType()
		 * @generated
		 */
		EClass REACTIVE_FALLBACK_TYPE = eINSTANCE.getReactiveFallbackType();

		/**
		 * The meta object literal for the '<em><b>Built In Multiple Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REACTIVE_FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES = eINSTANCE.getReactiveFallbackType_BuiltInMultipleTypes();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__ACTION = eINSTANCE.getReactiveFallbackType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__CONDITION = eINSTANCE.getReactiveFallbackType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__CONTROL = eINSTANCE.getReactiveFallbackType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__DECORATOR = eINSTANCE.getReactiveFallbackType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__SUBTREEPLUS = eINSTANCE.getReactiveFallbackType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__SEQUENCE = eINSTANCE.getReactiveFallbackType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getReactiveFallbackType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__FALLBACK = eINSTANCE.getReactiveFallbackType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__REACTIVE_FALLBACK = eINSTANCE.getReactiveFallbackType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__PARALLEL = eINSTANCE.getReactiveFallbackType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__INVERTER = eINSTANCE.getReactiveFallbackType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getReactiveFallbackType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__REPEAT = eINSTANCE.getReactiveFallbackType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__TIMEOUT = eINSTANCE.getReactiveFallbackType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__FORCE_SUCCES = eINSTANCE.getReactiveFallbackType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__FORCE_FAILURE = eINSTANCE.getReactiveFallbackType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__ALWAYS_SUCCES = eINSTANCE.getReactiveFallbackType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_FALLBACK_TYPE__ALWAYS_FAILURE = eINSTANCE.getReactiveFallbackType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REACTIVE_FALLBACK_TYPE__NAME = eINSTANCE.getReactiveFallbackType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.FallbackTypeImpl <em>Fallback Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.FallbackTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getFallbackType()
		 * @generated
		 */
		EClass FALLBACK_TYPE = eINSTANCE.getFallbackType();

		/**
		 * The meta object literal for the '<em><b>Built In Multiple Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FALLBACK_TYPE__BUILT_IN_MULTIPLE_TYPES = eINSTANCE.getFallbackType_BuiltInMultipleTypes();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__ACTION = eINSTANCE.getFallbackType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__CONDITION = eINSTANCE.getFallbackType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__CONTROL = eINSTANCE.getFallbackType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__DECORATOR = eINSTANCE.getFallbackType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__SUBTREEPLUS = eINSTANCE.getFallbackType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__SEQUENCE = eINSTANCE.getFallbackType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getFallbackType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__FALLBACK = eINSTANCE.getFallbackType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__REACTIVE_FALLBACK = eINSTANCE.getFallbackType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__PARALLEL = eINSTANCE.getFallbackType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__INVERTER = eINSTANCE.getFallbackType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getFallbackType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__REPEAT = eINSTANCE.getFallbackType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__TIMEOUT = eINSTANCE.getFallbackType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__FORCE_SUCCES = eINSTANCE.getFallbackType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__FORCE_FAILURE = eINSTANCE.getFallbackType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__ALWAYS_SUCCES = eINSTANCE.getFallbackType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FALLBACK_TYPE__ALWAYS_FAILURE = eINSTANCE.getFallbackType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FALLBACK_TYPE__NAME = eINSTANCE.getFallbackType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceFailureTypeImpl <em>Force Failure Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceFailureTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getForceFailureType()
		 * @generated
		 */
		EClass FORCE_FAILURE_TYPE = eINSTANCE.getForceFailureType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__ACTION = eINSTANCE.getForceFailureType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__CONDITION = eINSTANCE.getForceFailureType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__CONTROL = eINSTANCE.getForceFailureType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__DECORATOR = eINSTANCE.getForceFailureType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__SUBTREEPLUS = eINSTANCE.getForceFailureType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__SEQUENCE = eINSTANCE.getForceFailureType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getForceFailureType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__FALLBACK = eINSTANCE.getForceFailureType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__REACTIVE_FALLBACK = eINSTANCE.getForceFailureType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__PARALLEL = eINSTANCE.getForceFailureType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__INVERTER = eINSTANCE.getForceFailureType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getForceFailureType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__REPEAT = eINSTANCE.getForceFailureType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__TIMEOUT = eINSTANCE.getForceFailureType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__FORCE_SUCCES = eINSTANCE.getForceFailureType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__FORCE_FAILURE = eINSTANCE.getForceFailureType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__ALWAYS_SUCCES = eINSTANCE.getForceFailureType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_FAILURE_TYPE__ALWAYS_FAILURE = eINSTANCE.getForceFailureType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORCE_FAILURE_TYPE__NAME = eINSTANCE.getForceFailureType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceSuccesTypeImpl <em>Force Succes Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ForceSuccesTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getForceSuccesType()
		 * @generated
		 */
		EClass FORCE_SUCCES_TYPE = eINSTANCE.getForceSuccesType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__ACTION = eINSTANCE.getForceSuccesType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__CONDITION = eINSTANCE.getForceSuccesType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__CONTROL = eINSTANCE.getForceSuccesType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__DECORATOR = eINSTANCE.getForceSuccesType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__SUBTREEPLUS = eINSTANCE.getForceSuccesType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__SEQUENCE = eINSTANCE.getForceSuccesType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getForceSuccesType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__FALLBACK = eINSTANCE.getForceSuccesType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__REACTIVE_FALLBACK = eINSTANCE.getForceSuccesType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__PARALLEL = eINSTANCE.getForceSuccesType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__INVERTER = eINSTANCE.getForceSuccesType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getForceSuccesType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__REPEAT = eINSTANCE.getForceSuccesType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__TIMEOUT = eINSTANCE.getForceSuccesType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__FORCE_SUCCES = eINSTANCE.getForceSuccesType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__FORCE_FAILURE = eINSTANCE.getForceSuccesType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__ALWAYS_SUCCES = eINSTANCE.getForceSuccesType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORCE_SUCCES_TYPE__ALWAYS_FAILURE = eINSTANCE.getForceSuccesType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORCE_SUCCES_TYPE__NAME = eINSTANCE.getForceSuccesType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.InverterTypeImpl <em>Inverter Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.InverterTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getInverterType()
		 * @generated
		 */
		EClass INVERTER_TYPE = eINSTANCE.getInverterType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__ACTION = eINSTANCE.getInverterType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__CONDITION = eINSTANCE.getInverterType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__CONTROL = eINSTANCE.getInverterType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__DECORATOR = eINSTANCE.getInverterType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__SUBTREEPLUS = eINSTANCE.getInverterType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__SEQUENCE = eINSTANCE.getInverterType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getInverterType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__FALLBACK = eINSTANCE.getInverterType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__REACTIVE_FALLBACK = eINSTANCE.getInverterType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__PARALLEL = eINSTANCE.getInverterType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__INVERTER = eINSTANCE.getInverterType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getInverterType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__REPEAT = eINSTANCE.getInverterType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__TIMEOUT = eINSTANCE.getInverterType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__FORCE_SUCCES = eINSTANCE.getInverterType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__FORCE_FAILURE = eINSTANCE.getInverterType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__ALWAYS_SUCCES = eINSTANCE.getInverterType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INVERTER_TYPE__ALWAYS_FAILURE = eINSTANCE.getInverterType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INVERTER_TYPE__NAME = eINSTANCE.getInverterType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ParallelTypeImpl <em>Parallel Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ParallelTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getParallelType()
		 * @generated
		 */
		EClass PARALLEL_TYPE = eINSTANCE.getParallelType();

		/**
		 * The meta object literal for the '<em><b>Built In Multiple Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARALLEL_TYPE__BUILT_IN_MULTIPLE_TYPES = eINSTANCE.getParallelType_BuiltInMultipleTypes();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__ACTION = eINSTANCE.getParallelType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__CONDITION = eINSTANCE.getParallelType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__CONTROL = eINSTANCE.getParallelType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__DECORATOR = eINSTANCE.getParallelType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__SUBTREEPLUS = eINSTANCE.getParallelType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__SEQUENCE = eINSTANCE.getParallelType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getParallelType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__FALLBACK = eINSTANCE.getParallelType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__REACTIVE_FALLBACK = eINSTANCE.getParallelType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__PARALLEL = eINSTANCE.getParallelType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__INVERTER = eINSTANCE.getParallelType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getParallelType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__REPEAT = eINSTANCE.getParallelType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__TIMEOUT = eINSTANCE.getParallelType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__FORCE_SUCCES = eINSTANCE.getParallelType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__FORCE_FAILURE = eINSTANCE.getParallelType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__ALWAYS_SUCCES = eINSTANCE.getParallelType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL_TYPE__ALWAYS_FAILURE = eINSTANCE.getParallelType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARALLEL_TYPE__NAME = eINSTANCE.getParallelType_Name();

		/**
		 * The meta object literal for the '<em><b>Success Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARALLEL_TYPE__SUCCESS_THRESHOLD = eINSTANCE.getParallelType_SuccessThreshold();

		/**
		 * The meta object literal for the '<em><b>Failure Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARALLEL_TYPE__FAILURE_THRESHOLD = eINSTANCE.getParallelType_FailureThreshold();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RepeatTypeImpl <em>Repeat Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RepeatTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getRepeatType()
		 * @generated
		 */
		EClass REPEAT_TYPE = eINSTANCE.getRepeatType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__ACTION = eINSTANCE.getRepeatType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__CONDITION = eINSTANCE.getRepeatType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__CONTROL = eINSTANCE.getRepeatType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__DECORATOR = eINSTANCE.getRepeatType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__SUBTREEPLUS = eINSTANCE.getRepeatType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__SEQUENCE = eINSTANCE.getRepeatType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getRepeatType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__FALLBACK = eINSTANCE.getRepeatType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__REACTIVE_FALLBACK = eINSTANCE.getRepeatType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__PARALLEL = eINSTANCE.getRepeatType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__INVERTER = eINSTANCE.getRepeatType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getRepeatType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__REPEAT = eINSTANCE.getRepeatType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__TIMEOUT = eINSTANCE.getRepeatType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__FORCE_SUCCES = eINSTANCE.getRepeatType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__FORCE_FAILURE = eINSTANCE.getRepeatType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__ALWAYS_SUCCES = eINSTANCE.getRepeatType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_TYPE__ALWAYS_FAILURE = eINSTANCE.getRepeatType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPEAT_TYPE__NAME = eINSTANCE.getRepeatType_Name();

		/**
		 * The meta object literal for the '<em><b>Num Cycles</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPEAT_TYPE__NUM_CYCLES = eINSTANCE.getRepeatType_NumCycles();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RetryTypeImpl <em>Retry Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RetryTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getRetryType()
		 * @generated
		 */
		EClass RETRY_TYPE = eINSTANCE.getRetryType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__ACTION = eINSTANCE.getRetryType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__CONDITION = eINSTANCE.getRetryType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__CONTROL = eINSTANCE.getRetryType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__DECORATOR = eINSTANCE.getRetryType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__SUBTREEPLUS = eINSTANCE.getRetryType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__SEQUENCE = eINSTANCE.getRetryType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getRetryType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__FALLBACK = eINSTANCE.getRetryType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__REACTIVE_FALLBACK = eINSTANCE.getRetryType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__PARALLEL = eINSTANCE.getRetryType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__INVERTER = eINSTANCE.getRetryType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getRetryType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__REPEAT = eINSTANCE.getRetryType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__TIMEOUT = eINSTANCE.getRetryType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__FORCE_SUCCES = eINSTANCE.getRetryType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__FORCE_FAILURE = eINSTANCE.getRetryType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__ALWAYS_SUCCES = eINSTANCE.getRetryType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETRY_TYPE__ALWAYS_FAILURE = eINSTANCE.getRetryType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RETRY_TYPE__NAME = eINSTANCE.getRetryType_Name();

		/**
		 * The meta object literal for the '<em><b>Num Attempts</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RETRY_TYPE__NUM_ATTEMPTS = eINSTANCE.getRetryType_NumAttempts();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RootTypeImpl <em>Root Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.RootTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getRootType()
		 * @generated
		 */
		EClass ROOT_TYPE = eINSTANCE.getRootType();

		/**
		 * The meta object literal for the '<em><b>Behavior Tree</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOT_TYPE__BEHAVIOR_TREE = eINSTANCE.getRootType_BehaviorTree();

		/**
		 * The meta object literal for the '<em><b>Tree Node Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOT_TYPE__TREE_NODE_MODEL = eINSTANCE.getRootType_TreeNodeModel();

		/**
		 * The meta object literal for the '<em><b>Main Tree To Execute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOT_TYPE__MAIN_TREE_TO_EXECUTE = eINSTANCE.getRootType_MainTreeToExecute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveSequenceTypeImpl <em>Reactive Sequence Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.ReactiveSequenceTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getReactiveSequenceType()
		 * @generated
		 */
		EClass REACTIVE_SEQUENCE_TYPE = eINSTANCE.getReactiveSequenceType();

		/**
		 * The meta object literal for the '<em><b>Built In Multiple Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REACTIVE_SEQUENCE_TYPE__BUILT_IN_MULTIPLE_TYPES = eINSTANCE.getReactiveSequenceType_BuiltInMultipleTypes();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__ACTION = eINSTANCE.getReactiveSequenceType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__CONDITION = eINSTANCE.getReactiveSequenceType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__CONTROL = eINSTANCE.getReactiveSequenceType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__DECORATOR = eINSTANCE.getReactiveSequenceType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__SUBTREEPLUS = eINSTANCE.getReactiveSequenceType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__SEQUENCE = eINSTANCE.getReactiveSequenceType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getReactiveSequenceType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__FALLBACK = eINSTANCE.getReactiveSequenceType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__REACTIVE_FALLBACK = eINSTANCE.getReactiveSequenceType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__PARALLEL = eINSTANCE.getReactiveSequenceType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__INVERTER = eINSTANCE.getReactiveSequenceType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getReactiveSequenceType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__REPEAT = eINSTANCE.getReactiveSequenceType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__TIMEOUT = eINSTANCE.getReactiveSequenceType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__FORCE_SUCCES = eINSTANCE.getReactiveSequenceType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__FORCE_FAILURE = eINSTANCE.getReactiveSequenceType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__ALWAYS_SUCCES = eINSTANCE.getReactiveSequenceType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACTIVE_SEQUENCE_TYPE__ALWAYS_FAILURE = eINSTANCE.getReactiveSequenceType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REACTIVE_SEQUENCE_TYPE__NAME = eINSTANCE.getReactiveSequenceType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SequenceTypeImpl <em>Sequence Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SequenceTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getSequenceType()
		 * @generated
		 */
		EClass SEQUENCE_TYPE = eINSTANCE.getSequenceType();

		/**
		 * The meta object literal for the '<em><b>Built In Multiple Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEQUENCE_TYPE__BUILT_IN_MULTIPLE_TYPES = eINSTANCE.getSequenceType_BuiltInMultipleTypes();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__ACTION = eINSTANCE.getSequenceType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__CONDITION = eINSTANCE.getSequenceType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__CONTROL = eINSTANCE.getSequenceType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__DECORATOR = eINSTANCE.getSequenceType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__SUBTREEPLUS = eINSTANCE.getSequenceType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__SEQUENCE = eINSTANCE.getSequenceType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getSequenceType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__FALLBACK = eINSTANCE.getSequenceType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__REACTIVE_FALLBACK = eINSTANCE.getSequenceType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__PARALLEL = eINSTANCE.getSequenceType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__INVERTER = eINSTANCE.getSequenceType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getSequenceType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__REPEAT = eINSTANCE.getSequenceType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__TIMEOUT = eINSTANCE.getSequenceType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__FORCE_SUCCES = eINSTANCE.getSequenceType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__FORCE_FAILURE = eINSTANCE.getSequenceType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__ALWAYS_SUCCES = eINSTANCE.getSequenceType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_TYPE__ALWAYS_FAILURE = eINSTANCE.getSequenceType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEQUENCE_TYPE__NAME = eINSTANCE.getSequenceType_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SubTreePlusTypeImpl <em>Sub Tree Plus Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.SubTreePlusTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getSubTreePlusType()
		 * @generated
		 */
		EClass SUB_TREE_PLUS_TYPE = eINSTANCE.getSubTreePlusType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__ACTION = eINSTANCE.getSubTreePlusType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__CONDITION = eINSTANCE.getSubTreePlusType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__CONTROL = eINSTANCE.getSubTreePlusType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__DECORATOR = eINSTANCE.getSubTreePlusType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__SUBTREEPLUS = eINSTANCE.getSubTreePlusType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__SEQUENCE = eINSTANCE.getSubTreePlusType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getSubTreePlusType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__FALLBACK = eINSTANCE.getSubTreePlusType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__REACTIVE_FALLBACK = eINSTANCE.getSubTreePlusType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__PARALLEL = eINSTANCE.getSubTreePlusType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__INVERTER = eINSTANCE.getSubTreePlusType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getSubTreePlusType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__REPEAT = eINSTANCE.getSubTreePlusType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__TIMEOUT = eINSTANCE.getSubTreePlusType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__FORCE_SUCCES = eINSTANCE.getSubTreePlusType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__FORCE_FAILURE = eINSTANCE.getSubTreePlusType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__ALWAYS_SUCCES = eINSTANCE.getSubTreePlusType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE_PLUS_TYPE__ALWAYS_FAILURE = eINSTANCE.getSubTreePlusType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_TREE_PLUS_TYPE__NAME = eINSTANCE.getSubTreePlusType_Name();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_TREE_PLUS_TYPE__ID = eINSTANCE.getSubTreePlusType_ID();

		/**
		 * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUB_TREE_PLUS_TYPE__ANY_ATTRIBUTE = eINSTANCE.getSubTreePlusType_AnyAttribute();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TimeoutTypeImpl <em>Timeout Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TimeoutTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getTimeoutType()
		 * @generated
		 */
		EClass TIMEOUT_TYPE = eINSTANCE.getTimeoutType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__ACTION = eINSTANCE.getTimeoutType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__CONDITION = eINSTANCE.getTimeoutType_Condition();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__CONTROL = eINSTANCE.getTimeoutType_Control();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__DECORATOR = eINSTANCE.getTimeoutType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Subtreeplus</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__SUBTREEPLUS = eINSTANCE.getTimeoutType_Subtreeplus();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__SEQUENCE = eINSTANCE.getTimeoutType_Sequence();

		/**
		 * The meta object literal for the '<em><b>Reactive Sequence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__REACTIVE_SEQUENCE = eINSTANCE.getTimeoutType_ReactiveSequence();

		/**
		 * The meta object literal for the '<em><b>Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__FALLBACK = eINSTANCE.getTimeoutType_Fallback();

		/**
		 * The meta object literal for the '<em><b>Reactive Fallback</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__REACTIVE_FALLBACK = eINSTANCE.getTimeoutType_ReactiveFallback();

		/**
		 * The meta object literal for the '<em><b>Parallel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__PARALLEL = eINSTANCE.getTimeoutType_Parallel();

		/**
		 * The meta object literal for the '<em><b>Inverter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__INVERTER = eINSTANCE.getTimeoutType_Inverter();

		/**
		 * The meta object literal for the '<em><b>Retry Until Successful</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getTimeoutType_RetryUntilSuccessful();

		/**
		 * The meta object literal for the '<em><b>Repeat</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__REPEAT = eINSTANCE.getTimeoutType_Repeat();

		/**
		 * The meta object literal for the '<em><b>Timeout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__TIMEOUT = eINSTANCE.getTimeoutType_Timeout();

		/**
		 * The meta object literal for the '<em><b>Force Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__FORCE_SUCCES = eINSTANCE.getTimeoutType_ForceSucces();

		/**
		 * The meta object literal for the '<em><b>Force Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__FORCE_FAILURE = eINSTANCE.getTimeoutType_ForceFailure();

		/**
		 * The meta object literal for the '<em><b>Always Succes</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__ALWAYS_SUCCES = eINSTANCE.getTimeoutType_AlwaysSucces();

		/**
		 * The meta object literal for the '<em><b>Always Failure</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMEOUT_TYPE__ALWAYS_FAILURE = eINSTANCE.getTimeoutType_AlwaysFailure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMEOUT_TYPE__NAME = eINSTANCE.getTimeoutType_Name();

		/**
		 * The meta object literal for the '<em><b>Msec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMEOUT_TYPE__MSEC = eINSTANCE.getTimeoutType_Msec();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TreeNodeModelTypeImpl <em>Tree Node Model Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.TreeNodeModelTypeImpl
		 * @see org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.impl.BehaviortreeSchemaPackageImpl#getTreeNodeModelType()
		 * @generated
		 */
		EClass TREE_NODE_MODEL_TYPE = eINSTANCE.getTreeNodeModelType();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_NODE_MODEL_TYPE__ACTION = eINSTANCE.getTreeNodeModelType_Action();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_NODE_MODEL_TYPE__CONDITION = eINSTANCE.getTreeNodeModelType_Condition();

		/**
		 * The meta object literal for the '<em><b>Decorator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_NODE_MODEL_TYPE__DECORATOR = eINSTANCE.getTreeNodeModelType_Decorator();

		/**
		 * The meta object literal for the '<em><b>Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_NODE_MODEL_TYPE__CONTROL = eINSTANCE.getTreeNodeModelType_Control();

	}

} //BehaviortreeSchemaPackage
