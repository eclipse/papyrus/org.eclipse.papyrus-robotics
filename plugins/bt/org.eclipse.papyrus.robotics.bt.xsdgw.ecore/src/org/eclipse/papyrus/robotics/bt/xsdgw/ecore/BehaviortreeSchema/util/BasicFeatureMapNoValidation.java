/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.xsdgw.ecore.BehaviortreeSchema.util;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.BasicFeatureMap;

public class BasicFeatureMapNoValidation extends BasicFeatureMap {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BasicFeatureMapNoValidation(InternalEObject owner, int featureID) {
		super(owner, featureID);
	}

	@Override
	protected Entry validate(int index, Entry object) {
		return object;
	}
}
