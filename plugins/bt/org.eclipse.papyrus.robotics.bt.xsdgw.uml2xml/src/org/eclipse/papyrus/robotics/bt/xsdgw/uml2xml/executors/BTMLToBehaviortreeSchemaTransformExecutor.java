/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.executors;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.Log;
import org.eclipse.m2m.qvt.oml.util.WriterLog;
import org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.Activator;
import org.eclipse.uml2.uml.Activity;

public class BTMLToBehaviortreeSchemaTransformExecutor {

	public static void run(Activity tr) throws ExecutionException {
		// create a string to represent the base path of model files involved in the transformation
		URI resBasePathName = tr.getModel().eResource().getURI();

		// Refer to an existing transformation via URI
		URI transformationURI = URI.createURI("platform:/plugin/org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml/transforms/BTMLToBehaviortreeSchema.qvto");

		// create executor for the given transformation
		TransformationExecutor executor = new TransformationExecutor(transformationURI);

		// define the transformation inputs
		// semantics
		EList<EObject> semanticObjects = new BasicEList<EObject>();
		semanticObjects.add(tr);
		// notation
		ResourceSet resourceSet = tr.eResource().getResourceSet();
		Resource notationResource = resourceSet.getResource(resBasePathName.trimFileExtension().appendFileExtension("notation"), false);
		EList<EObject> notationObjects = notationResource.getContents();

		// create the input extent with its initial contents
		ModelExtent input1 = new BasicModelExtent(semanticObjects);
		ModelExtent input2 = new BasicModelExtent(notationObjects);
		// create an empty extent to catch the output
		ModelExtent output = new BasicModelExtent();

		// setup the execution environment details ->
		// configuration properties, logger, monitor object etc.
		OutputStreamWriter outStream = new OutputStreamWriter(System.out);
		Log log = new WriterLog(outStream);
		ExecutionContextImpl context = new ExecutionContextImpl();
		context.setLog(log);

		// run the transformation assigned to the executor with the given
		// input and output and execution context
		// Remark: variable arguments count is supported
		ExecutionDiagnostic result = executor.execute(context, input1, input2, output);

		// check the result for success
		if (result.getSeverity() == Diagnostic.OK) {
			// the output objects got captured in the output extent
			List<EObject> outObjects = output.getContents();
			// let's persist them using a resource
			ResourceSet resourceSet2 = new ResourceSetImpl();
			Resource outResource = resourceSet2.createResource(resBasePathName.trimSegments(1).appendSegment(tr.getName()).appendFileExtension("behaviortreeschema"));
			outResource.getContents().addAll(outObjects);
			try {
				outResource.save(Collections.emptyMap());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// turn the result diagnostic into status and send it to error log
			IStatus status = BasicDiagnostic.toIStatus(result);
			Activator.getDefault().getLog().log(status);
			throw new ExecutionException(status.getMessage());
		}
	}

}
