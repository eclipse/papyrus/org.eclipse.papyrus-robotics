package org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.executors;

import java.util.HashSet;
import java.util.LinkedHashSet;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.m2m.internal.qvt.oml.ast.parser.QvtOperationalParserUtil;
import org.eclipse.m2m.internal.qvt.oml.common.MdaException;
import org.eclipse.m2m.internal.qvt.oml.compiler.CompiledUnit;
import org.eclipse.m2m.internal.qvt.oml.compiler.QVTOCompiler;
import org.eclipse.m2m.internal.qvt.oml.compiler.QvtCompilerOptions;
import org.eclipse.m2m.internal.qvt.oml.compiler.UnitProxy;
import org.eclipse.m2m.internal.qvt.oml.expressions.Helper;
import org.eclipse.m2m.internal.qvt.oml.expressions.Module;
import org.eclipse.m2m.internal.qvt.oml.runtime.util.HelperOperationCall;
import org.eclipse.m2m.internal.qvt.oml.runtime.util.NonTransformationExecutionContext;
import org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.utils.ModuleResolver;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.gmf.runtime.notation.Diagram;

public class BTMLHelperCaller {

	private static String fLibraryName = "BTMLLib";
	private Module module;
	private NonTransformationExecutionContext fExecContext;
	private HelperOperationCall fCall;
	private String srcContainer = "transforms";

	public BTMLHelperCaller() {
	}

	public void setUp() throws MdaException {
		ModuleResolver importResolver = ModuleResolver.createTestPluginResolver(srcContainer);

		QVTOCompiler compiler = new QVTOCompiler();
		UnitProxy srcUnit = importResolver.resolveUnit(fLibraryName);

		CompiledUnit result = compiler.compile(srcUnit, new QvtCompilerOptions(), (IProgressMonitor)null);
		if (result.getErrors().size() != 0) throw new MdaException("Library " + fLibraryName + " has compilation errors!");

		module = result.getModules().get(0);

		HashSet<Module> importedModules = new HashSet<Module>();
		importedModules.add(module);
		QvtOperationalParserUtil.collectAllImports(module, importedModules);

		fExecContext = new NonTransformationExecutionContext(importedModules);
	}

	public void setUpOperation(String op_name) throws MdaException {
		Helper operation = findOperationByName(module, op_name);
		if (operation == null) throw new MdaException("Library operation "+ op_name +" not found");
		fCall = fExecContext.createHelperCall(operation);
	}

	public void dispose() {
		if (fExecContext != null)
			fExecContext.dispose();
	}

	public Diagram getBtDiagram(Activity self) throws Exception {
		Object callResult = fCall.invoke(self, new Object[0]);
		if (callResult == null || !(callResult instanceof Diagram)) throw new Exception("getBtDiagram() didn\'t return a Diagram type!");
		return (Diagram) callResult;
	}

	public LinkedHashSet<Action> getBtNodeChildrenOrdered(Action self, Diagram diag) throws Exception {
		Object callResult = fCall.invoke(self, new Object[] {diag});
		if (callResult == null) throw new Exception("getBtNodeChildrenOrdered() didn\'t return an ordered set of Action types!");
		return (LinkedHashSet<Action>) callResult;
	}

	private static Helper findOperationByName(Module module, String operationName) {
		for (EOperation eOperation : module.getEOperations()) {
			if(operationName.equals(eOperation.getName()) && eOperation instanceof Helper) {
				return (Helper) eOperation;
			}
		}

		return null;
	}
}
