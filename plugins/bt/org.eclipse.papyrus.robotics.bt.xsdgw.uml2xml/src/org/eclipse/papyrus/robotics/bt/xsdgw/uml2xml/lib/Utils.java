/*****************************************************************************
 * Copyright (c) 2019, 2023, 2024 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581733
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Issue #7/SubTreePlus nodes should use __autoremap
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.lib;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForResource;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.Parameter;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.ValueSpecificationAction;
import org.eclipse.uml2.uml.util.UMLUtil;

public class Utils {

	protected EClass cls;

	public Utils() {
		super();
	}

	@Operation(contextual=true)
	public FeatureMap.Entry createFeatureMapEntry(InputPin ip) throws ExecutionException {
		setTmpModel(ip.getName());
		return FeatureMapUtil.createEntry(getFlowPort(), generateFlowDirective(ip));
	}

	@Operation(contextual=true)
	public FeatureMap.Entry createFeatureMapEntry(OutputPin op) throws ExecutionException {
		setTmpModel(op.getName());
		return FeatureMapUtil.createEntry(getFlowPort(), generateFlowDirective(op));
	}

	@Operation(contextual=true)
	public FeatureMap.Entry createFeatureMapEntry(Activity act, String entryName) throws ExecutionException {
		setTmpModel(entryName);
		return FeatureMapUtil.createEntry(getFlowPort(), generateBlackBoardDirective(entryName, act.getQualifiedName()));
	}

	@Operation(contextual=true)
	public FeatureMap.Entry createFeatureMapEntry(Activity act, String entryName, String entryValue) throws ExecutionException {
		setTmpModel(entryName);
		return FeatureMapUtil.createEntry(getFlowPort(), generateDirectBlackBoardAssignment(entryName, entryValue, act.getQualifiedName()));
	}

	@Operation(contextual=true)
	public List<Diagram> getDiagrams(Activity act) throws ServiceException {
		ModelSet modelSet = ServiceUtilsForResource.getInstance().getModelSet(act.eResource());
		URI act_notationuri = act.getModel().eResource().getURI().trimFileExtension().appendFileExtension("notation");
		Resource notationResource = getNotationResource(modelSet, act_notationuri);
		return NotationUtils.getDiagrams(notationResource, act);
	}

	// =============================================
	// Utils methods not directly accessible by QVTo
	// =============================================

	//setTmpModel(flowport);
	protected void setTmpModel(String name) {
		// EStructuralFeature
		EAttribute flowport = EcoreFactory.eINSTANCE.createEAttribute();
		flowport.setName(name);
		flowport.setEType(EcorePackage.eINSTANCE.getEString());
		// containing EClass
		cls = EcoreFactory.eINSTANCE.createEClass();
		cls.getEStructuralFeatures().add(flowport);
	}

	protected EStructuralFeature getFlowPort() {
		return cls.getEStructuralFeature(0);
	}

	protected String generateFlowDirective(InputPin ip) throws ExecutionException {
		// InFlowPorts always have 1 incoming edge and 0 outgoing edges
		//    - get _the_ edge
		DataFlowEdge dfe = UMLUtil.getStereotypeApplication(ip.getIncomings().get(0), DataFlowEdge.class);
		if (dfe != null) {
			// we have to distinguish 2 cases:
			//   1. the incoming edge comes from a BT Parameter.
			//      Then the flow directive is the value specification itself.
			//   2. the incoming edge comes from anything else (action, explicit blackboard entry, ...).
			//      Then a blackboard directive must be generated
			OutFlowPort srcFlowport = dfe.getOutPort();
			if (srcFlowport != null) {
				Parameter param = UMLUtil.getStereotypeApplication(srcFlowport.getBase_OutputPin().getOwner(), Parameter.class);
				if (param != null) {
					ValueSpecificationAction vsa = param.getBase_ValueSpecificationAction();
					if (vsa != null) {
						ValueSpecification vs = vsa.getValue();
						if (vs != null && vs instanceof LiteralString) {
							// case 1.
							return vs.stringValue();
						}
					}
					throw new ExecutionException("Parameter \'" + param.getBase_ValueSpecificationAction().getQualifiedName() + "\' has no string value");
				}
			}
			// case 2.
			return generateBlackBoardDirective(dfe);
		}
		return null;
	}

	protected String generateFlowDirective(OutputPin op) throws ExecutionException {
		// OutFlowPorts always have 1 outgoing edge and 0 incoming edges
		//	    - get _the_ edge
		DataFlowEdge dfe = UMLUtil.getStereotypeApplication(op.getOutgoings().get(0), DataFlowEdge.class);
		if (dfe != null) {
			// OutFlowPorts always write to a blackboard entry, so a blackboard directive must be generated
			return generateBlackBoardDirective(dfe);
		}
		return null;
	}

	protected String generateBlackBoardDirective(DataFlowEdge dfe) throws ExecutionException {
		BlackBoardEntry bbe = dfe.getBbEntry();
		// The value of the directive is (i) the edge's name when In and Out flow ports are directly connected;
		// or (ii) the blackboard item's name when they are connected through an explicit blackboard entry
		String bbkey = null;
		String associated_elem_uid = null;
		if (bbe == null) {
			// case (i)
			associated_elem_uid = dfe.getInstance_uid();
			bbkey = dfe.getBase_ObjectFlow().getName();
		}
		else {
			// case (ii). Here the blackboard entry can either be exposed as tree port
			// (applied to a UML::ActivityParameterNode), or not (applied to a UML::CentralBufferNode)
			associated_elem_uid = bbe.getInstance_uid();
			if (bbe.getBase_ActivityParameterNode() != null) {
				// case (ii).a
				bbkey = bbe.getBase_ActivityParameterNode().getName();
			}
			else if (bbe.getBase_CentralBufferNode() != null) {
				// case (ii).b
				bbkey = bbe.getBase_CentralBufferNode().getName();
			}
		}
		return generateBlackBoardDirective(bbkey, associated_elem_uid);
	}

	protected String generateBlackBoardDirective(String bbkey, String associated_elem_uid) throws ExecutionException {
		if (bbkey == null)
			throw new ExecutionException("Inconsistent model! Cannot find the element associated to BlackBoardEntry \'" + associated_elem_uid + "\'");
		return ("{" + bbkey + "}");
	}

	protected String generateDirectBlackBoardAssignment(String bbkey, String bbval, String associated_elem_uid) throws ExecutionException {
		if (bbkey == null)
			throw new ExecutionException("Inconsistent model! Cannot find the element associated to BlackBoardEntry \'" + associated_elem_uid + "\'");
		return (bbval);
	}

	protected Resource getNotationResource(ModelSet ms, URI notationuri) {
		Resource notationResource = null;
		for (Resource r : ms.getResources()) {
			if (r.getURI().equals(notationuri)) {
				notationResource = r;
				break;
			}
		}
		return notationResource;
	}
}
