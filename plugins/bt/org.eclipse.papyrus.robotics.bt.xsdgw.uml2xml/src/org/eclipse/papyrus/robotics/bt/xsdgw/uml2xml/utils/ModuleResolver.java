package org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.utils;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.internal.qvt.oml.compiler.BlackboxUnitResolver;
import org.eclipse.m2m.internal.qvt.oml.compiler.UnitProxy;
import org.eclipse.m2m.internal.qvt.oml.compiler.UnitResolver;
import org.eclipse.m2m.internal.qvt.oml.runtime.project.PlatformPluginUnitResolver;
import org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.Activator;
import org.osgi.framework.Bundle;

public class ModuleResolver implements UnitResolver {

	private UnitResolver fPluginResolver;	
	private IPath fBasePath;

	public static ModuleResolver createTestPluginResolver(String sourceContainerPath) {
		return new ModuleResolver(Activator.PLUGIN_ID, sourceContainerPath);
	}

	public ModuleResolver(final String bundleSymbolicName, final String sourceContainerPath) {
		if(bundleSymbolicName == null || sourceContainerPath == null) {
			throw new IllegalArgumentException();
		}
		
		Bundle bundle =  Platform.getBundle(bundleSymbolicName);
		if(bundle == null) {
			throw new IllegalArgumentException("Bundle not existing: " + bundleSymbolicName); //$NON-NLS-1$
		}
		
		fBasePath = new Path(sourceContainerPath).makeAbsolute();
		
		fPluginResolver = new PlatformPluginUnitResolver(bundle, fBasePath) {
			UnitResolver fBlackboxResolver = new BlackboxUnitResolver(URI.createPlatformPluginURI(bundleSymbolicName, false));

			@Override
			public UnitProxy doResolveUnit(String qualifiedName) {			
				UnitProxy unit = super.doResolveUnit(qualifiedName);
				if(unit == null) {
					unit = fBlackboxResolver.resolveUnit(qualifiedName);
				}
				return unit;
			}
		};
		
	}

	@Override
	public UnitProxy resolveUnit(String qualifiedName) {
		return fPluginResolver.resolveUnit(qualifiedName);
	}

}
