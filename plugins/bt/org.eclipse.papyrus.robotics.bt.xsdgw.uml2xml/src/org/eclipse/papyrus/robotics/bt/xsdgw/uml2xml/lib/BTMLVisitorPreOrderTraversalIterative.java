package org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.lib;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Stack;

import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.executors.BTMLHelperCaller;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.util.UMLUtil;

public class BTMLVisitorPreOrderTraversalIterative {

	protected Stack<Action>    _stack;
	protected Activity         _treeRoot;
	protected Diagram          _treeDiag;
	protected BTMLHelperCaller _helperCaller;

	/**
	 * Adapt getBtDiagram to this usage context.
	 * 
	 * (i) prepare _helperCaller to execute its own getBtDiagram() method; (ii) prepare
	 * _helperCaller to execute the getBtNodeChildrenOrdered() method.
	 * @param aa
	 * @return
	 * @throws Exception
	 */
	protected Diagram getBtDiagram(Activity aa) throws Exception {
		Diagram diag = null;
		_helperCaller.setUpOperation("getBtDiagram");
		diag = _helperCaller.getBtDiagram(aa);
		_helperCaller.setUpOperation("getBtNodeChildrenOrdered");
		return diag;
	}

	protected void stack_push(LinkedHashSet<Action> ordset) {
		LinkedList<Action> ordlist = new LinkedList<>(ordset);
		Iterator<Action> itr = ordlist.descendingIterator();
		while(itr.hasNext())
			_stack.push(itr.next());
	}

	protected void validateTreeRoot(Activity next) throws Exception {
		if (next != _treeRoot) {
			_treeRoot = next;
			_treeDiag = getBtDiagram(next);
		}
	}

	public BTMLVisitorPreOrderTraversalIterative(Activity tr) throws Exception {
		// save the current tree root
		_treeRoot = tr;
		// prepare the stack
		_stack = new Stack<Action>();
		// put root in stack
		TreeRoot tr_ = UMLUtil.getStereotypeApplication(tr, TreeRoot.class);
		_stack.push(tr_.getTreenode().getBase_Action());
		// instantiate the helper caller
		_helperCaller = new BTMLHelperCaller();
		_helperCaller.setUp();
		// set the bt diagram for the current activity
		_treeDiag = getBtDiagram(_treeRoot);
	}

	public void dispose() {
		_helperCaller.dispose();
	}

	public boolean hasNext() {
		return !(_stack.empty());
	}

	public Action visitNext() throws Exception {
		Action next = _stack.pop(); // ready to be returned
		// prepare the stack for successive visits
		validateTreeRoot(next.getActivity());
		LinkedHashSet<Action> next_children = _helperCaller.getBtNodeChildrenOrdered(next,_treeDiag);
		if (next_children.size() > 0)
			stack_push(next_children);
		return next;
	}
}
