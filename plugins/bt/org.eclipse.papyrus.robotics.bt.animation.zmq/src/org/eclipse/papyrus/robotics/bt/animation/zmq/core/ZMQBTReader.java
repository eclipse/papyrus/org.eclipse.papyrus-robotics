/*****************************************************************************
 * Copyright (c) 2020, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581828
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.zmq.core;

import java.nio.ByteBuffer;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;

public class ZMQBTReader {

	protected ZContext _context;
	protected Socket   _treeReader;
	protected String   BT_READER_TCP = "tcp://*:2667";
	protected int      BT_READER_TIMEOUT_SEC = 60;
	byte[]             _treeInfo;

	public ZMQBTReader(ZContext ctx) {
		_context    = ctx;
		_treeInfo   = null;
		_treeReader = _context.createSocket(SocketType.REQ);
		_treeReader.connect(BT_READER_TCP);
		_treeReader.setReceiveTimeOut(1000);
	}

	public ByteBuffer waitForTree() throws Exception {
		String request = "0";
        int attempt_no = 0;
        _treeReader.send(request.getBytes(ZMQ.CHARSET), 0);
        while (attempt_no < BT_READER_TIMEOUT_SEC) {
        	//System.out.println("attempt number " + attempt_no);
            _treeInfo = _treeReader.recv();
            if (_treeInfo != null) break;
            attempt_no += 1;
        }
        if (attempt_no == BT_READER_TIMEOUT_SEC) {
        	close();
        	throw new Exception("[ZMQBTReader] Couldn\'t find an active server in " + BT_READER_TIMEOUT_SEC + " sec.");
        }
		return ByteBuffer.wrap(_treeInfo);
	}
 
	public void close() {
		_treeReader.close();
	}
}
