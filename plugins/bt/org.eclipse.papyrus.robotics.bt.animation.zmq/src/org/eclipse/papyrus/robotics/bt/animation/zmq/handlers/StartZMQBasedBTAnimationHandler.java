/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.zmq.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.papyrus.robotics.bt.animation.core.utils.BTAnimationResourceManager;
import org.eclipse.papyrus.robotics.bt.animation.core.utils.BTAnimationUtils;
import org.eclipse.papyrus.robotics.bt.animation.zmq.core.ZMQBehaviorTreeAnimator;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.ui.handlers.HandlerUtil;

public class StartZMQBasedBTAnimationHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// the handler executes when no monitoring is being done for the current TreeRoot
		TreeRoot tr = BTAnimationUtils.getSelectedTreeRoot(HandlerUtil.getCurrentSelection(event));
		if (tr == null) throw new ExecutionException("The selected object is not a TreeRoot!");
		ZMQBehaviorTreeAnimator treeDiagAnimator = null;
		try {
			treeDiagAnimator = new ZMQBehaviorTreeAnimator(tr.getBase_Activity());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		BTAnimationResourceManager.getInstance().animation_running = true;
		new Thread(treeDiagAnimator).start();
		return null;
	}

}
