/*****************************************************************************
 * Copyright (c) 2020, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581828
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.zmq.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.eclipse.papyrus.robotics.bt.animation.core.rendering.AnimationKind;
import org.eclipse.papyrus.robotics.bt.animation.core.runnables.BehaviorTreeAnimator;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

import Serialization.BehaviorTree;
import Serialization.NodeStatus;

public class ZMQBehaviorTreeAnimator extends BehaviorTreeAnimator {

	protected ZContext    _context;
	protected ZMQBTReader _treeReader;
	protected Socket      _logSubscriber;
	protected Poller      _poller;
	protected String      BT_LOG_SUBSCRIBER_TCP = "tcp://*:2666";
	protected ByteBuffer  bb;
	protected static final AnimationKind[] aks = { AnimationKind.VISITED, AnimationKind.RUNNING, AnimationKind.SUCCESS, AnimationKind.FAILURE };
	protected boolean _node_status_update_required = true;

	protected void wrapByteArray(byte[] data, int offset, int length)
	{
		bb = ByteBuffer.wrap(data,offset,length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
	}

	protected long getUnsignedInt(byte[] data, int offset)
	{
		wrapByteArray(data, offset, 4);
	    return bb.getInt() & 0xffffffffl;
	}

	protected long getUnsignedShort(byte[] data, int offset)
	{
		wrapByteArray(data, offset, 2);
	    return bb.getShort() & 0xffffffffl;
	}

	protected long getUnsignedChar(byte[] data, int offset)
	{
		wrapByteArray(data, offset, 1);
	    return bb.get() & 0xffffffffl;
	}

	protected AnimationKind toAnimationKind(int s)
	{
		return aks[s];
	}

	protected void animate() {
		byte[] buffer = _logSubscriber.recv(); // get the incoming message

    	// header
    	int header_size            = (int) getUnsignedInt(buffer, 0);
    	// transitions
    	int num_transitions        = (int) getUnsignedInt(buffer, 4+header_size);
    	System.out.print("\n\n");
    	if (_node_status_update_required) {
    		// reading the header...
    		for(int offset = 4; offset < header_size + 4; offset += 3) {
    			short uid    = (short) getUnsignedShort(buffer, offset);
    			char status  = (char)  getUnsignedChar(buffer, offset+2);
    			//
    			// access the map for nodes
    			ActivityNode treeNode = _uid_to_node.get((int)uid);
    			AnimationKind kind    = toAnimationKind(status);
    			if (treeNode != null) {
    				if (!_renderer.hasMarker(treeNode, kind)) {
    					ActivityEdge controlFlow = _uid_to_incomingedge.get((int)uid);
    		        	if (controlFlow != null)
    		        		_renderer.renderAs( controlFlow, kind );
    					_renderer.renderAs( treeNode, kind );
    				}
    			}
    			else
    				System.out.println("Found nullptr for uid " + uid + ".");
    		}
    		_node_status_update_required = false;
    	}
        // reading the transitions...
        //System.out.println("\nNum of transitions is " +  num_transitions);
        for (int t = 0; t < num_transitions; t++) {
        	int offset = 8 + header_size + 12*t;
        	short uid     = (short) getUnsignedShort(buffer, offset+8);
        	System.out.print("uid: " +  uid);
        	char prev_st = (char) getUnsignedChar(buffer, offset+10);
        	System.out.print(", " + NodeStatus.name(prev_st));
        	char curr_st = (char) getUnsignedChar(buffer, offset+11);
        	System.out.println(" -> " + NodeStatus.name(curr_st));
        	//
        	// access the map for transitions
        	ActivityEdge controlFlow = _uid_to_incomingedge.get((int)uid);
        	if (controlFlow != null) {
        		_renderer.renderAs(
        				controlFlow,
        				toAnimationKind(curr_st)
        			);
        	}
        	ActivityNode treeNode = _uid_to_node.get((int)uid);
        	if (treeNode != null) {
        	_renderer.renderAs(
        			treeNode,
        			toAnimationKind(curr_st)
        		);
        	}
        	else {
        		// Found nullptr for uid. This may occur for example when
        		// the BT is executed in a loop that requires re-instantiation
        		// of BT nodes btw each iteration (e.g.,
        		// when a ROS 2 managed sequencer node is (re-)activated multiple times)
        		System.out.println("Found nullptr for uid " + uid + ". Recovering.");
        		_node_status_update_required = true;
        	}
        }
	}

	public ZMQBehaviorTreeAnimator(Activity btr) throws Exception {
		super(btr);
		_context       = new ZContext(1);
		_treeReader    = new ZMQBTReader(_context);
		_logSubscriber = _context.createSocket(SocketType.SUB);
		_logSubscriber.subscribe("".getBytes());
		_logSubscriber.setConflate(true);
		_logSubscriber.connect(BT_LOG_SUBSCRIBER_TCP);
		_poller        = _context.createPoller(1);
		_poller.register(_logSubscriber, Poller.POLLIN);
	}

	@Override
	public void run() {
		// start animation with meaningful maps
		while (_rm.animation_running) {
			// ask the tree to the server
			ByteBuffer bb = null;
			try {
				bb = _treeReader.waitForTree();
				if (bb != null) {
					BehaviorTree bt = BehaviorTree.getRootAsBehaviorTree(bb);
					// get the uids in order
					int[] uids = new int[bt.nodesLength()];
					for (int i = 0; i < bt.nodesLength(); i++)
						uids[i] = bt.nodes(i).uid();
					// build the maps from ordered uids
					buildMaps(uids);
					// (log)
					_uid_to_node.entrySet().forEach(entry->{
						System.out.println(entry.getKey() + ", " + entry.getValue().getName());
					});
					// https://stackoverflow.com/questions/49350765/how-to-interrupt-a-call-to-read-method-on-a-jeromq-socket
					while(true) {
						boolean ec = _rm.animation_running;
						int rc = _poller.poll(5);
						if ( ec & rc == 0 ){
							// DO NOTHING, AS THERE IS NOTHING TO WORK ON
						}
						else {
							if ( !ec ){
								break; // break the while(true) cycle and stop animation!
							}
							if ( rc > 0 ){
								// ZeroMQ has a thing to indeed .recv(), so read it:
								animate();
								if (_node_status_update_required) break; // break the while(true) but does not stop the animation
							}
							else { // rc < 0
								// ZeroMQ .poll() Error Analysis & Error Recovery
							}
						}
					}
					// dispose all the old information and recompute
					dispose();
				}
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}
		stop();
	}

	@Override
	public void stop() {
		super.stop();
		_logSubscriber.close();
		_poller.close();
		_treeReader.close();
		_context.close();
		System.out.println("[ZMQBehaviorTreeAnimator] done.");
	}

}
