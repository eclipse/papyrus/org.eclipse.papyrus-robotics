// automatically generated by the FlatBuffers compiler, do not modify

package Serialization;

public final class NodeStatus {
  private NodeStatus() { }
  public static final byte IDLE = 0;
  public static final byte RUNNING = 1;
  public static final byte SUCCESS = 2;
  public static final byte FAILURE = 3;

  public static final String[] names = { "IDLE", "RUNNING", "SUCCESS", "FAILURE", };

  public static String name(int e) { return names[e]; }
}

