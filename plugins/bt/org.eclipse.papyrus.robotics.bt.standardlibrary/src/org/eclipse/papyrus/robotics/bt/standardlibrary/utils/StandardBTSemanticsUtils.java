/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr>
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.standardlibrary.utils;

import org.eclipse.emf.common.util.URI;

public class StandardBTSemanticsUtils {

	/**
	 * URI of the BT stdlibrary
	 */
	private final static String BT_STANDARD_SEMANTICS_URI = "pathmap://Robotics_LIBRARIES_BT/btsemantics.uml";

	/**
	 * Get the URI of BT stdlibrary
	 */
	public static URI getStandardBTSemanticsURI() {
		return URI.createURI(BT_STANDARD_SEMANTICS_URI);
	}
}
