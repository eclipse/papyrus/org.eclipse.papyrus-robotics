/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.core.propertytesters;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.bt.animation.core.utils.BTAnimationResourceManager;

/**
 * Check, if animation is currently running
 */
public class BTAnimationTester extends PropertyTester {

	public static final String IS_BT_ANIMATION_OFF = "isBTAnimationOff"; //$NON-NLS-1$
	public static final String IS_BT_ANIMATION_ON = "isBTAnimationOn"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (IS_BT_ANIMATION_OFF.equals(property)) {
			return isBTAnimationOff((IStructuredSelection) receiver);
		} else if (IS_BT_ANIMATION_ON.equals(property)) {
			return isBTAnimationOn((IStructuredSelection) receiver);
		}
		return false;
	}

	protected boolean isBTAnimationOff(IStructuredSelection selection) {
		return !BTAnimationResourceManager.getInstance().animation_running;
	}

	protected boolean isBTAnimationOn(IStructuredSelection selection) {
		return BTAnimationResourceManager.getInstance().animation_running;
	}
}
