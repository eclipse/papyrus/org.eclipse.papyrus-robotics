/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.core.utils;

public class BTAnimationResourceManager {

	/**
	 * Instance holder 
	 */
	private static class InstanceHolder {
		private static BTAnimationResourceManager INSTANCE = new BTAnimationResourceManager();
	}

	/**
	 * Attributes
	 */
	public volatile boolean animation_running;
	

	/**
	 * Private constructor and instance getter
	 */
	private BTAnimationResourceManager() {
		animation_running    = false;
	}
	public static BTAnimationResourceManager getInstance(){
		return InstanceHolder.INSTANCE;
	}
}
