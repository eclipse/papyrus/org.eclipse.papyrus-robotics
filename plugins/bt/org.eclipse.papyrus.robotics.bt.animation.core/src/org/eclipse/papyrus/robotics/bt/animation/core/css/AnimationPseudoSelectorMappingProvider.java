/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from CEA LIST)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.core.css;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.papyrus.infra.gmfdiag.css.service.IMarkerToPseudoSelectorMappingProvider;
import org.eclipse.papyrus.robotics.bt.animation.core.utils.BTAnimationUtils;

public class AnimationPseudoSelectorMappingProvider implements IMarkerToPseudoSelectorMappingProvider {

	protected Map<String, String> mappings;

	public final static String RUNNING_MARKER_PSEUDO_SELECTOR = "running"; //$NON-NLS-1$

	public final static String SUCCESS_MARKER_PSEUDO_SELECTOR = "success"; //$NON-NLS-1$

	public final static String FAILURE_MARKER_PSEUDO_SELECTOR = "failure"; //$NON-NLS-1$

	public final static String VISITED_MARKER_PSEUDO_SELECTOR = "visited"; //$NON-NLS-1$

	public AnimationPseudoSelectorMappingProvider() {
	}

	public Map<String, String> getMappings() {
		if (this.mappings == null) {
			this.mappings = new HashMap<String, String>();
			// Adds pseudo selectors
			this.mappings.put(BTAnimationUtils.RUNNING_MARKER_ID, RUNNING_MARKER_PSEUDO_SELECTOR);
			this.mappings.put(BTAnimationUtils.SUCCESS_MARKER_ID, SUCCESS_MARKER_PSEUDO_SELECTOR);
			this.mappings.put(BTAnimationUtils.FAILURE_MARKER_ID, FAILURE_MARKER_PSEUDO_SELECTOR);
			this.mappings.put(BTAnimationUtils.VISITED_MARKER_ID, VISITED_MARKER_PSEUDO_SELECTOR);
		}
		return this.mappings;
	}
}
