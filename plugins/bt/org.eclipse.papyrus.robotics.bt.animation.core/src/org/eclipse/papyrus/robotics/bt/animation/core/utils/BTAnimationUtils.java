/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.core.utils;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

public class BTAnimationUtils {

	public static TreeRoot getSelectedTreeRoot(ISelection selection) {
		TreeRoot tr = null;
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			for(Object current : structuredSelection.toArray())
				if(current instanceof IAdaptable) {
					EObject eObj = (EObject) ((IAdaptable) current).getAdapter(EObject.class);
					tr = (TreeRoot) UMLUtil.getStereotypeApplication((Element) eObj, TreeRoot.class);
					break;
				}
		}
		return tr;
	}

	// Declares the ID of the running marker
	// NOTE: this ID is the same that the one used to declare the marker in the extensions
	public static String RUNNING_MARKER_ID = "org.eclipse.papyrus.robotics.bt.animation.core.runningmarker";

	// Declares the ID of the success marker
	// NOTE: this ID is the same that the one used to declare the marker in the extensions
	public static String SUCCESS_MARKER_ID = "org.eclipse.papyrus.robotics.bt.animation.core.successmarker";

	// Declares the ID of the failure marker
	// NOTE: this ID is the same that the one used to declare the marker in the extensions
	public static String FAILURE_MARKER_ID = "org.eclipse.papyrus.robotics.bt.animation.core.failuremarker";

	// Declares the ID of the visited marker
	// NOTE: this ID is the same that the one used to declare the marker in the extensions
	public static String VISITED_MARKER_ID = "org.eclipse.papyrus.robotics.bt.animation.core.visitedmarker";

}
