/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.core.runnables;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.papyrus.robotics.bt.animation.core.rendering.AnimationRenderer;
import org.eclipse.papyrus.robotics.bt.animation.core.utils.BTAnimationResourceManager;
import org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.lib.BTMLVisitorPreOrderTraversalIterative;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;

public abstract class BehaviorTreeAnimator implements Runnable {

	// renderer
	protected AnimationRenderer _renderer;
	// BT root
	protected Activity _tr;
	// BT visitor
	protected BTMLVisitorPreOrderTraversalIterative _visitor;
	// maps
	protected Map<Integer, ActivityNode> _uid_to_node;
	protected Map<Integer, ActivityEdge> _uid_to_incomingedge;
	// animation status manager
	protected BTAnimationResourceManager _rm;

	protected boolean emptyMaps() {
		return ( (_uid_to_node == null || _uid_to_node.isEmpty()) &&
					(_uid_to_incomingedge == null || _uid_to_incomingedge.isEmpty()) );
	}

	protected void updateMaps(int key, Action node) {
		if (node != null) {
			_uid_to_node.put(key, node);
			List<ActivityEdge> ctrlFlowEdges = node.getIncomings();
			if (!(ctrlFlowEdges.isEmpty()))
				_uid_to_incomingedge.put(key, node.getIncomings().get(0));
		}
	}

	protected void instantiateMaps() {
		_uid_to_node         = new HashMap<Integer, ActivityNode>();
		_uid_to_incomingedge = new HashMap<Integer, ActivityEdge>();
	}

	public BehaviorTreeAnimator(Activity btr) throws Exception {
		// instantiate renderer
		_renderer = new AnimationRenderer();
		// store BT root
		_tr = btr;
		// instantiate BT visitor
		_visitor = new BTMLVisitorPreOrderTraversalIterative(_tr);
		// instantiate maps
		instantiateMaps();
		// store animation status manager
		_rm = BTAnimationResourceManager.getInstance();
	}

	public void buildMaps(int[] uids) {
		int i = 0; // step counter
		while (_visitor.hasNext()) {
			try {
				if (i == uids.length) break;
				updateMaps(uids[i++], _visitor.visitNext());
			} catch (Exception e) {
				e.printStackTrace();
				stop();
			}
		}
		if (i < uids.length || (i == uids.length && _visitor.hasNext())) {
			Exception e = new Exception("The number of nodes in the diagram and from the run-time BT description does not match!");
			e.printStackTrace();
			stop();
		}
	}

	public void dispose() throws Exception {
		_visitor = new BTMLVisitorPreOrderTraversalIterative(_tr);
	}

	public void stop() {
		_renderer.deleteAllMarkers();
		_visitor.dispose();
	}

}
