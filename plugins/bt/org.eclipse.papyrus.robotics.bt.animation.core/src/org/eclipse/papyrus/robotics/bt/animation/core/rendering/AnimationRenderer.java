/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from CEA LIST)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.animation.core.rendering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.papyrus.infra.services.markerlistener.IPapyrusMarker;
import org.eclipse.papyrus.infra.services.markerlistener.PapyrusMarkerAdapter;
import org.eclipse.papyrus.robotics.bt.animation.core.utils.BTAnimationUtils;

public class AnimationRenderer {

	// Maintain a map of model elements having makers
	// The only purpose of this map is too avoid a global search in diagrams at some points of the execution
	protected Map<EObject, List<IPapyrusMarker>> modelElementMarkers = new HashMap<EObject, List<IPapyrusMarker>>();

    public AnimationRenderer() {
		// Constructor
		this.modelElementMarkers = new HashMap<EObject, List<IPapyrusMarker>>();
	}

	public boolean hasMarker(EObject modelElement, AnimationKind kind) {
		// Find out if a marker of the given kind is applied on the model element
		if (modelElement == null) {
			return false;
		}
		List<IPapyrusMarker> markers = this.modelElementMarkers.get(modelElement);
		if (markers == null || markers.isEmpty()) {
			return false;
		}
		boolean found = false;
		int i = 0;
		while (!found && i < markers.size()) {
			IPapyrusMarker marker = markers.get(i);
			String type = "";
			try {
				type = marker.getType();
			} catch (CoreException e) {
				e.printStackTrace();
			}
			if (kind == AnimationKind.RUNNING) {
				found = type.equals(BTAnimationUtils.RUNNING_MARKER_ID);
			} else if (kind == AnimationKind.SUCCESS) {
				found = type.equals(BTAnimationUtils.SUCCESS_MARKER_ID);
			} else if (kind == AnimationKind.FAILURE) {
				found = type.equals(BTAnimationUtils.FAILURE_MARKER_ID);
			} else if (kind == AnimationKind.VISITED) {
				found = type.equals(BTAnimationUtils.VISITED_MARKER_ID);
			}
			i++;
		}
		return found;
	}

	@SuppressWarnings("unchecked")
	private IPapyrusMarker createMarker(final EObject modelElement, final String markerID, @SuppressWarnings("rawtypes") Map attributes) {
		// Create a marker of the given type (c.f. markerID) and set the attributes
		// with the provided map
		IPapyrusMarker marker = null;
		// Assert that the model element is usable
		if (modelElement == null || modelElement.eResource() == null) {
			return null;
		}
		final String uri = EcoreUtil.getURI(modelElement).toString();
		if (uri == null || uri.isEmpty()) {
			return null;
		}
		// Assert we are able to access the resource on which the marker will be placed
		IResource resource = this.getWorkspaceResource(modelElement);
		if (resource != null && resource.exists()) {
			IMarker newMarker = null;
			try {
				newMarker = resource.createMarker(markerID);
				newMarker.setAttributes(attributes);
			} catch (CoreException e) {
				e.printStackTrace();
			}
			if (newMarker != null) {
				// Place the Papyrus marker
				marker = PapyrusMarkerAdapter.wrap(modelElement.eResource(), newMarker, attributes);
			}
		}
		return marker;
	}

	private void deleteMarkers(final EObject modelElement) {
		// Find out a marker of the given type placed on a model element. This marker is
		// then deleted. The deleted marker is returned by the operation.
		List<IPapyrusMarker> markers = this.modelElementMarkers.get(modelElement);
		if (markers != null && !markers.isEmpty()) {
			Iterator<IPapyrusMarker> markersIterator = markers.iterator();
			while (markersIterator.hasNext()) {
				IPapyrusMarker currentMarker = markersIterator.next();
				try {
					currentMarker.delete();
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
			this.modelElementMarkers.get(modelElement).clear();
		}
	}

	private IResource getWorkspaceResource(final EObject modelElement) {
		IResource resource = null;
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		if (workspace != null) {
			resource = workspace.getRoot().getFile(new Path(modelElement.eResource().getURI().toPlatformString(true)));
		}
		return resource;
	}

	public void deleteAllMarkers() {
		// Make sure any marker placed by the framework over a model element is deleted when this operation is called
		// The operation is thread safe and lock a model element before starting to remove the markers
		for (EObject modelElement : this.modelElementMarkers.keySet()) {
			for (IPapyrusMarker marker : this.modelElementMarkers.get(modelElement)) {
				if (marker.exists()) {
					try {
						marker.delete();
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}
			}
			this.modelElementMarkers.get(modelElement).clear();
		}
		this.modelElementMarkers.clear();
	}

	public void renderAs(EObject modelElement, AnimationKind targetStyle) {
		IPapyrusMarker requestedMarker = null;
		deleteMarkers(modelElement);
//		if (!this.hasMarker(modelElement, targetStyle)) {
			Map<String, Object> attributes = new HashMap<String, Object>();
			attributes.put(EValidator.URI_ATTRIBUTE, EcoreUtil.getURI(modelElement).toString());
			//attributes.put("CONSTRAINTS", this.diagramManager.getAnimatedDiagrams(animator));
			if (targetStyle.equals(AnimationKind.RUNNING)) {
				requestedMarker = this.createMarker(modelElement, BTAnimationUtils.RUNNING_MARKER_ID, attributes);
			} else if (targetStyle.equals(AnimationKind.SUCCESS)) {
				requestedMarker = this.createMarker(modelElement, BTAnimationUtils.SUCCESS_MARKER_ID, attributes);
			} else if (targetStyle.equals(AnimationKind.FAILURE)) {
				requestedMarker = this.createMarker(modelElement, BTAnimationUtils.FAILURE_MARKER_ID, attributes);
			} else if (targetStyle.equals(AnimationKind.VISITED)) {
				requestedMarker = this.createMarker(modelElement, BTAnimationUtils.VISITED_MARKER_ID, attributes);
			} else {
				System.err.println("[renderAs] - animation kind not recognized");
			}
			// Update map of applied markers
			List<IPapyrusMarker> markerList = this.modelElementMarkers.get(modelElement);
			if (markerList == null) {
				markerList = new ArrayList<IPapyrusMarker>();
				this.modelElementMarkers.put(modelElement, markerList);
			}
			if (requestedMarker != null) {
				markerList.add(requestedMarker);
			}
//		}
	}

}
