/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;

import org.eclipse.uml2.uml.Activity;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tree Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getBase_Activity <em>Base Activity</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getTreenode <em>Treenode</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getTreeRoot()
 * @model
 * @generated
 */
public interface TreeRoot extends Entity {
	/**
	 * Returns the value of the '<em><b>Base Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Activity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Activity</em>' reference.
	 * @see #setBase_Activity(Activity)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getTreeRoot_Base_Activity()
	 * @model ordered="false"
	 * @generated
	 */
	Activity getBase_Activity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getBase_Activity <em>Base Activity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Activity</em>' reference.
	 * @see #getBase_Activity()
	 * @generated
	 */
	void setBase_Activity(Activity value);

	/**
	 * Returns the value of the '<em><b>Treenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Treenode</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Treenode</em>' reference.
	 * @see #setTreenode(TreeNode)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getTreeRoot_Treenode()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	TreeNode getTreenode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getTreenode <em>Treenode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Treenode</em>' reference.
	 * @see #getTreenode()
	 * @generated
	 */
	void setTreenode(TreeNode value);

} // TreeRoot
