/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.InputPin;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Flow Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_InputPin <em>Base Input Pin</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getInFlowPort()
 * @model
 * @generated
 */
public interface InFlowPort extends DataFlowPort {
	/**
	 * Returns the value of the '<em><b>Base Input Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Input Pin</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Input Pin</em>' reference.
	 * @see #setBase_InputPin(InputPin)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getInFlowPort_Base_InputPin()
	 * @model ordered="false"
	 * @generated
	 */
	InputPin getBase_InputPin();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_InputPin <em>Base Input Pin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Input Pin</em>' reference.
	 * @see #getBase_InputPin()
	 * @generated
	 */
	void setBase_InputPin(InputPin value);

	/**
	 * Returns the value of the '<em><b>Base Activity Parameter Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Activity Parameter Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Activity Parameter Node</em>' reference.
	 * @see #setBase_ActivityParameterNode(ActivityParameterNode)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getInFlowPort_Base_ActivityParameterNode()
	 * @model ordered="false"
	 * @generated
	 */
	ActivityParameterNode getBase_ActivityParameterNode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Activity Parameter Node</em>' reference.
	 * @see #getBase_ActivityParameterNode()
	 * @generated
	 */
	void setBase_ActivityParameterNode(ActivityParameterNode value);

} // InFlowPort
