/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.BlockImpl;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.Parameter;

import org.eclipse.uml2.uml.ValueSpecificationAction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParameterImpl#getBase_ValueSpecificationAction <em>Base Value Specification Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParameterImpl extends BlockImpl implements Parameter {
	/**
	 * The cached value of the '{@link #getBase_ValueSpecificationAction() <em>Base Value Specification Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_ValueSpecificationAction()
	 * @generated
	 * @ordered
	 */
	protected ValueSpecificationAction base_ValueSpecificationAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueSpecificationAction getBase_ValueSpecificationAction() {
		if (base_ValueSpecificationAction != null && base_ValueSpecificationAction.eIsProxy()) {
			InternalEObject oldBase_ValueSpecificationAction = (InternalEObject)base_ValueSpecificationAction;
			base_ValueSpecificationAction = (ValueSpecificationAction)eResolveProxy(oldBase_ValueSpecificationAction);
			if (base_ValueSpecificationAction != oldBase_ValueSpecificationAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BtPackage.PARAMETER__BASE_VALUE_SPECIFICATION_ACTION, oldBase_ValueSpecificationAction, base_ValueSpecificationAction));
			}
		}
		return base_ValueSpecificationAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueSpecificationAction basicGetBase_ValueSpecificationAction() {
		return base_ValueSpecificationAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_ValueSpecificationAction(ValueSpecificationAction newBase_ValueSpecificationAction) {
		ValueSpecificationAction oldBase_ValueSpecificationAction = base_ValueSpecificationAction;
		base_ValueSpecificationAction = newBase_ValueSpecificationAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BtPackage.PARAMETER__BASE_VALUE_SPECIFICATION_ACTION, oldBase_ValueSpecificationAction, base_ValueSpecificationAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.PARAMETER__BASE_VALUE_SPECIFICATION_ACTION:
				if (resolve) return getBase_ValueSpecificationAction();
				return basicGetBase_ValueSpecificationAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.PARAMETER__BASE_VALUE_SPECIFICATION_ACTION:
				setBase_ValueSpecificationAction((ValueSpecificationAction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.PARAMETER__BASE_VALUE_SPECIFICATION_ACTION:
				setBase_ValueSpecificationAction((ValueSpecificationAction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.PARAMETER__BASE_VALUE_SPECIFICATION_ACTION:
				return base_ValueSpecificationAction != null;
		}
		return super.eIsSet(featureID);
	}

} //ParameterImpl
