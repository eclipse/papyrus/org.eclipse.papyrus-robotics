/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.uml2.uml.ValueSpecificationAction;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.Parameter#getBase_ValueSpecificationAction <em>Base Value Specification Action</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends Block {
	/**
	 * Returns the value of the '<em><b>Base Value Specification Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Value Specification Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Value Specification Action</em>' reference.
	 * @see #setBase_ValueSpecificationAction(ValueSpecificationAction)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getParameter_Base_ValueSpecificationAction()
	 * @model ordered="false"
	 * @generated
	 */
	ValueSpecificationAction getBase_ValueSpecificationAction();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Parameter#getBase_ValueSpecificationAction <em>Base Value Specification Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Value Specification Action</em>' reference.
	 * @see #getBase_ValueSpecificationAction()
	 * @generated
	 */
	void setBase_ValueSpecificationAction(ValueSpecificationAction value);

} // Parameter
