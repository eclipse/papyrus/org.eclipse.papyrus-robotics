/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.robotics.bt.profile.bt.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BtFactoryImpl extends EFactoryImpl implements BtFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BtFactory init() {
		try {
			BtFactory theBtFactory = (BtFactory)EPackage.Registry.INSTANCE.getEFactory(BtPackage.eNS_URI);
			if (theBtFactory != null) {
				return theBtFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BtFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BtFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BtPackage.TREE_ROOT: return createTreeRoot();
			case BtPackage.SUB_TREE: return createSubTree();
			case BtPackage.IN_FLOW_PORT: return createInFlowPort();
			case BtPackage.OUT_FLOW_PORT: return createOutFlowPort();
			case BtPackage.CONDITION: return createCondition();
			case BtPackage.ACTION: return createAction();
			case BtPackage.FALLBACK: return createFallback();
			case BtPackage.SEQUENCE: return createSequence();
			case BtPackage.PARAMETER: return createParameter();
			case BtPackage.DECORATOR_NODE: return createDecoratorNode();
			case BtPackage.CONTROL_FLOW_EDGE: return createControlFlowEdge();
			case BtPackage.DATA_FLOW_EDGE: return createDataFlowEdge();
			case BtPackage.BLACK_BOARD_ENTRY: return createBlackBoardEntry();
			case BtPackage.REACTIVE_FALLBACK: return createReactiveFallback();
			case BtPackage.REACTIVE_SEQUENCE: return createReactiveSequence();
			case BtPackage.INVERTER: return createInverter();
			case BtPackage.RETRY_UNTIL_SUCCESSFUL: return createRetryUntilSuccessful();
			case BtPackage.REPEAT: return createRepeat();
			case BtPackage.PARALLEL_SEQUENCE: return createParallelSequence();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TreeRoot createTreeRoot() {
		TreeRootImpl treeRoot = new TreeRootImpl();
		return treeRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SubTree createSubTree() {
		SubTreeImpl subTree = new SubTreeImpl();
		return subTree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InFlowPort createInFlowPort() {
		InFlowPortImpl inFlowPort = new InFlowPortImpl();
		return inFlowPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OutFlowPort createOutFlowPort() {
		OutFlowPortImpl outFlowPort = new OutFlowPortImpl();
		return outFlowPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Fallback createFallback() {
		FallbackImpl fallback = new FallbackImpl();
		return fallback;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Sequence createSequence() {
		SequenceImpl sequence = new SequenceImpl();
		return sequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DecoratorNode createDecoratorNode() {
		DecoratorNodeImpl decoratorNode = new DecoratorNodeImpl();
		return decoratorNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ControlFlowEdge createControlFlowEdge() {
		ControlFlowEdgeImpl controlFlowEdge = new ControlFlowEdgeImpl();
		return controlFlowEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataFlowEdge createDataFlowEdge() {
		DataFlowEdgeImpl dataFlowEdge = new DataFlowEdgeImpl();
		return dataFlowEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BlackBoardEntry createBlackBoardEntry() {
		BlackBoardEntryImpl blackBoardEntry = new BlackBoardEntryImpl();
		return blackBoardEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReactiveFallback createReactiveFallback() {
		ReactiveFallbackImpl reactiveFallback = new ReactiveFallbackImpl();
		return reactiveFallback;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReactiveSequence createReactiveSequence() {
		ReactiveSequenceImpl reactiveSequence = new ReactiveSequenceImpl();
		return reactiveSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Inverter createInverter() {
		InverterImpl inverter = new InverterImpl();
		return inverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RetryUntilSuccessful createRetryUntilSuccessful() {
		RetryUntilSuccessfulImpl retryUntilSuccessful = new RetryUntilSuccessfulImpl();
		return retryUntilSuccessful;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Repeat createRepeat() {
		RepeatImpl repeat = new RepeatImpl();
		return repeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ParallelSequence createParallelSequence() {
		ParallelSequenceImpl parallelSequence = new ParallelSequenceImpl();
		return parallelSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BtPackage getBtPackage() {
		return (BtPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BtPackage getPackage() {
		return BtPackage.eINSTANCE;
	}

} //BtFactoryImpl
