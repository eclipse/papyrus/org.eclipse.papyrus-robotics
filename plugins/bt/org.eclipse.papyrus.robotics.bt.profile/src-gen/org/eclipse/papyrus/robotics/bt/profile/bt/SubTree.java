/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Tree</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getTreeroot <em>Treeroot</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getFlowport <em>Flowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getInflowport <em>Inflowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getOutflowport <em>Outflowport</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getSubTree()
 * @model
 * @generated
 */
public interface SubTree extends TreeNode {
	/**
	 * Returns the value of the '<em><b>Treeroot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Treeroot</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Treeroot</em>' reference.
	 * @see #setTreeroot(TreeRoot)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getSubTree_Treeroot()
	 * @model required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	TreeRoot getTreeroot();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getTreeroot <em>Treeroot</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Treeroot</em>' reference.
	 * @see #getTreeroot()
	 * @generated
	 */
	void setTreeroot(TreeRoot value);

	/**
	 * Returns the value of the '<em><b>Flowport</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flowport</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flowport</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getSubTree_Flowport()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<DataFlowPort> getFlowport();

	/**
	 * Returns the value of the '<em><b>Inflowport</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inflowport</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inflowport</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getSubTree_Inflowport()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<InFlowPort> getInflowport();

	/**
	 * Returns the value of the '<em><b>Outflowport</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outflowport</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outflowport</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getSubTree_Outflowport()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<OutFlowPort> getOutflowport();

} // SubTree
