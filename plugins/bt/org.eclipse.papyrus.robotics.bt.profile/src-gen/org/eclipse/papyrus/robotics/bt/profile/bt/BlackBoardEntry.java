/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector;

import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.CentralBufferNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Black Board Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getDfEdge <em>Df Edge</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_CentralBufferNode <em>Base Central Buffer Node</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getBlackBoardEntry()
 * @model
 * @generated
 */
public interface BlackBoardEntry extends Connector {
	/**
	 * Returns the value of the '<em><b>Df Edge</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Df Edge</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Df Edge</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getBlackBoardEntry_DfEdge()
	 * @model lower="2" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<DataFlowEdge> getDfEdge();

	/**
	 * Returns the value of the '<em><b>Base Central Buffer Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Central Buffer Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Central Buffer Node</em>' reference.
	 * @see #setBase_CentralBufferNode(CentralBufferNode)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getBlackBoardEntry_Base_CentralBufferNode()
	 * @model ordered="false"
	 * @generated
	 */
	CentralBufferNode getBase_CentralBufferNode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_CentralBufferNode <em>Base Central Buffer Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Central Buffer Node</em>' reference.
	 * @see #getBase_CentralBufferNode()
	 * @generated
	 */
	void setBase_CentralBufferNode(CentralBufferNode value);

	/**
	 * Returns the value of the '<em><b>Base Activity Parameter Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Activity Parameter Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Activity Parameter Node</em>' reference.
	 * @see #setBase_ActivityParameterNode(ActivityParameterNode)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getBlackBoardEntry_Base_ActivityParameterNode()
	 * @model ordered="false"
	 * @generated
	 */
	ActivityParameterNode getBase_ActivityParameterNode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Activity Parameter Node</em>' reference.
	 * @see #getBase_ActivityParameterNode()
	 * @generated
	 */
	void setBase_ActivityParameterNode(ActivityParameterNode value);

} // BlackBoardEntry
