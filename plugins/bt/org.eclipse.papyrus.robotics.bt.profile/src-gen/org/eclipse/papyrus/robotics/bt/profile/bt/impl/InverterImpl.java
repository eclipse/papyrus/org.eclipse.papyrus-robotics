/**
 * Copyright (c) 2019, 2023 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.Inverter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inverter</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InverterImpl extends DecoratorNodeImpl implements Inverter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InverterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.INVERTER;
	}

} //InverterImpl
