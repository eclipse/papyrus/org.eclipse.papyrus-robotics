/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decorator Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DecoratorNodeImpl#getTreenode <em>Treenode</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecoratorNodeImpl extends TreeNodeImpl implements DecoratorNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecoratorNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.DECORATOR_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public TreeNode getTreenode() {
		TreeNode treenode = null;
		ActivityNode sourceNode = getBase_Action();
		if (sourceNode != null) {
			EList<ActivityEdge> outEdges = sourceNode.getOutgoings();
			if (!outEdges.isEmpty()) {
				ActivityNode targetNode = outEdges.get(0).getTarget();
				if (targetNode != null) {
					TreeNode tn = UMLUtil.getStereotypeApplication(targetNode, TreeNode.class);
					if (tn != null) {
						treenode = tn;
					}
				}
			}
		}
		return treenode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TreeNode basicGetTreenode() {
		// TODO: implement this method to return the 'Treenode' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTreenode(TreeNode newTreenode) {
		// TODO: implement this method to set the 'Treenode' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.DECORATOR_NODE__TREENODE:
				if (resolve) return getTreenode();
				return basicGetTreenode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.DECORATOR_NODE__TREENODE:
				setTreenode((TreeNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.DECORATOR_NODE__TREENODE:
				setTreenode((TreeNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.DECORATOR_NODE__TREENODE:
				return basicGetTreenode() != null;
		}
		return super.eIsSet(featureID);
	}

} //DecoratorNodeImpl
