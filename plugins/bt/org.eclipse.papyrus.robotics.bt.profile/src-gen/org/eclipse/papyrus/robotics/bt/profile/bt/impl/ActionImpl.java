/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.papyrus.robotics.bt.profile.bt.Action;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl#getSkill <em>Skill</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl#getFlowport <em>Flowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl#getInflowport <em>Inflowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl#getOutflowport <em>Outflowport</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionImpl extends LeafNodeImpl implements Action {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SkillDefinition getSkill() {
		SkillDefinition skill = basicGetSkill();
		return skill != null && skill.eIsProxy() ? (SkillDefinition)eResolveProxy((InternalEObject)skill) : skill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SkillDefinition basicGetSkill() {
		SkillDefinition retSkDef = null;
		CallOperationAction coa = (CallOperationAction) getBase_Action();
		if (coa != null) {
			Operation op = coa.getOperation();
			if (op != null) {
				SkillDefinition sd = UMLUtil.getStereotypeApplication(op, SkillDefinition.class);
				if (sd != null) {
					retSkDef = sd;
				}
			}
		}
		return retSkDef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<DataFlowPort> getFlowport() {
		EList<DataFlowPort> dfPorts = new UniqueEList<DataFlowPort>();
		CallOperationAction coa = (CallOperationAction) getBase_Action();
		if (coa != null) {
			dfPorts.addAll(getInflowport());
			dfPorts.addAll(getOutflowport());
		}
		return dfPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<InFlowPort> getInflowport() {
		EList<InFlowPort> inFlowPorts = new UniqueEList<InFlowPort>();
		CallOperationAction coa = (CallOperationAction) getBase_Action();
		if (coa != null) {
			for (InputPin ip : coa.getInputs()) {
				InFlowPort infp = UMLUtil.getStereotypeApplication(ip, InFlowPort.class);
				if (infp != null) {
					inFlowPorts.add(infp);
				}
			}
		}
		return inFlowPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<OutFlowPort> getOutflowport() {
		EList<OutFlowPort> outFlowPorts = new UniqueEList<OutFlowPort>();
		CallOperationAction coa = (CallOperationAction) getBase_Action();
		if (coa != null) {
			for (OutputPin op : coa.getOutputs()) {
				OutFlowPort outfp = UMLUtil.getStereotypeApplication(op, OutFlowPort.class);
				if (outfp != null) {
					outFlowPorts.add(outfp);
				}
			}
		}
		return outFlowPorts;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.ACTION__SKILL:
				if (resolve) return getSkill();
				return basicGetSkill();
			case BtPackage.ACTION__FLOWPORT:
				return getFlowport();
			case BtPackage.ACTION__INFLOWPORT:
				return getInflowport();
			case BtPackage.ACTION__OUTFLOWPORT:
				return getOutflowport();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.ACTION__FLOWPORT:
				getFlowport().clear();
				getFlowport().addAll((Collection<? extends DataFlowPort>)newValue);
				return;
			case BtPackage.ACTION__INFLOWPORT:
				getInflowport().clear();
				getInflowport().addAll((Collection<? extends InFlowPort>)newValue);
				return;
			case BtPackage.ACTION__OUTFLOWPORT:
				getOutflowport().clear();
				getOutflowport().addAll((Collection<? extends OutFlowPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.ACTION__FLOWPORT:
				getFlowport().clear();
				return;
			case BtPackage.ACTION__INFLOWPORT:
				getInflowport().clear();
				return;
			case BtPackage.ACTION__OUTFLOWPORT:
				getOutflowport().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.ACTION__SKILL:
				return basicGetSkill() != null;
			case BtPackage.ACTION__FLOWPORT:
				return !getFlowport().isEmpty();
			case BtPackage.ACTION__INFLOWPORT:
				return !getInflowport().isEmpty();
			case BtPackage.ACTION__OUTFLOWPORT:
				return !getOutflowport().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ActionImpl
