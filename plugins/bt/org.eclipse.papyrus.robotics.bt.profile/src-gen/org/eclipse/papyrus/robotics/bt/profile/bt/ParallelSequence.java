/**
 * Copyright (c) 2019, 2023 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parallel Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getParallelSequence()
 * @model
 * @generated
 */
public interface ParallelSequence extends ControlNode {
} // ParallelSequence
