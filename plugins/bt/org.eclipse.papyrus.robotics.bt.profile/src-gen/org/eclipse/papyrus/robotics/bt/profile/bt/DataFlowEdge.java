/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connects;

import org.eclipse.uml2.uml.ObjectFlow;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Flow Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getInPort <em>In Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getOutPort <em>Out Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBase_ObjectFlow <em>Base Object Flow</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBbEntry <em>Bb Entry</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getDataFlowEdge()
 * @model
 * @generated
 */
public interface DataFlowEdge extends Connects {
	/**
	 * Returns the value of the '<em><b>In Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Port</em>' reference.
	 * @see #setInPort(InFlowPort)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getDataFlowEdge_InPort()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	InFlowPort getInPort();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getInPort <em>In Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Port</em>' reference.
	 * @see #getInPort()
	 * @generated
	 */
	void setInPort(InFlowPort value);

	/**
	 * Returns the value of the '<em><b>Out Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Port</em>' reference.
	 * @see #setOutPort(OutFlowPort)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getDataFlowEdge_OutPort()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	OutFlowPort getOutPort();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getOutPort <em>Out Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Out Port</em>' reference.
	 * @see #getOutPort()
	 * @generated
	 */
	void setOutPort(OutFlowPort value);

	/**
	 * Returns the value of the '<em><b>Base Object Flow</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Object Flow</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Object Flow</em>' reference.
	 * @see #setBase_ObjectFlow(ObjectFlow)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getDataFlowEdge_Base_ObjectFlow()
	 * @model ordered="false"
	 * @generated
	 */
	ObjectFlow getBase_ObjectFlow();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBase_ObjectFlow <em>Base Object Flow</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Object Flow</em>' reference.
	 * @see #getBase_ObjectFlow()
	 * @generated
	 */
	void setBase_ObjectFlow(ObjectFlow value);

	/**
	 * Returns the value of the '<em><b>Bb Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bb Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bb Entry</em>' reference.
	 * @see #setBbEntry(BlackBoardEntry)
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getDataFlowEdge_BbEntry()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	BlackBoardEntry getBbEntry();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBbEntry <em>Bb Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bb Entry</em>' reference.
	 * @see #getBbEntry()
	 * @generated
	 */
	void setBbEntry(BlackBoardEntry value);

} // DataFlowEdge
