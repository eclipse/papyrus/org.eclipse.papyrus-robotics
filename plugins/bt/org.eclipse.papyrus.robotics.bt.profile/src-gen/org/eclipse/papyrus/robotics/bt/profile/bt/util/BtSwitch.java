/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Block;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connector;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Connects;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Relation;

import org.eclipse.papyrus.robotics.bt.profile.bt.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage
 * @generated
 */
public class BtSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BtPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BtSwitch() {
		if (modelPackage == null) {
			modelPackage = BtPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BtPackage.TREE_ROOT: {
				TreeRoot treeRoot = (TreeRoot)theEObject;
				T result = caseTreeRoot(treeRoot);
				if (result == null) result = caseEntity(treeRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.TREE_NODE: {
				TreeNode treeNode = (TreeNode)theEObject;
				T result = caseTreeNode(treeNode);
				if (result == null) result = caseBlock(treeNode);
				if (result == null) result = caseEntity(treeNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.SUB_TREE: {
				SubTree subTree = (SubTree)theEObject;
				T result = caseSubTree(subTree);
				if (result == null) result = caseTreeNode(subTree);
				if (result == null) result = caseBlock(subTree);
				if (result == null) result = caseEntity(subTree);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.DATA_FLOW_PORT: {
				DataFlowPort dataFlowPort = (DataFlowPort)theEObject;
				T result = caseDataFlowPort(dataFlowPort);
				if (result == null) result = casePort(dataFlowPort);
				if (result == null) result = caseEntity(dataFlowPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.IN_FLOW_PORT: {
				InFlowPort inFlowPort = (InFlowPort)theEObject;
				T result = caseInFlowPort(inFlowPort);
				if (result == null) result = caseDataFlowPort(inFlowPort);
				if (result == null) result = casePort(inFlowPort);
				if (result == null) result = caseEntity(inFlowPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.OUT_FLOW_PORT: {
				OutFlowPort outFlowPort = (OutFlowPort)theEObject;
				T result = caseOutFlowPort(outFlowPort);
				if (result == null) result = caseDataFlowPort(outFlowPort);
				if (result == null) result = casePort(outFlowPort);
				if (result == null) result = caseEntity(outFlowPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.CONDITION: {
				Condition condition = (Condition)theEObject;
				T result = caseCondition(condition);
				if (result == null) result = caseLeafNode(condition);
				if (result == null) result = caseTreeNode(condition);
				if (result == null) result = caseBlock(condition);
				if (result == null) result = caseEntity(condition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.LEAF_NODE: {
				LeafNode leafNode = (LeafNode)theEObject;
				T result = caseLeafNode(leafNode);
				if (result == null) result = caseTreeNode(leafNode);
				if (result == null) result = caseBlock(leafNode);
				if (result == null) result = caseEntity(leafNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.ACTION: {
				Action action = (Action)theEObject;
				T result = caseAction(action);
				if (result == null) result = caseLeafNode(action);
				if (result == null) result = caseTreeNode(action);
				if (result == null) result = caseBlock(action);
				if (result == null) result = caseEntity(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.FALLBACK: {
				Fallback fallback = (Fallback)theEObject;
				T result = caseFallback(fallback);
				if (result == null) result = caseControlNode(fallback);
				if (result == null) result = caseTreeNode(fallback);
				if (result == null) result = caseBlock(fallback);
				if (result == null) result = caseEntity(fallback);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.CONTROL_NODE: {
				ControlNode controlNode = (ControlNode)theEObject;
				T result = caseControlNode(controlNode);
				if (result == null) result = caseTreeNode(controlNode);
				if (result == null) result = caseBlock(controlNode);
				if (result == null) result = caseEntity(controlNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.SEQUENCE: {
				Sequence sequence = (Sequence)theEObject;
				T result = caseSequence(sequence);
				if (result == null) result = caseControlNode(sequence);
				if (result == null) result = caseTreeNode(sequence);
				if (result == null) result = caseBlock(sequence);
				if (result == null) result = caseEntity(sequence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = caseBlock(parameter);
				if (result == null) result = caseEntity(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.DECORATOR_NODE: {
				DecoratorNode decoratorNode = (DecoratorNode)theEObject;
				T result = caseDecoratorNode(decoratorNode);
				if (result == null) result = caseTreeNode(decoratorNode);
				if (result == null) result = caseBlock(decoratorNode);
				if (result == null) result = caseEntity(decoratorNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.CONTROL_FLOW_EDGE: {
				ControlFlowEdge controlFlowEdge = (ControlFlowEdge)theEObject;
				T result = caseControlFlowEdge(controlFlowEdge);
				if (result == null) result = caseConnects(controlFlowEdge);
				if (result == null) result = caseRelation(controlFlowEdge);
				if (result == null) result = caseBlock(controlFlowEdge);
				if (result == null) result = caseEntity(controlFlowEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.DATA_FLOW_EDGE: {
				DataFlowEdge dataFlowEdge = (DataFlowEdge)theEObject;
				T result = caseDataFlowEdge(dataFlowEdge);
				if (result == null) result = caseConnects(dataFlowEdge);
				if (result == null) result = caseRelation(dataFlowEdge);
				if (result == null) result = caseBlock(dataFlowEdge);
				if (result == null) result = caseEntity(dataFlowEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.BLACK_BOARD_ENTRY: {
				BlackBoardEntry blackBoardEntry = (BlackBoardEntry)theEObject;
				T result = caseBlackBoardEntry(blackBoardEntry);
				if (result == null) result = caseConnector(blackBoardEntry);
				if (result == null) result = caseEntity(blackBoardEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.REACTIVE_FALLBACK: {
				ReactiveFallback reactiveFallback = (ReactiveFallback)theEObject;
				T result = caseReactiveFallback(reactiveFallback);
				if (result == null) result = caseControlNode(reactiveFallback);
				if (result == null) result = caseTreeNode(reactiveFallback);
				if (result == null) result = caseBlock(reactiveFallback);
				if (result == null) result = caseEntity(reactiveFallback);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.REACTIVE_SEQUENCE: {
				ReactiveSequence reactiveSequence = (ReactiveSequence)theEObject;
				T result = caseReactiveSequence(reactiveSequence);
				if (result == null) result = caseControlNode(reactiveSequence);
				if (result == null) result = caseTreeNode(reactiveSequence);
				if (result == null) result = caseBlock(reactiveSequence);
				if (result == null) result = caseEntity(reactiveSequence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.INVERTER: {
				Inverter inverter = (Inverter)theEObject;
				T result = caseInverter(inverter);
				if (result == null) result = caseDecoratorNode(inverter);
				if (result == null) result = caseTreeNode(inverter);
				if (result == null) result = caseBlock(inverter);
				if (result == null) result = caseEntity(inverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.RETRY_UNTIL_SUCCESSFUL: {
				RetryUntilSuccessful retryUntilSuccessful = (RetryUntilSuccessful)theEObject;
				T result = caseRetryUntilSuccessful(retryUntilSuccessful);
				if (result == null) result = caseDecoratorNode(retryUntilSuccessful);
				if (result == null) result = caseTreeNode(retryUntilSuccessful);
				if (result == null) result = caseBlock(retryUntilSuccessful);
				if (result == null) result = caseEntity(retryUntilSuccessful);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.REPEAT: {
				Repeat repeat = (Repeat)theEObject;
				T result = caseRepeat(repeat);
				if (result == null) result = caseDecoratorNode(repeat);
				if (result == null) result = caseTreeNode(repeat);
				if (result == null) result = caseBlock(repeat);
				if (result == null) result = caseEntity(repeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BtPackage.PARALLEL_SEQUENCE: {
				ParallelSequence parallelSequence = (ParallelSequence)theEObject;
				T result = caseParallelSequence(parallelSequence);
				if (result == null) result = caseControlNode(parallelSequence);
				if (result == null) result = caseTreeNode(parallelSequence);
				if (result == null) result = caseBlock(parallelSequence);
				if (result == null) result = caseEntity(parallelSequence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tree Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tree Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTreeRoot(TreeRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tree Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tree Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTreeNode(TreeNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub Tree</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub Tree</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubTree(SubTree object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCondition(Condition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeafNode(LeafNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Flow Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Flow Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFlowPort(DataFlowPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Flow Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Flow Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInFlowPort(InFlowPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Out Flow Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Out Flow Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutFlowPort(OutFlowPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fallback</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fallback</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFallback(Fallback object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlNode(ControlNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequence(Sequence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decorator Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decorator Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecoratorNode(DecoratorNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Flow Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Flow Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlFlowEdge(ControlFlowEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Flow Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Flow Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFlowEdge(DataFlowEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Black Board Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Black Board Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlackBoardEntry(BlackBoardEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reactive Fallback</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reactive Fallback</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReactiveFallback(ReactiveFallback object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reactive Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reactive Sequence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReactiveSequence(ReactiveSequence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inverter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inverter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInverter(Inverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Retry Until Successful</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Retry Until Successful</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRetryUntilSuccessful(RetryUntilSuccessful object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Repeat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Repeat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRepeat(Repeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parallel Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parallel Sequence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParallelSequence(ParallelSequence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelation(Relation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connects</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connects</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnects(Connects object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnector(Connector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BtSwitch
