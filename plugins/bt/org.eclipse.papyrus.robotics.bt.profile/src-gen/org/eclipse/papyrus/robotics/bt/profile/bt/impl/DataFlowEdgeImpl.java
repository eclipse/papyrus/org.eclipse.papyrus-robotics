/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectsImpl;

import org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge;
import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Flow Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl#getInPort <em>In Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl#getOutPort <em>Out Port</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl#getBase_ObjectFlow <em>Base Object Flow</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl#getBbEntry <em>Bb Entry</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataFlowEdgeImpl extends ConnectsImpl implements DataFlowEdge {
	/**
	 * The cached value of the '{@link #getBase_ObjectFlow() <em>Base Object Flow</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_ObjectFlow()
	 * @generated
	 * @ordered
	 */
	protected ObjectFlow base_ObjectFlow;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataFlowEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.DATA_FLOW_EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InFlowPort getInPort() {
		InFlowPort inPort = basicGetInPort();
		return inPort != null && inPort.eIsProxy() ? (InFlowPort)eResolveProxy((InternalEObject)inPort) : inPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public InFlowPort basicGetInPort() {
		// InFlowPorts can only be targets of DF edges (but it could be null when the target is a BlackBoard) 
		return ( UMLUtil.getStereotypeApplication(getBase_ObjectFlow().getTarget(), InFlowPort.class) );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInPort(InFlowPort newInPort) {
		// TODO: implement this method to set the 'In Port' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OutFlowPort getOutPort() {
		OutFlowPort outPort = basicGetOutPort();
		return outPort != null && outPort.eIsProxy() ? (OutFlowPort)eResolveProxy((InternalEObject)outPort) : outPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public OutFlowPort basicGetOutPort() {
		// OutFlowPorts can only be sources of DF edges (but it could be null when the source is a BlackBoard) 
		return ( UMLUtil.getStereotypeApplication(getBase_ObjectFlow().getSource(), OutFlowPort.class) );
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutPort(OutFlowPort newOutPort) {
		// TODO: implement this method to set the 'Out Port' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ObjectFlow getBase_ObjectFlow() {
		if (base_ObjectFlow != null && base_ObjectFlow.eIsProxy()) {
			InternalEObject oldBase_ObjectFlow = (InternalEObject)base_ObjectFlow;
			base_ObjectFlow = (ObjectFlow)eResolveProxy(oldBase_ObjectFlow);
			if (base_ObjectFlow != oldBase_ObjectFlow) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BtPackage.DATA_FLOW_EDGE__BASE_OBJECT_FLOW, oldBase_ObjectFlow, base_ObjectFlow));
			}
		}
		return base_ObjectFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectFlow basicGetBase_ObjectFlow() {
		return base_ObjectFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_ObjectFlow(ObjectFlow newBase_ObjectFlow) {
		ObjectFlow oldBase_ObjectFlow = base_ObjectFlow;
		base_ObjectFlow = newBase_ObjectFlow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BtPackage.DATA_FLOW_EDGE__BASE_OBJECT_FLOW, oldBase_ObjectFlow, base_ObjectFlow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BlackBoardEntry getBbEntry() {
		BlackBoardEntry bbEntry = basicGetBbEntry();
		return bbEntry != null && bbEntry.eIsProxy() ? (BlackBoardEntry)eResolveProxy((InternalEObject)bbEntry) : bbEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BlackBoardEntry basicGetBbEntry() {
		// A BlackBoardEntry can be either a source or a target of a DF edge
		// (but it could be null when the edge connects directly Out and In flow ports)
		BlackBoardEntry ret = UMLUtil.getStereotypeApplication(getBase_ObjectFlow().getTarget(), BlackBoardEntry.class);
		if (ret == null)
			ret = UMLUtil.getStereotypeApplication(getBase_ObjectFlow().getSource(), BlackBoardEntry.class);
		return ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBbEntry(BlackBoardEntry newBbEntry) {
		// TODO: implement this method to set the 'Bb Entry' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.DATA_FLOW_EDGE__IN_PORT:
				if (resolve) return getInPort();
				return basicGetInPort();
			case BtPackage.DATA_FLOW_EDGE__OUT_PORT:
				if (resolve) return getOutPort();
				return basicGetOutPort();
			case BtPackage.DATA_FLOW_EDGE__BASE_OBJECT_FLOW:
				if (resolve) return getBase_ObjectFlow();
				return basicGetBase_ObjectFlow();
			case BtPackage.DATA_FLOW_EDGE__BB_ENTRY:
				if (resolve) return getBbEntry();
				return basicGetBbEntry();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.DATA_FLOW_EDGE__IN_PORT:
				setInPort((InFlowPort)newValue);
				return;
			case BtPackage.DATA_FLOW_EDGE__OUT_PORT:
				setOutPort((OutFlowPort)newValue);
				return;
			case BtPackage.DATA_FLOW_EDGE__BASE_OBJECT_FLOW:
				setBase_ObjectFlow((ObjectFlow)newValue);
				return;
			case BtPackage.DATA_FLOW_EDGE__BB_ENTRY:
				setBbEntry((BlackBoardEntry)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.DATA_FLOW_EDGE__IN_PORT:
				setInPort((InFlowPort)null);
				return;
			case BtPackage.DATA_FLOW_EDGE__OUT_PORT:
				setOutPort((OutFlowPort)null);
				return;
			case BtPackage.DATA_FLOW_EDGE__BASE_OBJECT_FLOW:
				setBase_ObjectFlow((ObjectFlow)null);
				return;
			case BtPackage.DATA_FLOW_EDGE__BB_ENTRY:
				setBbEntry((BlackBoardEntry)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.DATA_FLOW_EDGE__IN_PORT:
				return basicGetInPort() != null;
			case BtPackage.DATA_FLOW_EDGE__OUT_PORT:
				return basicGetOutPort() != null;
			case BtPackage.DATA_FLOW_EDGE__BASE_OBJECT_FLOW:
				return base_ObjectFlow != null;
			case BtPackage.DATA_FLOW_EDGE__BB_ENTRY:
				return basicGetBbEntry() != null;
		}
		return super.eIsSet(featureID);
	}

} //DataFlowEdgeImpl
