/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.PortImpl;

import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Flow Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DataFlowPortImpl extends PortImpl implements DataFlowPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataFlowPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.DATA_FLOW_PORT;
	}

} //DataFlowPortImpl
