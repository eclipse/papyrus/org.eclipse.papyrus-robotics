/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.SubTree;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sub Tree</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl#getTreeroot <em>Treeroot</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl#getFlowport <em>Flowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl#getInflowport <em>Inflowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl#getOutflowport <em>Outflowport</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubTreeImpl extends TreeNodeImpl implements SubTree {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubTreeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.SUB_TREE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TreeRoot getTreeroot() {
		TreeRoot treeroot = basicGetTreeroot();
		return treeroot != null && treeroot.eIsProxy() ? (TreeRoot)eResolveProxy((InternalEObject)treeroot) : treeroot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public TreeRoot basicGetTreeroot() {
		TreeRoot retTreeRoot = null;
		CallBehaviorAction cba = (CallBehaviorAction) getBase_Action();
		if (cba != null) {
			Behavior b = cba.getBehavior();
			if (b != null) {
				TreeRoot tr = UMLUtil.getStereotypeApplication(b, TreeRoot.class);
				if (tr != null) {
					retTreeRoot = tr;
				}
			}
		}
		return retTreeRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTreeroot(TreeRoot newTreeroot) {
		// TODO: implement this method to set the 'Treeroot' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<DataFlowPort> getFlowport() {
		EList<DataFlowPort> dfPorts = new UniqueEList<DataFlowPort>();
		CallBehaviorAction cba = (CallBehaviorAction) getBase_Action();
		if (cba != null) {
			dfPorts.addAll(getInflowport());
			dfPorts.addAll(getOutflowport());
		}
		return dfPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<InFlowPort> getInflowport() {
		EList<InFlowPort> inFlowPorts = new UniqueEList<InFlowPort>();
		CallBehaviorAction cba = (CallBehaviorAction) getBase_Action();
		if (cba != null) {
			for (InputPin ip : cba.getInputs()) {
				InFlowPort infp = UMLUtil.getStereotypeApplication(ip, InFlowPort.class);
				if (infp != null) {
					inFlowPorts.add(infp);
				}
			}
		}
		return inFlowPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<OutFlowPort> getOutflowport() {
		EList<OutFlowPort> outFlowPorts = new UniqueEList<OutFlowPort>();
		CallBehaviorAction cba = (CallBehaviorAction) getBase_Action();
		if (cba != null) {
			for (OutputPin op : cba.getOutputs()) {
				OutFlowPort outfp = UMLUtil.getStereotypeApplication(op, OutFlowPort.class);
				if (outfp != null) {
					outFlowPorts.add(outfp);
				}
			}
		}
		return outFlowPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.SUB_TREE__TREEROOT:
				if (resolve) return getTreeroot();
				return basicGetTreeroot();
			case BtPackage.SUB_TREE__FLOWPORT:
				return getFlowport();
			case BtPackage.SUB_TREE__INFLOWPORT:
				return getInflowport();
			case BtPackage.SUB_TREE__OUTFLOWPORT:
				return getOutflowport();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.SUB_TREE__TREEROOT:
				setTreeroot((TreeRoot)newValue);
				return;
			case BtPackage.SUB_TREE__FLOWPORT:
				getFlowport().clear();
				getFlowport().addAll((Collection<? extends DataFlowPort>)newValue);
				return;
			case BtPackage.SUB_TREE__INFLOWPORT:
				getInflowport().clear();
				getInflowport().addAll((Collection<? extends InFlowPort>)newValue);
				return;
			case BtPackage.SUB_TREE__OUTFLOWPORT:
				getOutflowport().clear();
				getOutflowport().addAll((Collection<? extends OutFlowPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.SUB_TREE__TREEROOT:
				setTreeroot((TreeRoot)null);
				return;
			case BtPackage.SUB_TREE__FLOWPORT:
				getFlowport().clear();
				return;
			case BtPackage.SUB_TREE__INFLOWPORT:
				getInflowport().clear();
				return;
			case BtPackage.SUB_TREE__OUTFLOWPORT:
				getOutflowport().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.SUB_TREE__TREEROOT:
				return basicGetTreeroot() != null;
			case BtPackage.SUB_TREE__FLOWPORT:
				return !getFlowport().isEmpty();
			case BtPackage.SUB_TREE__INFLOWPORT:
				return !getInflowport().isEmpty();
			case BtPackage.SUB_TREE__OUTFLOWPORT:
				return !getOutflowport().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SubTreeImpl
