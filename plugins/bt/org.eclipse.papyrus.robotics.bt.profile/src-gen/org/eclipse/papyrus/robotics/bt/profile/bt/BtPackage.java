/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtFactory
 * @model kind="package"
 * @generated
 */
public interface BtPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bt"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/bt/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "bt"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BtPackage eINSTANCE = org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeRootImpl <em>Tree Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeRootImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getTreeRoot()
	 * @generated
	 */
	int TREE_ROOT = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__BASE_ACTIVITY = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT__TREENODE = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tree Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tree Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_ROOT_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeNodeImpl <em>Tree Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeNodeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getTreeNode()
	 * @generated
	 */
	int TREE_NODE = 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__PROPERTY = BPCPackage.BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__INSTANCE_UID = BPCPackage.BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__DESCRIPTION = BPCPackage.BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__AUTHORSHIP = BPCPackage.BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__PROVENANCE = BPCPackage.BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__MODEL_UID = BPCPackage.BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__METAMODEL_UID = BPCPackage.BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__PORT = BPCPackage.BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__CONNECTOR = BPCPackage.BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__COLLECTION = BPCPackage.BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__BLOCK = BPCPackage.BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__RELATION = BPCPackage.BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__BASE_CLASS = BPCPackage.BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE__BASE_ACTION = BPCPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tree Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_FEATURE_COUNT = BPCPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Tree Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TREE_NODE_OPERATION_COUNT = BPCPackage.BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl <em>Sub Tree</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getSubTree()
	 * @generated
	 */
	int SUB_TREE = 2;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__PROPERTY = TREE_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__INSTANCE_UID = TREE_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__DESCRIPTION = TREE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__AUTHORSHIP = TREE_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__PROVENANCE = TREE_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__MODEL_UID = TREE_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__METAMODEL_UID = TREE_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__PORT = TREE_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__CONNECTOR = TREE_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__COLLECTION = TREE_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__BLOCK = TREE_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__RELATION = TREE_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__BASE_CLASS = TREE_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__BASE_ACTION = TREE_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treeroot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__TREEROOT = TREE_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Flowport</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__FLOWPORT = TREE_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Inflowport</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__INFLOWPORT = TREE_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Outflowport</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE__OUTFLOWPORT = TREE_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Sub Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_FEATURE_COUNT = TREE_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Sub Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_TREE_OPERATION_COUNT = TREE_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.LeafNodeImpl <em>Leaf Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.LeafNodeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getLeafNode()
	 * @generated
	 */
	int LEAF_NODE = 7;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ConditionImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 6;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 8;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowPortImpl <em>Data Flow Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowPortImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getDataFlowPort()
	 * @generated
	 */
	int DATA_FLOW_PORT = 3;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__PROPERTY = BPCPackage.PORT__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__INSTANCE_UID = BPCPackage.PORT__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__DESCRIPTION = BPCPackage.PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__AUTHORSHIP = BPCPackage.PORT__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__PROVENANCE = BPCPackage.PORT__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__MODEL_UID = BPCPackage.PORT__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT__METAMODEL_UID = BPCPackage.PORT__METAMODEL_UID;

	/**
	 * The number of structural features of the '<em>Data Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT_FEATURE_COUNT = BPCPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Data Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_PORT_OPERATION_COUNT = BPCPackage.PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.InFlowPortImpl <em>In Flow Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.InFlowPortImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getInFlowPort()
	 * @generated
	 */
	int IN_FLOW_PORT = 4;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__PROPERTY = DATA_FLOW_PORT__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__INSTANCE_UID = DATA_FLOW_PORT__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__DESCRIPTION = DATA_FLOW_PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__AUTHORSHIP = DATA_FLOW_PORT__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__PROVENANCE = DATA_FLOW_PORT__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__MODEL_UID = DATA_FLOW_PORT__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__METAMODEL_UID = DATA_FLOW_PORT__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Input Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__BASE_INPUT_PIN = DATA_FLOW_PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Activity Parameter Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE = DATA_FLOW_PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>In Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT_FEATURE_COUNT = DATA_FLOW_PORT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>In Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_FLOW_PORT_OPERATION_COUNT = DATA_FLOW_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.OutFlowPortImpl <em>Out Flow Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.OutFlowPortImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getOutFlowPort()
	 * @generated
	 */
	int OUT_FLOW_PORT = 5;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__PROPERTY = DATA_FLOW_PORT__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__INSTANCE_UID = DATA_FLOW_PORT__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__DESCRIPTION = DATA_FLOW_PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__AUTHORSHIP = DATA_FLOW_PORT__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__PROVENANCE = DATA_FLOW_PORT__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__MODEL_UID = DATA_FLOW_PORT__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__METAMODEL_UID = DATA_FLOW_PORT__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Base Output Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__BASE_OUTPUT_PIN = DATA_FLOW_PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Activity Parameter Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE = DATA_FLOW_PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Out Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT_FEATURE_COUNT = DATA_FLOW_PORT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Out Flow Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_FLOW_PORT_OPERATION_COUNT = DATA_FLOW_PORT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__PROPERTY = TREE_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__INSTANCE_UID = TREE_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__DESCRIPTION = TREE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__AUTHORSHIP = TREE_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__PROVENANCE = TREE_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__MODEL_UID = TREE_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__METAMODEL_UID = TREE_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__PORT = TREE_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__CONNECTOR = TREE_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__COLLECTION = TREE_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__BLOCK = TREE_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__RELATION = TREE_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__BASE_CLASS = TREE_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE__BASE_ACTION = TREE_NODE__BASE_ACTION;

	/**
	 * The number of structural features of the '<em>Leaf Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE_FEATURE_COUNT = TREE_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Leaf Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_NODE_OPERATION_COUNT = TREE_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__PROPERTY = LEAF_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__INSTANCE_UID = LEAF_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__DESCRIPTION = LEAF_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__AUTHORSHIP = LEAF_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__PROVENANCE = LEAF_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__MODEL_UID = LEAF_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__METAMODEL_UID = LEAF_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__PORT = LEAF_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__CONNECTOR = LEAF_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__COLLECTION = LEAF_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__BLOCK = LEAF_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__RELATION = LEAF_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__BASE_CLASS = LEAF_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__BASE_ACTION = LEAF_NODE__BASE_ACTION;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = LEAF_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATION_COUNT = LEAF_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PROPERTY = LEAF_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__INSTANCE_UID = LEAF_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DESCRIPTION = LEAF_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__AUTHORSHIP = LEAF_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PROVENANCE = LEAF_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__MODEL_UID = LEAF_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__METAMODEL_UID = LEAF_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__PORT = LEAF_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__CONNECTOR = LEAF_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__COLLECTION = LEAF_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__BLOCK = LEAF_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__RELATION = LEAF_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__BASE_CLASS = LEAF_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__BASE_ACTION = LEAF_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__SKILL = LEAF_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Flowport</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__FLOWPORT = LEAF_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Inflowport</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__INFLOWPORT = LEAF_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Outflowport</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__OUTFLOWPORT = LEAF_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = LEAF_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = LEAF_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlNodeImpl <em>Control Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlNodeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getControlNode()
	 * @generated
	 */
	int CONTROL_NODE = 10;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__PROPERTY = TREE_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__INSTANCE_UID = TREE_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__DESCRIPTION = TREE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__AUTHORSHIP = TREE_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__PROVENANCE = TREE_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__MODEL_UID = TREE_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__METAMODEL_UID = TREE_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__PORT = TREE_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__CONNECTOR = TREE_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__COLLECTION = TREE_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__BLOCK = TREE_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__RELATION = TREE_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__BASE_CLASS = TREE_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__BASE_ACTION = TREE_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__TREENODE = TREE_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Control Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_FEATURE_COUNT = TREE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Control Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_OPERATION_COUNT = TREE_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.FallbackImpl <em>Fallback</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.FallbackImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getFallback()
	 * @generated
	 */
	int FALLBACK = 9;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__PROPERTY = CONTROL_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__INSTANCE_UID = CONTROL_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__DESCRIPTION = CONTROL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__AUTHORSHIP = CONTROL_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__PROVENANCE = CONTROL_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__MODEL_UID = CONTROL_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__METAMODEL_UID = CONTROL_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__PORT = CONTROL_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__CONNECTOR = CONTROL_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__COLLECTION = CONTROL_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__BLOCK = CONTROL_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__RELATION = CONTROL_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__BASE_CLASS = CONTROL_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__BASE_ACTION = CONTROL_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK__TREENODE = CONTROL_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Fallback</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_FEATURE_COUNT = CONTROL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fallback</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALLBACK_OPERATION_COUNT = CONTROL_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SequenceImpl <em>Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.SequenceImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getSequence()
	 * @generated
	 */
	int SEQUENCE = 11;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__PROPERTY = CONTROL_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__INSTANCE_UID = CONTROL_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__DESCRIPTION = CONTROL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__AUTHORSHIP = CONTROL_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__PROVENANCE = CONTROL_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__MODEL_UID = CONTROL_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__METAMODEL_UID = CONTROL_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__PORT = CONTROL_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__CONNECTOR = CONTROL_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__COLLECTION = CONTROL_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__BLOCK = CONTROL_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__RELATION = CONTROL_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__BASE_CLASS = CONTROL_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__BASE_ACTION = CONTROL_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__TREENODE = CONTROL_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FEATURE_COUNT = CONTROL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_OPERATION_COUNT = CONTROL_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParameterImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 12;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PROPERTY = BPCPackage.BLOCK__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__INSTANCE_UID = BPCPackage.BLOCK__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__DESCRIPTION = BPCPackage.BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__AUTHORSHIP = BPCPackage.BLOCK__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PROVENANCE = BPCPackage.BLOCK__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__MODEL_UID = BPCPackage.BLOCK__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__METAMODEL_UID = BPCPackage.BLOCK__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PORT = BPCPackage.BLOCK__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__CONNECTOR = BPCPackage.BLOCK__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__COLLECTION = BPCPackage.BLOCK__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__BLOCK = BPCPackage.BLOCK__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__RELATION = BPCPackage.BLOCK__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__BASE_CLASS = BPCPackage.BLOCK__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Value Specification Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__BASE_VALUE_SPECIFICATION_ACTION = BPCPackage.BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = BPCPackage.BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_OPERATION_COUNT = BPCPackage.BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DecoratorNodeImpl <em>Decorator Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.DecoratorNodeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getDecoratorNode()
	 * @generated
	 */
	int DECORATOR_NODE = 13;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__PROPERTY = TREE_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__INSTANCE_UID = TREE_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__DESCRIPTION = TREE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__AUTHORSHIP = TREE_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__PROVENANCE = TREE_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__MODEL_UID = TREE_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__METAMODEL_UID = TREE_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__PORT = TREE_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__CONNECTOR = TREE_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__COLLECTION = TREE_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__BLOCK = TREE_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__RELATION = TREE_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__BASE_CLASS = TREE_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__BASE_ACTION = TREE_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE__TREENODE = TREE_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Decorator Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE_FEATURE_COUNT = TREE_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Decorator Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECORATOR_NODE_OPERATION_COUNT = TREE_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlFlowEdgeImpl <em>Control Flow Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlFlowEdgeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getControlFlowEdge()
	 * @generated
	 */
	int CONTROL_FLOW_EDGE = 14;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__PROPERTY = BPCPackage.CONNECTS__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__INSTANCE_UID = BPCPackage.CONNECTS__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__DESCRIPTION = BPCPackage.CONNECTS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__AUTHORSHIP = BPCPackage.CONNECTS__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__PROVENANCE = BPCPackage.CONNECTS__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__MODEL_UID = BPCPackage.CONNECTS__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__METAMODEL_UID = BPCPackage.CONNECTS__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__PORT = BPCPackage.CONNECTS__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__CONNECTOR = BPCPackage.CONNECTS__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__COLLECTION = BPCPackage.CONNECTS__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__BLOCK = BPCPackage.CONNECTS__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__RELATION = BPCPackage.CONNECTS__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__BASE_CLASS = BPCPackage.CONNECTS__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__REIFICATION = BPCPackage.CONNECTS__REIFICATION;

	/**
	 * The feature id for the '<em><b>Base Control Flow</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__BASE_CONTROL_FLOW = BPCPackage.CONNECTS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Control Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE_FEATURE_COUNT = BPCPackage.CONNECTS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Control Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE_OPERATION_COUNT = BPCPackage.CONNECTS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl <em>Data Flow Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getDataFlowEdge()
	 * @generated
	 */
	int DATA_FLOW_EDGE = 15;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__PROPERTY = BPCPackage.CONNECTS__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__INSTANCE_UID = BPCPackage.CONNECTS__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__DESCRIPTION = BPCPackage.CONNECTS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__AUTHORSHIP = BPCPackage.CONNECTS__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__PROVENANCE = BPCPackage.CONNECTS__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__MODEL_UID = BPCPackage.CONNECTS__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__METAMODEL_UID = BPCPackage.CONNECTS__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__PORT = BPCPackage.CONNECTS__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__CONNECTOR = BPCPackage.CONNECTS__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__COLLECTION = BPCPackage.CONNECTS__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__BLOCK = BPCPackage.CONNECTS__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__RELATION = BPCPackage.CONNECTS__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__BASE_CLASS = BPCPackage.CONNECTS__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Reification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__REIFICATION = BPCPackage.CONNECTS__REIFICATION;

	/**
	 * The feature id for the '<em><b>In Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__IN_PORT = BPCPackage.CONNECTS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__OUT_PORT = BPCPackage.CONNECTS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Object Flow</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__BASE_OBJECT_FLOW = BPCPackage.CONNECTS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Bb Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE__BB_ENTRY = BPCPackage.CONNECTS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Data Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE_FEATURE_COUNT = BPCPackage.CONNECTS_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Data Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_EDGE_OPERATION_COUNT = BPCPackage.CONNECTS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl <em>Black Board Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getBlackBoardEntry()
	 * @generated
	 */
	int BLACK_BOARD_ENTRY = 16;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__PROPERTY = BPCPackage.CONNECTOR__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__INSTANCE_UID = BPCPackage.CONNECTOR__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__DESCRIPTION = BPCPackage.CONNECTOR__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__AUTHORSHIP = BPCPackage.CONNECTOR__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__PROVENANCE = BPCPackage.CONNECTOR__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__MODEL_UID = BPCPackage.CONNECTOR__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__METAMODEL_UID = BPCPackage.CONNECTOR__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Df Edge</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__DF_EDGE = BPCPackage.CONNECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base Central Buffer Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE = BPCPackage.CONNECTOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Activity Parameter Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE = BPCPackage.CONNECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Black Board Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY_FEATURE_COUNT = BPCPackage.CONNECTOR_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Black Board Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOARD_ENTRY_OPERATION_COUNT = BPCPackage.CONNECTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveFallbackImpl <em>Reactive Fallback</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveFallbackImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getReactiveFallback()
	 * @generated
	 */
	int REACTIVE_FALLBACK = 17;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__PROPERTY = CONTROL_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__INSTANCE_UID = CONTROL_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__DESCRIPTION = CONTROL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__AUTHORSHIP = CONTROL_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__PROVENANCE = CONTROL_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__MODEL_UID = CONTROL_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__METAMODEL_UID = CONTROL_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__PORT = CONTROL_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__CONNECTOR = CONTROL_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__COLLECTION = CONTROL_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__BLOCK = CONTROL_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__RELATION = CONTROL_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__BASE_CLASS = CONTROL_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__BASE_ACTION = CONTROL_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK__TREENODE = CONTROL_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Reactive Fallback</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_FEATURE_COUNT = CONTROL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reactive Fallback</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_FALLBACK_OPERATION_COUNT = CONTROL_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveSequenceImpl <em>Reactive Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveSequenceImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getReactiveSequence()
	 * @generated
	 */
	int REACTIVE_SEQUENCE = 18;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__PROPERTY = CONTROL_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__INSTANCE_UID = CONTROL_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__DESCRIPTION = CONTROL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__AUTHORSHIP = CONTROL_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__PROVENANCE = CONTROL_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__MODEL_UID = CONTROL_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__METAMODEL_UID = CONTROL_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__PORT = CONTROL_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__CONNECTOR = CONTROL_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__COLLECTION = CONTROL_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__BLOCK = CONTROL_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__RELATION = CONTROL_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__BASE_CLASS = CONTROL_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__BASE_ACTION = CONTROL_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE__TREENODE = CONTROL_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Reactive Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_FEATURE_COUNT = CONTROL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reactive Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACTIVE_SEQUENCE_OPERATION_COUNT = CONTROL_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.InverterImpl <em>Inverter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.InverterImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getInverter()
	 * @generated
	 */
	int INVERTER = 19;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__PROPERTY = DECORATOR_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__INSTANCE_UID = DECORATOR_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__DESCRIPTION = DECORATOR_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__AUTHORSHIP = DECORATOR_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__PROVENANCE = DECORATOR_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__MODEL_UID = DECORATOR_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__METAMODEL_UID = DECORATOR_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__PORT = DECORATOR_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__CONNECTOR = DECORATOR_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__COLLECTION = DECORATOR_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__BLOCK = DECORATOR_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__RELATION = DECORATOR_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__BASE_CLASS = DECORATOR_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__BASE_ACTION = DECORATOR_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER__TREENODE = DECORATOR_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Inverter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_FEATURE_COUNT = DECORATOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Inverter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTER_OPERATION_COUNT = DECORATOR_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.RetryUntilSuccessfulImpl <em>Retry Until Successful</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.RetryUntilSuccessfulImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getRetryUntilSuccessful()
	 * @generated
	 */
	int RETRY_UNTIL_SUCCESSFUL = 20;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__PROPERTY = DECORATOR_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__INSTANCE_UID = DECORATOR_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__DESCRIPTION = DECORATOR_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__AUTHORSHIP = DECORATOR_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__PROVENANCE = DECORATOR_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__MODEL_UID = DECORATOR_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__METAMODEL_UID = DECORATOR_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__PORT = DECORATOR_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__CONNECTOR = DECORATOR_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__COLLECTION = DECORATOR_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__BLOCK = DECORATOR_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__RELATION = DECORATOR_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__BASE_CLASS = DECORATOR_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__BASE_ACTION = DECORATOR_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL__TREENODE = DECORATOR_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Retry Until Successful</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL_FEATURE_COUNT = DECORATOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Retry Until Successful</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETRY_UNTIL_SUCCESSFUL_OPERATION_COUNT = DECORATOR_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.RepeatImpl <em>Repeat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.RepeatImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getRepeat()
	 * @generated
	 */
	int REPEAT = 21;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__PROPERTY = DECORATOR_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__INSTANCE_UID = DECORATOR_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__DESCRIPTION = DECORATOR_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__AUTHORSHIP = DECORATOR_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__PROVENANCE = DECORATOR_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__MODEL_UID = DECORATOR_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__METAMODEL_UID = DECORATOR_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__PORT = DECORATOR_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__CONNECTOR = DECORATOR_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__COLLECTION = DECORATOR_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__BLOCK = DECORATOR_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__RELATION = DECORATOR_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__BASE_CLASS = DECORATOR_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__BASE_ACTION = DECORATOR_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT__TREENODE = DECORATOR_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Repeat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_FEATURE_COUNT = DECORATOR_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Repeat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_OPERATION_COUNT = DECORATOR_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParallelSequenceImpl <em>Parallel Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParallelSequenceImpl
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getParallelSequence()
	 * @generated
	 */
	int PARALLEL_SEQUENCE = 22;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__PROPERTY = CONTROL_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__INSTANCE_UID = CONTROL_NODE__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__DESCRIPTION = CONTROL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__AUTHORSHIP = CONTROL_NODE__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__PROVENANCE = CONTROL_NODE__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__MODEL_UID = CONTROL_NODE__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__METAMODEL_UID = CONTROL_NODE__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__PORT = CONTROL_NODE__PORT;

	/**
	 * The feature id for the '<em><b>Connector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__CONNECTOR = CONTROL_NODE__CONNECTOR;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__COLLECTION = CONTROL_NODE__COLLECTION;

	/**
	 * The feature id for the '<em><b>Block</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__BLOCK = CONTROL_NODE__BLOCK;

	/**
	 * The feature id for the '<em><b>Relation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__RELATION = CONTROL_NODE__RELATION;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__BASE_CLASS = CONTROL_NODE__BASE_CLASS;

	/**
	 * The feature id for the '<em><b>Base Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__BASE_ACTION = CONTROL_NODE__BASE_ACTION;

	/**
	 * The feature id for the '<em><b>Treenode</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE__TREENODE = CONTROL_NODE__TREENODE;

	/**
	 * The number of structural features of the '<em>Parallel Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE_FEATURE_COUNT = CONTROL_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Parallel Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_SEQUENCE_OPERATION_COUNT = CONTROL_NODE_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot <em>Tree Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tree Root</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot
	 * @generated
	 */
	EClass getTreeRoot();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getBase_Activity <em>Base Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Activity</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getBase_Activity()
	 * @see #getTreeRoot()
	 * @generated
	 */
	EReference getTreeRoot_Base_Activity();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getTreenode <em>Treenode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Treenode</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot#getTreenode()
	 * @see #getTreeRoot()
	 * @generated
	 */
	EReference getTreeRoot_Treenode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode <em>Tree Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tree Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode
	 * @generated
	 */
	EClass getTreeNode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode#getBase_Action <em>Base Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode#getBase_Action()
	 * @see #getTreeNode()
	 * @generated
	 */
	EReference getTreeNode_Base_Action();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree <em>Sub Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Tree</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.SubTree
	 * @generated
	 */
	EClass getSubTree();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getTreeroot <em>Treeroot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Treeroot</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getTreeroot()
	 * @see #getSubTree()
	 * @generated
	 */
	EReference getSubTree_Treeroot();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getFlowport <em>Flowport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Flowport</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getFlowport()
	 * @see #getSubTree()
	 * @generated
	 */
	EReference getSubTree_Flowport();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getInflowport <em>Inflowport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Inflowport</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getInflowport()
	 * @see #getSubTree()
	 * @generated
	 */
	EReference getSubTree_Inflowport();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getOutflowport <em>Outflowport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outflowport</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.SubTree#getOutflowport()
	 * @see #getSubTree()
	 * @generated
	 */
	EReference getSubTree_Outflowport();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.LeafNode <em>Leaf Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.LeafNode
	 * @generated
	 */
	EClass getLeafNode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getSkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Skill</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Action#getSkill()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Skill();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getFlowport <em>Flowport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Flowport</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Action#getFlowport()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Flowport();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getInflowport <em>Inflowport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Inflowport</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Action#getInflowport()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Inflowport();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getOutflowport <em>Outflowport</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outflowport</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Action#getOutflowport()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Outflowport();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort <em>Data Flow Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Flow Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort
	 * @generated
	 */
	EClass getDataFlowPort();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort <em>In Flow Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Flow Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort
	 * @generated
	 */
	EClass getInFlowPort();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_InputPin <em>Base Input Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Input Pin</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_InputPin()
	 * @see #getInFlowPort()
	 * @generated
	 */
	EReference getInFlowPort_Base_InputPin();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Activity Parameter Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort#getBase_ActivityParameterNode()
	 * @see #getInFlowPort()
	 * @generated
	 */
	EReference getInFlowPort_Base_ActivityParameterNode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort <em>Out Flow Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Flow Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort
	 * @generated
	 */
	EClass getOutFlowPort();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort#getBase_OutputPin <em>Base Output Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Output Pin</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort#getBase_OutputPin()
	 * @see #getOutFlowPort()
	 * @generated
	 */
	EReference getOutFlowPort_Base_OutputPin();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Activity Parameter Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort#getBase_ActivityParameterNode()
	 * @see #getOutFlowPort()
	 * @generated
	 */
	EReference getOutFlowPort_Base_ActivityParameterNode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Fallback <em>Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Fallback
	 * @generated
	 */
	EClass getFallback();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ControlNode <em>Control Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ControlNode
	 * @generated
	 */
	EClass getControlNode();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ControlNode#getTreenode <em>Treenode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Treenode</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ControlNode#getTreenode()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_Treenode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Sequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Sequence
	 * @generated
	 */
	EClass getSequence();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Parameter#getBase_ValueSpecificationAction <em>Base Value Specification Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Value Specification Action</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Parameter#getBase_ValueSpecificationAction()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_Base_ValueSpecificationAction();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode <em>Decorator Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decorator Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode
	 * @generated
	 */
	EClass getDecoratorNode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode#getTreenode <em>Treenode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Treenode</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode#getTreenode()
	 * @see #getDecoratorNode()
	 * @generated
	 */
	EReference getDecoratorNode_Treenode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ControlFlowEdge <em>Control Flow Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Flow Edge</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ControlFlowEdge
	 * @generated
	 */
	EClass getControlFlowEdge();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ControlFlowEdge#getBase_ControlFlow <em>Base Control Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Control Flow</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ControlFlowEdge#getBase_ControlFlow()
	 * @see #getControlFlowEdge()
	 * @generated
	 */
	EReference getControlFlowEdge_Base_ControlFlow();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge <em>Data Flow Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Flow Edge</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge
	 * @generated
	 */
	EClass getDataFlowEdge();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getInPort <em>In Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>In Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getInPort()
	 * @see #getDataFlowEdge()
	 * @generated
	 */
	EReference getDataFlowEdge_InPort();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getOutPort <em>Out Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Out Port</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getOutPort()
	 * @see #getDataFlowEdge()
	 * @generated
	 */
	EReference getDataFlowEdge_OutPort();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBase_ObjectFlow <em>Base Object Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Object Flow</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBase_ObjectFlow()
	 * @see #getDataFlowEdge()
	 * @generated
	 */
	EReference getDataFlowEdge_Base_ObjectFlow();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBbEntry <em>Bb Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bb Entry</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge#getBbEntry()
	 * @see #getDataFlowEdge()
	 * @generated
	 */
	EReference getDataFlowEdge_BbEntry();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry <em>Black Board Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Black Board Entry</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry
	 * @generated
	 */
	EClass getBlackBoardEntry();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getDfEdge <em>Df Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Df Edge</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getDfEdge()
	 * @see #getBlackBoardEntry()
	 * @generated
	 */
	EReference getBlackBoardEntry_DfEdge();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_CentralBufferNode <em>Base Central Buffer Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Central Buffer Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_CentralBufferNode()
	 * @see #getBlackBoardEntry()
	 * @generated
	 */
	EReference getBlackBoardEntry_Base_CentralBufferNode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Activity Parameter Node</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry#getBase_ActivityParameterNode()
	 * @see #getBlackBoardEntry()
	 * @generated
	 */
	EReference getBlackBoardEntry_Base_ActivityParameterNode();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ReactiveFallback <em>Reactive Fallback</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reactive Fallback</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ReactiveFallback
	 * @generated
	 */
	EClass getReactiveFallback();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ReactiveSequence <em>Reactive Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reactive Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ReactiveSequence
	 * @generated
	 */
	EClass getReactiveSequence();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Inverter <em>Inverter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inverter</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Inverter
	 * @generated
	 */
	EClass getInverter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.RetryUntilSuccessful <em>Retry Until Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Retry Until Successful</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.RetryUntilSuccessful
	 * @generated
	 */
	EClass getRetryUntilSuccessful();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.Repeat <em>Repeat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repeat</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.Repeat
	 * @generated
	 */
	EClass getRepeat();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.bt.profile.bt.ParallelSequence <em>Parallel Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parallel Sequence</em>'.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.ParallelSequence
	 * @generated
	 */
	EClass getParallelSequence();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BtFactory getBtFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeRootImpl <em>Tree Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeRootImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getTreeRoot()
		 * @generated
		 */
		EClass TREE_ROOT = eINSTANCE.getTreeRoot();

		/**
		 * The meta object literal for the '<em><b>Base Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_ROOT__BASE_ACTIVITY = eINSTANCE.getTreeRoot_Base_Activity();

		/**
		 * The meta object literal for the '<em><b>Treenode</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_ROOT__TREENODE = eINSTANCE.getTreeRoot_Treenode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeNodeImpl <em>Tree Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.TreeNodeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getTreeNode()
		 * @generated
		 */
		EClass TREE_NODE = eINSTANCE.getTreeNode();

		/**
		 * The meta object literal for the '<em><b>Base Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TREE_NODE__BASE_ACTION = eINSTANCE.getTreeNode_Base_Action();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl <em>Sub Tree</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.SubTreeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getSubTree()
		 * @generated
		 */
		EClass SUB_TREE = eINSTANCE.getSubTree();

		/**
		 * The meta object literal for the '<em><b>Treeroot</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE__TREEROOT = eINSTANCE.getSubTree_Treeroot();

		/**
		 * The meta object literal for the '<em><b>Flowport</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE__FLOWPORT = eINSTANCE.getSubTree_Flowport();

		/**
		 * The meta object literal for the '<em><b>Inflowport</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE__INFLOWPORT = eINSTANCE.getSubTree_Inflowport();

		/**
		 * The meta object literal for the '<em><b>Outflowport</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUB_TREE__OUTFLOWPORT = eINSTANCE.getSubTree_Outflowport();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ConditionImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.LeafNodeImpl <em>Leaf Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.LeafNodeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getLeafNode()
		 * @generated
		 */
		EClass LEAF_NODE = eINSTANCE.getLeafNode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ActionImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Skill</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__SKILL = eINSTANCE.getAction_Skill();

		/**
		 * The meta object literal for the '<em><b>Flowport</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__FLOWPORT = eINSTANCE.getAction_Flowport();

		/**
		 * The meta object literal for the '<em><b>Inflowport</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__INFLOWPORT = eINSTANCE.getAction_Inflowport();

		/**
		 * The meta object literal for the '<em><b>Outflowport</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__OUTFLOWPORT = eINSTANCE.getAction_Outflowport();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowPortImpl <em>Data Flow Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowPortImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getDataFlowPort()
		 * @generated
		 */
		EClass DATA_FLOW_PORT = eINSTANCE.getDataFlowPort();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.InFlowPortImpl <em>In Flow Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.InFlowPortImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getInFlowPort()
		 * @generated
		 */
		EClass IN_FLOW_PORT = eINSTANCE.getInFlowPort();

		/**
		 * The meta object literal for the '<em><b>Base Input Pin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_FLOW_PORT__BASE_INPUT_PIN = eINSTANCE.getInFlowPort_Base_InputPin();

		/**
		 * The meta object literal for the '<em><b>Base Activity Parameter Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE = eINSTANCE.getInFlowPort_Base_ActivityParameterNode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.OutFlowPortImpl <em>Out Flow Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.OutFlowPortImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getOutFlowPort()
		 * @generated
		 */
		EClass OUT_FLOW_PORT = eINSTANCE.getOutFlowPort();

		/**
		 * The meta object literal for the '<em><b>Base Output Pin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_FLOW_PORT__BASE_OUTPUT_PIN = eINSTANCE.getOutFlowPort_Base_OutputPin();

		/**
		 * The meta object literal for the '<em><b>Base Activity Parameter Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE = eINSTANCE.getOutFlowPort_Base_ActivityParameterNode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.FallbackImpl <em>Fallback</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.FallbackImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getFallback()
		 * @generated
		 */
		EClass FALLBACK = eINSTANCE.getFallback();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlNodeImpl <em>Control Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlNodeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getControlNode()
		 * @generated
		 */
		EClass CONTROL_NODE = eINSTANCE.getControlNode();

		/**
		 * The meta object literal for the '<em><b>Treenode</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__TREENODE = eINSTANCE.getControlNode_Treenode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.SequenceImpl <em>Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.SequenceImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getSequence()
		 * @generated
		 */
		EClass SEQUENCE = eINSTANCE.getSequence();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParameterImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Base Value Specification Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__BASE_VALUE_SPECIFICATION_ACTION = eINSTANCE.getParameter_Base_ValueSpecificationAction();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DecoratorNodeImpl <em>Decorator Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.DecoratorNodeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getDecoratorNode()
		 * @generated
		 */
		EClass DECORATOR_NODE = eINSTANCE.getDecoratorNode();

		/**
		 * The meta object literal for the '<em><b>Treenode</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECORATOR_NODE__TREENODE = eINSTANCE.getDecoratorNode_Treenode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlFlowEdgeImpl <em>Control Flow Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ControlFlowEdgeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getControlFlowEdge()
		 * @generated
		 */
		EClass CONTROL_FLOW_EDGE = eINSTANCE.getControlFlowEdge();

		/**
		 * The meta object literal for the '<em><b>Base Control Flow</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_FLOW_EDGE__BASE_CONTROL_FLOW = eINSTANCE.getControlFlowEdge_Base_ControlFlow();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl <em>Data Flow Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.DataFlowEdgeImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getDataFlowEdge()
		 * @generated
		 */
		EClass DATA_FLOW_EDGE = eINSTANCE.getDataFlowEdge();

		/**
		 * The meta object literal for the '<em><b>In Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_EDGE__IN_PORT = eINSTANCE.getDataFlowEdge_InPort();

		/**
		 * The meta object literal for the '<em><b>Out Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_EDGE__OUT_PORT = eINSTANCE.getDataFlowEdge_OutPort();

		/**
		 * The meta object literal for the '<em><b>Base Object Flow</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_EDGE__BASE_OBJECT_FLOW = eINSTANCE.getDataFlowEdge_Base_ObjectFlow();

		/**
		 * The meta object literal for the '<em><b>Bb Entry</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_EDGE__BB_ENTRY = eINSTANCE.getDataFlowEdge_BbEntry();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl <em>Black Board Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getBlackBoardEntry()
		 * @generated
		 */
		EClass BLACK_BOARD_ENTRY = eINSTANCE.getBlackBoardEntry();

		/**
		 * The meta object literal for the '<em><b>Df Edge</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLACK_BOARD_ENTRY__DF_EDGE = eINSTANCE.getBlackBoardEntry_DfEdge();

		/**
		 * The meta object literal for the '<em><b>Base Central Buffer Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE = eINSTANCE.getBlackBoardEntry_Base_CentralBufferNode();

		/**
		 * The meta object literal for the '<em><b>Base Activity Parameter Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE = eINSTANCE.getBlackBoardEntry_Base_ActivityParameterNode();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveFallbackImpl <em>Reactive Fallback</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveFallbackImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getReactiveFallback()
		 * @generated
		 */
		EClass REACTIVE_FALLBACK = eINSTANCE.getReactiveFallback();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveSequenceImpl <em>Reactive Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ReactiveSequenceImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getReactiveSequence()
		 * @generated
		 */
		EClass REACTIVE_SEQUENCE = eINSTANCE.getReactiveSequence();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.InverterImpl <em>Inverter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.InverterImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getInverter()
		 * @generated
		 */
		EClass INVERTER = eINSTANCE.getInverter();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.RetryUntilSuccessfulImpl <em>Retry Until Successful</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.RetryUntilSuccessfulImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getRetryUntilSuccessful()
		 * @generated
		 */
		EClass RETRY_UNTIL_SUCCESSFUL = eINSTANCE.getRetryUntilSuccessful();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.RepeatImpl <em>Repeat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.RepeatImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getRepeat()
		 * @generated
		 */
		EClass REPEAT = eINSTANCE.getRepeat();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParallelSequenceImpl <em>Parallel Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.ParallelSequenceImpl
		 * @see org.eclipse.papyrus.robotics.bt.profile.bt.impl.BtPackageImpl#getParallelSequence()
		 * @generated
		 */
		EClass PARALLEL_SEQUENCE = eINSTANCE.getParallelSequence();

	}

} //BtPackage
