/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Flow Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getDataFlowPort()
 * @model abstract="true"
 * @generated
 */
public interface DataFlowPort extends Port {
} // DataFlowPort
