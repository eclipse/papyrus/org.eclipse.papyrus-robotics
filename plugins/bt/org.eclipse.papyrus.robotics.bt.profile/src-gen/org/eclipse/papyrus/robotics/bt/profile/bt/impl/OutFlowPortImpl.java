/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;

import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.OutputPin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Flow Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.OutFlowPortImpl#getBase_OutputPin <em>Base Output Pin</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.OutFlowPortImpl#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OutFlowPortImpl extends DataFlowPortImpl implements OutFlowPort {
	/**
	 * The cached value of the '{@link #getBase_OutputPin() <em>Base Output Pin</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_OutputPin()
	 * @generated
	 * @ordered
	 */
	protected OutputPin base_OutputPin;

	/**
	 * The cached value of the '{@link #getBase_ActivityParameterNode() <em>Base Activity Parameter Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_ActivityParameterNode()
	 * @generated
	 * @ordered
	 */
	protected ActivityParameterNode base_ActivityParameterNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutFlowPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.OUT_FLOW_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OutputPin getBase_OutputPin() {
		if (base_OutputPin != null && base_OutputPin.eIsProxy()) {
			InternalEObject oldBase_OutputPin = (InternalEObject)base_OutputPin;
			base_OutputPin = (OutputPin)eResolveProxy(oldBase_OutputPin);
			if (base_OutputPin != oldBase_OutputPin) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BtPackage.OUT_FLOW_PORT__BASE_OUTPUT_PIN, oldBase_OutputPin, base_OutputPin));
			}
		}
		return base_OutputPin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputPin basicGetBase_OutputPin() {
		return base_OutputPin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_OutputPin(OutputPin newBase_OutputPin) {
		OutputPin oldBase_OutputPin = base_OutputPin;
		base_OutputPin = newBase_OutputPin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BtPackage.OUT_FLOW_PORT__BASE_OUTPUT_PIN, oldBase_OutputPin, base_OutputPin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityParameterNode getBase_ActivityParameterNode() {
		if (base_ActivityParameterNode != null && base_ActivityParameterNode.eIsProxy()) {
			InternalEObject oldBase_ActivityParameterNode = (InternalEObject)base_ActivityParameterNode;
			base_ActivityParameterNode = (ActivityParameterNode)eResolveProxy(oldBase_ActivityParameterNode);
			if (base_ActivityParameterNode != oldBase_ActivityParameterNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BtPackage.OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE, oldBase_ActivityParameterNode, base_ActivityParameterNode));
			}
		}
		return base_ActivityParameterNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityParameterNode basicGetBase_ActivityParameterNode() {
		return base_ActivityParameterNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_ActivityParameterNode(ActivityParameterNode newBase_ActivityParameterNode) {
		ActivityParameterNode oldBase_ActivityParameterNode = base_ActivityParameterNode;
		base_ActivityParameterNode = newBase_ActivityParameterNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BtPackage.OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE, oldBase_ActivityParameterNode, base_ActivityParameterNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.OUT_FLOW_PORT__BASE_OUTPUT_PIN:
				if (resolve) return getBase_OutputPin();
				return basicGetBase_OutputPin();
			case BtPackage.OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE:
				if (resolve) return getBase_ActivityParameterNode();
				return basicGetBase_ActivityParameterNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.OUT_FLOW_PORT__BASE_OUTPUT_PIN:
				setBase_OutputPin((OutputPin)newValue);
				return;
			case BtPackage.OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE:
				setBase_ActivityParameterNode((ActivityParameterNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.OUT_FLOW_PORT__BASE_OUTPUT_PIN:
				setBase_OutputPin((OutputPin)null);
				return;
			case BtPackage.OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE:
				setBase_ActivityParameterNode((ActivityParameterNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.OUT_FLOW_PORT__BASE_OUTPUT_PIN:
				return base_OutputPin != null;
			case BtPackage.OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE:
				return base_ActivityParameterNode != null;
		}
		return super.eIsSet(featureID);
	}

} //OutFlowPortImpl
