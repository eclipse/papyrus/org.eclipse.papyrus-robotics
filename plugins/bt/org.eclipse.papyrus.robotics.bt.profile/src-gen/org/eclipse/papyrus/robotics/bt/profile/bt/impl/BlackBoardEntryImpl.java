/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.ConnectorImpl;

import org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge;

import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.CentralBufferNode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Black Board Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl#getDfEdge <em>Df Edge</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl#getBase_CentralBufferNode <em>Base Central Buffer Node</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.impl.BlackBoardEntryImpl#getBase_ActivityParameterNode <em>Base Activity Parameter Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlackBoardEntryImpl extends ConnectorImpl implements BlackBoardEntry {
	/**
	 * The cached value of the '{@link #getBase_CentralBufferNode() <em>Base Central Buffer Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_CentralBufferNode()
	 * @generated
	 * @ordered
	 */
	protected CentralBufferNode base_CentralBufferNode;

	/**
	 * The cached value of the '{@link #getBase_ActivityParameterNode() <em>Base Activity Parameter Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_ActivityParameterNode()
	 * @generated
	 * @ordered
	 */
	protected ActivityParameterNode base_ActivityParameterNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlackBoardEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BtPackage.Literals.BLACK_BOARD_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DataFlowEdge> getDfEdge() {
		// TODO: implement this method to return the 'Df Edge' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CentralBufferNode getBase_CentralBufferNode() {
		if (base_CentralBufferNode != null && base_CentralBufferNode.eIsProxy()) {
			InternalEObject oldBase_CentralBufferNode = (InternalEObject)base_CentralBufferNode;
			base_CentralBufferNode = (CentralBufferNode)eResolveProxy(oldBase_CentralBufferNode);
			if (base_CentralBufferNode != oldBase_CentralBufferNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BtPackage.BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE, oldBase_CentralBufferNode, base_CentralBufferNode));
			}
		}
		return base_CentralBufferNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CentralBufferNode basicGetBase_CentralBufferNode() {
		return base_CentralBufferNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_CentralBufferNode(CentralBufferNode newBase_CentralBufferNode) {
		CentralBufferNode oldBase_CentralBufferNode = base_CentralBufferNode;
		base_CentralBufferNode = newBase_CentralBufferNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BtPackage.BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE, oldBase_CentralBufferNode, base_CentralBufferNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityParameterNode getBase_ActivityParameterNode() {
		if (base_ActivityParameterNode != null && base_ActivityParameterNode.eIsProxy()) {
			InternalEObject oldBase_ActivityParameterNode = (InternalEObject)base_ActivityParameterNode;
			base_ActivityParameterNode = (ActivityParameterNode)eResolveProxy(oldBase_ActivityParameterNode);
			if (base_ActivityParameterNode != oldBase_ActivityParameterNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BtPackage.BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE, oldBase_ActivityParameterNode, base_ActivityParameterNode));
			}
		}
		return base_ActivityParameterNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityParameterNode basicGetBase_ActivityParameterNode() {
		return base_ActivityParameterNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_ActivityParameterNode(ActivityParameterNode newBase_ActivityParameterNode) {
		ActivityParameterNode oldBase_ActivityParameterNode = base_ActivityParameterNode;
		base_ActivityParameterNode = newBase_ActivityParameterNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BtPackage.BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE, oldBase_ActivityParameterNode, base_ActivityParameterNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BtPackage.BLACK_BOARD_ENTRY__DF_EDGE:
				return getDfEdge();
			case BtPackage.BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE:
				if (resolve) return getBase_CentralBufferNode();
				return basicGetBase_CentralBufferNode();
			case BtPackage.BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE:
				if (resolve) return getBase_ActivityParameterNode();
				return basicGetBase_ActivityParameterNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BtPackage.BLACK_BOARD_ENTRY__DF_EDGE:
				getDfEdge().clear();
				getDfEdge().addAll((Collection<? extends DataFlowEdge>)newValue);
				return;
			case BtPackage.BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE:
				setBase_CentralBufferNode((CentralBufferNode)newValue);
				return;
			case BtPackage.BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE:
				setBase_ActivityParameterNode((ActivityParameterNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BtPackage.BLACK_BOARD_ENTRY__DF_EDGE:
				getDfEdge().clear();
				return;
			case BtPackage.BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE:
				setBase_CentralBufferNode((CentralBufferNode)null);
				return;
			case BtPackage.BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE:
				setBase_ActivityParameterNode((ActivityParameterNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BtPackage.BLACK_BOARD_ENTRY__DF_EDGE:
				return !getDfEdge().isEmpty();
			case BtPackage.BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE:
				return base_CentralBufferNode != null;
			case BtPackage.BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE:
				return base_ActivityParameterNode != null;
		}
		return super.eIsSet(featureID);
	}

} //BlackBoardEntryImpl
