/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

import org.eclipse.papyrus.robotics.bt.profile.bt.Action;
import org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtFactory;
import org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage;
import org.eclipse.papyrus.robotics.bt.profile.bt.Condition;
import org.eclipse.papyrus.robotics.bt.profile.bt.ControlFlowEdge;
import org.eclipse.papyrus.robotics.bt.profile.bt.ControlNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowEdge;
import org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.Fallback;
import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.Inverter;
import org.eclipse.papyrus.robotics.bt.profile.bt.LeafNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.ParallelSequence;
import org.eclipse.papyrus.robotics.bt.profile.bt.Parameter;
import org.eclipse.papyrus.robotics.bt.profile.bt.ReactiveFallback;
import org.eclipse.papyrus.robotics.bt.profile.bt.ReactiveSequence;
import org.eclipse.papyrus.robotics.bt.profile.bt.Repeat;
import org.eclipse.papyrus.robotics.bt.profile.bt.RetryUntilSuccessful;
import org.eclipse.papyrus.robotics.bt.profile.bt.Sequence;
import org.eclipse.papyrus.robotics.bt.profile.bt.SubTree;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.papyrus.robotics.profile.robotics.roboticsPackage;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillsPackage;
import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BtPackageImpl extends EPackageImpl implements BtPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass treeRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass treeNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subTreeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leafNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFlowPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inFlowPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outFlowPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fallbackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decoratorNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlFlowEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFlowEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blackBoardEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reactiveFallbackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reactiveSequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inverterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass retryUntilSuccessfulEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass repeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parallelSequenceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BtPackageImpl() {
		super(eNS_URI, BtFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link BtPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BtPackage init() {
		if (isInited) return (BtPackage)EPackage.Registry.INSTANCE.getEPackage(BtPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredBtPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		BtPackageImpl theBtPackage = registeredBtPackage instanceof BtPackageImpl ? (BtPackageImpl)registeredBtPackage : new BtPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		BPCPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		roboticsPackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theBtPackage.createPackageContents();

		// Initialize created meta-data
		theBtPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBtPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BtPackage.eNS_URI, theBtPackage);
		return theBtPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTreeRoot() {
		return treeRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTreeRoot_Base_Activity() {
		return (EReference)treeRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTreeRoot_Treenode() {
		return (EReference)treeRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTreeNode() {
		return treeNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTreeNode_Base_Action() {
		return (EReference)treeNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubTree() {
		return subTreeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubTree_Treeroot() {
		return (EReference)subTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubTree_Flowport() {
		return (EReference)subTreeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubTree_Inflowport() {
		return (EReference)subTreeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubTree_Outflowport() {
		return (EReference)subTreeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCondition() {
		return conditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLeafNode() {
		return leafNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_Skill() {
		return (EReference)actionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_Flowport() {
		return (EReference)actionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_Inflowport() {
		return (EReference)actionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAction_Outflowport() {
		return (EReference)actionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDataFlowPort() {
		return dataFlowPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInFlowPort() {
		return inFlowPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInFlowPort_Base_InputPin() {
		return (EReference)inFlowPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInFlowPort_Base_ActivityParameterNode() {
		return (EReference)inFlowPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOutFlowPort() {
		return outFlowPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOutFlowPort_Base_OutputPin() {
		return (EReference)outFlowPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOutFlowPort_Base_ActivityParameterNode() {
		return (EReference)outFlowPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFallback() {
		return fallbackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getControlNode() {
		return controlNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getControlNode_Treenode() {
		return (EReference)controlNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSequence() {
		return sequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParameter_Base_ValueSpecificationAction() {
		return (EReference)parameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDecoratorNode() {
		return decoratorNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDecoratorNode_Treenode() {
		return (EReference)decoratorNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getControlFlowEdge() {
		return controlFlowEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getControlFlowEdge_Base_ControlFlow() {
		return (EReference)controlFlowEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDataFlowEdge() {
		return dataFlowEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataFlowEdge_InPort() {
		return (EReference)dataFlowEdgeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataFlowEdge_OutPort() {
		return (EReference)dataFlowEdgeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataFlowEdge_Base_ObjectFlow() {
		return (EReference)dataFlowEdgeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDataFlowEdge_BbEntry() {
		return (EReference)dataFlowEdgeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBlackBoardEntry() {
		return blackBoardEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlackBoardEntry_DfEdge() {
		return (EReference)blackBoardEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlackBoardEntry_Base_CentralBufferNode() {
		return (EReference)blackBoardEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlackBoardEntry_Base_ActivityParameterNode() {
		return (EReference)blackBoardEntryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReactiveFallback() {
		return reactiveFallbackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReactiveSequence() {
		return reactiveSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInverter() {
		return inverterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRetryUntilSuccessful() {
		return retryUntilSuccessfulEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRepeat() {
		return repeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParallelSequence() {
		return parallelSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BtFactory getBtFactory() {
		return (BtFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		treeRootEClass = createEClass(TREE_ROOT);
		createEReference(treeRootEClass, TREE_ROOT__BASE_ACTIVITY);
		createEReference(treeRootEClass, TREE_ROOT__TREENODE);

		treeNodeEClass = createEClass(TREE_NODE);
		createEReference(treeNodeEClass, TREE_NODE__BASE_ACTION);

		subTreeEClass = createEClass(SUB_TREE);
		createEReference(subTreeEClass, SUB_TREE__TREEROOT);
		createEReference(subTreeEClass, SUB_TREE__FLOWPORT);
		createEReference(subTreeEClass, SUB_TREE__INFLOWPORT);
		createEReference(subTreeEClass, SUB_TREE__OUTFLOWPORT);

		dataFlowPortEClass = createEClass(DATA_FLOW_PORT);

		inFlowPortEClass = createEClass(IN_FLOW_PORT);
		createEReference(inFlowPortEClass, IN_FLOW_PORT__BASE_INPUT_PIN);
		createEReference(inFlowPortEClass, IN_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE);

		outFlowPortEClass = createEClass(OUT_FLOW_PORT);
		createEReference(outFlowPortEClass, OUT_FLOW_PORT__BASE_OUTPUT_PIN);
		createEReference(outFlowPortEClass, OUT_FLOW_PORT__BASE_ACTIVITY_PARAMETER_NODE);

		conditionEClass = createEClass(CONDITION);

		leafNodeEClass = createEClass(LEAF_NODE);

		actionEClass = createEClass(ACTION);
		createEReference(actionEClass, ACTION__SKILL);
		createEReference(actionEClass, ACTION__FLOWPORT);
		createEReference(actionEClass, ACTION__INFLOWPORT);
		createEReference(actionEClass, ACTION__OUTFLOWPORT);

		fallbackEClass = createEClass(FALLBACK);

		controlNodeEClass = createEClass(CONTROL_NODE);
		createEReference(controlNodeEClass, CONTROL_NODE__TREENODE);

		sequenceEClass = createEClass(SEQUENCE);

		parameterEClass = createEClass(PARAMETER);
		createEReference(parameterEClass, PARAMETER__BASE_VALUE_SPECIFICATION_ACTION);

		decoratorNodeEClass = createEClass(DECORATOR_NODE);
		createEReference(decoratorNodeEClass, DECORATOR_NODE__TREENODE);

		controlFlowEdgeEClass = createEClass(CONTROL_FLOW_EDGE);
		createEReference(controlFlowEdgeEClass, CONTROL_FLOW_EDGE__BASE_CONTROL_FLOW);

		dataFlowEdgeEClass = createEClass(DATA_FLOW_EDGE);
		createEReference(dataFlowEdgeEClass, DATA_FLOW_EDGE__IN_PORT);
		createEReference(dataFlowEdgeEClass, DATA_FLOW_EDGE__OUT_PORT);
		createEReference(dataFlowEdgeEClass, DATA_FLOW_EDGE__BASE_OBJECT_FLOW);
		createEReference(dataFlowEdgeEClass, DATA_FLOW_EDGE__BB_ENTRY);

		blackBoardEntryEClass = createEClass(BLACK_BOARD_ENTRY);
		createEReference(blackBoardEntryEClass, BLACK_BOARD_ENTRY__DF_EDGE);
		createEReference(blackBoardEntryEClass, BLACK_BOARD_ENTRY__BASE_CENTRAL_BUFFER_NODE);
		createEReference(blackBoardEntryEClass, BLACK_BOARD_ENTRY__BASE_ACTIVITY_PARAMETER_NODE);

		reactiveFallbackEClass = createEClass(REACTIVE_FALLBACK);

		reactiveSequenceEClass = createEClass(REACTIVE_SEQUENCE);

		inverterEClass = createEClass(INVERTER);

		retryUntilSuccessfulEClass = createEClass(RETRY_UNTIL_SUCCESSFUL);

		repeatEClass = createEClass(REPEAT);

		parallelSequenceEClass = createEClass(PARALLEL_SEQUENCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BPCPackage theBPCPackage = (BPCPackage)EPackage.Registry.INSTANCE.getEPackage(BPCPackage.eNS_URI);
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		SkillsPackage theSkillsPackage = (SkillsPackage)EPackage.Registry.INSTANCE.getEPackage(SkillsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		treeRootEClass.getESuperTypes().add(theBPCPackage.getEntity());
		treeNodeEClass.getESuperTypes().add(theBPCPackage.getBlock());
		subTreeEClass.getESuperTypes().add(this.getTreeNode());
		dataFlowPortEClass.getESuperTypes().add(theBPCPackage.getPort());
		inFlowPortEClass.getESuperTypes().add(this.getDataFlowPort());
		outFlowPortEClass.getESuperTypes().add(this.getDataFlowPort());
		conditionEClass.getESuperTypes().add(this.getLeafNode());
		leafNodeEClass.getESuperTypes().add(this.getTreeNode());
		actionEClass.getESuperTypes().add(this.getLeafNode());
		fallbackEClass.getESuperTypes().add(this.getControlNode());
		controlNodeEClass.getESuperTypes().add(this.getTreeNode());
		sequenceEClass.getESuperTypes().add(this.getControlNode());
		parameterEClass.getESuperTypes().add(theBPCPackage.getBlock());
		decoratorNodeEClass.getESuperTypes().add(this.getTreeNode());
		controlFlowEdgeEClass.getESuperTypes().add(theBPCPackage.getConnects());
		dataFlowEdgeEClass.getESuperTypes().add(theBPCPackage.getConnects());
		blackBoardEntryEClass.getESuperTypes().add(theBPCPackage.getConnector());
		reactiveFallbackEClass.getESuperTypes().add(this.getControlNode());
		reactiveSequenceEClass.getESuperTypes().add(this.getControlNode());
		inverterEClass.getESuperTypes().add(this.getDecoratorNode());
		retryUntilSuccessfulEClass.getESuperTypes().add(this.getDecoratorNode());
		repeatEClass.getESuperTypes().add(this.getDecoratorNode());
		parallelSequenceEClass.getESuperTypes().add(this.getControlNode());

		// Initialize classes, features, and operations; add parameters
		initEClass(treeRootEClass, TreeRoot.class, "TreeRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getTreeRoot_Base_Activity(), theUMLPackage.getActivity(), null, "base_Activity", null, 0, 1, TreeRoot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getTreeRoot_Treenode(), this.getTreeNode(), null, "treenode", null, 0, 1, TreeRoot.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(treeNodeEClass, TreeNode.class, "TreeNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getTreeNode_Base_Action(), theUMLPackage.getAction(), null, "base_Action", null, 0, 1, TreeNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(subTreeEClass, SubTree.class, "SubTree", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSubTree_Treeroot(), this.getTreeRoot(), null, "treeroot", null, 1, 1, SubTree.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSubTree_Flowport(), this.getDataFlowPort(), null, "flowport", null, 0, -1, SubTree.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSubTree_Inflowport(), this.getInFlowPort(), null, "inflowport", null, 0, -1, SubTree.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSubTree_Outflowport(), this.getOutFlowPort(), null, "outflowport", null, 0, -1, SubTree.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(dataFlowPortEClass, DataFlowPort.class, "DataFlowPort", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(inFlowPortEClass, InFlowPort.class, "InFlowPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getInFlowPort_Base_InputPin(), theUMLPackage.getInputPin(), null, "base_InputPin", null, 0, 1, InFlowPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getInFlowPort_Base_ActivityParameterNode(), theUMLPackage.getActivityParameterNode(), null, "base_ActivityParameterNode", null, 0, 1, InFlowPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(outFlowPortEClass, OutFlowPort.class, "OutFlowPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getOutFlowPort_Base_OutputPin(), theUMLPackage.getOutputPin(), null, "base_OutputPin", null, 0, 1, OutFlowPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getOutFlowPort_Base_ActivityParameterNode(), theUMLPackage.getActivityParameterNode(), null, "base_ActivityParameterNode", null, 0, 1, OutFlowPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(conditionEClass, Condition.class, "Condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(leafNodeEClass, LeafNode.class, "LeafNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(actionEClass, Action.class, "Action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAction_Skill(), theSkillsPackage.getSkillDefinition(), null, "skill", null, 1, 1, Action.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAction_Flowport(), this.getDataFlowPort(), null, "flowport", null, 0, -1, Action.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAction_Inflowport(), this.getInFlowPort(), null, "inflowport", null, 0, -1, Action.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getAction_Outflowport(), this.getOutFlowPort(), null, "outflowport", null, 0, -1, Action.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(fallbackEClass, Fallback.class, "Fallback", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(controlNodeEClass, ControlNode.class, "ControlNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getControlNode_Treenode(), this.getTreeNode(), null, "treenode", null, 1, -1, ControlNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(sequenceEClass, Sequence.class, "Sequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getParameter_Base_ValueSpecificationAction(), theUMLPackage.getValueSpecificationAction(), null, "base_ValueSpecificationAction", null, 0, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(decoratorNodeEClass, DecoratorNode.class, "DecoratorNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getDecoratorNode_Treenode(), this.getTreeNode(), null, "treenode", null, 1, 1, DecoratorNode.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(controlFlowEdgeEClass, ControlFlowEdge.class, "ControlFlowEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getControlFlowEdge_Base_ControlFlow(), theUMLPackage.getControlFlow(), null, "base_ControlFlow", null, 0, 1, ControlFlowEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(dataFlowEdgeEClass, DataFlowEdge.class, "DataFlowEdge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getDataFlowEdge_InPort(), this.getInFlowPort(), null, "inPort", null, 0, 1, DataFlowEdge.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getDataFlowEdge_OutPort(), this.getOutFlowPort(), null, "outPort", null, 0, 1, DataFlowEdge.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getDataFlowEdge_Base_ObjectFlow(), theUMLPackage.getObjectFlow(), null, "base_ObjectFlow", null, 0, 1, DataFlowEdge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getDataFlowEdge_BbEntry(), this.getBlackBoardEntry(), null, "bbEntry", null, 0, 1, DataFlowEdge.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(blackBoardEntryEClass, BlackBoardEntry.class, "BlackBoardEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getBlackBoardEntry_DfEdge(), this.getDataFlowEdge(), null, "dfEdge", null, 2, -1, BlackBoardEntry.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getBlackBoardEntry_Base_CentralBufferNode(), theUMLPackage.getCentralBufferNode(), null, "base_CentralBufferNode", null, 0, 1, BlackBoardEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getBlackBoardEntry_Base_ActivityParameterNode(), theUMLPackage.getActivityParameterNode(), null, "base_ActivityParameterNode", null, 0, 1, BlackBoardEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(reactiveFallbackEClass, ReactiveFallback.class, "ReactiveFallback", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(reactiveSequenceEClass, ReactiveSequence.class, "ReactiveSequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(inverterEClass, Inverter.class, "Inverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(retryUntilSuccessfulEClass, RetryUntilSuccessful.class, "RetryUntilSuccessful", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(repeatEClass, Repeat.class, "Repeat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(parallelSequenceEClass, ParallelSequence.class, "ParallelSequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);
	}

} //BtPackageImpl
