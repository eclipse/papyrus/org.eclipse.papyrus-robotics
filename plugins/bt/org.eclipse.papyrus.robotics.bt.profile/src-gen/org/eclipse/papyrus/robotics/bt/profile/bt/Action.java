/**
 * Copyright (c) 2019 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 */
package org.eclipse.papyrus.robotics.bt.profile.bt;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getSkill <em>Skill</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getFlowport <em>Flowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getInflowport <em>Inflowport</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.bt.profile.bt.Action#getOutflowport <em>Outflowport</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends LeafNode {
	/**
	 * Returns the value of the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Skill</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skill</em>' reference.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getAction_Skill()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SkillDefinition getSkill();

	/**
	 * Returns the value of the '<em><b>Flowport</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.DataFlowPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flowport</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flowport</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getAction_Flowport()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<DataFlowPort> getFlowport();

	/**
	 * Returns the value of the '<em><b>Inflowport</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inflowport</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inflowport</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getAction_Inflowport()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<InFlowPort> getInflowport();

	/**
	 * Returns the value of the '<em><b>Outflowport</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outflowport</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outflowport</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.bt.profile.bt.BtPackage#getAction_Outflowport()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<OutFlowPort> getOutflowport();

} // Action
