/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.bt.profile.util;

/**
 * Utility class to get informations on BPC profile resources
 *
 */
public final class BTResource {

	public static final String PROFILES_PATHMAP = "pathmap://RobMoSysBT_PROFILES/"; //$NON-NLS-1$	
	
	public static final String PROFILE_PATH = PROFILES_PATHMAP + "bt.profile.uml"; //$NON-NLS-1$

	public static final String PROFILE_URI = "http://www.eclipse.org/papyrus/robotics/bt/1"; //$NON-NLS-1$
}
