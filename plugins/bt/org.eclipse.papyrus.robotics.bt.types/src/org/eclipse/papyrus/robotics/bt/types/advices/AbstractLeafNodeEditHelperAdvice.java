/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.bt.types.utils.BtNodePinUpdateCommand;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.swt.widgets.Display;
import org.eclipse.papyrus.robotics.bt.types.utils.BtActionPinUpdater;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.UMLPackage;

public abstract class AbstractLeafNodeEditHelperAdvice extends AbstractEditHelperAdvice {

	public abstract EncapsulatedContentProvider createContentProvider(CallOperationAction action);

	public abstract String getLeafNodeSemanticsQualifiedName();

	@Override
	public ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// get element which is being configured
		// (must be a CallOperationAction element, because we're operating on BT leafs) 
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof CallOperationAction)) {
			return super.getAfterConfigureCommand(request);
		}
		CallOperationAction action = (CallOperationAction) newElement;
	
		// Create a suitable content provider
		EncapsulatedContentProvider btWithWS = createContentProvider(action);

		// create a proper menu
		EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(btWithWS, "Choose " + getLeafNodeSemanticsQualifiedName(), false);

		// create and return the command to update the CallOperationAction
		CompositeCommand compositeCommand = new CompositeCommand("Update " + getLeafNodeSemanticsQualifiedName());
		if (popupMenuState.show(Display.getDefault().getActiveShell())) {
			// get the selected operation
			Object menuResult = popupMenuState.getSubResult();
			final Operation operation = menuResult instanceof Operation ? (Operation) menuResult : null;
			if (operation != null) {
				// change name
				ICommand setNameCmd = ElementEditServiceUtils.
						getCommandProvider(action).
						getEditCommand(
								new SetRequest(action, UMLPackage.eINSTANCE.getNamedElement_Name(), "")
								);
				compositeCommand.add(setNameCmd);

				// set it as operation referenced by the CallOperationAction
				ICommand setOperationCmd = ElementEditServiceUtils.
						getCommandProvider(action).
						getEditCommand(
								new SetRequest(action, UMLPackage.eINSTANCE.getCallOperationAction_Operation(), operation)
								);
				compositeCommand.add(setOperationCmd);

				// synch pins with operation ins/outs
				BtNodePinUpdateCommand<CallOperationAction> pinUpdateCommand =
						new BtNodePinUpdateCommand<CallOperationAction>(
								"Update pins of bt " + getLeafNodeSemanticsQualifiedName(),
								new BtActionPinUpdater(),
								action
								);
				compositeCommand.add(pinUpdateCommand);
			}
		}
		else {
			return new CancelCommand(newElement);
		}
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}
}
