/*****************************************************************************
 * Copyright (c) 2019, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581733
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.papyrus.robotics.bt.types.utils.BtNodePinUpdateCommand;
import org.eclipse.papyrus.robotics.bt.types.utils.BtSubTreePinUpdater;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.FilterStereotypes;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.UMLPackage;

public class SubTreeEditHelperAdvice extends AbstractEditHelperAdvice {

	protected class SubTreeFilterProvider extends FilterStereotypes {

		protected CallBehaviorAction subtree;

		public SubTreeFilterProvider(IStaticContentProvider encapsulated, Class<? extends EObject> stereotypeFilter) {
			super(encapsulated, stereotypeFilter);
			this.subtree = null;
		}

		public void setSubTreeElement(CallBehaviorAction st) {
			this.subtree = st;
		}

		public CallBehaviorAction getSubTreeElement() {
			return this.subtree;
		}

		@Override
		public boolean isValid(final Object object) {
			if (object instanceof Element) {
				Element eObject = (Element) object;
				if ( StereotypeUtil.isApplied(eObject, stereotypeFilter) ) {
					return ( eObject != subtree.getActivity() ); 
				}
			}
			return false;
		}
	}

	@Override
	public ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// get element which is being configured
		// (must be a CallBehaviorAction element, because we're operating on SubTrees) 
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof CallBehaviorAction)) {
			return super.getAfterConfigureCommand(request);
		}
		CallBehaviorAction subtree = (CallBehaviorAction) newElement;

		// Create a suitable content provider, but first
		// define a filter for the element which is being configured (subtree)
		EncapsulatedContentProvider btWithWS = new RoboticsContentProvider(subtree,
				new UMLContentProvider(subtree, UMLPackage.eINSTANCE.getCallBehaviorAction_Behavior()),
				TreeRoot.class, FileExtensions.BT_UML);

		// create a proper menu
		EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(btWithWS, "Choose behavior tree", false);

		// create and return the command to update the subtree
		CompositeCommand compositeCommand = new CompositeCommand("Update subtree");
		if (popupMenuState.show(Display.getDefault().getActiveShell())) {
			// get the selected activity (treeroot)
			Object menuResult = popupMenuState.getSubResult();
			final Activity activity = menuResult instanceof Activity ? (Activity) menuResult : null;
			if (activity != null) {
				//change name
				ICommand setNameCmd = ElementEditServiceUtils.
						getCommandProvider(subtree).
						getEditCommand(
								new SetRequest(subtree, UMLPackage.eINSTANCE.getNamedElement_Name(), activity.getName())
								);
				compositeCommand.add(setNameCmd);

				// set it as behavior referenced by the callbehavioraction (subtree)
				ICommand setBehaviorCmd = ElementEditServiceUtils.
						getCommandProvider(subtree).
						getEditCommand(
								new SetRequest(subtree, UMLPackage.eINSTANCE.getCallBehaviorAction_Behavior(), activity)
								);
				compositeCommand.add(setBehaviorCmd);

				// synch pins with behavior ins/outs
				BtNodePinUpdateCommand<CallBehaviorAction> pinUpdateCommand =
						new BtNodePinUpdateCommand<CallBehaviorAction>(
								"Update pins of bt " + activity.getName(),
								new BtSubTreePinUpdater(),
								subtree
								);
				compositeCommand.add(pinUpdateCommand);
			}
		}
		else {
			return new CancelCommand(newElement);
		}
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

}
