/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from Jérémie TATIBOUET, Sébastien REVOL and Nicolas FAUVERGUE)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.bt.types.utils.uml.AbstractCallActionPinUpdater;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.UMLFactory;

public class BtActionPinUpdater extends AbstractCallActionPinUpdater<CallOperationAction> {

	@Override
	public void updatePins(CallOperationAction node) {
		// Update both arguments and results
		super.updatePins(node);
		// apply stereotype
		for (InputPin ip : node.getInputs())
			StereotypeUtil.apply(ip, InFlowPort.class);
		for (OutputPin op : node.getOutputs())
			StereotypeUtil.apply(op, OutFlowPort.class);
		// Ensure a target is *unset* for this operation call
		if (node.getTarget() != null)
			node.setTarget(null);
	}

	@Override
	public List<InputPin> deriveArguments(CallOperationAction node) {
		List<InputPin> derivedInputPins = new ArrayList<InputPin>();
		if (node.getOperation() != null) {
			for (Parameter parameter : node.getOperation().getOwnedParameters()) {
				if (parameter.getDirection() == ParameterDirectionKind.INOUT_LITERAL
						|| parameter.getDirection() == ParameterDirectionKind.IN_LITERAL) {
					InputPin derivedPin = UMLFactory.eINSTANCE.createInputPin();
					derivedInputPins.add(derivedPin);
					derivedPin.setType(parameter.getType());
					derivedPin.setLower(parameter.getLower());
					derivedPin.setUpper(parameter.getUpper());
					if (parameter.getDirection() == ParameterDirectionKind.INOUT_LITERAL) {
						derivedPin.setName("[in] " + parameter.getName());
						/*if (InternationalizationPreferencesUtils.getInternationalizationPreference(parameter) && null != UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter)) {
							UMLLabelInternationalization.getInstance().setLabel(derivedPin, "[in] " + UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter), null);
						}*/
					} else {
						derivedPin.setName(parameter.getName());
						/*if (InternationalizationPreferencesUtils.getInternationalizationPreference(parameter) && null != UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter)) {
							UMLLabelInternationalization.getInstance().setLabel(derivedPin, UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter), null);
						}*/
					}
				}
			}
		}
		return derivedInputPins;
	}

	@Override
	/**
	 * @note Do not add RESULT_LITERAL as output pins
	 */
	public List<OutputPin> deriveResults(CallOperationAction node) {
		List<OutputPin> derivedOutputPins = new ArrayList<OutputPin>();
		if (node.getOperation() != null) {
			for (Parameter parameter : node.getOperation().getOwnedParameters()) {
				if (parameter.getDirection() == ParameterDirectionKind.INOUT_LITERAL
						|| parameter.getDirection() == ParameterDirectionKind.OUT_LITERAL) {
					OutputPin derivedPin = UMLFactory.eINSTANCE.createOutputPin();
					derivedOutputPins.add(derivedPin);
					derivedPin.setType(parameter.getType());
					derivedPin.setLower(parameter.getLower());
					derivedPin.setUpper(parameter.getUpper());
					if (parameter.getDirection() == ParameterDirectionKind.INOUT_LITERAL) {
						derivedPin.setName("[out] " + parameter.getName());
						/*if (InternationalizationPreferencesUtils.getInternationalizationPreference(parameter) && null != UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter)) {
							UMLLabelInternationalization.getInstance().setLabel(derivedPin, "[out] " + UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter), null);
						}*/
					} else {
						derivedPin.setName(parameter.getName());
						/*if (InternationalizationPreferencesUtils.getInternationalizationPreference(parameter) && null != UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter)) {
							UMLLabelInternationalization.getInstance().setLabel(derivedPin, UMLLabelInternationalization.getInstance().getLabelWithoutUML(parameter), null);
						}*/
					}
				}
			}
		}
		return derivedOutputPins;
	}


}
