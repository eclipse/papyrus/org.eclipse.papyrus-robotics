/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.robotics.bt.types.utils.BtNodePinUpdateCommand;
import org.eclipse.papyrus.robotics.bt.types.utils.BtParameterPinUpdater;
import org.eclipse.uml2.uml.ValueSpecificationAction;

public class ParameterValueSpecEditHelperAdvice extends AbstractEditHelperAdvice {

	/**
	 * This method call command to synchronize pin
	 * 
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterEditCommand(org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest)
	 *
	 * @param request
	 * @return
	 */
	@Override
	public ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// test the element which is being configured
		// (must be a ValueSpecificationAction element, because we're operating on Parameters)
		if (!(request.getElementToConfigure() instanceof ValueSpecificationAction)) {
			return super.getAfterConfigureCommand(request);
		}
		ValueSpecificationAction param = (ValueSpecificationAction) request.getElementToConfigure();
		return new BtNodePinUpdateCommand<ValueSpecificationAction>(
								"Update bt param pins",
									new BtParameterPinUpdater(),
										param
				);
	}

}
