/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from Jérémie TATIBOUET, Sébastien REVOL and Nicolas FAUVERGUE) - Bug #581733
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.bt.types.utils.uml.AbstractCallActionPinUpdater;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.OutAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLUtil;

public class BtSubTreePinUpdater extends AbstractCallActionPinUpdater<CallBehaviorAction> {

	@Override
	public void updatePins(CallBehaviorAction node) {
		// Update both arguments and results
		super.updatePins(node);
		// apply stereotype
		for (InputPin ip : node.getInputs())
			StereotypeUtil.apply(ip, InFlowPort.class);
		for (OutputPin op : node.getOutputs())
			StereotypeUtil.apply(op, OutFlowPort.class);
	}

	@Override
	public List<InputPin> deriveArguments(CallBehaviorAction node) {
		List<InputPin> derivedInputPins = new ArrayList<InputPin>();
		if (node.getBehavior() != null) {
			Task tsk = UMLUtil.getStereotypeApplication(node.getBehavior(), Task.class);
			if (tsk != null) {
				for (InAttribute ina : tsk.getIns()) {
					Parameter p = ina.getBase_Parameter();
					InputPin derivedPin = UMLFactory.eINSTANCE.createInputPin();
					derivedInputPins.add(derivedPin);
					derivedPin.setType(p.getType());
					derivedPin.setLower(p.getLower());
					derivedPin.setUpper(p.getUpper());
					derivedPin.setName(p.getName());
				}
			}
		}
		return derivedInputPins;
	}

	@Override
	/**
	 * @note Do not add RESULT_LITERAL as output pins
	 */
	public List<OutputPin> deriveResults(CallBehaviorAction node) {
		List<OutputPin> derivedOutputPins = new ArrayList<OutputPin>();
		if (node.getBehavior() != null) {
			Task tsk = UMLUtil.getStereotypeApplication(node.getBehavior(), Task.class);
			if (tsk != null) {
				for (OutAttribute outa : tsk.getOuts()) {
					Parameter p = outa.getBase_Parameter();
					OutputPin derivedPin = UMLFactory.eINSTANCE.createOutputPin();
					derivedOutputPins.add(derivedPin);
					derivedPin.setType(p.getType());
					derivedPin.setLower(p.getLower());
					derivedPin.setUpper(p.getUpper());
					derivedPin.setName(p.getName());
				}
			}
		}
		return derivedOutputPins;
	}


}
