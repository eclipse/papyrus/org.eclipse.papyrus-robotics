/*****************************************************************************
 * Copyright (c) 2019, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581697
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.robotics.bt.standardlibrary.utils.StandardBTSemanticsUtils;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLPackage;

public abstract class AbstractControlFlowNodeEditHelperAdvice extends AbstractEditHelperAdvice {

	/**
	 * qualified name of the control node semantics within the BT stdlibrary
	 */
	public abstract String getNodeSemanticsQualifiedName();

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      Add behavior semantics.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// get element which is being configured
		// (must be a CallBehaviorAction element, because we're operating on ControlNodes)
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof CallBehaviorAction)) {
			return super.getAfterConfigureCommand(request);
		}
		CallBehaviorAction control = (CallBehaviorAction) newElement;
		PackageUtil.loadPackage(StandardBTSemanticsUtils.getStandardBTSemanticsURI(), control.eResource().getResourceSet());
		NamedElement elem = ElementUtils.getQualifiedElementFromRS(control, this.getNodeSemanticsQualifiedName());
		CompositeCommand compositeCommand = new CompositeCommand("Update control node semantics");
		if (elem instanceof Behavior) {
			Behavior semantics = (Behavior) elem;

			// change name
			ICommand setNameCmd = ElementEditServiceUtils.
					getCommandProvider(control).
					getEditCommand(
							new SetRequest(control, UMLPackage.eINSTANCE.getNamedElement_Name(), "")
							);
			compositeCommand.add(setNameCmd);

			// set it as behavior referenced by the callbehavioraction
			ICommand setOperationCmd = ElementEditServiceUtils.
					getCommandProvider(control).
					getEditCommand(
							new SetRequest(control, UMLPackage.eINSTANCE.getCallBehaviorAction_Behavior(), semantics)
							);
			compositeCommand.add(setOperationCmd);
		}
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}
}
