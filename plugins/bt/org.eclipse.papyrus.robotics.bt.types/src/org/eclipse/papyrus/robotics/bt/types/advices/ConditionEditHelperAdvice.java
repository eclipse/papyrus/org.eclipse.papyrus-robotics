/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.core.provider.FilterStereotypes;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResult;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillResultKind;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ConditionEditHelperAdvice extends AbstractLeafNodeEditHelperAdvice {

	protected class ConditionFilterProvider extends FilterStereotypes {

		// the UML call action
		protected CallOperationAction action;

		public ConditionFilterProvider(CallOperationAction a, IStaticContentProvider encapsulated, Class<? extends EObject> stereotypeFilter) {
			super(encapsulated, stereotypeFilter);
			this.action = a;
		}

		@Override
		public boolean isValid(final Object object) {
			if (object instanceof Operation) {
				Operation op = (Operation) object;
				if ( StereotypeUtil.isApplied(op, stereotypeFilter) ) {
					for(Parameter par : op.getOwnedParameters()) {
						SkillResult res = (SkillResult) UMLUtil.getStereotypeApplication(par, SkillResult.class);
						if (res != null && res.getKind() == SkillResultKind.RUNNING)
							return false;
					}
					return true;
				}
			}
			return false;
		}
	}

	@Override
	public EncapsulatedContentProvider createContentProvider(CallOperationAction action) {
		return (
				new RoboticsContentProvider(
						action,
						new ConditionFilterProvider(
								action,
								new UMLContentProvider(action, UMLPackage.eINSTANCE.getCallOperationAction_Operation()),
								SkillDefinition.class
								),
						FileExtensions.SKILLS_UML
						)
				);
	}

	@Override
	public String getLeafNodeSemanticsQualifiedName() {
		return "condition";
	}
}
