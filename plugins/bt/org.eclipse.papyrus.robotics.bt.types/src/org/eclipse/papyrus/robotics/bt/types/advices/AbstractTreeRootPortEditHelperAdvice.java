/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation (Bug #581733)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.InAttribute;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.OutAttribute;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.ParameterEffectKind;
import org.eclipse.uml2.uml.UMLPackage;

public abstract class AbstractTreeRootPortEditHelperAdvice extends AbstractEditHelperAdvice {

	protected abstract ParameterDirectionKind getPortDirection();

	protected ICommand companionParameterCreateCommand(ActivityParameterNode apn) {
		RecordingCommand cmd = new RecordingCommand(TransactionUtil.getEditingDomain(apn), "Create companion Parameter for ActivityParameterNode command") { //$NON-NLS-1$
			@Override
			protected void doExecute(){
				Activity act = apn.getActivity();
				Parameter param = act.createOwnedParameter(apn.getName(), apn.getType());
				final ParameterDirectionKind ptDirKnd = getPortDirection(); 
				param.setDirection(ptDirKnd);
				if (ptDirKnd == ParameterDirectionKind.IN_LITERAL) {
					// IN_LITERAL
					param.setEffect(ParameterEffectKind.READ_LITERAL);
					StereotypeUtil.apply(param, InAttribute.class);
				}
				else{
					// OUT_LITERAL
					param.setEffect(ParameterEffectKind.CREATE_LITERAL);
					StereotypeUtil.apply(param, OutAttribute.class);
				}
				apn.setParameter(param);
			}
		};
		return EMFtoGMFCommandWrapper.wrap(cmd);
	}

	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		// Get the new ActivityParameterNode
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof ActivityParameterNode)){
			return super.getAfterConfigureCommand(request);
		}

		// prepare the command, etc.
		final ActivityParameterNode apn = (ActivityParameterNode) newElement;
		CompositeCommand compositeCommand = new CompositeCommand("TreeRootPort configuration command"); //$NON-NLS-1$
		ICommand companionParamCreateCmd = companionParameterCreateCommand(apn);
		compositeCommand.add(companionParamCreateCmd);

		// Execute the composite command
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	@Override
	protected ICommand getAfterSetCommand(SetRequest request) {
		EStructuralFeature feat = request.getFeature();
		EObject newElement      = request.getElementToEdit();
		// Do execute the afterSetCommand only if the modified structural feature is "name"
		// (synch names of ActivityParameterNode and its companion Parameter)
		if (!(feat.getName().equals("name") && newElement instanceof ActivityParameterNode)) {
			return super.getAfterSetCommand(request);
		}

		// prepare the command, etc.
		final ActivityParameterNode apn = (ActivityParameterNode) newElement;
		String newName = (String) request.getValue();
		CompositeCommand compositeCommand = new CompositeCommand("ActivityParameterNode/Parameter name synch command");
		Parameter param = apn.getParameter();
		if (param != null) {
			ICommand setNameCmd = ElementEditServiceUtils.
					getCommandProvider(apn).
					getEditCommand(
							new SetRequest(param, UMLPackage.eINSTANCE.getNamedElement_Name(), newName)
							);
			compositeCommand.add(setNameCmd);
		}

		// Execute the composite command
		return compositeCommand.isEmpty() ? super.getAfterSetCommand(request) : compositeCommand;
	}

	@Override
	protected ICommand getAfterDestroyDependentsCommand(DestroyDependentsRequest request) {
		// Get the ActivityParameterNode to be destroyed
		EObject destroyElement = request.getElementToDestroy();
		if (!(destroyElement instanceof ActivityParameterNode)) {
			return super.getAfterDestroyDependentsCommand(request);
		}

		// prepare the command, etc.
		final ActivityParameterNode apn = (ActivityParameterNode) destroyElement;
		CompositeCommand compositeCommand = new CompositeCommand("TreeRootPort destruction command"); //$NON-NLS-1$
		Parameter param = apn.getParameter();
		if (param != null) {
			final IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(apn);
			DestroyElementRequest destroyDepReq = new DestroyElementRequest(param, false);
			ICommand destroyDepCmd = commandProvider.getEditCommand(destroyDepReq);
			compositeCommand.add(destroyDepCmd);
		}

		// Execute the composite command
		return compositeCommand.isEmpty() ? super.getAfterDestroyDependentsCommand(request) : compositeCommand;
	}
}
