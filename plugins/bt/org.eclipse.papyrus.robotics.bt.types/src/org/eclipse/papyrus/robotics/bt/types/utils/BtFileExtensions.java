/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  from Ansgar Radermacher)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.utils;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;

/**
 * Manage file extensions and associated image paths
 */
public class BtFileExtensions {
	// behavior tree definition
	public static final String BT_UML = ".bt.uml"; //$NON-NLS-1$
	public static final String BT_ICON = "platform:/plugin/org.eclipse.papyrus.robotics.bt.architecture/icons/tree-16.png"; //$NON-NLS-1$

	private static Map<String, String> extPath;

	static {
		extPath = new HashMap<String, String>();
		extPath.put(BT_UML, BT_ICON);
	}

	public static String getExtension(URI path) {
		for (String extension : extPath.keySet()) {
			if (path.path().endsWith(extension)) {
				return extension;
			}
		}
		return null;
	}

	public static String getImagePath(URI path) {
		String extension = getExtension(path);
		if (extension != null) {
			return extPath.get(extension);
		}
		return null;
	}
}
