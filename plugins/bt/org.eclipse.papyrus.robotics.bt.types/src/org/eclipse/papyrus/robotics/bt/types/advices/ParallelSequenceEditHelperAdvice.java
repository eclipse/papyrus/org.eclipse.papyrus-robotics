/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation (Bug #581697)
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

public class ParallelSequenceEditHelperAdvice extends AbstractControlFlowNodeEditHelperAdvice {

	/**
	 * URI of the BT stdlibrary
	 */
	public static final String BT_PARALLEL_SEQUENCE_SEMANTICS = "BtSemantics::ParallelSequence"; //$NON-NLS-1$

	/**
	 * qualified name of the control node semantics within the BT stdlibrary
	 */
	@Override
	public String getNodeSemanticsQualifiedName() {
		return ParallelSequenceEditHelperAdvice.BT_PARALLEL_SEQUENCE_SEMANTICS;
	}
}
