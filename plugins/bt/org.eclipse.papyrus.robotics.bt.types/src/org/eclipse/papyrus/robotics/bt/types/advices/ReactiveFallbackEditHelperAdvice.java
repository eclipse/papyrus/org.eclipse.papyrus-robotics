/*****************************************************************************
 * Copyright (c) 2019, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581697
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

public class ReactiveFallbackEditHelperAdvice extends AbstractControlFlowNodeEditHelperAdvice {

	/**
	 * URI of the BT stdlibrary
	 */
	public static final String BT_REACTIVE_FALLBACK_SEMANTICS = "BtSemantics::ReactiveFallback"; //$NON-NLS-1$

	/**
	 * qualified name of the control node semantics within the BT stdlibrary
	 */
	public String getNodeSemanticsQualifiedName() {
		return ReactiveFallbackEditHelperAdvice.BT_REACTIVE_FALLBACK_SEMANTICS;
	}
}
