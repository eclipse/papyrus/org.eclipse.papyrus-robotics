/*****************************************************************************
 * Copyright (c) 2019, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  							from Ansgar Radermacher)
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581697
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.robotics.bt.profile.bt.ControlNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.DecoratorNode;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeNode;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ActivityNode;

/**
 * Edit Helper Advice for a {@link Connector}
 */
public class ControlFlowEdgeEditHelperAdvice extends AbstractEditHelperAdvice {

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateRelationshipRequest) {
				return approveCreateRelationshipRequest((CreateRelationshipRequest) context.getEditCommandRequest());
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Check that the source of the assignment is a technical policy
	 * @param request
	 * @return
	 */
	protected boolean approveCreateRelationshipRequest(CreateRelationshipRequest request) {
		
		ActivityNode source = null;
		ActivityNode target = null;
		if (request.getSource() instanceof ActivityNode)
			source = (ActivityNode) request.getSource();
		if (request.getTarget() instanceof ActivityNode)
			target = (ActivityNode) request.getTarget();
		if (source != null && target != null && target != source)
			// source must be a control flow node, i.e., either a ControlNode or a DecoratorNode
			// target can be any TreeNode (this already excludes Parameters)
			// target must not have incoming edges already
			if ( (StereotypeUtil.isApplied(source, ControlNode.class) || StereotypeUtil.isApplied(source, DecoratorNode.class)) &&
					StereotypeUtil.isApplied(target, TreeNode.class) &&
						target.getIncomings().isEmpty()) {
				// when source is a DecoratorNode, and additional constraint is that source must not have outgoing edges already
				if ( StereotypeUtil.isApplied(source, DecoratorNode.class) &&
						!source.getOutgoings().isEmpty()) {
					return false;
				}
				// finally (source is a ControlNode or DecoratorNode with 0 outgoing edges)...
				return true;
			}
		return false;
	}
}
