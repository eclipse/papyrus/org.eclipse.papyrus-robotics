/*****************************************************************************
 * Copyright (c) 2019, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> (based on similar file
 *  							from Ansgar Radermacher)
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581792
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.types.advices;

import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.papyrus.robotics.bt.profile.bt.BlackBoardEntry;
import org.eclipse.papyrus.robotics.bt.profile.bt.InFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.OutFlowPort;
import org.eclipse.papyrus.robotics.bt.profile.bt.Parameter;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ObjectNode;

/**
 * Edit Helper Advice for a {@link Connector}
 */
public class DataFlowEdgeEditHelperAdvice extends AbstractEditHelperAdvice {

	protected boolean isDataFlowFromStaticParameter(ActivityEdge df){
		if (df.getSource() != null)
			if (df.getSource().getOwner() != null)
				return StereotypeUtil.isApplied(df.getSource().getOwner(), Parameter.class);
		return false;
	}

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateRelationshipRequest) {
				return approveCreateRelationshipRequest((CreateRelationshipRequest) context.getEditCommandRequest());
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Check that the source of the assignment is a technical policy
	 * @param request
	 * @return
	 */
	protected boolean approveCreateRelationshipRequest(CreateRelationshipRequest request) {
		
		boolean approveSource = false;
		boolean approveTarget = false;
		if (request.getSource() instanceof ObjectNode && request.getTarget() instanceof ObjectNode) {
			// cast source & target
			ObjectNode source = (ObjectNode) request.getSource();
			ObjectNode target = (ObjectNode) request.getTarget();

			// approve source
			//   1. source is an OutFlowPort and does not have outgoing edges already, or
			//   2. source is a BlackBoardEntry
			if ((StereotypeUtil.isApplied(source, OutFlowPort.class) && source.getOutgoings().isEmpty()) /*1*/ ||
					(StereotypeUtil.isApplied(source, BlackBoardEntry.class)) /*2*/)
				approveSource = true;

			// approve target (will also depend on source)
			//   1. target is an InFlowPort and does not have incoming edges already, or
			//   2. target is a BlackBoardEntry and the following does _not_ hold:
			//     a. source is a BlackBoardEntry (blackboard entries cannot be chained), or
			//
			//     b. target is (also) a OutFlowPort and source is a Parameter (blackboard entries
			//        that are also tree-root out ports cannot have static parameters as input), or
			//
			//     c. source is a Parameter and target has already an incoming edge
			//        (a blackboard entry can have at most 1 static parameter as input and
			//         a blackboard entry with a static input cannot have dynamic inputs), or
			//
			//     d. source is a Skill and target has an incoming edge from a Parameter
			//        (a blackboard entry with a static input cannot have dynamic inputs)
			if ((StereotypeUtil.isApplied(target, InFlowPort.class) && target.getIncomings().isEmpty()) /*1*/ ||
				   (StereotypeUtil.isApplied(target, BlackBoardEntry.class) &&
					  !(
						(StereotypeUtil.isApplied(source, BlackBoardEntry.class)) /*2a*/ ||
						(StereotypeUtil.isApplied(target, OutFlowPort.class) && StereotypeUtil.isApplied(source.getOwner(), Parameter.class)) /*2b*/ ||
						(StereotypeUtil.isApplied(source.getOwner(), Parameter.class) && !target.getIncomings().isEmpty()) /*2c*/ ||
						(StereotypeUtil.isApplied(source, OutFlowPort.class) && !target.getIncomings().isEmpty() && isDataFlowFromStaticParameter(target.getIncomings().get(0))) /*2d*/
				      )))
				approveTarget = true;
		}
		return (approveSource && approveTarget);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getBeforeConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest)
	 */
	@Override
	protected ICommand getBeforeConfigureCommand(ConfigureRequest request) {
		return super.getBeforeConfigureCommand(request);
	}


	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		return super.getAfterConfigureCommand(request);
	}
}
