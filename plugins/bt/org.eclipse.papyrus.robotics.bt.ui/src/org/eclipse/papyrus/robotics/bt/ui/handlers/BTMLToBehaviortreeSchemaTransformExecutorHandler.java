/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.ui.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.papyrus.robotics.bt.ui.Activator;
import org.eclipse.papyrus.robotics.bt.xsdgw.uml2xml.executors.BTMLToBehaviortreeSchemaTransformExecutor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

public class BTMLToBehaviortreeSchemaTransformExecutorHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection)
		{
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			IRunnableWithProgress operation = new IRunnableWithProgress()
			{
				public void run(IProgressMonitor monitor)
				{
					try {
						for(Object current : structuredSelection.toArray())
							if(current instanceof IAdaptable) {
								EObject eObj = (EObject) ((IAdaptable) current).getAdapter(EObject.class);
								TreeRoot tr = (TreeRoot) UMLUtil.getStereotypeApplication((Element) eObj, TreeRoot.class);
								URI modelURI = URI.createURI(tr.getBase_Activity().getModel().eResource().getURI().trimFileExtension().toString()).appendFileExtension("di");
								IFile model = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(modelURI.toPlatformString(true)));
								monitor.beginTask("TaskXMLExport", 10);
								Thread.sleep(1000); // useful for fast transformations to show that the process
								monitor.worked(6);  // has started and progressed (low impact for longer transformations)
								BTMLToBehaviortreeSchemaTransformExecutor.run(tr.getBase_Activity());
								model.getProject().refreshLocal(IResource.DEPTH_INFINITE, monitor);
								monitor.done(); // safer to mark as done when the resource has also been refreshed
							}
					} catch (CoreException | ExecutionException | InterruptedException e) {
						// turn the result diagnostic into status and send it to error log			
						IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
						Activator.getDefault().getLog().log(status);
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								String errMsg = "Unable to export the P4R BT model to a XML model conforming to BehaviorTree.CPP XSD";
								ErrorDialog.openError(Display.getDefault().getActiveShell(), "Error", errMsg, status);
							}
						});
						return;
					}
				}
			};
			try {
				PlatformUI.getWorkbench().getProgressService().run(true, true, operation);
			} catch (InvocationTargetException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
				Activator.getDefault().getLog().log(status);
			} catch (InterruptedException e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
				Activator.getDefault().getLog().log(status);
			}
		}
		return null;
	}

}
