/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.bt.ui.propertywidgets;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.papyrus.infra.properties.ui.modelelement.ModelElement;
import org.eclipse.papyrus.uml.properties.xtext.widget.property.AbstractXtextPropertyEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;

/**
 * A Property editor for (BT) parameter values
 * 
 * @see AbstractXtextPropertyEditor
 */
public class ParameterValueEditor extends AbstractXtextPropertyEditor {

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            The composite in which the widget is created
	 * @param style
	 *            The style of the editor
	 */
	public ParameterValueEditor(Composite parent, int style) {
		super("PVAL editor", parent, style);	//$NON-NLS-1$
		// find size of owning scrolled composite and set height accordingly
		while (parent != null) {
			if (parent instanceof ScrolledComposite) {
				int height = parent.getBounds().height - 4;	// remove 4 pixels (for border, etc.)
				GridDataFactory.fillDefaults().grab(true, true).hint(SWT.DEFAULT, height).applyTo(xtextEditor.getTextControl());
				break;
			}
			parent = parent.getParent();
		}
	}

	@SuppressWarnings("nls")
	@Override
	protected void registerChangeListeners(ModelElement element) {
		registerObservable(element, "name");
		registerObservable(element, "ownedAttribute");
	}
}
