/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation for #12/Simplify the process to execute a system task
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.ui.handlers;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.papyrus.robotics.ros2.base.ProcessUtils;
import org.eclipse.papyrus.robotics.ros2.base.Ros2Constants;
import org.eclipse.papyrus.robotics.ros2.base.Ros2ProcessBuilder;
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SequencerUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Class;

public class ExecuteBtHandler extends CmdHandler {

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		updateSelectedEObject();
		if (selectedEObject instanceof Class) {
			final Class system = (Class) selectedEObject;

			Job job = new Job("Execute Bt") {

				@Override
				public IStatus run(IProgressMonitor monitor) {
					// execute the task ...
					Ros2ProcessBuilder pb = new Ros2ProcessBuilder(
												Ros2Constants.ACTION,
												Ros2Constants.SEND_GOAL,
												SequencerUtils.getSequencerExecuteBtActionName(system),
												SequencerUtils.EXECUTE_BT_ACTION_INTERFACE,
												SequencerUtils.EXECUTE_BT_ACTION_GOAL
											);
					try {
						Process p = pb.start();
						ProcessUtils.logErrors(p);  // result of error?
					} catch (IOException e) {
						e.printStackTrace();
					}
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
		return null;
	}
}
