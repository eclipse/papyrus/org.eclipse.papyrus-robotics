/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation for #12/Simplify the process to execute a system task
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.ui.propertytesters;

import java.util.Iterator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SequencerUtils;
import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState;
import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState.EState;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ExecuteBtTester extends PropertyTester {

	public static final String isSystemReadyToExecuteBt = "isSystemReadyToExecuteBt";

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (isSystemReadyToExecuteBt.equals(property))
            return isStructuredClassifier((IStructuredSelection) receiver);
        return false;
	}

	protected boolean isStructuredClassifier(IStructuredSelection structuredSelection){
        if(structuredSelection != null){
            @SuppressWarnings("rawtypes")
            Iterator iterator = structuredSelection.iterator();

            while(iterator.hasNext()) {
                Object selection = iterator.next();
                if(!(selection instanceof IAdaptable)) {
                    return false;
                }
                EObject object = (EObject) ((IAdaptable) selection).getAdapter(EObject.class);
                if (object instanceof Class) {
                	System scElem = UMLUtil.getStereotypeApplication((Class)object, System.class);
                	if (scElem != null) {
                		// selected item is a System
                		final String systemName = SequencerUtils.getSequencerName((Class)object);
                		if (LifecycleState.getState(systemName) == EState.ACTIVE) {
                			// System node is active => ready to execute the assigned Bt
                			return true;
                		}
                	}
                }
            }
        }
        return false;
    }

}
