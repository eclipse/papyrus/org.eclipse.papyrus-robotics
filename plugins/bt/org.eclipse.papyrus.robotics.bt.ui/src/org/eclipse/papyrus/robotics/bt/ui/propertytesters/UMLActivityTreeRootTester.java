/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.bt.ui.propertytesters;

import java.util.Iterator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.util.UMLUtil;

public class UMLActivityTreeRootTester extends PropertyTester {

	public static final String isTreeRoot = "isTreeRoot";

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (isTreeRoot.equals(property))
            return isStructuredClassifier((IStructuredSelection) receiver);
        return false;
	}

	protected boolean isStructuredClassifier(IStructuredSelection structuredSelection){
        if(structuredSelection != null){
            @SuppressWarnings("rawtypes")
            Iterator iterator = structuredSelection.iterator();

            while(iterator.hasNext()) {
                Object selection = iterator.next();
                if(!(selection instanceof IAdaptable)) {
                    return false;
                }
                EObject object = (EObject) ((IAdaptable) selection).getAdapter(EObject.class);
                if(!(object instanceof Activity)) return false;

                TreeRoot scElem = UMLUtil.getStereotypeApplication((Activity)object, TreeRoot.class);
                return scElem != null;
            }
        }
        return false;
    }

}
