/*******************************************************************************
 * Copyright (c) 2006 - 2012 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils;


public class Ros2PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences prefs = DefaultScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		prefs.put(Ros2PreferenceConstants.P_ROS_DISTRO, Ros2PreferenceConstants.P_DEFAULT_ROS_DISTRO);
		prefs.put(Ros2PreferenceConstants.P_SETUP_PATH, Ros2PreferenceConstants.P_DEFAULT_SETUP_PATH);
		prefs.put(Ros2PreferenceConstants.P_COLCON_OPTIONS, Ros2PreferenceConstants.P_DEFAULT_COLCON_OPTIONS);
		prefs.put(Ros2PreferenceConstants.P_COLCON_PACKAGES, Ros2PreferenceConstants.P_DEFAULT_COLCON_PACKAGES);
		prefs.put(Ros2PreferenceConstants.P_MAINTAINER_NAME, Ros2PreferenceConstants.P_DEFAULT_MAINTAINER_NAME);
		prefs.put(Ros2PreferenceConstants.P_MAINTAINER_MAIL, Ros2PreferenceConstants.P_DEFAULT_MAINTAINER_MAIL);
		prefs.put(Ros2PreferenceConstants.P_AUTHOR_NAME, Ros2PreferenceConstants.P_DEFAULT_AUTHOR_NAME);
		prefs.put(Ros2PreferenceConstants.P_AUTHOR_MAIL, Ros2PreferenceConstants.P_DEFAULT_AUTHOR_MAIL);
		
		/**
		 * if the user changes the ROS2 setup path, re-run setup jobs.
		 */
		IPreferenceChangeListener changeListener = new IPreferenceChangeListener() {
			
			@Override
			public void preferenceChange(PreferenceChangeEvent changeEvent) {
				// update ROS 2 environment variables
				if (changeEvent.getKey() == Ros2PreferenceConstants.P_SETUP_PATH) {
					EnvironmentUtils.waitForSetupJob();
					EnvironmentUtils.runCheckAndApplySetupJob(Ros2PreferenceUtils.getSetupPath());
				}				
			}
		};
		prefs.addPreferenceChangeListener(changeListener);
	}
}
