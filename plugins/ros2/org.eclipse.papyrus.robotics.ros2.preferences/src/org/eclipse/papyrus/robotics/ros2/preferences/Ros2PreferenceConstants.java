/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.preferences;

import org.eclipse.papyrus.robotics.ros2.preferences.Ros2Distributions.RDLiteral;

/**
 * ROS2 preference constants (key + default value)
 */
public class Ros2PreferenceConstants {

	/**
	 * used ROS 2 distribution
	 */
	public static final String P_ROS_DISTRO = "rosDistro"; //$NON-NLS-1$
	public static final String P_DEFAULT_ROS_DISTRO = Ros2Distributions.getDistroName(RDLiteral.JAZZY);

	/**
	 * list of paths in which to charge setup.bash files
	 */
	public static final String P_SETUP_PATH = "rosSetupPath"; //$NON-NLS-1$
	public static final String P_DEFAULT_SETUP_PATH = "/opt/ros/" + P_DEFAULT_ROS_DISTRO; //$NON-NLS-1$

	/**
	 * colcon build options, e.g. --symlink-install
	 */
	public static final String P_COLCON_OPTIONS = "colcon-options"; //$NON-NLS-1$
	public static final String P_DEFAULT_COLCON_OPTIONS = "--symlink-install"; //$NON-NLS-1$

	/**
	 * colcon option to build a particular package
	 */
	public static final String P_COLCON_PACKAGES = "colcon-packages"; //$NON-NLS-1$
	public static final String P_DEFAULT_COLCON_PACKAGES = "--packages-up-to"; //$NON-NLS-1$

	/**
	 * maintainer name and e-mail
	 */
	public static final String P_MAINTAINER_NAME = "maintainer-name"; //$NON-NLS-1$
	public static final String P_DEFAULT_MAINTAINER_NAME = "maintainer"; //$NON-NLS-1$

	public static final String P_MAINTAINER_MAIL = "maintainer-mail"; //$NON-NLS-1$
	public static final String P_DEFAULT_MAINTAINER_MAIL = "maintainer@somewhere.net"; //$NON-NLS-1$

	/**
	 * author name and e-mail
	 */
	public static final String P_AUTHOR_NAME = "author-name"; //$NON-NLS-1$
	public static final String P_DEFAULT_AUTHOR_NAME = "author"; //$NON-NLS-1$
	
	public static final String P_AUTHOR_MAIL = "author-mail"; //$NON-NLS-1$
	public static final String P_DEFAULT_AUTHOR_MAIL = "author@somewhere.net"; //$NON-NLS-1$
}
