/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.preferences;

/**
 * Utility functions around ROS 2 version, including a definition of different
 * names and literals
 */
public class Ros2Distributions {

	/**
	 * Enumeration with distributions
	 */
	public enum RDLiteral {
		UNKNOWN, ELOQUENT, FOXY, GALACTIC, HUMBLE, IRON, JAZZY
	}

	/**
	 * Return the ROS distribution as enumeration literal
	 * 
	 * @return the configured ROS distribution or UNKNOWN
	 */
	public static RDLiteral getRosDistro() {
		String rosDistro = Ros2PreferenceUtils.getRosDistroStr();
		for (RDLiteral lit : RDLiteral.values()) {
			if (getDistroName(lit).equals(rosDistro)) {
				return lit;
			}
		}
		return RDLiteral.UNKNOWN;
	}

	/**
	 * @param literal the distribution enumeration
	 * @return the name of the distribution (derived as lower case from literal)
	 */
	public static String getDistroName(RDLiteral literal) {
		return literal.toString().toLowerCase();
	}

	/**
	 * Check, if first passed distribution is newer (or the same) as the second one.
	 * 
	 * @param distroA A ROS 2 distribution
	 * @param distroB A ROS 2 distribution
	 * @return if first distribution is newer or the same as the second distribution
	 */
	public static boolean since(RDLiteral distroA, RDLiteral distroB) {
		return distroA.ordinal() >= distroB.ordinal();
	}
}
