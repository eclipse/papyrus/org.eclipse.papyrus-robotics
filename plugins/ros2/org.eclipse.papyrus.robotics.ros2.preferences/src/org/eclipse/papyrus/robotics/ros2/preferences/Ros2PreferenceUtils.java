/*******************************************************************************
 * Copyright (c) 2006 - 2012, 2024 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *     Bug #3 (Bad preferences)
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.preferences;

import org.eclipse.papyrus.designer.infra.base.ScopedPreferences;

/**
 * Utility class that returns the preference values (eventually taking
 * stereotypes into account)
 */
public class Ros2PreferenceUtils {

	protected static ScopedPreferences prefs = null;

	public static String getRosDistroStr() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_ROS_DISTRO, Ros2PreferenceConstants.P_DEFAULT_ROS_DISTRO);
	}

	public static String getSetupPath() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_SETUP_PATH, Ros2PreferenceConstants.P_DEFAULT_SETUP_PATH);
	}

	public static String getColconOptions() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_COLCON_OPTIONS, Ros2PreferenceConstants.P_DEFAULT_COLCON_OPTIONS);
	}

	public static String getColconPackageOptions() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_COLCON_PACKAGES, Ros2PreferenceConstants.P_DEFAULT_COLCON_PACKAGES);
	}

	public static String getMaintainerName() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_MAINTAINER_NAME, Ros2PreferenceConstants.P_DEFAULT_MAINTAINER_NAME);
	}

	public static String getMaintainerMail() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_MAINTAINER_MAIL, Ros2PreferenceConstants.P_DEFAULT_MAINTAINER_MAIL);
	}

	public static String getAuthorName() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_AUTHOR_NAME, Ros2PreferenceConstants.P_DEFAULT_AUTHOR_NAME);
	}

	public static String getAuthorMail() {
		initprefs();
		return prefs.get(Ros2PreferenceConstants.P_AUTHOR_MAIL, Ros2PreferenceConstants.P_DEFAULT_AUTHOR_MAIL);
	}

	public static void initprefs() {
		if (prefs == null) {
			prefs = new ScopedPreferences(Activator.PLUGIN_ID);
		}
	}
}
