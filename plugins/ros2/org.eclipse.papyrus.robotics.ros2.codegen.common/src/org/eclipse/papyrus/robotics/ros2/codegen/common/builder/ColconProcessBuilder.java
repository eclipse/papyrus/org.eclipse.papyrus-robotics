package org.eclipse.papyrus.robotics.ros2.codegen.common.builder;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils;
import org.eclipse.papyrus.robotics.ros2.preferences.Ros2PreferenceUtils;

public class ColconProcessBuilder {

	private static final String BUILD = "build"; //$NON-NLS-1$

	private static final String COLCON = "colcon"; //$NON-NLS-1$

	ProcessBuilder pb;

	public ColconProcessBuilder(String pkgName) {
		pb = new ProcessBuilder();
		String colcon = EnvironmentUtils.getFromPath(COLCON);
		if (colcon == null) {
			// don't throw an exception, otherwise the tests (running on a machine
			// without ROS 2 installed) are failing. Fall back to ros2, even if the
			// command cannot be executed in that case.
			colcon = COLCON;
		}
		pb.command().add(colcon);
		pb.command().add(BUILD);
		pb.command().add(Ros2PreferenceUtils.getColconOptions());
		pb.command().add(Ros2PreferenceUtils.getColconPackageOptions());
		pb.command().add(pkgName);
		File wsLocation = new File(ResourcesPlugin.getWorkspace().getRoot().getLocation().toString());
		pb.directory(wsLocation);

		Map<String, String> localEnv = EnvironmentUtils.getenv();
		Map<String, String> pbEnv = pb.environment();
		for (String key : localEnv.keySet()) {
			pbEnv.put(key, localEnv.get(key));
		}
	}

	public Process start() throws IOException {
		return pb.start();
	}

	public ProcessBuilder getProcessBuilder() {
		return pb;
	}
}
