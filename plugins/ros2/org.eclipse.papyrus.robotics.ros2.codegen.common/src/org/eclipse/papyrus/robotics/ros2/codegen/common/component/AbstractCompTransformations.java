/**
 * Copyright (c) 2022 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 */

package org.eclipse.papyrus.robotics.ros2.codegen.common.component;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;
import org.eclipse.papyrus.robotics.ros2.codegen.common.message.CreateMsgPackage;
import org.eclipse.uml2.uml.Class;

public abstract class AbstractCompTransformations {

	protected IPFileSystemAccess fileAccess;

	protected IProject genProject;

	public AbstractCompTransformations(final IPFileSystemAccess fileAccess, final IProject genProject) {
		this.fileAccess = fileAccess;
		this.genProject = genProject;
	}

	/**
	 * execute transformations for the passed component
	 * 
	 * @param component
	 *            a component definition
	 * @param msgPkgCreator
	 *            an instance of the CreateMsgPackage class
	 */
	public abstract void componentTrafo(final Class component, final CreateMsgPackage msgPkgCreator);

	/**
	 * generate code for the passed component (that must already have been transformed)
	 * 
	 * @param component
	 *            a component definition (after transformation)
	 * @param msgPkgCreator
	 *            an instance of the CreateMsgPackage class
	 */
	public abstract void componentCodegen(final Class component, final CreateMsgPackage msgPkgCreator);

}
