/*******************************************************************************
 * Copyright (c) 2020 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.utils;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.robotics.ros2.codegen.common.RoboticsTContext;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class TransformationUtils {

	public static final String ROS2_TRANSFORMATION_CHAIN = "ros2Library::m2mtransformations::ROSChain"; //$NON-NLS-1$

	/**
	 * Execute the ROS 2 transformation chain on a given root package
	 * @param pkg a root package (of a model)
	 * @param project the current project
	 * @param monitor a progress monitor
	 */
	public static void executeRosTranformation(RoboticsTContext context, Package pkg, IProject project, IProgressMonitor monitor) {
		M2MTrafoChain transformation = TransformationUtils.getChain(pkg);

		if (transformation != null) {
			// execute the task ...
			new ExecuteTransformationChain(pkg, project).executeTransformation(transformation, monitor, 0);
		}
	}

	/**
	 * Obtain the reference to the ROS 2 transformation chain
	 * 
	 * @param element
	 *            an arbitrary element (used to obtain resource set and editing domain)
	 * @return the ROS 2 transformation chain
	 */
	public static M2MTrafoChain getChain(Element element) {
		NamedElement rosChainCl = ElementUtils.getQualifiedElementFromRS(element, RosHelpers.ROS_LIBRARY_URI, ROS2_TRANSFORMATION_CHAIN);
		if (rosChainCl != null) {
			return UMLUtil.getStereotypeApplication(rosChainCl, M2MTrafoChain.class);
		}
		return null;
	}
}
