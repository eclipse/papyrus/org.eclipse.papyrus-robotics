/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/
 
 package org.eclipse.papyrus.robotics.ros2.codegen.common.component

import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.Class
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException

class CallbackGroups {
 	/**
 	 * name of a callback group for a port (which is connected to an activity)
 	 */
	def static callbackGroupName(Port port) {
		val activity = port.class_.getActivity(port)
		if (activity === null) {
			throw new TransformationException(String.format("no activity is associated with port '%s' => cannot determine callback group", port.name))
		}
 		return activity.callbackGroupName
 	}
 	
 	/**
 	 * Name of a callback group of a class representing an activity
 	 */
 	def static String callbackGroupName(Class activityCl) 
 		'''cbg_«activityCl.name»_'''
  
 	/**
 	 * name of a callback group for a timer of an activity
 	 */
 	def static String tCallbackGroupName(Class activityCl)
 		'''cbg_t_«activityCl.name»_'''
}