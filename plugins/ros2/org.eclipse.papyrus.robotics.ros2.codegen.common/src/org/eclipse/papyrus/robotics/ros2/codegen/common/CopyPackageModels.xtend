/*****************************************************************************
 * Copyright (c) 2018, 2021 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common

import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.*
import org.eclipse.papyrus.designer.transformation.base.utils.ProjectManagement

/**
 * If code generation is launched from a component within a package, the
 * resource set will likely not have the complete set of models defined in
 * that package as these are needed to generate the complete set.
 * The goal of this function is to trigger loading these models.
 * TODO: move-to common location, check whether we can provide a better solution.
 */
class CopyPackageModels implements IM2MTrafoCDP {

	override applyTrafo(M2MTrafo trafo, Package rootPkg) throws TransformationException {
		val project = ProjectManagement.getNamedProject(TransformationContext.initialSourceRoot.projectName)
		if (project !== null) {
			project.compDefs
			project.system
		}
		else {
			Activator.log.debug(
				"can not determine project (is generator called from a model within a registered plugin")
		}
	}
}
