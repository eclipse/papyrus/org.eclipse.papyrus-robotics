/*****************************************************************************
 * Copyright (c) 2020, 2023 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #566899
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581690
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.utils

import java.util.ArrayList
import java.util.List
import java.util.Map
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic

import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*
import org.eclipse.uml2.uml.PrimitiveType

class SkillRealizBuildUtils {

	/**
	 * Calculate the service definition dependencies for skills
	 */
	static def List<String> calcDependencies(Map<SkillDefinition, SkillSemantic> skdefToSemanticsMap) {
		val dependencies = new ArrayList<String>
		for (definition : skdefToSemanticsMap.keySet) {
			for (param : definition.ins + definition.outs) {
				val paramType = param.type
				if (!(paramType instanceof PrimitiveType)) {
					val pkgName = paramType.messagePackage.name.toLowerCase
					if (!dependencies.contains(pkgName)) {
						dependencies.add(pkgName)
					}	
				}
			}
		}
		for (semantics : skdefToSemanticsMap.values) {
			for (ops : semantics.operational) {
				if (ops.doesConfigAndCoordOfComponents) {
					val pkgName = SkillUtils.getCompInterface(ops).messagePackage.name.toLowerCase
					if (!dependencies.contains(pkgName)) {
						dependencies.add(pkgName)
					}
				}
			}
		}
		return dependencies
	}

}
