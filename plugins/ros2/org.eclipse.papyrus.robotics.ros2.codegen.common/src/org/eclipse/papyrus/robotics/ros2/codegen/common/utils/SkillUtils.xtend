/*****************************************************************************
 * Copyright (c) 2020, 2023, 2024 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #566899
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581690
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - #12/Simplify the process to execute a system task
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.utils

import java.util.LinkedHashSet
import org.eclipse.papyrus.robotics.profile.robotics.components.System
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillOperationalState
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillParameter
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.robotics.core.utils.InteractionUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.RosHelpers.escapeCamlCase
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*
import org.eclipse.uml2.uml.Namespace
import org.eclipse.uml2.uml.PrimitiveType
import org.eclipse.core.runtime.Path
import java.util.Arrays

class SkillUtils {

	/**
	 * Determine the file name of C++ code that realizes the skill definition by means of its default semantics
	 */
	def static realizationPackageName(Package pkg) {
		if (pkg=== null)
			throw new Exception("Model error! Can''t access the skill realization model!")
		// otherwise...
		return pkg.pkgName + "_skillrealizations"
	}

	/**
	 * Return the unique skills that the system needs to execute
	 */
	def static LinkedHashSet<SkillDefinition> getUniqueSkills(System sys) {
		var res = new LinkedHashSet<SkillDefinition>
		if (sys !== null)
			for (task : sys.task)
				res.addAll(task.skills)
		return res;
	}

	/**
	 * Get the skill definition name from the base UML::Operation
	 */
	def static getName(SkillDefinition sdef) {
		if (sdef !== null)
			return sdef.base_Operation.name
		return ""
	}

	/**
	 * Return the type of a skill definition parameter as a UML::DataType
	 */
	def static getType(SkillParameter param) {
		if (param !== null)
			if (param.base_Parameter.type instanceof DataType)
				return param.base_Parameter.type as DataType
		return null
	}

	/**
	 * Return the list of unique skill parameter types for a given skill definition
	 */
	def static LinkedHashSet<DataType> getUniqueSkillParameterTypes(SkillDefinition skill) {
		var res = new LinkedHashSet<DataType>
		if (skill !== null)
			for (p : skill.ins + skill.outs) {
				val paramType = p.type
				if (!(paramType instanceof PrimitiveType)) {
					res.add(paramType)	
				}
			}
		return res;
	}

	/**
	 * Return the ROS 2 header file name from a data type (communication object).
	 */
	def static getROS2TypeIncludePath(DataType tp) {
		if (tp !== null)
			return tp.ROS2qMsgName.escapeCamlCase + ".hpp"
		return ""
	}

	/**
	 * Return the ROS2 header file name from a data type (communication object).
	 */
	def static getROS2TypeFromMsgName(DataType tp) {
		if (tp !== null)
			return tp.ROS2qMsgName.replace("/", Namespace.SEPARATOR)
		return ""
	}

	/**
	 * Return the ROS 2 package that contains the .behaviortreeschema model of first item in the System's task list
	 * the path to the concrete .behaviortreeschema model in the package
	 */
	def static getFilePathCoordinatesOfDefaultTask(System sys) {
		var res = ""
		if (sys !== null) {
			if (!sys.task.nullOrEmpty) {
				val default_task = sys.task.get(0)
				val uri = default_task.base_Class.eResource().getURI()
				// ROS 2 package
				res = uri.segment(1)
			}
		}
		return res;
	}

	/**
	 * Return the path to the folder that contains the concrete .behaviortreeschema model of first item in the System's task list
	 *
	 * The path is either the full path relative to its ROS 2 package root, or just the name of the folder that contains the .behaviortreeschema model
	 */
	def static getPackageRelativePathOfDefaultTask(System sys, boolean fullpath) {
		var res = ""
		if (sys !== null) {
			if (!sys.task.nullOrEmpty) {
				val default_task = sys.task.get(0)
				val uri = default_task.base_Class.eResource().getURI()
				if (!fullpath) {
					// just the name of the folder that contains the .behaviortreeschema model
					res = uri.segment(uri.segmentCount-2) // second to last, as index range is [0..segmentCount-1]
				} else {
					// Full path relative to the root folder (ROS 2 package)
					var path = new Path("")
					val pertinentSegments = Arrays.copyOfRange(uri.segments, 2, uri.segmentCount-1)
					for (s : pertinentSegments) {
						path = path.append(s) as Path
					}
					res = path.toOSString
				}
			}
		}
		return res;
	}

	/**
	 * Return the filename of .behaviortreeschema model
	 */
	def static getBTFileNameOfDefaultTask(System sys) {
		var res = ""
		if (sys !== null) {
			if (!sys.task.nullOrEmpty) {
				val default_task = sys.task.get(0)
				// Filename of .behaviortreeschema model
				var modelname = default_task.base_Class.name
				var path = new Path(modelname).addFileExtension("behaviortreeschema") as Path
				res = path.toOSString
			}
		}
		return res;
	}

	/**
	 * Return the name of a skill definition parameter
	 */
	def static getName(SkillParameter param) {
		if (param !== null) {
			return param.base_Parameter.name
		}
		return null
	}

	/**
	 * Return the first operational state of skill FSM
	 */
	def static getFirstOpState(SkillSemantic sem) {
		if (sem !== null)
			return sem.operational.get(0)
		return null
	}

	/**
	 * Check whether a skill operational state will access underlying SW components
	 * by means of a configuration and coordination interface
	 */
	def static doesConfigAndCoordOfComponents(SkillOperationalState ops) {
		if (ops !== null)
			if (ops.compInterface !== null)
				return true;
		return false;
	}

	/**
	 * Return the qualified name of coordination and configuration interface used by a skill operational state
	 */
	def static getCoordinationIfQn(SkillOperationalState ops) {
		if (ops.doesConfigAndCoordOfComponents) {
			val qName = ops.compInterface.base_Interface.ROS2qMsgName
			return qName.replace("/", "::")
		}
		return ""
	}

	/**
	 * Return the qualified name of coordination and configuration interface used by a skill operational state
	 */
	def static getCoordinationIfIncludePath(SkillOperationalState ops) {
		if (ops.doesConfigAndCoordOfComponents) {
			val qName = ops.compInterface.base_Interface.ROS2qMsgName
			return qName.escapeCamlCase + ".hpp"
		}
		return ""
	}

	/**
	 * Return the qualified name of coordination and configuration interface used by a skill operational state
	 */
	def static getCompInterface(SkillOperationalState ops) {
		if (ops.doesConfigAndCoordOfComponents)
			return ops.compInterface.base_Interface
		return null
	}

	/**
	 * Determine the file name of C++ code that realizes the skill definition by means of its default semantics
	 */
	def static realizationFileName(SkillDefinition sdef) {
		if (sdef=== null)
			throw new Exception("Model error! Found UML::Operation with no stereotype applied!")
		// otherwise...
		val ops = sdef.defaultSemantic.firstOpState
		val suffix = 	if (doesConfigAndCoordOfComponents(ops) &&
								getCompInterface(ops).communicationPattern.isQuery)
							"_condition"
						else
							"_action"
		// return the file name with from split string when an uppercase letter is found
		// (see https://stackoverflow.com/questions/3752636/java-split-string-when-an-uppercase-letter-is-found)
		return String.join("_", sdef.base_Operation.name.split("(?<=.)(?=\\p{Lu})")).toLowerCase + suffix
	}

	/**
	 * Determine the file name of C++ code that realizes the skill operational state
	 * General version of the function that takes explicitly the SkillSemantic and one of its operational states (NOT USED BY NOW)
	 */
	/*def static realizationFileName(SkillDefinition sdef, SkillSemantic sem, SkillOperationalState ops) {
		if (sdef=== null)
			throw new Exception("Model error! " + sdef.base_Operation.name + " has no stereotype applied!")
		if (sem === null)
			throw new Exception("Model error! " + sem.base_StateMachine.name + " has no stereotype applied!")
		if (ops === null)
			throw new Exception("Model error! " + ops.base_State.name + " has no stereotype applied!")
		if (!sem.operational.contains(ops))
			throw new Exception("Model error! " + ops.base_State.name + " is not a state of " + sem.base_StateMachine.name)	
		// otherwise...
		var realizBaseName = String.join("_", sdef.base_Operation.name.split("(?<=.)(?=\\p{Lu})")).toLowerCase
		if (sdef.defaultSemantic !== sem)
			realizBaseName += "_" + sem.base_StateMachine.name.toLowerCase + "_" + ops.base_State.name.toLowerCase
		return realizBaseName + "_action_bt_node"
	}*/

	/**
	 * API that operates on UML object (i.e., not directly on Stereotypes)
	 * ===================================================================
	 */

	/**
	 * doesConfigAndCoordOfComponents() on UML::State
	 */
	def static doesConfigAndCoordOfComponents(State st) {
		return doesConfigAndCoordOfComponents( UMLUtil.getStereotypeApplication(st, SkillOperationalState) )
	}

	/**
	 * getUniqueSkills() on UML::Class
	 */
	def static LinkedHashSet<SkillDefinition> getUniqueSkills(Class sys) {
		if (sys === null)
			return null;
		return getUniqueSkills( UMLUtil.getStereotypeApplication(sys, System) )
	}

	def static getROS2PackageNameOfDefaultTask(Class sys) {
		if (sys === null)
			return null;
		return getFilePathCoordinatesOfDefaultTask(UMLUtil.getStereotypeApplication(sys, System))
	}

	def static getPackageRelativePathOfDefaultTask(Class sys) {
		if (sys === null)
			return null;
		return getPackageRelativePathOfDefaultTask(UMLUtil.getStereotypeApplication(sys, System), true)
	}

	def static getPackageRelativeFolderNameOfDefaultTask(Class sys) {
		if (sys === null)
			return null;
		return getPackageRelativePathOfDefaultTask(UMLUtil.getStereotypeApplication(sys, System), false)
	}

	def static getBTFileNameOfDefaultTask(Class sys) {
		if (sys === null)
			return null;
		return getBTFileNameOfDefaultTask(UMLUtil.getStereotypeApplication(sys, System))
	}
}
