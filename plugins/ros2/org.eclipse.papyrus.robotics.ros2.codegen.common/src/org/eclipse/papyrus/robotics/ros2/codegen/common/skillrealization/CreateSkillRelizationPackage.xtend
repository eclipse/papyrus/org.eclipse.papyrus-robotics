/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.skillrealization

import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException
import org.eclipse.papyrus.robotics.core.utils.PortUtils
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.core.utils.InteractionUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*

/**
 * Handle creation of a ROS 2 skill realization package
 */
abstract class CreateSkillRelizationPackage {

	private static def msgAbortManyNamesForInterface(String ifName, String registeredName, String newName) {
		return String.format(
					"abort transformation, coordination interface (%s) exposed through different names (%s, %s) is not supported.",
						ifName, registeredName, newName
				)
	}

	private static def msgAbortIncompatibleServices(String refName, String registeredIfName, String newIfName) {
		return String.format(
					"abort transformation, the same name (%s) cannot be used to refer different Coordination interfaces (%s, %s).",
						refName, registeredIfName, newIfName
				)
	}

	/**
	 * List of selected realization semantics for each skill definition
	 */
	Map<SkillDefinition, SkillSemantic> skdefToSemanticsMap

	/**
	 * Map to ensure that each CoordinationService is exposed through the same name
	 */
	Map<Interface, String> serviceToNameMap

	/**
	 * Map to ensure that a given name does not correspond to different incompatible CoordinationServices
	 */
	Map<String, Interface> nameToServiceMap

	/**
	 * List of referenced packages
	 */
	// Map<String, Boolean> packageNames

	

	/**
	 * Construct
	 * 
	 * Assuming default semantics of skill is used.
	 * TODO. Manage the general case where alternative realization semantics can be specified
	 */
	new(Class system, List<Class> components) {
		// packageNames = new HashMap<String, Boolean>() // TODO (remove unused)

		skdefToSemanticsMap = new HashMap<SkillDefinition, SkillSemantic>()

		// Assuming default semantics of skill is used.
		// TODO. Manage the general case where alternative realization semantics can be specified
		// (this information should normally be accessible through the system Class)
		for (skill : system.uniqueSkills) {
			// Register Default Semantics as the selected one for the skill definition
			skdefToSemanticsMap.put(skill, skill.defaultSemantic)
		}

		// Check that the same coordination interface is exposed by system components using the same name
		// TODO. Manage this situation with remapping of ROS 2 actions (when available)
		serviceToNameMap = new HashMap<Interface, String>()
		nameToServiceMap = new HashMap<String, Interface>()
		for (component : components)
			for (port : PortUtils.getAllPorts(component))
				if (port.communicationPattern !== null && port.communicationPattern.isAction) {
					// Consistency check 1 - each CoordinationService is exposed through the same name
					if (serviceToNameMap.containsKey(port.serviceDefinition)) {
						// Interface already registered, check consistency!
						if (serviceToNameMap.get(port.serviceDefinition) != port.name)
							throw new
								TransformationException(
									msgAbortManyNamesForInterface(
										port.serviceDefinition.name,
										serviceToNameMap.get(port.serviceDefinition),
										port.name
									)
								);
					}
					// Interface not registered. Register it
					serviceToNameMap.put(port.serviceDefinition, port.name)

					// Consistency check 2 - a given name does not correspond to different incompatible CoordinationServices
					if (nameToServiceMap.containsKey(port.name))
						// Name already registered, check consistency!
						if (nameToServiceMap.get(port.name) !== port.serviceDefinition)
							throw new
								TransformationException(
									msgAbortIncompatibleServices(
										port.name,
										nameToServiceMap.get(port.name).name,
										port.serviceDefinition.name
									)
								);
					// Name not registered. Register it
					nameToServiceMap.put(port.name, port.serviceDefinition)
				}
	}

	/**
	 * Create a skill realization package from the list of skills to be executed by a given system
	 */
	abstract def void createSkillRealizationPkg(Package pkg);
}
