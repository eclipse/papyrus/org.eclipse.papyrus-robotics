/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common;

import java.util.Map;

/**
 * Robotics transformation context
 */
public class RoboticsTContext {
	public static RoboticsTContext current;

	/**
	 * Use a re-write map to store which projects have already been
	 * rewritten (if the user chooses to do so). This attribute is used
	 * to assure that project is rewritten once only.
	 */
	public Map<String, Boolean> rewriteMap = null;

	/**
	 * A reference to language specific transformations
	 */
	public LangSpecificTransformations lst;

	/**
	 * return true, if the passed project should be rewritten
	 * 
	 * @param projectName
	 *            a name identifying a project
	 * @return
	 */
	public static boolean rewriteProject(String projectName) {
		Map<String, Boolean> rewriteMap = current.rewriteMap;
		if (rewriteMap != null) {
			if (!rewriteMap.containsKey(projectName)) {
				rewriteMap.put(projectName, true);
				return true;
			}
		}
		return false;
	}
}
