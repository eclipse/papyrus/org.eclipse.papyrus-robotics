/*****************************************************************************
 * Copyright (c) 2018, 2021 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr - Bug #571655
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common

import java.util.ArrayList
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.papyrus.designer.languages.common.base.file.FileSystemAccessFactory
import org.eclipse.papyrus.designer.languages.common.base.file.ICleanUntouched
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo
import org.eclipse.papyrus.robotics.codegen.common.utils.ApplyProfiles
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinitionModel
import org.eclipse.papyrus.robotics.profile.robotics.components.SystemComponentArchitectureModel
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinitionModel
import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils
import org.eclipse.papyrus.robotics.ros2.codegen.common.message.CreateMsgPackage
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils
import org.eclipse.papyrus.uml.tools.utils.PackageUtil
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.*
import static extension org.eclipse.papyrus.robotics.core.utils.InstanceUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.launch.LaunchScript.generateLaunch
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*

class RosTransformations implements IM2MTrafoCDP {

	CreateMsgPackage msgPkgCreator

	/**
	 * Create code for a ROS 2 package. The passed pkg is used to
	 * identify the ros2 package
	 * @param pkg the passed ROS 2 package
	 */
	def pkgTrafos(Package pkg) {
		val lst = RoboticsTContext.current.lst
		val project = lst.getProject(TransformationContext.initialSourceRoot.projectName)
		if (project !== null) {
			/*
			 * obtain the list of component definitions defined in the ROS 2 package (from the project),
			 * the list of components defined in the system and the concatenation of these (a system
			 * does not necessarily instantiate all components defined within the package)
			 */ 
			val compDefsInPkg = project.compDefs
			val compDefsInSys = new ArrayList<Class>();
			// all component definitions
			val compDefs = new ArrayList<Class>();
			compDefs.addAll(compDefsInPkg)
			val system = project.system
			if (system !== null) {
				for (part : system.compInstanceList) {
					val type = part.type;
					compDefsInSys.add(type as Class)
					if (!compDefs.contains(type)) {
						compDefs.add(type as Class)
					}
				} 
			}
			val fileAccess = FileSystemAccessFactory.create(project)
			lst.createCompDefBuildFiles(fileAccess, pkg, compDefs, compDefsInPkg, system)
			val depsForCDT = new ArrayList<String>
			depsForCDT.addAll(MessageUtils.calcDependencies(compDefsInPkg));
			val additionalDeps = #[
				"builtin_interfaces",
				"rcl_interfaces",
				"rcl_logging_interface",
				"rcpputils",
				"rcutils"
			]
			depsForCDT.addAll(additionalDeps)
			
			lst.configureProject(project, depsForCDT)
			if (system !== null) {
				// generate launch before the component transformations
				// since the latter remove ports
				fileAccess.generateLaunch(system)
			}
			val ct = lst.getCompTransformation(fileAccess, project);
			for (compDef : compDefsInPkg) {
				ct.componentTrafo(compDef, msgPkgCreator)
			}
			if (!system.uniqueSkills.nullOrEmpty) {
				lst.createSkillRealizationPkg(system, system.componentList, pkg)
			}
			for (compDef : compDefsInPkg) {
				ct.componentCodegen(compDef, msgPkgCreator)
			}
			val cleanup = fileAccess as ICleanUntouched
			cleanup.cleanUntouched(project.getFolder("src-gen"), new NullProgressMonitor);
			cleanup.cleanUntouched(project.getFolder("src-skel"), new NullProgressMonitor);

			TransformationContext.current.project = project;
		} else {
			throw new TransformationException(ExecuteTransformationChain.USER_CANCEL);
		}
	}

	/**
	 * Apply transformation, support three cases
	 * 1. User called transformation in a component definition model
	 * 	=> apply transformation in that model (only)
	 * 2. User called transformation in a component definition model
	 * 	=> apply transformation in that model (only)
	 * 3. User called transformation in a system architecture
	 *  => obtain all components within the system
	 */
	override applyTrafo(M2MTrafo trafo, Package rootPkg) throws TransformationException {
		// assure that Common and C++ profiles is applied
		EnvironmentUtils.waitForSetupJob
		ApplyProfiles.applyCommonProfile(rootPkg)
		// TODO: remove C++ profile
		ApplyProfiles.applyCppProfile(rootPkg)
		msgPkgCreator = new CreateMsgPackage();
		if (StereotypeUtil.isApplied(rootPkg, ServiceDefinitionModel)) {
			msgPkgCreator.createMsgPkg(rootPkg)
		}
		else if (StereotypeUtil.isApplied(rootPkg, ComponentDefinitionModel)) {
			val component = rootPkg.componentFromPkg
			if (component !== null) {
				val pkg = PackageUtil.getRootPackage(component);
				pkgTrafos(pkg)
			}
		} else if (StereotypeUtil.isApplied(rootPkg, SystemComponentArchitectureModel)) {
			val system = rootPkg.system
			if (system !== null) {
				val pkgList = new ArrayList<Package>
				val systemPkg = PackageUtil.getRootPackage(system);
				pkgList.add(systemPkg)

				for (component : system.componentList) {
					val compPkg = PackageUtil.getRootPackage(system)
					if (!pkgList.contains(compPkg)) {
						pkgList.add(compPkg)
					}
				}

				for (pkg : pkgList) {
					pkgTrafos(pkg)
				}
			}
		} else {
			Activator.log.debug(
				String.format("model %s is neither a component definition or a component assembly", rootPkg.name));
		}
 		// CCorePlugin.getIndexManager().setDefaultIndexerId(currentIndexer);
	}
}
