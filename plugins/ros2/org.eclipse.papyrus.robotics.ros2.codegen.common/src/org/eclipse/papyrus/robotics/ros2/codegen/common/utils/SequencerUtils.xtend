/*****************************************************************************
 * Copyright (c) 2020, 2024 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 *  Matteo MORELLI      matteo.morelli@cea.fr - #12/Simplify the process to execute a system task
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.utils

import org.eclipse.uml2.uml.Class

class SequencerUtils {

	/**
	 * Return a name for the Sequencer object
	 * (The current logics is to return the system name, with the word "Sequencer" appended)
	 */
	def static getSequencerName(Class system) {
		return system.name + "Sequencer"
	}

	/**
	 * Return the name of Sequencer's execute bt action
	 */
	def static getSequencerExecuteBtActionName(Class system) {
		return "/"+ system.getSequencerName + "/execute_bt"
	}

	/**
	 * Return the name of Sequencer's execute bt action interface
	 */
	public static final String EXECUTE_BT_ACTION_INTERFACE = "bt_sequencer_msgs/action/ExecuteBt"

	/**
	 * Return the goal of Sequencer's execute bt action
	 */
	public static final String EXECUTE_BT_ACTION_GOAL = "{}"
}
