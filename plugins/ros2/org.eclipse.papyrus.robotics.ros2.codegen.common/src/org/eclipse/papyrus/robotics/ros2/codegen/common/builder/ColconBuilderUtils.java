/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher (CEA LIST) - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.papyrus.robotics.ros2.codegen.common.Activator;

/**
 * Utility functions for adding colcon buildspec to existing projects
 *
 */
public class ColconBuilderUtils {

	/**
	 * Add the colcon build command to the build specification of the passed project
	 * 
	 * @param project
	 *            an Eclipse project
	 * @return if the nature has been added (false, if the project already had the nature or there was an exception)
	 */
	public static boolean add(final IProject project) {

		if (project != null) {
			try {
				// verify already registered builders
				if (hasBuilder(project))
					// already enabled
					return false;

				// add builder to project properties
				IProjectDescription description = project.getDescription();
				final ICommand buildCommand = description.newCommand();
				buildCommand.setBuilderName(ColconBuilder.BUILDER_ID);

				final List<ICommand> commands = new ArrayList<ICommand>();
				commands.addAll(Arrays.asList(description.getBuildSpec()));
				commands.add(buildCommand);

				description.setBuildSpec(commands.toArray(new ICommand[commands.size()]));
				project.setDescription(description, null);

			} catch (final CoreException e) {
				Activator.log.error(e);
			}
		}
		return false;
	}

	/**
	 * Check, if the passed project already has a colcon build command
	 * 
	 * @param project
	 *            an Eclipse project
	 * @return true, if the passed project has a colcon builder
	 */
	public static final boolean hasBuilder(final IProject project) {
		try {
			for (final ICommand buildSpec : project.getDescription().getBuildSpec()) {
				if (ColconBuilder.BUILDER_ID.equals(buildSpec.getBuilderName()))
					return true;
			}
		} catch (final CoreException e) {
			Activator.log.error(e);
		}
		return false;
	}
}
