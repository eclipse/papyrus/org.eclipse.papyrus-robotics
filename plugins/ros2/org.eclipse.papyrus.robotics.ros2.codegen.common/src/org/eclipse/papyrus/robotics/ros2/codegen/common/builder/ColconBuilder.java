/*****************************************************************************
 * Copyright (c) 2023 CEA LIST and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher (CEA LIST) - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.builder;

import java.io.IOException;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.robotics.ros2.base.ProcessUtils;
import org.eclipse.papyrus.robotics.ros2.base.ProcessUtils.LogKind;
import org.eclipse.papyrus.robotics.ros2.codegen.common.Activator;

public class ColconBuilder extends IncrementalProjectBuilder {

	public static final String BUILDER_ID = "org.eclipse.papyrus.robotics.ros2.colconbuilder"; //$NON-NLS-1$

	@Override
	protected IProject[] build(final int kind, final Map<String, String> args, final IProgressMonitor monitor)
			throws CoreException {

		// don't check build kind, launch a full colcon build
		ColconProcessBuilder cpb = new ColconProcessBuilder(getProject().getName().toLowerCase());
		try {
			Process p = cpb.start();
			ProcessUtils.writeToConsole(String.format("[directory: %s]", //$NON-NLS-1$
					cpb.getProcessBuilder().directory().getAbsolutePath()));
			ProcessUtils.writeToConsole(p.info().commandLine().orElse("not available")); //$NON-NLS-1$
			synchronized (p){
				p.wait(100);
		    }
			while (p.isAlive() && !monitor.isCanceled()) {
			}
			ProcessUtils.logProcess(p, LogKind.ALL);
		} catch (IOException | InterruptedException e) {
			Activator.log.error(e);
		}
		return null;
	}
}