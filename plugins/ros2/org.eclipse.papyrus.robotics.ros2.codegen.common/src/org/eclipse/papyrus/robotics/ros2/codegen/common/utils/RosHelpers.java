/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.utils;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.External;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr;
import org.eclipse.papyrus.robotics.codegen.common.utils.Helpers;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class RosHelpers {
	public static final URI ROS_PRIMITIVE_URI = URI.createURI("pathmap://ROS2_LIBRARY/ros2.primitive.uml"); //$NON-NLS-1$

	public static final URI ROS_LIBRARY_URI = URI.createURI("pathmap://ROS2_LIBRARY/ros2Library.uml"); //$NON-NLS-1$

	/**
	 * Get a type from the ROS primitive library
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @param qualifiedName
	 *            the qualified name of the element to load
	 * @return the element, if found, null otherwise
	 */
	public static Type getRosPrimitiveType(Element element, String qualifiedName) {
		return Helpers.getTypeFromRS(element, ROS_PRIMITIVE_URI, qualifiedName, true);
	}

	/**
	 * Get a type from the ROS library
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @param qualifiedName
	 *            the qualified name of the element to load
	 * @return the element, if found, null otherwise
	 */
	public static Type getRosType(Element element, String qualifiedName) {
		return Helpers.getTypeFromRS(element, ROS_LIBRARY_URI, qualifiedName, false);
	}

	/**
	 * Return the scoped name of the communication object, based on its external stereotype.
	 * 
	 * @param type
	 *            a type with the "External" stereotype
	 * @return the external name used for that type
	 */
	public static String externalName(Type type) {
		if (type != null) {
			External external = UMLUtil.getStereotypeApplication(type, External.class);
			if (external != null) {
				return external.getName();
			}
			return type.getQualifiedName();
		}
		return null;
	}

	/**
	 * use Ptr declaration to add ::SharedPtr postfix
	 * 
	 * @param typedElement
	 */
	public static void useSharedPtr(TypedElement typedElement) {
		Ptr ptr = StereotypeUtil.applyApp(typedElement, Ptr.class);
		ptr.setDeclaration("::SharedPtr"); //$NON-NLS-1$
	}

	/**
	 * Return a small_caps string (for file names) in which CamlCase is prefixed with "_"
	 * e.g. ServiceType is transformed into service_type. The "_" is added to upper case
	 * characters following previous alphabetic or digit characters. I.e. it is not added
	 * after special characters including in particular the FileSeparator.
	 * This function is not only used by ROS, but also by protobuf
	 *
	 * @param str
	 *            a string
	 * @return the transformed string
	 */
	public static String escapeCamlCase(String str) {
		String out = StringConstants.EMPTY;
		boolean previousLC = false;
		boolean previousUC = false;
		int i = 1;
		for (char c : str.toCharArray()) {
			// underscore prefix also depends on next character
			boolean nextLC = i < str.length() ? Character.isLowerCase(str.charAt(i++)) : false;
			if (Character.isUpperCase(c)) {
				if (previousLC || (previousUC && nextLC)) {
					out += StringConstants.UNDERSCORE;
				}
			}
			previousLC = Character.isLowerCase(c);
			previousUC = Character.isUpperCase(c);
			out += Character.toLowerCase(c);
		}
		return out;
	}

	/**
	 * Simple implementation of avoiding underscores by just removing them (for service definition filenames)
	 *
	 * @param str
	 *            a string
	 * @return the string without underscore
	 */
	public static String escapeUnderscore(String str) {
		return str.replaceAll(StringConstants.UNDERSCORE, StringConstants.SPACE);
	}

}
