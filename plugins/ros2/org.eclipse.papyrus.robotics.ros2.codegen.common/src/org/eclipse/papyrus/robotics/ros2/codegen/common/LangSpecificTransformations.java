/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;
import org.eclipse.papyrus.robotics.ros2.codegen.common.component.AbstractCompTransformations;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;

/**
 * Interface for transformations that are specific for a specific programming language, notably
 * C++ and Python
 */
public interface LangSpecificTransformations {
	/**
	 * 
	 * @param fileAccess
	 * @param pkg
	 * @param allComponents
	 * @param componentsInPkg
	 * @param system
	 */
	public void createCompDefBuildFiles(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system);

	/**
	 * 
	 * @param fileAccess
	 * @param pkg
	 */
	public void createMsgBuildFiles(IPFileSystemAccess fileAccess, Package pkg);

	/**
	 * Create a message package (Eclipse project) for a service-definition model
	 * 
	 * @param msgPkg
	 *            a service-definition model
	 */
	public void createMsgPackage(final Package msgPkg);

	/**
	 * Create a new instance of the abstract class AbstractCompTransformation that must be implemented
	 * by subclasses.
	 * 
	 * @param fileAccess
	 *            a file system access
	 * @param project
	 *            the associated eclipse project
	 * @return the component-transformation instance
	 */
	public AbstractCompTransformations getCompTransformation(IPFileSystemAccess fileAccess, IProject project);

	/**
	 * Create a new skill-realization package
	 * 
	 * @param system
	 *            a system (top-level class)
	 * @param components
	 *            a list of component definitions
	 * @param pkg
	 *            a skill-realization-package
	 */
	public void createSkillRealizationPkg(Class system, List<Class> components, Package pkg);

	/**
	 * Return a language specific Eclipse project, given its name
	 */
	public IProject getProject(final String projectName);

	/**
	 * Configure project. For instance, in case of C++, configure include directories
	 * 
	 * @param project
	 * @param depPkgList
	 */
	public void configureProject(IProject project, List<String> depPkgList);
}
