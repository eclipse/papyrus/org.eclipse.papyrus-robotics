/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.common.utils

import java.util.Collections
import java.util.List
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity
import org.eclipse.papyrus.robotics.ros2.preferences.Ros2PreferenceUtils
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.util.UMLUtil

/**
 * Set of utility functions for package creation
 */
class PackageXMLUtils {

	/**
	 * Return the description of a model from the Entity stereotype
	 */
	static def description(List<Package> modelList) {
		val description = getField(modelList, BPCPackage.eINSTANCE.entity_Description);
		if (description !== null) {
			return description
		}
		return "No description"
	}

	static def description(Package model) {
		return description(Collections.singletonList(model))
	}

	/**
	 * get author name. If authorship is set and contains a space, assume that it starts with an email address
	 * @param modelList a list of models (belonging to the same ROS 2 package) - take
	 *		information from first model with non-null information
	 */
	def static getAuthorName(List<Package> modelList) {
		val author = getField(modelList, BPCPackage.eINSTANCE.entity_Authorship);
		if (author !== null) {
			val idx = author.indexOf(" ");
			if (idx > 0) {
				return author.substring(idx + 1);
			}
			return author;
		}
		return Ros2PreferenceUtils.authorName;
	}

	def static getAuthorName(Package model) {
		return getAuthorName(Collections.singletonList(model))
	}

	/**
	 * get author mail. If authorship is set and contains a space, assume that it starts with an email address
	 * @param modelList a list of models (belonging to the same ROS 2 package) - take
	 *		information from first model with non-null information
	 */
	def static getAuthorMail(List<Package> modelList) {
		val author = getField(modelList, BPCPackage.eINSTANCE.entity_Authorship);
		if (author !== null) {
			val idx = author.indexOf(" ");
			if (idx > 0) {
				return author.substring(0, idx);
			}
			return author;
		}
		return Ros2PreferenceUtils.authorMail;
	}

	def static getAuthorMail(Package model) {
		return getAuthorMail(Collections.singletonList(model))
	}

	/**
	 * Get maintainer name. If maintainer is set and contains a space, assume that it starts with an email address
	 * Take maintainer information from provenance field in RobMoSys
	 * @param modelList a list of models (belonging to the same ROS 2 package) - take
	 *		information from first model with non-null information
	 */
	def static getMaintainerName(List<Package> modelList) {
		val maintainer = getField(modelList, BPCPackage.eINSTANCE.entity_Provenance);
		if (maintainer !== null) {
			val idx = maintainer.indexOf(" ");
			if (idx > 0) {
				return maintainer.substring(idx + 1);
			}
			return maintainer;
		}
		return Ros2PreferenceUtils.maintainerName;
	}

	def static getMaintainerName(Package model) {
		return getMaintainerName(Collections.singletonList(model))
	}

	/**
	 * Get maintainer mail. If maintainer is set and contains a space, assume that it starts with an email address
	 * Take maintainer information from provenance field in RobMoSys
	 * @param modelList a list of models (belonging to the same ROS 2 package) - take
	 *		information from first model with non-null information
	 */
	def static getMaintainerMail(List<Package> modelList) {
		val maintainer = getField(modelList, BPCPackage.eINSTANCE.entity_Provenance);
		if (maintainer !== null) {
			val idx = maintainer.indexOf(" ");
			if (idx > 0) {
				return maintainer.substring(0, idx);
			}
			return maintainer;
		}
		return Ros2PreferenceUtils.maintainerMail;
	}

	def static getMaintainerMail(Package model) {
		return getMaintainerMail(Collections.singletonList(model))
	}

	/**
	 * return the first non-null field (feature) from the Entity stereotype
	 * from a list of packages
	 * 
	 * @param modelList
	 *            a list of packages (root of models)
	 * @param feature
	 *            The feature to query
	 * @return the author/maintainer/... information
	 */
	def static String getField(List<Package> modelList, EAttribute feature) {
		for (pkg : modelList) {
			val application = UMLUtil.getStereotypeApplication(pkg, Entity);
			if (application !== null && application.eGet(feature) instanceof String) {
				return application.eGet(feature) as String;
			}
		}
		return null
	}
}
