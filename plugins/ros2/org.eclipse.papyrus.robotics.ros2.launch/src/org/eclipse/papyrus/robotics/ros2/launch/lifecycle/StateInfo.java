package org.eclipse.papyrus.robotics.ros2.launch.lifecycle;

import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState.EState;

/**
 * an entry for the state info: state info comes
 * with a time information in order to enable caching of state
 * data
 */
public class StateInfo {
	public EState state;
	long time;
}
