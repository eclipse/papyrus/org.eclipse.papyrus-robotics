/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch.utils;

import java.io.File;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.robotics.codegen.common.utils.ComponentUtils;
import org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;

public class LaunchUtils {
	/**
	 * Return the file name of the executable associated with an instance
	 * This filename is a path relative to the workspace root
	 *
	 * @param instance
	 *            a component instance
	 * @return the file name of the executable
	 */
	public static String getExecutable(Property instance) {
		final Class component = (Class) instance.getType();
		final Package compRoot = PackageUtil.getRootPackage(instance);
		final String pkgName = PackageTools.pkgName(compRoot);
		String executable = String.format("build/%s/%s", pkgName, component.getName()); //$NON-NLS-1$
		if (ComponentUtils.isRegistered(component)) {
			executable += "_main"; //$NON-NLS-1$
		}
		return executable;
	}

	/**
	 * Check whether the executable associated within an instance exists
	 * 
	 * @param instance
	 * @return true, iff the executable exists
	 */
	public static boolean existsExecutable(Property instance) {
		String relPath = getExecutable(instance);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String fileName = root.getLocation().toString() + StringConstants.SLASH + relPath;
		return new File(fileName).exists();
	}

	/**
	 * Check whether the executable associated within an instance exists
	 * 
	 * @param instance
	 * @return true, iff the executable exists
	 */
	public static boolean isSingleton(Property instance) {
		String relPath = getExecutable(instance);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String fileName = root.getLocation().toString() + StringConstants.SLASH + relPath;
		return new File(fileName).exists();
	}
}
