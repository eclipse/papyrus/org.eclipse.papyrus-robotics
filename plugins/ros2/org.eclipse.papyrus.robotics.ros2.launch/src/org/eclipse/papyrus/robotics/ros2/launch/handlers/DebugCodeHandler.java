/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.papyrus.robotics.ros2.launch.Activator;
import org.eclipse.papyrus.robotics.ros2.launch.CreateDebugConfig;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Property;

@SuppressWarnings("nls")
public class DebugCodeHandler extends CmdHandler {

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		updateSelectedEObject();
		if (selectedEObject instanceof Property) {
			final Property instance = (Property) selectedEObject;

			Job job = new Job("Launch ROS code") {

				@Override
				public IStatus run(IProgressMonitor monitor) {
					// execute the task ...
					try {
						ILaunchConfiguration config = CreateDebugConfig.create(instance);
						DebugUITools.launch(config, ILaunchManager.DEBUG_MODE);
					} catch (CoreException e) {
						Activator.log.error(e);
					}

					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
		return null;
	}
}
