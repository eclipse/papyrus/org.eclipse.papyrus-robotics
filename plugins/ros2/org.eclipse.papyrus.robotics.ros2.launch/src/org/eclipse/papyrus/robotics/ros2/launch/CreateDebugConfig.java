/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.cdt.debug.core.ICDTLaunchConfigurationConstants;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools;
import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils;
import org.eclipse.papyrus.robotics.ros2.codegen.common.launch.LaunchScript;
import org.eclipse.papyrus.robotics.ros2.launch.utils.LaunchUtils;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;

/**
 * Create a standard CDT launch configuration in order to debug the binary of a
 * component definition. Since CDT needs debug the file, no launch scripts are
 * used.
 */
@SuppressWarnings("nls")
public class CreateDebugConfig {
	private static final String COLORIZE_OUTPUT = "RCUTILS_COLORIZED_OUTPUT"; //$NON-NLS-1$

	private static final String LIB_PATH = "LD_LIBRARY_PATH"; //$NON-NLS-1$

	/**
	 * Create a new launch configuration or return an existing one (based on naming)
	 *
	 * 
	 * @param instance
	 *            the instance to create a launch configuration for
	 * @return the obtained launch configuration
	 * @throws CoreException
	 */
	public static ILaunchConfiguration create(Property instance) throws CoreException {
		Package compRoot = PackageUtil.getRootPackage(instance);
		final String pkgName = PackageTools.pkgName(compRoot);
		final String launchFile = instance instanceof Property ? String.format("activate.%s.py", instance.getName()) : //$NON-NLS-1$
				"activate.py"; //$NON-NLS-1$
		final String name = String.format("debug %s %s", pkgName, instance.getName()); //$NON-NLS-1$

		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager.getLaunchConfigurationType(ICDTLaunchConfigurationConstants.ID_LAUNCH_C_APP);

		ILaunchConfiguration[] lcs = manager.getLaunchConfigurations(type);
		for (ILaunchConfiguration lc : lcs) {
			if (lc.getName().equals(name)) {
				return lc;
			}
		}
		// not found, create a new one.
		ILaunchConfigurationWorkingCopy config = type.newInstance(null, name);
		config.setAttribute(LaunchConstants.PKG_NAME, pkgName);
		config.setAttribute(LaunchConstants.LAUNCH_SCRIPT, launchFile);
		// Eclipse console can't handle color escapes (unless you install
		// additional tools such as ANSI Escape)
		Map<String, String> map = new HashMap<String, String>();
		map.put(COLORIZE_OUTPUT, "0"); //$NON-NLS-1$
		map.put(LIB_PATH, EnvironmentUtils.getenv().get(LIB_PATH));
		config.setAttribute(ILaunchManager.ATTR_ENVIRONMENT_VARIABLES, map);
		String remapsNode = "__node:=" + instance.getName();
		String remapsPort = LaunchScript.remapsCmd(instance.getClass_(), instance).toString();
		String paramFileName = String.format("${workspace_loc}/install/%s/share/%s/launch/cfg/param.yaml", pkgName, pkgName);
		String args = String.format("--ros-args -r %s %s --params-file %s", remapsNode, remapsPort, paramFileName);
		config.setAttribute(ICDTLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS, args);
		String executable = "${workspace_loc}/" + LaunchUtils.getExecutable(instance);
		config.setAttribute(ICDTLaunchConfigurationConstants.ATTR_PROGRAM_NAME, executable);
		config.doSave();
		return config;
	}
}
