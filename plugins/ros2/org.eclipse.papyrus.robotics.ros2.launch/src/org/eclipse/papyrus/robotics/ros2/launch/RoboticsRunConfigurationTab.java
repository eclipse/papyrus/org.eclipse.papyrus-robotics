/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * The main configuration tab containing information about the
 * 1. package name
 * 2. launch script name
 *
 */
public class RoboticsRunConfigurationTab extends AbstractLaunchConfigurationTab {

	private static final String ICON_P4R_PNG = "/icons/robotics-16x16.png"; //$NON-NLS-1$

	protected static String TAB_NAME = "Robotics Main"; //$NON-NLS-1$

	protected Composite mainContainer;

	protected Image image;

	Label pkgNameLabel;
	Text pkgNameText;

	Label scriptLabel;
	Text scriptText;

	public void initializeFrom(ILaunchConfiguration configuration) {
		try {
			pkgNameText.setText(configuration.getAttribute(LaunchConstants.PKG_NAME, StringConstants.EMPTY));
			scriptText.setText(configuration.getAttribute(LaunchConstants.LAUNCH_SCRIPT, StringConstants.EMPTY));
		} catch (CoreException e) {
			Activator.log.error(e);
		}
	}

	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		configuration.setAttribute(LaunchConstants.PKG_NAME,
				pkgNameText.getText());
		configuration.setAttribute(LaunchConstants.LAUNCH_SCRIPT,
				scriptText.getText());
	}

	public void createControl(Composite parent) {
		mainContainer = new Composite(parent, SWT.FILL);
		mainContainer.setLayout(new GridLayout(2, false));

		GridData descriptionGD = new GridData();
		descriptionGD.horizontalAlignment = GridData.FILL;
		descriptionGD.grabExcessHorizontalSpace = true;
		
		pkgNameLabel = new Label(mainContainer,  SWT.NONE);
		pkgNameLabel.setText(Messages.RoboticsRunConfigurationTab_PKG_NAME);
		pkgNameText = new Text(mainContainer, SWT.NONE | SWT.WRAP);
		pkgNameText.setLayoutData(descriptionGD);
		pkgNameText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				updateLaunchConfigurationDialog();
			}
		});
		scriptLabel = new Label(mainContainer,  SWT.NONE);
		scriptLabel.setText(Messages.RoboticsRunConfigurationTab_LAUNCH_SCRIPT);
		scriptText = new Text(mainContainer, SWT.NONE | SWT.WRAP);
		scriptText.setLayoutData(descriptionGD);

		setControl(mainContainer);
	}

	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
	}

	@Override
	public void updateLaunchConfigurationDialog() {
		super.updateLaunchConfigurationDialog();
	}

	public String getName() {
		return TAB_NAME;
	}

	@Override
	public Image getImage() {
		if (this.image == null) {
			this.image = new Image(Display.getCurrent(), org.eclipse.papyrus.robotics.diagrams.Activator.getDefault().getClass().getResourceAsStream(ICON_P4R_PNG));
		}
		return this.image;
	}
}
