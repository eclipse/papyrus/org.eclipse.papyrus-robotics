package org.eclipse.papyrus.robotics.ros2.launch.lifecycle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.robotics.ros2.base.ProcessUtils;
import org.eclipse.papyrus.robotics.ros2.base.Ros2Constants;
import org.eclipse.papyrus.robotics.ros2.base.Ros2ProcessBuilder;
import org.eclipse.papyrus.robotics.ros2.launch.Activator;

public class LifecycleState {

	// refresh lifecycle info after 1500ms
	public static long MAX_AGE = 1500;

	public enum EState {
		UNKNOWN, UNCONFIGURED, INACTIVE, ACTIVE, FINALIZED,
		// transition states
		CONFIGURE, ACTIVATE, DEACTIVATE, SHUTDOWN, CLEANUP
	}

	@SuppressWarnings("nls")
	protected String statesStr[] = {
			"unconfigured",
			"inactive",
			"active",
			"shutdown"
	};

	static Map<String, StateInfo> hash = new WeakHashMap<String, StateInfo>();

	public static EState getState(String nodename) {
		long time = System.currentTimeMillis();
		StateInfo si = hash.get(nodename);
		if (si != null) {
			if (time - si.time < MAX_AGE) {
				return si.state;
			}
		} else {
			si = new StateInfo();
			hash.put(nodename, si);
		}

		// information not present or too old
		si.time = time;
		si.state = EState.UNKNOWN;
		Ros2ProcessBuilder pbLC = new Ros2ProcessBuilder(Ros2Constants.LIFECYCLE, Ros2Constants.GET, nodename);

		try {
			Process p = pbLC.start();
			// wait up to 2 seconds for a result, otherwise log a timeout
			if (p.waitFor(2000, TimeUnit.MILLISECONDS)) {
				boolean error = ProcessUtils.logErrors(p);
				if (!error) {
					BufferedReader results = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String line;
					if ((line = results.readLine()) != null) {
						si.state = getEState(line);
					}
				}
			}
			else {
				Activator.log.debug("failed to obtain state before timeout"); //$NON-NLS-1$
			}
		} catch (IOException e) {
			Activator.log.error(e);
		} catch (InterruptedException e) {
			Activator.log.error(e);
		}
		return si.state;
	}

	public static void setState(String nodename, EState state) {
		StateInfo si = hash.get(nodename);
		if (si == null) {
			si = new StateInfo();
			hash.put(nodename, si);
		}

		Ros2ProcessBuilder pbLC = new Ros2ProcessBuilder(Ros2Constants.LIFECYCLE, Ros2Constants.SET, nodename, state.name().toLowerCase());
		try {
			Process p = pbLC.start();
			ProcessUtils.logErrors(p);
			long time = System.currentTimeMillis();
			si.time = time;
			si.state = state;
		} catch (IOException e) {
			Activator.log.error(e);
		}
	}

	public static EState getEState(String stateStr) {
		try {
			String stateArray[] = stateStr.split(StringConstants.SPACE);
			return EState.valueOf(stateArray[0].toUpperCase().trim());
		} catch (IllegalArgumentException e) {
			return EState.UNKNOWN;
		}
	}
}
