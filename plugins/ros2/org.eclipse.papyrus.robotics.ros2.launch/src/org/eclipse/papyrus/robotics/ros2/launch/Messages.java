package org.eclipse.papyrus.robotics.ros2.launch;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.robotics.ros2.launch.messages"; //$NON-NLS-1$
	public static String RoboticsRunConfigurationTab_LAUNCH_SCRIPT;
	public static String RoboticsRunConfigurationTab_PKG_NAME;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
