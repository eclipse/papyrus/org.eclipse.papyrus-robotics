/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState;
import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState.EState;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Property;

public class ActivateNodeHandler extends CmdHandler {

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		updateSelectedEObject();
		final Property instance = (Property) selectedEObject;
		final String instanceName = instance.getName();
		LifecycleState.setState(instanceName,  EState.ACTIVATE);
		return null;
	}
}
