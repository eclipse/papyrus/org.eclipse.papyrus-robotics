/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch;

public class LaunchConstants {
	final public static String PKG_NAME = "pkgName"; //$NON-NLS-1$

	final public static String LAUNCH_SCRIPT = "launchScript"; //$NON-NLS-1$

	final public static String ROBOTICS_LAUNCH_CONFIG =
			"org.eclipse.papyrus.robotics.ros2.launchConfiguration"; //$NON-NLS-1$
}
