/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;

/**
 * Create a launch configuration based on starting launch scripts via
 * "ros2 launch"
 *
 */
public class CreateLaunchConfig {
	private static final String COLORIZE_OUTPUT = "RCUTILS_COLORIZED_OUTPUT"; //$NON-NLS-1$

	/**
	 * Create a new launch configuration or return an existing one (based on naming)
	 *
	 * 
	 * @param instanceOrSystem
	 *            the instance or system to create a launch configuration for
	 * @param baseLaunch
	 *            base launch file (launch or activate)
	 * @return the obtained launch configuration
	 * @throws CoreException
	 */
	public static ILaunchConfiguration create(NamedElement instanceOrSystem, String baseLaunch) throws CoreException {

		Package compRoot = PackageUtil.getRootPackage(instanceOrSystem);
		final String pkgName = PackageTools.pkgName(compRoot);
		final String launchFile = instanceOrSystem instanceof Property ? String.format("%s_%s.py", baseLaunch, instanceOrSystem.getName()) : //$NON-NLS-1$
				String.format("%s.py", baseLaunch); //$NON-NLS-1$
		final String name = String.format("%s %s %s", baseLaunch, pkgName, instanceOrSystem.getName()); //$NON-NLS-1$

		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager.getLaunchConfigurationType(LaunchConstants.ROBOTICS_LAUNCH_CONFIG);

		ILaunchConfiguration[] lcs = manager.getLaunchConfigurations(type);
		for (ILaunchConfiguration lc : lcs) {
			if (lc.getName().equals(name)) {
				return lc;
			}
		}
		// not found, create a new one.
		ILaunchConfigurationWorkingCopy config = type.newInstance(null, name);
		config.setAttribute(LaunchConstants.PKG_NAME, pkgName);
		config.setAttribute(LaunchConstants.LAUNCH_SCRIPT, launchFile);
		// Eclipse console can't handle color escapes (unless you install
		// additional tools such as ANSI Escape)
		Map<String, String> map = new HashMap<String, String>();
		map.put(COLORIZE_OUTPUT, "0"); //$NON-NLS-1$
		config.setAttribute(ILaunchManager.ATTR_ENVIRONMENT_VARIABLES, map);
		config.doSave();
		return config;
	}
}
