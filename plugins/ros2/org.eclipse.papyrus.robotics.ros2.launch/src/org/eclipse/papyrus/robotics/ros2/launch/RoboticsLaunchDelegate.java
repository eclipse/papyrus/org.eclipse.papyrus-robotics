/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.launch;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.utils.spawner.ProcessFactory;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils;
import org.eclipse.papyrus.robotics.ros2.base.Ros2Constants;

/**
 * Launch a ROS 2 python script via the ros2 command line tool
 *
 */
public class RoboticsLaunchDelegate extends LaunchConfigurationDelegate implements ILaunchConfigurationDelegate {

	@Override
	public boolean buildForLaunch(ILaunchConfiguration configuration, String mode, IProgressMonitor monitor) throws CoreException {
		// don't build (todo: assure that only current project is build)
		return false;
	}

	@Override
	public void buildProjects(IProject projects[], IProgressMonitor monitor) {
		// do nothing
	}

	@Override
	public boolean preLaunchCheck(ILaunchConfiguration configuration, String mode, IProgressMonitor monitor)
			throws CoreException {
		EnvironmentUtils.sourceWorkspace();
		return true;
	}

	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {
		// execute the task ...

		try {
			String pkgName = configuration.getAttribute(LaunchConstants.PKG_NAME, StringConstants.EMPTY);
			String launchScript = configuration.getAttribute(LaunchConstants.LAUNCH_SCRIPT, StringConstants.EMPTY);

			String[] cmd = {
				Ros2Constants.ROS2, Ros2Constants.LAUNCH, pkgName, launchScript
			};
			Map<String, String> systemEnv = EnvironmentUtils.getenv();
			Map<String, String> configEnv = configuration.getAttribute(ILaunchManager.ATTR_ENVIRONMENT_VARIABLES, (Map<String, String>) null);
			
			// We don't use DebugPlugin.getDefault().getLaunchManager().getEnvironment
			// as it does not use the specific environment setup for ROS 2
			List<String> envp = new ArrayList<String>();
			for (String key : systemEnv.keySet()) {
				envp.add(
					String.format("%s=%s", key, systemEnv.get(key))); //$NON-NLS-1$
			}
			for (String key : configEnv.keySet()) {
				envp.add(
					String.format("%s=%s", key, configEnv.get(key))); //$NON-NLS-1$
			}
			String processLabel = String.format("%s %s %s %s", cmd[0], cmd[1], cmd[2], cmd[3]); //$NON-NLS-1$

			// Use CDT's process factory, it uses a "spawner" that takes care of properly
			// termination"
			Process p = ProcessFactory.getFactory().exec(cmd, envp.toArray(new String[0]), null /*, new PTY() */);
			DebugPlugin.newProcess(launch, p, processLabel, null);
		} catch (Exception e) {
			Activator.log.error(e);
		}
	}
}
