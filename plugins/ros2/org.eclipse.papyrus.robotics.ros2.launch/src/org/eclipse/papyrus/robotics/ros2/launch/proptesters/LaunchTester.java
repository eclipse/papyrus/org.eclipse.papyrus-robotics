package org.eclipse.papyrus.robotics.ros2.launch.proptesters;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.robotics.core.utils.InstanceUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState;
import org.eclipse.papyrus.robotics.ros2.launch.lifecycle.LifecycleState.EState;
import org.eclipse.papyrus.robotics.ros2.launch.utils.LaunchUtils;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;

public class LaunchTester extends PropertyTester {

	public static final String CAN_ACTIVATE = "canActivate"; //$NON-NLS-1$

	public static final String CURRENT_STATE = "currentState"; //$NON-NLS-1$

	public static final String BIN_AVAILABLE = "binAvailable"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (receiver instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) receiver;
			if (CURRENT_STATE.equals(property)) {
				return checkState(selection, expectedValue);
			}
			if (BIN_AVAILABLE.equals(property)) {
				return binaryAvailable(selection);
			}
			if (CAN_ACTIVATE.equals(property)) {
				return isLifecycle(selection);
			}
		}
		return false;
	}

	/**
	 * Check the state of a ROS 2 node in order to filter the possible transitions
	 *
	 * Check whether a binary is available
	 * 
	 * @param expectedValue
	 * @return
	 */
	protected boolean checkState(IStructuredSelection selection, Object expectedValue) {
		EObject prop = EMFHelper.getEObject(selection.getFirstElement());
		String expectedStateStr = (String) expectedValue;
		if (prop instanceof Property && expectedValue instanceof String) {
			Property instance = (Property) prop;
			if (StereotypeUtil.isApplied(instance, ComponentInstance.class)) {
				EState currentState = LifecycleState.getState(instance.getName());
				for (String expectedState : expectedStateStr.split(",")) { //$NON-NLS-1$
					if (currentState == LifecycleState.getEState(expectedState)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Check whether a binary is available
	 * 
	 * @param selection
	 *            a structured selection
	 * @return
	 */
	protected boolean binaryAvailable(IStructuredSelection selection) {
		EObject part = EMFHelper.getEObject(selection.getFirstElement());
		if (part instanceof Property) {
			Property instance = (Property) part;
			if (StereotypeUtil.isApplied(instance, ComponentInstance.class)) {
				return LaunchUtils.existsExecutable(instance);
			}
		}
		return false;
	}

	/**
	 * Check whether a system or instance can be launched and activated
	 * 
	 * @param selection
	 *            a structured selection
	 * @return
	 */
	protected boolean isLifecycle(IStructuredSelection selection) {
		EObject eObject = EMFHelper.getEObject(selection.getFirstElement());
		if (eObject instanceof Element) {
			Element systemOrpart = (Element) eObject;
			if (StereotypeUtil.isApplied(systemOrpart, org.eclipse.papyrus.robotics.profile.robotics.components.System.class)) {
				// for the moment, we can always activate a system,
				return true;
			}
			if (StereotypeUtil.isApplied(systemOrpart, ComponentInstance.class)) {
				Property instance = (Property) systemOrpart;
				return InstanceUtils.isLifecycle(instance);
			}
		}
		return true;
	}
}
