/*****************************************************************************
 * Copyright (c) 2012, 2016 CEA LIST, Christian W. Damus, and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) Vincent.Lorenzo@cea.fr - Initial API and implementation
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr
 *  Christian W. Damus - bug 485220
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.ros2.cdteditor.handler;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.editor.SyncCDTEditor;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.handler.SyncCDTEditorHandler;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.core.utils.ServiceUtils;
import org.eclipse.papyrus.infra.emf.gmf.command.CheckedOperationHistory;
import org.eclipse.papyrus.infra.ui.util.ServiceUtilsForHandlers;
import org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools;
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity;
import org.eclipse.papyrus.robotics.ros2.cdteditor.Activator;
import org.eclipse.papyrus.robotics.ros2.cdteditor.sync.SyncRoboticsCDTtoModel;
import org.eclipse.papyrus.robotics.ros2.cdteditor.sync.SyncRoboticsModelToCDT;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.uml2.uml.Classifier;


/**
 * The handler creates a new CDT editor
 */
public class SyncRoboticsCDTEditorHandler extends SyncCDTEditorHandler {


	public static final String CDT_EDITOR_QNAME = "org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.editor.SyncCDTEditor"; //$NON-NLS-1$
	public static final String STANDARD_C = "Standard C++"; //$NON-NLS-1$

	public SyncRoboticsCDTEditorHandler() {
	}

	/**
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 *
	 * @param event
	 *            an execution event
	 * @return the execution result
	 * @throws ExecutionException
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		try {
			final ServicesRegistry serviceRegistry = ServiceUtilsForHandlers.getInstance().getServiceRegistry(event);
			TransactionalEditingDomain domain = ServiceUtils.getInstance().getTransactionalEditingDomain(serviceRegistry);


			// Create the transactional command
			AbstractEMFOperation command = new AbstractEMFOperation(domain, "Create CDT editor") { //$NON-NLS-1$

				@Override
				protected IStatus doExecute(final IProgressMonitor monitor, final IAdaptable info) throws ExecutionException {
					try {
						SyncRoboticsCDTEditorHandler.this.doExecute(serviceRegistry);
					} catch (ServiceException e) {
						Activator.log.error(e);
						return Status.CANCEL_STATUS;
					} catch (NotFoundException e) {
						Activator.log.error(e);
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				}
			};

			// Execute the command
			CheckedOperationHistory.getInstance().execute(command, new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			Activator.log.error("Can't create a CDT editor", e); //$NON-NLS-1$
		} catch (ServiceException e) {
			Activator.log.error("Service exception during creation of CDT editor", e); //$NON-NLS-1$
		}
		return null;
	}


	/**
	 * Do the execution of the command.
	 *
	 * @param serviceRegistry
	 * @throws ServiceException
	 * @throws NotFoundException
	 */
	public void doExecute(final ServicesRegistry serviceRegistry) throws ServiceException, NotFoundException {
		Classifier classifierToEdit = getClassifierToEdit();

		IFile srcFile = SyncRoboticsModelToCDT.syncModelToCDT(classifierToEdit, STANDARD_C);
		if (srcFile == null) {
			return;
		}

		Display.getDefault().asyncExec(new Runnable() {

			@Override
			public void run() {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					// IEditorPart editorPart = page.openEditor(new FileEditorInput(srcFile), "org.eclipse.cdt.ui.editor.CEditor");

					IEditorInput input = new FileEditorInput(srcFile);
					IEditorPart editorPart = page.openEditor(input, CDT_EDITOR_QNAME);
					if (editorPart instanceof SyncCDTEditor) {
						// TODO: use editorPart.setInitializationData(cfig, propertyName, data);
						String projectName = PackageTools.getProjectName(classifierToEdit.getNearestPackage());
						SyncRoboticsCDTtoModel syncCpp = new SyncRoboticsCDTtoModel(input, classifierToEdit, projectName, STANDARD_C);

						((SyncCDTEditor) editorPart).setEditorData(serviceRegistry, syncCpp);
					}
				} catch (PartInitException e) {
					Activator.log.error(e);
				}
			}
		});
	}

	@Override
	protected Classifier getClassifierToEdit() {
		Classifier cl = super.getClassifierToEdit();
		if (StereotypeUtil.isApplied(cl, Activity.class)) {
			// activities are always within components
			return (Classifier) cl.getOwner();
		}
		return cl;
	}
}
