package org.eclipse.papyrus.robotics.ros2.cdteditor.sync;

import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.papyrus.robotics.core.commands.PortCommands;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.papyrus.robotics.ros2.cdteditor.Activator;
import org.eclipse.papyrus.robotics.ros2.reverse.ParamInfo;
import org.eclipse.papyrus.robotics.ros2.reverse.PortInfo;
import org.eclipse.papyrus.robotics.ros2.reverse.fromfile.ReverseParametersFromSource;
import org.eclipse.papyrus.robotics.ros2.reverse.utils.CreatePortUtils;
import org.eclipse.papyrus.robotics.ros2.reverse.utils.ServiceDefUtils;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

/**
 * Synchronize ports if a C++ body file back to the model
 */
public class SyncParametersToModel {

	/**
	 * @param clazz
	 *            a class or component
	 * @param ast
	 *            a C++ AST node
	 */
	public static void sync(Class clazz, IASTTranslationUnit ast) {
		ReverseParametersFromSource paramFromSource = new ReverseParametersFromSource(clazz, ast);

		Class paramSet = ParameterUtils.getParameterClass(clazz);
		paramFromSource.scanFunctions(ast);
		int i = 0;
		for (ParamInfo pi : paramFromSource.getParamInfos()) {
			Property paramUML = paramSet.getOwnedAttribute(pi.name, null);
			if (paramUML == null) {
				// check rename
				paramUML = paramSet.getOwnedAttributes().get(i);
				if (paramUML.getType() == pi.type) {
					// element at same position has same type => assume renaming
					paramUML.setName(pi.name);
				} else {
					// new element. Create
					paramUML = paramSet.createOwnedAttribute(pi.name, pi.type);
				}
			}
			StereotypeUtil.applyApp(paramUML, ParameterEntry.class);

			if (pi.defaultValue != null) {
				paramUML.setDefault(pi.defaultValue);
			}
			// TODO don't update description, it's not properly reversed
			// if (pi.description != null) {
			// entry.setDescription(pi.description);
			// }
			i++;
		}
	}

	/**
	 * Rename or create - rename an existing port, if a port with the passed
	 * service definition exists. Create a ew port otherwise
	 * 
	 * @param clazz
	 *            a class or component
	 * @param pi
	 *            the port info
	 * @param sd
	 *            the changed service definition
	 */
	public static void renameOrCreate(Class clazz, PortInfo pi, Interface sd) {
		// assume that no two operations are done at the same time (and that there is only one port
		// a certain service definition
		for (Port port : clazz.getOwnedPorts()) {
			Interface existingSD = InteractionUtils.getServiceDefinition(port);
			if (existingSD == sd) {
				// assume renaming;
				port.setName(pi.topic);
				return;
			}
		}
		CreatePortUtils.createPort(clazz, pi, sd);
	}

	/**
	 * Update the service definition of a port
	 * 
	 * @param clazz
	 *            a class or component
	 * @param port
	 *            an existing (UML2) port
	 * @param pi
	 *            the port info
	 * @param sd
	 *            the changed service definition
	 */
	public static void updateSD(Class clazz, Port port, PortInfo pi, Interface sd) {
		Class cs = (Class) port.getType(); // always assume that already well setup?
		try {
			PortCommands.removeProvReq(port).execute(null, null);
		} catch (ExecutionException e) {
			Activator.log.error(e);
		}
		if (ServiceDefUtils.isProvided(pi.pk)) {
			cs.createInterfaceRealization(null, sd);
		} else {
			cs.createUsage(sd);
		}
	}
}
