package org.eclipse.papyrus.robotics.ros2.cdteditor.sync;

import java.util.List;

import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.robotics.core.commands.PortCommands;
import org.eclipse.papyrus.robotics.core.utils.InteractionUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.ros2.cdteditor.Activator;
import org.eclipse.papyrus.robotics.ros2.reverse.PortInfo;
import org.eclipse.papyrus.robotics.ros2.reverse.fromfile.ReversePortsFromSource;
import org.eclipse.papyrus.robotics.ros2.reverse.utils.CreatePortUtils;
import org.eclipse.papyrus.robotics.ros2.reverse.utils.ServiceDefUtils;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Synchronize ports if a C++ body file back to the model
 */
public class SyncPortsToModel {

	/**
	 * @param clazz
	 *            a class or component
	 * @param pathMapURIs
	 *            a set of pathmaps to explore in order to retrieve service definitions
	 * @param node
	 *            a C++ AST node
	 */
	public static void sync(Class clazz, List<URI> pathMapURIs, IASTNode node, ITranslationUnit itu) {
		ReversePortsFromSource reversePorts = new ReversePortsFromSource(clazz, pathMapURIs, itu);
		reversePorts.scanFunctions(node);

		int i = 0;
		List<Port> ports = clazz.getOwnedPorts();
		for (PortInfo pi : reversePorts.getPortInfoList()) {
			String nameArray[] = pi.dtQName.split(Namespace.SEPARATOR);
			String pkgName = nameArray[0];
			String sdName = nameArray[2];
			Property existing = clazz.getOwnedAttribute(pi.topic, null);
			Port port = null;
			if (existing == null) {
				// name does not exist any more. Create or rename?
				if (i < ports.size()) {
					port = ports.get(i);
					port.setName(pi.topic);
				}
				else {
					Interface newSD = ServiceDefUtils.getServiceDef(clazz, pathMapURIs, pi.pk, pkgName, sdName);
					port = CreatePortUtils.createPort(clazz, pi, newSD);
				}
			} else if (existing instanceof Port) {
				// port already exists, check whether service definition is unchanged
				port = (Port) existing;
				Interface existingSD = InteractionUtils.getServiceDefinition(port);
				Interface newSD = ServiceDefUtils.getServiceDef(clazz, pathMapURIs, pi.pk, pkgName, sdName);
				// TODO: handle existingSD == null?

				if (newSD != existingSD) {
					updateSD(clazz, port, pi, newSD);
				}
			}
			if (port != null) {
				ComponentPort portSt = UMLUtil.getStereotypeApplication(port, ComponentPort.class);
				portSt.setQos(pi.qos);
			}
			i++;
		}
	}

	/**
	 * Update the service definition of a port
	 * 
	 * @param clazz
	 *            a class or component
	 * @param port
	 *            an existing (UML2) port
	 * @param pi
	 *            the port info
	 * @param sd
	 *            the changed service definition
	 */
	public static void updateSD(Class clazz, Port port, PortInfo pi, Interface sd) {
		Class cs = (Class) port.getType(); // always assume that already well setup?
		try {
			PortCommands.removeProvReq(port).execute(null, null);
		} catch (ExecutionException e) {
			Activator.log.error(e);
		}
		if (ServiceDefUtils.isProvided(pi.pk)) {
			cs.createInterfaceRealization(null, sd);
		} else {
			cs.createUsage(sd);
		}
	}
}
