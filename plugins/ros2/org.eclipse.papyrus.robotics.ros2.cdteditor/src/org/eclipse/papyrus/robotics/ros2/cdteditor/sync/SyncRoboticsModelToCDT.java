/*******************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - ansgar.radermacher@cea.fr CEA LIST - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.cdteditor.sync;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.papyrus.designer.infra.ui.UIProjectManagement;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen.FILE_KIND;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;
import org.eclipse.papyrus.designer.languages.cpp.codegen.Constants;
import org.eclipse.papyrus.infra.core.Activator;
import org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools;
import org.eclipse.papyrus.robotics.ros2.cdteditor.TextEditorConstants;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.jobs.CppGenJob;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Package;

/**
 * Main listener for model changes (registered via plugin.xml). It will delegate
 * to the sub-listeners for specific sub-elements (type, operation, port, ...) that
 * can be found in this package
 *
 */
public class SyncRoboticsModelToCDT {

	/**
	 * set to true, if a synchronization from an CDT editor to the model is active
	 */
	public static boolean syncFromEditor;

	public static IFile syncModelToCDT(Classifier classifier, String generatorID) {
		if (!(classifier instanceof Class) || classifier.eResource() == null) {
			return null;
		}

		final Package pkg = PackageUtil.getRootPackage((Class) classifier);

		Job job = new CppGenJob(pkg, UIProjectManagement.getCurrentProject());

		job.setUser(true);
		job.schedule();

		ILangCodegen codegen = LanguageCodegen.getGenerator(TextEditorConstants.CPP, generatorID);
		String projectName = PackageTools.getProjectName(pkg);
		IProject targetProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);

		IContainer srcPkg = null;
		IFile cppFile = null;
		try {

			String suffix = codegen.getSuffix(FILE_KIND.BODY);

			cppFile = targetProject.getFile(new Path(codegen.getFileName(targetProject, classifier) + Constants.DOT + suffix));

			// IStorage storage = new TextStorage(string);
		} finally {
			// Refresh the container for the newly created files. This needs to be done even
			// during error because of the possibility for partial results.
			try {
				if (srcPkg != null) {
					srcPkg.refreshLocal(IResource.DEPTH_INFINITE, null);
				}
			} catch (CoreException e) {
				Activator.log.error(e);
			}
		}
		return cppFile;
	}
}
