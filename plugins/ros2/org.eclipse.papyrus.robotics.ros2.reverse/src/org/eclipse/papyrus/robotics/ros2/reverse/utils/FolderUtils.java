/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.utils;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.papyrus.robotics.core.utils.FolderNames;
import org.eclipse.papyrus.robotics.ros2.reverse.Activator;

public class FolderUtils {
	public static IFolder createFolderStructure(IProject project) {
		// project.open(progressMonitor);
		IFolder fModels = getOrCreate(project, FolderNames.MODELS);
		getOrCreate(fModels, FolderNames.COMPONENTS);
		getOrCreate(fModels, FolderNames.SYSTEM);
		return fModels;
	}

	public static IFolder getOrCreate(IContainer parent, String name) {
		IFolder folder = parent.getFolder(new Path(name));
		if (!folder.exists()) {
			try {
				folder.create(false, true, null);
			} catch (CoreException e) {
				Activator.log.error(e);
			}
		}
		return folder;
	}

	public static IFolder getComponentFolder(IFolder fModels) {
		return getOrCreate(fModels, FolderNames.COMPONENTS);

	}
	
	/**
	 * Obtain an existing project (and thus the presumed package) based on
	 * the assumption that the filename of the component definition corresponds
	 * to the node name that is published.
	 * 
	 * @param fileName
	 *            the filename of a component definition
	 * @return the obtained project name or null, if none found
	 */
	public static IProject obtainProject(String fileName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		for (IProject project : root.getProjects()) {
			IPath path = new Path("/models/components/" + fileName);
			IFile file = project.getFile(path);
			if (file.exists()) {
				return project;
			}
			// also check for di file (location will still be found, even if
			// .uml file is deleted in order to force that it will be overwritten)
			String diFileName = fileName.substring(0, fileName.length() - ".uml".length()) + ".di";
			path = new Path("/models/components/" + diFileName);
			file = project.getFile(path);
			if (file.exists()) {
				return project;
			}
		}
		return null;
	}

}
