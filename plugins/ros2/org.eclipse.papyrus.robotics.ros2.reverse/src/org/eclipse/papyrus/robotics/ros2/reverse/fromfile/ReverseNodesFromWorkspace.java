/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.fromfile;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.robotics.ros2.reverse.Activator;

/**
 * Reverse a whole ROS 2 workspace from source
 */
public class ReverseNodesFromWorkspace {

	protected static final String CPP = ".cpp"; //$NON-NLS-1$

	public static final String SRC = "src"; //$NON-NLS-1$

	protected IProject project;

	protected IProgressMonitor monitor;

	
	public ReverseNodesFromWorkspace(IProject project) {
		this.project = project;
	}

	public void reverseWorkspace(IProgressMonitor monitor) {
		this.monitor = monitor;
		try {
			// IFolder srcFolder = folder.getFolder(SRC);
			// IProject srcFolder = folder;
			// if (srcFolder != null) {
				for (IResource rosPkg : project.members()) {
					if (rosPkg instanceof IFolder) {
						reverseRosPkg((IFolder) rosPkg);
					}
				}
			// }
		} catch (CoreException e) {
			Activator.log.error(e);
		}
	}

	public void reverseRosPkg(IFolder rosPkg) throws CoreException {
		IFolder srcFolder = rosPkg.getFolder(SRC);
		if (srcFolder != null && srcFolder.exists()) {
			scanFiles(srcFolder);
		}
	}
	
	public void scanFiles(IFolder srcFolder) throws CoreException {
		for (IResource member : srcFolder.members()) {
			if (monitor.isCanceled()) {
				break;
			}
			if (member instanceof IFolder) {
				scanFiles((IFolder) member);
			}
			else if (member instanceof IFile && member.getName().endsWith(CPP)) {
				monitor.subTask(String.format("analyze file %s", member.getName()));
				monitor.worked(1);
				ReverseNodeFromSource rn = new ReverseNodeFromSource((IFile) member);
				rn.reverseNode(monitor, project);
			}
		}
	}
}