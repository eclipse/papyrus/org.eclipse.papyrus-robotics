/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.ros2.reverse.fromfile.ReverseNodeFromSource;
import org.eclipse.ui.PlatformUI;

public class ReverseNodeFromFileHandler extends AbstractHandler {

	protected IFile file;

	@Override
	public boolean isEnabled() {
		// Retrieve selected elements
		IStructuredSelection selection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		Object selected = selection.getFirstElement();
		if (selected instanceof IAdaptable) {
			selected = ((IAdaptable) selected).getAdapter(IFile.class);		}
		if (selected instanceof IFile) {
			file = (IFile) selected;
			String extension = file.getFileExtension().toLowerCase();
			if (extension.equals("cpp") || extension.equals("cc")) {  //$NON-NLS-1$//$NON-NLS-2$
				return true;
			}
		}
		return false;
	}

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		Job job = new Job("Reverse node from file") { //$NON-NLS-1$
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				ReverseNodeFromSource reverseFromSource =
						new ReverseNodeFromSource(file);
				reverseFromSource.reverseNode(monitor);
				return Status.OK_STATUS;
			}
		};
		job.schedule();

		return null;
	}
}
