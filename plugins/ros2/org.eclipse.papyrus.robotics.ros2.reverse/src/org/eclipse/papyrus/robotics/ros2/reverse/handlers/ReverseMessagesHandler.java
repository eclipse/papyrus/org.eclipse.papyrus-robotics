/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.core.utils.FolderNames;
import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils;
import org.eclipse.papyrus.robotics.ros2.reverse.fromsys.ReverseMessages;
import org.eclipse.papyrus.robotics.ros2.reverse.utils.FolderUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class ReverseMessagesHandler extends AbstractHandler {

	protected IContainer container;

	@Override
	public boolean isEnabled() {
		// Retrieve selected elements
		IStructuredSelection selection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		Object selected = selection.getFirstElement();
		if (selected instanceof IAdaptable) {
			// adaptation is required for selections in project explorer
			selected = ((IAdaptable) selected).getAdapter(IContainer.class);
		}
		if (selected instanceof IContainer) {
			container = (IContainer) selected;
			return true;
		}
		return false;
	}

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		final Shell shell = Display.getCurrent().getActiveShell();
		boolean sourceSh = MessageDialog.openQuestion(shell, "Add workspace to path?",
				"Do you want to \"source\" an additional setup.sh script" +
				"before starting the reverse (typically in the install folder of a ROS 2 workspace)");
		if (sourceSh) {
			EnvironmentUtils.selectAndSourceScript(container.getLocation().toString(), shell);
		}
		Job job = new Job("Reverse ROS messages and services") { //$NON-NLS-1$
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				IFolder folder = null;
				if (container instanceof IProject) {
					IFolder models = FolderUtils.getOrCreate((IProject) container, FolderNames.MODELS);
					folder = FolderUtils.getOrCreate(models, FolderNames.SERVICES);
				}
				else if (container instanceof IFolder) {
					folder = (IFolder) container;
				}
				if (folder != null) {
					new ReverseMessages(folder).reverseMessages(monitor);
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();

		return null;
	}
}
