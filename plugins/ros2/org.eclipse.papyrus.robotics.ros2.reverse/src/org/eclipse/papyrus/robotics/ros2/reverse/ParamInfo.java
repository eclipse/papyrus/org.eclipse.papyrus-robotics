/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse;

import org.eclipse.uml2.uml.Type;

/**
 * Store information about a found parameter
 */
public class ParamInfo {
	
	public String name;
	public Type type;
	public boolean isList;
	public String defaultValue;
	public String description;
}
