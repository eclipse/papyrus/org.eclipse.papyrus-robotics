package org.eclipse.papyrus.robotics.ros2.reverse.utils;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.papyrus.robotics.core.utils.PortUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.ros2.reverse.PortInfo;
import org.eclipse.papyrus.uml.diagram.wizards.Activator;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;

/**
 * 
 *
 */
public class CreatePortUtils {

	/**
	 * Create a UML port ...
	 * 
	 * @param component
	 *            the component/class for which to create a port
	 * @param pi
	 *            Port information
	 * @param sd
	 *            the service definition
	 * @return the created port
	 */
	public static Port createPort(Class component, PortInfo pi, Interface sd) {
		try {
			Port port = component.createOwnedPort(pi.topic, null);
			ReverseUtils.setXmlID(port);
			
			port.setAggregation(AggregationKind.COMPOSITE_LITERAL);
			ComponentPort portSt = StereotypeUtil.applyApp(port, ComponentPort.class);
			ICommand csCmd = PortUtils.associateCSCommand(port);
			csCmd.execute(null, null);
			Class cs = (Class) port.getType();
			if (sd != null) {
				if (ServiceDefUtils.isProvided(pi.pk)) {
					cs.createInterfaceRealization(null, sd);
				} else {
					cs.createUsage(sd);
				}
			}
			portSt.setQos(pi.qos);
			return port;
		} catch (ExecutionException e) {
			Activator.log.error(e);
		}
		return null;
	}
}
