/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.papyrus.robotics.ros2.reverse.fromsys.ReverseNodes;

public class ReverseNodesHandler extends AbstractHandler {

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		Job job = new Job("Reverse ROS nodes") { //$NON-NLS-1$
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				new ReverseNodes(monitor).readNodeList();
				return Status.OK_STATUS;
			}
		};
		job.schedule();

		return null;
	}
}
