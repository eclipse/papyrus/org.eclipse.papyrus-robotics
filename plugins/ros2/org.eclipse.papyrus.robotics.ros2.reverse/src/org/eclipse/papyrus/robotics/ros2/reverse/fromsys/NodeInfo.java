/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.fromsys;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.robotics.ros2.reverse.PortInfo;
import org.eclipse.uml2.uml.Class;

public class NodeInfo {
	String name;
	Class type;
	List<PortInfo> ports = new ArrayList<PortInfo>();
}
