/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.utils.ProjectTools;
import org.eclipse.papyrus.robotics.ros2.reverse.fromfile.ReverseNodesFromWorkspace;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class ReverseNodesFromWorkspaceHandler extends AbstractHandler {

	protected IProject project;

	@Override
	public boolean isEnabled() {
		// Retrieve selected elements
		IStructuredSelection selection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();

		Object selected = selection.getFirstElement();
		if (selected instanceof IAdaptable) {
			selected = ((IAdaptable) selected).getAdapter(IProject.class);
		}
		if (selected instanceof IProject) {
			project = (IProject) selected;
			return true;
		}
		return false;
	}

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		final Shell shell = Display.getCurrent().getActiveShell();
		final boolean configureCDT =
			MessageDialog.openQuestion(shell, "Configure CDT project?",
				"Do you want to configure this project with CDT settings corresponding to the current setup?");
			
		Job job = new Job("Reverse node from workspace") { //$NON-NLS-1$
			@Override
			protected IStatus run(final IProgressMonitor monitor) {
				
				if (configureCDT) {
					TransformationContext.monitor = monitor;
					ProjectTools.configureCDT(project, project.getName());
				}
				ReverseNodesFromWorkspace reverseFromSource =
						new ReverseNodesFromWorkspace(project);
				reverseFromSource.reverseWorkspace(monitor);
				return Status.OK_STATUS;
			}
		};
		job.schedule();

		return null;
	}
}
