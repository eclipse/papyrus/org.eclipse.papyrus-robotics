/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.utils;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.core.utils.ScanUtils;
import org.eclipse.papyrus.robotics.library.advice.ActionCommPatternAdvice;
import org.eclipse.papyrus.robotics.library.advice.PubSubCommPatternAdvice;
import org.eclipse.papyrus.robotics.library.advice.QueryCommPatternAdvice;
import org.eclipse.papyrus.robotics.ros2.base.Ros2Constants;
import org.eclipse.papyrus.robotics.ros2.reverse.PortInfo.PortKind;
import org.eclipse.papyrus.robotics.ros2.reverse.fromsys.MessageParser.MessageEntry;
import org.eclipse.papyrus.uml.diagram.wizards.Activator;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;

public class ServiceDefUtils {
	/**
	 * Retrieve a service definition from the resource set of the component
	 * 
	 * @param component  a component (only used to determine resourceSet)
	 * @param portKind   the provider kind, i.e. PUBLISHER, ...
	 * @param pkgName    the ROS 2 package name
	 * @param svcDefName the name of the service definition. The function prefixes
	 *                   it with P_, Q_, A_, U_ depending on the providerKind. It
	 *                   also tries without prefix, if the service definition cannot
	 *                   be found
	 * @return the service definition, if it can be found or null
	 */
	public static Interface getServiceDef(Class component, PortKind portKind, String pkgName, String svcDefName) {
		String prefix = ""; //$NON-NLS-1$
		if (portKind == PortKind.PUBLISHER || portKind == PortKind.SUBSCRIBER) {
			prefix = PubSubCommPatternAdvice.PREFIX;
		} else if (portKind == PortKind.SERVER || portKind == PortKind.CLIENT) {
			prefix = QueryCommPatternAdvice.PREFIX;
		} else if (portKind == PortKind.ACTION_SRV || portKind == PortKind.ACTION_CLI) {
			prefix = ActionCommPatternAdvice.PREFIX;
		}
		String qName = pkgName + NamedElement.SEPARATOR + Ros2Constants.SVCDEFS + NamedElement.SEPARATOR + prefix
				+ svcDefName;
		NamedElement ne = ElementUtils.getQualifiedElementFromRS(component, qName);
		if (ne instanceof Interface) {
			return (Interface) ne;
		}
		// also try without prefixing
		qName = pkgName + NamedElement.SEPARATOR + Ros2Constants.SVCDEFS + NamedElement.SEPARATOR + svcDefName;
		ne = ElementUtils.getQualifiedElementFromRS(component, qName);
		if (ne instanceof Interface) {
			return (Interface) ne;
		}
		return null;
	}

	/**
	 * Convenience function to retrieve the service definition from a given message
	 * entry
	 *
	 * @param component   a component (only used to determine resourceSet)
	 * @param pathMapURIs a list of URIs accessible via pathmaps (that store service
	 *                    definitions)
	 * @param portKind    the provider kind, i.e. PUBLISHER, ...
	 * @param entry       a message entry
	 * @return the service definition, if it can be found or null
	 */
	public static Interface getServiceDef(Class component, List<URI> pathMapURIs, PortKind portKind,
			MessageEntry entry) {
		return getServiceDef(component, pathMapURIs, portKind, entry.pkgName, entry.name);
	}

	/**
	 * Retrieve a service definition from a given service definition and package
	 * name
	 *
	 * @param component   a component (only used to determine resourceSet)
	 * @param pathMapURIs a list of URIs accessible via pathmaps (that store service
	 *                    definitions)
	 * @param portKind    the provider kind, i.e. PUBLISHER, ...
	 * @param pkgName     the ROS 2 package name
	 * @param svcDefName  the service definition name
	 * @return the service definition, if it can be found or null
	 */
	public static Interface getServiceDef(Class component, List<URI> pathMapURIs, PortKind portKind, String pkgName,
			String svcDefName) {
		Interface sd = ServiceDefUtils.getServiceDef(component, portKind, pkgName, svcDefName);
		if (sd == null) {
			// check, if the model representing the ROS 2 resource is already in the
			// resource set
			if (ElementUtils.getQualifiedElementFromRS(component, pkgName) == null) {
				// not loaded => load and retry to get service definition
				String fileName = ReverseUtils.fileName(pkgName);
				// load message set from registered packages
				// Cannot use the load function in ReverseUtils, since this loads into the
				// "wrong" resource set
				for (URI pathURI : pathMapURIs) {
					if (pathURI.toString().endsWith(fileName)) {
						component.eResource().getResourceSet().getResource(pathURI, true);
						sd = ServiceDefUtils.getServiceDef(component, portKind, pkgName, svcDefName);
						break;
					}
				}
				if (sd == null) {
					// now scan for all packages in the workspace
					URI pathURI = ScanUtils.firstModelInWorkspace(fileName);
					if (pathURI != null) {
						component.eResource().getResourceSet().getResource(pathURI, true);
						sd = ServiceDefUtils.getServiceDef(component, portKind, pkgName, svcDefName);
					}
				}
			}
			if (sd == null) {
				Activator.log.debug(String.format("Cannot find service definition for %s/%s.", pkgName, svcDefName));
				return null;
			}
		}
		return sd;
	}

	public static boolean isProvided(PortKind portKind) {
		return portKind == PortKind.PUBLISHER || portKind == PortKind.SERVER || portKind == PortKind.ACTION_SRV;
	}
}