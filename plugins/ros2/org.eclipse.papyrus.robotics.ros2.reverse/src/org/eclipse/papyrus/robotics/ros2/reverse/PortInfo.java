/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse;

public class PortInfo {
	
	public enum PortKind {
		PUBLISHER, SUBSCRIBER, CLIENT, SERVER, ACTION_CLI, ACTION_SRV
	}
	
	public PortKind pk;
	public String dtQName;
	public String qos;
	public String topic;
}
