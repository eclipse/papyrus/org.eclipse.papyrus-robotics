/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.fromsys;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.robotics.ros2.reverse.Activator;
import org.eclipse.uml2.uml.DataType;

public class MessageParser {
	private static final String HASH = "#"; //$NON-NLS-1$
	// skip an arbitrary number of whitespace
	private static final String WS_REG = "\\s+"; //$NON-NLS-1$

	public static final String SLASH = "/"; //$NON-NLS-1$

	public static final String COLON = ":"; //$NON-NLS-1$

	private static final String ASSIGN = "="; //$NON-NLS-1$

	/**
	 * Remove comment from a line
	 * 
	 * @param line
	 *            a line (of a message file)
	 * @return the passed line without the comment
	 */
	public static String filterComment(String line) {
		int hash = line.indexOf(HASH);
		if (hash >= 0) {
			// comment after some declarations
			line = line.substring(0, hash).trim();
		}
		return line;
	}

	/**
	 * Get a comment from a line
	 * 
	 * @param line
	 *            a line (of a message file)
	 * @return the extracted comment or null
	 */
	public static String extractComment(String line) {
		int hash = line.indexOf(HASH);
		if (hash >= 0) {
			// comment after some declarations
			return line.substring(hash + 1).trim();
		}
		return null;
	}

	public static class NameType {
		public String name;
		public String typePkg;
		public String typeName;
		public Integer upper;
		public String defaultValue;
	}

	public static class MessageEntry {
		public String pkgName;
		public String type;
		public String name;
		List<DataType> commObjects = new ArrayList<DataType>();
	}

	public static MessageEntry extractMessageEntry(String line) {
		String frags[] = line.split(MessageParser.SLASH);
		if (frags.length == 3) {
			MessageEntry msg = new MessageEntry();
			msg.pkgName = frags[0].trim();
			msg.type = frags[1].trim(); // msg, srv or action
			msg.name = frags[2].trim();
			return msg;
		}
		return null;
	}

	public static NameType extractNameType(String line) {
		String frags[] = line.split(WS_REG); // use an arbitrary number of white-space as delimiter between name and type
		if (frags.length >= 2) {
			NameType nt = new NameType();
			String type = frags[0];
			if (type.contains(SLASH)) {
				String fragsPkg[] = type.split(SLASH);
				nt.typePkg = fragsPkg[0];
				nt.typeName = fragsPkg[1];
			} else {
				nt.typeName = type;
			}
			if (nt.typeName.contains("<")) {
				// ROS2 definition may limit the size of a type (only string?), e.g. string<=255.
				// This information is not captured in the model, the size hint needs to be stripped
				int pos = nt.typeName.indexOf("<");
				nt.typeName = nt.typeName.substring(0, pos);
			}
			nt.name = frags[1];

			setUpper(nt);

			if (line.contains(ASSIGN)) {
				// value assignment => constant
				String valueStr = line.substring(line.indexOf(ASSIGN) + 1);
				nt.defaultValue = valueStr.trim();
				// remove default value from name
				int assign = nt.name.indexOf(ASSIGN);
				if (assign > 0) {
					nt.name = nt.name.substring(0, assign).trim();
				}
			}
			return nt;
		}
		return null;
	}

	/**
	 * Update upper information in passed NT structure
	 * 
	 * @param nt
	 */
	public static void setUpper(NameType nt) {
		String[] ext = nt.typeName.split("\\["); //$NON-NLS-1$
		if (ext.length > 1) {
			int upper;
			nt.typeName = ext[0];
			if (ext[1].equals("]")) { //$NON-NLS-1$
				upper = -1;
			} else {
				upper = extractNumber(ext[1]);
			}
			nt.upper = upper;
		}
	}

	/**
	 * Extract a number from a string. Waits for the first digit, then stops after the next non-digit character
	 * 
	 * @param str
	 *            a string
	 * @return an integer number (0 in case of a number exception)
	 */
	public static int extractNumber(String str) {
		String filteredStr = ""; //$NON-NLS-1$
		boolean first = true;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (Character.isDigit(c)) {
				filteredStr += c;
				first = false;
			} else if (!first) {
				break;
			}
		}
		try {
			return Integer.parseInt(filteredStr);
		} catch (NumberFormatException e) {
			Activator.log.error(e);
			return 0;
		}
	}
}
