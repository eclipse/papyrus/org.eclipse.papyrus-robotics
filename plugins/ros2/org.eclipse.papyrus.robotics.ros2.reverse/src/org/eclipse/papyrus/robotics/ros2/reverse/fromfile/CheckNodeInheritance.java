/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.reverse.fromfile;

import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTNodeSelector;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.dom.ast.IBinding;
import org.eclipse.cdt.core.dom.ast.IType;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPBase;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPClassType;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPMember;
import org.eclipse.cdt.core.model.CModelException;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.IMethod;
import org.eclipse.cdt.core.model.INamespace;
import org.eclipse.cdt.core.model.IParent;
import org.eclipse.cdt.core.model.ISourceRange;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.papyrus.designer.deployment.tools.Activator;

/**
 * Check, whether the class defined in the translation extends Node or LifecycleNode
 */
public class CheckNodeInheritance {

	protected static final String LIFECYCLE_NODE = "rclcpp_lifecycle::LifecycleNode"; //$NON-NLS-1$

	protected static final String NODE = "rclcpp::Node"; //$NON-NLS-1$

	protected IASTNodeSelector selector;

	public CheckNodeInheritance(IASTTranslationUnit ast) {
		selector = ast.getNodeSelector(null);
	}

	public boolean isNode(ITranslationUnit itu) {
		try {
			ICPPClassType classType = findClass(itu);
			if (classType != null) {
				return isNode(classType);
			}
		} catch (CModelException e) {
			Activator.log.error(e);
		}
		return false;
	}

	/**
	 * Obtain class from ITU or namespace. The class finds the first method and then obtains the
	 * class from its declaration
	 *
	 * @param parent
	 *            a translation unit or namespace
	 * @return the found class or null, if no method or the associated association is not found
	 * @throws CModelException
	 */
	public ICPPClassType findClass(IParent parent) throws CModelException {
		ICPPClassType classType = null;
		for (ICElement child : parent.getChildren()) {
			if (child instanceof INamespace) {
				// recurse into namespace
				classType = findClass((INamespace) child);
			} else if (child instanceof IMethod) {
				IMethod method = (IMethod) child;
				ISourceRange range = method.getSourceRange();
				IASTNode node = selector.findEnclosingNode(range.getStartPos(), range.getLength());

				if (node instanceof IASTFunctionDefinition) {
					IASTFunctionDefinition fctDef = (IASTFunctionDefinition) node;
					IBinding binding = fctDef.getDeclarator().getName().resolveBinding();
					if (binding instanceof ICPPMember) {
						classType = ((ICPPMember) binding).getClassOwner();
					}
				}
				if (classType != null) {
					return classType;
				}
			}
		}
		return classType;
	}

	/**
	 * Return true, if the passed classe inherits directly or indirectly
	 * from Node or LifeCycleNode
	 * 
	 * @param classType
	 *            an AST representation of a class
	 * @return true, if Node or LifeCycleNode
	 */
	public boolean isNode(ICPPClassType classType) {
		for (ICPPBase base : classType.getBases()) {
			IType baseClass = base.getBaseClassType();
			if (baseClass.toString().equals(LIFECYCLE_NODE) ||
					baseClass.toString().equals(NODE)) {
				return true;
			}
			// TODO: name based check is not reliable
			if (baseClass instanceof ICPPClassType) {
				boolean isNode = isNode((ICPPClassType) baseClass);
				if (isNode) {
					return true;
				}
			}
		}
		return false;
	}
}
