package org.eclipse.papyrus.robotics.ros2.reverse.utils;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.infra.core.services.ExtensionServicesRegistry;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServiceMultiException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationModel;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.papyrus.robotics.ros2.reverse.Activator;
import org.eclipse.papyrus.uml.diagram.wizards.command.InitFromTemplateCommand;
import org.eclipse.papyrus.uml.diagram.wizards.command.NewPapyrusModelCommand;
import org.eclipse.papyrus.uml.tools.model.UmlModel;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.uml2.uml.Package;

public class ModelTemplate {
	ServicesRegistry registry;

	ModelSet modelSet;

	/**
	 * Create a new model template, based on
	 * 
	 * @param uri       the URI of the model to copy from the template
	 * @param extension the used P4R extension, e.g. compdef or service
	 * @throws ServiceException
	 */
	public ModelTemplate(URI uri, String extension) throws ServiceException {
		registry = new ExtensionServicesRegistry(org.eclipse.papyrus.infra.core.Activator.PLUGIN_ID);
		registry.startServicesByClassKeys(ModelSet.class);

		modelSet = registry.getService(ModelSet.class);
		TransactionalEditingDomain domain = modelSet.getTransactionalEditingDomain();

		RecordingCommand command = new NewPapyrusModelCommand(modelSet, uri);
		domain.getCommandStack().execute(command);

		String templateName = "templates/robotics." + extension;
		InitFromTemplateCommand tp = new InitFromTemplateCommand(modelSet.getTransactionalEditingDomain(), modelSet,
				"org.eclipse.papyrus.robotics.wizards", templateName + ".uml", templateName + ".notation",
				templateName + ".di");
		domain.getCommandStack().execute(tp);
	}

	public Package getUMLModel() {
		UmlModel umlModel = UmlUtils.getUmlModel(modelSet);
		try {
			return (Package) umlModel.lookupRoot();
		} catch (NotFoundException e) {
			Activator.log.error(e);
		}
		return null;
	}

	public void save(IProgressMonitor progressMonitor) {
		try {
			modelSet.save(progressMonitor);
		} catch (IOException e) {
			Activator.log.error(e);
		}
	}

	public TransactionalEditingDomain getDomain() {
		return modelSet.getTransactionalEditingDomain();
	}

	public void executeCmd(Command command) {
		getDomain().getCommandStack().execute(command);
	}

	public NotationModel getNotationModel() {
		return NotationUtils.getNotationModel(modelSet);
	}

	public void dispose() throws ServiceMultiException {
		registry.disposeRegistry();
	}
}
