/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * Log errors (and output) to the ROS 2 console
 */
public class ProcessUtils {

	public static final String ROS2_CON = "ROS 2"; //$NON-NLS-1$

	public enum LogKind {
		ERRORS,
		OUTPUT,
		ALL
	}

	/**
	 * evaluate and log an error messages
	 * 
	 * @param p
	 *            a process
	 * @return true, if an error message exists (in this case, it gets logged)
	 */
	public static boolean logErrors(Process p) {
		return logProcess(p, LogKind.ERRORS);
	}

	public static boolean logProcess(Process p, LogKind kind) {
		boolean hasErrors = false;
		try {
			MessageConsole mc = findConsole(ROS2_CON);
			MessageConsoleStream conOut = mc.newMessageStream();
			String msg;
			if (kind == LogKind.ERRORS || kind == LogKind.ALL) {
				BufferedReader errors = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				while (errors.ready() && (msg = errors.readLine()) != null) {
					conOut.println(msg);
					if (!hasErrors) hasErrors = true;
				}
				errors.close();
			}
			if (kind == LogKind.OUTPUT || kind == LogKind.ALL) {
				BufferedReader outputs = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while (outputs.ready() && (msg = outputs.readLine()) != null) {
					conOut.println(msg);
				}
				outputs.close();
			}
			conOut.close();
		} catch (IOException e) {
			Activator.log.error(e);
		}
		return hasErrors;
	}

	public static void writeToConsole(String info) {
		MessageConsole mc = findConsole(ROS2_CON);
		MessageConsoleStream conOut = mc.newMessageStream();
		conOut.println(info);
		try {
			conOut.close();
		} catch (IOException e) {
			Activator.log.error(e);
		}
	}
	
	/**
	 * Find a named console
	 * 
	 * @param name
	 *            the named console
	 * @return an existing or new console
	 */
	public static MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++) {
			if (name.equals(existing[i].getName())) {
				return (MessageConsole) existing[i];
			}
		}
		// no console found, so create a new one
		MessageConsole ros2Console = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { ros2Console });
		return ros2Console;
	}

}
