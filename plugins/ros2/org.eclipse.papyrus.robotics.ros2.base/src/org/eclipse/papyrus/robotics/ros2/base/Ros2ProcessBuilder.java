package org.eclipse.papyrus.robotics.ros2.base;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Ros2ProcessBuilder {

	private static final String ROS2 = "ros2"; //$NON-NLS-1$
	
	ProcessBuilder pb;

	public Ros2ProcessBuilder(String... cmdParams) {
		pb = new ProcessBuilder();
		String ros2 = EnvironmentUtils.getFromPath(ROS2);
		if (ros2 == null) {
			// don't throw an exception, otherwise the tests (running on a machine
			// without ROS 2 installed) are failing. Fall back to ros2, even if the
			// command cannot be executed in that case.
			ros2 = ROS2;
		}
		pb.command().add(ros2);
		for(String cmdParam : cmdParams) {
			pb.command().add(cmdParam);
		}
		Map<String, String> localEnv = EnvironmentUtils.getenv();
		Map<String, String> pbEnv = pb.environment();
		for (String key : localEnv.keySet()) {
			pbEnv.put(key, localEnv.get(key));
		}
	}

	public Process start() throws IOException {
		return pb.start();
	}

	public List<String> command() {
		return pb.command();
	}
	
	public Map<String, String> environment() {
		return pb.environment();
	}
}
