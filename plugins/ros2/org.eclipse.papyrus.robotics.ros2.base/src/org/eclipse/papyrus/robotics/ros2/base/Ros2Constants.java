/*****************************************************************************
 * Copyright (c) 2020, 2024 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *  Matteo MORELLI      matteo.morelli@cea.fr - #12/Simplify the process to execute a system task
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.base;

@SuppressWarnings("nls")
public class Ros2Constants {

	// ros2 command line tool
	public static final String ROS2 = "ros2";

	// ros2 message command option
	public static final String PKG = "pkg";
	// ros2 service command option
	public static final String MSG = "msg";
	// ros2 service command option
	public static final String SRV = "srv";
	// ros2 service command option
	public static final String NODE = "node";
	// ros2 interface command
	public static final String INTF = "interface";
	// ros2 lifecycle command
	public static final String LIFECYCLE = "lifecycle";
	// ros2 launch command
	public static final String LAUNCH = "launch";
	// ros2 param command
	public static final String PARAM = "param";
	// ros2 action marker
	public static final String ACTION = "action";

	// ros2 list command option
	public static final String PKGS = "packages";
	// ros2 list command option
	public static final String LIST = "list";
	// ros2 info command option
	public static final String INFO = "info";
	// ros2 life-cycle get command option
	public static final String GET = "get";
	// ros2 life-cycle set command option
	public static final String SET = "set";
	// ros2 action send_goal command option
	public static final String SEND_GOAL = "send_goal";
	// ros2 show command option
	public static final String SHOW = "show";
	// ros2 describe command option
	public static final String DESCRIBE = "describe";
	// ros2 parameter type option
	public static final String PARAM_TYPE_OPT = "--param-type";
	
	// request
	public static final String REQ = "Req";
	// response / result (also used for actions)
	public static final String RES = "Res";

	// goal, result (same Constant as for REQ/RES), feedback
	public static final String GOAL = "Goal";
	// feedback
	public static final String FEEDBACK = "Feedback";

	// folder where to store service definitions
	public static final String SVCDEFS = "svcdefs";
	
	// environment variables
	public static final String AMENT_PREFIX_PATH = "AMENT_PREFIX_PATH";
	public static final String CMAKE_PREFIX_PATH = "CMAKE_PREFIX_PATH";
	public static final String PYTHON_PATH = "PYTHONPATH";
}
