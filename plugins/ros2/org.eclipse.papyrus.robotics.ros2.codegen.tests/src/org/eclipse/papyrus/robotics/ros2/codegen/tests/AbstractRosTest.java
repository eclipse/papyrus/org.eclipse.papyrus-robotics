/*******************************************************************************
 * Copyright (c) 2020 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.tests;

import org.eclipse.core.resources.IProject;

public class AbstractRosTest {
	public static final String SIMPLE_MSGS = "simple_msgs"; //$NON-NLS-1$
	
	protected IProject simpleProject;

	protected LwTransformationTestSupport tts;
}
