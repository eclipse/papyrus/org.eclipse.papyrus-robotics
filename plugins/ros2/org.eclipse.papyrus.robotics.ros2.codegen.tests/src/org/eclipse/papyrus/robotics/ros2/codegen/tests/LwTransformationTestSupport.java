/*******************************************************************************
 * Copyright (c) 2020 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.tests;

import static org.hamcrest.MatcherAssert.assertThat;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.papyrus.designer.languages.common.base.TestInfo;
import org.eclipse.papyrus.designer.languages.common.testutils.FileComparison;
import org.eclipse.papyrus.designer.languages.common.testutils.RecursiveCopy;
import org.eclipse.papyrus.designer.languages.common.testutils.TestConstants;
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain;
import org.eclipse.papyrus.junit.utils.rules.AbstractHouseKeeperRule;
import org.eclipse.uml2.uml.Package;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Variant of the TransformationTestSUpport in SW designer. This variant does
 * not require a PapyrusEditorFixture
 */
public class LwTransformationTestSupport {

	static {
		// This system property avoids opening dialogs during Papyrus operations. It must
		// be set before trying to load any of the Papyrus classes.
		System.setProperty(TestInfo.PAPYRUS_RUN_HEADLESS, Boolean.TRUE.toString());
	}

	/**
	 * The house keeper
	 */
	protected AbstractHouseKeeperRule houseKeeper;

	/**
	 * The project containing the expected results
	 */
	protected IProject modelProject;

	/**
	 * The test class (used to get access to its bundle)
	 */
	Class<?> testClass;

	public LwTransformationTestSupport(Class<?> testClass, AbstractHouseKeeperRule houseKeeper, IProject modelProject) {
		this.testClass = testClass;
		this.houseKeeper = houseKeeper;
		this.modelProject = modelProject;
	}

	/**
	 * Run a transformation with a given deployment plan
	 *
	 * @param nePkg
	 *            a deployment plan or package with transformation stereotype
	 *            within the provided example
	 */
	public void runTransformation(Package nePkg) {

		final int genOptions = 0;
		new ExecuteTransformationChain(nePkg, modelProject).executeTransformation(new NullProgressMonitor(), genOptions);
	}

	/**
	 * Validate the results
	 * 
	 * @param genProject
	 *            the project into which generation has been done;
	 * @param folderInTestBundle
	 *            The folder within the source bundle containing the expected results
	 * @param srcGen
	 *            The folder name containing the generated code (a folder of the same name is created within the model project to facilitate the comparison)
	 */
	public void validateResults(IProject genProject, String folderInTestBundle, String srcGen) {
		RecursiveCopy copier = new RecursiveCopy(houseKeeper);
		Bundle srcBundle = FrameworkUtil.getBundle(testClass);
		// copy expected results folder to model project
		copier.copy(srcBundle, folderInTestBundle, modelProject, TestConstants.EXPECTED_RESULT);

		IContainer expectedSrcGen = modelProject.getFolder(TestConstants.EXPECTED_RESULT);
		assertThat("expected source folder must exist", expectedSrcGen.exists()); //$NON-NLS-1$
		IContainer generatedSrcGen = srcGen.equals(".") ? genProject : genProject.getFolder(srcGen); //$NON-NLS-1$
		assertThat("generated source folder must exist", generatedSrcGen.exists()); //$NON-NLS-1$

		FileComparison.assertGeneratedMatchesExpected(generatedSrcGen, expectedSrcGen);
	}
}
