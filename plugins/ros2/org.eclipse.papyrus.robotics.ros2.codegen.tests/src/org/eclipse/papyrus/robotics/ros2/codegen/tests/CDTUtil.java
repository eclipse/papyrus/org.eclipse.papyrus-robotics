package org.eclipse.papyrus.robotics.ros2.codegen.tests;

import static org.junit.Assert.fail;

import org.eclipse.cdt.core.CCProjectNature;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;

/**
 * Simple utility class adding the C++ nature
 */
public class CDTUtil {
	public static void addCppNature(IProject project) {
		try {
			IProjectDescription description = project.getDescription();
			description.setNatureIds(new String[] {
					CCProjectNature.C_NATURE_ID,
					CCProjectNature.CC_NATURE_ID
			});
			project.setDescription(description, null);
		} catch (CoreException e) {
			fail(e.getMessage());
		}
	}
}
