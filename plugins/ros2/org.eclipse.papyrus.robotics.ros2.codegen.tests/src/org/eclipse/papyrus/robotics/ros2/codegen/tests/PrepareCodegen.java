/*******************************************************************************
 * Copyright (c) 2020 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.tests;

import org.eclipse.papyrus.designer.transformation.profile.TrafoProfileResource;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain;
import org.eclipse.papyrus.robotics.ros2.codegen.common.RoboticsTContext;
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.RosHelpers;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.CppSpecificTransformations;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;

@SuppressWarnings("nls")
public class PrepareCodegen {

	protected ExecuteTrafoChain chain;

	protected Package pkg;

	public PrepareCodegen(Package pkg) {
		this.pkg = pkg;
	}

	public boolean prepare() {
		chain = UMLUtil.getStereotypeApplication(pkg, ExecuteTrafoChain.class);
		if (chain == null) {
			chain = StereotypeUtil.applyApp(pkg, ExecuteTrafoChain.class);
			if (chain == null) {
				Package profile = PackageUtil.loadPackage(TrafoProfileResource.PROFILE_PATH_URI, pkg.eResource().getResourceSet());
				pkg.applyProfile((Profile) profile);
				chain = StereotypeUtil.applyApp(pkg, ExecuteTrafoChain.class);
			}
		}
		if (chain == null) {
			Activator.log.debug("Can't apply TransformationChain stereotype");
			return false;
		}

		if (chain.getChain() == null) {
			NamedElement rosChainCl = RosHelpers.getRosType(pkg, "ros2Library::m2mtransformations::ROSChain");
			if (rosChainCl != null) {
				M2MTrafoChain rosChain = UMLUtil.getStereotypeApplication(rosChainCl, M2MTrafoChain.class);
				chain.setChain(rosChain);
			}
		}

		// select C++ codegen
		RoboticsTContext current = new RoboticsTContext();
		RoboticsTContext.current = current;
		current.lst = new CppSpecificTransformations();

		return true;
	}
}
