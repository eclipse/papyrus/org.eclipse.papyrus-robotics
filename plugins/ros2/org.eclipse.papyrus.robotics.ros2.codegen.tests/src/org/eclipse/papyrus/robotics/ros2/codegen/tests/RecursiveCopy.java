/*******************************************************************************
 * Copyright (c) 2020 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.tests;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.papyrus.designer.languages.common.testutils.TestConstants;
import org.eclipse.papyrus.junit.utils.PapyrusProjectUtils;
import org.eclipse.papyrus.junit.utils.rules.AbstractHouseKeeperRule;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Enable a recursive copy of directories from an installed bundle to a workspace file
 * Unlike the RecursiveCopy in designer, it handles copying files from another bundle than the
 * test bundle
 */
public class RecursiveCopy {

	protected AbstractHouseKeeperRule houseKeeper;

	protected Class<?> testClass;

	public RecursiveCopy(Class<?> testClass) {
		this.testClass = testClass;
	}

	/**
	 * Make a recursive copy from a source directory to a destination directory within a workspace project. The
	 * source is assumed to be part of a bundle, not stored in the workspace
	 * 
	 * @param srcBundle
	 *            the installed source bundle
	 * @param srcPath
	 *            a path pointing to a file or folder within the source bundle
	 * @param dstProject
	 *            a destination project within the workspace
	 * @param dstPath
	 *            a path within the destination project
	 */
	public void copy(Bundle srcBundle, String srcPath, IProject dstProject, String dstPath) {

		URL fileURL = srcBundle.getEntry(srcPath);
		assertThat("source path is not contained in bundle", fileURL != null); //$NON-NLS-1$

		try {
			// caveat - handle copying from jars outside of current plugin
			URL resolvedFileURL = FileLocator.toFileURL(fileURL);
			File file = new File(new URI(resolvedFileURL.getProtocol(), resolvedFileURL.getPath(), null));

			assertThat("source file must exist", file.exists()); //$NON-NLS-1$

			if (file.isDirectory()) {
				for (File subFile : file.listFiles()) {
					copy(srcBundle, srcPath + TestConstants.FILE_SEP + subFile.getName(), dstProject, dstPath + TestConstants.FILE_SEP + subFile.getName());
				}
			} else if (file.isFile()) {
				try {
					PapyrusProjectUtils.copyIFile(srcPath, FrameworkUtil.getBundle(testClass), dstProject, dstPath);
				} catch (CoreException e) {
					fail(e.getMessage());
				}
			}
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}

	}
}
