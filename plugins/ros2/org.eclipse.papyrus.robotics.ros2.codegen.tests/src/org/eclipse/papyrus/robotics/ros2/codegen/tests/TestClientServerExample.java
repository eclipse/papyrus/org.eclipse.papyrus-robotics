/*******************************************************************************
 * Copyright (c) 2019 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.tests;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.papyrus.designer.languages.common.testutils.TestConstants;
import org.eclipse.papyrus.junit.utils.rules.HouseKeeper;
import org.eclipse.uml2.uml.Package;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

// @PluginResource("org.eclipse.papyrus.robotics.ros2.examples:/testmodels/clientServer/models/system/clientserver.system.di")
@SuppressWarnings("nls")
public class TestClientServerExample extends AbstractRosTest {

	public static final String CLIENT_SERVER = "clientServer";

	protected IProject clientServerProject;

	protected LwTransformationTestSupport tts;

	@ClassRule
	public static HouseKeeper.Static houseKeeper = new HouseKeeper.Static();

	@Before
	public void setup() {
		// create generated projects. For generation purposes, it is not important that these are not CDT projects
		clientServerProject = houseKeeper.createProject(CLIENT_SERVER);
		CDTUtil.addCppNature(clientServerProject);
		IProject simpleProject = houseKeeper.createProject(SIMPLE_MSGS);
		CDTUtil.addCppNature(simpleProject);
		
		// the set fixture does not contain the models with the right paths.
		ResourceSet rs = houseKeeper.createResourceSet();

		RecursiveCopy copier = new RecursiveCopy(org.eclipse.papyrus.robotics.ros2.examples.Activator.class);
		Bundle srcBundle = FrameworkUtil.getBundle(org.eclipse.papyrus.robotics.ros2.examples.Activator.class);
		// copy expected results folder to model project
		copier.copy(srcBundle, "testmodels/" + CLIENT_SERVER + "/models", clientServerProject, "models");

		Resource r = rs.getResource(URI.createURI("platform:/resource/clientServer/models/system/clientserver.system.uml"), true);
		assert (r != null);
		assert (r.getContents().size() > 0);
		assert (r.getContents().get(0) instanceof Package);

		Package rootPkg = (Package) r.getContents().get(0);

		PrepareCodegen prepare = new PrepareCodegen(rootPkg);
		boolean prepareOk = prepare.prepare();
		assert (prepareOk);

		tts = new LwTransformationTestSupport(this.getClass(), houseKeeper, clientServerProject);
		tts.runTransformation(rootPkg);
	}

	@Test
	public void testSystem() throws InterruptedException {
		tts.validateResults(clientServerProject, TestConstants.EXPECTED_RESULT + TestConstants.FILE_SEP +
				CLIENT_SERVER, ".");
	}
}
