import launch.actions
import launch_ros
import lifecycle_msgs.msg

import sys
import os.path

from launch_ros.events.lifecycle import ChangeState
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory

share_dir = get_package_share_directory('publishsubscribe')
# generated scripts are in launch folder
sys.path.append(os.path.join(share_dir, 'launch'))
# load launch scripts for each instance
import launch_publisher
import launch_subscriber

# Start of user code imports
# End of user code

def generate_launch_description():

	# Launch Description
	ld = launch.LaunchDescription()
	
	# call entity creation for each instance
	publisher_node = launch_publisher.generate_entity()
	subscriber_node = launch_subscriber.generate_entity()
	# now add all obtained entities to the launch description
	ld.add_entity(publisher_node)
	ld.add_entity(subscriber_node)

	# transition to configure after startup
	configure_publisher = launch.actions.RegisterEventHandler(
		launch.event_handlers.on_process_start.OnProcessStart(
			target_action=publisher_node,
			on_start=[
	 			launch.actions.EmitEvent(
					event=launch_ros.events.lifecycle.ChangeState(
						lifecycle_node_matcher=launch.events.matches_action(publisher_node),
						transition_id=lifecycle_msgs.msg.Transition.TRANSITION_CONFIGURE
					)
				)
			]
		)
	)
	ld.add_entity(configure_publisher)
	configure_subscriber = launch.actions.RegisterEventHandler(
		launch.event_handlers.on_process_start.OnProcessStart(
			target_action=subscriber_node,
			on_start=[
	 			launch.actions.EmitEvent(
					event=launch_ros.events.lifecycle.ChangeState(
						lifecycle_node_matcher=launch.events.matches_action(subscriber_node),
						transition_id=lifecycle_msgs.msg.Transition.TRANSITION_CONFIGURE
					)
				)
			]
		)
	)
	ld.add_entity(configure_subscriber)

	# transition to activate, once inactive
	activate_publisher = launch.actions.RegisterEventHandler(
		launch_ros.event_handlers.OnStateTransition(
			target_lifecycle_node=publisher_node,
			start_state='configuring', goal_state='inactive',
			entities=[
				launch.actions.EmitEvent(
					event=launch_ros.events.lifecycle.ChangeState(
						lifecycle_node_matcher=launch.events.matches_action(publisher_node),
						transition_id=lifecycle_msgs.msg.Transition.TRANSITION_ACTIVATE
					)
				)
			]
		)
	)
	ld.add_entity(activate_publisher)
	activate_subscriber = launch.actions.RegisterEventHandler(
		launch_ros.event_handlers.OnStateTransition(
			target_lifecycle_node=subscriber_node,
			start_state='configuring', goal_state='inactive',
			entities=[
				launch.actions.EmitEvent(
					event=launch_ros.events.lifecycle.ChangeState(
						lifecycle_node_matcher=launch.events.matches_action(subscriber_node),
						transition_id=lifecycle_msgs.msg.Transition.TRANSITION_ACTIVATE
					)
				)
			]
		)
	)
	ld.add_entity(activate_subscriber)

	
	# Start of user code post-launch
	# End of user code
	return ld
