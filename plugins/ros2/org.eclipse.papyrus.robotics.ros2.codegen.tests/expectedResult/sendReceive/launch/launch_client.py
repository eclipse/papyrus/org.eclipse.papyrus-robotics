from launch import LaunchDescription
from launch_ros.actions import LifecycleNode
from ament_index_python.packages import get_package_share_directory

share_dir = get_package_share_directory('sendreceive')

# Start of user code imports
# End of user code

def generate_entity():
	# define empty user parameter (eventually redefined in the protected section afterwards)
	client_custom_params = {}

	# Start of user code parameters
	# End of user code

	# Add the actions to the launch description.
	return LifecycleNode(
		name='client',
		package='sendreceive', executable='SendClient_main',
		namespace='',
		remappings=[
			('map', 'server/Map/map')
		],
		parameters=[share_dir+'/launch/cfg/param.yaml', client_custom_params],
		output='screen',
		emulate_tty=True	# assure that RCLCPP output gets flushed
	)

def generate_launch_description():

	# Launch Description
	ld = LaunchDescription()
	ld.add_entity(generate_entity())

	# Start of user code post-launch
	# End of user code
	return ld
