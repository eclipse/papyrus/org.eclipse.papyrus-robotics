import launch.actions
import launch_ros
import lifecycle_msgs.msg

import sys
import os.path

from launch_ros.events.lifecycle import ChangeState
from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory

share_dir = get_package_share_directory('sendreceive')
# generated scripts are in launch folder
sys.path.append(os.path.join(share_dir, 'launch'))
# load launch scripts for each instance
import launch_client
import launch_server

# Start of user code imports
# End of user code

def generate_launch_description():

	# Launch Description
	ld = launch.LaunchDescription()
	
	# call entity creation for each instance
	client_node = launch_client.generate_entity()
	server_node = launch_server.generate_entity()
	# now add all obtained entities to the launch description
	ld.add_entity(client_node)
	ld.add_entity(server_node)

	
	# Start of user code post-launch
	# End of user code
	return ld
