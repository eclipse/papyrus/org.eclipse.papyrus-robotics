// --------------------------------------------------------
// Copyright (c)
//
// contributions by Ansgar
//                  ansgar.radermacher@cea.fr
// maintained by    CEA LIST
//                  info-list@cea.fr

// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef ADDCLIENTCOMPDEF_ADDCLIENT_H
#define ADDCLIENTCOMPDEF_ADDCLIENT_H

/************************************************************
 AddClient class header
 ************************************************************/

#include "addClientCompdef/Pkg_addClientCompdef.h"

#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "simple_msgs/srv/add_service_def.hpp"

// Include from Include stereotype (header)
#include <rclcpp/rclcpp.hpp>

// End of Include stereotype (header)

namespace ros2Library {
namespace rclcpp {
class CallbackGroup;
}
}
namespace ros2Library {
namespace rclcpp {
class Client;
}
}
namespace ros2Library {
namespace rclcpp {
class NodeOptions;
}
}
namespace ros2Library {
namespace rclcpp {
namespace timer {
class TimerBase;
}
}
}

namespace addClientCompdef {

/************************************************************/
/**
 * 
 */
class AddClient: public rclcpp_lifecycle::LifecycleNode {
public:

  /**
   * 
   */
  rclcpp::CallbackGroup::SharedPtr cbg_CallAdd_;

  /**
   * 
   */
  rclcpp::TimerBase::SharedPtr timer_CallAdd_;

  /**
   * 
   */
  rclcpp::CallbackGroup::SharedPtr cbg_t_CallAdd_;

  /**
   * 
   */
  rclcpp::Client<simple_msgs::srv::AddServiceDef>::SharedPtr useAdd_client_;

  /**
   * 
   * @param options 
   */
  AddClient(rclcpp::NodeOptions /*in*/options);

};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace addClientCompdef

/************************************************************
 End of AddClient class header
 ************************************************************/

#endif
