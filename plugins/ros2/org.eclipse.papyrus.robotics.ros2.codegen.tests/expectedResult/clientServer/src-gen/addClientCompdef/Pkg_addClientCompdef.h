// --------------------------------------------------------
// Copyright (c)
//
// contributions by Ansgar
//                  ansgar.radermacher@cea.fr
// maintained by    CEA LIST
//                  info-list@cea.fr

#ifndef PKG_ADDCLIENTCOMPDEF
#define PKG_ADDCLIENTCOMPDEF

/************************************************************
 Pkg_addClientCompdef package header
 ************************************************************/

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace addClientCompdef {

// Types defined within the package
}// of namespace addClientCompdef

/************************************************************
 End of Pkg_addClientCompdef package header
 ************************************************************/

#endif
