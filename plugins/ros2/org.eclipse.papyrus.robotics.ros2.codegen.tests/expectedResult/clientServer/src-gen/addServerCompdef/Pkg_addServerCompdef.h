// --------------------------------------------------------
// Copyright (c)
//
// contributions by Ansgar
//                  ansgar.radermacher@cea.fr
// maintained by    CEA LIST
//                  info-list@cea.fr

#ifndef PKG_ADDSERVERCOMPDEF
#define PKG_ADDSERVERCOMPDEF

/************************************************************
 Pkg_addServerCompdef package header
 ************************************************************/

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace addServerCompdef {

// Types defined within the package
}// of namespace addServerCompdef

/************************************************************
 End of Pkg_addServerCompdef package header
 ************************************************************/

#endif
