// --------------------------------------------------------
// Copyright (c)
//
// contributions by Ansgar
//                  ansgar.radermacher@cea.fr
// maintained by    CEA LIST
//                  info-list@cea.fr

#ifndef PKG_INTERMEDIATE
#define PKG_INTERMEDIATE

/************************************************************
 Pkg_intermediate package header
 ************************************************************/

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace intermediate {

// Types defined within the package
}// of namespace intermediate

/************************************************************
 End of Pkg_intermediate package header
 ************************************************************/

#endif
