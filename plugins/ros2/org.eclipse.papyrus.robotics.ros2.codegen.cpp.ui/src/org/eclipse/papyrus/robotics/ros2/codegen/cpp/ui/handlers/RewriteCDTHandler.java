/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp.ui.handlers;

import java.util.HashMap;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.papyrus.designer.infra.ui.UIProjectManagement;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.jobs.CppGenJob;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Classifier;

public class RewriteCDTHandler extends CmdHandler {

	protected ExecuteTrafoChain chain;

	@Override
	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		updateSelectedEObject();
		if (!(selectedEObject instanceof Classifier) || GenerateCodeHandler.isRunning()) {
			return null;
		}

		Job job = new CppGenJob(
				PackageUtil.getRootPackage((Classifier) selectedEObject),
				UIProjectManagement.getCurrentProject(),
				new HashMap<String, Boolean>());

		job.setUser(true);
		job.schedule();
		return null;
	}
}
