/*****************************************************************************
 * Copyright (c) 2020, 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #566899
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Bug #581690
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp.build

import java.util.List
import java.util.Map
import java.util.Set
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillRealizBuildUtils
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*

/**
 * Create CMakeLists file for a list of skill realizations
 */
class CreateSkillRealizCMakeLists {
	static def createCMakeLists(Package pkg, Set<SkillDefinition> skills, List<String> serviceDependNames) '''
		cmake_minimum_required(VERSION 3.5.0)
		project(«pkg.realizationPackageName»)

		# Default to C++14
		if(NOT CMAKE_CXX_STANDARD)
		  set(CMAKE_CXX_STANDARD 14)
		endif()

		if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
		  add_compile_options(-Wall -Wextra -Wpedantic)
		endif()

		find_package(ament_cmake REQUIRED)
		find_package(nav2_common REQUIRED)
		find_package(rclcpp REQUIRED)
		find_package(rclcpp_action REQUIRED)
		find_package(behaviortree_cpp_v3 REQUIRED)
		find_package(nav2_behavior_tree REQUIRED)
		find_package(bt_utils REQUIRED)
		«FOR pkgName : serviceDependNames»
			find_package(«pkgName» REQUIRED)
		«ENDFOR»

		nav2_package()

«««		include_directories(
«««		  include
«««		)

		set(dependencies
		  rclcpp
		  rclcpp_action
		  behaviortree_cpp_v3
		  nav2_behavior_tree
		  bt_utils
		  «FOR pkgName : serviceDependNames»
		  	«pkgName»
		  «ENDFOR»
		)

		«FOR skill : skills»
			add_library(«skill.realizationFileName»_bt_node SHARED src/«skill.realizationFileName».cpp)
			list(APPEND plugin_libs «skill.realizationFileName»_bt_node)
		«ENDFOR»

		foreach(bt_plugin ${plugin_libs})
		  ament_target_dependencies(${bt_plugin} ${dependencies})
		  target_compile_definitions(${bt_plugin} PRIVATE BT_PLUGIN_EXPORT)
		endforeach()

		install(TARGETS ${plugin_libs}
		  ARCHIVE DESTINATION lib
		  LIBRARY DESTINATION lib
		  RUNTIME DESTINATION lib/${PROJECT_NAME}
		)

«««		install(DIRECTORY include/
«««		  DESTINATION include/
«««		)

		if(BUILD_TESTING)
		  find_package(ament_lint_auto REQUIRED)
		  ament_lint_auto_find_test_dependencies()
		endif()

«««		ament_export_include_directories(include)

		ament_export_libraries(
		  ${plugin_libs}
		)

		ament_export_dependencies(${dependencies})

		ament_package()
	'''

	static def generate(IPFileSystemAccess fileAccess, Package pkg, Map<SkillDefinition, SkillSemantic> skdefToSemanticsMap) {
		val serviceDependNames = SkillRealizBuildUtils.calcDependencies(skdefToSemanticsMap)
		fileAccess.generateFile("CMakeLists.txt", createCMakeLists(pkg, skdefToSemanticsMap.keySet, serviceDependNames).toString)
	}
}