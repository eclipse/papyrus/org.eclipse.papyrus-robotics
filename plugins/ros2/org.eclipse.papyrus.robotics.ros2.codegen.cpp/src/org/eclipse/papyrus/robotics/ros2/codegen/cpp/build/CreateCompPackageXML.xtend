/*****************************************************************************
 * Copyright (c) 2018, 2021, 2024 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr - #8 / remove references to Python packages
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp.build

import java.util.ArrayList
import java.util.List
import org.eclipse.cdt.core.CCProjectNature
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.papyrus.designer.transformation.base.utils.ProjectManagement
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils
import org.eclipse.papyrus.uml.tools.utils.PackageUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ComponentUtils.getDependsPackage
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*

/**
 * Create PackageXML file for a ROS 2 package, containing components and eventually one system
 */
class CreateCompPackageXML {

	public static String USER = "USER"

	static def createPackageXML(Package model, List<Class> allComponents, List<Class> componentsInPkg, Class system) '''
		«val entities = entityArray(allComponents, system)»
		<?xml version="1.0"?>
		<package format="3">
			<name>«model.pkgName»</name>
			<version>0.0.0</version>
			<description>«entities.description»</description>
			<maintainer email="«entities.maintainerMail»">«entities.maintainerName»</maintainer>
			<license>Apache2.0</license>
			<url type="website">https://eclipse.org/papyrus</url>
			<author email="«entities.authorMail»">«model.authorName»</author>

			<buildtool_depend>ament_cmake</buildtool_depend>

			«FOR dependency : model.calcBuildDependencies(allComponents, componentsInPkg)»
				<build_depend>«dependency»</build_depend>
			«ENDFOR»
			«IF !system.uniqueSkills.nullOrEmpty»
				<build_depend>«model.realizationPackageName»</build_depend>
			«ENDIF»

			«FOR dependency : model.calcDependencies(allComponents, componentsInPkg)»
				 <exec_depend>«dependency»</exec_depend>
			«ENDFOR»
			«IF !system.uniqueSkills.nullOrEmpty»
				<exec_depend>«model.realizationPackageName»</exec_depend>
			«ENDIF»

«««			«FOR dependency : calcDependencies»
«««				 <run_depend>«dependency»</run_depend>
«««			«ENDFOR»
			<export>
				<build_type>ament_cmake</build_type>
			</export>
		</package>
	'''

	/**
	 * Calculate the dependencies of a list of all components and those defined in
	 * this package. The dependencies includes the message dependencies of the
	 * components in this package and user-defined dependencies as well as the package
	 * defining the component itself for all components.
	 */
	static def calcDependencies(Package model, List<Class> allComponents, List<Class> componentsInPkg) {
		val dependencies = MessageUtils.calcDependencies(componentsInPkg)
		for (component : allComponents) {
			// add user-defined dependencies defined for a component package
			for (pkgName : component.dependsPackage) {
				dependencies.add(pkgName)
			}
			// add component package itself, if not part of the current package
			if (component.model.pkgName != model.pkgName) {
				dependencies.add(component.model.pkgName)
			}
		}
		return dependencies
	}

	/**
	 * Calculate the build dependencies which do not contain dependencies to Python
	 * packages as Python does not have package.xml / CMakeLists.txt files
	 */
	static def calcBuildDependencies(Package model, List<Class> allComponents, List<Class> componentsInPkg) {
		val dependencies = calcDependencies(model, allComponents, componentsInPkg)
		val buildDependencies = new ArrayList<String>
		for (dependency : dependencies) {
			if (!isPython(dependency)) {
				buildDependencies.add(dependency)
			}
		}
		return buildDependencies
	}

	/**
	 * Determine, if a package is a Python package. This function is used to exclude these dependencies
	 * as Python does not have package.xml / CMakeLists.txt files
	 */
	static def isPython(String pkgName) {
		var project = ProjectManagement.getNamedProject(pkgName)
		if (project !== null && project.exists()) {
			if (project.getNature(CCProjectNature.CC_NATURE_ID) !== null) {
				// C++ project => no Python
				return false;
			}
			val setup = project.getFile("setup.py")
			if (setup !== null && setup.exists()) {
				// setup.py file is present => a ROS python project
				return true;
			}
		}
		// defined outside of Eclipse, assume that is it not a Python package
		return false;
	}
	
	static def entityArray(List<Class> components, Class system) {
		val entities = new ArrayList<Package>
		if (system !== null) {
			entities.add(PackageUtil.getRootPackage(system))
		}
		for (Class component : components) {
			entities.add(PackageUtil.getRootPackage(component))
		}
		return entities
	}

	static def generate(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) {
		fileAccess.generateFile("package.xml", createPackageXML(pkg, allComponents, componentsInPkg, system).toString)
	}
}