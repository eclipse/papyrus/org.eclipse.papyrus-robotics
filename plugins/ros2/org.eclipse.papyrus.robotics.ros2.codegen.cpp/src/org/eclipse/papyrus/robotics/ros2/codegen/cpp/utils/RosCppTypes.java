/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp.utils;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.robotics.codegen.common.utils.Helpers;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Type;

public class RosCppTypes {

	public static final URI RCLCPP_LIBRARY_URI = URI.createURI("pathmap://ROS2_CPP_LIBRARY/rclcppLibrary.uml"); //$NON-NLS-1$

	/**
	 * Get a type from the ROS C++ library
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @param qualifiedName
	 *            the qualified name of the element to load
	 * @return the element, if found, null otherwise
	 */
	public static Type getType(Element element, String qualifiedName) {
		// don't need a copy, as these types are not in previous transformation steps
		return Helpers.getTypeFromRS(element, RCLCPP_LIBRARY_URI, qualifiedName, false);
	}
	
}
