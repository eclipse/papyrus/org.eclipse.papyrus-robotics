/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;
import org.eclipse.papyrus.robotics.ros2.codegen.common.LangSpecificTransformations;
import org.eclipse.papyrus.robotics.ros2.codegen.common.component.AbstractCompTransformations;
import org.eclipse.papyrus.robotics.ros2.codegen.common.message.CreateMsgPackage;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.build.CreateCompCMakeLists;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.build.CreateCompPackageXML;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.build.CreateMsgPkgCMakeLists;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.build.CreateMsgPkgPackageXML;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.component.ComponentTransformations;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.skillrealization.CreateSkillRelizationPackage;
import org.eclipse.papyrus.robotics.ros2.codegen.cpp.utils.ProjectTools;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;

/**
 * Implementation of abstract transformations
 */
public class CppSpecificTransformations implements LangSpecificTransformations {

	protected CreateMsgPackage creator;
	
	public CppSpecificTransformations() {
		creator = new CreateMsgPackage();
	}

	@Override
	public void createCompDefBuildFiles(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) {
		CreateCompCMakeLists.generate(fileAccess, pkg, allComponents, componentsInPkg, system);
		CreateCompPackageXML.generate(fileAccess, pkg, allComponents, componentsInPkg, system);
	}

	@Override
	public void createMsgBuildFiles(IPFileSystemAccess fileAccess, Package pkg) {
		CreateMsgPkgCMakeLists.generate(fileAccess, pkg);
		CreateMsgPkgPackageXML.generate(fileAccess, pkg);
	}

	@Override
	public void createMsgPackage(Package msgPkg) {
		creator.createMsgPkg(msgPkg);
	}

	@Override
	public AbstractCompTransformations getCompTransformation(IPFileSystemAccess fileAccess, IProject project) {
		return new ComponentTransformations(fileAccess, project);
	}

	@Override
	public void createSkillRealizationPkg(Class system, List<Class> components, Package pkg) {
		CreateSkillRelizationPackage skillRealization = new CreateSkillRelizationPackage(system, components);
		skillRealization.createSkillRealizationPkg(pkg);
	}

	@Override
	public IProject getProject(String projectName) {
		return ProjectTools.getProject(projectName);
	}

	@Override
	public void configureProject(IProject project, List<String> depPkgList) {
		ProjectTools.configureIncludes(project, depPkgList);
	}
}
