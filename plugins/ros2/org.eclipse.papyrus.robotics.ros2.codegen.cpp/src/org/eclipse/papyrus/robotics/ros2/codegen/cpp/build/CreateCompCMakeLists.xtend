/*****************************************************************************
 * Copyright (c) 2018, 2021, 2024 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr - Bug #571655
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr - #8 / remove references to Python packages
 *  Matteo MORELLI      matteo.morelli@cea.fr - #12/Simplify the process to execute a system task
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.robotics.ros2.codegen.cpp.build

import java.util.Collections
import java.util.List
import org.eclipse.papyrus.designer.languages.common.base.file.ProtSection
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.papyrus.robotics.codegen.common.component.CodeSkeleton
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.hasExternalCode
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ComponentUtils.isRegistered
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*

/**
 * Create CMakeLists file for a ROS 2 package, containing components and eventually one system
 */
class CreateCompCMakeLists {
	static def createCMakeLists(Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) '''
		cmake_minimum_required(VERSION 3.5.0)
		project(«pkg.pkgName»)

		# Default to C++14
		if(NOT CMAKE_CXX_STANDARD)
		  set(CMAKE_CXX_STANDARD 14)
		endif()

		if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
		  add_compile_options(-Wall -Wextra -Wpedantic)
		endif()

		find_package(ament_cmake REQUIRED)
		«FOR dep_pkg : CreateCompPackageXML.calcBuildDependencies(pkg, allComponents, componentsInPkg)»
			find_package(«dep_pkg» REQUIRED)
		«ENDFOR»
		«IF !system.uniqueSkills.nullOrEmpty»
			find_package(«pkg.realizationPackageName» REQUIRED)
		«ENDIF»

		include_directories(
			# assure that generated .h files are found
			${PROJECT_SOURCE_DIR}/src
			${PROJECT_SOURCE_DIR}/src-gen
		)

		«FOR component : componentsInPkg»
			«val compBaseFile = component.fileName»
			«IF component.isRegistered»
				add_library(«component.name»_comp SHARED
					src-gen/«compBaseFile».cpp«
					IF component.hasExternalCode»
						src/«compBaseFile»«CodeSkeleton.POSTFIX».cpp
					«ENDIF»
				)
				target_compile_definitions(«component.name»_comp
					PRIVATE "«component.name»_shared_library")
				ament_target_dependencies(«component.name»_comp
					«pkg.msgDependencies(component)»
					«IF !system.uniqueSkills.nullOrEmpty»
						«pkg.realizationPackageName»
					«ENDIF»
				)
				rclcpp_components_register_nodes(«component.name»_comp "«pkg.pkgName»::«component.name»")

				add_executable(«component.name»_main src-gen/«compBaseFile»_main.cpp)
				target_link_libraries(«component.name»_main «component.name»_comp)
				ament_target_dependencies(«component.name»_main rclcpp)

			«ELSE»
				add_executable(«component.name»
					src-gen/«compBaseFile».cpp
					src-gen/«compBaseFile»_main.cpp
					«IF component.hasExternalCode»
						src/«compBaseFile»«CodeSkeleton.POSTFIX».cpp«
					ENDIF»
				)
				ament_target_dependencies(«component.name»
					«pkg.msgDependencies(component)»
					«IF !system.uniqueSkills.nullOrEmpty»
						«pkg.realizationPackageName»
					«ENDIF»
				)

			«ENDIF»
		«ENDFOR»

		# «ProtSection.protSection("dependencies")»
		# «ProtSection.protSection»

		«FOR component : componentsInPkg»
			install(TARGETS
				«IF component.isRegistered»
					«component.name»_main
					«component.name»_comp
				«ELSE»
					«component.name»
				«ENDIF»
				DESTINATION lib/${PROJECT_NAME}
			)
		«ENDFOR»
		«IF system !== null»

			# Install launch files.
			install(DIRECTORY
				launch «IF !system.uniqueSkills.nullOrEmpty»«system.getPackageRelativePathOfDefaultTask»«ENDIF»
				DESTINATION share/${PROJECT_NAME}/
			)
		«ENDIF»
		ament_package()
	'''

	static def msgDependencies(Package model, Class component) '''
		«val compList = Collections.singletonList(component)»
		«FOR msg_pkg : CreateCompPackageXML.calcBuildDependencies(model, compList, compList)»
			«msg_pkg»
		«ENDFOR»
	'''

	static def generate(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) {
		fileAccess.generateFile("CMakeLists.txt", createCMakeLists(pkg, allComponents, componentsInPkg, system).toString)
	}

	static def fileName(NamedElement ne) {
		ne.qualifiedName.replace("::", "/")
	}
}