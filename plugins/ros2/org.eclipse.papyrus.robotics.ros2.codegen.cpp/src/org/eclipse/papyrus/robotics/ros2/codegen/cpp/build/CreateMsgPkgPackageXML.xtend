/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp.build

import java.util.ArrayList
import java.util.List
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.calcDependencies
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.*

class CreateMsgPkgPackageXML {
	
	static def createPackageXML(Package msgPackage) '''
		<?xml version="1.0"?>
		<package format="3">
			<name>«msgPackage.pkgName»</name>
			<version>0.0.0</version>
			<description>«msgPackage.name» package</description>
			<maintainer email="«msgPackage.maintainerMail»">«msgPackage.maintainerName»</maintainer>
			<license>Apache2.0</license>
			<url type="website">https://eclipse.org/papyrus</url>
			<author email="«msgPackage.authorMail»">«msgPackage.authorName»</author>
		
			<buildtool_depend>rosidl_default_generators</buildtool_depend>

			«FOR dependency : msgPackage.calcDependencies»
				<depend>«dependency.name.toLowerCase»</depend>
			«ENDFOR»

			<member_of_group>rosidl_interface_packages</member_of_group>

			<export>
				<build_type>ament_cmake</build_type>
			</export>
		</package>
	'''

	def List<String> calcDependencies() {
		val list = new ArrayList<String>
		list.add("rosidl_default_generators")
		list.add("roscpp")
		return list
	}

	static def generate(IPFileSystemAccess fileAccess, Package pkg) {
		fileAccess.generateFile("package.xml", createPackageXML(pkg).toString)
	}
}