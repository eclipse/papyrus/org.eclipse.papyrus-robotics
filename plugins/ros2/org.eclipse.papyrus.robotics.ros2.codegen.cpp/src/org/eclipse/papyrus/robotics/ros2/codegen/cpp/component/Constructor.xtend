/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.cpp.component

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.ConstInit
import org.eclipse.papyrus.robotics.codegen.common.utils.ApplyProfiles
import org.eclipse.papyrus.robotics.core.utils.PortUtils
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.TopicUtils.getTopic
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*
import static extension org.eclipse.papyrus.robotics.core.utils.InteractionUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.cpp.component.Callbacks.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.component.CallbackGroups.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.RosHelpers.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.cpp.utils.RosCppTypes.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils.varName

import org.eclipse.papyrus.robotics.profile.robotics.components.Activity

/**
 * Manage constructor creation, including port
 */
class Constructor {

	/**
	 * Create constructor and (if required) callback methods for handlers 
	 */
	def static createConstructor(Class component) {
		val lcNodeSC = getType(component, "ros2Library::rclcpp_lifecycle::LifecycleNode")
		if (lcNodeSC instanceof Classifier) {
			component.createGeneralization(lcNodeSC)
		}
		val op = addConstrOp(component);
		addConstrMethod(component, op);
	}

	/**
	 * Add an init operation (constructor) that creates the ROS 2 publishers/subscribers/clients/... for ports 
	 */
	def static addConstrOp(Class component) {
		// val nodeSC = getType(component, "ros2Library::rclcpp::Node");
		val nodeOptions = getType(component, "ros2Library::rclcpp::NodeOptions")
		// val string = getPrimitiveType(component, "PrimitiveTypes::String")
		// component.createOwnedAttribute("instName", string)
		val init = component.createOwnedOperation(component.name, null, null)
		init.createOwnedParameter("options", nodeOptions)
		var create = StereotypeUtil.applyApp(init, Create)
		if (create === null) {
			ApplyProfiles.applyStdProfile(init)
			create = StereotypeUtil.applyApp(init, Create)
		}
		var constInit = StereotypeUtil.applyApp(init, ConstInit)
		if (constInit === null) {
			ApplyProfiles.applyCppProfile(init)
			constInit = StereotypeUtil.applyApp(init, ConstInit)
		}
		constInit.initialisation = '''rclcpp_lifecycle::LifecycleNode("«component.name»", options)'''
		return init
	}

	/**
	 * Add a method body to the constructor operation
	 */
	def static addConstrMethod(Class component, Operation constructorOp) {
		val ob = component.createOwnedBehavior(component.name, UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		constructorOp.methods.add(ob)
		ob.languages.add("C++")
		ob.bodies.add('''
			«FOR activity : component.activities»
				«activity.createCallbackGroupVar(component)»
				«activity.createCallbackGroupCode»
			«ENDFOR»
			«FOR port : PortUtils.getAllPorts(component)»
				«IF port.communicationPattern !== null»
					«val pattern = port.communicationPattern»
					«IF pattern.isPush || pattern.isPubSub»
						«port.createPush»

					«ELSEIF pattern.isSend»
						«port.createSend»

					«ELSEIF pattern.isQuery»
						«port.createQuery»

					«ELSEIF pattern.isAction»
						«port.createAction»

					«ELSEIF pattern.isEvent»
						««« TODO: unsupported
					«ENDIF»
				«ENDIF»
			«ENDFOR»
			
		''')
	}

	/**
	 * Create a callback_group attribute for each activity
	 */
	def static void createCallbackGroupVar(Activity activity, Class component) {
		val cbgType = getType(component, "ros2Library::rclcpp::CallbackGroup");
		val cbgAttr = component.createOwnedAttribute(activity.base_Class.callbackGroupName, cbgType);
		cbgAttr.useSharedPtr
	}

	/**
	 * Add the code that creates a callback group for each activity and optionally
	 * also one for the timer
	 */
	def static createCallbackGroupCode(Activity activity) '''
		«activity.base_Class.callbackGroupName» = create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
		«IF activity.period !== null»
			«activity.base_Class.tCallbackGroupName» = create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
		«ENDIF»

	'''

	/**
	 * Create a publisher or subscriber
	 */
	def static createPush(Port port) '''
		«IF port.provideds.size() > 0»
««« Publisher
			rclcpp::PublisherOptions «port.varName»_options;
			«port.varName»_options.callback_group = «port.callbackGroupName»;
			«port.varName»_pub_ = create_publisher<«port.commObject.externalName»>("«port.topic»",
					«port.qos("1")», «port.varName»_options);
			// directly activate a publisher
			«port.varName»_pub_->on_activate();
		«ELSEIF port.requireds.size() > 0»
««« Subscriber
			rclcpp::SubscriptionOptions «port.varName»_options;
			«port.varName»_options.callback_group = «port.callbackGroupName»;
			«val defaultQoS = "rclcpp::QoS(rclcpp::KeepLast(100)).best_effort()"»
			«port.varName»_sub_ = create_subscription<«port.commObject.externalName»>("«port.topic»",
				«port.qos(defaultQoS)»,
				«port.class_.callBackMethodForPush(port)», «port.varName»_options);
		«ENDIF»
	'''

	/**
	 * Create send (publisher and subscriber as well)
	 */
	def static createSend(Port port) '''
		«IF port.provideds.size() > 0»
««« Subscriber
			rclcpp::SubscriptionOptions «port.varName»_options;
			«port.varName»_options.callback_group = «port.callbackGroupName»;
			«val defaultQoS = "rclcpp::QoS(rclcpp::KeepLast(100)).best_effort()"»
			«port.varName»_recv_ = create_subscription<«port.commObject.externalName»>("«port.topic»",
				«port.qos(defaultQoS)»,
				«port.class_.callBackMethodForPush(port)», «port.varName»_options);
		«ELSEIF port.requireds.size() > 0»
««« Publisher
			rclcpp::PublisherOptions «port.varName»_options;
			«port.varName»_options.callback_group = «port.callbackGroupName»;
			«port.varName»_send_ = create_publisher<«port.commObject.externalName»>("«port.topic»",
					«port.qos("1")», «port.varName»_options);
			// directly activate a publisher
			«port.varName»_send_->on_activate();
		«ENDIF»
	'''

	/**
	 * Create a service client or server
	 */
	def static createQuery(Port port) '''
		«IF port.provideds.size() > 0»
			««« Server
			«port.varName»_srv_ = create_service<«port.serviceType.externalName»>("«port.topic»", «port.class_.serverCallBackMethodForService(port)», rmw_qos_profile_default, «port.callbackGroupName»);
		«ELSEIF port.requireds.size() > 0»
			«port.varName»_client_ = create_client<«port.serviceType.externalName»>("«port.topic»", rmw_qos_profile_default, «port.callbackGroupName»);
			«port.class_.clientCallBackMethodForService(port)»
		«ENDIF»
	'''

	/**
	 * Create an action client or server
	 */
	def static createAction(Port port) '''
		«IF port.provideds.size() > 0»
			«port.varName»_actsrv_ = rclcpp_action::create_server<«port.serviceType.externalName»>(this, 
				"«port.topic»",
				«port.class_.serverCallsbacksForAction(port)», rcl_action_server_get_default_options(), «port.callbackGroupName»);
		«ELSE»
			«port.varName»_actcli_ = rclcpp_action::create_client<«port.serviceType.externalName»>(this, 
				"«port.topic»", «port.callbackGroupName»);
			«port.class_.clientCallsbacksForAction(port)»
		«ENDIF»
	'''

	/**
	 * Create an event client and server
	 * TODO - not supported
	 */
	def static createEvent(Port port) '''
«««		This pattern is aimed to create run-time configurable notification mechanism.
«««		The client can register to be notified when a particular event happens on the server side.
«««		The server catches the events and sends a message only to clients interested to that
«««		particular event.
«««		The event condition check on the server side must be written by user, the pattern specifies
«««		the semantic of the <RequestedEvent, ReplyMessage, DataToCheck>
«««		There is no equivalent pattern in YARP, but it can be implemented via specialized
«««		client/server devices.
	'''

	/**
	 * Return the QoS associated with a port or defaultQoS, if none is specified
	 * 
	 * The "KEEP_LAST" history setting tells DDS to store a fixed-size buffer of values before they
	 * are sent, to aid with recovery in the event of dropped messages.
	 * use best effort to avoid blocking during execution.
	 */	
	def static qos(Port port, String defaultQoS) {
		val compPort = UMLUtil.getStereotypeApplication(port, ComponentPort)
		if (compPort !== null && compPort.qos !== null && compPort.qos.length > 0) {
			return compPort.qos
		}
		else {
			return defaultQoS
		}
 	}
}
