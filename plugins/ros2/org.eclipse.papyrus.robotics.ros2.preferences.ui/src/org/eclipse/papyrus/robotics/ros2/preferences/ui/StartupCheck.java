/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.preferences.ui;

import org.eclipse.papyrus.robotics.ros2.base.EnvironmentUtils;
import org.eclipse.papyrus.robotics.ros2.preferences.Ros2PreferenceUtils;
import org.eclipse.ui.IStartup;

/**
 * Initialize the startup here. We cannot do this in the activator of other P4R plugins, as
 * the build via colcon does not trigger any P4R plugin loading (unless we add additional
 * build commands).
 * TODO: needs re-consideration in the context of web-based RCP
 */
public class StartupCheck implements IStartup {

	@Override
	public void earlyStartup() {
		EnvironmentUtils.runCheckAndApplySetupJob(Ros2PreferenceUtils.getSetupPath());
	}
}
