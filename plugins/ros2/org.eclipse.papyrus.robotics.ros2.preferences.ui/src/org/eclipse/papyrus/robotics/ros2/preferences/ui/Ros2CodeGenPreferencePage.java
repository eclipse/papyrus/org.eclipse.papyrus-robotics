/*******************************************************************************
 * Copyright (c) 2019, 2024 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *             Bug #3 (Bad preferences)
 *******************************************************************************/

package org.eclipse.papyrus.robotics.ros2.preferences.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PathEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.papyrus.robotics.ros2.preferences.Ros2Distributions;
import org.eclipse.papyrus.robotics.ros2.preferences.Ros2Distributions.RDLiteral;
import org.eclipse.papyrus.robotics.ros2.preferences.Ros2PreferenceConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the
 * preference store that belongs to the main plug-in class. That way,
 * preferences can be accessed directly via the preference store.
 */

public class Ros2CodeGenPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	protected ComboFieldEditor distroEditor;

	protected PathEditor pathEditor;
	
	public Ros2CodeGenPreferencePage() {
		super(GRID);
		IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE,
				org.eclipse.papyrus.robotics.ros2.preferences.Activator.PLUGIN_ID);
		setPreferenceStore(store);
		setDescription("This preferences page allows to customize ROS 2 C++ code generation/reverse"); //$NON-NLS-1$
	}

	public void addTextField(String name, String label, Document currentDoc) {
		// ///////////////////////////////////////////////////////////////////////
		// Create a Group for the text
		// ///////////////////////////////////////////////////////////////////////
		Group txtGroup = new Group(getFieldEditorParent(), SWT.RESIZE);
		txtGroup.setLayout(new FillLayout());
		txtGroup.setText(label);
		GridData gd = new GridData(GridData.FILL_BOTH /* FILL_HORIZONTAL */);
		// gd.heightHint = 250;
		gd.horizontalSpan = 2;
		txtGroup.setLayoutData(gd);

		// Text area
		SourceViewer txtViewer = new SourceViewer(txtGroup, null, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);

		txtViewer.setDocument(currentDoc);

		// Retrieving existing preference
		String content = getPreferenceStore().getString(name);

		// Loading preference in txt zone
		currentDoc.set(content);
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI
	 * blocks needed to manipulate various types of preferences. Each field editor
	 * knows how to save and restore itself.
	 */
	@Override
	public void createFieldEditors() {

		List<String[]> distributions = new ArrayList<String[]>();
		// build list from enumeration (avoid that we need to change the UI code, if the
		// list of distributions changes)
		for (RDLiteral lit : RDLiteral.values()) {
			if (lit != RDLiteral.UNKNOWN) {
				String name = Ros2Distributions.getDistroName(lit);
				distributions.add(new String[] { name, name });
			}
		}
		Composite parent = getFieldEditorParent();
		distroEditor = new ComboFieldEditor(Ros2PreferenceConstants.P_ROS_DISTRO, "ROS 2 distribution", //$NON-NLS-1$
				distributions.toArray(new String[2][0]), parent);
		addField(distroEditor);
		pathEditor = new PathEditor(Ros2PreferenceConstants.P_SETUP_PATH,
				"List of paths (that must contain a setup.bash file) to source", //$NON-NLS-1$
				"select a directory with a ROS 2 setup.bash", parent); //$NON-NLS-1$
		addField(pathEditor);

		addField(new StringFieldEditor(Ros2PreferenceConstants.P_COLCON_OPTIONS, "colcon build options", parent)); //$NON-NLS-1$
		addField(new StringFieldEditor(Ros2PreferenceConstants.P_COLCON_PACKAGES,
				"colcon switch to select a certain package (or set thereof)", parent)); //$NON-NLS-1$
		addField(new StringFieldEditor(Ros2PreferenceConstants.P_AUTHOR_NAME, "author name, if no information in model", //$NON-NLS-1$
				parent));
		addField(new StringFieldEditor(Ros2PreferenceConstants.P_AUTHOR_MAIL, "author mail, if no information in model", //$NON-NLS-1$
				parent));
		addField(new StringFieldEditor(Ros2PreferenceConstants.P_MAINTAINER_NAME,
				"maintainer name, if no information in model", parent)); //$NON-NLS-1$
		addField(new StringFieldEditor(Ros2PreferenceConstants.P_MAINTAINER_MAIL,
				"maintainer mail, if no information in model", parent)); //$NON-NLS-1$
	}

	/**
	 * replace ROS 2 distribution name in paths, if distribution changes
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (event.getSource() == distroEditor) {
			Composite parent = getFieldEditorParent();
			String oldDistro = (String) event.getOldValue();
			String newDistro = (String) event.getNewValue();
			int i = 0;
			for (String item : pathEditor.getListControl(parent).getItems()) {
				if (item.contains(oldDistro)) {
					String newPath = item.replaceAll(oldDistro, newDistro);
					pathEditor.getListControl(parent).setItem(i, newPath);
				}
				i++;
			}
		}
	}

	@Override
	public void init(IWorkbench workbench) {
	}
}
