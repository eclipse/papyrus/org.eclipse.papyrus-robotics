/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.build

import java.util.List
import java.util.Map
import java.util.Set
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillSemantic
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillRealizBuildUtils
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.SkillUtils.*

/**
 * Create CMakeLists file for a list of skill realizations
 */
class CreateSkillRealizPackageXML {
	static def createPackageXML(Package model, Set<SkillDefinition> skills, List<String> serviceDependNames) '''
		<?xml version="1.0"?>
		<?xml-model href="http://download.ros.org/schema/package_format3.xsd" schematypens="http://www.w3.org/2001/XMLSchema"?>
		<package format="3">
		  <name>«model.realizationPackageName»</name>
		  <version>0.0.0</version>
		  <description>«model.name» package</description>
		  <maintainer email="«model.maintainerMail»">«model.maintainerName»</maintainer>
		  <license>Apache2.0</license>
		  <url type="website">https://eclipse.org/papyrus</url>
		  <author email="«model.authorMail»">«model.authorName»</author>
		
		  <buildtool_depend>ament_cmake</buildtool_depend>
		
		  <build_depend>nav2_common</build_depend>
		  <build_depend>rclcpp</build_depend>
		  <build_depend>rclcpp_action</build_depend>
		  <build_depend>behaviortree_cpp_v3</build_depend>
		  <build_depend>nav2_behavior_tree</build_depend>
		  «FOR pkgName : serviceDependNames»
		    <build_depend>«pkgName»</build_depend>
		  «ENDFOR»
		
		  <exec_depend>rclcpp</exec_depend>
		  <exec_depend>rclcpp_action</exec_depend>
		  <exec_depend>behaviortree_cpp_v3</exec_depend>
		  <exec_depend>nav2_behavior_tree</exec_depend>
		  «FOR pkgName : serviceDependNames»
		    <exec_depend>«pkgName»</exec_depend>
		  «ENDFOR»
		
		  <test_depend>ament_lint_auto</test_depend>
		  <test_depend>ament_lint_common</test_depend>
		
		  <export>
		  <build_type>ament_cmake</build_type>
		  </export>
		</package>
	'''

	static def generate(IPFileSystemAccess fileAccess, Package pkg, Map<SkillDefinition, SkillSemantic> skdefToSemanticsMap) {
		val serviceDependNames = SkillRealizBuildUtils.calcDependencies(skdefToSemanticsMap)
		fileAccess.generateFile("package.xml", createPackageXML(pkg, skdefToSemanticsMap.keySet, serviceDependNames).toString)
	}
}
