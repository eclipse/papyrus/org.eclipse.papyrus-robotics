/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.jobs;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.papyrus.robotics.ros2.codegen.common.RoboticsTContext;
import org.eclipse.papyrus.robotics.ros2.codegen.common.utils.TransformationUtils;
import org.eclipse.papyrus.robotics.ros2.codegen.python.PythonSpecificTransformations;
import org.eclipse.uml2.uml.Package;

/**
 * Job for generating Python code
 */
public class PythonGenJob extends Job {
	final Package pkg;
	final IProject project;
	final Map<String, Boolean> rewriteMap;

	public PythonGenJob(Package pkg, IProject project) {
		this(pkg, project, null);
	}

	public PythonGenJob(Package pkg, IProject project, Map<String, Boolean> rewriteMap) {
		super("Generate ROS/Python code"); //$NON-NLS-1$
		this.pkg = pkg;
		this.project = project;
		this.rewriteMap = rewriteMap;
	}

	@Override
	public IStatus run(IProgressMonitor monitor) {
		// execute the task ...
		RoboticsTContext current = new RoboticsTContext();
		RoboticsTContext.current = current;
		current.lst = new PythonSpecificTransformations();
		current.rewriteMap = rewriteMap;
		TransformationUtils.executeRosTranformation(current, pkg, project, monitor);
		return Status.OK_STATUS;
	}
};
