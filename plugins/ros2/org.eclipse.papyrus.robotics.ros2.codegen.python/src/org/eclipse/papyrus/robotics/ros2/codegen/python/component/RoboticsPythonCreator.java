/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.common.base.file.IFileExists;
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;
import org.eclipse.papyrus.designer.languages.python.codegen.transformation.PythonModelElementsCreator;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.robotics.codegen.common.component.CodeSkeleton;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.xtext.xbase.lib.Exceptions;

/**
 * A variant of the PythonModelElementsCreator that creates a skeleton for
 * the implementation file and creates the implementation file only,
 * if it does not yet exist.
 */
public class RoboticsPythonCreator extends PythonModelElementsCreator {

	static final String _SKEL = "_skel."; //$NON-NLS-1$

	Package currentModel;

	/**
	 * Constructor.
	 *
	 * @param fileAccess
	 *            the file system access
	 * @param skeletonFolder
	 *            the folder, into which skeletons should be placed (with a trailing "/")
	 */
	public RoboticsPythonCreator(IProject project, IPFileSystemAccess fileAccess) {
		super(project, fileAccess);
	}

	@Override
	public void createPackageableElement(PackageableElement element, IProgressMonitor monitor) {
		currentModel = PackageUtil.getRootPackage(element);
		// create __init__ for model itself
		createPackageableElementFile(currentModel, monitor);
		super.createPackageableElement(element, monitor);
	}

	/**
	 * If the file is a skeleton file and the user source code file (in src) does not exist yet, generate this
	 * file as well.
	 * 
	 * @see org.eclipse.papyrus.designer.languages.common.base.ModelElementsCreator#generateFile(java.lang.String, java.lang.String)
	 *
	 * @param fileName
	 *            the file to generate
	 * @param content
	 */
	@Override
	protected void generateFile(String fileName, String content) {
		// don't generate, if indexer is active (non-idle)
		if (TransformationContext.monitor.isCanceled()) {
			// use xtend trick to throw a non-declared exception
			throw Exceptions.sneakyThrow(new TransformationException(ExecuteTransformationChain.USER_CANCEL));
		}
		TransformationContext.monitor.subTask("generate file " + fileName); //$NON-NLS-1$
		TransformationContext.monitor.worked(1);

		if (currentModel != null) {
			content = ComponentHeader.getHeader(currentModel) + content;
		}
		String postfix = CodeSkeleton.POSTFIX + StringConstants.DOT + pythonExt;
		if (fileName.endsWith(postfix)) {
			// write file with additional _skel postfix
			String srcFileName = fileName;
			fileName = fileName.replace(postfix, CodeSkeleton.POSTFIX + _SKEL + pythonExt);
			if (fileSystemAccess instanceof IFileExists) {
				IFileExists existsFSA = (IFileExists) fileSystemAccess;
				if (!existsFSA.existsFile(srcFileName)) {
					super.generateFile(srcFileName, content);
				}
			}
		}
		super.generateFile(fileName, content);
	}

	/**
	 * @see org.eclipse.papyrus.designer.languages.common.base.ModelElementsCreator#getFileName(org.eclipse.uml2.uml.NamedElement)
	 *
	 * @param element
	 * @return the filename prefixed either with the "normal" src-gen or the skeleton folder
	 */
	@Override
	public String getFileName(NamedElement element) {
		// remove first segment of filename
		// Package model = PackageUtil.getRootPackage(element);
		String fileName = super.getFileName(element);
		return fileName; // .replaceFirst(super.getFileName(model) + File.separator, StringConstants.EMPTY);
	}

	/**
	 * use absolute imports (otherwise, imports are not found after installation into ROS 2 install folders)
	 */
	@Override
	protected boolean useRelativeImports() {
		return false;
	}
}
