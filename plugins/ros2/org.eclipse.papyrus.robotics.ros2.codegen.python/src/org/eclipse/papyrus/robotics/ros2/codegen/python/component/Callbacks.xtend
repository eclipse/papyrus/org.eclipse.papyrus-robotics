/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import org.eclipse.papyrus.designer.languages.common.profile.Codegen.TemplateBinding
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException
import org.eclipse.papyrus.robotics.codegen.common.utils.ApplyProfiles
import org.eclipse.papyrus.robotics.core.utils.FunctionUtils
import org.eclipse.papyrus.robotics.profile.robotics.components.ActivityPort
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.Port

import static org.eclipse.papyrus.robotics.ros2.codegen.python.utils.RosPythonTypes.*

import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.applyApp
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*
import static extension org.eclipse.papyrus.robotics.core.utils.FunctionUtils.getFunction
import static extension org.eclipse.papyrus.robotics.core.utils.InteractionUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.RosHelpers.*

/**
 * provide the callback functions (via bind) for the various kind of ports
 */
class Callbacks {
	final static String PUSH = "PUSH or SEND"
	final static String QUERY = "QUERY"
	final static String ACTION = "ACTION"

	final static String WHOLE_PKG = "Please note that code gets generated for the whole ROS 2 package, not only for the currently open component"
	
	/**
	 * Return the of callback method (which corresponds to activity handling the port)
	 * and provide additional parameter
	 */
	def static String callBackMethodForPush(Class component, Port port) {
		val activity = component.getActivityForPort(port)
		checkActivity(activity, PUSH, port)
		val fct = activity.getFunction(FunctionKind.HANDLER)
		checkFunction(fct, PUSH, activity, port)
		// obtain function that has been copied into the node (in liftFunction)
		val fctCopy = component.getOwnedBehavior(fct.name)
		if (fctCopy.specification !== null) {
			// use commObject name as parameter
			fctCopy.specification.createOwnedParameter("commobj", port.commObject)

			return '''self.«fct.name»'''
		}
	}

	/**
	 * Add parameters to return type
	 */
	def static void clientCallBackMethodForService(Class component, Port port) {
		val activityPort = component.getActivityForPort(port)
		checkActivity(activityPort, QUERY, port)
		val resultFct = activityPort.getFunction(FunctionKind.HANDLER, "result")
		if (resultFct !== null) {
			// obtain function that has been copied into the node (in liftFunction)
			val clientType = getType(port, "rclpy::client::Client");
			val lResultFct = component.getOwnedBehavior(resultFct.name)
			// add parameter for goal_response callback: future
			val resultParam = lResultFct.specification.createOwnedParameter("future", clientType)
			ApplyProfiles.applyCommonProfile(resultParam)
			val resultTpl = resultParam.applyApp(TemplateBinding)
			resultTpl.actuals.add(port.serviceType)
		}
	}

	/**
	 * Return the callback method (which corresponds to activity handling of the port)
	 * and provide additional parameter
	 */
	def static String serverCallBackMethodForService(Class component, Port port) {
		val activityPort = component.getActivityForPort(port)
		checkActivity(activityPort, QUERY, port)
		val fct = activityPort.getFunction(FunctionKind.HANDLER);
		checkFunction(fct, QUERY, activityPort, port)
		// obtain function that has been copied into the node (in liftFunction)
		val fctCopy = component.getOwnedBehavior(fct.name)
		if (fctCopy.specification !== null) {
			// request/response parameter (and boolean return value)
			fctCopy.specification.createOwnedParameter("request", port.serviceType)
			fctCopy.specification.createOwnedParameter("response", port.serviceType)
			// use bool from ROS 2 primitive type library (not all types there work without additional includes)
			val retParam = fctCopy.specification.createOwnedParameter("ret",
				port.getRosPrimitiveType("primitive::bool"))
			retParam.direction = ParameterDirectionKind.RETURN_LITERAL

			return '''self.«fct.name»'''
		}
	}

	/**
	 * Adapt client side callback methods, i.e. add necessary parameters to their signature.
	 * The actual association with a specific call must be done by the user code during setup of
	 * the call options.
	 *  - The goal callback, that gets called if a client sets a new goal.
	 *  - The cancel callback for canceling a previous goal
	 *  - The accept callback to accept ... (What?)
	 * The code leaves many parameters untyped for simplicity 
	 * 
	 * @param component a component
	 * @param port a port for which to provide callbacks
	 */
	def static void clientCallsbacksForAction(Class component, Port port) {
		val activityPort = component.getActivityForPort(port)
		checkActivity(activityPort, ACTION, port)
		val feedbackFct = activityPort.getFunction(FunctionKind.HANDLER, FunctionUtils.R_FEEDBACK)
		// no need to check functions, as their use is optional (developer can also
		// define his own lambda's)
		if (feedbackFct !== null) {
			// add parameters for feedback callback: handle and feedback
			val lFeedbackFct = component.getOwnedBehavior(feedbackFct.name)
			lFeedbackFct.specification.createOwnedParameter("handle", null)
			lFeedbackFct.specification.createOwnedParameter("feedback", null)
		}

		val resultFct = activityPort.getFunction(FunctionKind.HANDLER, FunctionUtils.R_RESULT)
		if (resultFct !== null) {
			// add parameter for result callback: result
			val lResultFct = component.getOwnedBehavior(resultFct.name)
			lResultFct.specification.createOwnedParameter("result", null)
		}

		val goalFct = activityPort.getFunction(FunctionKind.HANDLER, FunctionUtils.GOAL)
		if (goalFct !== null) {
			// obtain function that has been copied into the node (in liftFunction)
			val lGoalFct = component.getOwnedBehavior(goalFct.name)
			// add parameter for future
			lGoalFct.specification.createOwnedParameter("future", null)
		}
	}

	def static String serverCallsbacksForAction(Class component, Port port) {
		val activityPort = component.getActivityForPort(port)
		checkActivity(activityPort, ACTION, port)
		val goalFct = activityPort.getFunction(FunctionKind.HANDLER, FunctionUtils.GOAL)
		checkFunction(goalFct, ACTION + "/feedback", activityPort, port)
		val cancelFct = activityPort.getFunction(FunctionKind.HANDLER, FunctionUtils.P_CANCEL)
		checkFunction(cancelFct, ACTION + "/result", activityPort, port)
		val acceptedFct = activityPort.getFunction(FunctionKind.HANDLER, FunctionUtils.P_ACCEPTED)
		checkFunction(acceptedFct, ACTION + "/goal", activityPort, port)
		// obtain function that has been copied into the node (in liftFunction)
		val lGoalFct = component.getOwnedBehavior(goalFct.name)
		val lCancelFct = component.getOwnedBehavior(cancelFct.name)
		val lAcceptedFct = component.getOwnedBehavior(acceptedFct.name)
		// val goalHandle = getRosType(port, "ros2Library::rclcpp_action::ClientGoalHandle");
		val goalResponse = getType(port, "rclpy::action::GoalResponse");
		val cancelResponse = getType(port, "rclpy::action::CancelResponse");

		lGoalFct.specification.createOwnedParameter("goal", goalResponse)
		val goalRetParam = lGoalFct.specification.createOwnedParameter("return", goalResponse)
		goalRetParam.direction = ParameterDirectionKind.RETURN_LITERAL

		// add parameter for cancel: goal_handle and cancelReponse (return value)
		lCancelFct.specification.createOwnedParameter("goal_handle", goalResponse)
		val cancelRetParam = lCancelFct.specification.createOwnedParameter("return", cancelResponse)
		cancelRetParam.direction = ParameterDirectionKind.RETURN_LITERAL

		// add parameter for goal_response callback: future
		lAcceptedFct.specification.createOwnedParameter("goal_handle", null)
		return '''
			execute_callback = self.«acceptedFct.name»,
			goal_callback = self.«goalFct.name»,
			cancel_callback = self.«cancelFct.name»'''
	}

	/**
	 * Utility function for error handling
	 */
	def static void checkActivity(ActivityPort activityPort, String portKind, Port port) {
		if (activityPort === null) {
			throw new TransformationException(
				String.format("The %s port \"%s\" of component \"%s\" is not connected with any activity port. " + WHOLE_PKG,
					portKind, port.name, port.class_.name
				))
		}
	}

	/**
	 * Utility function for error handling
	 */
	def static void checkFunction(Behavior function, String portKind, ActivityPort activity, Port port) {
		if (function === null) {
			throw new TransformationException(
				String.format("No handler function (for %s) is found for activity port \"%s\" associated with port \"%s\" of component \"%s\". " + WHOLE_PKG,
					portKind, activity.base_Port.name, port.name, port.class_.name));
		}
	}
}
