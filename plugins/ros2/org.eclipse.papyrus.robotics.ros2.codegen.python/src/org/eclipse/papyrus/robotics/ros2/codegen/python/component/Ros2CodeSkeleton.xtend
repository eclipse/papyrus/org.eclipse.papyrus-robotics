/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import org.eclipse.papyrus.robotics.codegen.common.component.CodeSkeleton
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.OpaqueBehavior

/**
 * Create a template for the code that needs to be filled by a developer.
 * 
 */
class Ros2CodeSkeleton extends CodeSkeleton {

	new(Class component) {
		super(component);
	}

	override addConstrOp(Class skeleton) {
		val constructorOp = Constructor.addConstrOp(skeleton)
		val ob = component.createOwnedBehavior(skeleton.name, UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		constructorOp.methods.add(ob)
		ob.languages.add("Python")
		ob.bodies.add('''
			super().__init__(instName)
		''')
		return constructorOp
	}

	override moveFunction(Class skeleton, Function function) {
		super.moveFunction(skeleton, function)
//		if (function.base_Class instanceof OpaqueBehavior) {
//			val ob = function.base_Class as OpaqueBehavior
//			ob.getLanguages().add("C++");
//			var code = '''
//				// replace with your code
//				
//				// sample code to write data to an outgoing port
//				«FOR port : skeleton.ownedPorts»
//				«ENDFOR»
//			''';
//			ob.bodies.add(code)
//		}
	}
}
