package org.eclipse.papyrus.robotics.ros2.codegen.python.build

import java.util.List
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Package
import org.eclipse.emf.common.util.UniqueEList

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.*
import static extension org.eclipse.papyrus.designer.languages.python.codegen.transformation.PythonCodeGenUtils.getPythonQName

class CreateCompSetupPy {
	def static createSetupfile(Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system)'''
	from setuptools import setup
	import glob
	
	package_name = '«pkg.pkgName»'
	
	setup(
		name=package_name,
		version='0.0.0',
		packages=[package_name, «FOR compPkg : componentsInPkg.usedPackages SEPARATOR ', '»'«pkg.pkgName».«compPkg.name.toLowerCase»'«ENDFOR»],
		data_files = [
				('share/ament_index/resource_index/packages',
					['resource/' + package_name]),
				('share/' + package_name, ['package.xml']),
				('share/' + package_name + '/launch', glob.glob('launch/*.py')),
				('share/' + package_name + '/launch', glob.glob('launch/*.launch')),
				('share/' + package_name + '/launch/cfg', glob.glob('launch/cfg/*.yaml')),
			],
		install_requires=['setuptools'],
		zip_safe=True,
		maintainer='«pkg.maintainerName»',
		maintainer_email='«pkg.maintainerMail»',
		description='TODO: Package description',
		license='TODO: License declaration',
		tests_require=['pytest'],
		entry_points={
			'console_scripts': [
				«FOR component : componentsInPkg»
					'«component.name» = «pkg.pkgName».«component.getPythonQName»_main:main',
				«ENDFOR»
				]
			}
		)
	'''

	protected static def usedPackages(List<Class> componentsInPkg) {
		val pkgs = new UniqueEList<Package>()
		for (component : componentsInPkg) {
			pkgs.add(component.nearestPackage)
		}
		return pkgs
	}
	

	static def generate(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) {
		fileAccess.generateFile("setup.py", createSetupfile(pkg, allComponents, componentsInPkg, system).toString)
	}
}