/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https:#www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.getAuthorMail
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.getAuthorName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.getMaintainerMail
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.PackageXMLUtils.getMaintainerName

/**
 * Provide a simple header for components in order to comply with ROS 2 conventions
 */
class ComponentHeader {
	static def getHeader(Package model) '''
		# --------------------------------------------------------
		# Copyright (c)
		#
		# contributions by «model.authorName»
		#                  «model.authorMail»
		# maintained by    «model.maintainerName»
		#                  «model.maintainerMail»
		
	'''
}
