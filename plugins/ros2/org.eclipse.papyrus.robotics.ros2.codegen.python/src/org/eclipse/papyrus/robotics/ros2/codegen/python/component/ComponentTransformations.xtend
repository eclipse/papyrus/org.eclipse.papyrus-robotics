/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import java.util.ArrayList
import org.eclipse.core.resources.IProject
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.papyrus.designer.languages.python.profile.python.Module
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.papyrus.robotics.core.utils.PortUtils
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition
import org.eclipse.papyrus.robotics.profile.robotics.functions.Function
import org.eclipse.papyrus.robotics.profile.robotics.functions.FunctionKind
import org.eclipse.papyrus.robotics.ros2.codegen.common.component.AbstractCompTransformations
import org.eclipse.papyrus.robotics.ros2.codegen.common.message.CreateMsgPackage
import org.eclipse.papyrus.robotics.ros2.codegen.python.utils.ApplyPythonProfile
import org.eclipse.papyrus.robotics.ros2.codegen.python.utils.ProjectTools
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.UMLFactory
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.utils.RosPythonTypes.*

import static extension org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils.varName
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ComponentUtils.isRegistered
import static extension org.eclipse.papyrus.robotics.core.utils.FunctionUtils.getFunctions
import static extension org.eclipse.papyrus.robotics.core.utils.InteractionUtils.*
import static extension org.eclipse.papyrus.robotics.core.utils.ParameterUtils.getAllParameters
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.component.ComponentTransformationUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.RosHelpers.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.component.CallbackGroups.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.Constructor.createConstructor
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.CreateMain.createMain
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.ParameterTransformations.declareParameters
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.ParameterTransformations.initParameters
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.ParameterTransformations.moveParameters
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.applyApp

class ComponentTransformations extends AbstractCompTransformations {

	new(IPFileSystemAccess fileAccess, IProject genProject) {
		super(fileAccess, genProject)
	}

	/**
	 * 
	 */
	def createOnConfigureMethod(Class component) {
		var op = component.createOwnedOperation('on_configure', null, null)
		var ob = component.createOwnedBehavior('on_configure', UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		op.methods.add(ob)
		op.createOwnedParameter('state', getType(component, "rclpy::lifecycle::State"))
		ob.languages.add("Python")
		ob.bodies.add('''
			«FOR activity : component.activities»
				«val activityCl = activity.base_Class»
				«val period = activity.period»
				«val activateFcts = activity.getFunctions(FunctionKind.ON_CONFIGURE)»
				«val periodicFcts = activity.getFunctions(FunctionKind.PERIODIC)»
				«IF activateFcts.size > 0»
					«FOR activateFct : activateFcts»
						self.«activateFct.name»()
					«ENDFOR»
				«ENDIF»
				«IF period !== null && periodicFcts.size > 0»
					# periodic execution («period») for «activityCl.name» using a timer
					«FOR periodicFct : periodicFcts»
						self.timer_«activityCl.name» = self.create_timer(«period.convertPeriod», self.«periodicFct.name», self.«activityCl.tCallbackGroupName»)
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			return TransitionCallbackReturn.SUCCESS
		''')
	}

	/**
	 * Convert a method that uses a unit in seconds or milliseconds to a number
	 * passed to the Python call (unlike in C++, there are no ChronoLiterals)
	 */
	def static convertPeriod(String period) {
		if (period.contains("ms")) {
			val floatPeriod = Float.parseFloat(period.replace("ms", "")) / 1000
			return floatPeriod.toString
		} else if (period.contains("s")) {
			return period.replace("s", "")
		}
		else {
			// return unmodified period
			return period;
		}
	}

	/**
	 * 
	 */
	def createOnActivateMethod(Class component) {
		var op = component.createOwnedOperation('on_activate', null, null)
		var ob = component.createOwnedBehavior('on_activate', UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		ob.languages.add("Python")
		op.methods.add(ob)
		op.createOwnedParameter('state', getType(component, "rclpy::lifecycle::State"))
		ob.bodies.add('''
			«FOR activity : component.activities»
				«val activateFcts = activity.getFunctions(FunctionKind.ON_ACTIVATE)»
				«IF activateFcts.size > 0»
					«FOR activateFct : activateFcts»
						self.«activateFct.name»()
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			return TransitionCallbackReturn.SUCCESS
		''')
	}

	/**
	 * 
	 */
	def createOnDeactivateMethod(Class component) {
		var op = component.createOwnedOperation('on_deactivate', null, null)
		var ob = component.createOwnedBehavior('on_deactivate',
			UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		ob.languages.add("Python")
		op.methods.add(ob)
		op.createOwnedParameter('state', getType(component, "rclpy::lifecycle::State"))
		ob.bodies.add('''
			«FOR activity : component.activities»
				«val activityCl = activity.base_Class»
				«val deactivateFcts = activity.getFunctions(FunctionKind.ON_DEACTIVATE)»
				«val periodicFcts = activity.getFunctions(FunctionKind.PERIODIC)»
				«IF deactivateFcts.size > 0»
					«FOR deactivateFct : deactivateFcts»
						self.«deactivateFct.name»()
					«ENDFOR»
				«ENDIF»
				«IF periodicFcts.size > 0»
					«FOR periodicFct : periodicFcts»
						self.timer_«activityCl.name».cancel()
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			return TransitionCallbackReturn.SUCCESS
		''')
	}

	/**
	 * 
	 */
	def createOnCleanupMethod(Class component) {
		var op = component.createOwnedOperation('on_cleanup', null, null)
		var ob = component.createOwnedBehavior('on_cleanup', UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		ob.languages.add("Python")
		op.methods.add(ob)
		op.createOwnedParameter('state', getType(component, "rclpy::lifecycle::State"))
	}

	/**
	 * 
	 */
	def createOnShutdownMethod(Class component) {
		var op = component.createOwnedOperation('on_shutdown', null, null)
		var ob = component.createOwnedBehavior('on_shutdown', UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		ob.languages.add("Python")
		op.methods.add(ob)
		op.createOwnedParameter('state', getType(component, "rclpy::lifecycle::State"))
		ob.bodies.add('''
			«FOR activity : component.activities»
				«val activityCl = activity.base_Class»
				«val shutdownFcts = activity.getFunctions(FunctionKind.ON_SHUTDOWN)»
				«val periodicFcts = activity.getFunctions(FunctionKind.PERIODIC)»
				«IF shutdownFcts.size > 0»
					«FOR shutdownFct : shutdownFcts»
						self.«shutdownFct.name»()
					«ENDFOR»
				«ENDIF»
				«IF periodicFcts.size > 0»
					«FOR periodicFct : periodicFcts»
						self.timer_«activityCl.name».destroy()
					«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			return TransitionCallbackReturn.SUCCESS
		''')
	}

	/**
	 * Create dependencies for types that are added directly to the code, i.e.
	 * don't appear as types
	 * Apply Module stereotype
	 */
	def createROS2Dependencies(Class component) {
		val lcTransitionCallbackReturnSC = getType(component, "rclpy::lifecycle::TransitionCallbackReturn")
		component.createDependency(component.getRclPy)

		if (lcTransitionCallbackReturnSC instanceof Classifier) {
			component.createDependency(lcTransitionCallbackReturnSC)
		}
	
		for (port : PortUtils.getAllPorts(component)) {
			if (port.communicationPattern.query) {
				val msg_type = port.serviceType
				msg_type.name = port.serviceType.externalName.getSrvName
				val pkg = msg_type.package
				pkg.name = 'srv'
				ApplyPythonProfile.apply(pkg)
				pkg.applyApp(Module)
				component.createDependency(msg_type)
			} else if (port.communicationPattern.action) {
				// TODO
			} else {
				val msg_type = port.commObject
				// msg_type.name = port.commObject.externalName
				val pkg = msg_type.package
				ApplyPythonProfile.apply(pkg)
				pkg.applyApp(Module)
				component.createDependency(msg_type)
			}
		}
	}

	/**
	 * 
	 */
	def static String getSrvName(String externalName) {
		val index = externalName.lastIndexOf("::")
		if (index != -1) {
			return externalName.substring(index + 2)
		}
		return ''
	}

	/**
	 * 
	 */
	def static hasProvidedsPort(Class component) {
		for (port : PortUtils.getAllPorts(component)) {
			val cp = port.communicationPattern
			if (port.provideds.size() > 0 && (cp.isPush || cp.isPubSub || cp.isSend)) {
				return true
			}
		}
		return false
	}

	/**
	 * Create a parent python package that contains the component's package. 
	 */
	def createParentPkg(Class component) {
		val aPkg = UMLFactory.eINSTANCE.createPackage
		aPkg.name = genProject.name.toLowerCase
		aPkg.packagedElements.add(component.nearestPackage)
	}

	def static createPubsSubsAttrs(Class component) {
		for (port : PortUtils.getAllPorts(component)) {
			val cp = port.communicationPattern
			if (cp.isPush || cp.isPubSub) {
				if (port.provideds.size() > 0) {
					val rosPublisher = getType(port, "rclpy::lifecycle::Publisher")
					component.createOwnedAttribute('''«port.varName»_pub_''', rosPublisher)
				} else if (port.requireds.size() > 0) {
					val rosSubscriber = getType(port, "rclpy::subscription::Subscription")
					component.createOwnedAttribute('''«port.varName»_sub_''', rosSubscriber)
				}
			}
		}
	}

	def static createSendAttrs(Class component) {
		for (port : PortUtils.getAllPorts(component)) {
			if (port.communicationPattern.isSend) {
				if (port.provideds.size() > 0) {
					val rosSubscriber = getType(port, "rclpy::subscription::Subscription")
					component.createOwnedAttribute('''«port.varName»_recv_''', rosSubscriber)
				} else if (port.requireds.size() > 0) {
					val rosPublisher = getType(port, "rclpy::lifecycle::Publisher")
					component.createOwnedAttribute('''«port.varName»_send_''', rosPublisher)
				}
			}
		}
	}

	def static createServiceAttrs(Class component) {
		for (port : PortUtils.getAllPorts(component)) {
			if (port.communicationPattern.isQuery) {
				if (port.provideds.size() > 0) {
					val rosService = getType(port, "rclpy::service::Service");
					component.createOwnedAttribute('''«port.varName»_srv_''', rosService);
				} else if (port.requireds.size() > 0) {
					val rosClient = getType(port, "rclpy::client::Client");
					component.createOwnedAttribute('''«port.varName»_client_''', rosClient);
				}
			}
		}
	}

	/**
	 * Add action attributes
	 */
	def static createActionAttrs(Class component) {
		for (port : PortUtils.getAllPorts(component)) {
			if (port.communicationPattern.isAction) {
				if (port.provideds.size() > 0) {
					val rosService = getType(port, "rclpy::action::ActionServer");
					component.createDependency(rosService);
					component.createOwnedAttribute('''«port.varName»_actsrv_''', rosService);
				} else if (port.requireds.size() > 0) {
					val rosClient = getType(port, "rclpy::action::ActionClient");
					component.createDependency(rosClient);
					component.createOwnedAttribute('''«port.varName»_actcli_''', rosClient);
				}
			}
		}
	}

	/**
	 * Remove functions that are not referenced by activities (this can happen after
	 * deletion of a function  an activity)
	 */
	def static removeUnrefFunctions(Class component) {
		val cd = UMLUtil.getStereotypeApplication(component, ComponentDefinition)
		val fctList = new ArrayList<Function>();

		for (activity : cd.activities) {
			fctList.addAll(activity.functions)
		}
		for (pe : component.nearestPackage.packagedElements.clone) {
			if (pe instanceof OpaqueBehavior) {
				val fct = UMLUtil.getStereotypeApplication(pe, Function)
				if (fct !== null && !fctList.contains(fct)) {
					pe.destroy();
				}
			}
		}
	}

	/**
	 * 
	 */
	def static void createTimerAttrs(Class component) {
		for (activity : component.activities) {
			val period = activity.period
			if (period !== null) {
				val timerBase = ElementUtils.getQualifiedElementFromRS(component, "rclpy::timer::Timer") as Type;
				component.createOwnedAttribute('''timer_«activity.base_Class.name»_''', timerBase);
			}
		}
	}

	override componentTrafo(Class component, CreateMsgPackage msgPkgCreator) {
		msgPkgCreator.createMessagesOrServices(component)
		component.createParentPkg
		ApplyPythonProfile.apply(component)
		if (genProject === null) {
			throw new TransformationException(ExecuteTransformationChain.USER_CANCEL);
		}
		component.createOnConfigureMethod
		component.createOnActivateMethod
		component.createOnDeactivateMethod
		component.createOnCleanupMethod
		component.createOnShutdownMethod
		component.liftFunctions
		component.createConstructor
		component.createROS2Dependencies

		if (component.isRegistered) {
		}

		if (component.hasExternalCode) {
			new Ros2CodeSkeleton(component).createSkeleton();
		}
		// call main after skeleton creation
		component.createMain;

		// val stdString = getType(node, "ros2Library::std_msgs::String");
		// node.createDependency(stdString)
		component.removeActivities
		component.createPubsSubsAttrs
		component.createSendAttrs
		component.createServiceAttrs
		component.createActionAttrs
		component.createTimerAttrs

		if (component.allParameters.size > 0) {
			component.declareParameters
			component.initParameters
		}
		// move parameter needs to be called even if there are not parameters
		// as it also removes the nested class holding parameters
		component.moveParameters
	}

	override componentCodegen(Class component, CreateMsgPackage msgPkgCreator) {
		val codeGen = new RoboticsPythonCreator(genProject, fileAccess);

		component.removeTemplateSig
		component.removePorts
		component.removeUnrefFunctions;
		TransformationContext.current.project = genProject
		// RoboticsTContext.current.getProjet
		ProjectTools.genCode(codeGen, component)
	}
}
