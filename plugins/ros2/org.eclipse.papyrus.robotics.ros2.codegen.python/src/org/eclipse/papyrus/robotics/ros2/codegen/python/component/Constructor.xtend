/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import org.eclipse.papyrus.robotics.codegen.common.utils.ApplyProfiles
import org.eclipse.papyrus.robotics.core.utils.PortUtils
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.papyrus.designer.languages.python.profile.python.Module

import static org.eclipse.papyrus.robotics.ros2.codegen.python.utils.RosPythonTypes.*

import static extension org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils.varName
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.TopicUtils.getTopic
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.component.CallbackGroups.*
import static extension org.eclipse.papyrus.robotics.core.utils.InteractionUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.RosHelpers.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.Callbacks.*
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.component.ComponentTransformations.getSrvName
import org.eclipse.papyrus.robotics.profile.robotics.components.Activity
import org.eclipse.uml2.uml.LiteralString
import org.eclipse.papyrus.designer.languages.python.profile.PythonProfileResource
import org.eclipse.papyrus.designer.transformation.base.utils.ApplyProfileUtils
import org.eclipse.uml2.uml.Interface
import org.eclipse.uml2.uml.Dependency

/**
 * Manage constructor creation, including port
 */
class Constructor {

	def static createConstructor(Class component) {
		val lcNodeSC = getType(component, "rclpy::lifecycle::Node")	
		if (lcNodeSC instanceof Classifier) {
			component.createGeneralization(lcNodeSC)
		}
		val op = addConstrOp(component)
		addConstrMethod(component, op)
		val qosRPolicy = getType(component, "rclpy::qos::ReliabilityPolicy")	
		val qosHPolicy = getType(component, "rclpy::qos::HistoryPolicy")
		component.createDependency(qosRPolicy)
		component.createDependency(qosHPolicy)
	}

	/**
	 * Add an init operation (constructor) that creates the ROS 2 publishers/subscribers/clients/... for ports 
	 */
	def static addConstrOp(Class component) {
		val string = getType(component, "PrimitiveTypes::String")
		val init = component.createOwnedOperation(component.name, null, null)
		init.createOwnedParameter("instName", string)
		var create = StereotypeUtil.applyApp(init, Create)
		if (create === null) {
			ApplyProfiles.applyStdProfile(init)
			create = StereotypeUtil.applyApp(init, Create)
		}
		return init
	}

	/**
	 * Add a method body to the constructor operation
	 */
	def static addConstrMethod(Class component, Operation constructorOp) {
		val ob = component.createOwnedBehavior(component.name, UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		constructorOp.methods.add(ob)
		ob.languages.add("Python")
		ob.bodies.add('''
			super().__init__(instName)
			«FOR activity : component.activities»
				«activity.createCallbackGroupVar(component)»
			«ENDFOR»
			«FOR port : PortUtils.getAllPorts(component)»
				«IF port.communicationPattern !== null»
					«val pattern = port.communicationPattern»
					«IF pattern.isPush || pattern.isPubSub»
						«port.createPush»

					«ELSEIF pattern.isSend»
						«port.createSend»

					«ELSEIF pattern.isQuery»
						«port.createQuery»

					«ELSEIF pattern.isAction»
						«port.createAction»

					«ELSEIF pattern.isEvent»
					««« TODO: unsupported
					«ENDIF»
				«ENDIF»
			«ENDFOR»
		''')
	}

	/**
	 * Create a callback_group attribute for each activity
	 */
	def static void createCallbackGroupVar(Activity activity, Class component) {
		val cbgType = getType(component, "rclpy::callback_groups::MutuallyExclusiveCallbackGroup");
		val cbgAttr = component.createOwnedAttribute(activity.base_Class.callbackGroupName, cbgType);
		val defaultV = cbgAttr.createDefaultValue(null, null, UMLPackage.eINSTANCE.literalString) as LiteralString
		defaultV.setValue("MutuallyExclusiveCallbackGroup()")
		if (activity.period !== null) {
			val tCbgAttr = component.createOwnedAttribute(activity.base_Class.tCallbackGroupName, cbgType);
			val tDevaultV = tCbgAttr.createDefaultValue(null, null, UMLPackage.eINSTANCE.literalString) as LiteralString
			tDevaultV.setValue(defaultV.getValue())
		}
	}


	/**
	 * Create a publisher or subscriber
	 */
	def static createPush(Port port) '''
		«IF port.provideds.size() > 0»
			self.«port.varName»_pub_ = self.create_publisher(«port.commObject.name», "«port.topic»",
				«port.qos("1")», callback_group = self.«port.callbackGroupName»)
			«««port.varName»_pub_->on_activate()
		«ELSEIF port.requireds.size() > 0»
			defaultQoSProfile = rclpy.qos.QoSProfile(
					reliability=ReliabilityPolicy.BEST_EFFORT,
					history=HistoryPolicy.KEEP_LAST,
					depth = 100)
			self.«port.varName»_sub_ = self.create_subscription(«port.commObject.name», "«port.topic»", «port.class_.callBackMethodForPush(port)»,
				qos_profile = defaultQoSProfile, callback_group = self.«port.callbackGroupName»)
		«ENDIF»
	'''

	/**
	 * Create send (publisher and subscriber as well)
	 */
	def static createSend(Port port) '''
		«IF port.provideds.size() > 0»
««« Subscriber
			defaultQoSProfile = rclpy.qos.QoSProfile(
					reliability=ReliabilityPolicy.BEST_EFFORT,
					history=HistoryPolicy.KEEP_LAST,
					depth = 100)
			self.«port.varName»_recv_ = self.create_subscription(«port.commObject.name», "«port.topic»", «port.class_.callBackMethodForPush(port)», qos_profile = defaultQoSProfile, callback_group = self.«port.callbackGroupName»)
		«ELSEIF port.requireds.size() > 0»
««« Publisher
			self.«port.varName»_send_ = self.create_publisher(«port.commObject.name», "«port.topic»", «port.qos("1")», callback_group = self.«port.callbackGroupName»)
			// directly activate a publisher
««« port.varName»_send_->on_activate();
		«ENDIF»
	'''

	/**
	 * Create a service client or server
	 */
	def static createQuery(Port port) '''
		«IF port.provideds.size() > 0»
			««« Server
			self.«port.varName»_srv_ = self.create_service(«port.serviceType.externalName.getSrvName», "«port.topic»", «port.class_.serverCallBackMethodForService(port)», callback_group = self.«port.callbackGroupName»)
		«ELSEIF port.requireds.size() > 0»
			self.«port.varName»_client_ = self.create_client(«port.serviceType.externalName.getSrvName», "«port.topic»", callback_group = self.«port.callbackGroupName»)
		«ENDIF»
	'''

	/**
	 * Create an action client or server
	 */
	def static createAction(Port port) '''
		«IF port.provideds.size() > 0»
			self.«port.varName»_actsrv_ = ActionServer(
				self, «port.serviceType.externalName.getSrvName», '«port.topic»',
				«port.class_.serverCallsbacksForAction(port)»,
				callback_group = self.«port.callbackGroupName»)
			«port.class_.createSvcDependency(port.serviceType)»
		«ELSE»
			self.«port.varName»_actcli_ = ActionClient(
				self, «port.serviceType.externalName.getSrvName», '«port.topic»',
				callback_group = self.«port.callbackGroupName»)
			«port.class_.clientCallsbacksForAction(port)»
			«port.class_.createSvcDependency(port.serviceType)»
		«ENDIF»
	'''

	/**
	 * Create an event client and server
	 * TODO - not supported
	 */
	def static createEvent(Port port) '''
«««		This pattern is aimed to create run-time configurable notification mechanism.
«««		The client can register to be notified when a particular event happens on the server side.
«««		The server catches the events and sends a message only to clients interested to that
«««		particular event.
«««		The event condition check on the server side must be written by user, the pattern specifies
«««		the semantic of the <RequestedEvent, ReplyMessage, DataToCheck>
«««		There is no equivalent pattern in YARP, but it can be implemented via specialized
«««		client/server devices.
	'''

	/**
	 * Return the QoS associated with a port or defaultQoS, if none is specified
	 * 
	 * The "KEEP_LAST" history setting tells DDS to store a fixed-size buffer of values before they
	 * are sent, to aid with recovery in the event of dropped messages.
	 * use best effort to avoid blocking during execution.
	 */	
	def static qos(Port port, String defaultQoS) {
		val compPort = UMLUtil.getStereotypeApplication(port, ComponentPort)
		if (compPort !== null && compPort.qos !== null && compPort.qos.length > 0) {
			return compPort.qos
		}
		else {
			return defaultQoS
		}
 	}
	
	/**
	 * Helper to create a dependency to a service interface
	 */
	def static void createSvcDependency(Class component, Interface svcType) {
		for (Dependency dep : component.clientDependencies) {
			if (dep.targets.contains(svcType)) {
				// a dependency exists already
				return
			}
		}		
		component.createDependency(svcType)		
	}

	
}


