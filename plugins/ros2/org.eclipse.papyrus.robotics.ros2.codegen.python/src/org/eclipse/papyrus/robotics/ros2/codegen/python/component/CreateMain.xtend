/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import org.eclipse.papyrus.designer.languages.python.profile.python.Main
import org.eclipse.uml2.uml.Class

import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.applyApp
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.utils.RosPythonTypes.*
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*
import static extension org.eclipse.papyrus.robotics.core.utils.ParameterUtils.getAllParameters

import org.eclipse.papyrus.robotics.ros2.codegen.python.utils.ApplyPythonProfile
import org.eclipse.papyrus.robotics.codegen.common.component.CodeSkeleton

class CreateMain {

	/**
	 * Create the main entry point for a class
	 */
	def static createMain(Class component) {
		val mainClass = component.nearestPackage.createOwnedClass(component.name + "_main", false)
		val implClass = component.nearestPackage.getOwnedMember(component.name + CodeSkeleton.POSTFIX) as Class
		ApplyPythonProfile.apply(mainClass)
		val mainST = mainClass.applyApp(Main)
		mainST.body = component.createMainCode.toString
		if (implClass !== null) {
			mainClass.createDependency(implClass);
		}
		else {
			mainClass.createDependency(component);
		}
		mainClass.createDependency(mainClass.getRclPy)
	}

	def static createMainCode(Class component) '''
		rclpy.init()
		«component.name.toLowerCase» = «component.name»«IF component.hasExternalCode»«CodeSkeleton.POSTFIX»«ENDIF»('«component.name»_node')
		«component.name.toLowerCase».get_logger().info('«component.name» node has been created')
		«IF component.allParameters.size > 0»
			«component.name.toLowerCase».declareParameters()
			«component.name.toLowerCase».initParameterVars()
		«ENDIF»
		executor = rclpy.executors.MultiThreadedExecutor()
		executor.add_node(«component.name.toLowerCase»)
		executor.spin()
		rclpy.shutdown()
	'''
	
}
