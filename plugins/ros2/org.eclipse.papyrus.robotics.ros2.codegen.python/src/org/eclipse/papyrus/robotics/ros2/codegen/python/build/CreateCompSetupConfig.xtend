package org.eclipse.papyrus.robotics.ros2.codegen.python.build

import java.util.List
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Package
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName

class CreateCompSetupConfig {
	def static createSetupCfgfile(Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system)'''
	[develop]
	script_dir=$base/lib/«pkg.pkgName»
	[install]
	install_scripts=$base/lib/«pkg.pkgName»
	'''
	static def generate(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) {
		fileAccess.generateFile("setup.cfg", createSetupCfgfile(pkg, allComponents, componentsInPkg, system).toString)
	}
}