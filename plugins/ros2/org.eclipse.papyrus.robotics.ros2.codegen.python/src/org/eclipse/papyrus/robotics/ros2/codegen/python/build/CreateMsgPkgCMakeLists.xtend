/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.robotics.ros2.codegen.python.build

import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.papyrus.designer.languages.common.base.file.ProtSection
import org.eclipse.uml2.uml.Package

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName
import static extension org.eclipse.papyrus.robotics.ros2.codegen.common.utils.MessageUtils.*

/**
 * Create CMakeLists file for a message package
 */
class CreateMsgPkgCMakeLists {
	static def createCMakeLists(Package msgPackage) '''
		cmake_minimum_required(VERSION 3.5.0)
		project(«msgPackage.pkgName»)
		
		# Default to C++14
		if(NOT CMAKE_CXX_STANDARD)
		  set(CMAKE_CXX_STANDARD 14)
		endif()
		
		if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
		  add_compile_options(-Wall -Wextra -Wpedantic)
		endif()
		
		«val requiredPkgs = calcDependencies(msgPackage)»
		«FOR requiredPkg : requiredPkgs»
			find_package(«requiredPkg.name.toLowerCase» REQUIRED)
		«ENDFOR»
		
		# «ProtSection.protSection("dependencies")»
		# «ProtSection.protSection»
		
		«val msgFileNames = msgPackage.getMsgFileNames»
		«val srvFileNames = msgPackage.getSrvFileNames»
		«val actFileNames = msgPackage.getActFileNames»
		«IF msgFileNames.size > 0 || srvFileNames.size > 0 || actFileNames.size > 0»
			find_package(rosidl_default_generators REQUIRED)
		
			# Generate messages, services or actions
			rosidl_generate_interfaces(
				«msgPackage.pkgName»
				«FOR messageFN : msgFileNames»
					"msg/«messageFN»"
				«ENDFOR»
				«FOR serviceFN : srvFileNames»
					"srv/«serviceFN»"
				«ENDFOR»
				«FOR actionFN : actFileNames»
					"action/«actionFN»"
				«ENDFOR»
				DEPENDENCIES «FOR requiredPkg : requiredPkgs SEPARATOR ' '»«requiredPkg.name»«ENDFOR»
			)
		«ENDIF»
		
		«««		# get_default_rmw_implementation(rmw_implementation)
«««		# find_package("${rmw_implementation}" REQUIRED)
«««		# get_rmw_typesupport(typesupport_impls "${rmw_implementation}" LANGUAGE "cpp")
«»		ament_package()
	'''

	static def generate(IPFileSystemAccess fileAccess, Package pkg) {
		fileAccess.generateFile("CMakeLists.txt", createCMakeLists(pkg).toString)
	}
}
