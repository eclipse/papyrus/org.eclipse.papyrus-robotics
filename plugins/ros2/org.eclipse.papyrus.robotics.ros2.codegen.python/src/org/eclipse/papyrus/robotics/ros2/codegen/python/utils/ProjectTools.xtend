/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.utils

import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.UniqueEList
import org.eclipse.papyrus.robotics.ros2.codegen.python.component.RoboticsPythonCreator
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Package
import org.eclipse.papyrus.robotics.ros2.codegen.common.builder.ColconBuilderUtils

/** 
 * get or create a project with a given name
 */
class ProjectTools {
	/**
	 * get a simple project
	 */
	static def IProject getProject(String projectName) {
		var root = ResourcesPlugin.workspace.root
		var genProject = root.getProject(projectName)
		if(!genProject.exists){
			genProject.create(new NullProgressMonitor)
			genProject.open(null)
			if (genProject !== null && !genProject.exists()) {
				throw new RuntimeException(String.format(
					"project does not exist"
				))
			}
		}
		if (!ColconBuilderUtils.hasBuilder(genProject)) {
			ColconBuilderUtils.add(genProject);
		}
		return genProject
	}
	/**
	 * 
	 */
	static def genCode(RoboticsPythonCreator codeGen, Class component) {
		val packagesToGenerate = new UniqueEList<Package>();
		packagesToGenerate.add(component.nearestPackage);
		for (pkg : packagesToGenerate) {
			codeGen.createPackageableElement(pkg, new NullProgressMonitor)
		}
	}
}
