/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.utils;

import org.eclipse.papyrus.designer.languages.python.profile.PythonProfileResource;
import org.eclipse.uml2.uml.Element;
import org.eclipse.papyrus.designer.transformation.base.utils.ApplyProfileUtils;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;

public class ApplyPythonProfile {
	public static void apply(Element element) throws TransformationException {
		ApplyProfileUtils.applyProfile(element, PythonProfileResource.PROFILE_PATH_URI);
	}
}