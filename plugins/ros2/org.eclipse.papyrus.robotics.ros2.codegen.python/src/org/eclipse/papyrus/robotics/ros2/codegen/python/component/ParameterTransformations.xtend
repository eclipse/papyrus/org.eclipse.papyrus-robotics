/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.component

import org.eclipse.papyrus.designer.infra.base.StringUtils
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.designer.infra.base.StringUtils.varName
import static extension org.eclipse.papyrus.robotics.core.utils.ParameterUtils.getAllParameters
import static extension org.eclipse.papyrus.robotics.core.utils.ParameterUtils.getParameterClass
import static extension org.eclipse.papyrus.robotics.ros2.codegen.python.utils.RosPythonTypes.*
import org.eclipse.uml2.uml.LiteralBoolean

class ParameterTransformations {
	/**
	 * Declare ROS 2 parameters, taking default values and descriptions into account
	 */
	def static declareParameters(Class component) {
		val declareParamsOp = component.createOwnedOperation("declareParameters", null, null)
		val declareParamsOb = component.createOwnedBehavior(component.name,
			UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		declareParamsOp.methods.add(declareParamsOb)
		declareParamsOb.languages.add("Python")

		declareParamsOb.bodies.add('''
			«FOR parameter : component.allParameters»
				«val description = parameter.description»
				«var String descriptorVar = null»
				«IF description !== null»
					««« not supported?
				«ENDIF»
				self.declare_parameter('«parameter.name»', «parameter.strDefaultValue(descriptorVar !== null)»)
			«ENDFOR»
		''')
	}

	def static initParameters(Class component) {
		val initParamsOp = component.createOwnedOperation("initParameterVars", null, null)
		val initParamsOb = component.createOwnedBehavior(component.name,
			UMLPackage.eINSTANCE.getOpaqueBehavior) as OpaqueBehavior
		initParamsOp.methods.add(initParamsOb)
		initParamsOb.languages.add("Python")
		initParamsOb.bodies.add('''
			«FOR parameter : component.allParameters»
				self.«parameter.name.varName»_ = self.get_parameter("«parameter.name»")
			«ENDFOR»
		''')
	}

	/**
	 * Move parameters into the main components, rename it eventually to
	 * ensure that it is a valid identifier and remove default values (as
	 * the have been taken into account for parameter declaration already
	 * At the end, it also destroys the nested class holding the parameters
	 */
	def static moveParameters(Class component) {
		val paramSet = component.parameterClass
		if (paramSet !== null) {
			// move attributes to node class
			for (parameter : paramSet.ownedAttributes.clone) {
				component.ownedAttributes.add(parameter)
				// make sure that it is a valid C++ variable name, use ROS 2 naming
				// convention to append an "_" to member variables
				parameter.name = parameter.name.varName + "_"
				if (parameter.defaultValue !== null) {
					parameter.defaultValue.destroy
				}
			}
			// now destroy the nested classifier
			paramSet.destroy()
		}
	}

	/**
	 * Produce a string representation of the default value
	 * @param nonNull if true, return emptyParameterValue instead of null. This is
	 *   useful, since a non-empty description requires the presence of a default value 
	 */
	def static strDefaultValue(Property parameter, boolean nonNull) {
		if (parameter.defaultValue !== null) {
			var value = parameter.defaultValue.stringValue;
			if (parameter.type !== null) {
				// use lowerCase typename with std:: qualification
				val typeName = parameter.type.name.toLowerCase.replace("std::", "")
				if (typeName == "string") {
					value = String.format("std::string(\"%s\")", value)
					// map all strings to std::string
					parameter.type = parameter.getType("ros2Library::stdlib::string")
				} else if (parameter.defaultValue instanceof LiteralBoolean) {
					// transform true/false into True/False
					value = value.toFirstUpper;
				}
			}
			return String.format("%s", value);
		}
		if (nonNull) {
			return "rclcpp::ParameterValue()";
		}
		return null
	}

	def static getDescription(Property umlParameter) {
		val parameter = UMLUtil.getStereotypeApplication(umlParameter, ParameterEntry)
		if (parameter !== null) {
			val description = parameter.description
			if (description !== null) {
				return StringUtils.quote(description.trim)
			}
		}
	}
}
