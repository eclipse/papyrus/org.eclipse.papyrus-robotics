package org.eclipse.papyrus.robotics.ros2.codegen.python.build

import java.util.List
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Package
import org.eclipse.papyrus.designer.infra.base.StringConstants
import static extension org.eclipse.papyrus.robotics.codegen.common.utils.PackageTools.pkgName

class CreateCompTestAndResource {
	def static createTestCopyRight()'''
	# Copyright 2015 Open Source Robotics Foundation, Inc.
	#
	# Licensed under the Apache License, Version 2.0 (the "License");
	# you may not use this file except in compliance with the License.
	# You may obtain a copy of the License at
	#
	#     http://www.apache.org/licenses/LICENSE-2.0
	#
	# Unless required by applicable law or agreed to in writing, software
	# distributed under the License is distributed on an "AS IS" BASIS,
	# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	# See the License for the specific language governing permissions and
	# limitations under the License.
	
	from ament_copyright.main import main
	import pytest
	
	
	# Remove the `skip` decorator once the source file(s) have a copyright header
	@pytest.mark.skip(reason='No copyright header has been placed in the generated source file.')
	@pytest.mark.copyright
	@pytest.mark.linter
	def test_copyright():
	    rc = main(argv=['.', 'test'])
	    assert rc == 0, 'Found errors'
	'''
	
	def static createTestFlake8()'''
	# Copyright 2017 Open Source Robotics Foundation, Inc.
	#
	# Licensed under the Apache License, Version 2.0 (the "License");
	# you may not use this file except in compliance with the License.
	# You may obtain a copy of the License at
	#
	#     http://www.apache.org/licenses/LICENSE-2.0
	#
	# Unless required by applicable law or agreed to in writing, software
	# distributed under the License is distributed on an "AS IS" BASIS,
	# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	# See the License for the specific language governing permissions and
	# limitations under the License.
	
	from ament_flake8.main import main_with_errors
	import pytest
	
	
	@pytest.mark.flake8
	@pytest.mark.linter
	def test_flake8():
	    rc, errors = main_with_errors(argv=[])
	    assert rc == 0, \
	        'Found %d code style errors / warnings:\n' % len(errors) + \
	        '\n'.join(errors)
	'''
	def static createTestPep257()'''
	# Copyright 2017 Open Source Robotics Foundation, Inc.
	#
	# Licensed under the Apache License, Version 2.0 (the "License");
	# you may not use this file except in compliance with the License.
	# You may obtain a copy of the License at
	#
	#     http://www.apache.org/licenses/LICENSE-2.0
	#
	# Unless required by applicable law or agreed to in writing, software
	# distributed under the License is distributed on an "AS IS" BASIS,
	# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	# See the License for the specific language governing permissions and
	# limitations under the License.
	
	from ament_flake8.main import main_with_errors
	import pytest
	
	
	@pytest.mark.flake8
	@pytest.mark.linter
	def test_flake8():
	    rc, errors = main_with_errors(argv=[])
	    assert rc == 0, \
	        'Found %d code style errors / warnings:\n' % len(errors) + \
	        '\n'.join(errors)
	
	'''
	static def generate(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg) {
		fileAccess.generateFile("test/test_copyright.py", createTestCopyRight().toString)
		fileAccess.generateFile("test/test_flake8.py", createTestFlake8().toString)
		fileAccess.generateFile("test/test_pep257.py", createTestPep257().toString)
		fileAccess.generateFile("resource/" + pkg.pkgName, StringConstants.EMPTY)
	}
}