/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.ros2.codegen.python.utils;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.robotics.codegen.common.utils.Helpers;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Type;

public class RosPythonTypes {

	public static final String RCLPY = "rclpy"; //$NON-NLS-1$
	public static final URI RCLPY_LIBRARY_URI = URI.createURI("pathmap://ROS2_PYTHON_LIBRARY/rclpy.uml"); //$NON-NLS-1$

	/**
	 * Get a type from the ROS Python library
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @param qualifiedName
	 *            the qualified name of the element to load
	 * @return the element, if found, null otherwise
	 */
	public static Type getType(Element element, String qualifiedName) {
		// don't need a copy, as these types are not in previous transformation steps
		return Helpers.getTypeFromRS(element, RCLPY_LIBRARY_URI, qualifiedName, false);
	}

	/**
	 * return a reference to the rclpy package
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @return the rclpy package if found
	 */
	public static Package getRclPy(Element element) {
		return (Package) ElementUtils.getQualifiedElementFromRS(element, RCLPY_LIBRARY_URI, RCLPY);
	}
}