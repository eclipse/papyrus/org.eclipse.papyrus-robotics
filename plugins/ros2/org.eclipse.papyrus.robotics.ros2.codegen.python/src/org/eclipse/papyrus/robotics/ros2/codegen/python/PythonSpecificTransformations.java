package org.eclipse.papyrus.robotics.ros2.codegen.python;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;
import org.eclipse.papyrus.robotics.ros2.codegen.common.LangSpecificTransformations;
import org.eclipse.papyrus.robotics.ros2.codegen.common.component.AbstractCompTransformations;
import org.eclipse.papyrus.robotics.ros2.codegen.common.message.CreateMsgPackage;
import org.eclipse.papyrus.robotics.ros2.codegen.python.build.CreateCompPackageXML;
import org.eclipse.papyrus.robotics.ros2.codegen.python.build.CreateCompSetupConfig;
import org.eclipse.papyrus.robotics.ros2.codegen.python.build.CreateCompSetupPy;
import org.eclipse.papyrus.robotics.ros2.codegen.python.build.CreateCompTestAndResource;
import org.eclipse.papyrus.robotics.ros2.codegen.python.build.CreateMsgPkgCMakeLists;
import org.eclipse.papyrus.robotics.ros2.codegen.python.build.CreateMsgPkgPackageXML;
import org.eclipse.papyrus.robotics.ros2.codegen.python.component.ComponentTransformations;
import org.eclipse.papyrus.robotics.ros2.codegen.python.utils.ProjectTools;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;

/**
 * Implementation of abstract transformations
 */
public class PythonSpecificTransformations implements LangSpecificTransformations {

	protected CreateMsgPackage creator;

	public PythonSpecificTransformations() {
		creator = new CreateMsgPackage();
	}

	@Override
	public void createCompDefBuildFiles(IPFileSystemAccess fileAccess, Package pkg, List<Class> allComponents, List<Class> componentsInPkg, Class system) {
		CreateCompPackageXML.generate(fileAccess, pkg, allComponents, componentsInPkg, system);
		CreateCompSetupPy.generate(fileAccess, pkg, allComponents, componentsInPkg, system);
		CreateCompSetupConfig.generate(fileAccess, pkg, allComponents, componentsInPkg, system);
		CreateCompTestAndResource.generate(fileAccess, pkg, allComponents, componentsInPkg);
	}

	@Override
	public void createMsgBuildFiles(IPFileSystemAccess fileAccess, Package pkg) {
		CreateMsgPkgCMakeLists.generate(fileAccess, pkg);
		CreateMsgPkgPackageXML.generate(fileAccess, pkg);
	}

	@Override
	public void createMsgPackage(Package msgPkg) {
		creator.createMsgPkg(msgPkg);
	}

	@Override
	public AbstractCompTransformations getCompTransformation(IPFileSystemAccess fileAccess, IProject project) {
		return new ComponentTransformations(fileAccess, project);
	}

	@Override
	public void createSkillRealizationPkg(Class system, List<Class> components, Package pkg) {
		// skillRealization = new CreateSkillRelizationPackage(system, components);
		// skillRealization.createSkillRealizationPkg(pkg);
	}

	@Override
	public IProject getProject(String projectName) {
		return ProjectTools.getProject(projectName);
	}

	@Override
	public void configureProject(IProject project, List<String> depPkgList) {
		// ProjectTools.configureIncludes(project, depPkgList);
	}
}
