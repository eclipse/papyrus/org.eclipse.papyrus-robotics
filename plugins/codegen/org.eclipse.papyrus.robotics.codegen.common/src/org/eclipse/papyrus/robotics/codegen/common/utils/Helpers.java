/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.codegen.common.utils;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.designer.languages.common.base.StdUriConstants;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.External;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class Helpers {

	/**
	 * Get a type from the UML primitive types library
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @param qualifiedName
	 *            the qualified name of the element to load
	 * @return the element, if found, null otherwise
	 */
	public static Type getUMLPrimitiveType(Element element, String qualifiedName) {
		return getTypeFromRS(element, StdUriConstants.UML_PRIM_TYPES_URI, qualifiedName, true);
	}

	/**
	 * Get a type from the resource set
	 * 
	 * @param element
	 *            an element of the source model (used to identify resource set)
	 * @param resourceURI
	 *            the resource URI, if the type is not already present
	 * @param qualifiedName
	 *            the qualified name of the element to load
	 * @return the element, if found, null otherwise
	 */
	public static Type getTypeFromRS(Element element, URI resourceURI, String qualifiedName, boolean needCopy) {
		NamedElement ne = ElementUtils.getQualifiedElementFromRS(element, resourceURI, qualifiedName);
		if (needCopy) {
			// return a copy using the current copier (avoid referencing copy and original URI)
			ne = TransformationContext.current.copier.getCopy(ne);
		}

		if (ne instanceof Type) {
			return (Type) ne;
		}
		return null;
	}

	/**
	 * Return the scoped name of the communication object, based on its external stereotype.
	 * 
	 * @param type
	 *            a type with the "External" stereotype
	 * @return the external name used for that type
	 */
	public static String externalName(Type type) {
		if (type != null) {
			External external = UMLUtil.getStereotypeApplication(type, External.class);
			if (external != null) {
				return external.getName();
			}
			return type.getQualifiedName();
		}
		return null;
	}

	/**
	 * use Ptr declaration to add ::SharedPtr postfix
	 * 
	 * @param typedElement
	 */
	public static void useSharedPtr(TypedElement typedElement) {
		Ptr ptr = StereotypeUtil.applyApp(typedElement, Ptr.class);
		ptr.setDeclaration("::SharedPtr"); //$NON-NLS-1$
	}
}
