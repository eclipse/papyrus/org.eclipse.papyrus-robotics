/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.codegen.common.utils

import org.eclipse.emf.common.util.URI
import org.eclipse.papyrus.designer.languages.common.profile.CommonProfileResource
import org.eclipse.papyrus.designer.languages.cpp.profile.CppProfileResource
import org.eclipse.papyrus.designer.transformation.profile.TrafoProfileResource
import org.eclipse.uml2.uml.Element

import static org.eclipse.papyrus.designer.transformation.base.utils.ApplyProfileUtils.*

class ApplyProfiles {
	static def applyCppProfile(Element element) {
		applyProfile(element, CppProfileResource.PROFILE_PATH_URI)
	}
	
	static def applyCommonProfile(Element element) {
		applyProfile(element, CommonProfileResource.PROFILE_PATH_URI)
	}
	
	static def applyStdProfile(Element element) {
		applyProfile(element, URI.createURI("pathmap://UML_PROFILES/Standard.profile.uml"))
	}
	
	static def applyTrafoProfile(Element element) {
		applyProfile(element, TrafoProfileResource.PROFILE_PATH_URI)
	}
}