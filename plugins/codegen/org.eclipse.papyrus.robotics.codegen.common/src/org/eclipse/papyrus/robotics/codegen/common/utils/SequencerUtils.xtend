/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Matteo MORELLI      matteo.morelli@cea.fr - Bug #566899
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.codegen.common.utils

import org.eclipse.uml2.uml.Class

class SequencerUtils {

	/**
	 * Return a name for the Sequencer object
	 * (The current logics is to return the system name, with the word "Sequencer" appended)
	 */
	def static getSequencerName(Class system) {
		return system.name + "Sequencer"
	}
}
