/*****************************************************************************
 * Copyright (c) 2018, 2020 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.codegen.common.utils

import java.util.Collections
import java.util.List
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinitionModel
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.util.UMLUtil

class ComponentUtils {
	def static getComponentDefinitionModel(Class component) {
		val root = PackageUtil.getRootPackage(component)
		return UMLUtil.getStereotypeApplication(root, ComponentDefinitionModel)
	}

	def static getDependsPackage(Class component) {
		val cdm = getComponentDefinitionModel(component)
		if (cdm !== null) {
			return cdm.dependsPackage;
		} else {
			return Collections.emptyList()
		}
	}

	def static isRegistered(Class component) {
		val cdm = getComponentDefinitionModel(component)
		if (cdm !== null) {
			return cdm.isRegistered;
		}
		return false
	}

	/**
	 * Return true, if at least one of the passed components
	 * is a registered component
	 */
	def static isRegistered(List<Class> components) {
		for (component : components) {
			if (component.isRegistered) {
				return true
			}
		}
		return false
	}

	/**
	 * Return the name of the variable holding the component instance.
	 */
	def static getInstName(Class component) {
		return component.name.toLowerCase
	}
}
