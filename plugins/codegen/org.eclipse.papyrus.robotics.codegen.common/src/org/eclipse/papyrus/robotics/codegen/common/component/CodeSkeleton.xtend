/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.codegen.common.component

import org.eclipse.papyrus.robotics.profile.robotics.functions.Function
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Operation

import static extension org.eclipse.papyrus.robotics.codegen.common.utils.ActivityUtils.*

/**
 * Create a template for the implementation class that needs to be filled
 * by a developer.
 */
abstract class CodeSkeleton {
	public static String POSTFIX = "_impl"

	protected Class component;

	new(Class component) {
		this.component = component;
	}
	
	def createSkeleton() {
		val skeleton = component.nearestPackage.createOwnedClass(component.name + POSTFIX, false);
		val comment = skeleton.createOwnedComment()
		comment.body = '''
			This is a skeleton class generated for component «component.name»
			Copy it into the source folder as an initial base (or copy parts
			of it whenever you add modify the component).
		'''
		// template inherits from generated component code
		skeleton.createGeneralization(component);
		// create a suitable constructor
		skeleton.addConstrOp

		for (activity : component.activities) {
			for (function : activity.functions) {
				if (!function.codeInModel) {
					moveFunction(skeleton, function)
				}
			}
		}
	}

	def abstract Operation addConstrOp(Class skeleton);

	/**
	 *  Move function from generated code into skeleton (empty declaration)
	 */
	def moveFunction(Class skeleton, Function function) {
		if (function.base_Class instanceof Behavior) {
			val ob = function.base_Class as Behavior
			skeleton.ownedBehaviors.add(ob);
			skeleton.ownedOperations.add(ob.specification as Operation)
		}
	}
}
