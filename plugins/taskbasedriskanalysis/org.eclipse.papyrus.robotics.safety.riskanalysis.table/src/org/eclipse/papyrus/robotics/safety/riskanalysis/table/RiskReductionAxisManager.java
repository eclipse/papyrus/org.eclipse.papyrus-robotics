/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are property of the CEA, their use is subject to specific agreement 
 * with the CEA.
 *
 * Project:  Papyrus Robotics
 * 
 * Contributors:
 *  CEA LIST - Initial API and implementation
 * 
 *****************************************************************************/


package org.eclipse.papyrus.robotics.safety.riskanalysis.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction;
import org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage;

/**
 * 
 * RiskReductionAxisManager Class is dedicated exclusively to manger the items (rows)
 * in a RiskReduction Table.
 * 
 */
// TODO: Auto-generated Javadoc

public class RiskReductionAxisManager
		extends AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager<RiskReduction> {

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.uml.nattable.manager.axis.
	 * AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager#
	 * stereotypeApplicationHasChanged(org.eclipse.emf.common.notify.
	 * Notification)
	 */
	@Override
	protected void stereotypeApplicationHasChanged(final Notification notification) {
		if (notification.isTouch()) {
			return;
		}
		final Object notifier = notification.getNotifier();
		if (!isInstanceOfRequiredStereotypeApplication(notifier)) {
			return;
		}
		Object feature = notification.getFeature();
		int eventType = notification.getEventType();
		if (feature instanceof EReference
				&& ((EReference) feature).getName().startsWith(getStereotypeApplicationBasePropertyName())) {
			switch (eventType) {
			case Notification.SET:
				final Object newValue = notification.getNewValue();
				final Object oldValue = notification.getOldValue();
				if (newValue instanceof Element && isAllowedAsBaseElement((Element) newValue)) {
					updateManagedList(Collections.singletonList(newValue), Collections.emptyList());
					this.stereotypedElementsMap.put((EObject) notifier, (Element) newValue);
				} else if (newValue == null && this.managedObject.contains(oldValue)) {
					updateManagedList(Collections.emptyList(), Collections.singletonList(oldValue));
					this.stereotypedElementsMap.remove(notifier);
					removeStereotypeApplicationListener((EObject) notifier);
				}
				break;

			case Notification.UNSET:// never used?
				final Object oldValue2 = notification.getOldValue();
				if (oldValue2 instanceof Element && this.managedObject.contains(oldValue2)) {
					updateManagedList(Collections.emptyList(), Collections.singletonList(oldValue2));
					this.stereotypedElementsMap.remove(notifier);
					removeStereotypeApplicationListener((EObject) notifier);
				}
				break;
			default:
				break;
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.infra.emf.nattable.manager.axis.
	 * AbstractSynchronizedOnEStructuralFeatureAxisManager#getAllManagedAxis()
	 */
	@Override
	public List<Object> getAllManagedAxis() {
		List<Object> RiskReduction = new ArrayList<Object>();

		for (Element el : ((org.eclipse.uml2.uml.Interface) getTableContext()).allOwnedElements()) {
			if (el.getAppliedStereotype("TaskBasedRiskAnalysis::RiskReduction") != null)
				RiskReduction.add(el);
		}
		return RiskReduction;
	}

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.infra.nattable.manager.axis.AbstractAxisManager#
	 * canCreateAxisElement(java.lang.String)
	 */
	@Override
	public boolean canCreateAxisElement(String elementId) {
		return RiskanalysisPackage.eINSTANCE.getRiskReduction().getInstanceTypeName().equals(elementId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.uml.nattable.manager.axis.
	 * AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager#
	 * isInstanceOfRequiredStereotypeApplication(java.lang.Object)
	 */
	@Override
	protected boolean isInstanceOfRequiredStereotypeApplication(Object object) {
		return object instanceof RiskReduction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.uml.nattable.manager.axis.
	 * AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager#
	 * isAllowedAsBaseElement(org.eclipse.uml2.uml.Element)
	 */
	@Override
	protected boolean isAllowedAsBaseElement(Element element) {
		return element instanceof org.eclipse.uml2.uml.Property;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.uml.nattable.manager.axis.
	 * AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager#
	 * getStereotypeApplication(org.eclipse.uml2.uml.Element)
	 */
	@Override
	protected RiskReduction getStereotypeApplication(Element element) {
		return UMLUtil.getStereotypeApplication(element, RiskReduction.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.papyrus.uml.nattable.manager.axis.
	 * AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager#
	 * getStereotypeApplicationBasePropertyName()
	 */
	@Override
	protected String getStereotypeApplicationBasePropertyName() {
		return RiskanalysisPackage.eINSTANCE.getRiskReduction_Base_Property().getName();
	}

	/**
	 * Gets the stereotype base element.
	 *
	 * @param stereotypeApplication
	 *            the stereotype application
	 * @return the stereotype base element
	 * @see org.eclipse.papyrus.uml.nattable.manager.axis.AbstractStereotypedElementUMLSynchronizedOnFeatureAxisManager#getStereotypeBaseElement(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected Element getStereotypeBaseElement(final RiskReduction stereotypeApplication) {
		return stereotypeApplication.getBase_Property();
	}

}
