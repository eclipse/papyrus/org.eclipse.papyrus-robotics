/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.safety.riskanalysis.table;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.ValueSpecification;

public class ParameterTableUtils {

	public static boolean isProperty(Object modelElement) {
		return modelElement instanceof Property;
	}

	public static boolean hasDefaultValue(Property property) {
		return property.getDefaultValue() != null;
	}

	/*
	 * public static boolean hasPropertyTypeMaskedParameters(Property property){
	 * boolean isTypeWithMaskedParameters = false;
	 * if(property != null && property.getType() instanceof Classifier){
	 * Iterator<Property> propertiesIterator = ((Classifier)property.getType()).getAttributes().iterator();
	 * while(!isTypeWithMaskedParameters && propertiesIterator.hasNext()){
	 * isTypeWithMaskedParameters = isMaskedParameter(propertiesIterator.next());
	 * }
	 * }
	 * return isTypeWithMaskedParameters;
	 * }
	 * 
	 * private static boolean isMaskedParameter(Property property){
	 * if(property != null){
	 * return UMLUtil.getStereotypeApplication(property, ConfigurationProperty.class) != null;
	 * }
	 * return false;
	 * }
	 */

	public static boolean isSlotValueAnOpaqueExpression(Slot slot) {
		if (slot != null) {
			if (slot.getValues().size() == 1) {
				return slot.getValues().iterator().next() instanceof OpaqueExpression;
			}
		}
		return false;
	}

	public static OpaqueExpression getSlotValue(Slot slot) {
		if (slot != null) {
			if (slot.getValues().size() == 1) {
				ValueSpecification specification = slot.getValues().iterator().next();
				if (specification instanceof OpaqueExpression) {
					return (OpaqueExpression) specification;
				}
			}
		}
		return null;
	}
	
	
	/**
	 * Return the part within a component that is typed with the passed nested classifier
	 */
	public static Property getParameterProperty(Class nestedClassifier) {
		Element owner = nestedClassifier.getOwner();
		if (owner instanceof Class) {
			for (Property p : ((Class) owner).getOwnedAttributes()) {
				if (p.getType() == nestedClassifier) {
					return p;
				}
			}
		}
		return null;
	}

	public static ValueSpecification createValueSpecification(Element context, Slot slot, String value) {
		if (slot != null && slot.getDefiningFeature() != null && slot.getDefiningFeature().getType() != null) {
			Type type = slot.getDefiningFeature().getType();
			return createValueSpecification(context, type, value);
		}
		return null;
	}

	public static ValueSpecification createValueSpecification(Element context, Type type, String value) {
		ValueSpecification specification = null;
		if (type == UMLPrimitiveTypesUtils.getInteger(context)) {
			specification = UMLFactory.eINSTANCE.createLiteralInteger();
			Integer integerValue = null;
			try {
				integerValue = Integer.parseInt(value);
			} catch (Exception e) {
			}
			if (integerValue != null) {
				((LiteralInteger) specification).setValue(integerValue);
			}
		} else if (type == UMLPrimitiveTypesUtils.getBoolean(context)) {
			specification = UMLFactory.eINSTANCE.createLiteralBoolean();
			Boolean booleanValue = null;
			try {
				booleanValue = Boolean.parseBoolean(value);
			} catch (Exception e) {
			}
			if (booleanValue != null) {
				((LiteralBoolean) specification).setValue(booleanValue);
			}
		} else if (type == UMLPrimitiveTypesUtils.getUnlimitedNatural(context)) {
			specification = UMLFactory.eINSTANCE.createLiteralUnlimitedNatural();
			Integer integerValue = null;
			try {
				integerValue = Integer.parseInt(value);
			} catch (Exception e) {
			}
			if (integerValue != null) {
				((LiteralUnlimitedNatural) specification).setValue(integerValue);
			}
		} else if (type == UMLPrimitiveTypesUtils.getReal(context)) {
			specification = UMLFactory.eINSTANCE.createLiteralReal();
			Double doubleValue = null;
			try {
				doubleValue = Double.parseDouble(value);
			} catch (Exception e) {
			}
			if (doubleValue != null) {
				((LiteralReal) specification).setValue(doubleValue);
			}
		} else if (type == UMLPrimitiveTypesUtils.getString(context)) {
			specification = UMLFactory.eINSTANCE.createLiteralString();
			((LiteralString) specification).setValue(value);
		} else {
			specification = UMLFactory.eINSTANCE.createOpaqueExpression();
			((OpaqueExpression) specification).getLanguages().add("VSL"); //$NON-NLS-1$
			((OpaqueExpression) specification).getBodies().add(value);
		}

		return specification;
	}

}
