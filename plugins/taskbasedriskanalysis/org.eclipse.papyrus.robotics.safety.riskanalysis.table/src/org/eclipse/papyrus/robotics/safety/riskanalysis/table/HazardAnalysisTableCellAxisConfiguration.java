/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are property of the CEA, their use is subject to specific agreement 
 * with the CEA.
 *
 * Project:  Papyrus Robotics
 * 
 * Contributors:
 *  CEA LIST - Initial API and implementation
 * 
 *****************************************************************************/

package org.eclipse.papyrus.robotics.safety.riskanalysis.table;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.nebula.widgets.nattable.config.CellConfigAttributes;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.edit.editor.ICellEditor;
import org.eclipse.nebula.widgets.nattable.layer.cell.ILayerCell;
import org.eclipse.nebula.widgets.nattable.painter.cell.TextPainter;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.infra.nattable.celleditor.TextCellEditor;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.papyrus.uml.nattable.config.UMLStereotypeSingleUMLEnumerationCellEditorConfiguration;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.uml2.uml.Property;

public class HazardAnalysisTableCellAxisConfiguration extends UMLStereotypeSingleUMLEnumerationCellEditorConfiguration implements ICellAxisConfiguration {

	
	public HazardAnalysisTableCellAxisConfiguration() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getConfigurationId() {
		return "HazardAnalysisTable.id"; //$NON-NLS-1$
	}

	@Override
	public String getConfigurationDescription() {
		return "Hazard Analysis table"; //$NON-NLS-1$
	}

	@Override
	public boolean handles(Table table, Object axisElement) {
		String colID = "property_of_stereotype:/TaskBasedRiskAnalysis::HazardAnalysis::Initial_RiskLevel";
		String colID2 = "property_of_stereotype:/TaskBasedRiskAnalysis::HazardAnalysis::Final_RiskLevel";
		String type = table.getTableConfiguration().getType();
		// Check if it is in the right column and in the right table
		if(AxisUtils.getRepresentedElement(axisElement).equals(colID) && type.equals(HazardAnalysisTableCellManager.INSTANCE_VALUE_TABLE)) {
			return true;
		}
		if(AxisUtils.getRepresentedElement(axisElement).equals(colID2) && type.equals(HazardAnalysisTableCellManager.INSTANCE_VALUE_TABLE)) {
			return true;
		}
		return false;

	}

	private class InstanceValuePainter extends TextPainter {

		@Override
		public void paintCell(ILayerCell cell, GC gc, Rectangle bounds, IConfigRegistry configRegistry) {
			Object dataValue = cell.getDataValue();
			boolean defaultValue = true;
			if (dataValue instanceof EObject) {
				// value is contained in a property, is a default value
				defaultValue = ((EObject) dataValue).eContainer() instanceof Property;
			}
			if (defaultValue) {
				Color originalBackground;
				int originalAlpha;
				
				//Change the color of the cell according to the different risk levels
				switch(dataValue.toString()) {
				
				case "NegligibleRisk":
					originalBackground = gc.getBackground();
					originalAlpha = gc.getAlpha();
					super.paintCell(cell, gc, bounds, configRegistry);
					gc.setBackground(new Color(null, 55, 125, 0));
					gc.setAlpha(100);
					gc.fillRectangle(bounds);
					gc.setAlpha(originalAlpha);
					gc.setBackground(originalBackground);
					break;
				
				case "VeryLowRisk":
					originalBackground = gc.getBackground();
					originalAlpha = gc.getAlpha();
					super.paintCell(cell, gc, bounds, configRegistry);
					gc.setBackground(new Color(null, 255, 255, 0));
					gc.setAlpha(100);
					gc.fillRectangle(bounds);
					gc.setAlpha(originalAlpha);
					gc.setBackground(originalBackground);
					break;
					
				case "LowRisk":
					originalBackground = gc.getBackground();
					originalAlpha = gc.getAlpha();
					super.paintCell(cell, gc, bounds, configRegistry);
					gc.setBackground(new Color(null, 255, 255, 0));
					gc.setAlpha(100);
					gc.fillRectangle(bounds);
					gc.setAlpha(originalAlpha);
					gc.setBackground(originalBackground);
					break;
					
				case "SignificantRisk":
					originalBackground = gc.getBackground();
					originalAlpha = gc.getAlpha();
					super.paintCell(cell, gc, bounds, configRegistry);
					gc.setBackground(new Color(null, 255, 0, 0));
					gc.setAlpha(100);
					gc.fillRectangle(bounds);
					gc.setAlpha(originalAlpha);
					gc.setBackground(originalBackground);
					break;
					
				case "HighRisk":
					originalBackground = gc.getBackground();
					originalAlpha = gc.getAlpha();
					super.paintCell(cell, gc, bounds, configRegistry);
					gc.setBackground(new Color(null, 255, 0, 0));
					gc.setAlpha(100);
					gc.fillRectangle(bounds);
					gc.setAlpha(originalAlpha);
					gc.setBackground(originalBackground);
					break;
					
				case "VeryHighRisk":
					originalBackground = gc.getBackground();
					originalAlpha = gc.getAlpha();
					super.paintCell(cell, gc, bounds, configRegistry);
					gc.setBackground(new Color(null, 255, 0, 0));
					gc.setAlpha(100);
					gc.fillRectangle(bounds);
					gc.setAlpha(originalAlpha);
					gc.setBackground(originalBackground);
					break;
					
				default:
					System.out.println("Problems with coloring");
				}
			} else {
				super.paintCell(cell, gc, bounds, configRegistry);
			}
		}
	}

	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axis, String configLabel) {
		super.configureCellEditor(configRegistry, axis, configLabel);
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, new InstanceValuePainter(), DisplayMode.NORMAL, configLabel);
	}

	/**
	 * This allows to get the cell editor to use for the table.
	 * 
	 * @return The cell editor.
	 * 
	 * @since 3.0
	 */
	protected ICellEditor getCellEditor(final IConfigRegistry configRegistry, final Object axis, final String configLabel) {
		return new TextCellEditor(true);
	}
}
