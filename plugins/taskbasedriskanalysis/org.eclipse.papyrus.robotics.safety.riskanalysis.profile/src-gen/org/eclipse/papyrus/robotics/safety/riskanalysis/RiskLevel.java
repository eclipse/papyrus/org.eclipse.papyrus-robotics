/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Risk Level</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getRiskLevel()
 * @model
 * @generated
 */
public enum RiskLevel implements Enumerator {
	/**
	 * The '<em><b>Negligible Risk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEGLIGIBLE_RISK_VALUE
	 * @generated
	 * @ordered
	 */
	NEGLIGIBLE_RISK(0, "NegligibleRisk", "NegligibleRisk"),

	/**
	 * The '<em><b>Very Low Risk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VERY_LOW_RISK_VALUE
	 * @generated
	 * @ordered
	 */
	VERY_LOW_RISK(1, "VeryLowRisk", "VeryLowRisk"),

	/**
	 * The '<em><b>Low Risk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOW_RISK_VALUE
	 * @generated
	 * @ordered
	 */
	LOW_RISK(2, "LowRisk", "LowRisk"),

	/**
	 * The '<em><b>Significant Risk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIGNIFICANT_RISK_VALUE
	 * @generated
	 * @ordered
	 */
	SIGNIFICANT_RISK(3, "SignificantRisk", "SignificantRisk"),

	/**
	 * The '<em><b>High Risk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIGH_RISK_VALUE
	 * @generated
	 * @ordered
	 */
	HIGH_RISK(4, "HighRisk", "HighRisk"),

	/**
	 * The '<em><b>Very High Risk</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VERY_HIGH_RISK_VALUE
	 * @generated
	 * @ordered
	 */
	VERY_HIGH_RISK(5, "VeryHighRisk", "VeryHighRisk");

	/**
	 * The '<em><b>Negligible Risk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEGLIGIBLE_RISK
	 * @model name="NegligibleRisk"
	 * @generated
	 * @ordered
	 */
	public static final int NEGLIGIBLE_RISK_VALUE = 0;

	/**
	 * The '<em><b>Very Low Risk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VERY_LOW_RISK
	 * @model name="VeryLowRisk"
	 * @generated
	 * @ordered
	 */
	public static final int VERY_LOW_RISK_VALUE = 1;

	/**
	 * The '<em><b>Low Risk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOW_RISK
	 * @model name="LowRisk"
	 * @generated
	 * @ordered
	 */
	public static final int LOW_RISK_VALUE = 2;

	/**
	 * The '<em><b>Significant Risk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SIGNIFICANT_RISK
	 * @model name="SignificantRisk"
	 * @generated
	 * @ordered
	 */
	public static final int SIGNIFICANT_RISK_VALUE = 3;

	/**
	 * The '<em><b>High Risk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIGH_RISK
	 * @model name="HighRisk"
	 * @generated
	 * @ordered
	 */
	public static final int HIGH_RISK_VALUE = 4;

	/**
	 * The '<em><b>Very High Risk</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VERY_HIGH_RISK
	 * @model name="VeryHighRisk"
	 * @generated
	 * @ordered
	 */
	public static final int VERY_HIGH_RISK_VALUE = 5;

	/**
	 * An array of all the '<em><b>Risk Level</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final RiskLevel[] VALUES_ARRAY =
		new RiskLevel[] {
			NEGLIGIBLE_RISK,
			VERY_LOW_RISK,
			LOW_RISK,
			SIGNIFICANT_RISK,
			HIGH_RISK,
			VERY_HIGH_RISK,
		};

	/**
	 * A public read-only list of all the '<em><b>Risk Level</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<RiskLevel> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Risk Level</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RiskLevel get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RiskLevel result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Risk Level</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RiskLevel getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RiskLevel result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Risk Level</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RiskLevel get(int value) {
		switch (value) {
			case NEGLIGIBLE_RISK_VALUE: return NEGLIGIBLE_RISK;
			case VERY_LOW_RISK_VALUE: return VERY_LOW_RISK;
			case LOW_RISK_VALUE: return LOW_RISK;
			case SIGNIFICANT_RISK_VALUE: return SIGNIFICANT_RISK;
			case HIGH_RISK_VALUE: return HIGH_RISK;
			case VERY_HIGH_RISK_VALUE: return VERY_HIGH_RISK;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private RiskLevel(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //RiskLevel
