/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Body Region</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getBodyRegion()
 * @model
 * @generated
 */
public enum BodyRegion implements Enumerator {
	/**
	 * The '<em><b>Skull Forehead</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SKULL_FOREHEAD_VALUE
	 * @generated
	 * @ordered
	 */
	SKULL_FOREHEAD(0, "Skull_Forehead", "Skull_Forehead"),

	/**
	 * The '<em><b>Face</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FACE_VALUE
	 * @generated
	 * @ordered
	 */
	FACE(1, "Face", "Face"),

	/**
	 * The '<em><b>Neck</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NECK_VALUE
	 * @generated
	 * @ordered
	 */
	NECK(2, "Neck", "Neck"),

	/**
	 * The '<em><b>Bac KShoulders</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BAC_KSHOULDERS_VALUE
	 * @generated
	 * @ordered
	 */
	BAC_KSHOULDERS(3, "BacK_Shoulders", "BacK_Shoulders"),

	/**
	 * The '<em><b>Chest</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHEST_VALUE
	 * @generated
	 * @ordered
	 */
	CHEST(4, "Chest", "Chest"),

	/**
	 * The '<em><b>Abdomen</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABDOMEN_VALUE
	 * @generated
	 * @ordered
	 */
	ABDOMEN(5, "Abdomen", "Abdomen"),

	/**
	 * The '<em><b>Pelvis</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PELVIS_VALUE
	 * @generated
	 * @ordered
	 */
	PELVIS(6, "Pelvis", "Pelvis"),

	/**
	 * The '<em><b>Upper Arms Elbow Joints</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UPPER_ARMS_ELBOW_JOINTS_VALUE
	 * @generated
	 * @ordered
	 */
	UPPER_ARMS_ELBOW_JOINTS(7, "UpperArms_ElbowJoints", "UpperArms_ElbowJoints"),

	/**
	 * The '<em><b>Lower Arms Wrist Joints</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOWER_ARMS_WRIST_JOINTS_VALUE
	 * @generated
	 * @ordered
	 */
	LOWER_ARMS_WRIST_JOINTS(8, "LowerArms_WristJoints", "LowerArms_WristJoints"),

	/**
	 * The '<em><b>Hands Fingers</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HANDS_FINGERS_VALUE
	 * @generated
	 * @ordered
	 */
	HANDS_FINGERS(9, "Hands_Fingers", "Hands_Fingers"),

	/**
	 * The '<em><b>Thighs Knees</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THIGHS_KNEES_VALUE
	 * @generated
	 * @ordered
	 */
	THIGHS_KNEES(10, "Thighs_Knees", "Thighs_Knees"),

	/**
	 * The '<em><b>Lower Legs</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOWER_LEGS_VALUE
	 * @generated
	 * @ordered
	 */
	LOWER_LEGS(11, "LowerLegs", "LowerLegs");

	/**
	 * The '<em><b>Skull Forehead</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SKULL_FOREHEAD
	 * @model name="Skull_Forehead"
	 * @generated
	 * @ordered
	 */
	public static final int SKULL_FOREHEAD_VALUE = 0;

	/**
	 * The '<em><b>Face</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FACE
	 * @model name="Face"
	 * @generated
	 * @ordered
	 */
	public static final int FACE_VALUE = 1;

	/**
	 * The '<em><b>Neck</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NECK
	 * @model name="Neck"
	 * @generated
	 * @ordered
	 */
	public static final int NECK_VALUE = 2;

	/**
	 * The '<em><b>Bac KShoulders</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BAC_KSHOULDERS
	 * @model name="BacK_Shoulders"
	 * @generated
	 * @ordered
	 */
	public static final int BAC_KSHOULDERS_VALUE = 3;

	/**
	 * The '<em><b>Chest</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CHEST
	 * @model name="Chest"
	 * @generated
	 * @ordered
	 */
	public static final int CHEST_VALUE = 4;

	/**
	 * The '<em><b>Abdomen</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABDOMEN
	 * @model name="Abdomen"
	 * @generated
	 * @ordered
	 */
	public static final int ABDOMEN_VALUE = 5;

	/**
	 * The '<em><b>Pelvis</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PELVIS
	 * @model name="Pelvis"
	 * @generated
	 * @ordered
	 */
	public static final int PELVIS_VALUE = 6;

	/**
	 * The '<em><b>Upper Arms Elbow Joints</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UPPER_ARMS_ELBOW_JOINTS
	 * @model name="UpperArms_ElbowJoints"
	 * @generated
	 * @ordered
	 */
	public static final int UPPER_ARMS_ELBOW_JOINTS_VALUE = 7;

	/**
	 * The '<em><b>Lower Arms Wrist Joints</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOWER_ARMS_WRIST_JOINTS
	 * @model name="LowerArms_WristJoints"
	 * @generated
	 * @ordered
	 */
	public static final int LOWER_ARMS_WRIST_JOINTS_VALUE = 8;

	/**
	 * The '<em><b>Hands Fingers</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HANDS_FINGERS
	 * @model name="Hands_Fingers"
	 * @generated
	 * @ordered
	 */
	public static final int HANDS_FINGERS_VALUE = 9;

	/**
	 * The '<em><b>Thighs Knees</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #THIGHS_KNEES
	 * @model name="Thighs_Knees"
	 * @generated
	 * @ordered
	 */
	public static final int THIGHS_KNEES_VALUE = 10;

	/**
	 * The '<em><b>Lower Legs</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOWER_LEGS
	 * @model name="LowerLegs"
	 * @generated
	 * @ordered
	 */
	public static final int LOWER_LEGS_VALUE = 11;

	/**
	 * An array of all the '<em><b>Body Region</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final BodyRegion[] VALUES_ARRAY =
		new BodyRegion[] {
			SKULL_FOREHEAD,
			FACE,
			NECK,
			BAC_KSHOULDERS,
			CHEST,
			ABDOMEN,
			PELVIS,
			UPPER_ARMS_ELBOW_JOINTS,
			LOWER_ARMS_WRIST_JOINTS,
			HANDS_FINGERS,
			THIGHS_KNEES,
			LOWER_LEGS,
		};

	/**
	 * A public read-only list of all the '<em><b>Body Region</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<BodyRegion> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Body Region</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BodyRegion get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BodyRegion result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Body Region</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BodyRegion getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			BodyRegion result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Body Region</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static BodyRegion get(int value) {
		switch (value) {
			case SKULL_FOREHEAD_VALUE: return SKULL_FOREHEAD;
			case FACE_VALUE: return FACE;
			case NECK_VALUE: return NECK;
			case BAC_KSHOULDERS_VALUE: return BAC_KSHOULDERS;
			case CHEST_VALUE: return CHEST;
			case ABDOMEN_VALUE: return ABDOMEN;
			case PELVIS_VALUE: return PELVIS;
			case UPPER_ARMS_ELBOW_JOINTS_VALUE: return UPPER_ARMS_ELBOW_JOINTS;
			case LOWER_ARMS_WRIST_JOINTS_VALUE: return LOWER_ARMS_WRIST_JOINTS;
			case HANDS_FINGERS_VALUE: return HANDS_FINGERS;
			case THIGHS_KNEES_VALUE: return THIGHS_KNEES;
			case LOWER_LEGS_VALUE: return LOWER_LEGS;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private BodyRegion(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //BodyRegion
