/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage
 * @generated
 */
public interface RiskanalysisFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RiskanalysisFactory eINSTANCE = org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Hazard Analysis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hazard Analysis</em>'.
	 * @generated
	 */
	HazardAnalysis createHazardAnalysis();

	/**
	 * Returns a new object of class '<em>Risk Reduction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Risk Reduction</em>'.
	 * @generated
	 */
	RiskReduction createRiskReduction();

	/**
	 * Returns a new object of class '<em>Hazard Analysis Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hazard Analysis Context</em>'.
	 * @generated
	 */
	HazardAnalysisContext createHazardAnalysisContext();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RiskanalysisPackage getRiskanalysisPackage();

} //RiskanalysisFactory
