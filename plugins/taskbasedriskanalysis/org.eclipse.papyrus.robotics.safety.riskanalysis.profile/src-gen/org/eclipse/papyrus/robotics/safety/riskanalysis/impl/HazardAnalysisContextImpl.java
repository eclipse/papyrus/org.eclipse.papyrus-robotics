/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.impl.EntityImpl;

import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis;
import org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext;
import org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage;

import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hazard Analysis Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl#getHazardanalysis <em>Hazardanalysis</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl#getBase_Interface <em>Base Interface</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HazardAnalysisContextImpl extends EntityImpl implements HazardAnalysisContext {
	/**
	 * The cached value of the '{@link #getTask() <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTask()
	 * @generated
	 * @ordered
	 */
	protected Task task;
	/**
	 * The cached value of the '{@link #getBase_Interface() <em>Base Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Interface()
	 * @generated
	 * @ordered
	 */
	protected Interface base_Interface;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HazardAnalysisContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RiskanalysisPackage.Literals.HAZARD_ANALYSIS_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<HazardAnalysis> getHazardanalysis() {
		EList<HazardAnalysis> hazardsList = new UniqueEList<HazardAnalysis>();
		if (getBase_Interface() != null) {
			for (Property property : getBase_Interface().getAllAttributes()) {
				HazardAnalysis hazard = UMLUtil.getStereotypeApplication(property, HazardAnalysis.class);
				if (hazard != null) {
					hazardsList.add(hazard);
				}
			}
		}
		return hazardsList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Task getTask() {
		if (task != null && task.eIsProxy()) {
			InternalEObject oldTask = (InternalEObject)task;
			task = (Task)eResolveProxy(oldTask);
			if (task != oldTask) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__TASK, oldTask, task));
			}
		}
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Task basicGetTask() {
		Task task = null;
		if (getBase_Interface() != null) {
			for (Property property : getBase_Interface().getAllAttributes()) {
				Task t = UMLUtil.getStereotypeApplication(property, Task.class);
				if (t != null) {
					task = t;
				}
			}
		}
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTask(Task newTask) {
		Task oldTask = task;
		task = newTask;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__TASK, oldTask, task));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Interface getBase_Interface() {
		if (base_Interface != null && base_Interface.eIsProxy()) {
			InternalEObject oldBase_Interface = (InternalEObject)base_Interface;
			base_Interface = (Interface)eResolveProxy(oldBase_Interface);
			if (base_Interface != oldBase_Interface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE, oldBase_Interface, base_Interface));
			}
		}
		return base_Interface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface basicGetBase_Interface() {
		return base_Interface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Interface(Interface newBase_Interface) {
		Interface oldBase_Interface = base_Interface;
		base_Interface = newBase_Interface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE, oldBase_Interface, base_Interface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__HAZARDANALYSIS:
				return getHazardanalysis();
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__TASK:
				if (resolve) return getTask();
				return basicGetTask();
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE:
				if (resolve) return getBase_Interface();
				return basicGetBase_Interface();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__HAZARDANALYSIS:
				getHazardanalysis().clear();
				getHazardanalysis().addAll((Collection<? extends HazardAnalysis>)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__TASK:
				setTask((Task)newValue);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE:
				setBase_Interface((Interface)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__HAZARDANALYSIS:
				getHazardanalysis().clear();
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__TASK:
				setTask((Task)null);
				return;
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE:
				setBase_Interface((Interface)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__HAZARDANALYSIS:
				return !getHazardanalysis().isEmpty();
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__TASK:
				return task != null;
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE:
				return base_Interface != null;
		}
		return super.eIsSet(featureID);
	}

} //HazardAnalysisContextImpl
