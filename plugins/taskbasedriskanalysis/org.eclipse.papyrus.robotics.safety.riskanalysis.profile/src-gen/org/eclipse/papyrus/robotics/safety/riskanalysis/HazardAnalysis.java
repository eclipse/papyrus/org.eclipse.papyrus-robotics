/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.robotics.profile.robotics.skills.SkillDefinition;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hazard Analysis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getSkill <em>Skill</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getOrigin <em>Origin</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousSituation <em>Hazardous Situation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousEvent <em>Hazardous Event</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getPossibleHarm <em>Possible Harm</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Operation <em>Base Operation</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getComment <em>Comment</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Occurence <em>Initial Occurence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Avoidance <em>Initial Avoidance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Frequency <em>Initial Frequency</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Severity <em>Initial Severity</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Criticality <em>Initial Criticality</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Occurence <em>Final Occurence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Avoidance <em>Final Avoidance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Frequency <em>Final Frequency</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Severity <em>Final Severity</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Criticality <em>Final Criticality</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Property <em>Base Property</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazard <em>Hazard</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_RiskLevel <em>Initial Risk Level</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_RiskLevel <em>Final Risk Level</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getContactAera <em>Contact Aera</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_DesignRiskReduction <em>Initial Design Risk Reduction</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_DesignRiskReduction <em>Final Design Risk Reduction</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_TechnicalRiskReduction <em>Initial Technical Risk Reduction</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_TechnicalRiskReduction <em>Final Technical Risk Reduction</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_OrganizationalRiskReduction <em>Initial Organizational Risk Reduction</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_OrganizationalRiskReduction <em>Final Organizational Risk Reduction</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis()
 * @model
 * @generated
 */
public interface HazardAnalysis extends org.eclipse.papyrus.robotics.bpc.profile.bpc.Property {
	/**
	 * Returns the value of the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Skill</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skill</em>' reference.
	 * @see #setSkill(SkillDefinition)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Skill()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	SkillDefinition getSkill();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getSkill <em>Skill</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skill</em>' reference.
	 * @see #getSkill()
	 * @generated
	 */
	void setSkill(SkillDefinition value);

	/**
	 * Returns the value of the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin</em>' attribute.
	 * @see #setOrigin(String)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Origin()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getOrigin();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getOrigin <em>Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' attribute.
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(String value);

	/**
	 * Returns the value of the '<em><b>Hazardous Situation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hazardous Situation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hazardous Situation</em>' attribute.
	 * @see #setHazardousSituation(String)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_HazardousSituation()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getHazardousSituation();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousSituation <em>Hazardous Situation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hazardous Situation</em>' attribute.
	 * @see #getHazardousSituation()
	 * @generated
	 */
	void setHazardousSituation(String value);

	/**
	 * Returns the value of the '<em><b>Hazardous Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hazardous Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hazardous Event</em>' attribute.
	 * @see #setHazardousEvent(String)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_HazardousEvent()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getHazardousEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousEvent <em>Hazardous Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hazardous Event</em>' attribute.
	 * @see #getHazardousEvent()
	 * @generated
	 */
	void setHazardousEvent(String value);

	/**
	 * Returns the value of the '<em><b>Possible Harm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Possible Harm</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Possible Harm</em>' attribute.
	 * @see #setPossibleHarm(String)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_PossibleHarm()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getPossibleHarm();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getPossibleHarm <em>Possible Harm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Possible Harm</em>' attribute.
	 * @see #getPossibleHarm()
	 * @generated
	 */
	void setPossibleHarm(String value);

	/**
	 * Returns the value of the '<em><b>Base Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Operation</em>' reference.
	 * @see #setBase_Operation(Operation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Base_Operation()
	 * @model ordered="false"
	 * @generated
	 */
	Operation getBase_Operation();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Operation <em>Base Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Operation</em>' reference.
	 * @see #getBase_Operation()
	 * @generated
	 */
	void setBase_Operation(Operation value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' attribute.
	 * @see #setComment(String)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Comment()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getComment();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getComment <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' attribute.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(String value);

	/**
	 * Returns the value of the '<em><b>Initial Occurence</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Occurence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Occurence</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
	 * @see #setInitial_Occurence(OccurenceEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_Occurence()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	OccurenceEstimation getInitial_Occurence();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Occurence <em>Initial Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Occurence</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
	 * @see #getInitial_Occurence()
	 * @generated
	 */
	void setInitial_Occurence(OccurenceEstimation value);

	/**
	 * Returns the value of the '<em><b>Initial Avoidance</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Avoidance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Avoidance</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #setInitial_Avoidance(RiskEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_Avoidance()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RiskEstimation getInitial_Avoidance();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Avoidance <em>Initial Avoidance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Avoidance</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #getInitial_Avoidance()
	 * @generated
	 */
	void setInitial_Avoidance(RiskEstimation value);

	/**
	 * Returns the value of the '<em><b>Initial Frequency</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Frequency</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #setInitial_Frequency(RiskEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_Frequency()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RiskEstimation getInitial_Frequency();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Frequency <em>Initial Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Frequency</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #getInitial_Frequency()
	 * @generated
	 */
	void setInitial_Frequency(RiskEstimation value);

	/**
	 * Returns the value of the '<em><b>Initial Severity</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Severity</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #setInitial_Severity(RiskEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_Severity()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RiskEstimation getInitial_Severity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Severity <em>Initial Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Severity</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #getInitial_Severity()
	 * @generated
	 */
	void setInitial_Severity(RiskEstimation value);

	/**
	 * Returns the value of the '<em><b>Initial Criticality</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Criticality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Criticality</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_Criticality()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	RiskIndex getInitial_Criticality();

	/**
	 * Returns the value of the '<em><b>Final Occurence</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Occurence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Occurence</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
	 * @see #setFinal_Occurence(OccurenceEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_Occurence()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	OccurenceEstimation getFinal_Occurence();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Occurence <em>Final Occurence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Occurence</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
	 * @see #getFinal_Occurence()
	 * @generated
	 */
	void setFinal_Occurence(OccurenceEstimation value);

	/**
	 * Returns the value of the '<em><b>Final Avoidance</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Avoidance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Avoidance</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #setFinal_Avoidance(RiskEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_Avoidance()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RiskEstimation getFinal_Avoidance();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Avoidance <em>Final Avoidance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Avoidance</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #getFinal_Avoidance()
	 * @generated
	 */
	void setFinal_Avoidance(RiskEstimation value);

	/**
	 * Returns the value of the '<em><b>Final Frequency</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Frequency</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #setFinal_Frequency(RiskEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_Frequency()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RiskEstimation getFinal_Frequency();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Frequency <em>Final Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Frequency</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #getFinal_Frequency()
	 * @generated
	 */
	void setFinal_Frequency(RiskEstimation value);

	/**
	 * Returns the value of the '<em><b>Final Severity</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Severity</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #setFinal_Severity(RiskEstimation)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_Severity()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RiskEstimation getFinal_Severity();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Severity <em>Final Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Severity</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see #getFinal_Severity()
	 * @generated
	 */
	void setFinal_Severity(RiskEstimation value);

	/**
	 * Returns the value of the '<em><b>Final Criticality</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Criticality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Criticality</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_Criticality()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	RiskIndex getFinal_Criticality();

	/**
	 * Returns the value of the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Property</em>' reference.
	 * @see #setBase_Property(Property)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Base_Property()
	 * @model ordered="false"
	 * @generated
	 */
	Property getBase_Property();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Property <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Property</em>' reference.
	 * @see #getBase_Property()
	 * @generated
	 */
	void setBase_Property(Property value);

	/**
	 * Returns the value of the '<em><b>Hazard</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hazard</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hazard</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType
	 * @see #setHazard(HazardType)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Hazard()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	HazardType getHazard();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazard <em>Hazard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hazard</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType
	 * @see #getHazard()
	 * @generated
	 */
	void setHazard(HazardType value);

	/**
	 * Returns the value of the '<em><b>Initial Risk Level</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Risk Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Risk Level</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
	 * @see #setInitial_RiskLevel(RiskLevel)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_RiskLevel()
	 * @model required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	RiskLevel getInitial_RiskLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_RiskLevel <em>Initial Risk Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Risk Level</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
	 * @see #getInitial_RiskLevel()
	 * @generated
	 */
	void setInitial_RiskLevel(RiskLevel value);

	/**
	 * Returns the value of the '<em><b>Final Risk Level</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Risk Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Risk Level</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
	 * @see #setFinal_RiskLevel(RiskLevel)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_RiskLevel()
	 * @model required="true" transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	RiskLevel getFinal_RiskLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_RiskLevel <em>Final Risk Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Risk Level</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
	 * @see #getFinal_RiskLevel()
	 * @generated
	 */
	void setFinal_RiskLevel(RiskLevel value);

	/**
	 * Returns the value of the '<em><b>Contact Aera</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contact Aera</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contact Aera</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion
	 * @see #setContactAera(BodyRegion)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_ContactAera()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BodyRegion getContactAera();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getContactAera <em>Contact Aera</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contact Aera</em>' attribute.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion
	 * @see #getContactAera()
	 * @generated
	 */
	void setContactAera(BodyRegion value);

	/**
	 * Returns the value of the '<em><b>Initial Design Risk Reduction</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Design Risk Reduction</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Design Risk Reduction</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_DesignRiskReduction()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RiskReduction> getInitial_DesignRiskReduction();

	/**
	 * Returns the value of the '<em><b>Final Design Risk Reduction</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Design Risk Reduction</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Design Risk Reduction</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_DesignRiskReduction()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RiskReduction> getFinal_DesignRiskReduction();

	/**
	 * Returns the value of the '<em><b>Initial Technical Risk Reduction</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Technical Risk Reduction</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Technical Risk Reduction</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_TechnicalRiskReduction()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RiskReduction> getInitial_TechnicalRiskReduction();

	/**
	 * Returns the value of the '<em><b>Final Technical Risk Reduction</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Technical Risk Reduction</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Technical Risk Reduction</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_TechnicalRiskReduction()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RiskReduction> getFinal_TechnicalRiskReduction();

	/**
	 * Returns the value of the '<em><b>Initial Organizational Risk Reduction</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Organizational Risk Reduction</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Organizational Risk Reduction</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Initial_OrganizationalRiskReduction()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RiskReduction> getInitial_OrganizationalRiskReduction();

	/**
	 * Returns the value of the '<em><b>Final Organizational Risk Reduction</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Organizational Risk Reduction</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Organizational Risk Reduction</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysis_Final_OrganizationalRiskReduction()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RiskReduction> getFinal_OrganizationalRiskReduction();

} // HazardAnalysis
