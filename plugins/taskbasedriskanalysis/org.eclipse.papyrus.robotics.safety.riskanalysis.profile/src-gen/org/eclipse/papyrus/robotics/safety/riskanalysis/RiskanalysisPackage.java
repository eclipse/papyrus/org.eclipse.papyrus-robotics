/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.BPCPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='TaskBasedRiskAnalysis'"
 * @generated
 */
public interface RiskanalysisPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "riskanalysis";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/robotics/safety/riskanalysis/1";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "RiskAnalysis";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RiskanalysisPackage eINSTANCE = org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisImpl <em>Hazard Analysis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisImpl
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getHazardAnalysis()
	 * @generated
	 */
	int HAZARD_ANALYSIS = 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__PROPERTY = BPCPackage.PROPERTY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INSTANCE_UID = BPCPackage.PROPERTY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__DESCRIPTION = BPCPackage.PROPERTY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__AUTHORSHIP = BPCPackage.PROPERTY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__PROVENANCE = BPCPackage.PROPERTY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__MODEL_UID = BPCPackage.PROPERTY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__METAMODEL_UID = BPCPackage.PROPERTY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Skill</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__SKILL = BPCPackage.PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__ORIGIN = BPCPackage.PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Hazardous Situation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__HAZARDOUS_SITUATION = BPCPackage.PROPERTY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Hazardous Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__HAZARDOUS_EVENT = BPCPackage.PROPERTY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Possible Harm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__POSSIBLE_HARM = BPCPackage.PROPERTY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Base Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__BASE_OPERATION = BPCPackage.PROPERTY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__COMMENT = BPCPackage.PROPERTY_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Initial Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_OCCURENCE = BPCPackage.PROPERTY_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Initial Avoidance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_AVOIDANCE = BPCPackage.PROPERTY_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Initial Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_FREQUENCY = BPCPackage.PROPERTY_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Initial Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_SEVERITY = BPCPackage.PROPERTY_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Initial Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_CRITICALITY = BPCPackage.PROPERTY_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Final Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_OCCURENCE = BPCPackage.PROPERTY_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Final Avoidance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_AVOIDANCE = BPCPackage.PROPERTY_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Final Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_FREQUENCY = BPCPackage.PROPERTY_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Final Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_SEVERITY = BPCPackage.PROPERTY_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Final Criticality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_CRITICALITY = BPCPackage.PROPERTY_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__BASE_PROPERTY = BPCPackage.PROPERTY_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Hazard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__HAZARD = BPCPackage.PROPERTY_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Initial Risk Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_RISK_LEVEL = BPCPackage.PROPERTY_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Final Risk Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_RISK_LEVEL = BPCPackage.PROPERTY_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Contact Aera</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__CONTACT_AERA = BPCPackage.PROPERTY_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Initial Design Risk Reduction</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_DESIGN_RISK_REDUCTION = BPCPackage.PROPERTY_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Final Design Risk Reduction</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_DESIGN_RISK_REDUCTION = BPCPackage.PROPERTY_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Initial Technical Risk Reduction</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_TECHNICAL_RISK_REDUCTION = BPCPackage.PROPERTY_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Final Technical Risk Reduction</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_TECHNICAL_RISK_REDUCTION = BPCPackage.PROPERTY_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Initial Organizational Risk Reduction</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__INITIAL_ORGANIZATIONAL_RISK_REDUCTION = BPCPackage.PROPERTY_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>Final Organizational Risk Reduction</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS__FINAL_ORGANIZATIONAL_RISK_REDUCTION = BPCPackage.PROPERTY_FEATURE_COUNT + 27;

	/**
	 * The number of structural features of the '<em>Hazard Analysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_FEATURE_COUNT = BPCPackage.PROPERTY_FEATURE_COUNT + 28;

	/**
	 * The number of operations of the '<em>Hazard Analysis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_OPERATION_COUNT = BPCPackage.PROPERTY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskReductionImpl <em>Risk Reduction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskReductionImpl
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskReduction()
	 * @generated
	 */
	int RISK_REDUCTION = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_REDUCTION__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_REDUCTION__BASE_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_REDUCTION__BASE_PROPERTY = 2;

	/**
	 * The number of structural features of the '<em>Risk Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_REDUCTION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Risk Reduction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_REDUCTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl <em>Hazard Analysis Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getHazardAnalysisContext()
	 * @generated
	 */
	int HAZARD_ANALYSIS_CONTEXT = 2;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__PROPERTY = BPCPackage.ENTITY__PROPERTY;

	/**
	 * The feature id for the '<em><b>Instance uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__INSTANCE_UID = BPCPackage.ENTITY__INSTANCE_UID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__DESCRIPTION = BPCPackage.ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Authorship</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__AUTHORSHIP = BPCPackage.ENTITY__AUTHORSHIP;

	/**
	 * The feature id for the '<em><b>Provenance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__PROVENANCE = BPCPackage.ENTITY__PROVENANCE;

	/**
	 * The feature id for the '<em><b>Model uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__MODEL_UID = BPCPackage.ENTITY__MODEL_UID;

	/**
	 * The feature id for the '<em><b>Metamodel uid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__METAMODEL_UID = BPCPackage.ENTITY__METAMODEL_UID;

	/**
	 * The feature id for the '<em><b>Hazardanalysis</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__HAZARDANALYSIS = BPCPackage.ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__TASK = BPCPackage.ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE = BPCPackage.ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hazard Analysis Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT_FEATURE_COUNT = BPCPackage.ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Hazard Analysis Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAZARD_ANALYSIS_CONTEXT_OPERATION_COUNT = BPCPackage.ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation <em>Occurence Estimation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getOccurenceEstimation()
	 * @generated
	 */
	int OCCURENCE_ESTIMATION = 3;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation <em>Risk Estimation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskEstimation()
	 * @generated
	 */
	int RISK_ESTIMATION = 4;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex <em>Risk Index</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskIndex()
	 * @generated
	 */
	int RISK_INDEX = 5;


	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType <em>Hazard Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getHazardType()
	 * @generated
	 */
	int HAZARD_TYPE = 6;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel <em>Risk Level</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskLevel()
	 * @generated
	 */
	int RISK_LEVEL = 7;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion <em>Body Region</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getBodyRegion()
	 * @generated
	 */
	int BODY_REGION = 8;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis <em>Hazard Analysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hazard Analysis</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis
	 * @generated
	 */
	EClass getHazardAnalysis();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getSkill <em>Skill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Skill</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getSkill()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Skill();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Origin</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getOrigin()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Origin();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousSituation <em>Hazardous Situation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hazardous Situation</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousSituation()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_HazardousSituation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousEvent <em>Hazardous Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hazardous Event</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazardousEvent()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_HazardousEvent();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getPossibleHarm <em>Possible Harm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Possible Harm</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getPossibleHarm()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_PossibleHarm();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Operation <em>Base Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Operation</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Operation()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Base_Operation();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getComment()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Comment();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Occurence <em>Initial Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Occurence</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Occurence()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Occurence();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Avoidance <em>Initial Avoidance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Avoidance</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Avoidance()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Avoidance();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Frequency <em>Initial Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Frequency</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Frequency()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Severity <em>Initial Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Severity</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Severity()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Severity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Criticality <em>Initial Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Criticality</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_Criticality()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_Criticality();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Occurence <em>Final Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Occurence</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Occurence()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Occurence();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Avoidance <em>Final Avoidance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Avoidance</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Avoidance()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Avoidance();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Frequency <em>Final Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Frequency</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Frequency()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Frequency();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Severity <em>Final Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Severity</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Severity()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Severity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Criticality <em>Final Criticality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Criticality</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_Criticality()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_Criticality();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction <em>Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction
	 * @generated
	 */
	EClass getRiskReduction();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction#getDescription()
	 * @see #getRiskReduction()
	 * @generated
	 */
	EAttribute getRiskReduction_Description();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction#getBase_Class()
	 * @see #getRiskReduction()
	 * @generated
	 */
	EReference getRiskReduction_Base_Class();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction#getBase_Property()
	 * @see #getRiskReduction()
	 * @generated
	 */
	EReference getRiskReduction_Base_Property();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getBase_Property()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Base_Property();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazard <em>Hazard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hazard</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getHazard()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Hazard();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_RiskLevel <em>Initial Risk Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Risk Level</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_RiskLevel()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Initial_RiskLevel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_RiskLevel <em>Final Risk Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Risk Level</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_RiskLevel()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_Final_RiskLevel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getContactAera <em>Contact Aera</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Contact Aera</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getContactAera()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EAttribute getHazardAnalysis_ContactAera();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_DesignRiskReduction <em>Initial Design Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Initial Design Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_DesignRiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Initial_DesignRiskReduction();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_DesignRiskReduction <em>Final Design Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Final Design Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_DesignRiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Final_DesignRiskReduction();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_TechnicalRiskReduction <em>Initial Technical Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Initial Technical Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_TechnicalRiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Initial_TechnicalRiskReduction();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_TechnicalRiskReduction <em>Final Technical Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Final Technical Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_TechnicalRiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Final_TechnicalRiskReduction();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_OrganizationalRiskReduction <em>Initial Organizational Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Initial Organizational Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getInitial_OrganizationalRiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Initial_OrganizationalRiskReduction();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_OrganizationalRiskReduction <em>Final Organizational Risk Reduction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Final Organizational Risk Reduction</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis#getFinal_OrganizationalRiskReduction()
	 * @see #getHazardAnalysis()
	 * @generated
	 */
	EReference getHazardAnalysis_Final_OrganizationalRiskReduction();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext <em>Hazard Analysis Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hazard Analysis Context</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext
	 * @generated
	 */
	EClass getHazardAnalysisContext();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getHazardanalysis <em>Hazardanalysis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Hazardanalysis</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getHazardanalysis()
	 * @see #getHazardAnalysisContext()
	 * @generated
	 */
	EReference getHazardAnalysisContext_Hazardanalysis();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getTask()
	 * @see #getHazardAnalysisContext()
	 * @generated
	 */
	EReference getHazardAnalysisContext_Task();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getBase_Interface <em>Base Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Interface</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getBase_Interface()
	 * @see #getHazardAnalysisContext()
	 * @generated
	 */
	EReference getHazardAnalysisContext_Base_Interface();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation <em>Occurence Estimation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Occurence Estimation</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
	 * @generated
	 */
	EEnum getOccurenceEstimation();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation <em>Risk Estimation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Estimation</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
	 * @generated
	 */
	EEnum getRiskEstimation();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex <em>Risk Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Index</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex
	 * @generated
	 */
	EEnum getRiskIndex();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType <em>Hazard Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Hazard Type</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType
	 * @generated
	 */
	EEnum getHazardType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel <em>Risk Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Level</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
	 * @generated
	 */
	EEnum getRiskLevel();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion <em>Body Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Body Region</em>'.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion
	 * @generated
	 */
	EEnum getBodyRegion();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RiskanalysisFactory getRiskanalysisFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisImpl <em>Hazard Analysis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisImpl
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getHazardAnalysis()
		 * @generated
		 */
		EClass HAZARD_ANALYSIS = eINSTANCE.getHazardAnalysis();

		/**
		 * The meta object literal for the '<em><b>Skill</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__SKILL = eINSTANCE.getHazardAnalysis_Skill();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__ORIGIN = eINSTANCE.getHazardAnalysis_Origin();

		/**
		 * The meta object literal for the '<em><b>Hazardous Situation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__HAZARDOUS_SITUATION = eINSTANCE.getHazardAnalysis_HazardousSituation();

		/**
		 * The meta object literal for the '<em><b>Hazardous Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__HAZARDOUS_EVENT = eINSTANCE.getHazardAnalysis_HazardousEvent();

		/**
		 * The meta object literal for the '<em><b>Possible Harm</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__POSSIBLE_HARM = eINSTANCE.getHazardAnalysis_PossibleHarm();

		/**
		 * The meta object literal for the '<em><b>Base Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__BASE_OPERATION = eINSTANCE.getHazardAnalysis_Base_Operation();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__COMMENT = eINSTANCE.getHazardAnalysis_Comment();

		/**
		 * The meta object literal for the '<em><b>Initial Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_OCCURENCE = eINSTANCE.getHazardAnalysis_Initial_Occurence();

		/**
		 * The meta object literal for the '<em><b>Initial Avoidance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_AVOIDANCE = eINSTANCE.getHazardAnalysis_Initial_Avoidance();

		/**
		 * The meta object literal for the '<em><b>Initial Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_FREQUENCY = eINSTANCE.getHazardAnalysis_Initial_Frequency();

		/**
		 * The meta object literal for the '<em><b>Initial Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_SEVERITY = eINSTANCE.getHazardAnalysis_Initial_Severity();

		/**
		 * The meta object literal for the '<em><b>Initial Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_CRITICALITY = eINSTANCE.getHazardAnalysis_Initial_Criticality();

		/**
		 * The meta object literal for the '<em><b>Final Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_OCCURENCE = eINSTANCE.getHazardAnalysis_Final_Occurence();

		/**
		 * The meta object literal for the '<em><b>Final Avoidance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_AVOIDANCE = eINSTANCE.getHazardAnalysis_Final_Avoidance();

		/**
		 * The meta object literal for the '<em><b>Final Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_FREQUENCY = eINSTANCE.getHazardAnalysis_Final_Frequency();

		/**
		 * The meta object literal for the '<em><b>Final Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_SEVERITY = eINSTANCE.getHazardAnalysis_Final_Severity();

		/**
		 * The meta object literal for the '<em><b>Final Criticality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_CRITICALITY = eINSTANCE.getHazardAnalysis_Final_Criticality();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskReductionImpl <em>Risk Reduction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskReductionImpl
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskReduction()
		 * @generated
		 */
		EClass RISK_REDUCTION = eINSTANCE.getRiskReduction();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RISK_REDUCTION__DESCRIPTION = eINSTANCE.getRiskReduction_Description();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISK_REDUCTION__BASE_CLASS = eINSTANCE.getRiskReduction_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISK_REDUCTION__BASE_PROPERTY = eINSTANCE.getRiskReduction_Base_Property();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__BASE_PROPERTY = eINSTANCE.getHazardAnalysis_Base_Property();

		/**
		 * The meta object literal for the '<em><b>Hazard</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__HAZARD = eINSTANCE.getHazardAnalysis_Hazard();

		/**
		 * The meta object literal for the '<em><b>Initial Risk Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__INITIAL_RISK_LEVEL = eINSTANCE.getHazardAnalysis_Initial_RiskLevel();

		/**
		 * The meta object literal for the '<em><b>Final Risk Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__FINAL_RISK_LEVEL = eINSTANCE.getHazardAnalysis_Final_RiskLevel();

		/**
		 * The meta object literal for the '<em><b>Contact Aera</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAZARD_ANALYSIS__CONTACT_AERA = eINSTANCE.getHazardAnalysis_ContactAera();

		/**
		 * The meta object literal for the '<em><b>Initial Design Risk Reduction</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__INITIAL_DESIGN_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Initial_DesignRiskReduction();

		/**
		 * The meta object literal for the '<em><b>Final Design Risk Reduction</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__FINAL_DESIGN_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Final_DesignRiskReduction();

		/**
		 * The meta object literal for the '<em><b>Initial Technical Risk Reduction</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__INITIAL_TECHNICAL_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Initial_TechnicalRiskReduction();

		/**
		 * The meta object literal for the '<em><b>Final Technical Risk Reduction</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__FINAL_TECHNICAL_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Final_TechnicalRiskReduction();

		/**
		 * The meta object literal for the '<em><b>Initial Organizational Risk Reduction</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__INITIAL_ORGANIZATIONAL_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Initial_OrganizationalRiskReduction();

		/**
		 * The meta object literal for the '<em><b>Final Organizational Risk Reduction</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS__FINAL_ORGANIZATIONAL_RISK_REDUCTION = eINSTANCE.getHazardAnalysis_Final_OrganizationalRiskReduction();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl <em>Hazard Analysis Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.HazardAnalysisContextImpl
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getHazardAnalysisContext()
		 * @generated
		 */
		EClass HAZARD_ANALYSIS_CONTEXT = eINSTANCE.getHazardAnalysisContext();

		/**
		 * The meta object literal for the '<em><b>Hazardanalysis</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS_CONTEXT__HAZARDANALYSIS = eINSTANCE.getHazardAnalysisContext_Hazardanalysis();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS_CONTEXT__TASK = eINSTANCE.getHazardAnalysisContext_Task();

		/**
		 * The meta object literal for the '<em><b>Base Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAZARD_ANALYSIS_CONTEXT__BASE_INTERFACE = eINSTANCE.getHazardAnalysisContext_Base_Interface();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation <em>Occurence Estimation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.OccurenceEstimation
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getOccurenceEstimation()
		 * @generated
		 */
		EEnum OCCURENCE_ESTIMATION = eINSTANCE.getOccurenceEstimation();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation <em>Risk Estimation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskEstimation
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskEstimation()
		 * @generated
		 */
		EEnum RISK_ESTIMATION = eINSTANCE.getRiskEstimation();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex <em>Risk Index</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskIndex
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskIndex()
		 * @generated
		 */
		EEnum RISK_INDEX = eINSTANCE.getRiskIndex();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType <em>Hazard Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.HazardType
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getHazardType()
		 * @generated
		 */
		EEnum HAZARD_TYPE = eINSTANCE.getHazardType();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel <em>Risk Level</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskLevel
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getRiskLevel()
		 * @generated
		 */
		EEnum RISK_LEVEL = eINSTANCE.getRiskLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion <em>Body Region</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.BodyRegion
		 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.impl.RiskanalysisPackageImpl#getBodyRegion()
		 * @generated
		 */
		EEnum BODY_REGION = eINSTANCE.getBodyRegion();

	}

} //RiskanalysisPackage
