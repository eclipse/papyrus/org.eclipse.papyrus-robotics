/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.robotics.safety.riskanalysis.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RiskanalysisFactoryImpl extends EFactoryImpl implements RiskanalysisFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RiskanalysisFactory init() {
		try {
			RiskanalysisFactory theRiskanalysisFactory = (RiskanalysisFactory)EPackage.Registry.INSTANCE.getEFactory(RiskanalysisPackage.eNS_URI);
			if (theRiskanalysisFactory != null) {
				return theRiskanalysisFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RiskanalysisFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskanalysisFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RiskanalysisPackage.HAZARD_ANALYSIS: return createHazardAnalysis();
			case RiskanalysisPackage.RISK_REDUCTION: return createRiskReduction();
			case RiskanalysisPackage.HAZARD_ANALYSIS_CONTEXT: return createHazardAnalysisContext();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RiskanalysisPackage.OCCURENCE_ESTIMATION:
				return createOccurenceEstimationFromString(eDataType, initialValue);
			case RiskanalysisPackage.RISK_ESTIMATION:
				return createRiskEstimationFromString(eDataType, initialValue);
			case RiskanalysisPackage.RISK_INDEX:
				return createRiskIndexFromString(eDataType, initialValue);
			case RiskanalysisPackage.HAZARD_TYPE:
				return createHazardTypeFromString(eDataType, initialValue);
			case RiskanalysisPackage.RISK_LEVEL:
				return createRiskLevelFromString(eDataType, initialValue);
			case RiskanalysisPackage.BODY_REGION:
				return createBodyRegionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RiskanalysisPackage.OCCURENCE_ESTIMATION:
				return convertOccurenceEstimationToString(eDataType, instanceValue);
			case RiskanalysisPackage.RISK_ESTIMATION:
				return convertRiskEstimationToString(eDataType, instanceValue);
			case RiskanalysisPackage.RISK_INDEX:
				return convertRiskIndexToString(eDataType, instanceValue);
			case RiskanalysisPackage.HAZARD_TYPE:
				return convertHazardTypeToString(eDataType, instanceValue);
			case RiskanalysisPackage.RISK_LEVEL:
				return convertRiskLevelToString(eDataType, instanceValue);
			case RiskanalysisPackage.BODY_REGION:
				return convertBodyRegionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HazardAnalysis createHazardAnalysis() {
		HazardAnalysisImpl hazardAnalysis = new HazardAnalysisImpl();
		return hazardAnalysis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskReduction createRiskReduction() {
		RiskReductionImpl riskReduction = new RiskReductionImpl();
		return riskReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HazardAnalysisContext createHazardAnalysisContext() {
		HazardAnalysisContextImpl hazardAnalysisContext = new HazardAnalysisContextImpl();
		return hazardAnalysisContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OccurenceEstimation createOccurenceEstimationFromString(EDataType eDataType, String initialValue) {
		OccurenceEstimation result = OccurenceEstimation.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOccurenceEstimationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskEstimation createRiskEstimationFromString(EDataType eDataType, String initialValue) {
		RiskEstimation result = RiskEstimation.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRiskEstimationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskIndex createRiskIndexFromString(EDataType eDataType, String initialValue) {
		RiskIndex result = RiskIndex.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRiskIndexToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HazardType createHazardTypeFromString(EDataType eDataType, String initialValue) {
		HazardType result = HazardType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertHazardTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskLevel createRiskLevelFromString(EDataType eDataType, String initialValue) {
		RiskLevel result = RiskLevel.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRiskLevelToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BodyRegion createBodyRegionFromString(EDataType eDataType, String initialValue) {
		BodyRegion result = BodyRegion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBodyRegionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RiskanalysisPackage getRiskanalysisPackage() {
		return (RiskanalysisPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RiskanalysisPackage getPackage() {
		return RiskanalysisPackage.eINSTANCE;
	}

} //RiskanalysisFactoryImpl
