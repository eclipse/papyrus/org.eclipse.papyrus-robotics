/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;

import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.uml2.uml.Interface;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hazard Analysis Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getHazardanalysis <em>Hazardanalysis</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getTask <em>Task</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getBase_Interface <em>Base Interface</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysisContext()
 * @model
 * @generated
 */
public interface HazardAnalysisContext extends Entity {
	/**
	 * Returns the value of the '<em><b>Hazardanalysis</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysis}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hazardanalysis</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hazardanalysis</em>' reference list.
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysisContext_Hazardanalysis()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<HazardAnalysis> getHazardanalysis();

	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(Task)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysisContext_Task()
	 * @model ordered="false"
	 * @generated
	 */
	Task getTask();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(Task value);

	/**
	 * Returns the value of the '<em><b>Base Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Interface</em>' reference.
	 * @see #setBase_Interface(Interface)
	 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardAnalysisContext_Base_Interface()
	 * @model ordered="false"
	 * @generated
	 */
	Interface getBase_Interface();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext#getBase_Interface <em>Base Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Interface</em>' reference.
	 * @see #getBase_Interface()
	 * @generated
	 */
	void setBase_Interface(Interface value);

} // HazardAnalysisContext
