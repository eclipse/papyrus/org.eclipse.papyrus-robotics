/**
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 */
package org.eclipse.papyrus.robotics.safety.riskanalysis;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Hazard Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.safety.riskanalysis.RiskanalysisPackage#getHazardType()
 * @model
 * @generated
 */
public enum HazardType implements Enumerator {
	/**
	 * The '<em><b>Mechanical</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MECHANICAL_VALUE
	 * @generated
	 * @ordered
	 */
	MECHANICAL(0, "Mechanical", "Mechanical"),

	/**
	 * The '<em><b>Electrical</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ELECTRICAL_VALUE
	 * @generated
	 * @ordered
	 */
	ELECTRICAL(1, "Electrical", "Electrical"),

	/**
	 * The '<em><b>Noise</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOISE_VALUE
	 * @generated
	 * @ordered
	 */
	NOISE(2, "Noise", "Noise"),

	/**
	 * The '<em><b>Vibration</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VIBRATION_VALUE
	 * @generated
	 * @ordered
	 */
	VIBRATION(3, "Vibration", "Vibration"),

	/**
	 * The '<em><b>Radiation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RADIATION_VALUE
	 * @generated
	 * @ordered
	 */
	RADIATION(4, "Radiation", "Radiation"),

	/**
	 * The '<em><b>Material Substance</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_SUBSTANCE_VALUE
	 * @generated
	 * @ordered
	 */
	MATERIAL_SUBSTANCE(5, "MaterialSubstance", "MaterialSubstance"),

	/**
	 * The '<em><b>Ergonomic</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERGONOMIC_VALUE
	 * @generated
	 * @ordered
	 */
	ERGONOMIC(6, "Ergonomic", "Ergonomic"),

	/**
	 * The '<em><b>Environment Workspace</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENVIRONMENT_WORKSPACE_VALUE
	 * @generated
	 * @ordered
	 */
	ENVIRONMENT_WORKSPACE(7, "EnvironmentWorkspace", "EnvironmentWorkspace"),

	/**
	 * The '<em><b>Combination</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMBINATION_VALUE
	 * @generated
	 * @ordered
	 */
	COMBINATION(8, "Combination", "Combination"),

	/**
	 * The '<em><b>Other</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(9, "Other", "Other");

	/**
	 * The '<em><b>Mechanical</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MECHANICAL
	 * @model name="Mechanical"
	 * @generated
	 * @ordered
	 */
	public static final int MECHANICAL_VALUE = 0;

	/**
	 * The '<em><b>Electrical</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ELECTRICAL
	 * @model name="Electrical"
	 * @generated
	 * @ordered
	 */
	public static final int ELECTRICAL_VALUE = 1;

	/**
	 * The '<em><b>Noise</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOISE
	 * @model name="Noise"
	 * @generated
	 * @ordered
	 */
	public static final int NOISE_VALUE = 2;

	/**
	 * The '<em><b>Vibration</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VIBRATION
	 * @model name="Vibration"
	 * @generated
	 * @ordered
	 */
	public static final int VIBRATION_VALUE = 3;

	/**
	 * The '<em><b>Radiation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RADIATION
	 * @model name="Radiation"
	 * @generated
	 * @ordered
	 */
	public static final int RADIATION_VALUE = 4;

	/**
	 * The '<em><b>Material Substance</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MATERIAL_SUBSTANCE
	 * @model name="MaterialSubstance"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='Material/Substance'"
	 * @generated
	 * @ordered
	 */
	public static final int MATERIAL_SUBSTANCE_VALUE = 5;

	/**
	 * The '<em><b>Ergonomic</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERGONOMIC
	 * @model name="Ergonomic"
	 * @generated
	 * @ordered
	 */
	public static final int ERGONOMIC_VALUE = 6;

	/**
	 * The '<em><b>Environment Workspace</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENVIRONMENT_WORKSPACE
	 * @model name="EnvironmentWorkspace"
	 *        annotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='Environment/Workspace'"
	 * @generated
	 * @ordered
	 */
	public static final int ENVIRONMENT_WORKSPACE_VALUE = 7;

	/**
	 * The '<em><b>Combination</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMBINATION
	 * @model name="Combination"
	 * @generated
	 * @ordered
	 */
	public static final int COMBINATION_VALUE = 8;

	/**
	 * The '<em><b>Other</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model name="Other"
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 9;

	/**
	 * An array of all the '<em><b>Hazard Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final HazardType[] VALUES_ARRAY =
		new HazardType[] {
			MECHANICAL,
			ELECTRICAL,
			NOISE,
			VIBRATION,
			RADIATION,
			MATERIAL_SUBSTANCE,
			ERGONOMIC,
			ENVIRONMENT_WORKSPACE,
			COMBINATION,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Hazard Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<HazardType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Hazard Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HazardType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			HazardType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Hazard Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HazardType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			HazardType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Hazard Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HazardType get(int value) {
		switch (value) {
			case MECHANICAL_VALUE: return MECHANICAL;
			case ELECTRICAL_VALUE: return ELECTRICAL;
			case NOISE_VALUE: return NOISE;
			case VIBRATION_VALUE: return VIBRATION;
			case RADIATION_VALUE: return RADIATION;
			case MATERIAL_SUBSTANCE_VALUE: return MATERIAL_SUBSTANCE;
			case ERGONOMIC_VALUE: return ERGONOMIC;
			case ENVIRONMENT_WORKSPACE_VALUE: return ENVIRONMENT_WORKSPACE;
			case COMBINATION_VALUE: return COMBINATION;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private HazardType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //HazardType
