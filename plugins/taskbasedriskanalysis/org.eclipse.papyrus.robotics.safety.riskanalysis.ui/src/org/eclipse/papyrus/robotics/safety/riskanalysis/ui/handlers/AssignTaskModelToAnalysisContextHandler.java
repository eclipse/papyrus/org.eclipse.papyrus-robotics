/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.robotics.safety.riskanalysis.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.polarsys.esf.core.utils.ModelUtil;

public class AssignTaskModelToAnalysisContextHandler extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public AssignTaskModelToAnalysisContextHandler() {

	}	

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection vSelection = HandlerUtil.getCurrentSelection(event);
		Interface vSelectedInterface =
				(Interface) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getInterface());
		if (vSelectedInterface != null) {
			TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(vSelectedInterface.getPackage());
			vDomain.getCommandStack().execute(
					new RecordingCommand(vDomain) {
						@Override
						protected void doExecute() {
							// get the analysis context
							HazardAnalysisContext context = UMLUtil.getStereotypeApplication(vSelectedInterface, HazardAnalysisContext.class);
							if (context != null) {
								// create a proper menu
								EncapsulatedContentProvider ecp = new RoboticsContentProvider(vSelectedInterface,
										new UMLContentProvider(UMLFactory.eINSTANCE.createCallBehaviorAction(), UMLPackage.eINSTANCE.getCallBehaviorAction_Behavior()),
										Task.class, FileExtensions.BT_UML);
								EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(ecp, "Assign Task Model to HARA Context", false);
								// update the analysis context with information about the referenced task model							
								if (popupMenuState.show(Display.getDefault().getActiveShell())) {
									Object menuResult = popupMenuState.getSubResult();
									final Activity activity = menuResult instanceof Activity ? (Activity) menuResult : null;
									if (activity != null) {
										// get the task model to be referenced
										Task targetTask = UMLUtil.getStereotypeApplication(activity, Task.class);
										context.setTask(targetTask);
									}
								}
							}
						}
					}
			) ;
		}
		return null;
	}

}
