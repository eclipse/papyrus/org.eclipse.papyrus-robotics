package org.eclipse.papyrus.robotics.safety.riskanalysis.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.bt.profile.bt.TreeRoot;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.MenuHelper;
import org.eclipse.papyrus.robotics.core.provider.RoboticsContentProvider;
import org.eclipse.papyrus.robotics.core.utils.FileExtensions;
import org.eclipse.papyrus.robotics.profile.robotics.behavior.Task;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.utils.ModelUtil;

public class ImportRoboticTaskHandler extends AbstractHandler {

	/**
	 * Default constructor.
	 */
	public ImportRoboticTaskHandler() {
		
	}	

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection vSelection = HandlerUtil.getCurrentSelection(event);
		final Interface vSelectedInterface =
				(Interface) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getInterface());
		if (vSelectedInterface != null) {
			CallBehaviorAction tmp__ = UMLFactory.eINSTANCE.createCallBehaviorAction();
			EncapsulatedContentProvider ecp = new RoboticsContentProvider(vSelectedInterface,
					new UMLContentProvider(tmp__, UMLPackage.eINSTANCE.getCallBehaviorAction_Behavior()),
					Task.class, FileExtensions.BT_UML);

			// create a proper menu
			EnhancedPopupMenu popupMenuState = MenuHelper.createPopupMenu(ecp, "Import existing Robotic Task", false);

			// create and return the command to update the action
			CompositeCommand compositeCommand = new CompositeCommand("Import task");
			if (popupMenuState.show(Display.getDefault().getActiveShell())) {
				// get the selected operation (action)
				Object menuResult = popupMenuState.getSubResult();
				final Property prop = menuResult instanceof Property ? (Property) menuResult : null;
			}
		}
		return null;
	}

}
