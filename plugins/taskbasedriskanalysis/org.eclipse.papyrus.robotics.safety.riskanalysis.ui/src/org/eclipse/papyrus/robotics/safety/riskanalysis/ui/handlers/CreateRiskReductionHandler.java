/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.robotics.safety.riskanalysis.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.polarsys.esf.core.utils.ModelUtil;
import org.eclipse.papyrus.robotics.safety.riskanalysis.RiskReduction;


public class CreateRiskReductionHandler extends AbstractHandler  {
	/** Create Risk Reduction. */
	private static final String CREATE_RISKREDUCTION_LABEL = "Create Risk Reduction"; //$NON-NLS-1$

	/**
	 * Default constructor.
	 */
	public CreateRiskReductionHandler() {
		
	}	
	
	/**
	 * Get the selected element (Interface) and call the command for creating a RiskReduction.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection vSelection = HandlerUtil.getCurrentSelection(event);
		final Interface vSelectedInterface =
				(Interface) ModelUtil.getSelectedEObjectOfType(vSelection, UMLPackage.eINSTANCE.getInterface());
		if (vSelectedInterface != null) {
		
				TransactionalEditingDomain vDomain = ModelUtil.getTransactionalEditingDomain(vSelectedInterface.getPackage());
				RecordingCommand vCreateRiskReduction = new RecordingCommand(vDomain, CREATE_RISKREDUCTION_LABEL) {

					@Override
					protected void doExecute() {
						// compute the name for the new Class
						String vName = "RiskReduction";
						// Create the object 'Property' of the interface containing the hazard to analyse
						Property riskReductionProperty = vSelectedInterface.createOwnedAttribute(vName, null);

						// Apply 'RiskReduction' stereotype on 'hazardAnalysisProperty'
						StereotypeUtil.apply(riskReductionProperty, RiskReduction.class);
						
					}
				};

				// Verify if command can be executed
				if (vCreateRiskReduction.canExecute()) {
					// Execute command
					vDomain.getCommandStack().execute(vCreateRiskReduction);
				}
			}
		
		return null;
	}

	

}
