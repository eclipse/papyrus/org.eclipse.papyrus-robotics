/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Matteo MORELLI (CEA LIST) <matteo.morelli@cea.fr> - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.safety.riskanalysis.ui.propertytesters;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.safety.riskanalysis.HazardAnalysisContext;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.util.UMLUtil;

public class HazardAnalysisContextTester extends PropertyTester {

	public static final String IS_HAZARD_ANALYSIS_CONTEXT = "isHazardAnalysisContext";
	public static final String IS_TASK_MODEL_UNDEFINED    = "isTaskModelUndefined";

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (IS_HAZARD_ANALYSIS_CONTEXT.equals(property))
			return isHazardAnalysisContext((IStructuredSelection) receiver);
		else if (IS_TASK_MODEL_UNDEFINED.equals(property))
			return isTaskModelUndefined((IStructuredSelection) receiver);
		else
			return false;
	}

	protected boolean isHazardAnalysisContext(IStructuredSelection selection){
		return getHazardAnalysisContext(selection) != null;
	}

	protected boolean isTaskModelUndefined(IStructuredSelection selection){
		HazardAnalysisContext hac = getHazardAnalysisContext(selection);
		if (hac != null)
			return hac.getTask() == null;
		return false;
	}

	private HazardAnalysisContext getHazardAnalysisContext(IStructuredSelection structuredSelection){
		if(structuredSelection != null) {
			Object selection = structuredSelection.getFirstElement();
			if(selection instanceof IAdaptable) {
				EObject object = (EObject) ((IAdaptable) selection).getAdapter(EObject.class);
				if(object instanceof Interface)
					return UMLUtil.getStereotypeApplication((Interface)object, HazardAnalysisContext.class);
			}
		}
		return null;
	}
}