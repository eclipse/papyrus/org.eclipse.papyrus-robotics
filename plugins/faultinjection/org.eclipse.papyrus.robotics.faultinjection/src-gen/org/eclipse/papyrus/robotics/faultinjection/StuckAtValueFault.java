/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stuck At Value Fault</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getStuckAtValueFault()
 * @model
 * @generated
 */
public interface StuckAtValueFault extends Fault {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(float)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getStuckAtValueFault_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Float"
	 * @generated
	 */
	float getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(float value);

} // StuckAtValueFault
