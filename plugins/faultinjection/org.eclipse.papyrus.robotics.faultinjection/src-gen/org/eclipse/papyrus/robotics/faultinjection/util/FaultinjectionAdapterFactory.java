/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.robotics.faultinjection.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage
 * @generated
 */
public class FaultinjectionAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FaultinjectionPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultinjectionAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FaultinjectionPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FaultinjectionSwitch<Adapter> modelSwitch = new FaultinjectionSwitch<Adapter>() {
		@Override
		public Adapter caseFaultList(FaultList object) {
			return createFaultListAdapter();
		}

		@Override
		public Adapter caseFault(Fault object) {
			return createFaultAdapter();
		}

		@Override
		public Adapter caseReadout(Readout object) {
			return createReadoutAdapter();
		}

		@Override
		public Adapter caseStuckAt0Fault(StuckAt0Fault object) {
			return createStuckAt0FaultAdapter();
		}

		@Override
		public Adapter caseStuckAtLastValueFault(StuckAtLastValueFault object) {
			return createStuckAtLastValueFaultAdapter();
		}

		@Override
		public Adapter caseStuckAtValueFault(StuckAtValueFault object) {
			return createStuckAtValueFaultAdapter();
		}

		@Override
		public Adapter caseInvertedFault(InvertedFault object) {
			return createInvertedFaultAdapter();
		}

		@Override
		public Adapter caseTooHighFault(TooHighFault object) {
			return createTooHighFaultAdapter();
		}

		@Override
		public Adapter caseTooLowFault(TooLowFault object) {
			return createTooLowFaultAdapter();
		}

		@Override
		public Adapter caseRampUpFault(RampUpFault object) {
			return createRampUpFaultAdapter();
		}

		@Override
		public Adapter caseRampDownFault(RampDownFault object) {
			return createRampDownFaultAdapter();
		}

		@Override
		public Adapter caseBitFlipFault(BitFlipFault object) {
			return createBitFlipFaultAdapter();
		}

		@Override
		public Adapter caseOscillationFault(OscillationFault object) {
			return createOscillationFaultAdapter();
		}

		@Override
		public Adapter caseRandomFault(RandomFault object) {
			return createRandomFaultAdapter();
		}

		@Override
		public Adapter caseDelayFault(DelayFault object) {
			return createDelayFaultAdapter();
		}

		@Override
		public Adapter caseFIElement(FIElement object) {
			return createFIElementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.FaultList <em>Fault List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultList
	 * @generated
	 */
	public Adapter createFaultListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.Fault <em>Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.Fault
	 * @generated
	 */
	public Adapter createFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.Readout <em>Readout</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.Readout
	 * @generated
	 */
	public Adapter createReadoutAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAt0Fault <em>Stuck At0 Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAt0Fault
	 * @generated
	 */
	public Adapter createStuckAt0FaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtLastValueFault <em>Stuck At Last Value Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAtLastValueFault
	 * @generated
	 */
	public Adapter createStuckAtLastValueFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault <em>Stuck At Value Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault
	 * @generated
	 */
	public Adapter createStuckAtValueFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.InvertedFault <em>Inverted Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.InvertedFault
	 * @generated
	 */
	public Adapter createInvertedFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.TooHighFault <em>Too High Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.TooHighFault
	 * @generated
	 */
	public Adapter createTooHighFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.TooLowFault <em>Too Low Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.TooLowFault
	 * @generated
	 */
	public Adapter createTooLowFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.RampUpFault <em>Ramp Up Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.RampUpFault
	 * @generated
	 */
	public Adapter createRampUpFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.RampDownFault <em>Ramp Down Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.RampDownFault
	 * @generated
	 */
	public Adapter createRampDownFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.BitFlipFault <em>Bit Flip Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.BitFlipFault
	 * @generated
	 */
	public Adapter createBitFlipFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.OscillationFault <em>Oscillation Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.OscillationFault
	 * @generated
	 */
	public Adapter createOscillationFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.RandomFault <em>Random Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.RandomFault
	 * @generated
	 */
	public Adapter createRandomFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.DelayFault <em>Delay Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.DelayFault
	 * @generated
	 */
	public Adapter createDelayFaultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement <em>FI Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FIElement
	 * @generated
	 */
	public Adapter createFIElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FaultinjectionAdapterFactory
