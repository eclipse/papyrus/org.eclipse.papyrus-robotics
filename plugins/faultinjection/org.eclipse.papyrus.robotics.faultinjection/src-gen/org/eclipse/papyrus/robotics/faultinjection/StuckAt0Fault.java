/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stuck At0 Fault</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getStuckAt0Fault()
 * @model
 * @generated
 */
public interface StuckAt0Fault extends Fault {
} // StuckAt0Fault
