/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delay Fault</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.DelayFault#getDelay <em>Delay</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getDelayFault()
 * @model
 * @generated
 */
public interface DelayFault extends Fault {
	/**
	 * Returns the value of the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' attribute.
	 * @see #setDelay(long)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getDelayFault_Delay()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Long"
	 * @generated
	 */
	long getDelay();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.DelayFault#getDelay <em>Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay</em>' attribute.
	 * @see #getDelay()
	 * @generated
	 */
	void setDelay(long value);

} // DelayFault
