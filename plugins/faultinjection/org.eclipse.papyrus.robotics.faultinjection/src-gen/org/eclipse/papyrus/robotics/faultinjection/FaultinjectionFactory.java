/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage
 * @generated
 */
public interface FaultinjectionFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FaultinjectionFactory eINSTANCE = org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Fault List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fault List</em>'.
	 * @generated
	 */
	FaultList createFaultList();

	/**
	 * Returns a new object of class '<em>Readout</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Readout</em>'.
	 * @generated
	 */
	Readout createReadout();

	/**
	 * Returns a new object of class '<em>Stuck At0 Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stuck At0 Fault</em>'.
	 * @generated
	 */
	StuckAt0Fault createStuckAt0Fault();

	/**
	 * Returns a new object of class '<em>Stuck At Last Value Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stuck At Last Value Fault</em>'.
	 * @generated
	 */
	StuckAtLastValueFault createStuckAtLastValueFault();

	/**
	 * Returns a new object of class '<em>Stuck At Value Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stuck At Value Fault</em>'.
	 * @generated
	 */
	StuckAtValueFault createStuckAtValueFault();

	/**
	 * Returns a new object of class '<em>Inverted Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inverted Fault</em>'.
	 * @generated
	 */
	InvertedFault createInvertedFault();

	/**
	 * Returns a new object of class '<em>Too High Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Too High Fault</em>'.
	 * @generated
	 */
	TooHighFault createTooHighFault();

	/**
	 * Returns a new object of class '<em>Too Low Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Too Low Fault</em>'.
	 * @generated
	 */
	TooLowFault createTooLowFault();

	/**
	 * Returns a new object of class '<em>Ramp Up Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ramp Up Fault</em>'.
	 * @generated
	 */
	RampUpFault createRampUpFault();

	/**
	 * Returns a new object of class '<em>Ramp Down Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ramp Down Fault</em>'.
	 * @generated
	 */
	RampDownFault createRampDownFault();

	/**
	 * Returns a new object of class '<em>Bit Flip Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bit Flip Fault</em>'.
	 * @generated
	 */
	BitFlipFault createBitFlipFault();

	/**
	 * Returns a new object of class '<em>Oscillation Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Oscillation Fault</em>'.
	 * @generated
	 */
	OscillationFault createOscillationFault();

	/**
	 * Returns a new object of class '<em>Random Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Random Fault</em>'.
	 * @generated
	 */
	RandomFault createRandomFault();

	/**
	 * Returns a new object of class '<em>Delay Fault</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delay Fault</em>'.
	 * @generated
	 */
	DelayFault createDelayFault();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FaultinjectionPackage getFaultinjectionPackage();

} //FaultinjectionFactory
