/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionFactory
 * @model kind="package"
 * @generated
 */
public interface FaultinjectionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "faultinjection";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.eclipse.papyrus.robotics/faultinjection";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "faultinjection";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FaultinjectionPackage eINSTANCE = org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.FaultListImpl <em>Fault List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultListImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getFaultList()
	 * @generated
	 */
	int FAULT_LIST = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_LIST__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_LIST__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Simulation Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_LIST__SIMULATION_TIME = 2;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_LIST__ELEMENTS = 3;

	/**
	 * The number of structural features of the '<em>Fault List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_LIST_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Fault List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_LIST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.FIElementImpl <em>FI Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FIElementImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getFIElement()
	 * @generated
	 */
	int FI_ELEMENT = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FI_ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FI_ELEMENT__COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FI_ELEMENT__PORT = 2;

	/**
	 * The number of structural features of the '<em>FI Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FI_ELEMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>FI Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FI_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.FaultImpl <em>Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getFault()
	 * @generated
	 */
	int FAULT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT__NAME = FI_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT__COMPONENT = FI_ELEMENT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT__PORT = FI_ELEMENT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT__TRIGGER = FI_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT__DURATION = FI_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_FEATURE_COUNT = FI_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FAULT_OPERATION_COUNT = FI_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.ReadoutImpl <em>Readout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.ReadoutImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getReadout()
	 * @generated
	 */
	int READOUT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READOUT__NAME = FI_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READOUT__COMPONENT = FI_ELEMENT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READOUT__PORT = FI_ELEMENT__PORT;

	/**
	 * The number of structural features of the '<em>Readout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READOUT_FEATURE_COUNT = FI_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Readout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READOUT_OPERATION_COUNT = FI_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.StuckAt0FaultImpl <em>Stuck At0 Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.StuckAt0FaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getStuckAt0Fault()
	 * @generated
	 */
	int STUCK_AT0_FAULT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Stuck At0 Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Stuck At0 Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT0_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtLastValueFaultImpl <em>Stuck At Last Value Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtLastValueFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getStuckAtLastValueFault()
	 * @generated
	 */
	int STUCK_AT_LAST_VALUE_FAULT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Stuck At Last Value Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Stuck At Last Value Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_LAST_VALUE_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtValueFaultImpl <em>Stuck At Value Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtValueFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getStuckAtValueFault()
	 * @generated
	 */
	int STUCK_AT_VALUE_FAULT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT__VALUE = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Stuck At Value Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Stuck At Value Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUCK_AT_VALUE_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.InvertedFaultImpl <em>Inverted Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.InvertedFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getInvertedFault()
	 * @generated
	 */
	int INVERTED_FAULT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Inverted Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Inverted Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVERTED_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.TooHighFaultImpl <em>Too High Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.TooHighFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getTooHighFault()
	 * @generated
	 */
	int TOO_HIGH_FAULT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Too High Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Too High Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_HIGH_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.TooLowFaultImpl <em>Too Low Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.TooLowFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getTooLowFault()
	 * @generated
	 */
	int TOO_LOW_FAULT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Too Low Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Too Low Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOO_LOW_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.RampUpFaultImpl <em>Ramp Up Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.RampUpFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getRampUpFault()
	 * @generated
	 */
	int RAMP_UP_FAULT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Ramp Up Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ramp Up Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_UP_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.RampDownFaultImpl <em>Ramp Down Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.RampDownFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getRampDownFault()
	 * @generated
	 */
	int RAMP_DOWN_FAULT = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Ramp Down Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ramp Down Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAMP_DOWN_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.BitFlipFaultImpl <em>Bit Flip Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.BitFlipFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getBitFlipFault()
	 * @generated
	 */
	int BIT_FLIP_FAULT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Bit Flip Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bit Flip Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_FLIP_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.OscillationFaultImpl <em>Oscillation Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.OscillationFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getOscillationFault()
	 * @generated
	 */
	int OSCILLATION_FAULT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Oscillation Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Oscillation Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OSCILLATION_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.RandomFaultImpl <em>Random Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.RandomFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getRandomFault()
	 * @generated
	 */
	int RANDOM_FAULT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The number of structural features of the '<em>Random Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Random Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.DelayFaultImpl <em>Delay Fault</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.DelayFaultImpl
	 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getDelayFault()
	 * @generated
	 */
	int DELAY_FAULT = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT__NAME = FAULT__NAME;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT__COMPONENT = FAULT__COMPONENT;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT__PORT = FAULT__PORT;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT__TRIGGER = FAULT__TRIGGER;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT__DURATION = FAULT__DURATION;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT__DELAY = FAULT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Delay Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT_FEATURE_COUNT = FAULT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Delay Fault</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FAULT_OPERATION_COUNT = FAULT_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.FaultList <em>Fault List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fault List</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultList
	 * @generated
	 */
	EClass getFaultList();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.FaultList#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultList#getName()
	 * @see #getFaultList()
	 * @generated
	 */
	EAttribute getFaultList_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.FaultList#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultList#getDescription()
	 * @see #getFaultList()
	 * @generated
	 */
	EAttribute getFaultList_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.FaultList#getSimulationTime <em>Simulation Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Simulation Time</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultList#getSimulationTime()
	 * @see #getFaultList()
	 * @generated
	 */
	EAttribute getFaultList_SimulationTime();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.robotics.faultinjection.FaultList#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultList#getElements()
	 * @see #getFaultList()
	 * @generated
	 */
	EReference getFaultList_Elements();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.Fault <em>Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.Fault
	 * @generated
	 */
	EClass getFault();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.Fault#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.Fault#getTrigger()
	 * @see #getFault()
	 * @generated
	 */
	EAttribute getFault_Trigger();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.Fault#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duration</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.Fault#getDuration()
	 * @see #getFault()
	 * @generated
	 */
	EAttribute getFault_Duration();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.Readout <em>Readout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Readout</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.Readout
	 * @generated
	 */
	EClass getReadout();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAt0Fault <em>Stuck At0 Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stuck At0 Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAt0Fault
	 * @generated
	 */
	EClass getStuckAt0Fault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtLastValueFault <em>Stuck At Last Value Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stuck At Last Value Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAtLastValueFault
	 * @generated
	 */
	EClass getStuckAtLastValueFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault <em>Stuck At Value Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stuck At Value Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault
	 * @generated
	 */
	EClass getStuckAtValueFault();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault#getValue()
	 * @see #getStuckAtValueFault()
	 * @generated
	 */
	EAttribute getStuckAtValueFault_Value();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.InvertedFault <em>Inverted Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inverted Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.InvertedFault
	 * @generated
	 */
	EClass getInvertedFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.TooHighFault <em>Too High Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Too High Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.TooHighFault
	 * @generated
	 */
	EClass getTooHighFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.TooLowFault <em>Too Low Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Too Low Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.TooLowFault
	 * @generated
	 */
	EClass getTooLowFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.RampUpFault <em>Ramp Up Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ramp Up Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.RampUpFault
	 * @generated
	 */
	EClass getRampUpFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.RampDownFault <em>Ramp Down Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ramp Down Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.RampDownFault
	 * @generated
	 */
	EClass getRampDownFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.BitFlipFault <em>Bit Flip Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bit Flip Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.BitFlipFault
	 * @generated
	 */
	EClass getBitFlipFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.OscillationFault <em>Oscillation Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Oscillation Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.OscillationFault
	 * @generated
	 */
	EClass getOscillationFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.RandomFault <em>Random Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.RandomFault
	 * @generated
	 */
	EClass getRandomFault();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.DelayFault <em>Delay Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delay Fault</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.DelayFault
	 * @generated
	 */
	EClass getDelayFault();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.DelayFault#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.DelayFault#getDelay()
	 * @see #getDelayFault()
	 * @generated
	 */
	EAttribute getDelayFault_Delay();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement <em>FI Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FI Element</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FIElement
	 * @generated
	 */
	EClass getFIElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FIElement#getName()
	 * @see #getFIElement()
	 * @generated
	 */
	EAttribute getFIElement_Name();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FIElement#getComponent()
	 * @see #getFIElement()
	 * @generated
	 */
	EReference getFIElement_Component();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see org.eclipse.papyrus.robotics.faultinjection.FIElement#getPort()
	 * @see #getFIElement()
	 * @generated
	 */
	EReference getFIElement_Port();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FaultinjectionFactory getFaultinjectionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.FaultListImpl <em>Fault List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultListImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getFaultList()
		 * @generated
		 */
		EClass FAULT_LIST = eINSTANCE.getFaultList();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FAULT_LIST__NAME = eINSTANCE.getFaultList_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FAULT_LIST__DESCRIPTION = eINSTANCE.getFaultList_Description();

		/**
		 * The meta object literal for the '<em><b>Simulation Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FAULT_LIST__SIMULATION_TIME = eINSTANCE.getFaultList_SimulationTime();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FAULT_LIST__ELEMENTS = eINSTANCE.getFaultList_Elements();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.FaultImpl <em>Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getFault()
		 * @generated
		 */
		EClass FAULT = eINSTANCE.getFault();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FAULT__TRIGGER = eINSTANCE.getFault_Trigger();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FAULT__DURATION = eINSTANCE.getFault_Duration();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.ReadoutImpl <em>Readout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.ReadoutImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getReadout()
		 * @generated
		 */
		EClass READOUT = eINSTANCE.getReadout();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.StuckAt0FaultImpl <em>Stuck At0 Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.StuckAt0FaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getStuckAt0Fault()
		 * @generated
		 */
		EClass STUCK_AT0_FAULT = eINSTANCE.getStuckAt0Fault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtLastValueFaultImpl <em>Stuck At Last Value Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtLastValueFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getStuckAtLastValueFault()
		 * @generated
		 */
		EClass STUCK_AT_LAST_VALUE_FAULT = eINSTANCE.getStuckAtLastValueFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtValueFaultImpl <em>Stuck At Value Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.StuckAtValueFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getStuckAtValueFault()
		 * @generated
		 */
		EClass STUCK_AT_VALUE_FAULT = eINSTANCE.getStuckAtValueFault();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUCK_AT_VALUE_FAULT__VALUE = eINSTANCE.getStuckAtValueFault_Value();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.InvertedFaultImpl <em>Inverted Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.InvertedFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getInvertedFault()
		 * @generated
		 */
		EClass INVERTED_FAULT = eINSTANCE.getInvertedFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.TooHighFaultImpl <em>Too High Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.TooHighFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getTooHighFault()
		 * @generated
		 */
		EClass TOO_HIGH_FAULT = eINSTANCE.getTooHighFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.TooLowFaultImpl <em>Too Low Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.TooLowFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getTooLowFault()
		 * @generated
		 */
		EClass TOO_LOW_FAULT = eINSTANCE.getTooLowFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.RampUpFaultImpl <em>Ramp Up Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.RampUpFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getRampUpFault()
		 * @generated
		 */
		EClass RAMP_UP_FAULT = eINSTANCE.getRampUpFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.RampDownFaultImpl <em>Ramp Down Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.RampDownFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getRampDownFault()
		 * @generated
		 */
		EClass RAMP_DOWN_FAULT = eINSTANCE.getRampDownFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.BitFlipFaultImpl <em>Bit Flip Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.BitFlipFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getBitFlipFault()
		 * @generated
		 */
		EClass BIT_FLIP_FAULT = eINSTANCE.getBitFlipFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.OscillationFaultImpl <em>Oscillation Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.OscillationFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getOscillationFault()
		 * @generated
		 */
		EClass OSCILLATION_FAULT = eINSTANCE.getOscillationFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.RandomFaultImpl <em>Random Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.RandomFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getRandomFault()
		 * @generated
		 */
		EClass RANDOM_FAULT = eINSTANCE.getRandomFault();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.DelayFaultImpl <em>Delay Fault</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.DelayFaultImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getDelayFault()
		 * @generated
		 */
		EClass DELAY_FAULT = eINSTANCE.getDelayFault();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELAY_FAULT__DELAY = eINSTANCE.getDelayFault_Delay();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.robotics.faultinjection.impl.FIElementImpl <em>FI Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FIElementImpl
		 * @see org.eclipse.papyrus.robotics.faultinjection.impl.FaultinjectionPackageImpl#getFIElement()
		 * @generated
		 */
		EClass FI_ELEMENT = eINSTANCE.getFIElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FI_ELEMENT__NAME = eINSTANCE.getFIElement_Name();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FI_ELEMENT__COMPONENT = eINSTANCE.getFIElement_Component();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FI_ELEMENT__PORT = eINSTANCE.getFIElement_Port();

	}

} //FaultinjectionPackage
