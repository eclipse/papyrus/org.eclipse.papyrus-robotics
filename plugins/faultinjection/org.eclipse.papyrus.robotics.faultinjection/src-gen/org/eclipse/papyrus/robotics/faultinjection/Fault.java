/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fault</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.Fault#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.Fault#getDuration <em>Duration</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFault()
 * @model abstract="true"
 * @generated
 */
public interface Fault extends FIElement {
	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see #setTrigger(float)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFault_Trigger()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.Float" ordered="false"
	 * @generated
	 */
	float getTrigger();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.Fault#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(float value);

	/**
	 * Returns the value of the '<em><b>Duration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration</em>' attribute.
	 * @see #setDuration(float)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFault_Duration()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.Float" ordered="false"
	 * @generated
	 */
	float getDuration();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.Fault#getDuration <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration</em>' attribute.
	 * @see #getDuration()
	 * @generated
	 */
	void setDuration(float value);

} // Fault
