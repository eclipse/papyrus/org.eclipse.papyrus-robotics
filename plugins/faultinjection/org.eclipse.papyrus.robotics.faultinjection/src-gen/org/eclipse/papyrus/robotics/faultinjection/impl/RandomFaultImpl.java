/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage;
import org.eclipse.papyrus.robotics.faultinjection.RandomFault;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Random Fault</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RandomFaultImpl extends FaultImpl implements RandomFault {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RandomFaultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FaultinjectionPackage.Literals.RANDOM_FAULT;
	}

} //RandomFaultImpl
