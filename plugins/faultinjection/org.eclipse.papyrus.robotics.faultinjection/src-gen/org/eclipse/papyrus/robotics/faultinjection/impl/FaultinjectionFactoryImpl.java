/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.robotics.faultinjection.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FaultinjectionFactoryImpl extends EFactoryImpl implements FaultinjectionFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FaultinjectionFactory init() {
		try {
			FaultinjectionFactory theFaultinjectionFactory = (FaultinjectionFactory) EPackage.Registry.INSTANCE.getEFactory(FaultinjectionPackage.eNS_URI);
			if (theFaultinjectionFactory != null) {
				return theFaultinjectionFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FaultinjectionFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultinjectionFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case FaultinjectionPackage.FAULT_LIST:
			return createFaultList();
		case FaultinjectionPackage.READOUT:
			return createReadout();
		case FaultinjectionPackage.STUCK_AT0_FAULT:
			return createStuckAt0Fault();
		case FaultinjectionPackage.STUCK_AT_LAST_VALUE_FAULT:
			return createStuckAtLastValueFault();
		case FaultinjectionPackage.STUCK_AT_VALUE_FAULT:
			return createStuckAtValueFault();
		case FaultinjectionPackage.INVERTED_FAULT:
			return createInvertedFault();
		case FaultinjectionPackage.TOO_HIGH_FAULT:
			return createTooHighFault();
		case FaultinjectionPackage.TOO_LOW_FAULT:
			return createTooLowFault();
		case FaultinjectionPackage.RAMP_UP_FAULT:
			return createRampUpFault();
		case FaultinjectionPackage.RAMP_DOWN_FAULT:
			return createRampDownFault();
		case FaultinjectionPackage.BIT_FLIP_FAULT:
			return createBitFlipFault();
		case FaultinjectionPackage.OSCILLATION_FAULT:
			return createOscillationFault();
		case FaultinjectionPackage.RANDOM_FAULT:
			return createRandomFault();
		case FaultinjectionPackage.DELAY_FAULT:
			return createDelayFault();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FaultList createFaultList() {
		FaultListImpl faultList = new FaultListImpl();
		return faultList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Readout createReadout() {
		ReadoutImpl readout = new ReadoutImpl();
		return readout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StuckAt0Fault createStuckAt0Fault() {
		StuckAt0FaultImpl stuckAt0Fault = new StuckAt0FaultImpl();
		return stuckAt0Fault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StuckAtLastValueFault createStuckAtLastValueFault() {
		StuckAtLastValueFaultImpl stuckAtLastValueFault = new StuckAtLastValueFaultImpl();
		return stuckAtLastValueFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StuckAtValueFault createStuckAtValueFault() {
		StuckAtValueFaultImpl stuckAtValueFault = new StuckAtValueFaultImpl();
		return stuckAtValueFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InvertedFault createInvertedFault() {
		InvertedFaultImpl invertedFault = new InvertedFaultImpl();
		return invertedFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TooHighFault createTooHighFault() {
		TooHighFaultImpl tooHighFault = new TooHighFaultImpl();
		return tooHighFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TooLowFault createTooLowFault() {
		TooLowFaultImpl tooLowFault = new TooLowFaultImpl();
		return tooLowFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RampUpFault createRampUpFault() {
		RampUpFaultImpl rampUpFault = new RampUpFaultImpl();
		return rampUpFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RampDownFault createRampDownFault() {
		RampDownFaultImpl rampDownFault = new RampDownFaultImpl();
		return rampDownFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BitFlipFault createBitFlipFault() {
		BitFlipFaultImpl bitFlipFault = new BitFlipFaultImpl();
		return bitFlipFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OscillationFault createOscillationFault() {
		OscillationFaultImpl oscillationFault = new OscillationFaultImpl();
		return oscillationFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RandomFault createRandomFault() {
		RandomFaultImpl randomFault = new RandomFaultImpl();
		return randomFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DelayFault createDelayFault() {
		DelayFaultImpl delayFault = new DelayFaultImpl();
		return delayFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FaultinjectionPackage getFaultinjectionPackage() {
		return (FaultinjectionPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FaultinjectionPackage getPackage() {
		return FaultinjectionPackage.eINSTANCE;
	}

} //FaultinjectionFactoryImpl
