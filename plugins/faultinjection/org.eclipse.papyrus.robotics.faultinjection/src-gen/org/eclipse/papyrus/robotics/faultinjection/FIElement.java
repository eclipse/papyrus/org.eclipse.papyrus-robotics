/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FI Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getComponent <em>Component</em>}</li>
 *   <li>{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFIElement()
 * @model abstract="true"
 * @generated
 */
public interface FIElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFIElement_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(EObject)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFIElement_Component()
	 * @model
	 * @generated
	 */
	EObject getComponent();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(EObject value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' reference.
	 * @see #setPort(EObject)
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#getFIElement_Port()
	 * @model
	 * @generated
	 */
	EObject getPort();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.faultinjection.FIElement#getPort <em>Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(EObject value);

} // FIElement
