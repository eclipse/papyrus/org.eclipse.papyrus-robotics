/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.robotics.faultinjection.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage
 * @generated
 */
public class FaultinjectionSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FaultinjectionPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaultinjectionSwitch() {
		if (modelPackage == null) {
			modelPackage = FaultinjectionPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case FaultinjectionPackage.FAULT_LIST: {
			FaultList faultList = (FaultList) theEObject;
			T result = caseFaultList(faultList);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.FAULT: {
			Fault fault = (Fault) theEObject;
			T result = caseFault(fault);
			if (result == null)
				result = caseFIElement(fault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.READOUT: {
			Readout readout = (Readout) theEObject;
			T result = caseReadout(readout);
			if (result == null)
				result = caseFIElement(readout);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.STUCK_AT0_FAULT: {
			StuckAt0Fault stuckAt0Fault = (StuckAt0Fault) theEObject;
			T result = caseStuckAt0Fault(stuckAt0Fault);
			if (result == null)
				result = caseFault(stuckAt0Fault);
			if (result == null)
				result = caseFIElement(stuckAt0Fault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.STUCK_AT_LAST_VALUE_FAULT: {
			StuckAtLastValueFault stuckAtLastValueFault = (StuckAtLastValueFault) theEObject;
			T result = caseStuckAtLastValueFault(stuckAtLastValueFault);
			if (result == null)
				result = caseFault(stuckAtLastValueFault);
			if (result == null)
				result = caseFIElement(stuckAtLastValueFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.STUCK_AT_VALUE_FAULT: {
			StuckAtValueFault stuckAtValueFault = (StuckAtValueFault) theEObject;
			T result = caseStuckAtValueFault(stuckAtValueFault);
			if (result == null)
				result = caseFault(stuckAtValueFault);
			if (result == null)
				result = caseFIElement(stuckAtValueFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.INVERTED_FAULT: {
			InvertedFault invertedFault = (InvertedFault) theEObject;
			T result = caseInvertedFault(invertedFault);
			if (result == null)
				result = caseFault(invertedFault);
			if (result == null)
				result = caseFIElement(invertedFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.TOO_HIGH_FAULT: {
			TooHighFault tooHighFault = (TooHighFault) theEObject;
			T result = caseTooHighFault(tooHighFault);
			if (result == null)
				result = caseFault(tooHighFault);
			if (result == null)
				result = caseFIElement(tooHighFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.TOO_LOW_FAULT: {
			TooLowFault tooLowFault = (TooLowFault) theEObject;
			T result = caseTooLowFault(tooLowFault);
			if (result == null)
				result = caseFault(tooLowFault);
			if (result == null)
				result = caseFIElement(tooLowFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.RAMP_UP_FAULT: {
			RampUpFault rampUpFault = (RampUpFault) theEObject;
			T result = caseRampUpFault(rampUpFault);
			if (result == null)
				result = caseFault(rampUpFault);
			if (result == null)
				result = caseFIElement(rampUpFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.RAMP_DOWN_FAULT: {
			RampDownFault rampDownFault = (RampDownFault) theEObject;
			T result = caseRampDownFault(rampDownFault);
			if (result == null)
				result = caseFault(rampDownFault);
			if (result == null)
				result = caseFIElement(rampDownFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.BIT_FLIP_FAULT: {
			BitFlipFault bitFlipFault = (BitFlipFault) theEObject;
			T result = caseBitFlipFault(bitFlipFault);
			if (result == null)
				result = caseFault(bitFlipFault);
			if (result == null)
				result = caseFIElement(bitFlipFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.OSCILLATION_FAULT: {
			OscillationFault oscillationFault = (OscillationFault) theEObject;
			T result = caseOscillationFault(oscillationFault);
			if (result == null)
				result = caseFault(oscillationFault);
			if (result == null)
				result = caseFIElement(oscillationFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.RANDOM_FAULT: {
			RandomFault randomFault = (RandomFault) theEObject;
			T result = caseRandomFault(randomFault);
			if (result == null)
				result = caseFault(randomFault);
			if (result == null)
				result = caseFIElement(randomFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.DELAY_FAULT: {
			DelayFault delayFault = (DelayFault) theEObject;
			T result = caseDelayFault(delayFault);
			if (result == null)
				result = caseFault(delayFault);
			if (result == null)
				result = caseFIElement(delayFault);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case FaultinjectionPackage.FI_ELEMENT: {
			FIElement fiElement = (FIElement) theEObject;
			T result = caseFIElement(fiElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fault List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fault List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFaultList(FaultList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFault(Fault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Readout</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Readout</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReadout(Readout object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stuck At0 Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stuck At0 Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStuckAt0Fault(StuckAt0Fault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stuck At Last Value Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stuck At Last Value Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStuckAtLastValueFault(StuckAtLastValueFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stuck At Value Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stuck At Value Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStuckAtValueFault(StuckAtValueFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inverted Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inverted Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInvertedFault(InvertedFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Too High Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Too High Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTooHighFault(TooHighFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Too Low Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Too Low Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTooLowFault(TooLowFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ramp Up Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ramp Up Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRampUpFault(RampUpFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ramp Down Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ramp Down Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRampDownFault(RampDownFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bit Flip Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bit Flip Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBitFlipFault(BitFlipFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Oscillation Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Oscillation Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOscillationFault(OscillationFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Random Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Random Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRandomFault(RandomFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay Fault</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay Fault</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelayFault(DelayFault object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FI Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FI Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFIElement(FIElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //FaultinjectionSwitch
