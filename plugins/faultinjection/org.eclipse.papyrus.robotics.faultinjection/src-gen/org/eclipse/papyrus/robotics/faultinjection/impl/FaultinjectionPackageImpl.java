/**
 * Copyright (c) 2019 Tecnalia Research & Innovation
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 * 
 * Contributors:
 *     Tecnalia Research & Innovation - initial API and implementation
 * 
 */
package org.eclipse.papyrus.robotics.faultinjection.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.papyrus.robotics.faultinjection.BitFlipFault;
import org.eclipse.papyrus.robotics.faultinjection.DelayFault;
import org.eclipse.papyrus.robotics.faultinjection.FIElement;
import org.eclipse.papyrus.robotics.faultinjection.Fault;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionFactory;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage;
import org.eclipse.papyrus.robotics.faultinjection.InvertedFault;
import org.eclipse.papyrus.robotics.faultinjection.OscillationFault;
import org.eclipse.papyrus.robotics.faultinjection.RampDownFault;
import org.eclipse.papyrus.robotics.faultinjection.RampUpFault;
import org.eclipse.papyrus.robotics.faultinjection.RandomFault;
import org.eclipse.papyrus.robotics.faultinjection.Readout;
import org.eclipse.papyrus.robotics.faultinjection.StuckAt0Fault;
import org.eclipse.papyrus.robotics.faultinjection.StuckAtLastValueFault;
import org.eclipse.papyrus.robotics.faultinjection.StuckAtValueFault;
import org.eclipse.papyrus.robotics.faultinjection.TooHighFault;
import org.eclipse.papyrus.robotics.faultinjection.TooLowFault;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FaultinjectionPackageImpl extends EPackageImpl implements FaultinjectionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass faultListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass faultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass readoutEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stuckAt0FaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stuckAtLastValueFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stuckAtValueFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass invertedFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tooHighFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tooLowFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rampUpFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rampDownFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bitFlipFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oscillationFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass randomFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass delayFaultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fiElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FaultinjectionPackageImpl() {
		super(eNS_URI, FaultinjectionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FaultinjectionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FaultinjectionPackage init() {
		if (isInited)
			return (FaultinjectionPackage) EPackage.Registry.INSTANCE.getEPackage(FaultinjectionPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFaultinjectionPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FaultinjectionPackageImpl theFaultinjectionPackage = registeredFaultinjectionPackage instanceof FaultinjectionPackageImpl ? (FaultinjectionPackageImpl) registeredFaultinjectionPackage : new FaultinjectionPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theFaultinjectionPackage.createPackageContents();

		// Initialize created meta-data
		theFaultinjectionPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFaultinjectionPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FaultinjectionPackage.eNS_URI, theFaultinjectionPackage);
		return theFaultinjectionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFaultList() {
		return faultListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFaultList_Name() {
		return (EAttribute) faultListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFaultList_Description() {
		return (EAttribute) faultListEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFaultList_SimulationTime() {
		return (EAttribute) faultListEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFaultList_Elements() {
		return (EReference) faultListEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFault() {
		return faultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFault_Trigger() {
		return (EAttribute) faultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFault_Duration() {
		return (EAttribute) faultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReadout() {
		return readoutEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStuckAt0Fault() {
		return stuckAt0FaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStuckAtLastValueFault() {
		return stuckAtLastValueFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStuckAtValueFault() {
		return stuckAtValueFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStuckAtValueFault_Value() {
		return (EAttribute) stuckAtValueFaultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInvertedFault() {
		return invertedFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTooHighFault() {
		return tooHighFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTooLowFault() {
		return tooLowFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRampUpFault() {
		return rampUpFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRampDownFault() {
		return rampDownFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBitFlipFault() {
		return bitFlipFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOscillationFault() {
		return oscillationFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRandomFault() {
		return randomFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDelayFault() {
		return delayFaultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDelayFault_Delay() {
		return (EAttribute) delayFaultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFIElement() {
		return fiElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFIElement_Name() {
		return (EAttribute) fiElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFIElement_Component() {
		return (EReference) fiElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFIElement_Port() {
		return (EReference) fiElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FaultinjectionFactory getFaultinjectionFactory() {
		return (FaultinjectionFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		faultListEClass = createEClass(FAULT_LIST);
		createEAttribute(faultListEClass, FAULT_LIST__NAME);
		createEAttribute(faultListEClass, FAULT_LIST__DESCRIPTION);
		createEAttribute(faultListEClass, FAULT_LIST__SIMULATION_TIME);
		createEReference(faultListEClass, FAULT_LIST__ELEMENTS);

		faultEClass = createEClass(FAULT);
		createEAttribute(faultEClass, FAULT__TRIGGER);
		createEAttribute(faultEClass, FAULT__DURATION);

		readoutEClass = createEClass(READOUT);

		stuckAt0FaultEClass = createEClass(STUCK_AT0_FAULT);

		stuckAtLastValueFaultEClass = createEClass(STUCK_AT_LAST_VALUE_FAULT);

		stuckAtValueFaultEClass = createEClass(STUCK_AT_VALUE_FAULT);
		createEAttribute(stuckAtValueFaultEClass, STUCK_AT_VALUE_FAULT__VALUE);

		invertedFaultEClass = createEClass(INVERTED_FAULT);

		tooHighFaultEClass = createEClass(TOO_HIGH_FAULT);

		tooLowFaultEClass = createEClass(TOO_LOW_FAULT);

		rampUpFaultEClass = createEClass(RAMP_UP_FAULT);

		rampDownFaultEClass = createEClass(RAMP_DOWN_FAULT);

		bitFlipFaultEClass = createEClass(BIT_FLIP_FAULT);

		oscillationFaultEClass = createEClass(OSCILLATION_FAULT);

		randomFaultEClass = createEClass(RANDOM_FAULT);

		delayFaultEClass = createEClass(DELAY_FAULT);
		createEAttribute(delayFaultEClass, DELAY_FAULT__DELAY);

		fiElementEClass = createEClass(FI_ELEMENT);
		createEAttribute(fiElementEClass, FI_ELEMENT__NAME);
		createEReference(fiElementEClass, FI_ELEMENT__COMPONENT);
		createEReference(fiElementEClass, FI_ELEMENT__PORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		faultEClass.getESuperTypes().add(this.getFIElement());
		readoutEClass.getESuperTypes().add(this.getFIElement());
		stuckAt0FaultEClass.getESuperTypes().add(this.getFault());
		stuckAtLastValueFaultEClass.getESuperTypes().add(this.getFault());
		stuckAtValueFaultEClass.getESuperTypes().add(this.getFault());
		invertedFaultEClass.getESuperTypes().add(this.getFault());
		tooHighFaultEClass.getESuperTypes().add(this.getFault());
		tooLowFaultEClass.getESuperTypes().add(this.getFault());
		rampUpFaultEClass.getESuperTypes().add(this.getFault());
		rampDownFaultEClass.getESuperTypes().add(this.getFault());
		bitFlipFaultEClass.getESuperTypes().add(this.getFault());
		oscillationFaultEClass.getESuperTypes().add(this.getFault());
		randomFaultEClass.getESuperTypes().add(this.getFault());
		delayFaultEClass.getESuperTypes().add(this.getFault());

		// Initialize classes, features, and operations; add parameters
		initEClass(faultListEClass, FaultList.class, "FaultList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getFaultList_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, FaultList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getFaultList_Description(), theXMLTypePackage.getString(), "description", null, 0, 1, FaultList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getFaultList_SimulationTime(), theXMLTypePackage.getFloat(), "simulationTime", null, 0, 1, FaultList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getFaultList_Elements(), this.getFIElement(), null, "elements", null, 0, -1, FaultList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(faultEClass, Fault.class, "Fault", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getFault_Trigger(), theXMLTypePackage.getFloat(), "trigger", null, 0, 1, Fault.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getFault_Duration(), theXMLTypePackage.getFloat(), "duration", null, 0, 1, Fault.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(readoutEClass, Readout.class, "Readout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(stuckAt0FaultEClass, StuckAt0Fault.class, "StuckAt0Fault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(stuckAtLastValueFaultEClass, StuckAtLastValueFault.class, "StuckAtLastValueFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(stuckAtValueFaultEClass, StuckAtValueFault.class, "StuckAtValueFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getStuckAtValueFault_Value(), theXMLTypePackage.getFloat(), "value", null, 0, 1, StuckAtValueFault.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(invertedFaultEClass, InvertedFault.class, "InvertedFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(tooHighFaultEClass, TooHighFault.class, "TooHighFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(tooLowFaultEClass, TooLowFault.class, "TooLowFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(rampUpFaultEClass, RampUpFault.class, "RampUpFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(rampDownFaultEClass, RampDownFault.class, "RampDownFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(bitFlipFaultEClass, BitFlipFault.class, "BitFlipFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(oscillationFaultEClass, OscillationFault.class, "OscillationFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(randomFaultEClass, RandomFault.class, "RandomFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(delayFaultEClass, DelayFault.class, "DelayFault", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getDelayFault_Delay(), theXMLTypePackage.getLong(), "delay", null, 0, 1, DelayFault.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(fiElementEClass, FIElement.class, "FIElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getFIElement_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, FIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getFIElement_Component(), ecorePackage.getEObject(), null, "component", null, 0, 1, FIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(getFIElement_Port(), ecorePackage.getEObject(), null, "port", null, 0, 1, FIElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);
	}

} //FaultinjectionPackageImpl
