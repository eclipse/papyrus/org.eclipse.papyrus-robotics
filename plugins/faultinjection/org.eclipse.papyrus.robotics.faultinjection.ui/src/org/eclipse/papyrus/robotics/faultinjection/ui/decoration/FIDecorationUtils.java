/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.decoration;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.services.decoration.DecorationService;
import org.eclipse.papyrus.infra.services.decoration.util.Decoration;
import org.eclipse.papyrus.infra.services.markerlistener.IPapyrusMarker;
import org.eclipse.papyrus.infra.ui.util.ServiceUtilsForHandlers;
import org.eclipse.papyrus.robotics.faultinjection.FIElement;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.papyrus.robotics.faultinjection.ui.Activator;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;

public class FIDecorationUtils {
	public static void addDecoration(FIElement element, ExecutionEvent event) {
		try {
			DecorationService decorationService =
					ServiceUtilsForHandlers.getInstance().getService(DecorationService.class, event);
			IPapyrusMarker marker = new FIMarker(element, decorationService);
			decorationService.addDecoration(marker, element.getPort());
		} catch (final ServiceException ex) {
			Activator.log.error(ex);
		}
	}

	public static void removeDecorations(Element anyElement, ExecutionEvent event) {
		try {
			DecorationService decorationService =
					ServiceUtilsForHandlers.getInstance().getService(DecorationService.class, event);
			List<String> removalList = new ArrayList<String>();
			for (Decoration d : decorationService.getDecorations().values()) {
				if (d.getType() == FIMarker.MARKER_TYPE) {
					removalList.add(d.getId());
				}
			}
			for (String id : removalList) {
				decorationService.removeDecoration(id);
			}
		} catch (final ServiceException ex) {
			Activator.log.error(ex);
		}
	}

	public static void addDecorations(FaultList list, ExecutionEvent event) {
		try {
			DecorationService decorationService =
					ServiceUtilsForHandlers.getInstance().getService(DecorationService.class, event);
			for (FIElement element : list.getElements()) {
				EObject port = element.getPort();
				if (port instanceof Port) {
					IPapyrusMarker marker = new FIMarker(element, decorationService);
					decorationService.addDecoration(marker, port);
				}
			}
		} catch (final ServiceException ex) {
			Activator.log.error(ex);
		}
	}

}
