/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.decoration;

import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.infra.services.decoration.DecorationService;
import org.eclipse.papyrus.infra.services.markerlistener.IPapyrusMarker;
import org.eclipse.papyrus.robotics.faultinjection.FIElement;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage;
import org.eclipse.uml2.uml.Port;

public class FIMarker implements IPapyrusMarker {

	public static final String MARKER_TYPE = "org.eclipse.papyrus.robotics.faultinjection"; //$NON-NLS-1$

	protected FIElement fiElement;

	public FIMarker(final FIElement fiElement, final DecorationService decorationService) {
		this.fiElement = fiElement;
		fiElement.eAdapters().add(new Adapter() {

			@Override
			public Notifier getTarget() {
				return null;
			}

			@Override
			public boolean isAdapterForType(Object type) {
				return false;
			}

			@Override
			public void notifyChanged(Notification notification) {
				// handle deletion and undo/redo
				if (notification.getEventType() == Notification.SET) {
					if (notification.getFeature() == FaultinjectionPackage.eINSTANCE.getFIElement_Port()) {
						String id = FIMarker.this.toString();
						decorationService.removeDecoration(id);
						if (notification.getNewValue() instanceof Port) {
							decorationService.addDecoration(FIMarker.this, (Port) notification.getNewValue());
						}
					}
				}
			}

			@Override
			public void setTarget(Notifier arg0) {
			}
		});
	}

	@Override
	public boolean exists() {
		return fiElement != null;
	}

	@Override
	public String getType() throws CoreException {
		return MARKER_TYPE;
	}

	@Override
	public String getTypeLabel() throws CoreException {
		return ""; //$NON-NLS-1$
	}

	@Override
	public Resource getResource() {
		return null;
	}

	@Override
	public EObject getEObject() {
		return null;
	}

	@Override
	public void delete() throws CoreException {
	}

	@Override

	public Object getAttribute(String name) throws CoreException {
		return null;
	}

	@Override
	public String getAttribute(String name, String defaultValue) {
		return null;
	}

	@Override
	public boolean getAttribute(String name, boolean defaultValue) {
		return false;
	}

	@Override
	public int getAttribute(String name, int defaultValue) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, ?> getAttributes() throws CoreException {
		return null;
	}

	@Override
	public boolean isSubtypeOf(String type) throws CoreException {
		return false;
	}

	@Override
	public void setAttribute(String name, Object value) throws CoreException {
	}

	@Override
	public void setAttribute(String name, String value) throws CoreException {
	}

	@Override
	public void setAttribute(String name, boolean value) throws CoreException {
	}

	@Override
	public void setAttribute(String name, int value) throws CoreException {
	}

	@Override
	public void setAttributes(Map<String, ?> attributes) throws CoreException {
	}
}
