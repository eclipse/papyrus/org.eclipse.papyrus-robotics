/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.menu.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionFactory;
import org.eclipse.papyrus.robotics.faultinjection.Readout;
import org.eclipse.papyrus.robotics.faultinjection.ui.Messages;
import org.eclipse.papyrus.robotics.faultinjection.ui.decoration.FIDecorationUtils;
import org.eclipse.papyrus.robotics.faultinjection.ui.utils.FaultInjectionUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class NewObservationHandler extends CmdHandler {

	@Override
	public boolean isEnabled() {
		updateSelectedEObject();
		return FaultInjectionUtils.getFaultInjectionResource(selectedEObject) != null;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if (selectedEObject instanceof Port) {

			IStructuredSelection selection = (IStructuredSelection) PlatformUI
					.getWorkbench().getActiveWorkbenchWindow()
					.getSelectionService().getSelection();

			View view = null;
			for (Object currentObject : selection.toList()) {
				// If the object is an edit part, try to get semantic bridge
				if (currentObject instanceof GraphicalEditPart) {
					GraphicalEditPart editPart = (GraphicalEditPart) currentObject;
					if (editPart.getModel() instanceof View) {
						view = (View) editPart.getModel();
					}
				}
			}
			final View parentView = (view != null) ? ViewUtil.getContainerView(view) : null;

			FaultList faultList = FaultInjectionUtils.getFaultList((Element) selectedEObject);

			if (faultList != null) {
				Readout readout = (Readout) FaultinjectionFactory.eINSTANCE.createReadout();

				// build commands
				TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(selectedEObject);
				RecordingCommand addSabotageFault = new RecordingCommand(ted,
						"Add readout") { //$NON-NLS-1$

					@Override
					protected void doExecute() {
						faultList.getElements().add(readout);
						readout.setPort(selectedEObject);
						if (parentView != null && parentView.getElement() instanceof Property) {
							readout.setComponent(parentView.getElement());
						}
						else {
							MessageDialog.openInformation(Display.getCurrent().getActiveShell(), Messages.NewObservationHandler_CANNOT_OBTAIN_PART, Messages.NewObservationHandler_REF_REMAINS_UNSET);
						}
						FIDecorationUtils.addDecoration(readout, event);
					}
				};
				// execute
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						ted.getCommandStack().execute(addSabotageFault);
					}
				});
			}
		}
		return null;
	}

}
