/**
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.papyrus.infra.widgets.providers.AbstractStaticContentProvider;
import org.eclipse.papyrus.infra.widgets.providers.IStaticContentProvider;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage;

public class FaultInjectionContentProvider extends AbstractStaticContentProvider implements IStaticContentProvider {

	@Override
	public Object[] getElements() {
		List<EClass> results = new ArrayList<EClass>();
		EClass fault = FaultinjectionPackage.eINSTANCE.getFault();
		for (EClassifier cl : FaultinjectionPackage.eINSTANCE.getEClassifiers()) {
			if (cl instanceof EClass) {
				EClass ecl = (EClass) cl;
				if (ecl.getEAllSuperTypes().contains(fault)) {
					results.add(ecl);
				}
			}
		}
		return results.toArray();
	}
}
