/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.menu.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.faultinjection.Fault;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionFactory;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionPackage;
import org.eclipse.papyrus.robotics.faultinjection.ui.decoration.FIDecorationUtils;
import org.eclipse.papyrus.robotics.faultinjection.ui.utils.FaultInjectionUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class NewFaultHandler extends CmdHandler {

	@Override
	public boolean isEnabled() {
		updateSelectedEObject();
		return FaultInjectionUtils.getFaultInjectionResource(selectedEObject) != null;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if (selectedEObject instanceof Port) {

			IStructuredSelection selection = (IStructuredSelection) PlatformUI
		                .getWorkbench().getActiveWorkbenchWindow()
		                .getSelectionService().getSelection();

			View view = null;
			for (Object currentObject : selection.toList()) {
				// If the object is an edit part, try to get semantic bridge
				if (currentObject instanceof GraphicalEditPart) {
					GraphicalEditPart editPart = (GraphicalEditPart) currentObject;
						if (editPart.getModel() instanceof View) {
		                     view = (View) editPart.getModel();
						}
				}
			}
			final View parentView = (view != null) ? ViewUtil.getContainerView(view) : null;

			FaultList faultList = FaultInjectionUtils.getFaultList((Element) selectedEObject);

			if (faultList != null) {
				List<Object> menuItems = new ArrayList<Object>();

				menuItems.add(new EnhancedPopupMenu.Disabled("Choose fault type"));
				menuItems.add(new EnhancedPopupMenu.Separator());
				EClass faultEC = FaultinjectionPackage.eINSTANCE.getFault();
				for (EClassifier cl : FaultinjectionPackage.eINSTANCE.getEClassifiers()) {
					if (cl instanceof EClass) {
						EClass ecl = (EClass) cl;
						if (ecl.getEAllSuperTypes().contains(faultEC)) {
							menuItems.add(ecl);
						}
					}
				}
				EnhancedPopupMenu popupMenuEvent = new EnhancedPopupMenu(menuItems, new LabelProvider() {
					@Override
					public String getText(Object element) {
						if (element instanceof EClass) {
							return ((EClass) element).getName();
						}
						else if (element instanceof String) {
							return (String) element;
						}
						return "";
					}
				});
				
				if (popupMenuEvent.show(Display.getCurrent().getActiveShell())) {
					Object menuResult = popupMenuEvent.getSubResult();
					if (menuResult instanceof EClass) {

						Fault fault = (Fault) FaultinjectionFactory.eINSTANCE.create((EClass) menuResult);

						// build commands
						TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(selectedEObject);
						RecordingCommand addSabotageFault = new RecordingCommand(ted,
								"Add fault") { //$NON-NLS-1$

							@Override
							protected void doExecute() {
								faultList.getElements().add(fault);
								fault.setPort(selectedEObject);
								if (parentView != null && parentView.getElement() instanceof Property) {
									fault.setComponent(parentView.getElement());
								}
								else {
									MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Cannot obtain part", "Component reference remains unset, please select a port of a part within an assembly diagram");
								}
								FIDecorationUtils.addDecoration(fault, event);
							}
						};
						// execute
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								ted.getCommandStack().execute(addSabotageFault);
							}
						});
					}
				}
			}
		}
		return null;
	}

}
