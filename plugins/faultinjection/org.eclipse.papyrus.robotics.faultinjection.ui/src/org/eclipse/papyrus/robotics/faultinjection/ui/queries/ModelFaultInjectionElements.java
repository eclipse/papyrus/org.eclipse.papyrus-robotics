/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * 
 * 	Ansgar Radermacher (ansgar.radermacher@cea.fr) CEA LIST
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.faultinjection.ui.queries;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.papyrus.robotics.faultinjection.ui.utils.FaultInjectionUtils;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;

/**
 * Variant of packaged elements query that hides component services (to be extended)
 */
public class ModelFaultInjectionElements implements IJavaQuery2<Element, FaultList> {

	public FaultList evaluate(final Element context,
			final IParameterValueList2 parameterValues,
			final IFacetManager facetManager)
			throws DerivedTypedElementException {

		if (context instanceof Model) {
			return FaultInjectionUtils.getFaultList((Model) context);
		}
		return null;
	}
}

