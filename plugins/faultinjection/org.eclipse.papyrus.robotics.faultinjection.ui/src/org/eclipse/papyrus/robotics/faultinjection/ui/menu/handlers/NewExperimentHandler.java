/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.menu.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.infra.core.resource.ReadOnlyAxis;
import org.eclipse.papyrus.infra.emf.readonly.ReadOnlyManager;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.papyrus.robotics.faultinjection.FaultinjectionFactory;
import org.eclipse.papyrus.robotics.faultinjection.ui.FaultInjectionConstants;
import org.eclipse.papyrus.robotics.faultinjection.ui.decoration.FIDecorationUtils;
import org.eclipse.papyrus.robotics.faultinjection.ui.utils.FaultInjectionUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Element;

public class NewExperimentHandler extends CmdHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();

		if (selectedEObject instanceof Element) {

			String path = selectedEObject.eResource().getURI().toString().replace(".uml", "." + FaultInjectionConstants.EXTENSION);
			final URI uri = URI.createURI(path, true);

			FaultInjectionUtils.removeFaultInjectionResource(selectedEObject);
			final Resource sabotageResource = selectedEObject.eResource().getResourceSet().createResource(uri);

			final FaultList faultList = FaultinjectionFactory.eINSTANCE.createFaultList();

			URI[] uris = { uri };
			ReadOnlyManager.getReadOnlyHandler(TransactionUtil.getEditingDomain(selectedEObject)).makeWritable(ReadOnlyAxis.anyAxis(), uris);

			// build commands
			TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(selectedEObject);
			RecordingCommand newSabotageModel = new RecordingCommand(ted,
					"New fault injection experiment") { //$NON-NLS-1$

				@Override
				protected void doExecute() {
					sabotageResource.getContents().add(faultList);
				}
			};
			// execute
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {
					ted.getCommandStack().execute(newSabotageModel);
				}
			});
			FIDecorationUtils.removeDecorations((Element) selectedEObject, event);
		}
		return null;
	}

}
