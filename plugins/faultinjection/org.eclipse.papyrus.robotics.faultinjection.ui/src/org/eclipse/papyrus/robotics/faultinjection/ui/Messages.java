package org.eclipse.papyrus.robotics.faultinjection.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.robotics.faultinjection.ui.messages"; //$NON-NLS-1$
	public static String LoadExperimentHandler_LOAD_EXPERIMENT;
	public static String LoadExperimentHandler_SELECT_EXPERIMENT;
	public static String NewObservationHandler_CANNOT_OBTAIN_PART;
	public static String NewObservationHandler_REF_REMAINS_UNSET;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
