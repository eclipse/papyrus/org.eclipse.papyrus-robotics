/**
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.utils;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.papyrus.robotics.faultinjection.FIElement;
import org.eclipse.papyrus.robotics.faultinjection.FaultList;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

public class FaultInjectionUtils {

	/**
	 * Get the fault list associated with the resource set the element is in
	 *
	 * @param element
	 * @return the fault list
	 */
	public static FaultList getFaultList(Element element) {
		Resource r = getFaultInjectionResource(element);
		return getFaultList(r);
	}

	public static FaultList getFaultList(Resource resource) {
		if (resource != null && resource.getContents().size() > 0) {
			if (resource.getContents().get(0) instanceof FaultList) {
				return (FaultList) resource.getContents().get(0);
			}
		}
		return null;
	}

	public static List<FIElement> getFaultsFromPart(Property part) {
		FaultList fl = getFaultList(part.getModel());
		List<FIElement> faults = new BasicEList<FIElement>();
		if (fl != null) {
			for (FIElement fault : fl.getElements()) {
				if (fault.getComponent() == part) {
					faults.add(fault);
				}
			}
		}
		return faults;
	}

	public static List<FIElement> getFaultsFromPort(Port port) {
		FaultList fl = getFaultList(port.getModel());
		List<FIElement> faults = new BasicEList<FIElement>();
		if (fl != null) {
			for (FIElement fault : fl.getElements()) {
				if (fault.getPort() == port) {
					faults.add(fault);
				}
			}
		}
		return faults;
	}

	public static List<FIElement> getFaultsFromPartAndPort(Property part, Port port) {
		FaultList fl = getFaultList(port.getModel());
		List<FIElement> faults = new BasicEList<FIElement>();
		if (fl != null) {
			for (FIElement fault : fl.getElements()) {
				if (fault.getPort() == port && fault.getComponent() == part) {
					faults.add(fault);
				}
			}
		}
		return faults;
	}

	public static Resource getFaultInjectionResource(EObject eObj) {
		if (eObj != null && eObj.eResource() != null) {
			ResourceSet rs = eObj.eResource().getResourceSet();
			for (Resource r : rs.getResources()) {
				if (getFaultList(r) != null) {
					return r;
				}
			}
		}
		return null;
	}

	public static void removeFaultInjectionResource(EObject eObj) {
		Resource r = getFaultInjectionResource(eObj);
		if (r != null) {
			eObj.eResource().getResourceSet().getResources().remove(r);
		}
	}

}
