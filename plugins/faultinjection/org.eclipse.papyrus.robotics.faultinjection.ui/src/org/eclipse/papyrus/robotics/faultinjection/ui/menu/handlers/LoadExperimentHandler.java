/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.menu.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.papyrus.infra.core.resource.ReadOnlyAxis;
import org.eclipse.papyrus.infra.emf.readonly.ReadOnlyManager;
import org.eclipse.papyrus.robotics.faultinjection.ui.FaultInjectionConstants;
import org.eclipse.papyrus.robotics.faultinjection.ui.Messages;
import org.eclipse.papyrus.robotics.faultinjection.ui.decoration.FIDecorationUtils;
import org.eclipse.papyrus.robotics.faultinjection.ui.utils.FaultInjectionUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.uml2.uml.Element;

public class LoadExperimentHandler extends CmdHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();

		if (selectedEObject instanceof Element) {
			ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
					Display.getDefault().getActiveShell(),
					new WorkbenchLabelProvider(),
					new BaseWorkbenchContentProvider());

			dialog.addFilter(new ViewerFilter() {
				@Override
				public boolean select(Viewer viewer, Object parentElement, Object element) {
					if (element instanceof IFile && true) {
						IFile file = (IFile) element;
						return FaultInjectionConstants.EXTENSION.equalsIgnoreCase(file.getFileExtension());
					}
					return true;
				}
			});

			dialog.setTitle(Messages.LoadExperimentHandler_LOAD_EXPERIMENT);
			dialog.setMessage(Messages.LoadExperimentHandler_SELECT_EXPERIMENT);
			dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
			String path = selectedEObject.eResource().getURI().toPlatformString(false);
			IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(
					new Path(path));
			boolean t = file.exists();
			dialog.setInitialSelection(file.getParent());

			if (dialog.open() == Window.OK) {
				Object selection = dialog.getFirstResult();
				if (selection instanceof IFile) {
					IFile selectedFile = (IFile) selection;

					final URI uri = URI.createPlatformResourceURI(selectedFile.getFullPath().toString(), true);

					FaultInjectionUtils.removeFaultInjectionResource(selectedEObject);

					final Resource sabotageResource = selectedEObject.eResource().getResourceSet().getResource(uri, true);

					ReadOnlyManager.getReadOnlyHandler(TransactionUtil.getEditingDomain(selectedEObject)).makeWritable(ReadOnlyAxis.anyAxis(), sabotageResource.getContents().get(0));
					FIDecorationUtils.removeDecorations((Element) selectedEObject, event);
					FIDecorationUtils.addDecorations(FaultInjectionUtils.getFaultList(sabotageResource), event);
				}
			}
		}
		return null;
	}

}
