/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.menu.handlers;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.window.Window;
import org.eclipse.papyrus.robotics.faultinjection.ui.FaultInjectionConstants;
import org.eclipse.papyrus.robotics.faultinjection.ui.utils.FaultInjectionUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.uml2.uml.Element;

public class SaveModelHandler extends CmdHandler {

	@Override
	public boolean isEnabled() {
		updateSelectedEObject();
		return FaultInjectionUtils.getFaultInjectionResource(selectedEObject) != null;
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if (selectedEObject instanceof Element) {
			Resource sabotageResource = FaultInjectionUtils.getFaultInjectionResource(selectedEObject);

			if (sabotageResource != null) {
				SaveAsDialog dialog = new SaveAsDialog(
						Display.getDefault().getActiveShell());

				dialog.setTitle("Save experiment");
				String path = selectedEObject.eResource().getURI().toPlatformString(false);
				IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(
						new Path(path).removeFileExtension().addFileExtension(FaultInjectionConstants.EXTENSION));
				dialog.setOriginalFile(file);

				if (dialog.open() == Window.OK) {
					Object selection = dialog.getResult();
					if (selection instanceof IPath) {
					
						final URI uri = URI.createPlatformResourceURI(((IPath) selection).toString(), true);

						sabotageResource.setURI(uri);
						try {
							sabotageResource.save(null);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
		return null;
	}

}
