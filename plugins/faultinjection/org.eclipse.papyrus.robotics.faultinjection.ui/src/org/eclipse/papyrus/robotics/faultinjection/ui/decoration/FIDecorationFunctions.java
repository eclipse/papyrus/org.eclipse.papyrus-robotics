/**
 * Copyright (c) 2019 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher (CEA LIST)- Initial API and implementation
 */
package org.eclipse.papyrus.robotics.faultinjection.ui.decoration;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.infra.services.decoration.IDecorationSpecificFunctions;
import org.eclipse.papyrus.infra.services.decoration.util.Decoration.PreferedPosition;
import org.eclipse.papyrus.infra.services.decoration.util.IPapyrusDecoration;
import org.eclipse.papyrus.infra.services.markerlistener.IPapyrusMarker;
import org.eclipse.papyrus.robotics.faultinjection.FIElement;
import org.eclipse.papyrus.robotics.faultinjection.Fault;
import org.eclipse.papyrus.robotics.faultinjection.Readout;

public class FIDecorationFunctions implements IDecorationSpecificFunctions {
	@Override
	public MarkChildren supportsMarkerPropagation() {
		// This marker should not be propagated
		return null;
	}

	@Override
	public ImageDescriptor getImageDescriptorForGE(final IPapyrusMarker marker) {
		return org.eclipse.papyrus.infra.widgets.Activator.getDefault().getImageDescriptor(
				"org.eclipse.papyrus.robotics.faultinjection.ui", //$NON-NLS-1$
				"icons/faultMarker.gif"); //$NON-NLS-1$
	}

	@Override
	public PreferedPosition getPreferedPosition(final IPapyrusMarker marker) {
		return PreferedPosition.NORTH_EAST;
	}

	@Override
	@SuppressWarnings("nls")
	public String getMessage(final IPapyrusMarker marker) {
		if (marker instanceof FIMarker) {
			FIElement element = ((FIMarker) marker).fiElement;
			if (element instanceof Fault) {
				return String.format("%s injection", element.eClass().getName());
			} else if (element instanceof Readout) {
				return "Observation";
			}
		}
		return "";
	}

	@Override
	public ImageDescriptor getImageDescriptorForME(IPapyrusMarker marker) {
		return getImageDescriptorForGE(marker);
	}

	@Override
	public int getPriority(IPapyrusMarker marker) {
		return 0;
	}

	@Override
	public IPapyrusDecoration markerPropagation(EList<IPapyrusDecoration> childDecorations) {
		return null;
	}
}