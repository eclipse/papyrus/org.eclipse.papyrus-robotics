/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.profile.util;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get informations on BPC profile resources
 *
 */
public final class AssertionsResource {

	public static final String PROFILES_PATHMAP = "pathmap://Robotics_PROFILES_ASSERTIONS/"; //$NON-NLS-1$	
	
	public static final String PROFILE_PATH = PROFILES_PATHMAP + "assertions.profile.uml"; //$NON-NLS-1$

	public static final URI PROFILE_PATH_URI = URI.createURI(PROFILE_PATH);

	public static final String PROFILE_URI = "http://www.eclipse.org/papyrus/robotics/assertions/1"; //$NON-NLS-1$
}
