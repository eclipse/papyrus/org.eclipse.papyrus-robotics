/**
 */
package org.eclipse.papyrus.robotics.assertions.profile.assertions;

import org.eclipse.papyrus.robotics.bpc.profile.bpc.Entity;

import org.eclipse.uml2.uml.Constraint;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion#getBase_Constraint <em>Base Constraint</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.robotics.assertions.profile.assertions.AssertionsPackage#getAssertion()
 * @model
 * @generated
 */
public interface Assertion extends Entity {
	/**
	 * Returns the value of the '<em><b>Base Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Constraint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Constraint</em>' reference.
	 * @see #setBase_Constraint(Constraint)
	 * @see org.eclipse.papyrus.robotics.assertions.profile.assertions.AssertionsPackage#getAssertion_Base_Constraint()
	 * @model ordered="false"
	 * @generated
	 */
	Constraint getBase_Constraint();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion#getBase_Constraint <em>Base Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Constraint</em>' reference.
	 * @see #getBase_Constraint()
	 * @generated
	 */
	void setBase_Constraint(Constraint value);

} // Assertion
