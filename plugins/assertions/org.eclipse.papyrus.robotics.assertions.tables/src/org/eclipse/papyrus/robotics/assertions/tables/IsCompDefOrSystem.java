/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.constraints.constraints.JavaQuery;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;

/**
 * Return true, if selected element is a component definition
 * => used in assertionTables.ctx to display associated tables
 */
public class IsCompDefOrSystem implements JavaQuery {

	@Override
	public boolean match(Object selection) {
		boolean matches = false;
		if (selection != null) {
			final EObject modelElement = EMFHelper.getEObject(selection);
			// Properties, Assertions or Contract table
			matches = modelElement instanceof Class && (
						StereotypeUtil.isApplied((Class) modelElement, ComponentDefinition.class)
					 || StereotypeUtil.isApplied((Class) modelElement, System.class));
		}
		return matches;
	}
}
