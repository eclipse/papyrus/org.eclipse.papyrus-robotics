/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.contracts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.jface.window.Window;
import org.eclipse.papyrus.infra.widgets.editors.TreeSelectorDialog;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.robotics.core.provider.EClassGraphicalContentProvider;
import org.eclipse.papyrus.robotics.core.provider.FilterStereotypes;
import org.eclipse.papyrus.robotics.properties.widgets.TableEditorPlus;
import org.eclipse.papyrus.uml.tools.providers.UMLLabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

public abstract class ContractTable extends TableEditorPlus {

	public ContractTable(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * Subclass should return feature (assumptions or guarantees)
	 */
	protected abstract EReference getFeature();

	@Override
	public AbstractEditCommandRequest createElementRequest() {
		Comment comment = (Comment) context;
		Contract contract = UMLUtil.getStereotypeApplication(comment, Contract.class);

		FilterStereotypes cp = new FilterStereotypes(
				new EClassGraphicalContentProvider(context, UMLPackage.eINSTANCE.getConstraint()), Assertion.class);
		TreeSelectorDialog dialog = new TreeSelectorDialog(Display.getCurrent().getActiveShell());
		dialog.setContentProvider(cp);
		dialog.setLabelProvider(new UMLLabelProvider());
		int ok = dialog.open();
		if (ok == Window.OK) {
			if (dialog.getResult().length == 1) {
				Object result = dialog.getResult()[0];
				if (result != null) {
					Assertion assertion = UMLUtil.getStereotypeApplication((Constraint) result, Assertion.class);
					@SuppressWarnings("unchecked")
					List<Assertion> list = new ArrayList<Assertion>((List<Assertion>) contract.eGet(getFeature()));
					if (!list.contains(assertion)) {
						list.add(assertion);
					}
					return new SetRequest(contract, getFeature(), list);
				}
			}
		}
		return null;
	}

	@Override
	public AbstractEditCommandRequest removeElementRequest(Object object) {
		Comment comment = (Comment) context;
		Contract contract = UMLUtil.getStereotypeApplication(comment, Contract.class);
		if (contract != null /* && assertion != null */) {
			@SuppressWarnings("unchecked")
			List<Assertion> list = new ArrayList<Assertion>((List<Assertion>) contract.eGet(getFeature()));
			list.remove((Assertion) object);
			return new SetRequest(contract, getFeature(), list);
		}
		return null;
	}
}
