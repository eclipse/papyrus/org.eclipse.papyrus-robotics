package org.eclipse.papyrus.robotics.assertions.tables.contracts;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.papyrus.robotics.properties.widgets.LayoutUtil;
import org.eclipse.papyrus.uml.properties.widgets.CommentPropertyEditor;
import org.eclipse.swt.widgets.Composite;

public class CommentEditorWoAdd extends CommentPropertyEditor {

	public CommentEditorWoAdd(Composite parent, int style) {
		super(parent, style);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(parent);
		LayoutUtil.fillVSpace(parent);
	}

	@Override
	protected void doBinding() {
		super.doBinding();
		// remove value factory => + button will be disabled
		editor.setFactory(null);
	}
}
