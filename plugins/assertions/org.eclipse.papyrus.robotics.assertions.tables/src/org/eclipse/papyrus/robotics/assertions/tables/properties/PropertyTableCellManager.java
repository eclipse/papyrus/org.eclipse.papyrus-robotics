/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.properties;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.IAxis;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

public class PropertyTableCellManager extends AbstractCellManager {

	/**
	 * Alias names as in table definitions
	 */
	private static final String EXPRESSION = "Expression"; //$NON-NLS-1$
	private static final String TYPE = "Type"; //$NON-NLS-1$
	private static final String NAME = "Name"; //$NON-NLS-1$
	private static final String DESC = "Description"; //$NON-NLS-1$

	// Table type handled by the cell manager
	public static final String PROPERTY_TABLE = "PropertyTable"; //$NON-NLS-1$

	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// Check if the table type is a NF property table. If not, the table cannot
		// be handled by this cell manager.
		if (columnElement instanceof IAxis) {
			EObject parent = ((IAxis) columnElement).eContainer(); // SlaveObjectAxisProvider
			if (null != parent) {
				parent = parent.eContainer();
				if (parent instanceof Table && ((Table) parent).getTableConfiguration().getType().equals(PROPERTY_TABLE)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isCellEditable(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// The cell can be edited as soon as the element displayed in the row
		// is a slot and the defining feature is only allowed to have at most
		// a single value.
		boolean isEditable = false;
		if (rowElement instanceof Property) {
			Property p = (Property) rowElement;
			if (p.upperBound() == 1) {
				isEditable = true;
			}
		}
		return isEditable;
	}

	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// Provide the value to be edited. The value to be edited is always a literal specification.
		// Below are the possible origins for the literal specifications:
		// - (1) The defining feature has no associated values. In that case, an empty
		// literal specification gets generated. This specification has no value.
		// - (2) The defining feature has an associated value and this value is a literal
		// specification. In that case, the literal specification is provided.
		// - (3) The defining feature has an associated value and this latter is an
		// an opaque expression. In this situation an literal string gets generated
		// and its value is the content of the first body of the opaque expression.
		Object value = ""; //$NON-NLS-1$
		String alias = columnElement instanceof IAxis ? ((IAxis) columnElement).getAlias() : null;

		if (rowElement instanceof Property && alias != null) {
			Property p = (Property) rowElement;
			
			if (alias.equals(NAME)) {
				return p.getName();
			} else if (alias.equals(TYPE)) {
				Type type = p.getType();
				return type != null ? type.getName() : "undefined"; //$NON-NLS-1$
			} else if (alias.equals(EXPRESSION)) {
				if (p != null && p.getDefaultValue() != null) {
					return p.getDefaultValue();
				}
			} else if (alias.equals(DESC)) {
				if (p != null) {
					ParameterEntry parameter = UMLUtil.getStereotypeApplication(p, ParameterEntry.class);
					if (parameter != null) {
						return parameter.getDescription();
					}
				}
			}
		}
		return value;
	}

	@Override
	public Command getSetValueCommand(TransactionalEditingDomain domain, Object columnElement, Object rowElement,
			Object newValue, INattableModelManager tableManager) {
		// The text provided by the cell editor in usage is retrieved. If the value associated
		// to the defining feature was an opaque expression then the first body of this opaque
		// expression is
		String alias = columnElement instanceof IAxis ? ((IAxis) columnElement).getAlias() : null;
		Property property = (Property) rowElement;
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(property);

		CompositeCommand editingCommand = new CompositeCommand(null);
		if (newValue instanceof Type) {
			AbstractEditCommandRequest setTypeReq = new SetRequest(domain, property, UMLPackage.eINSTANCE.getTypedElement_Type(), (Type) newValue);
			editingCommand.compose(provider.getEditCommand(setTypeReq));
		}
		else if (alias.equals(DESC)) {
			if (property != null) {
				ParameterEntry parameter = UMLUtil.getStereotypeApplication(property, ParameterEntry.class);
				if (parameter != null) {
					return new RecordingCommand(TransactionUtil.getEditingDomain(property)) {
						
						@Override
						protected void doExecute() {
							parameter.setDescription((String) newValue);
						}
					};
				}
			}
		}
		else if (newValue instanceof String) {
			String text = (String) newValue;
			if (text != null) {
				if (alias.equals(NAME)) {
					AbstractEditCommandRequest setNameReq = new SetRequest(domain, property, UMLPackage.eINSTANCE.getNamedElement_Name(), text);
					editingCommand.compose(provider.getEditCommand(setNameReq));
				}
				// nothing to do for expression & language, handled by popup dialog
			}
		}
		return new GMFtoEMFCommandWrapper(editingCommand);
	}
}
