/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.tables.properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

public class PropertyTableRowAxisManager extends AbstractUMLSynchronizedOnFeatureAxisManager {

	@Override
	protected List<Object> getFeaturesValue() {
		EObject context = getTableContext();
		List<Object> featureValue = new ArrayList<Object>();
		if (context instanceof Class) {
			Class contextCl = (Class) context;
			if (StereotypeUtil.isApplied(contextCl, ComponentDefinition.class)) {
				for (Property p : contextCl.getOwnedAttributes()) {
					if (StereotypeUtil.isApplied(p, org.eclipse.papyrus.robotics.assertions.profile.assertions.Property.class)) {
						featureValue.add(p);
					}
				}
			}
		}
		return featureValue;
	}

	@Override
	protected Collection<EStructuralFeature> getListenFeatures() {
		Collection<EStructuralFeature> listenedFeatures = super.getListenFeatures();
		listenedFeatures.add(UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute());
		return listenedFeatures;
	}
}
