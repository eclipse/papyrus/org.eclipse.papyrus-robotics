/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.assertions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Axis manager for assertions, it will list all constraints owned by a given element (a component or system).
 */
public class AssertionTableRowAxisManager extends AbstractUMLSynchronizedOnFeatureAxisManager {

	@Override
	protected List<Object> getFeaturesValue() {
		EObject context = getTableContext();
		List<Object> featureValue = new ArrayList<Object>();
		if (context instanceof Class) {
			Class contextCl = (Class) context;
			if (StereotypeUtil.isApplied(contextCl, ComponentDefinition.class)
					|| StereotypeUtil.isApplied(contextCl, System.class)) {
				for (Constraint c : contextCl.getOwnedRules()) {
					if (StereotypeUtil.isApplied(c, Assertion.class)) {
						featureValue.add(c);
					}
				}
			}
		}
		return featureValue;
	}

	@Override
	protected Collection<EStructuralFeature> getListenFeatures() {
		Collection<EStructuralFeature> listenedFeatures = super.getListenFeatures();
		// owned rules (of a namespace (class)
		listenedFeatures.add(UMLPackage.eINSTANCE.getNamespace_OwnedRule());
		return listenedFeatures;
	}
}
