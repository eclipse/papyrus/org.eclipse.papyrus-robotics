/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.nebula.widgets.nattable.edit.gui.AbstractDialogCellEditor;
import org.eclipse.papyrus.infra.properties.ui.creation.EditionDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.OpaqueExpression;

/**
 * A cell editor that opens an opaque expression view after a double-click.
 * Opened dialog is based on an XWT view defined in oepr.assertions.properties
 * (assertions.ctx)
 */
public class OpaqueExpressionCellEditor extends AbstractDialogCellEditor {
	
	public static final String ASSERTION_CTX = "Assertions"; //$NON-NLS-1$

	public static final String OPAQUE_EXPRESSION_VIEW = "Single OpaqueExpression"; //$NON-NLS-1$

	
	public class EditDialog extends EditionDialog {

		public EditDialog(Shell shell) {
			super(shell, true);
			setTitle("Edit expression");

			setViewData(ASSERTION_CTX, OPAQUE_EXPRESSION_VIEW);
		}
	}
	
	@Override
	public void setCanonicalValue(Object value) {
		if (value instanceof OpaqueExpression) {
			((EditDialog) dialog).setInput((OpaqueExpression) value);
		}
		else {
			throw new RuntimeException
				("Passed value must be an opaque expression, verify return value in cell manager"); //$NON-NLS-1$
		}
		super.setCanonicalValue(value);
	}

	@Override
	public void setEditorValue(Object input) {
	}
	
	@Override
	public int open() {
		return ((Dialog) dialog).open();
	}
	
	@Override
	public boolean isClosed() {
		return false;
	}
	
	@Override
	public Object getEditorValue() {
		return ((EditDialog) dialog).getResult()[0];
	}
	
	@Override
	public Object getDialogInstance() {
		return dialog;
	}
	
	@Override
	public Object createDialogInstance() {
		return new EditDialog(Display.getCurrent().getActiveShell());
	}
	
	@Override
	public void close() {
	}
}
