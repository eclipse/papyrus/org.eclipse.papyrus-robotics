/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *  Jabier Martinez, Tecnalia
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.assertions;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.nebula.widgets.nattable.grid.layer.GridLayer;
import org.eclipse.nebula.widgets.nattable.layer.ILayer;
import org.eclipse.nebula.widgets.nattable.selection.SelectionLayer;
import org.eclipse.papyrus.infra.nattable.layerstack.BodyLayerStack;
import org.eclipse.papyrus.robotics.assertions.types.AssertionElementTypesEnumerator;
import org.eclipse.papyrus.robotics.assertions.ui.actions.ValidateAssertionHandler;
import org.eclipse.papyrus.robotics.properties.widgets.TableEditorPlus;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.UMLPackage;

public class AssertionTable extends TableEditorPlus {

	protected Button calculate;

	public AssertionTable(Composite parent, int style) {
		super(parent, style);
	}

	@Override
	public AbstractEditCommandRequest createElementRequest() {
		CreateElementRequest createAssertionRequest = new CreateElementRequest(context,
				AssertionElementTypesEnumerator.ASSERTION, UMLPackage.eINSTANCE.getNamespace_OwnedRule());

		return createAssertionRequest;
	}

	@Override
	public AbstractEditCommandRequest removeElementRequest(Object object) {
		return new DestroyElementRequest((EObject) object, false);
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		super.widgetSelected(e);
		if (e.getSource() == calculate) {
			// obtain selected row
			ILayer gridLayer = natTableWidget.getLayer();
			ILayer bodyLayer = ((GridLayer) gridLayer).getBodyLayer();
			SelectionLayer sl = ((BodyLayerStack) bodyLayer).getSelectionLayer();
			int row = sl.getSelectionAnchor().rowPosition;
			if (row >= 0) {
				Object object = nattableManager.getRowElement(row);
				if (object instanceof Constraint) {
					ValidateAssertionHandler.evaluateAndShowResult((Constraint) object);
				}
			} else {
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Calculate", "No assertion was selected");
			}
		}
	}
	
	@Override
	protected void createListControls() {
		super.createListControls();
		calculate = createButton(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_INFO_TSK), "Calculate selected element");
	}

}
