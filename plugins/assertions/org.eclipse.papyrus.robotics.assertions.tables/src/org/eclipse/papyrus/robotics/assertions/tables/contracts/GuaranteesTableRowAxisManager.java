/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.contracts;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.AssertionsPackage;

/**
 * Return all guarantees from owned Comments which are Contracts
 */
public class GuaranteesTableRowAxisManager extends ContractRowAxisManager {

	@Override
	protected EReference getFeature() {
		return AssertionsPackage.eINSTANCE.getContract_Guarantees();
	}
}
