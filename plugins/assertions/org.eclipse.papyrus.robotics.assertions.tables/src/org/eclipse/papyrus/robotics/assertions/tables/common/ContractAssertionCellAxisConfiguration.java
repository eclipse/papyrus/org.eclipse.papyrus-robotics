/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.common;

import org.eclipse.nebula.widgets.nattable.config.CellConfigAttributes;
import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.data.convert.DisplayConverter;
import org.eclipse.nebula.widgets.nattable.edit.EditConfigAttributes;
import org.eclipse.nebula.widgets.nattable.edit.editor.ICellEditor;
import org.eclipse.nebula.widgets.nattable.painter.cell.TextPainter;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.papyrus.robotics.assertions.tables.OpaqueExpressionCellEditor;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Common cell axis configuration for contract (assumptions and guarantees) and assertion tables.
 * Uses OpaqueEpression popup editor.
 */
public class ContractAssertionCellAxisConfiguration implements ICellAxisConfiguration {

	@Override
	public String getConfigurationId() {
		return "ContractAndAssertion.id"; //$NON-NLS-1$
	}

	@Override
	public String getConfigurationDescription() {
		return "ContractAndAssertion"; //$NON-NLS-1$
	}

	class LanguageDisplayConverter extends DisplayConverter {
		
		@Override
		public Object displayToCanonicalValue(Object display) {
			return display; // not possible in general
		}
		
		@Override
		public Object canonicalToDisplayValue(Object canonical) {
			if (canonical instanceof OpaqueExpression) {
				return ((OpaqueExpression) canonical).getLanguages().get(0);
			}
			return canonical;
		}
	};
	
	class BodyDisplayConverter extends DisplayConverter {
		
		@Override
		public Object displayToCanonicalValue(Object display) {
			return display; // not possible in general
		}
		
		@Override
		public Object canonicalToDisplayValue(Object canonical) {
			if (canonical instanceof OpaqueExpression) {
				return ((OpaqueExpression) canonical).getBodies().get(0);
			}
			return canonical;
		}
	};
	
	@Override
	public boolean handles(Table table, Object axisElement) {
		String type = table.getTableConfiguration().getType();
		Object represents = AxisUtils.getRepresentedElement(axisElement);
		boolean typeMatch =
				type.equals(ContractAssertionCellManager.ASSERTION_TABLE) ||
				type.equals(ContractAssertionCellManager.GUARANTEES_TABLE) ||
				type.equals(ContractAssertionCellManager.ASSUMPTIONS_TABLE);
		boolean axisMatch =
				represents.equals(UMLPackage.eINSTANCE.getOpaqueExpression_Language()) ||
				represents.equals(UMLPackage.eINSTANCE.getOpaqueExpression_Body());
		return typeMatch && axisMatch;
	}

	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axisElement, String configLabel) {
		final Object represents = AxisUtils.getRepresentedElement(axisElement);
		if (represents.equals(UMLPackage.eINSTANCE.getOpaqueExpression_Language())) {
			configRegistry.registerConfigAttribute(CellConfigAttributes.DISPLAY_CONVERTER, new LanguageDisplayConverter(),
					DisplayMode.NORMAL, configLabel);
		}
		else if (represents.equals(UMLPackage.eINSTANCE.getOpaqueExpression_Body())) {
			configRegistry.registerConfigAttribute(CellConfigAttributes.DISPLAY_CONVERTER, new BodyDisplayConverter(),
					DisplayMode.NORMAL, configLabel);
		}
		// TODO: NatTable bug? Need to register a new painter in order to activate the display converter above
		configRegistry.registerConfigAttribute(CellConfigAttributes.CELL_PAINTER, new TextPainter(),
			DisplayMode.NORMAL, configLabel);
		configRegistry.registerConfigAttribute(EditConfigAttributes.CELL_EDITOR, getCellEditor(),
			DisplayMode.EDIT, configLabel);
	}

	/**
	 * This allows to get the cell editor to use for the table.
	 * 
	 * @return The cell editor.
	 * 
	 * @since 3.0
	 */
	protected ICellEditor getCellEditor() {
		return new OpaqueExpressionCellEditor();
	}
}
