/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.tables;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.papyrus.infra.nattable.tester.ITableTester;

public class AssertionTableTester implements ITableTester {

	@Override
	public IStatus isAllowed(Object context) {
		IStatus status = null;
		if(context instanceof Class){
			status = Status.OK_STATUS;
		}
		return status;
	}
}
