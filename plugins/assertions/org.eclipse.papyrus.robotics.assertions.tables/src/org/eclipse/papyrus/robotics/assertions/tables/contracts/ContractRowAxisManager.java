/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.contracts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.uml.nattable.manager.axis.AbstractUMLSynchronizedOnFeatureAxisManager;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Return all assumptions from owned Comments which are Contracts
 */
public abstract class ContractRowAxisManager extends AbstractUMLSynchronizedOnFeatureAxisManager {

	protected Adapter adapter;
	
	protected abstract EReference getFeature();
	
	@SuppressWarnings("unchecked")
	@Override
	protected List<Object> getFeaturesValue() {
		EObject context = getTableContext();
		List<Object> featureValue = new ArrayList<Object>();
		if (context instanceof Comment) {
			Contract contract = UMLUtil.getStereotypeApplication((Comment) context, Contract.class);
			if (contract != null) {
				adapter = new AdapterImpl() {

					@Override
					public void notifyChanged(Notification notification) {
						if (notification.getFeature() == getFeature()) {
							featureValueHasChanged(notification);
						}
					}
				};
				contract.eAdapters().add(adapter);
				featureValue.addAll((List<Assertion>) contract.eGet(getFeature()));
			}
		}
		return featureValue;
	}

	@Override
	/**
	 *
	 * @param notification
	 *            update the list of the managed objects if its required
	 */
	protected void featureValueHasChanged(final Notification notification) {
		if (notification.isTouch()) {
			return;
		}
		if (notification.getEventType() == Notification.REMOVE_MANY) {
			// broken in current Papyrus;
			List<Object> toRemove = new ArrayList<Object>();
			Collection<?> oldValues = (Collection<?>) notification.getOldValue();
			for (final Object current : oldValues) {
				if (this.managedObject.contains(current)) {
					toRemove.add(current);
				}
			}
			if (toRemove.size() > 0) {
				updateManagedList(Collections.<Object>emptyList(), toRemove);
			}
		}
		else {
			super.featureValueHasChanged(notification);
		}
	}

	@Override
	protected void removeListeners() {
		super.removeListeners();
		EObject context = getTableContext();
		if (adapter != null && context instanceof Comment) {
			Contract contract = UMLUtil.getStereotypeApplication((Comment) context, Contract.class);
			contract.eAdapters().remove(adapter);
			adapter = null;
		}
	}

	@Override
	protected Collection<EStructuralFeature> getListenFeatures() {
		Collection<EStructuralFeature> listenedFeatures = super.getListenFeatures();
		listenedFeatures.add(getFeature());
		return listenedFeatures;
	}
}
