/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.contracts;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.constraints.constraints.JavaQuery;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Comment;

/**
 * Return true, if selected element is a contract
 * => used in assertionTables.ctx to display associated tables
 */
public class IsContract implements JavaQuery {

	@Override
	public boolean match(Object selection) {
		boolean matches = false;
		if (selection != null) {
			final EObject modelElement = EMFHelper.getEObject(selection);
			matches = modelElement instanceof Comment
				&& StereotypeUtil.isApplied((Comment) modelElement, Contract.class);
		}
		return matches;
	}
}
