/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Jeremie Tatibouet (CEA LIST)
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.tables.assertions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.designer.infra.base.StringUtils;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.IAxis;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AssertionTableCellManager extends AbstractCellManager {

	/**
	 * Alias names as in table definitions
	 */
	private static final String NAME = "Name"; //$NON-NLS-1$
	private static final String LANGUAGE = "Language"; //$NON-NLS-1$
	private static final String EXPRESSION = "Expression"; //$NON-NLS-1$
	private static final String DESC = "Description"; //$NON-NLS-1$

	// Table type handled by the cell manager
	public static final String ASSERTION_TABLE = "AssertionTable"; //$NON-NLS-1$
	public static final String ASSUMPTIONS_TABLE = "AssumptionsTable"; //$NON-NLS-1$
	public static final String GUARANTEES_TABLE = "GuaranteesTable"; //$NON-NLS-1$

	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// Check if the table type is PHYSYSTEM_TABLE_TYPE. If not, the table cannot
		// be handled by this cell manager.
		if (columnElement instanceof IAxis) {
			EObject parent = ((IAxis) columnElement).eContainer(); // SlaveObjectAxisProvider
			if (null != parent) {
				parent = parent.eContainer();
				String type = ((Table) parent).getTableConfiguration().getType();
				if (parent instanceof Table && (
						type.equals(ASSERTION_TABLE) ||
						type.equals(GUARANTEES_TABLE) ||
						type.equals(ASSUMPTIONS_TABLE))) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isCellEditable(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// The cell can be edited as soon as the element displayed in the row
		// is a slot and the defining feature is only allowed to have at most
		// a single value.
		boolean isEditable = false;
		if (rowElement instanceof Constraint) {
			isEditable = true;
		}
		return isEditable;
	}

	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// Provide the value to be edited. The value to be edited is always a literal specification.
		// Below are the possible origins for the literal specifications:
		// - (1) The defining feature has no associated values. In that case, an empty
		// literal specification gets generated. This specification has no value.
		// - (2) The defining feature has an associated value and this value is a literal
		// specification. In that case, the literal specification is provided.
		// - (3) The defining feature has an associated value and this latter is an
		// an opaque expression. In this situation an literal string gets generated
		// and its value is the content of the first body of the opaque expression.
		Object value = ""; //$NON-NLS-1$
		String alias = columnElement instanceof IAxis ? ((IAxis) columnElement).getAlias() : null;

		if (rowElement instanceof Constraint && alias != null) {
			Constraint c = (Constraint) rowElement;
			OpaqueExpression oe = null;
			if (c != null && c.getSpecification() instanceof OpaqueExpression) {
				oe = (OpaqueExpression) c.getSpecification();
			}
			else {
				return null;
			}
			if (alias.equals(NAME)) {
				return c.getName();
			} else if (alias.equals(LANGUAGE)) {
				if (oe.getLanguages().size() > 0) {
					return oe.getLanguages().get(0);
				}
			} else if (alias.equals(EXPRESSION)) {
				if (oe.getBodies().size() > 0) {
					return oe.getBodies().get(0);
				}
			} else if (alias.equals(DESC)) {
				if (c != null) {
					ParameterEntry parameter = UMLUtil.getStereotypeApplication(c, ParameterEntry.class);
					if (parameter != null) {
						return parameter.getDescription();
					}
				}
			}
		}
		return value;
	}

	@Override
	public Command getSetValueCommand(TransactionalEditingDomain domain, Object columnElement, Object rowElement,
			Object newValue, INattableModelManager tableManager) {
		// The text provided by the cell editor in usage is retrieved. If the value associated
		// to the defining feature was an opaque expression then the first body of this opaque
		// expression is
		String alias = columnElement instanceof IAxis ? ((IAxis) columnElement).getAlias() : null;
		Constraint constraint = (Constraint) rowElement;
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(constraint);

		CompositeCommand editingCommand = new CompositeCommand(null);
		if (newValue instanceof String) {
			String text = (String) newValue;
			if (text != null) {
				if (alias.equals(NAME)) {
					AbstractEditCommandRequest setNameReq = new SetRequest(domain, constraint, UMLPackage.eINSTANCE.getNamedElement_Name(), text);
					editingCommand.compose(provider.getEditCommand(setNameReq));
				} else if (alias.equals(EXPRESSION)) {
					text = StringUtils.unquote(text);
					
					if (constraint.getSpecification() instanceof OpaqueExpression) {
						OpaqueExpression expression = (OpaqueExpression) constraint.getSpecification();
						if (provider != null) {
							List<String> bodies = expression.getBodies();
							Collection<String> newBodies = new ArrayList<String>();
							newBodies.add(text);
							if (!bodies.isEmpty()) {
								newBodies.addAll(bodies.subList(1, bodies.size()));
							} else {
								Collection<String> newLanguages = new ArrayList<String>();
								newLanguages.add("Matlab"); //$NON-NLS-1$
								AbstractEditCommandRequest languageRequest = new SetRequest(domain, expression, UMLPackage.eINSTANCE.getOpaqueExpression_Language(), newLanguages);
								editingCommand.compose(provider.getEditCommand(languageRequest));
							}
							AbstractEditCommandRequest bodiesRequest = new SetRequest(domain, expression, UMLPackage.eINSTANCE.getOpaqueExpression_Body(), newBodies);
							editingCommand.compose(provider.getEditCommand(bodiesRequest));
						}
					}
				}
			}
		}
		return new GMFtoEMFCommandWrapper(editingCommand);
	}
}
