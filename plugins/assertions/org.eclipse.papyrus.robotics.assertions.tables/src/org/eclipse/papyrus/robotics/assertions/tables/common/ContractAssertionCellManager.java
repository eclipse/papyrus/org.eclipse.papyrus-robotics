/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.common;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.AbstractEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.GMFtoEMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.manager.cell.AbstractCellManager;
import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.model.nattable.nattableaxis.IAxis;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Common cell manager for contract (assumptions and guarantees) and assertion tables.
 * It supports constraints as rowElements and further assumes that the
 * constraint specification is based on an opaque expression
 */
public class ContractAssertionCellManager extends AbstractCellManager {

	/**
	 * Alias names as in table definitions
	 */
	private static final String NAME = "Name"; //$NON-NLS-1$
	private static final String LANGUAGE = "Language"; //$NON-NLS-1$
	private static final String EXPRESSION = "Expression"; //$NON-NLS-1$
	private static final String DESC = "Description"; //$NON-NLS-1$

	// Table type handled by the cell manager
	public static final String ASSERTION_TABLE = "AssertionTable"; //$NON-NLS-1$
	public static final String ASSUMPTIONS_TABLE = "AssumptionsTable"; //$NON-NLS-1$
	public static final String GUARANTEES_TABLE = "GuaranteesTable"; //$NON-NLS-1$

	@Override
	public boolean handles(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// Check if the table type is PHYSYSTEM_TABLE_TYPE. If not, the table cannot
		// be handled by this cell manager.
		if (columnElement instanceof IAxis) {
			EObject parent = ((IAxis) columnElement).eContainer(); // SlaveObjectAxisProvider
			if (null != parent) {
				parent = parent.eContainer();
				if (parent instanceof Table) {
					String type = ((Table) parent).getTableConfiguration().getType();
					if (type.equals(ASSERTION_TABLE) ||
						type.equals(GUARANTEES_TABLE) ||
						type.equals(ASSUMPTIONS_TABLE)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean isCellEditable(Object columnElement, Object rowElement, INattableModelManager mngr) {
		// The cell can be edited as soon as the element displayed in the row
		// is a slot and the defining feature is only allowed to have at most
		// a single value.
		boolean isEditable = false;
		if (rowElement instanceof Constraint) {
			isEditable = true;
		}
		return isEditable;
	}

	@Override
	protected Object doGetValue(Object columnElement, Object rowElement, INattableModelManager tableManager) {
		// row element is either an Assertion (in case of Assumptions or Guarantees) or
		// its base Constraint. Two different elements are used to match the different
		// features, in case of Contracts on the stereotype level, in case of a Class on
		// the UML level.
		Object value = ""; //$NON-NLS-1$
		String alias = columnElement instanceof IAxis ? ((IAxis) columnElement).getAlias() : null;

		Constraint c = null;
		if (rowElement instanceof Assertion) {
			c = ((Assertion) rowElement).getBase_Constraint();
		}
		else if (rowElement instanceof Constraint) {
			c = (Constraint) rowElement;
		}
		if (c != null && alias != null) {
			OpaqueExpression oe = null;
			if (c != null && c.getSpecification() instanceof OpaqueExpression) {
				oe = (OpaqueExpression) c.getSpecification();
			}
			else {
				return null;
			}
			if (alias.equals(NAME)) {
				return c.getName();
			} else if (alias.equals(LANGUAGE)) {
				return oe;
			} else if (alias.equals(EXPRESSION)) {
				return oe;
			} else if (alias.equals(DESC)) {
				if (c != null) {
					ParameterEntry parameter = UMLUtil.getStereotypeApplication(c, ParameterEntry.class);
					if (parameter != null) {
						return parameter.getDescription();
					}
				}
			}
		}
		return value;
	}

	@Override
	public Command getSetValueCommand(TransactionalEditingDomain domain, Object columnElement, Object rowElement,
			Object newValue, INattableModelManager tableManager) {
		// The text provided by the cell editor in usage is retrieved. If the value associated
		// to the defining feature was an opaque expression then the first body of this opaque
		// expression is
		String alias = columnElement instanceof IAxis ? ((IAxis) columnElement).getAlias() : null;
		Constraint constraint = (Constraint) rowElement;
		IElementEditService provider = ElementEditServiceUtils.getCommandProvider(constraint);

		CompositeCommand editingCommand = new CompositeCommand(null);
		if (newValue instanceof String) {
			String text = (String) newValue;
			if (text != null) {
				if (alias.equals(NAME)) {
					AbstractEditCommandRequest setNameReq = new SetRequest(domain, constraint, UMLPackage.eINSTANCE.getNamedElement_Name(), text);
					editingCommand.compose(provider.getEditCommand(setNameReq));
				}
				// nothing to do for expression & language, handled by popup dialog
			}
		}
		return new GMFtoEMFCommandWrapper(editingCommand);
	}
}
