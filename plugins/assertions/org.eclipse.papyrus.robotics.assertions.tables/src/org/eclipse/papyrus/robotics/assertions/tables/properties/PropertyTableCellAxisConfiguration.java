/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.tables.properties;

import org.eclipse.nebula.widgets.nattable.config.IConfigRegistry;
import org.eclipse.nebula.widgets.nattable.edit.EditConfigAttributes;
import org.eclipse.nebula.widgets.nattable.edit.editor.ICellEditor;
import org.eclipse.nebula.widgets.nattable.style.DisplayMode;
import org.eclipse.papyrus.infra.nattable.celleditor.config.ICellAxisConfiguration;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.utils.AxisUtils;
import org.eclipse.papyrus.robotics.assertions.tables.OpaqueExpressionCellEditor;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * The objective of this class is to install a pop-up editor for opaque expressions for NFProperties table
 */
public class PropertyTableCellAxisConfiguration implements ICellAxisConfiguration {

	@Override
	public String getConfigurationId() {
		return "OpaqueExpressionEditor.id"; //$NON-NLS-1$
	}

	@Override
	public String getConfigurationDescription() {
		return "OpaqueExpressionEditor"; //$NON-NLS-1$
	}

	@Override
	public boolean handles(Table table, Object axisElement) {
		String type = table.getTableConfiguration().getType();
		Object represents = AxisUtils.getRepresentedElement(axisElement);
		// supports default value in Property table
		boolean handles =
				type.equals(PropertyTableCellManager.PROPERTY_TABLE) &&
				represents.equals(UMLPackage.eINSTANCE.getProperty_DefaultValue());
		return handles;
	}

	
	@Override
	public void configureCellEditor(IConfigRegistry configRegistry, Object axis, String configLabel) {
		final Object axisElement = AxisUtils.getRepresentedElement(axis);
		ICellEditor cellEditor = getCellEditor(configRegistry, axisElement, configLabel);
		configRegistry.registerConfigAttribute(EditConfigAttributes.CELL_EDITOR, cellEditor, DisplayMode.EDIT, configLabel);
	}

	/**
	 * This allows to get the cell editor to use for the table.
	 * 
	 * @return The cell editor.
	 */
	protected ICellEditor getCellEditor(final IConfigRegistry configRegistry, final Object axis, final String configLabel) {
		return new OpaqueExpressionCellEditor();
	}
}
