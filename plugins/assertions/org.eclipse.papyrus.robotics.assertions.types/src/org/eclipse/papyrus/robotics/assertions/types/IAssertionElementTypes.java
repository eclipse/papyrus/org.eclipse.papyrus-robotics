/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   Ansgar Radermacher
 *   
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.types;

/**
 * Declaration of all constants for robotic element types.
 */
public interface IAssertionElementTypes {

	public static final String PREFIX = "org.eclipse.papyrus.robotics.assertions."; //$NON-NLS-1$

	public static final String ASSERTION_ID = PREFIX + "Assertion"; //$NON-NLS-1$
	public static final String ASSUMPTION_ID = PREFIX + "Assumption"; //$NON-NLS-1$
	public static final String GUARANTEE_ID = PREFIX + "Guarantee"; //$NON-NLS-1$
	public static final String PROPERTY_ID = PREFIX + "Property"; //$NON-NLS-1$
}
