/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.types.advice;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.robotics.assertions.profile.util.AssertionsResource;
import org.eclipse.papyrus.robotics.core.types.advice.AbstractApplyStereotypeEditHelperAdvice;
import org.eclipse.papyrus.robotics.core.utils.NamingUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.UMLPackage;

public class ContractAdvice extends AbstractApplyStereotypeEditHelperAdvice {

	private static final String CONTRACT_PREFIX = "C"; //$NON-NLS-1$

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateElementRequest) {
				CreateElementRequest createReq = (CreateElementRequest) context.getEditCommandRequest();
				if (!(createReq.getContainer() instanceof Class)) {
					return false;
				}
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 *
	 *      Add data-type.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Configure contract"); //$NON-NLS-1$
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Comment)) {
			return super.getAfterConfigureCommand(request);
		}
		final Comment contract = (Comment) newElement;

		final IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(contract);
		SetRequest setContractBodyReq = new SetRequest(contract, UMLPackage.eINSTANCE.getComment_Body(),
				NamingUtil.getName(contract, CONTRACT_PREFIX, "%04d")); //$NON-NLS-1$
		ICommand setContractBodyCmd = commandProvider.getEditCommand(setContractBodyReq);
		compositeCommand.add(setContractBodyCmd);

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	@Override
	protected URI getProfileURI() {
		return AssertionsResource.PROFILE_PATH_URI;
	}

	@Override
	protected java.lang.Class<? extends EObject> stereotypeToApply() {
		return Contract.class;
	}
}
