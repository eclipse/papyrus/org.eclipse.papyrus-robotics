/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.types;

import org.eclipse.gmf.runtime.emf.type.core.AbstractElementTypeEnumerator;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;

/**
 * Static list of robotics specific element types
 */
public class AssertionElementTypesEnumerator extends AbstractElementTypeEnumerator implements IAssertionElementTypes {

	public static final IHintedType ASSERTION = (IHintedType) getElementType(ASSERTION_ID);
	public static final IHintedType ASSUMPTION = (IHintedType) getElementType(ASSUMPTION_ID);
	public static final IHintedType GUARANTEE = (IHintedType) getElementType(GUARANTEE_ID);
	public static final IHintedType PROPERTY = (IHintedType) getElementType(PROPERTY_ID);
}
