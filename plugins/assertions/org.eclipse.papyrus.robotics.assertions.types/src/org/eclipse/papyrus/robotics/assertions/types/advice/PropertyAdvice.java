/*****************************************************************************
 * Copyright (c) 2020 CEA LIST
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  ansgar.radermacher@cea.fr  - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.types.advice;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.widgets.providers.EncapsulatedContentProvider;
import org.eclipse.papyrus.robotics.assertions.profile.util.AssertionsResource;
import org.eclipse.papyrus.robotics.core.commands.CancelCommand;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu;
import org.eclipse.papyrus.robotics.core.menu.EnhancedPopupMenu.SubSelect;
import org.eclipse.papyrus.robotics.core.types.advice.AbstractApplyStereotypeEditHelperAdvice;
import org.eclipse.papyrus.uml.tools.providers.UMLContentProvider;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;

public class PropertyAdvice extends AbstractApplyStereotypeEditHelperAdvice {

	// Language to use.
	private static final String DEFAULT_LANGUAGE = "OCL"; //$NON-NLS-1$

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(ogrg.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * 
	 *      Add data-type.
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		CompositeCommand compositeCommand = new CompositeCommand("Configure Property"); //$NON-NLS-1$
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Property)) {
			return super.getAfterConfigureCommand(request);
		}
		final Property property = (Property) newElement;

		EncapsulatedContentProvider cpWithWS = new UMLContentProvider(property, UMLPackage.eINSTANCE.getTypedElement_Type());
		SubSelect serviceDef = new EnhancedPopupMenu.SubSelect("Select parameter type", cpWithWS); //$NON-NLS-1$
		Object value = serviceDef.getResult();

		if (value instanceof Classifier) {
			final Classifier newType = (Classifier) value;

			RecordingCommand defaultValue = new RecordingCommand(TransactionUtil.getEditingDomain(newElement)) {
				@Override
				protected void doExecute() {
					property.setType(newType);
	
					OpaqueExpression oe = UMLFactory.eINSTANCE.createOpaqueExpression();
					property.setDefaultValue(oe);
					oe.getLanguages().add(DEFAULT_LANGUAGE);
					oe.getBodies().add(""); //$NON-NLS-1$
				}
			};

			compositeCommand.add(EMFtoGMFCommandWrapper.wrap(defaultValue));

		} else {
			return new CancelCommand(property);
		}
		

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	@Override
	protected URI getProfileURI() {
		return AssertionsResource.PROFILE_PATH_URI;
	}
	
	@Override
	protected Class<?  extends EObject> stereotypeToApply() {
		return org.eclipse.papyrus.robotics.assertions.profile.assertions.Property.class;
	}
}
