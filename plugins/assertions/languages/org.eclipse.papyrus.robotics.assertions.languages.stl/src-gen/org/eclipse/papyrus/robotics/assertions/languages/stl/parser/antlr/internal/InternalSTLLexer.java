package org.eclipse.papyrus.robotics.assertions.languages.stl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSTLLexer extends Lexer {
    public static final int RULE_EXP=9;
    public static final int RULE_MSEC=20;
    public static final int RULE_SIGN=64;
    public static final int RULE_DOMAINTYPEREAL=38;
    public static final int RULE_BINARYDIGITS=57;
    public static final int RULE_INTERNAL=36;
    public static final int RULE_DOMAINTYPEINT=42;
    public static final int RULE_UNDERSCORES=49;
    public static final int RULE_OUTPUT=35;
    public static final int RULE_NONZERODIGIT=47;
    public static final int RULE_ID=69;
    public static final int RULE_TIMES=24;
    public static final int RULE_DIGIT=50;
    public static final int RULE_COLON=15;
    public static final int RULE_LETTERORUNDERSCORE=67;
    public static final int RULE_DOMAINTYPELONG=40;
    public static final int RULE_USEC=21;
    public static final int RULE_INT=70;
    public static final int RULE_ML_COMMENT=72;
    public static final int RULE_BINARYDIGITORUNDERSCORE=60;
    public static final int RULE_IMPORT=33;
    public static final int RULE_DIGITSANDUNDERSCORES=51;
    public static final int RULE_AT=30;
    public static final int RULE_POW=10;
    public static final int RULE_DIVIDE=25;
    public static final int RULE_CONSTANT=37;
    public static final int RULE_SEC=19;
    public static final int RULE_DOMAINTYPEFLOAT=39;
    public static final int RULE_DOT=29;
    public static final int RULE_RBRACK=16;
    public static final int RULE_PSEC=31;
    public static final int RULE_LBRACE=26;
    public static final int RULE_HEXNUMERAL=45;
    public static final int RULE_HEXDIGITS=53;
    public static final int RULE_RBRACE=27;
    public static final int RULE_LETTER=68;
    public static final int RULE_EXPONENTINDICATOR=62;
    public static final int RULE_LBRACK=14;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int RULE_HEXDIGITORUNDERSCORE=56;
    public static final int RULE_LPAREN=5;
    public static final int RULE_DOMAINTYPECOMPLEX=41;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int RULE_NSEC=22;
    public static final int RULE_HEXDIGIT=54;
    public static final int RULE_COMMA=11;
    public static final int RULE_DOMAINTYPEBOOL=43;
    public static final int RULE_SQRT=8;
    public static final int RULE_DECIMALNUMERAL=44;
    public static final int RULE_BINARYNUMERAL=46;
    public static final int RULE_DIGITORUNDERSCORE=52;
    public static final int RULE_ABS=7;
    public static final int RULE_INTEGERLITERAL=12;
    public static final int RULE_IDENTIFIERPART=66;
    public static final int RULE_HEXDIGITSANDUNDERSCORES=55;
    public static final int RULE_SEMICOLON=28;
    public static final int RULE_IDENTIFIER=4;
    public static final int RULE_STRING=71;
    public static final int RULE_INPUT=34;
    public static final int RULE_DIGITS=48;
    public static final int RULE_UNIT=17;
    public static final int RULE_SL_COMMENT=73;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int RULE_PLUS=23;
    public static final int RULE_IDENTIFIERSTART=65;
    public static final int EOF=-1;
    public static final int T__76=76;
    public static final int RULE_ROS_TOPIC=32;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=74;
    public static final int RULE_SIGNEDINTEGER=63;
    public static final int RULE_ANY_OTHER=75;
    public static final int RULE_MINUS=13;
    public static final int RULE_RPAREN=6;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int RULE_BINARYDIGITSANDUNDERSCORES=59;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int RULE_BINARYDIGIT=58;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int RULE_EXPONENTPART=61;
    public static final int T__87=87;
    public static final int T__105=105;
    public static final int RULE_DECIMALREALLITERAL=18;

    // delegates
    // delegators

    public InternalSTLLexer() {;} 
    public InternalSTLLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalSTLLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalSTL.g"; }

    // $ANTLR start "T__76"
    public final void mT__76() throws RecognitionException {
        try {
            int _type = T__76;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:11:7: ( 'or' )
            // InternalSTL.g:11:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__76"

    // $ANTLR start "T__77"
    public final void mT__77() throws RecognitionException {
        try {
            int _type = T__77;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:12:7: ( '|' )
            // InternalSTL.g:12:9: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__77"

    // $ANTLR start "T__78"
    public final void mT__78() throws RecognitionException {
        try {
            int _type = T__78;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:13:7: ( 'and' )
            // InternalSTL.g:13:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__78"

    // $ANTLR start "T__79"
    public final void mT__79() throws RecognitionException {
        try {
            int _type = T__79;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:14:7: ( '&' )
            // InternalSTL.g:14:9: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__79"

    // $ANTLR start "T__80"
    public final void mT__80() throws RecognitionException {
        try {
            int _type = T__80;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:15:7: ( 'iff' )
            // InternalSTL.g:15:9: 'iff'
            {
            match("iff"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__80"

    // $ANTLR start "T__81"
    public final void mT__81() throws RecognitionException {
        try {
            int _type = T__81;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:16:7: ( '<->' )
            // InternalSTL.g:16:9: '<->'
            {
            match("<->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__81"

    // $ANTLR start "T__82"
    public final void mT__82() throws RecognitionException {
        try {
            int _type = T__82;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:17:7: ( 'implies' )
            // InternalSTL.g:17:9: 'implies'
            {
            match("implies"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__82"

    // $ANTLR start "T__83"
    public final void mT__83() throws RecognitionException {
        try {
            int _type = T__83;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:18:7: ( '->' )
            // InternalSTL.g:18:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__83"

    // $ANTLR start "T__84"
    public final void mT__84() throws RecognitionException {
        try {
            int _type = T__84;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:19:7: ( 'xor' )
            // InternalSTL.g:19:9: 'xor'
            {
            match("xor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__84"

    // $ANTLR start "T__85"
    public final void mT__85() throws RecognitionException {
        try {
            int _type = T__85;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:20:7: ( 'rise' )
            // InternalSTL.g:20:9: 'rise'
            {
            match("rise"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__85"

    // $ANTLR start "T__86"
    public final void mT__86() throws RecognitionException {
        try {
            int _type = T__86;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:21:7: ( 'fall' )
            // InternalSTL.g:21:9: 'fall'
            {
            match("fall"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__86"

    // $ANTLR start "T__87"
    public final void mT__87() throws RecognitionException {
        try {
            int _type = T__87;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:22:7: ( 'always' )
            // InternalSTL.g:22:9: 'always'
            {
            match("always"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__87"

    // $ANTLR start "T__88"
    public final void mT__88() throws RecognitionException {
        try {
            int _type = T__88;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:23:7: ( 'G' )
            // InternalSTL.g:23:9: 'G'
            {
            match('G'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__88"

    // $ANTLR start "T__89"
    public final void mT__89() throws RecognitionException {
        try {
            int _type = T__89;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:24:7: ( 'eventually' )
            // InternalSTL.g:24:9: 'eventually'
            {
            match("eventually"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__89"

    // $ANTLR start "T__90"
    public final void mT__90() throws RecognitionException {
        try {
            int _type = T__90;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:25:7: ( 'F' )
            // InternalSTL.g:25:9: 'F'
            {
            match('F'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__90"

    // $ANTLR start "T__91"
    public final void mT__91() throws RecognitionException {
        try {
            int _type = T__91;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:26:7: ( 'historically' )
            // InternalSTL.g:26:9: 'historically'
            {
            match("historically"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__91"

    // $ANTLR start "T__92"
    public final void mT__92() throws RecognitionException {
        try {
            int _type = T__92;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:27:7: ( 'H' )
            // InternalSTL.g:27:9: 'H'
            {
            match('H'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__92"

    // $ANTLR start "T__93"
    public final void mT__93() throws RecognitionException {
        try {
            int _type = T__93;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:28:7: ( 'once' )
            // InternalSTL.g:28:9: 'once'
            {
            match("once"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__93"

    // $ANTLR start "T__94"
    public final void mT__94() throws RecognitionException {
        try {
            int _type = T__94;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:29:7: ( 'O' )
            // InternalSTL.g:29:9: 'O'
            {
            match('O'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__94"

    // $ANTLR start "T__95"
    public final void mT__95() throws RecognitionException {
        try {
            int _type = T__95;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:30:7: ( 'next' )
            // InternalSTL.g:30:9: 'next'
            {
            match("next"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__95"

    // $ANTLR start "T__96"
    public final void mT__96() throws RecognitionException {
        try {
            int _type = T__96;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:31:7: ( 'X' )
            // InternalSTL.g:31:9: 'X'
            {
            match('X'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__96"

    // $ANTLR start "T__97"
    public final void mT__97() throws RecognitionException {
        try {
            int _type = T__97;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:32:7: ( 'prev' )
            // InternalSTL.g:32:9: 'prev'
            {
            match("prev"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__97"

    // $ANTLR start "T__98"
    public final void mT__98() throws RecognitionException {
        try {
            int _type = T__98;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:33:7: ( 'Y' )
            // InternalSTL.g:33:9: 'Y'
            {
            match('Y'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__98"

    // $ANTLR start "T__99"
    public final void mT__99() throws RecognitionException {
        try {
            int _type = T__99;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:34:7: ( '==' )
            // InternalSTL.g:34:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__99"

    // $ANTLR start "T__100"
    public final void mT__100() throws RecognitionException {
        try {
            int _type = T__100;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:35:8: ( '=' )
            // InternalSTL.g:35:10: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__100"

    // $ANTLR start "T__101"
    public final void mT__101() throws RecognitionException {
        try {
            int _type = T__101;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:36:8: ( '!==' )
            // InternalSTL.g:36:10: '!=='
            {
            match("!=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__101"

    // $ANTLR start "T__102"
    public final void mT__102() throws RecognitionException {
        try {
            int _type = T__102;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:37:8: ( '!=' )
            // InternalSTL.g:37:10: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__102"

    // $ANTLR start "T__103"
    public final void mT__103() throws RecognitionException {
        try {
            int _type = T__103;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:38:8: ( '>=' )
            // InternalSTL.g:38:10: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__103"

    // $ANTLR start "T__104"
    public final void mT__104() throws RecognitionException {
        try {
            int _type = T__104;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:39:8: ( '<=' )
            // InternalSTL.g:39:10: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__104"

    // $ANTLR start "T__105"
    public final void mT__105() throws RecognitionException {
        try {
            int _type = T__105;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:40:8: ( '>' )
            // InternalSTL.g:40:10: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__105"

    // $ANTLR start "T__106"
    public final void mT__106() throws RecognitionException {
        try {
            int _type = T__106;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:41:8: ( '<' )
            // InternalSTL.g:41:10: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__106"

    // $ANTLR start "T__107"
    public final void mT__107() throws RecognitionException {
        try {
            int _type = T__107;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:42:8: ( 'true' )
            // InternalSTL.g:42:10: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__107"

    // $ANTLR start "T__108"
    public final void mT__108() throws RecognitionException {
        try {
            int _type = T__108;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:43:8: ( 'TRUE' )
            // InternalSTL.g:43:10: 'TRUE'
            {
            match("TRUE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__108"

    // $ANTLR start "T__109"
    public final void mT__109() throws RecognitionException {
        try {
            int _type = T__109;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:44:8: ( 'false' )
            // InternalSTL.g:44:10: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__109"

    // $ANTLR start "T__110"
    public final void mT__110() throws RecognitionException {
        try {
            int _type = T__110;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:45:8: ( 'FALSE' )
            // InternalSTL.g:45:10: 'FALSE'
            {
            match("FALSE"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__110"

    // $ANTLR start "RULE_UNIT"
    public final void mRULE_UNIT() throws RecognitionException {
        try {
            int _type = RULE_UNIT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1830:11: ( ( RULE_SEC | RULE_MSEC | RULE_USEC | RULE_NSEC ) )
            // InternalSTL.g:1830:13: ( RULE_SEC | RULE_MSEC | RULE_USEC | RULE_NSEC )
            {
            // InternalSTL.g:1830:13: ( RULE_SEC | RULE_MSEC | RULE_USEC | RULE_NSEC )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 's':
                {
                alt1=1;
                }
                break;
            case 'm':
                {
                alt1=2;
                }
                break;
            case 'u':
                {
                alt1=3;
                }
                break;
            case 'n':
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalSTL.g:1830:14: RULE_SEC
                    {
                    mRULE_SEC(); 

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1830:23: RULE_MSEC
                    {
                    mRULE_MSEC(); 

                    }
                    break;
                case 3 :
                    // InternalSTL.g:1830:33: RULE_USEC
                    {
                    mRULE_USEC(); 

                    }
                    break;
                case 4 :
                    // InternalSTL.g:1830:43: RULE_NSEC
                    {
                    mRULE_NSEC(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNIT"

    // $ANTLR start "RULE_MINUS"
    public final void mRULE_MINUS() throws RecognitionException {
        try {
            int _type = RULE_MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1832:12: ( '-' )
            // InternalSTL.g:1832:14: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MINUS"

    // $ANTLR start "RULE_PLUS"
    public final void mRULE_PLUS() throws RecognitionException {
        try {
            int _type = RULE_PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1834:11: ( '+' )
            // InternalSTL.g:1834:13: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PLUS"

    // $ANTLR start "RULE_TIMES"
    public final void mRULE_TIMES() throws RecognitionException {
        try {
            int _type = RULE_TIMES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1836:12: ( '*' )
            // InternalSTL.g:1836:14: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TIMES"

    // $ANTLR start "RULE_DIVIDE"
    public final void mRULE_DIVIDE() throws RecognitionException {
        try {
            int _type = RULE_DIVIDE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1838:13: ( '/' )
            // InternalSTL.g:1838:15: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIVIDE"

    // $ANTLR start "RULE_LPAREN"
    public final void mRULE_LPAREN() throws RecognitionException {
        try {
            int _type = RULE_LPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1840:13: ( '(' )
            // InternalSTL.g:1840:15: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LPAREN"

    // $ANTLR start "RULE_RPAREN"
    public final void mRULE_RPAREN() throws RecognitionException {
        try {
            int _type = RULE_RPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1842:13: ( ')' )
            // InternalSTL.g:1842:15: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RPAREN"

    // $ANTLR start "RULE_LBRACE"
    public final void mRULE_LBRACE() throws RecognitionException {
        try {
            int _type = RULE_LBRACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1844:13: ( '{' )
            // InternalSTL.g:1844:15: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LBRACE"

    // $ANTLR start "RULE_RBRACE"
    public final void mRULE_RBRACE() throws RecognitionException {
        try {
            int _type = RULE_RBRACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1846:13: ( '}' )
            // InternalSTL.g:1846:15: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RBRACE"

    // $ANTLR start "RULE_LBRACK"
    public final void mRULE_LBRACK() throws RecognitionException {
        try {
            int _type = RULE_LBRACK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1848:13: ( '[' )
            // InternalSTL.g:1848:15: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_LBRACK"

    // $ANTLR start "RULE_RBRACK"
    public final void mRULE_RBRACK() throws RecognitionException {
        try {
            int _type = RULE_RBRACK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1850:13: ( ']' )
            // InternalSTL.g:1850:15: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RBRACK"

    // $ANTLR start "RULE_SEMICOLON"
    public final void mRULE_SEMICOLON() throws RecognitionException {
        try {
            int _type = RULE_SEMICOLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1852:16: ( ';' )
            // InternalSTL.g:1852:18: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEMICOLON"

    // $ANTLR start "RULE_COLON"
    public final void mRULE_COLON() throws RecognitionException {
        try {
            int _type = RULE_COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1854:12: ( ':' )
            // InternalSTL.g:1854:14: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COLON"

    // $ANTLR start "RULE_COMMA"
    public final void mRULE_COMMA() throws RecognitionException {
        try {
            int _type = RULE_COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1856:12: ( ',' )
            // InternalSTL.g:1856:14: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMMA"

    // $ANTLR start "RULE_DOT"
    public final void mRULE_DOT() throws RecognitionException {
        try {
            int _type = RULE_DOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1858:10: ( '.' )
            // InternalSTL.g:1858:12: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOT"

    // $ANTLR start "RULE_AT"
    public final void mRULE_AT() throws RecognitionException {
        try {
            int _type = RULE_AT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1860:9: ( '@' )
            // InternalSTL.g:1860:11: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_AT"

    // $ANTLR start "RULE_ABS"
    public final void mRULE_ABS() throws RecognitionException {
        try {
            int _type = RULE_ABS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1862:10: ( 'abs' )
            // InternalSTL.g:1862:12: 'abs'
            {
            match("abs"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ABS"

    // $ANTLR start "RULE_SQRT"
    public final void mRULE_SQRT() throws RecognitionException {
        try {
            int _type = RULE_SQRT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1864:11: ( 'sqrt' )
            // InternalSTL.g:1864:13: 'sqrt'
            {
            match("sqrt"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SQRT"

    // $ANTLR start "RULE_EXP"
    public final void mRULE_EXP() throws RecognitionException {
        try {
            int _type = RULE_EXP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1866:10: ( 'exp' )
            // InternalSTL.g:1866:12: 'exp'
            {
            match("exp"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXP"

    // $ANTLR start "RULE_POW"
    public final void mRULE_POW() throws RecognitionException {
        try {
            int _type = RULE_POW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1868:10: ( 'pow' )
            // InternalSTL.g:1868:12: 'pow'
            {
            match("pow"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_POW"

    // $ANTLR start "RULE_SEC"
    public final void mRULE_SEC() throws RecognitionException {
        try {
            // InternalSTL.g:1870:19: ( 's' )
            // InternalSTL.g:1870:21: 's'
            {
            match('s'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEC"

    // $ANTLR start "RULE_MSEC"
    public final void mRULE_MSEC() throws RecognitionException {
        try {
            // InternalSTL.g:1872:20: ( 'ms' )
            // InternalSTL.g:1872:22: 'ms'
            {
            match("ms"); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_MSEC"

    // $ANTLR start "RULE_USEC"
    public final void mRULE_USEC() throws RecognitionException {
        try {
            // InternalSTL.g:1874:20: ( 'us' )
            // InternalSTL.g:1874:22: 'us'
            {
            match("us"); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_USEC"

    // $ANTLR start "RULE_NSEC"
    public final void mRULE_NSEC() throws RecognitionException {
        try {
            // InternalSTL.g:1876:20: ( 'ns' )
            // InternalSTL.g:1876:22: 'ns'
            {
            match("ns"); 


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_NSEC"

    // $ANTLR start "RULE_PSEC"
    public final void mRULE_PSEC() throws RecognitionException {
        try {
            int _type = RULE_PSEC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1878:11: ( 'ps' )
            // InternalSTL.g:1878:13: 'ps'
            {
            match("ps"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PSEC"

    // $ANTLR start "RULE_ROS_TOPIC"
    public final void mRULE_ROS_TOPIC() throws RecognitionException {
        try {
            int _type = RULE_ROS_TOPIC;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1880:16: ( 'topic' )
            // InternalSTL.g:1880:18: 'topic'
            {
            match("topic"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ROS_TOPIC"

    // $ANTLR start "RULE_IMPORT"
    public final void mRULE_IMPORT() throws RecognitionException {
        try {
            int _type = RULE_IMPORT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1882:13: ( 'import' )
            // InternalSTL.g:1882:15: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IMPORT"

    // $ANTLR start "RULE_INPUT"
    public final void mRULE_INPUT() throws RecognitionException {
        try {
            int _type = RULE_INPUT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1884:12: ( 'input' )
            // InternalSTL.g:1884:14: 'input'
            {
            match("input"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INPUT"

    // $ANTLR start "RULE_OUTPUT"
    public final void mRULE_OUTPUT() throws RecognitionException {
        try {
            int _type = RULE_OUTPUT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1886:13: ( 'output' )
            // InternalSTL.g:1886:15: 'output'
            {
            match("output"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OUTPUT"

    // $ANTLR start "RULE_INTERNAL"
    public final void mRULE_INTERNAL() throws RecognitionException {
        try {
            int _type = RULE_INTERNAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1888:15: ( 'internal' )
            // InternalSTL.g:1888:17: 'internal'
            {
            match("internal"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INTERNAL"

    // $ANTLR start "RULE_CONSTANT"
    public final void mRULE_CONSTANT() throws RecognitionException {
        try {
            int _type = RULE_CONSTANT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1890:15: ( 'const' )
            // InternalSTL.g:1890:17: 'const'
            {
            match("const"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CONSTANT"

    // $ANTLR start "RULE_DOMAINTYPEREAL"
    public final void mRULE_DOMAINTYPEREAL() throws RecognitionException {
        try {
            int _type = RULE_DOMAINTYPEREAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1892:21: ( 'real' )
            // InternalSTL.g:1892:23: 'real'
            {
            match("real"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOMAINTYPEREAL"

    // $ANTLR start "RULE_DOMAINTYPEFLOAT"
    public final void mRULE_DOMAINTYPEFLOAT() throws RecognitionException {
        try {
            int _type = RULE_DOMAINTYPEFLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1894:22: ( 'float' )
            // InternalSTL.g:1894:24: 'float'
            {
            match("float"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOMAINTYPEFLOAT"

    // $ANTLR start "RULE_DOMAINTYPELONG"
    public final void mRULE_DOMAINTYPELONG() throws RecognitionException {
        try {
            int _type = RULE_DOMAINTYPELONG;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1896:21: ( 'long' )
            // InternalSTL.g:1896:23: 'long'
            {
            match("long"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOMAINTYPELONG"

    // $ANTLR start "RULE_DOMAINTYPECOMPLEX"
    public final void mRULE_DOMAINTYPECOMPLEX() throws RecognitionException {
        try {
            int _type = RULE_DOMAINTYPECOMPLEX;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1898:24: ( 'complex' )
            // InternalSTL.g:1898:26: 'complex'
            {
            match("complex"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOMAINTYPECOMPLEX"

    // $ANTLR start "RULE_DOMAINTYPEINT"
    public final void mRULE_DOMAINTYPEINT() throws RecognitionException {
        try {
            int _type = RULE_DOMAINTYPEINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1900:20: ( 'int' )
            // InternalSTL.g:1900:22: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOMAINTYPEINT"

    // $ANTLR start "RULE_DOMAINTYPEBOOL"
    public final void mRULE_DOMAINTYPEBOOL() throws RecognitionException {
        try {
            int _type = RULE_DOMAINTYPEBOOL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1902:21: ( 'bool' )
            // InternalSTL.g:1902:23: 'bool'
            {
            match("bool"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOMAINTYPEBOOL"

    // $ANTLR start "RULE_INTEGERLITERAL"
    public final void mRULE_INTEGERLITERAL() throws RecognitionException {
        try {
            int _type = RULE_INTEGERLITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1904:21: ( ( RULE_DECIMALNUMERAL | RULE_HEXNUMERAL | RULE_BINARYNUMERAL ) )
            // InternalSTL.g:1904:23: ( RULE_DECIMALNUMERAL | RULE_HEXNUMERAL | RULE_BINARYNUMERAL )
            {
            // InternalSTL.g:1904:23: ( RULE_DECIMALNUMERAL | RULE_HEXNUMERAL | RULE_BINARYNUMERAL )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='0') ) {
                switch ( input.LA(2) ) {
                case 'X':
                case 'x':
                    {
                    alt2=2;
                    }
                    break;
                case 'B':
                case 'b':
                    {
                    alt2=3;
                    }
                    break;
                default:
                    alt2=1;}

            }
            else if ( ((LA2_0>='1' && LA2_0<='9')) ) {
                alt2=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalSTL.g:1904:24: RULE_DECIMALNUMERAL
                    {
                    mRULE_DECIMALNUMERAL(); 

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1904:44: RULE_HEXNUMERAL
                    {
                    mRULE_HEXNUMERAL(); 

                    }
                    break;
                case 3 :
                    // InternalSTL.g:1904:60: RULE_BINARYNUMERAL
                    {
                    mRULE_BINARYNUMERAL(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INTEGERLITERAL"

    // $ANTLR start "RULE_DECIMALNUMERAL"
    public final void mRULE_DECIMALNUMERAL() throws RecognitionException {
        try {
            // InternalSTL.g:1906:30: ( ( '0' | RULE_NONZERODIGIT ( ( RULE_DIGITS )? | RULE_UNDERSCORES RULE_DIGITS ) ) )
            // InternalSTL.g:1906:32: ( '0' | RULE_NONZERODIGIT ( ( RULE_DIGITS )? | RULE_UNDERSCORES RULE_DIGITS ) )
            {
            // InternalSTL.g:1906:32: ( '0' | RULE_NONZERODIGIT ( ( RULE_DIGITS )? | RULE_UNDERSCORES RULE_DIGITS ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='0') ) {
                alt5=1;
            }
            else if ( ((LA5_0>='1' && LA5_0<='9')) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalSTL.g:1906:33: '0'
                    {
                    match('0'); 

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1906:37: RULE_NONZERODIGIT ( ( RULE_DIGITS )? | RULE_UNDERSCORES RULE_DIGITS )
                    {
                    mRULE_NONZERODIGIT(); 
                    // InternalSTL.g:1906:55: ( ( RULE_DIGITS )? | RULE_UNDERSCORES RULE_DIGITS )
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0=='_') ) {
                        alt4=2;
                    }
                    else {
                        alt4=1;}
                    switch (alt4) {
                        case 1 :
                            // InternalSTL.g:1906:56: ( RULE_DIGITS )?
                            {
                            // InternalSTL.g:1906:56: ( RULE_DIGITS )?
                            int alt3=2;
                            int LA3_0 = input.LA(1);

                            if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                                alt3=1;
                            }
                            switch (alt3) {
                                case 1 :
                                    // InternalSTL.g:1906:56: RULE_DIGITS
                                    {
                                    mRULE_DIGITS(); 

                                    }
                                    break;

                            }


                            }
                            break;
                        case 2 :
                            // InternalSTL.g:1906:69: RULE_UNDERSCORES RULE_DIGITS
                            {
                            mRULE_UNDERSCORES(); 
                            mRULE_DIGITS(); 

                            }
                            break;

                    }


                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DECIMALNUMERAL"

    // $ANTLR start "RULE_DIGITS"
    public final void mRULE_DIGITS() throws RecognitionException {
        try {
            // InternalSTL.g:1908:22: ( RULE_DIGIT ( ( RULE_DIGITSANDUNDERSCORES )? RULE_DIGIT )? )
            // InternalSTL.g:1908:24: RULE_DIGIT ( ( RULE_DIGITSANDUNDERSCORES )? RULE_DIGIT )?
            {
            mRULE_DIGIT(); 
            // InternalSTL.g:1908:35: ( ( RULE_DIGITSANDUNDERSCORES )? RULE_DIGIT )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>='0' && LA7_0<='9')||LA7_0=='_') ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalSTL.g:1908:36: ( RULE_DIGITSANDUNDERSCORES )? RULE_DIGIT
                    {
                    // InternalSTL.g:1908:36: ( RULE_DIGITSANDUNDERSCORES )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                        int LA6_1 = input.LA(2);

                        if ( ((LA6_1>='0' && LA6_1<='9')||LA6_1=='_') ) {
                            alt6=1;
                        }
                    }
                    else if ( (LA6_0=='_') ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // InternalSTL.g:1908:36: RULE_DIGITSANDUNDERSCORES
                            {
                            mRULE_DIGITSANDUNDERSCORES(); 

                            }
                            break;

                    }

                    mRULE_DIGIT(); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGITS"

    // $ANTLR start "RULE_DIGIT"
    public final void mRULE_DIGIT() throws RecognitionException {
        try {
            // InternalSTL.g:1910:21: ( ( '0' | RULE_NONZERODIGIT ) )
            // InternalSTL.g:1910:23: ( '0' | RULE_NONZERODIGIT )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGIT"

    // $ANTLR start "RULE_NONZERODIGIT"
    public final void mRULE_NONZERODIGIT() throws RecognitionException {
        try {
            // InternalSTL.g:1912:28: ( '1' .. '9' )
            // InternalSTL.g:1912:30: '1' .. '9'
            {
            matchRange('1','9'); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_NONZERODIGIT"

    // $ANTLR start "RULE_DIGITSANDUNDERSCORES"
    public final void mRULE_DIGITSANDUNDERSCORES() throws RecognitionException {
        try {
            // InternalSTL.g:1914:36: ( ( RULE_DIGITORUNDERSCORE )+ )
            // InternalSTL.g:1914:38: ( RULE_DIGITORUNDERSCORE )+
            {
            // InternalSTL.g:1914:38: ( RULE_DIGITORUNDERSCORE )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='0' && LA8_0<='9')||LA8_0=='_') ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalSTL.g:1914:38: RULE_DIGITORUNDERSCORE
            	    {
            	    mRULE_DIGITORUNDERSCORE(); 

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGITSANDUNDERSCORES"

    // $ANTLR start "RULE_DIGITORUNDERSCORE"
    public final void mRULE_DIGITORUNDERSCORE() throws RecognitionException {
        try {
            // InternalSTL.g:1916:33: ( ( RULE_DIGIT | '_' ) )
            // InternalSTL.g:1916:35: ( RULE_DIGIT | '_' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||input.LA(1)=='_' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGITORUNDERSCORE"

    // $ANTLR start "RULE_UNDERSCORES"
    public final void mRULE_UNDERSCORES() throws RecognitionException {
        try {
            // InternalSTL.g:1918:27: ( ( '_' )+ )
            // InternalSTL.g:1918:29: ( '_' )+
            {
            // InternalSTL.g:1918:29: ( '_' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='_') ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalSTL.g:1918:29: '_'
            	    {
            	    match('_'); 

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_UNDERSCORES"

    // $ANTLR start "RULE_HEXNUMERAL"
    public final void mRULE_HEXNUMERAL() throws RecognitionException {
        try {
            // InternalSTL.g:1920:26: ( '0' ( 'x' | 'X' ) RULE_HEXDIGITS )
            // InternalSTL.g:1920:28: '0' ( 'x' | 'X' ) RULE_HEXDIGITS
            {
            match('0'); 
            if ( input.LA(1)=='X'||input.LA(1)=='x' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            mRULE_HEXDIGITS(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEXNUMERAL"

    // $ANTLR start "RULE_HEXDIGITS"
    public final void mRULE_HEXDIGITS() throws RecognitionException {
        try {
            // InternalSTL.g:1922:25: ( RULE_HEXDIGIT ( ( RULE_HEXDIGITSANDUNDERSCORES )? RULE_HEXDIGIT )? )
            // InternalSTL.g:1922:27: RULE_HEXDIGIT ( ( RULE_HEXDIGITSANDUNDERSCORES )? RULE_HEXDIGIT )?
            {
            mRULE_HEXDIGIT(); 
            // InternalSTL.g:1922:41: ( ( RULE_HEXDIGITSANDUNDERSCORES )? RULE_HEXDIGIT )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( ((LA11_0>='0' && LA11_0<='9')||(LA11_0>='A' && LA11_0<='F')||LA11_0=='_'||(LA11_0>='a' && LA11_0<='f')) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalSTL.g:1922:42: ( RULE_HEXDIGITSANDUNDERSCORES )? RULE_HEXDIGIT
                    {
                    // InternalSTL.g:1922:42: ( RULE_HEXDIGITSANDUNDERSCORES )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( ((LA10_0>='0' && LA10_0<='9')||(LA10_0>='A' && LA10_0<='F')||(LA10_0>='a' && LA10_0<='f')) ) {
                        int LA10_1 = input.LA(2);

                        if ( ((LA10_1>='0' && LA10_1<='9')||(LA10_1>='A' && LA10_1<='F')||LA10_1=='_'||(LA10_1>='a' && LA10_1<='f')) ) {
                            alt10=1;
                        }
                    }
                    else if ( (LA10_0=='_') ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalSTL.g:1922:42: RULE_HEXDIGITSANDUNDERSCORES
                            {
                            mRULE_HEXDIGITSANDUNDERSCORES(); 

                            }
                            break;

                    }

                    mRULE_HEXDIGIT(); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEXDIGITS"

    // $ANTLR start "RULE_HEXDIGIT"
    public final void mRULE_HEXDIGIT() throws RecognitionException {
        try {
            // InternalSTL.g:1924:24: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // InternalSTL.g:1924:26: ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEXDIGIT"

    // $ANTLR start "RULE_HEXDIGITSANDUNDERSCORES"
    public final void mRULE_HEXDIGITSANDUNDERSCORES() throws RecognitionException {
        try {
            // InternalSTL.g:1926:39: ( ( RULE_HEXDIGITORUNDERSCORE )+ )
            // InternalSTL.g:1926:41: ( RULE_HEXDIGITORUNDERSCORE )+
            {
            // InternalSTL.g:1926:41: ( RULE_HEXDIGITORUNDERSCORE )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='0' && LA12_0<='9')||(LA12_0>='A' && LA12_0<='F')||LA12_0=='_'||(LA12_0>='a' && LA12_0<='f')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalSTL.g:1926:41: RULE_HEXDIGITORUNDERSCORE
            	    {
            	    mRULE_HEXDIGITORUNDERSCORE(); 

            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEXDIGITSANDUNDERSCORES"

    // $ANTLR start "RULE_HEXDIGITORUNDERSCORE"
    public final void mRULE_HEXDIGITORUNDERSCORE() throws RecognitionException {
        try {
            // InternalSTL.g:1928:36: ( ( RULE_HEXDIGIT | '_' ) )
            // InternalSTL.g:1928:38: ( RULE_HEXDIGIT | '_' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEXDIGITORUNDERSCORE"

    // $ANTLR start "RULE_BINARYNUMERAL"
    public final void mRULE_BINARYNUMERAL() throws RecognitionException {
        try {
            // InternalSTL.g:1930:29: ( '0' ( 'b' | 'B' ) RULE_BINARYDIGITS )
            // InternalSTL.g:1930:31: '0' ( 'b' | 'B' ) RULE_BINARYDIGITS
            {
            match('0'); 
            if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            mRULE_BINARYDIGITS(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_BINARYNUMERAL"

    // $ANTLR start "RULE_BINARYDIGITS"
    public final void mRULE_BINARYDIGITS() throws RecognitionException {
        try {
            // InternalSTL.g:1932:28: ( RULE_BINARYDIGIT ( ( RULE_BINARYDIGITSANDUNDERSCORES )? RULE_BINARYDIGIT )? )
            // InternalSTL.g:1932:30: RULE_BINARYDIGIT ( ( RULE_BINARYDIGITSANDUNDERSCORES )? RULE_BINARYDIGIT )?
            {
            mRULE_BINARYDIGIT(); 
            // InternalSTL.g:1932:47: ( ( RULE_BINARYDIGITSANDUNDERSCORES )? RULE_BINARYDIGIT )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( ((LA14_0>='0' && LA14_0<='1')||LA14_0=='_') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalSTL.g:1932:48: ( RULE_BINARYDIGITSANDUNDERSCORES )? RULE_BINARYDIGIT
                    {
                    // InternalSTL.g:1932:48: ( RULE_BINARYDIGITSANDUNDERSCORES )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( ((LA13_0>='0' && LA13_0<='1')) ) {
                        int LA13_1 = input.LA(2);

                        if ( ((LA13_1>='0' && LA13_1<='1')||LA13_1=='_') ) {
                            alt13=1;
                        }
                    }
                    else if ( (LA13_0=='_') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalSTL.g:1932:48: RULE_BINARYDIGITSANDUNDERSCORES
                            {
                            mRULE_BINARYDIGITSANDUNDERSCORES(); 

                            }
                            break;

                    }

                    mRULE_BINARYDIGIT(); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_BINARYDIGITS"

    // $ANTLR start "RULE_BINARYDIGIT"
    public final void mRULE_BINARYDIGIT() throws RecognitionException {
        try {
            // InternalSTL.g:1934:27: ( ( '0' | '1' ) )
            // InternalSTL.g:1934:29: ( '0' | '1' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_BINARYDIGIT"

    // $ANTLR start "RULE_BINARYDIGITSANDUNDERSCORES"
    public final void mRULE_BINARYDIGITSANDUNDERSCORES() throws RecognitionException {
        try {
            // InternalSTL.g:1936:42: ( ( RULE_BINARYDIGITORUNDERSCORE )+ )
            // InternalSTL.g:1936:44: ( RULE_BINARYDIGITORUNDERSCORE )+
            {
            // InternalSTL.g:1936:44: ( RULE_BINARYDIGITORUNDERSCORE )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='0' && LA15_0<='1')||LA15_0=='_') ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalSTL.g:1936:44: RULE_BINARYDIGITORUNDERSCORE
            	    {
            	    mRULE_BINARYDIGITORUNDERSCORE(); 

            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_BINARYDIGITSANDUNDERSCORES"

    // $ANTLR start "RULE_BINARYDIGITORUNDERSCORE"
    public final void mRULE_BINARYDIGITORUNDERSCORE() throws RecognitionException {
        try {
            // InternalSTL.g:1938:39: ( ( RULE_BINARYDIGIT | '_' ) )
            // InternalSTL.g:1938:41: ( RULE_BINARYDIGIT | '_' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='1')||input.LA(1)=='_' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_BINARYDIGITORUNDERSCORE"

    // $ANTLR start "RULE_DECIMALREALLITERAL"
    public final void mRULE_DECIMALREALLITERAL() throws RecognitionException {
        try {
            int _type = RULE_DECIMALREALLITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1940:25: ( ( RULE_DIGITS '.' ( RULE_DIGITS )? ( RULE_EXPONENTPART )? | '.' RULE_DIGITS ( RULE_EXPONENTPART )? | RULE_DIGITS RULE_EXPONENTPART ) )
            // InternalSTL.g:1940:27: ( RULE_DIGITS '.' ( RULE_DIGITS )? ( RULE_EXPONENTPART )? | '.' RULE_DIGITS ( RULE_EXPONENTPART )? | RULE_DIGITS RULE_EXPONENTPART )
            {
            // InternalSTL.g:1940:27: ( RULE_DIGITS '.' ( RULE_DIGITS )? ( RULE_EXPONENTPART )? | '.' RULE_DIGITS ( RULE_EXPONENTPART )? | RULE_DIGITS RULE_EXPONENTPART )
            int alt19=3;
            alt19 = dfa19.predict(input);
            switch (alt19) {
                case 1 :
                    // InternalSTL.g:1940:28: RULE_DIGITS '.' ( RULE_DIGITS )? ( RULE_EXPONENTPART )?
                    {
                    mRULE_DIGITS(); 
                    match('.'); 
                    // InternalSTL.g:1940:44: ( RULE_DIGITS )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( ((LA16_0>='0' && LA16_0<='9')) ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // InternalSTL.g:1940:44: RULE_DIGITS
                            {
                            mRULE_DIGITS(); 

                            }
                            break;

                    }

                    // InternalSTL.g:1940:57: ( RULE_EXPONENTPART )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0=='E'||LA17_0=='e') ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalSTL.g:1940:57: RULE_EXPONENTPART
                            {
                            mRULE_EXPONENTPART(); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1940:76: '.' RULE_DIGITS ( RULE_EXPONENTPART )?
                    {
                    match('.'); 
                    mRULE_DIGITS(); 
                    // InternalSTL.g:1940:92: ( RULE_EXPONENTPART )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0=='E'||LA18_0=='e') ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // InternalSTL.g:1940:92: RULE_EXPONENTPART
                            {
                            mRULE_EXPONENTPART(); 

                            }
                            break;

                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:1940:111: RULE_DIGITS RULE_EXPONENTPART
                    {
                    mRULE_DIGITS(); 
                    mRULE_EXPONENTPART(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DECIMALREALLITERAL"

    // $ANTLR start "RULE_EXPONENTPART"
    public final void mRULE_EXPONENTPART() throws RecognitionException {
        try {
            // InternalSTL.g:1942:28: ( RULE_EXPONENTINDICATOR RULE_SIGNEDINTEGER )
            // InternalSTL.g:1942:30: RULE_EXPONENTINDICATOR RULE_SIGNEDINTEGER
            {
            mRULE_EXPONENTINDICATOR(); 
            mRULE_SIGNEDINTEGER(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXPONENTPART"

    // $ANTLR start "RULE_EXPONENTINDICATOR"
    public final void mRULE_EXPONENTINDICATOR() throws RecognitionException {
        try {
            // InternalSTL.g:1944:33: ( ( 'e' | 'E' ) )
            // InternalSTL.g:1944:35: ( 'e' | 'E' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXPONENTINDICATOR"

    // $ANTLR start "RULE_SIGNEDINTEGER"
    public final void mRULE_SIGNEDINTEGER() throws RecognitionException {
        try {
            // InternalSTL.g:1946:29: ( ( RULE_SIGN )? ( RULE_DIGIT )+ )
            // InternalSTL.g:1946:31: ( RULE_SIGN )? ( RULE_DIGIT )+
            {
            // InternalSTL.g:1946:31: ( RULE_SIGN )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0=='+'||LA20_0=='-') ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalSTL.g:1946:31: RULE_SIGN
                    {
                    mRULE_SIGN(); 

                    }
                    break;

            }

            // InternalSTL.g:1946:42: ( RULE_DIGIT )+
            int cnt21=0;
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>='0' && LA21_0<='9')) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalSTL.g:1946:42: RULE_DIGIT
            	    {
            	    mRULE_DIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt21 >= 1 ) break loop21;
                        EarlyExitException eee =
                            new EarlyExitException(21, input);
                        throw eee;
                }
                cnt21++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_SIGNEDINTEGER"

    // $ANTLR start "RULE_SIGN"
    public final void mRULE_SIGN() throws RecognitionException {
        try {
            // InternalSTL.g:1948:20: ( ( '+' | '-' ) )
            // InternalSTL.g:1948:22: ( '+' | '-' )
            {
            if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_SIGN"

    // $ANTLR start "RULE_IDENTIFIER"
    public final void mRULE_IDENTIFIER() throws RecognitionException {
        try {
            int _type = RULE_IDENTIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1950:17: ( RULE_IDENTIFIERSTART ( RULE_IDENTIFIERPART )* )
            // InternalSTL.g:1950:19: RULE_IDENTIFIERSTART ( RULE_IDENTIFIERPART )*
            {
            mRULE_IDENTIFIERSTART(); 
            // InternalSTL.g:1950:40: ( RULE_IDENTIFIERPART )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0=='$'||(LA22_0>='.' && LA22_0<='9')||(LA22_0>='A' && LA22_0<='Z')||LA22_0=='_'||(LA22_0>='a' && LA22_0<='z')) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalSTL.g:1950:40: RULE_IDENTIFIERPART
            	    {
            	    mRULE_IDENTIFIERPART(); 

            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDENTIFIER"

    // $ANTLR start "RULE_IDENTIFIERSTART"
    public final void mRULE_IDENTIFIERSTART() throws RecognitionException {
        try {
            // InternalSTL.g:1952:31: ( ( RULE_LETTERORUNDERSCORE | '$' ) )
            // InternalSTL.g:1952:33: ( RULE_LETTERORUNDERSCORE | '$' )
            {
            if ( input.LA(1)=='$'||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDENTIFIERSTART"

    // $ANTLR start "RULE_IDENTIFIERPART"
    public final void mRULE_IDENTIFIERPART() throws RecognitionException {
        try {
            // InternalSTL.g:1954:30: ( ( RULE_IDENTIFIERSTART | RULE_DIGIT | '.' | '/' ) )
            // InternalSTL.g:1954:32: ( RULE_IDENTIFIERSTART | RULE_DIGIT | '.' | '/' )
            {
            if ( input.LA(1)=='$'||(input.LA(1)>='.' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDENTIFIERPART"

    // $ANTLR start "RULE_LETTERORUNDERSCORE"
    public final void mRULE_LETTERORUNDERSCORE() throws RecognitionException {
        try {
            // InternalSTL.g:1956:34: ( ( RULE_LETTER | '_' ) )
            // InternalSTL.g:1956:36: ( RULE_LETTER | '_' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_LETTERORUNDERSCORE"

    // $ANTLR start "RULE_LETTER"
    public final void mRULE_LETTER() throws RecognitionException {
        try {
            // InternalSTL.g:1958:22: ( ( 'A' .. 'Z' | 'a' .. 'z' ) )
            // InternalSTL.g:1958:24: ( 'A' .. 'Z' | 'a' .. 'z' )
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_LETTER"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1960:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalSTL.g:1960:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalSTL.g:1960:11: ( '^' )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0=='^') ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalSTL.g:1960:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalSTL.g:1960:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>='0' && LA24_0<='9')||(LA24_0>='A' && LA24_0<='Z')||LA24_0=='_'||(LA24_0>='a' && LA24_0<='z')) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalSTL.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1962:10: ( ( '0' .. '9' )+ )
            // InternalSTL.g:1962:12: ( '0' .. '9' )+
            {
            // InternalSTL.g:1962:12: ( '0' .. '9' )+
            int cnt25=0;
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>='0' && LA25_0<='9')) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalSTL.g:1962:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt25 >= 1 ) break loop25;
                        EarlyExitException eee =
                            new EarlyExitException(25, input);
                        throw eee;
                }
                cnt25++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1964:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalSTL.g:1964:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalSTL.g:1964:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0=='\"') ) {
                alt28=1;
            }
            else if ( (LA28_0=='\'') ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalSTL.g:1964:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalSTL.g:1964:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop26:
                    do {
                        int alt26=3;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0=='\\') ) {
                            alt26=1;
                        }
                        else if ( ((LA26_0>='\u0000' && LA26_0<='!')||(LA26_0>='#' && LA26_0<='[')||(LA26_0>=']' && LA26_0<='\uFFFF')) ) {
                            alt26=2;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // InternalSTL.g:1964:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalSTL.g:1964:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1964:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalSTL.g:1964:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop27:
                    do {
                        int alt27=3;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0=='\\') ) {
                            alt27=1;
                        }
                        else if ( ((LA27_0>='\u0000' && LA27_0<='&')||(LA27_0>='(' && LA27_0<='[')||(LA27_0>=']' && LA27_0<='\uFFFF')) ) {
                            alt27=2;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // InternalSTL.g:1964:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalSTL.g:1964:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1966:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalSTL.g:1966:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalSTL.g:1966:24: ( options {greedy=false; } : . )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0=='*') ) {
                    int LA29_1 = input.LA(2);

                    if ( (LA29_1=='/') ) {
                        alt29=2;
                    }
                    else if ( ((LA29_1>='\u0000' && LA29_1<='.')||(LA29_1>='0' && LA29_1<='\uFFFF')) ) {
                        alt29=1;
                    }


                }
                else if ( ((LA29_0>='\u0000' && LA29_0<=')')||(LA29_0>='+' && LA29_0<='\uFFFF')) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalSTL.g:1966:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1968:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalSTL.g:1968:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalSTL.g:1968:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( ((LA30_0>='\u0000' && LA30_0<='\t')||(LA30_0>='\u000B' && LA30_0<='\f')||(LA30_0>='\u000E' && LA30_0<='\uFFFF')) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalSTL.g:1968:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            // InternalSTL.g:1968:40: ( ( '\\r' )? '\\n' )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0=='\n'||LA32_0=='\r') ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalSTL.g:1968:41: ( '\\r' )? '\\n'
                    {
                    // InternalSTL.g:1968:41: ( '\\r' )?
                    int alt31=2;
                    int LA31_0 = input.LA(1);

                    if ( (LA31_0=='\r') ) {
                        alt31=1;
                    }
                    switch (alt31) {
                        case 1 :
                            // InternalSTL.g:1968:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1970:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalSTL.g:1970:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalSTL.g:1970:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt33=0;
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( ((LA33_0>='\t' && LA33_0<='\n')||LA33_0=='\r'||LA33_0==' ') ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalSTL.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt33 >= 1 ) break loop33;
                        EarlyExitException eee =
                            new EarlyExitException(33, input);
                        throw eee;
                }
                cnt33++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalSTL.g:1972:16: ( . )
            // InternalSTL.g:1972:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalSTL.g:1:8: ( T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | RULE_UNIT | RULE_MINUS | RULE_PLUS | RULE_TIMES | RULE_DIVIDE | RULE_LPAREN | RULE_RPAREN | RULE_LBRACE | RULE_RBRACE | RULE_LBRACK | RULE_RBRACK | RULE_SEMICOLON | RULE_COLON | RULE_COMMA | RULE_DOT | RULE_AT | RULE_ABS | RULE_SQRT | RULE_EXP | RULE_POW | RULE_PSEC | RULE_ROS_TOPIC | RULE_IMPORT | RULE_INPUT | RULE_OUTPUT | RULE_INTERNAL | RULE_CONSTANT | RULE_DOMAINTYPEREAL | RULE_DOMAINTYPEFLOAT | RULE_DOMAINTYPELONG | RULE_DOMAINTYPECOMPLEX | RULE_DOMAINTYPEINT | RULE_DOMAINTYPEBOOL | RULE_INTEGERLITERAL | RULE_DECIMALREALLITERAL | RULE_IDENTIFIER | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt34=78;
        alt34 = dfa34.predict(input);
        switch (alt34) {
            case 1 :
                // InternalSTL.g:1:10: T__76
                {
                mT__76(); 

                }
                break;
            case 2 :
                // InternalSTL.g:1:16: T__77
                {
                mT__77(); 

                }
                break;
            case 3 :
                // InternalSTL.g:1:22: T__78
                {
                mT__78(); 

                }
                break;
            case 4 :
                // InternalSTL.g:1:28: T__79
                {
                mT__79(); 

                }
                break;
            case 5 :
                // InternalSTL.g:1:34: T__80
                {
                mT__80(); 

                }
                break;
            case 6 :
                // InternalSTL.g:1:40: T__81
                {
                mT__81(); 

                }
                break;
            case 7 :
                // InternalSTL.g:1:46: T__82
                {
                mT__82(); 

                }
                break;
            case 8 :
                // InternalSTL.g:1:52: T__83
                {
                mT__83(); 

                }
                break;
            case 9 :
                // InternalSTL.g:1:58: T__84
                {
                mT__84(); 

                }
                break;
            case 10 :
                // InternalSTL.g:1:64: T__85
                {
                mT__85(); 

                }
                break;
            case 11 :
                // InternalSTL.g:1:70: T__86
                {
                mT__86(); 

                }
                break;
            case 12 :
                // InternalSTL.g:1:76: T__87
                {
                mT__87(); 

                }
                break;
            case 13 :
                // InternalSTL.g:1:82: T__88
                {
                mT__88(); 

                }
                break;
            case 14 :
                // InternalSTL.g:1:88: T__89
                {
                mT__89(); 

                }
                break;
            case 15 :
                // InternalSTL.g:1:94: T__90
                {
                mT__90(); 

                }
                break;
            case 16 :
                // InternalSTL.g:1:100: T__91
                {
                mT__91(); 

                }
                break;
            case 17 :
                // InternalSTL.g:1:106: T__92
                {
                mT__92(); 

                }
                break;
            case 18 :
                // InternalSTL.g:1:112: T__93
                {
                mT__93(); 

                }
                break;
            case 19 :
                // InternalSTL.g:1:118: T__94
                {
                mT__94(); 

                }
                break;
            case 20 :
                // InternalSTL.g:1:124: T__95
                {
                mT__95(); 

                }
                break;
            case 21 :
                // InternalSTL.g:1:130: T__96
                {
                mT__96(); 

                }
                break;
            case 22 :
                // InternalSTL.g:1:136: T__97
                {
                mT__97(); 

                }
                break;
            case 23 :
                // InternalSTL.g:1:142: T__98
                {
                mT__98(); 

                }
                break;
            case 24 :
                // InternalSTL.g:1:148: T__99
                {
                mT__99(); 

                }
                break;
            case 25 :
                // InternalSTL.g:1:154: T__100
                {
                mT__100(); 

                }
                break;
            case 26 :
                // InternalSTL.g:1:161: T__101
                {
                mT__101(); 

                }
                break;
            case 27 :
                // InternalSTL.g:1:168: T__102
                {
                mT__102(); 

                }
                break;
            case 28 :
                // InternalSTL.g:1:175: T__103
                {
                mT__103(); 

                }
                break;
            case 29 :
                // InternalSTL.g:1:182: T__104
                {
                mT__104(); 

                }
                break;
            case 30 :
                // InternalSTL.g:1:189: T__105
                {
                mT__105(); 

                }
                break;
            case 31 :
                // InternalSTL.g:1:196: T__106
                {
                mT__106(); 

                }
                break;
            case 32 :
                // InternalSTL.g:1:203: T__107
                {
                mT__107(); 

                }
                break;
            case 33 :
                // InternalSTL.g:1:210: T__108
                {
                mT__108(); 

                }
                break;
            case 34 :
                // InternalSTL.g:1:217: T__109
                {
                mT__109(); 

                }
                break;
            case 35 :
                // InternalSTL.g:1:224: T__110
                {
                mT__110(); 

                }
                break;
            case 36 :
                // InternalSTL.g:1:231: RULE_UNIT
                {
                mRULE_UNIT(); 

                }
                break;
            case 37 :
                // InternalSTL.g:1:241: RULE_MINUS
                {
                mRULE_MINUS(); 

                }
                break;
            case 38 :
                // InternalSTL.g:1:252: RULE_PLUS
                {
                mRULE_PLUS(); 

                }
                break;
            case 39 :
                // InternalSTL.g:1:262: RULE_TIMES
                {
                mRULE_TIMES(); 

                }
                break;
            case 40 :
                // InternalSTL.g:1:273: RULE_DIVIDE
                {
                mRULE_DIVIDE(); 

                }
                break;
            case 41 :
                // InternalSTL.g:1:285: RULE_LPAREN
                {
                mRULE_LPAREN(); 

                }
                break;
            case 42 :
                // InternalSTL.g:1:297: RULE_RPAREN
                {
                mRULE_RPAREN(); 

                }
                break;
            case 43 :
                // InternalSTL.g:1:309: RULE_LBRACE
                {
                mRULE_LBRACE(); 

                }
                break;
            case 44 :
                // InternalSTL.g:1:321: RULE_RBRACE
                {
                mRULE_RBRACE(); 

                }
                break;
            case 45 :
                // InternalSTL.g:1:333: RULE_LBRACK
                {
                mRULE_LBRACK(); 

                }
                break;
            case 46 :
                // InternalSTL.g:1:345: RULE_RBRACK
                {
                mRULE_RBRACK(); 

                }
                break;
            case 47 :
                // InternalSTL.g:1:357: RULE_SEMICOLON
                {
                mRULE_SEMICOLON(); 

                }
                break;
            case 48 :
                // InternalSTL.g:1:372: RULE_COLON
                {
                mRULE_COLON(); 

                }
                break;
            case 49 :
                // InternalSTL.g:1:383: RULE_COMMA
                {
                mRULE_COMMA(); 

                }
                break;
            case 50 :
                // InternalSTL.g:1:394: RULE_DOT
                {
                mRULE_DOT(); 

                }
                break;
            case 51 :
                // InternalSTL.g:1:403: RULE_AT
                {
                mRULE_AT(); 

                }
                break;
            case 52 :
                // InternalSTL.g:1:411: RULE_ABS
                {
                mRULE_ABS(); 

                }
                break;
            case 53 :
                // InternalSTL.g:1:420: RULE_SQRT
                {
                mRULE_SQRT(); 

                }
                break;
            case 54 :
                // InternalSTL.g:1:430: RULE_EXP
                {
                mRULE_EXP(); 

                }
                break;
            case 55 :
                // InternalSTL.g:1:439: RULE_POW
                {
                mRULE_POW(); 

                }
                break;
            case 56 :
                // InternalSTL.g:1:448: RULE_PSEC
                {
                mRULE_PSEC(); 

                }
                break;
            case 57 :
                // InternalSTL.g:1:458: RULE_ROS_TOPIC
                {
                mRULE_ROS_TOPIC(); 

                }
                break;
            case 58 :
                // InternalSTL.g:1:473: RULE_IMPORT
                {
                mRULE_IMPORT(); 

                }
                break;
            case 59 :
                // InternalSTL.g:1:485: RULE_INPUT
                {
                mRULE_INPUT(); 

                }
                break;
            case 60 :
                // InternalSTL.g:1:496: RULE_OUTPUT
                {
                mRULE_OUTPUT(); 

                }
                break;
            case 61 :
                // InternalSTL.g:1:508: RULE_INTERNAL
                {
                mRULE_INTERNAL(); 

                }
                break;
            case 62 :
                // InternalSTL.g:1:522: RULE_CONSTANT
                {
                mRULE_CONSTANT(); 

                }
                break;
            case 63 :
                // InternalSTL.g:1:536: RULE_DOMAINTYPEREAL
                {
                mRULE_DOMAINTYPEREAL(); 

                }
                break;
            case 64 :
                // InternalSTL.g:1:556: RULE_DOMAINTYPEFLOAT
                {
                mRULE_DOMAINTYPEFLOAT(); 

                }
                break;
            case 65 :
                // InternalSTL.g:1:577: RULE_DOMAINTYPELONG
                {
                mRULE_DOMAINTYPELONG(); 

                }
                break;
            case 66 :
                // InternalSTL.g:1:597: RULE_DOMAINTYPECOMPLEX
                {
                mRULE_DOMAINTYPECOMPLEX(); 

                }
                break;
            case 67 :
                // InternalSTL.g:1:620: RULE_DOMAINTYPEINT
                {
                mRULE_DOMAINTYPEINT(); 

                }
                break;
            case 68 :
                // InternalSTL.g:1:639: RULE_DOMAINTYPEBOOL
                {
                mRULE_DOMAINTYPEBOOL(); 

                }
                break;
            case 69 :
                // InternalSTL.g:1:659: RULE_INTEGERLITERAL
                {
                mRULE_INTEGERLITERAL(); 

                }
                break;
            case 70 :
                // InternalSTL.g:1:679: RULE_DECIMALREALLITERAL
                {
                mRULE_DECIMALREALLITERAL(); 

                }
                break;
            case 71 :
                // InternalSTL.g:1:703: RULE_IDENTIFIER
                {
                mRULE_IDENTIFIER(); 

                }
                break;
            case 72 :
                // InternalSTL.g:1:719: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 73 :
                // InternalSTL.g:1:727: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 74 :
                // InternalSTL.g:1:736: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 75 :
                // InternalSTL.g:1:748: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 76 :
                // InternalSTL.g:1:764: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 77 :
                // InternalSTL.g:1:780: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 78 :
                // InternalSTL.g:1:788: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA19 dfa19 = new DFA19(this);
    protected DFA34 dfa34 = new DFA34(this);
    static final String DFA19_eotS =
        "\7\uffff";
    static final String DFA19_eofS =
        "\7\uffff";
    static final String DFA19_minS =
        "\2\56\1\uffff\1\56\1\60\2\uffff";
    static final String DFA19_maxS =
        "\1\71\1\145\1\uffff\1\145\1\137\2\uffff";
    static final String DFA19_acceptS =
        "\2\uffff\1\2\2\uffff\1\1\1\3";
    static final String DFA19_specialS =
        "\7\uffff}>";
    static final String[] DFA19_transitionS = {
            "\1\2\1\uffff\12\1",
            "\1\5\1\uffff\12\3\13\uffff\1\6\31\uffff\1\4\5\uffff\1\6",
            "",
            "\1\5\1\uffff\12\3\13\uffff\1\6\31\uffff\1\4\5\uffff\1\6",
            "\12\3\45\uffff\1\4",
            "",
            ""
    };

    static final short[] DFA19_eot = DFA.unpackEncodedString(DFA19_eotS);
    static final short[] DFA19_eof = DFA.unpackEncodedString(DFA19_eofS);
    static final char[] DFA19_min = DFA.unpackEncodedStringToUnsignedChars(DFA19_minS);
    static final char[] DFA19_max = DFA.unpackEncodedStringToUnsignedChars(DFA19_maxS);
    static final short[] DFA19_accept = DFA.unpackEncodedString(DFA19_acceptS);
    static final short[] DFA19_special = DFA.unpackEncodedString(DFA19_specialS);
    static final short[][] DFA19_transition;

    static {
        int numStates = DFA19_transitionS.length;
        DFA19_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA19_transition[i] = DFA.unpackEncodedString(DFA19_transitionS[i]);
        }
    }

    class DFA19 extends DFA {

        public DFA19(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 19;
            this.eot = DFA19_eot;
            this.eof = DFA19_eof;
            this.min = DFA19_min;
            this.max = DFA19_max;
            this.accept = DFA19_accept;
            this.special = DFA19_special;
            this.transition = DFA19_transition;
        }
        public String getDescription() {
            return "1940:27: ( RULE_DIGITS '.' ( RULE_DIGITS )? ( RULE_EXPONENTPART )? | '.' RULE_DIGITS ( RULE_EXPONENTPART )? | RULE_DIGITS RULE_EXPONENTPART )";
        }
    }
    static final String DFA34_eotS =
        "\1\uffff\1\73\1\uffff\1\73\1\uffff\1\73\1\106\1\110\3\73\1\116\1\73\1\122\1\73\1\124\1\125\1\73\1\130\1\73\1\134\1\136\1\66\1\141\2\73\1\146\2\73\2\uffff\1\155\11\uffff\1\170\1\uffff\3\73\2\175\1\73\1\66\1\uffff\2\66\2\uffff\1\u0084\3\73\2\uffff\3\73\1\uffff\3\73\5\uffff\5\73\1\uffff\3\73\1\uffff\1\73\2\uffff\1\73\1\146\1\uffff\2\73\1\u009a\3\uffff\1\u009c\2\uffff\4\73\1\uffff\2\146\21\uffff\3\73\1\uffff\1\u00a5\1\175\5\uffff\2\73\1\u00ab\1\73\1\u00ad\1\u00ae\2\73\1\u00b3\1\u00b4\5\73\1\u00bb\4\73\1\u00c0\3\uffff\10\73\1\uffff\1\175\1\uffff\1\175\1\u00cc\1\73\1\uffff\1\73\2\uffff\4\73\2\uffff\1\u00d3\1\u00d4\1\u00d5\3\73\1\uffff\2\73\1\u00db\1\u00dc\1\uffff\1\u00dd\1\73\1\u00df\1\u00e0\2\73\1\u00e3\1\u00e4\2\175\2\uffff\4\73\1\u00e9\1\73\3\uffff\1\u00eb\1\u00ec\1\73\1\u00ee\1\73\3\uffff\1\u00f0\2\uffff\1\u00f1\1\73\2\uffff\1\u00f3\1\u00f4\1\73\1\u00f6\1\uffff\1\73\2\uffff\1\73\1\uffff\1\73\2\uffff\1\73\2\uffff\1\u00fb\1\uffff\3\73\1\u00ff\1\uffff\1\u0100\2\73\2\uffff\2\73\1\u0105\1\73\1\uffff\1\73\1\u0108\1\uffff";
    static final String DFA34_eofS =
        "\u0109\uffff";
    static final String DFA34_minS =
        "\1\0\1\60\1\uffff\1\60\1\uffff\1\60\1\55\1\76\3\60\1\44\1\60\1\44\1\60\2\44\1\60\1\44\1\60\1\44\3\75\2\60\1\44\2\60\2\uffff\1\52\11\uffff\1\60\1\uffff\3\60\2\56\1\60\1\101\1\uffff\2\0\2\uffff\1\44\3\60\2\uffff\3\60\1\uffff\3\60\5\uffff\5\60\1\uffff\3\60\1\uffff\1\60\2\uffff\1\60\1\44\1\uffff\2\60\1\44\3\uffff\1\75\2\uffff\4\60\1\uffff\2\44\21\uffff\3\60\1\uffff\2\56\1\60\4\uffff\2\60\1\44\1\60\2\44\2\60\2\44\5\60\1\44\4\60\1\44\3\uffff\10\60\1\uffff\1\56\1\60\1\56\1\44\1\60\1\uffff\1\60\2\uffff\4\60\2\uffff\3\44\3\60\1\uffff\2\60\2\44\1\uffff\1\44\1\60\2\44\2\60\2\44\2\56\1\60\1\uffff\4\60\1\44\1\60\3\uffff\2\44\1\60\1\44\1\60\3\uffff\1\44\2\uffff\1\44\1\60\2\uffff\2\44\1\60\1\44\1\uffff\1\60\2\uffff\1\60\1\uffff\1\60\2\uffff\1\60\2\uffff\1\44\1\uffff\3\60\1\44\1\uffff\1\44\2\60\2\uffff\2\60\1\44\1\60\1\uffff\1\60\1\44\1\uffff";
    static final String DFA34_maxS =
        "\1\uffff\1\172\1\uffff\1\172\1\uffff\1\172\1\75\1\76\15\172\3\75\5\172\2\uffff\1\57\11\uffff\1\71\1\uffff\3\172\2\145\2\172\1\uffff\2\uffff\2\uffff\4\172\2\uffff\3\172\1\uffff\3\172\5\uffff\5\172\1\uffff\3\172\1\uffff\1\172\2\uffff\2\172\1\uffff\3\172\3\uffff\1\75\2\uffff\4\172\1\uffff\2\172\21\uffff\3\172\1\uffff\2\145\1\137\4\uffff\25\172\3\uffff\10\172\1\uffff\1\145\1\137\1\145\2\172\1\uffff\1\172\2\uffff\4\172\2\uffff\6\172\1\uffff\4\172\1\uffff\10\172\2\145\1\137\1\uffff\6\172\3\uffff\5\172\3\uffff\1\172\2\uffff\2\172\2\uffff\4\172\1\uffff\1\172\2\uffff\1\172\1\uffff\1\172\2\uffff\1\172\2\uffff\1\172\1\uffff\4\172\1\uffff\3\172\2\uffff\4\172\1\uffff\2\172\1\uffff";
    static final String DFA34_acceptS =
        "\2\uffff\1\2\1\uffff\1\4\30\uffff\1\46\1\47\1\uffff\1\51\1\52\1\53\1\54\1\55\1\56\1\57\1\60\1\61\1\uffff\1\63\7\uffff\1\107\2\uffff\1\115\1\116\4\uffff\1\107\1\2\3\uffff\1\4\3\uffff\1\6\1\35\1\37\1\10\1\45\5\uffff\1\15\3\uffff\1\17\1\uffff\1\21\1\23\2\uffff\1\25\3\uffff\1\27\1\30\1\31\1\uffff\1\34\1\36\4\uffff\1\44\2\uffff\1\46\1\47\1\113\1\114\1\50\1\51\1\52\1\53\1\54\1\55\1\56\1\57\1\60\1\61\1\106\1\62\1\63\3\uffff\1\105\3\uffff\1\110\1\112\1\115\1\1\25\uffff\1\70\1\32\1\33\10\uffff\1\111\5\uffff\1\3\1\uffff\1\64\1\5\4\uffff\1\103\1\11\6\uffff\1\66\4\uffff\1\67\13\uffff\1\22\6\uffff\1\12\1\77\1\13\5\uffff\1\24\1\26\1\40\1\uffff\1\41\1\65\2\uffff\1\101\1\104\4\uffff\1\73\1\uffff\1\42\1\100\1\uffff\1\43\1\uffff\1\71\1\76\1\uffff\1\74\1\14\1\uffff\1\72\4\uffff\1\7\3\uffff\1\102\1\75\4\uffff\1\16\2\uffff\1\20";
    static final String DFA34_specialS =
        "\1\1\62\uffff\1\2\1\0\u00d4\uffff}>";
    static final String[] DFA34_transitionS = {
            "\11\66\2\65\2\66\1\65\22\66\1\65\1\26\1\63\1\66\1\62\1\66\1\4\1\64\1\40\1\41\1\36\1\35\1\50\1\7\1\51\1\37\1\56\11\57\1\47\1\46\1\6\1\25\1\27\1\66\1\52\5\60\1\15\1\13\1\17\6\60\1\20\4\60\1\31\3\60\1\22\1\24\1\60\1\44\1\66\1\45\1\61\1\60\1\66\1\3\1\55\1\53\1\60\1\14\1\12\1\60\1\16\1\5\2\60\1\54\1\33\1\21\1\1\1\23\1\60\1\11\1\32\1\30\1\34\2\60\1\10\2\60\1\42\1\2\1\43\uff82\66",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\15\72\1\70\3\72\1\67\2\72\1\71\5\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\72\1\77\11\72\1\76\1\72\1\75\14\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\5\72\1\101\6\72\1\102\1\103\14\72",
            "\1\104\17\uffff\1\105",
            "\1\107",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\111\13\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\113\3\72\1\112\21\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\114\12\72\1\115\16\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\25\72\1\117\1\72\1\120\2\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\1\121\31\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\10\72\1\123\21\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\126\15\72\1\127\7\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\132\2\72\1\131\1\133\7\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\135",
            "\1\137",
            "\1\140",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\143\2\72\1\142\10\72",
            "\12\72\7\uffff\21\72\1\144\10\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\20\72\1\145\11\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\147\7\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\150\7\72",
            "",
            "",
            "\1\153\4\uffff\1\154",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\167",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\172\13\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\173\13\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\174\13\72",
            "\1\167\1\uffff\12\176\13\uffff\1\167\31\uffff\1\167\5\uffff\1\167",
            "\1\167\1\uffff\12\177\13\uffff\1\167\31\uffff\1\u0080\5\uffff\1\167",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\32\u0081\4\uffff\1\u0081\1\uffff\32\u0081",
            "",
            "\0\u0082",
            "\0\u0082",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\2\72\1\u0085\27\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u0086\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\3\72\1\u0087\26\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\26\72\1\u0088\3\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\u0089\7\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\5\72\1\u008a\24\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\17\72\1\u008b\12\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\17\72\1\u008c\3\72\1\u008d\6\72",
            "",
            "",
            "",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\21\72\1\u008e\10\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\u008f\7\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\u0090\31\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u0091\16\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\u0092\13\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u0093\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\17\72\1\u0094\12\72",
            "\12\72\7\uffff\13\72\1\u0095\16\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\u0096\7\72",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\27\72\1\u0097\2\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u0098\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\26\72\1\u0099\3\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "",
            "",
            "\1\u009b",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\24\72\1\u009d\5\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\17\72\1\u009e\12\72",
            "\12\72\7\uffff\24\72\1\u009f\5\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\21\72\1\u00a0\10\72",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\14\72\1\u00a2\1\u00a1\14\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\15\72\1\u00a3\14\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\u00a4\13\72",
            "",
            "\1\167\1\uffff\12\176\13\uffff\1\167\31\uffff\1\167\5\uffff\1\167",
            "\1\167\1\uffff\12\u00a6\13\uffff\1\167\31\uffff\1\u00a7\5\uffff\1\167",
            "\12\u00a8\45\uffff\1\u0080",
            "",
            "",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00a9\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\17\72\1\u00aa\12\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\u00ac\31\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00af\2\72\1\u00b0\13\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\24\72\1\u00b1\5\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00b2\25\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00b5\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00b6\16\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00b7\6\72\1\u00b8\7\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\u00b9\31\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\15\72\1\u00ba\14\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\22\72\1\u00bc\7\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00bd\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00be\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\25\72\1\u00bf\4\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00c1\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\10\72\1\u00c2\21\72",
            "\12\72\7\uffff\4\72\1\u00c3\25\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00c4\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\u00c5\7\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\17\72\1\u00c6\12\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\6\72\1\u00c7\23\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00c8\16\72",
            "",
            "\1\167\1\uffff\12\u00a6\13\uffff\1\167\31\uffff\1\u00a7\5\uffff\1\167",
            "\12\u00c9\45\uffff\1\u00a7",
            "\1\167\1\uffff\12\u00ca\13\uffff\1\167\31\uffff\1\u00cb\5\uffff\1\167",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\24\72\1\u00cd\5\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\30\72\1\u00ce\1\72",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\10\72\1\u00cf\21\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\21\72\1\u00d0\10\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00d1\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\21\72\1\u00d2\10\72",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00d6\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00d7\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00d8\6\72",
            "",
            "\12\72\7\uffff\4\72\1\u00d9\25\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\16\72\1\u00da\13\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\2\72\1\u00de\27\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00e1\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00e2\16\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\167\1\uffff\12\u00c9\13\uffff\1\167\31\uffff\1\u00a7\5\uffff\1\167",
            "\1\167\1\uffff\12\u00ca\13\uffff\1\167\31\uffff\1\u00cb\5\uffff\1\167",
            "\12\u00ca\45\uffff\1\u00cb",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00e5\6\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\u00e6\7\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00e7\25\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\23\72\1\u00e8\6\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\15\72\1\u00ea\14\72",
            "",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\24\72\1\u00ed\5\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\21\72\1\u00ef\10\72",
            "",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\4\72\1\u00f2\25\72",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\22\72\1\u00f5\7\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\u00f7\31\72",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\u00f8\31\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\10\72\1\u00f9\21\72",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\27\72\1\u00fa\2\72",
            "",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00fc\16\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u00fd\16\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\2\72\1\u00fe\27\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u0101\16\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\1\u0102\31\72",
            "",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\30\72\1\u0103\1\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u0104\16\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\13\72\1\u0106\16\72",
            "",
            "\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\30\72\1\u0107\1\72",
            "\1\73\11\uffff\2\73\12\72\7\uffff\32\72\4\uffff\1\72\1\uffff\32\72",
            ""
    };

    static final short[] DFA34_eot = DFA.unpackEncodedString(DFA34_eotS);
    static final short[] DFA34_eof = DFA.unpackEncodedString(DFA34_eofS);
    static final char[] DFA34_min = DFA.unpackEncodedStringToUnsignedChars(DFA34_minS);
    static final char[] DFA34_max = DFA.unpackEncodedStringToUnsignedChars(DFA34_maxS);
    static final short[] DFA34_accept = DFA.unpackEncodedString(DFA34_acceptS);
    static final short[] DFA34_special = DFA.unpackEncodedString(DFA34_specialS);
    static final short[][] DFA34_transition;

    static {
        int numStates = DFA34_transitionS.length;
        DFA34_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA34_transition[i] = DFA.unpackEncodedString(DFA34_transitionS[i]);
        }
    }

    class DFA34 extends DFA {

        public DFA34(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 34;
            this.eot = DFA34_eot;
            this.eof = DFA34_eof;
            this.min = DFA34_min;
            this.max = DFA34_max;
            this.accept = DFA34_accept;
            this.special = DFA34_special;
            this.transition = DFA34_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | RULE_UNIT | RULE_MINUS | RULE_PLUS | RULE_TIMES | RULE_DIVIDE | RULE_LPAREN | RULE_RPAREN | RULE_LBRACE | RULE_RBRACE | RULE_LBRACK | RULE_RBRACK | RULE_SEMICOLON | RULE_COLON | RULE_COMMA | RULE_DOT | RULE_AT | RULE_ABS | RULE_SQRT | RULE_EXP | RULE_POW | RULE_PSEC | RULE_ROS_TOPIC | RULE_IMPORT | RULE_INPUT | RULE_OUTPUT | RULE_INTERNAL | RULE_CONSTANT | RULE_DOMAINTYPEREAL | RULE_DOMAINTYPEFLOAT | RULE_DOMAINTYPELONG | RULE_DOMAINTYPECOMPLEX | RULE_DOMAINTYPEINT | RULE_DOMAINTYPEBOOL | RULE_INTEGERLITERAL | RULE_DECIMALREALLITERAL | RULE_IDENTIFIER | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA34_52 = input.LA(1);

                        s = -1;
                        if ( ((LA34_52>='\u0000' && LA34_52<='\uFFFF')) ) {s = 130;}

                        else s = 54;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA34_0 = input.LA(1);

                        s = -1;
                        if ( (LA34_0=='o') ) {s = 1;}

                        else if ( (LA34_0=='|') ) {s = 2;}

                        else if ( (LA34_0=='a') ) {s = 3;}

                        else if ( (LA34_0=='&') ) {s = 4;}

                        else if ( (LA34_0=='i') ) {s = 5;}

                        else if ( (LA34_0=='<') ) {s = 6;}

                        else if ( (LA34_0=='-') ) {s = 7;}

                        else if ( (LA34_0=='x') ) {s = 8;}

                        else if ( (LA34_0=='r') ) {s = 9;}

                        else if ( (LA34_0=='f') ) {s = 10;}

                        else if ( (LA34_0=='G') ) {s = 11;}

                        else if ( (LA34_0=='e') ) {s = 12;}

                        else if ( (LA34_0=='F') ) {s = 13;}

                        else if ( (LA34_0=='h') ) {s = 14;}

                        else if ( (LA34_0=='H') ) {s = 15;}

                        else if ( (LA34_0=='O') ) {s = 16;}

                        else if ( (LA34_0=='n') ) {s = 17;}

                        else if ( (LA34_0=='X') ) {s = 18;}

                        else if ( (LA34_0=='p') ) {s = 19;}

                        else if ( (LA34_0=='Y') ) {s = 20;}

                        else if ( (LA34_0=='=') ) {s = 21;}

                        else if ( (LA34_0=='!') ) {s = 22;}

                        else if ( (LA34_0=='>') ) {s = 23;}

                        else if ( (LA34_0=='t') ) {s = 24;}

                        else if ( (LA34_0=='T') ) {s = 25;}

                        else if ( (LA34_0=='s') ) {s = 26;}

                        else if ( (LA34_0=='m') ) {s = 27;}

                        else if ( (LA34_0=='u') ) {s = 28;}

                        else if ( (LA34_0=='+') ) {s = 29;}

                        else if ( (LA34_0=='*') ) {s = 30;}

                        else if ( (LA34_0=='/') ) {s = 31;}

                        else if ( (LA34_0=='(') ) {s = 32;}

                        else if ( (LA34_0==')') ) {s = 33;}

                        else if ( (LA34_0=='{') ) {s = 34;}

                        else if ( (LA34_0=='}') ) {s = 35;}

                        else if ( (LA34_0=='[') ) {s = 36;}

                        else if ( (LA34_0==']') ) {s = 37;}

                        else if ( (LA34_0==';') ) {s = 38;}

                        else if ( (LA34_0==':') ) {s = 39;}

                        else if ( (LA34_0==',') ) {s = 40;}

                        else if ( (LA34_0=='.') ) {s = 41;}

                        else if ( (LA34_0=='@') ) {s = 42;}

                        else if ( (LA34_0=='c') ) {s = 43;}

                        else if ( (LA34_0=='l') ) {s = 44;}

                        else if ( (LA34_0=='b') ) {s = 45;}

                        else if ( (LA34_0=='0') ) {s = 46;}

                        else if ( ((LA34_0>='1' && LA34_0<='9')) ) {s = 47;}

                        else if ( ((LA34_0>='A' && LA34_0<='E')||(LA34_0>='I' && LA34_0<='N')||(LA34_0>='P' && LA34_0<='S')||(LA34_0>='U' && LA34_0<='W')||LA34_0=='Z'||LA34_0=='_'||LA34_0=='d'||LA34_0=='g'||(LA34_0>='j' && LA34_0<='k')||LA34_0=='q'||(LA34_0>='v' && LA34_0<='w')||(LA34_0>='y' && LA34_0<='z')) ) {s = 48;}

                        else if ( (LA34_0=='^') ) {s = 49;}

                        else if ( (LA34_0=='$') ) {s = 50;}

                        else if ( (LA34_0=='\"') ) {s = 51;}

                        else if ( (LA34_0=='\'') ) {s = 52;}

                        else if ( ((LA34_0>='\t' && LA34_0<='\n')||LA34_0=='\r'||LA34_0==' ') ) {s = 53;}

                        else if ( ((LA34_0>='\u0000' && LA34_0<='\b')||(LA34_0>='\u000B' && LA34_0<='\f')||(LA34_0>='\u000E' && LA34_0<='\u001F')||LA34_0=='#'||LA34_0=='%'||LA34_0=='?'||LA34_0=='\\'||LA34_0=='`'||(LA34_0>='~' && LA34_0<='\uFFFF')) ) {s = 54;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA34_51 = input.LA(1);

                        s = -1;
                        if ( ((LA34_51>='\u0000' && LA34_51<='\uFFFF')) ) {s = 130;}

                        else s = 54;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 34, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}