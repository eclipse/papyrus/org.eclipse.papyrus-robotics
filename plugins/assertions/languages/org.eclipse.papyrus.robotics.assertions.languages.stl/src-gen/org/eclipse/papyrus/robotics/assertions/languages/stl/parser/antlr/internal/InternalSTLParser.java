package org.eclipse.papyrus.robotics.assertions.languages.stl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.papyrus.robotics.assertions.languages.stl.services.STLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSTLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_IDENTIFIER", "RULE_LPAREN", "RULE_RPAREN", "RULE_ABS", "RULE_SQRT", "RULE_EXP", "RULE_POW", "RULE_COMMA", "RULE_INTEGERLITERAL", "RULE_MINUS", "RULE_LBRACK", "RULE_COLON", "RULE_RBRACK", "RULE_UNIT", "RULE_DECIMALREALLITERAL", "RULE_SEC", "RULE_MSEC", "RULE_USEC", "RULE_NSEC", "RULE_PLUS", "RULE_TIMES", "RULE_DIVIDE", "RULE_LBRACE", "RULE_RBRACE", "RULE_SEMICOLON", "RULE_DOT", "RULE_AT", "RULE_PSEC", "RULE_ROS_TOPIC", "RULE_IMPORT", "RULE_INPUT", "RULE_OUTPUT", "RULE_INTERNAL", "RULE_CONSTANT", "RULE_DOMAINTYPEREAL", "RULE_DOMAINTYPEFLOAT", "RULE_DOMAINTYPELONG", "RULE_DOMAINTYPECOMPLEX", "RULE_DOMAINTYPEINT", "RULE_DOMAINTYPEBOOL", "RULE_DECIMALNUMERAL", "RULE_HEXNUMERAL", "RULE_BINARYNUMERAL", "RULE_NONZERODIGIT", "RULE_DIGITS", "RULE_UNDERSCORES", "RULE_DIGIT", "RULE_DIGITSANDUNDERSCORES", "RULE_DIGITORUNDERSCORE", "RULE_HEXDIGITS", "RULE_HEXDIGIT", "RULE_HEXDIGITSANDUNDERSCORES", "RULE_HEXDIGITORUNDERSCORE", "RULE_BINARYDIGITS", "RULE_BINARYDIGIT", "RULE_BINARYDIGITSANDUNDERSCORES", "RULE_BINARYDIGITORUNDERSCORE", "RULE_EXPONENTPART", "RULE_EXPONENTINDICATOR", "RULE_SIGNEDINTEGER", "RULE_SIGN", "RULE_IDENTIFIERSTART", "RULE_IDENTIFIERPART", "RULE_LETTERORUNDERSCORE", "RULE_LETTER", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'or'", "'|'", "'and'", "'&'", "'iff'", "'<->'", "'implies'", "'->'", "'xor'", "'rise'", "'fall'", "'always'", "'G'", "'eventually'", "'F'", "'historically'", "'H'", "'once'", "'O'", "'next'", "'X'", "'prev'", "'Y'", "'=='", "'='", "'!=='", "'!='", "'>='", "'<='", "'>'", "'<'", "'true'", "'TRUE'", "'false'", "'FALSE'"
    };
    public static final int RULE_EXP=9;
    public static final int RULE_MSEC=20;
    public static final int RULE_SIGN=64;
    public static final int RULE_DOMAINTYPEREAL=38;
    public static final int RULE_BINARYDIGITS=57;
    public static final int RULE_INTERNAL=36;
    public static final int RULE_DOMAINTYPEINT=42;
    public static final int RULE_UNDERSCORES=49;
    public static final int RULE_OUTPUT=35;
    public static final int RULE_NONZERODIGIT=47;
    public static final int RULE_ID=69;
    public static final int RULE_TIMES=24;
    public static final int RULE_DIGIT=50;
    public static final int RULE_COLON=15;
    public static final int RULE_LETTERORUNDERSCORE=67;
    public static final int RULE_DOMAINTYPELONG=40;
    public static final int RULE_USEC=21;
    public static final int RULE_INT=70;
    public static final int RULE_ML_COMMENT=72;
    public static final int RULE_BINARYDIGITORUNDERSCORE=60;
    public static final int RULE_IMPORT=33;
    public static final int RULE_DIGITSANDUNDERSCORES=51;
    public static final int RULE_AT=30;
    public static final int RULE_POW=10;
    public static final int RULE_DIVIDE=25;
    public static final int RULE_CONSTANT=37;
    public static final int RULE_SEC=19;
    public static final int RULE_DOMAINTYPEFLOAT=39;
    public static final int RULE_DOT=29;
    public static final int RULE_RBRACK=16;
    public static final int RULE_PSEC=31;
    public static final int RULE_LBRACE=26;
    public static final int RULE_HEXNUMERAL=45;
    public static final int RULE_HEXDIGITS=53;
    public static final int RULE_RBRACE=27;
    public static final int RULE_LETTER=68;
    public static final int RULE_EXPONENTINDICATOR=62;
    public static final int RULE_LBRACK=14;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int RULE_HEXDIGITORUNDERSCORE=56;
    public static final int RULE_LPAREN=5;
    public static final int RULE_DOMAINTYPECOMPLEX=41;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int RULE_NSEC=22;
    public static final int RULE_HEXDIGIT=54;
    public static final int RULE_COMMA=11;
    public static final int RULE_DOMAINTYPEBOOL=43;
    public static final int RULE_SQRT=8;
    public static final int RULE_DECIMALNUMERAL=44;
    public static final int RULE_BINARYNUMERAL=46;
    public static final int RULE_DIGITORUNDERSCORE=52;
    public static final int RULE_ABS=7;
    public static final int RULE_INTEGERLITERAL=12;
    public static final int RULE_IDENTIFIERPART=66;
    public static final int RULE_HEXDIGITSANDUNDERSCORES=55;
    public static final int RULE_SEMICOLON=28;
    public static final int RULE_IDENTIFIER=4;
    public static final int RULE_STRING=71;
    public static final int RULE_INPUT=34;
    public static final int RULE_DIGITS=48;
    public static final int RULE_UNIT=17;
    public static final int RULE_SL_COMMENT=73;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int RULE_PLUS=23;
    public static final int RULE_IDENTIFIERSTART=65;
    public static final int EOF=-1;
    public static final int T__76=76;
    public static final int RULE_ROS_TOPIC=32;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=74;
    public static final int RULE_SIGNEDINTEGER=63;
    public static final int RULE_ANY_OTHER=75;
    public static final int RULE_MINUS=13;
    public static final int RULE_RPAREN=6;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int RULE_BINARYDIGITSANDUNDERSCORES=59;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int RULE_BINARYDIGIT=58;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int RULE_EXPONENTPART=61;
    public static final int T__87=87;
    public static final int T__105=105;
    public static final int RULE_DECIMALREALLITERAL=18;

    // delegates
    // delegators


        public InternalSTLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSTLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSTLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSTL.g"; }



     	private STLGrammarAccess grammarAccess;

        public InternalSTLParser(TokenStream input, STLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "assignments";
       	}

       	@Override
       	protected STLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleassignments"
    // InternalSTL.g:64:1: entryRuleassignments returns [EObject current=null] : iv_ruleassignments= ruleassignments EOF ;
    public final EObject entryRuleassignments() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleassignments = null;


        try {
            // InternalSTL.g:64:52: (iv_ruleassignments= ruleassignments EOF )
            // InternalSTL.g:65:2: iv_ruleassignments= ruleassignments EOF
            {
             newCompositeNode(grammarAccess.getAssignmentsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleassignments=ruleassignments();

            state._fsp--;

             current =iv_ruleassignments; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleassignments"


    // $ANTLR start "ruleassignments"
    // InternalSTL.g:71:1: ruleassignments returns [EObject current=null] : ( (lv_assignments_0_0= ruleassignment ) )* ;
    public final EObject ruleassignments() throws RecognitionException {
        EObject current = null;

        EObject lv_assignments_0_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:77:2: ( ( (lv_assignments_0_0= ruleassignment ) )* )
            // InternalSTL.g:78:2: ( (lv_assignments_0_0= ruleassignment ) )*
            {
            // InternalSTL.g:78:2: ( (lv_assignments_0_0= ruleassignment ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_IDENTIFIER) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSTL.g:79:3: (lv_assignments_0_0= ruleassignment )
            	    {
            	    // InternalSTL.g:79:3: (lv_assignments_0_0= ruleassignment )
            	    // InternalSTL.g:80:4: lv_assignments_0_0= ruleassignment
            	    {

            	    				newCompositeNode(grammarAccess.getAssignmentsAccess().getAssignmentsAssignmentParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_assignments_0_0=ruleassignment();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getAssignmentsRule());
            	    				}
            	    				add(
            	    					current,
            	    					"assignments",
            	    					lv_assignments_0_0,
            	    					"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.assignment");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleassignments"


    // $ANTLR start "entryRuleassignment"
    // InternalSTL.g:100:1: entryRuleassignment returns [EObject current=null] : iv_ruleassignment= ruleassignment EOF ;
    public final EObject entryRuleassignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleassignment = null;


        try {
            // InternalSTL.g:100:51: (iv_ruleassignment= ruleassignment EOF )
            // InternalSTL.g:101:2: iv_ruleassignment= ruleassignment EOF
            {
             newCompositeNode(grammarAccess.getAssignmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleassignment=ruleassignment();

            state._fsp--;

             current =iv_ruleassignment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleassignment"


    // $ANTLR start "ruleassignment"
    // InternalSTL.g:107:1: ruleassignment returns [EObject current=null] : ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) ruleEqualOperator ( (lv_expr_2_0= ruleexpression ) ) ) ;
    public final EObject ruleassignment() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;
        EObject lv_expr_2_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:113:2: ( ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) ruleEqualOperator ( (lv_expr_2_0= ruleexpression ) ) ) )
            // InternalSTL.g:114:2: ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) ruleEqualOperator ( (lv_expr_2_0= ruleexpression ) ) )
            {
            // InternalSTL.g:114:2: ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) ruleEqualOperator ( (lv_expr_2_0= ruleexpression ) ) )
            // InternalSTL.g:115:3: ( (lv_id_0_0= RULE_IDENTIFIER ) ) ruleEqualOperator ( (lv_expr_2_0= ruleexpression ) )
            {
            // InternalSTL.g:115:3: ( (lv_id_0_0= RULE_IDENTIFIER ) )
            // InternalSTL.g:116:4: (lv_id_0_0= RULE_IDENTIFIER )
            {
            // InternalSTL.g:116:4: (lv_id_0_0= RULE_IDENTIFIER )
            // InternalSTL.g:117:5: lv_id_0_0= RULE_IDENTIFIER
            {
            lv_id_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_4); 

            					newLeafNode(lv_id_0_0, grammarAccess.getAssignmentAccess().getIdIdentifierTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssignmentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_0_0,
            						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.Identifier");
            				

            }


            }


            			newCompositeNode(grammarAccess.getAssignmentAccess().getEqualOperatorParserRuleCall_1());
            		
            pushFollow(FOLLOW_5);
            ruleEqualOperator();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            // InternalSTL.g:140:3: ( (lv_expr_2_0= ruleexpression ) )
            // InternalSTL.g:141:4: (lv_expr_2_0= ruleexpression )
            {
            // InternalSTL.g:141:4: (lv_expr_2_0= ruleexpression )
            // InternalSTL.g:142:5: lv_expr_2_0= ruleexpression
            {

            					newCompositeNode(grammarAccess.getAssignmentAccess().getExprExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_expr_2_0=ruleexpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssignmentRule());
            					}
            					set(
            						current,
            						"expr",
            						lv_expr_2_0,
            						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleassignment"


    // $ANTLR start "entryRuleexpression"
    // InternalSTL.g:163:1: entryRuleexpression returns [EObject current=null] : iv_ruleexpression= ruleexpression EOF ;
    public final EObject entryRuleexpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleexpression = null;


        try {
            // InternalSTL.g:163:51: (iv_ruleexpression= ruleexpression EOF )
            // InternalSTL.g:164:2: iv_ruleexpression= ruleexpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleexpression=ruleexpression();

            state._fsp--;

             current =iv_ruleexpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleexpression"


    // $ANTLR start "ruleexpression"
    // InternalSTL.g:170:1: ruleexpression returns [EObject current=null] : ( ( (lv_first_0_0= ruleexpression_start ) ) ( (lv_ec_1_0= ruleexpression_cont ) )? ) ;
    public final EObject ruleexpression() throws RecognitionException {
        EObject current = null;

        EObject lv_first_0_0 = null;

        EObject lv_ec_1_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:176:2: ( ( ( (lv_first_0_0= ruleexpression_start ) ) ( (lv_ec_1_0= ruleexpression_cont ) )? ) )
            // InternalSTL.g:177:2: ( ( (lv_first_0_0= ruleexpression_start ) ) ( (lv_ec_1_0= ruleexpression_cont ) )? )
            {
            // InternalSTL.g:177:2: ( ( (lv_first_0_0= ruleexpression_start ) ) ( (lv_ec_1_0= ruleexpression_cont ) )? )
            // InternalSTL.g:178:3: ( (lv_first_0_0= ruleexpression_start ) ) ( (lv_ec_1_0= ruleexpression_cont ) )?
            {
            // InternalSTL.g:178:3: ( (lv_first_0_0= ruleexpression_start ) )
            // InternalSTL.g:179:4: (lv_first_0_0= ruleexpression_start )
            {
            // InternalSTL.g:179:4: (lv_first_0_0= ruleexpression_start )
            // InternalSTL.g:180:5: lv_first_0_0= ruleexpression_start
            {

            					newCompositeNode(grammarAccess.getExpressionAccess().getFirstExpression_startParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_6);
            lv_first_0_0=ruleexpression_start();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExpressionRule());
            					}
            					set(
            						current,
            						"first",
            						lv_first_0_0,
            						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression_start");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSTL.g:197:3: ( (lv_ec_1_0= ruleexpression_cont ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>=76 && LA2_0<=106)) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalSTL.g:198:4: (lv_ec_1_0= ruleexpression_cont )
                    {
                    // InternalSTL.g:198:4: (lv_ec_1_0= ruleexpression_cont )
                    // InternalSTL.g:199:5: lv_ec_1_0= ruleexpression_cont
                    {

                    					newCompositeNode(grammarAccess.getExpressionAccess().getEcExpression_contParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_ec_1_0=ruleexpression_cont();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExpressionRule());
                    					}
                    					set(
                    						current,
                    						"ec",
                    						lv_ec_1_0,
                    						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression_cont");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleexpression"


    // $ANTLR start "entryRuleexpression_start"
    // InternalSTL.g:220:1: entryRuleexpression_start returns [EObject current=null] : iv_ruleexpression_start= ruleexpression_start EOF ;
    public final EObject entryRuleexpression_start() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleexpression_start = null;


        try {
            // InternalSTL.g:220:57: (iv_ruleexpression_start= ruleexpression_start EOF )
            // InternalSTL.g:221:2: iv_ruleexpression_start= ruleexpression_start EOF
            {
             newCompositeNode(grammarAccess.getExpression_startRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleexpression_start=ruleexpression_start();

            state._fsp--;

             current =iv_ruleexpression_start; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleexpression_start"


    // $ANTLR start "ruleexpression_start"
    // InternalSTL.g:227:1: ruleexpression_start returns [EObject current=null] : ( ( (lv_r_0_0= rulereal_expression ) ) | (this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) ) ;
    public final EObject ruleexpression_start() throws RecognitionException {
        EObject current = null;

        Token this_LPAREN_1=null;
        Token this_RPAREN_5=null;
        EObject lv_r_0_0 = null;

        EObject lv_e1_2_0 = null;

        EObject lv_e2_4_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:233:2: ( ( ( (lv_r_0_0= rulereal_expression ) ) | (this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) ) )
            // InternalSTL.g:234:2: ( ( (lv_r_0_0= rulereal_expression ) ) | (this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) )
            {
            // InternalSTL.g:234:2: ( ( (lv_r_0_0= rulereal_expression ) ) | (this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_IDENTIFIER||(LA3_0>=RULE_ABS && LA3_0<=RULE_POW)||(LA3_0>=RULE_INTEGERLITERAL && LA3_0<=RULE_MINUS)||LA3_0==RULE_DECIMALREALLITERAL) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_LPAREN) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalSTL.g:235:3: ( (lv_r_0_0= rulereal_expression ) )
                    {
                    // InternalSTL.g:235:3: ( (lv_r_0_0= rulereal_expression ) )
                    // InternalSTL.g:236:4: (lv_r_0_0= rulereal_expression )
                    {
                    // InternalSTL.g:236:4: (lv_r_0_0= rulereal_expression )
                    // InternalSTL.g:237:5: lv_r_0_0= rulereal_expression
                    {

                    					newCompositeNode(grammarAccess.getExpression_startAccess().getRReal_expressionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_r_0_0=rulereal_expression();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExpression_startRule());
                    					}
                    					set(
                    						current,
                    						"r",
                    						lv_r_0_0,
                    						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:255:3: (this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN )
                    {
                    // InternalSTL.g:255:3: (this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN )
                    // InternalSTL.g:256:4: this_LPAREN_1= RULE_LPAREN ( (lv_e1_2_0= rulereal_expression ) ) rulecomparisonOp ( (lv_e2_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN
                    {
                    this_LPAREN_1=(Token)match(input,RULE_LPAREN,FOLLOW_7); 

                    				newLeafNode(this_LPAREN_1, grammarAccess.getExpression_startAccess().getLPARENTerminalRuleCall_1_0());
                    			
                    // InternalSTL.g:260:4: ( (lv_e1_2_0= rulereal_expression ) )
                    // InternalSTL.g:261:5: (lv_e1_2_0= rulereal_expression )
                    {
                    // InternalSTL.g:261:5: (lv_e1_2_0= rulereal_expression )
                    // InternalSTL.g:262:6: lv_e1_2_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getExpression_startAccess().getE1Real_expressionParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_e1_2_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_startRule());
                    						}
                    						set(
                    							current,
                    							"e1",
                    							lv_e1_2_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    				newCompositeNode(grammarAccess.getExpression_startAccess().getComparisonOpParserRuleCall_1_2());
                    			
                    pushFollow(FOLLOW_7);
                    rulecomparisonOp();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:286:4: ( (lv_e2_4_0= rulereal_expression ) )
                    // InternalSTL.g:287:5: (lv_e2_4_0= rulereal_expression )
                    {
                    // InternalSTL.g:287:5: (lv_e2_4_0= rulereal_expression )
                    // InternalSTL.g:288:6: lv_e2_4_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getExpression_startAccess().getE2Real_expressionParserRuleCall_1_3_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_e2_4_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_startRule());
                    						}
                    						set(
                    							current,
                    							"e2",
                    							lv_e2_4_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_5=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_5, grammarAccess.getExpression_startAccess().getRPARENTerminalRuleCall_1_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleexpression_start"


    // $ANTLR start "entryRuleexpression_cont"
    // InternalSTL.g:314:1: entryRuleexpression_cont returns [EObject current=null] : iv_ruleexpression_cont= ruleexpression_cont EOF ;
    public final EObject entryRuleexpression_cont() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleexpression_cont = null;


        try {
            // InternalSTL.g:314:56: (iv_ruleexpression_cont= ruleexpression_cont EOF )
            // InternalSTL.g:315:2: iv_ruleexpression_cont= ruleexpression_cont EOF
            {
             newCompositeNode(grammarAccess.getExpression_contRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleexpression_cont=ruleexpression_cont();

            state._fsp--;

             current =iv_ruleexpression_cont; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleexpression_cont"


    // $ANTLR start "ruleexpression_cont"
    // InternalSTL.g:321:1: ruleexpression_cont returns [EObject current=null] : ( ( ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) ) ) | ( ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) ) ) | ( ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) ) ) | ( ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) ) ) | ( ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN ) | ( ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN ) | ( rulePreviousOperator ( (lv_e_21_0= ruleexpression ) ) ) | ( ruleNextOperator ( (lv_e_23_0= ruleexpression ) ) ) | ( ruleOrOperator ( (lv_e_25_0= ruleexpression ) ) ) | ( ruleAndOperator ( (lv_e_27_0= ruleexpression ) ) ) | ( ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) ) ) | ( ruleIffOperator ( (lv_e_32_0= ruleexpression ) ) ) | ( ruleXorOperator ( (lv_e_34_0= ruleexpression ) ) ) | ( rulecomparisonOp ( (lv_e_36_0= ruleexpression ) ) ) ) ;
    public final EObject ruleexpression_cont() throws RecognitionException {
        EObject current = null;

        Token this_LPAREN_13=null;
        Token this_RPAREN_15=null;
        Token this_LPAREN_17=null;
        Token this_RPAREN_19=null;
        EObject lv_i_1_0 = null;

        EObject lv_e_2_0 = null;

        EObject lv_i_4_0 = null;

        EObject lv_e_5_0 = null;

        EObject lv_i_7_0 = null;

        EObject lv_e_8_0 = null;

        EObject lv_i_10_0 = null;

        EObject lv_e_11_0 = null;

        EObject lv_e_14_0 = null;

        EObject lv_e_18_0 = null;

        EObject lv_e_21_0 = null;

        EObject lv_e_23_0 = null;

        EObject lv_e_25_0 = null;

        EObject lv_e_27_0 = null;

        EObject lv_e_29_0 = null;

        EObject lv_e_30_0 = null;

        EObject lv_e_32_0 = null;

        EObject lv_e_34_0 = null;

        EObject lv_e_36_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:327:2: ( ( ( ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) ) ) | ( ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) ) ) | ( ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) ) ) | ( ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) ) ) | ( ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN ) | ( ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN ) | ( rulePreviousOperator ( (lv_e_21_0= ruleexpression ) ) ) | ( ruleNextOperator ( (lv_e_23_0= ruleexpression ) ) ) | ( ruleOrOperator ( (lv_e_25_0= ruleexpression ) ) ) | ( ruleAndOperator ( (lv_e_27_0= ruleexpression ) ) ) | ( ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) ) ) | ( ruleIffOperator ( (lv_e_32_0= ruleexpression ) ) ) | ( ruleXorOperator ( (lv_e_34_0= ruleexpression ) ) ) | ( rulecomparisonOp ( (lv_e_36_0= ruleexpression ) ) ) ) )
            // InternalSTL.g:328:2: ( ( ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) ) ) | ( ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) ) ) | ( ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) ) ) | ( ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) ) ) | ( ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN ) | ( ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN ) | ( rulePreviousOperator ( (lv_e_21_0= ruleexpression ) ) ) | ( ruleNextOperator ( (lv_e_23_0= ruleexpression ) ) ) | ( ruleOrOperator ( (lv_e_25_0= ruleexpression ) ) ) | ( ruleAndOperator ( (lv_e_27_0= ruleexpression ) ) ) | ( ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) ) ) | ( ruleIffOperator ( (lv_e_32_0= ruleexpression ) ) ) | ( ruleXorOperator ( (lv_e_34_0= ruleexpression ) ) ) | ( rulecomparisonOp ( (lv_e_36_0= ruleexpression ) ) ) )
            {
            // InternalSTL.g:328:2: ( ( ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) ) ) | ( ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) ) ) | ( ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) ) ) | ( ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) ) ) | ( ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN ) | ( ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN ) | ( rulePreviousOperator ( (lv_e_21_0= ruleexpression ) ) ) | ( ruleNextOperator ( (lv_e_23_0= ruleexpression ) ) ) | ( ruleOrOperator ( (lv_e_25_0= ruleexpression ) ) ) | ( ruleAndOperator ( (lv_e_27_0= ruleexpression ) ) ) | ( ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) ) ) | ( ruleIffOperator ( (lv_e_32_0= ruleexpression ) ) ) | ( ruleXorOperator ( (lv_e_34_0= ruleexpression ) ) ) | ( rulecomparisonOp ( (lv_e_36_0= ruleexpression ) ) ) )
            int alt9=14;
            switch ( input.LA(1) ) {
            case 87:
            case 88:
                {
                alt9=1;
                }
                break;
            case 89:
            case 90:
                {
                alt9=2;
                }
                break;
            case 91:
            case 92:
                {
                alt9=3;
                }
                break;
            case 93:
            case 94:
                {
                alt9=4;
                }
                break;
            case 85:
                {
                alt9=5;
                }
                break;
            case 86:
                {
                alt9=6;
                }
                break;
            case 97:
            case 98:
                {
                alt9=7;
                }
                break;
            case 95:
            case 96:
                {
                alt9=8;
                }
                break;
            case 76:
            case 77:
                {
                alt9=9;
                }
                break;
            case 78:
            case 79:
                {
                alt9=10;
                }
                break;
            case 82:
            case 83:
                {
                alt9=11;
                }
                break;
            case 80:
            case 81:
                {
                alt9=12;
                }
                break;
            case 84:
                {
                alt9=13;
                }
                break;
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
                {
                alt9=14;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalSTL.g:329:3: ( ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:329:3: ( ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) ) )
                    // InternalSTL.g:330:4: ruleAlwaysOperator ( (lv_i_1_0= ruleinterval ) )? ( (lv_e_2_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getAlwaysOperatorParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_10);
                    ruleAlwaysOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:337:4: ( (lv_i_1_0= ruleinterval ) )?
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==RULE_LBRACK) ) {
                        alt4=1;
                    }
                    switch (alt4) {
                        case 1 :
                            // InternalSTL.g:338:5: (lv_i_1_0= ruleinterval )
                            {
                            // InternalSTL.g:338:5: (lv_i_1_0= ruleinterval )
                            // InternalSTL.g:339:6: lv_i_1_0= ruleinterval
                            {

                            						newCompositeNode(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_0_1_0());
                            					
                            pushFollow(FOLLOW_5);
                            lv_i_1_0=ruleinterval();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                            						}
                            						set(
                            							current,
                            							"i",
                            							lv_i_1_0,
                            							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.interval");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalSTL.g:356:4: ( (lv_e_2_0= ruleexpression ) )
                    // InternalSTL.g:357:5: (lv_e_2_0= ruleexpression )
                    {
                    // InternalSTL.g:357:5: (lv_e_2_0= ruleexpression )
                    // InternalSTL.g:358:6: lv_e_2_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_0_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_2_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_2_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:377:3: ( ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:377:3: ( ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) ) )
                    // InternalSTL.g:378:4: ruleEventuallyOperator ( (lv_i_4_0= ruleinterval ) )? ( (lv_e_5_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getEventuallyOperatorParserRuleCall_1_0());
                    			
                    pushFollow(FOLLOW_10);
                    ruleEventuallyOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:385:4: ( (lv_i_4_0= ruleinterval ) )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==RULE_LBRACK) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // InternalSTL.g:386:5: (lv_i_4_0= ruleinterval )
                            {
                            // InternalSTL.g:386:5: (lv_i_4_0= ruleinterval )
                            // InternalSTL.g:387:6: lv_i_4_0= ruleinterval
                            {

                            						newCompositeNode(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_1_1_0());
                            					
                            pushFollow(FOLLOW_5);
                            lv_i_4_0=ruleinterval();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                            						}
                            						set(
                            							current,
                            							"i",
                            							lv_i_4_0,
                            							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.interval");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalSTL.g:404:4: ( (lv_e_5_0= ruleexpression ) )
                    // InternalSTL.g:405:5: (lv_e_5_0= ruleexpression )
                    {
                    // InternalSTL.g:405:5: (lv_e_5_0= ruleexpression )
                    // InternalSTL.g:406:6: lv_e_5_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_1_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_5_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_5_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:425:3: ( ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:425:3: ( ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) ) )
                    // InternalSTL.g:426:4: ruleHistoricallyOperator ( (lv_i_7_0= ruleinterval ) )? ( (lv_e_8_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getHistoricallyOperatorParserRuleCall_2_0());
                    			
                    pushFollow(FOLLOW_10);
                    ruleHistoricallyOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:433:4: ( (lv_i_7_0= ruleinterval ) )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==RULE_LBRACK) ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // InternalSTL.g:434:5: (lv_i_7_0= ruleinterval )
                            {
                            // InternalSTL.g:434:5: (lv_i_7_0= ruleinterval )
                            // InternalSTL.g:435:6: lv_i_7_0= ruleinterval
                            {

                            						newCompositeNode(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_2_1_0());
                            					
                            pushFollow(FOLLOW_5);
                            lv_i_7_0=ruleinterval();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                            						}
                            						set(
                            							current,
                            							"i",
                            							lv_i_7_0,
                            							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.interval");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalSTL.g:452:4: ( (lv_e_8_0= ruleexpression ) )
                    // InternalSTL.g:453:5: (lv_e_8_0= ruleexpression )
                    {
                    // InternalSTL.g:453:5: (lv_e_8_0= ruleexpression )
                    // InternalSTL.g:454:6: lv_e_8_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_8_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_8_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalSTL.g:473:3: ( ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:473:3: ( ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) ) )
                    // InternalSTL.g:474:4: ruleOnceOperator ( (lv_i_10_0= ruleinterval ) )? ( (lv_e_11_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getOnceOperatorParserRuleCall_3_0());
                    			
                    pushFollow(FOLLOW_10);
                    ruleOnceOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:481:4: ( (lv_i_10_0= ruleinterval ) )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==RULE_LBRACK) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalSTL.g:482:5: (lv_i_10_0= ruleinterval )
                            {
                            // InternalSTL.g:482:5: (lv_i_10_0= ruleinterval )
                            // InternalSTL.g:483:6: lv_i_10_0= ruleinterval
                            {

                            						newCompositeNode(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_3_1_0());
                            					
                            pushFollow(FOLLOW_5);
                            lv_i_10_0=ruleinterval();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                            						}
                            						set(
                            							current,
                            							"i",
                            							lv_i_10_0,
                            							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.interval");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }
                            break;

                    }

                    // InternalSTL.g:500:4: ( (lv_e_11_0= ruleexpression ) )
                    // InternalSTL.g:501:5: (lv_e_11_0= ruleexpression )
                    {
                    // InternalSTL.g:501:5: (lv_e_11_0= ruleexpression )
                    // InternalSTL.g:502:6: lv_e_11_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_11_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_11_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 5 :
                    // InternalSTL.g:521:3: ( ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN )
                    {
                    // InternalSTL.g:521:3: ( ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN )
                    // InternalSTL.g:522:4: ruleRiseOperator this_LPAREN_13= RULE_LPAREN ( (lv_e_14_0= ruleexpression ) ) this_RPAREN_15= RULE_RPAREN
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getRiseOperatorParserRuleCall_4_0());
                    			
                    pushFollow(FOLLOW_11);
                    ruleRiseOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    this_LPAREN_13=(Token)match(input,RULE_LPAREN,FOLLOW_5); 

                    				newLeafNode(this_LPAREN_13, grammarAccess.getExpression_contAccess().getLPARENTerminalRuleCall_4_1());
                    			
                    // InternalSTL.g:533:4: ( (lv_e_14_0= ruleexpression ) )
                    // InternalSTL.g:534:5: (lv_e_14_0= ruleexpression )
                    {
                    // InternalSTL.g:534:5: (lv_e_14_0= ruleexpression )
                    // InternalSTL.g:535:6: lv_e_14_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_e_14_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_14_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_15=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_15, grammarAccess.getExpression_contAccess().getRPARENTerminalRuleCall_4_3());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalSTL.g:558:3: ( ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN )
                    {
                    // InternalSTL.g:558:3: ( ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN )
                    // InternalSTL.g:559:4: ruleFallOperator this_LPAREN_17= RULE_LPAREN ( (lv_e_18_0= ruleexpression ) ) this_RPAREN_19= RULE_RPAREN
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getFallOperatorParserRuleCall_5_0());
                    			
                    pushFollow(FOLLOW_11);
                    ruleFallOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    this_LPAREN_17=(Token)match(input,RULE_LPAREN,FOLLOW_5); 

                    				newLeafNode(this_LPAREN_17, grammarAccess.getExpression_contAccess().getLPARENTerminalRuleCall_5_1());
                    			
                    // InternalSTL.g:570:4: ( (lv_e_18_0= ruleexpression ) )
                    // InternalSTL.g:571:5: (lv_e_18_0= ruleexpression )
                    {
                    // InternalSTL.g:571:5: (lv_e_18_0= ruleexpression )
                    // InternalSTL.g:572:6: lv_e_18_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_e_18_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_18_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_19=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_19, grammarAccess.getExpression_contAccess().getRPARENTerminalRuleCall_5_3());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalSTL.g:595:3: ( rulePreviousOperator ( (lv_e_21_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:595:3: ( rulePreviousOperator ( (lv_e_21_0= ruleexpression ) ) )
                    // InternalSTL.g:596:4: rulePreviousOperator ( (lv_e_21_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getPreviousOperatorParserRuleCall_6_0());
                    			
                    pushFollow(FOLLOW_5);
                    rulePreviousOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:603:4: ( (lv_e_21_0= ruleexpression ) )
                    // InternalSTL.g:604:5: (lv_e_21_0= ruleexpression )
                    {
                    // InternalSTL.g:604:5: (lv_e_21_0= ruleexpression )
                    // InternalSTL.g:605:6: lv_e_21_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_21_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_21_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 8 :
                    // InternalSTL.g:624:3: ( ruleNextOperator ( (lv_e_23_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:624:3: ( ruleNextOperator ( (lv_e_23_0= ruleexpression ) ) )
                    // InternalSTL.g:625:4: ruleNextOperator ( (lv_e_23_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getNextOperatorParserRuleCall_7_0());
                    			
                    pushFollow(FOLLOW_5);
                    ruleNextOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:632:4: ( (lv_e_23_0= ruleexpression ) )
                    // InternalSTL.g:633:5: (lv_e_23_0= ruleexpression )
                    {
                    // InternalSTL.g:633:5: (lv_e_23_0= ruleexpression )
                    // InternalSTL.g:634:6: lv_e_23_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_23_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_23_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 9 :
                    // InternalSTL.g:653:3: ( ruleOrOperator ( (lv_e_25_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:653:3: ( ruleOrOperator ( (lv_e_25_0= ruleexpression ) ) )
                    // InternalSTL.g:654:4: ruleOrOperator ( (lv_e_25_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getOrOperatorParserRuleCall_8_0());
                    			
                    pushFollow(FOLLOW_5);
                    ruleOrOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:661:4: ( (lv_e_25_0= ruleexpression ) )
                    // InternalSTL.g:662:5: (lv_e_25_0= ruleexpression )
                    {
                    // InternalSTL.g:662:5: (lv_e_25_0= ruleexpression )
                    // InternalSTL.g:663:6: lv_e_25_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_25_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_25_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 10 :
                    // InternalSTL.g:682:3: ( ruleAndOperator ( (lv_e_27_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:682:3: ( ruleAndOperator ( (lv_e_27_0= ruleexpression ) ) )
                    // InternalSTL.g:683:4: ruleAndOperator ( (lv_e_27_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getAndOperatorParserRuleCall_9_0());
                    			
                    pushFollow(FOLLOW_5);
                    ruleAndOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:690:4: ( (lv_e_27_0= ruleexpression ) )
                    // InternalSTL.g:691:5: (lv_e_27_0= ruleexpression )
                    {
                    // InternalSTL.g:691:5: (lv_e_27_0= ruleexpression )
                    // InternalSTL.g:692:6: lv_e_27_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_27_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_27_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 11 :
                    // InternalSTL.g:711:3: ( ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) ) )
                    {
                    // InternalSTL.g:711:3: ( ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) ) )
                    // InternalSTL.g:712:4: ruleImpliesOperator ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getImpliesOperatorParserRuleCall_10_0());
                    			
                    pushFollow(FOLLOW_12);
                    ruleImpliesOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:719:4: ( ( (lv_e_29_0= ruleexpression ) ) | ( (lv_e_30_0= ruleexpression_cont ) ) )
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( ((LA8_0>=RULE_IDENTIFIER && LA8_0<=RULE_LPAREN)||(LA8_0>=RULE_ABS && LA8_0<=RULE_POW)||(LA8_0>=RULE_INTEGERLITERAL && LA8_0<=RULE_MINUS)||LA8_0==RULE_DECIMALREALLITERAL) ) {
                        alt8=1;
                    }
                    else if ( ((LA8_0>=76 && LA8_0<=106)) ) {
                        alt8=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 8, 0, input);

                        throw nvae;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalSTL.g:720:5: ( (lv_e_29_0= ruleexpression ) )
                            {
                            // InternalSTL.g:720:5: ( (lv_e_29_0= ruleexpression ) )
                            // InternalSTL.g:721:6: (lv_e_29_0= ruleexpression )
                            {
                            // InternalSTL.g:721:6: (lv_e_29_0= ruleexpression )
                            // InternalSTL.g:722:7: lv_e_29_0= ruleexpression
                            {

                            							newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_10_1_0_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_e_29_0=ruleexpression();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getExpression_contRule());
                            							}
                            							set(
                            								current,
                            								"e",
                            								lv_e_29_0,
                            								"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalSTL.g:740:5: ( (lv_e_30_0= ruleexpression_cont ) )
                            {
                            // InternalSTL.g:740:5: ( (lv_e_30_0= ruleexpression_cont ) )
                            // InternalSTL.g:741:6: (lv_e_30_0= ruleexpression_cont )
                            {
                            // InternalSTL.g:741:6: (lv_e_30_0= ruleexpression_cont )
                            // InternalSTL.g:742:7: lv_e_30_0= ruleexpression_cont
                            {

                            							newCompositeNode(grammarAccess.getExpression_contAccess().getEExpression_contParserRuleCall_10_1_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_e_30_0=ruleexpression_cont();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getExpression_contRule());
                            							}
                            							set(
                            								current,
                            								"e",
                            								lv_e_30_0,
                            								"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression_cont");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 12 :
                    // InternalSTL.g:762:3: ( ruleIffOperator ( (lv_e_32_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:762:3: ( ruleIffOperator ( (lv_e_32_0= ruleexpression ) ) )
                    // InternalSTL.g:763:4: ruleIffOperator ( (lv_e_32_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getIffOperatorParserRuleCall_11_0());
                    			
                    pushFollow(FOLLOW_5);
                    ruleIffOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:770:4: ( (lv_e_32_0= ruleexpression ) )
                    // InternalSTL.g:771:5: (lv_e_32_0= ruleexpression )
                    {
                    // InternalSTL.g:771:5: (lv_e_32_0= ruleexpression )
                    // InternalSTL.g:772:6: lv_e_32_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_11_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_32_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_32_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 13 :
                    // InternalSTL.g:791:3: ( ruleXorOperator ( (lv_e_34_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:791:3: ( ruleXorOperator ( (lv_e_34_0= ruleexpression ) ) )
                    // InternalSTL.g:792:4: ruleXorOperator ( (lv_e_34_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getXorOperatorParserRuleCall_12_0());
                    			
                    pushFollow(FOLLOW_5);
                    ruleXorOperator();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:799:4: ( (lv_e_34_0= ruleexpression ) )
                    // InternalSTL.g:800:5: (lv_e_34_0= ruleexpression )
                    {
                    // InternalSTL.g:800:5: (lv_e_34_0= ruleexpression )
                    // InternalSTL.g:801:6: lv_e_34_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_34_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_34_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 14 :
                    // InternalSTL.g:820:3: ( rulecomparisonOp ( (lv_e_36_0= ruleexpression ) ) )
                    {
                    // InternalSTL.g:820:3: ( rulecomparisonOp ( (lv_e_36_0= ruleexpression ) ) )
                    // InternalSTL.g:821:4: rulecomparisonOp ( (lv_e_36_0= ruleexpression ) )
                    {

                    				newCompositeNode(grammarAccess.getExpression_contAccess().getComparisonOpParserRuleCall_13_0());
                    			
                    pushFollow(FOLLOW_5);
                    rulecomparisonOp();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:828:4: ( (lv_e_36_0= ruleexpression ) )
                    // InternalSTL.g:829:5: (lv_e_36_0= ruleexpression )
                    {
                    // InternalSTL.g:829:5: (lv_e_36_0= ruleexpression )
                    // InternalSTL.g:830:6: lv_e_36_0= ruleexpression
                    {

                    						newCompositeNode(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_13_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_e_36_0=ruleexpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExpression_contRule());
                    						}
                    						set(
                    							current,
                    							"e",
                    							lv_e_36_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleexpression_cont"


    // $ANTLR start "entryRulereal_expression"
    // InternalSTL.g:852:1: entryRulereal_expression returns [EObject current=null] : iv_rulereal_expression= rulereal_expression EOF ;
    public final EObject entryRulereal_expression() throws RecognitionException {
        EObject current = null;

        EObject iv_rulereal_expression = null;


        try {
            // InternalSTL.g:852:56: (iv_rulereal_expression= rulereal_expression EOF )
            // InternalSTL.g:853:2: iv_rulereal_expression= rulereal_expression EOF
            {
             newCompositeNode(grammarAccess.getReal_expressionRule()); 
            pushFollow(FOLLOW_1);
            iv_rulereal_expression=rulereal_expression();

            state._fsp--;

             current =iv_rulereal_expression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulereal_expression"


    // $ANTLR start "rulereal_expression"
    // InternalSTL.g:859:1: rulereal_expression returns [EObject current=null] : ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) | ( (lv_lit_1_0= rulenum_literal ) ) | (this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) | (this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN ) | (this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN ) | (this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN ) ) ;
    public final EObject rulereal_expression() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;
        Token this_ABS_2=null;
        Token this_LPAREN_3=null;
        Token this_RPAREN_5=null;
        Token this_SQRT_6=null;
        Token this_LPAREN_7=null;
        Token this_RPAREN_9=null;
        Token this_EXP_10=null;
        Token this_LPAREN_11=null;
        Token this_RPAREN_13=null;
        Token this_POW_14=null;
        Token this_LPAREN_15=null;
        Token this_COMMA_17=null;
        Token this_RPAREN_19=null;
        EObject lv_lit_1_0 = null;

        EObject lv_exp_4_0 = null;

        EObject lv_exp_8_0 = null;

        EObject lv_exp_12_0 = null;

        EObject lv_exp1_16_0 = null;

        EObject lv_exp2_18_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:865:2: ( ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) | ( (lv_lit_1_0= rulenum_literal ) ) | (this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) | (this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN ) | (this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN ) | (this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN ) ) )
            // InternalSTL.g:866:2: ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) | ( (lv_lit_1_0= rulenum_literal ) ) | (this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) | (this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN ) | (this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN ) | (this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN ) )
            {
            // InternalSTL.g:866:2: ( ( (lv_id_0_0= RULE_IDENTIFIER ) ) | ( (lv_lit_1_0= rulenum_literal ) ) | (this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN ) | (this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN ) | (this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN ) | (this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN ) )
            int alt10=6;
            switch ( input.LA(1) ) {
            case RULE_IDENTIFIER:
                {
                alt10=1;
                }
                break;
            case RULE_INTEGERLITERAL:
            case RULE_MINUS:
            case RULE_DECIMALREALLITERAL:
                {
                alt10=2;
                }
                break;
            case RULE_ABS:
                {
                alt10=3;
                }
                break;
            case RULE_SQRT:
                {
                alt10=4;
                }
                break;
            case RULE_EXP:
                {
                alt10=5;
                }
                break;
            case RULE_POW:
                {
                alt10=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalSTL.g:867:3: ( (lv_id_0_0= RULE_IDENTIFIER ) )
                    {
                    // InternalSTL.g:867:3: ( (lv_id_0_0= RULE_IDENTIFIER ) )
                    // InternalSTL.g:868:4: (lv_id_0_0= RULE_IDENTIFIER )
                    {
                    // InternalSTL.g:868:4: (lv_id_0_0= RULE_IDENTIFIER )
                    // InternalSTL.g:869:5: lv_id_0_0= RULE_IDENTIFIER
                    {
                    lv_id_0_0=(Token)match(input,RULE_IDENTIFIER,FOLLOW_2); 

                    					newLeafNode(lv_id_0_0, grammarAccess.getReal_expressionAccess().getIdIdentifierTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getReal_expressionRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"id",
                    						lv_id_0_0,
                    						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.Identifier");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:886:3: ( (lv_lit_1_0= rulenum_literal ) )
                    {
                    // InternalSTL.g:886:3: ( (lv_lit_1_0= rulenum_literal ) )
                    // InternalSTL.g:887:4: (lv_lit_1_0= rulenum_literal )
                    {
                    // InternalSTL.g:887:4: (lv_lit_1_0= rulenum_literal )
                    // InternalSTL.g:888:5: lv_lit_1_0= rulenum_literal
                    {

                    					newCompositeNode(grammarAccess.getReal_expressionAccess().getLitNum_literalParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_lit_1_0=rulenum_literal();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getReal_expressionRule());
                    					}
                    					set(
                    						current,
                    						"lit",
                    						lv_lit_1_0,
                    						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.num_literal");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:906:3: (this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN )
                    {
                    // InternalSTL.g:906:3: (this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN )
                    // InternalSTL.g:907:4: this_ABS_2= RULE_ABS this_LPAREN_3= RULE_LPAREN ( (lv_exp_4_0= rulereal_expression ) ) this_RPAREN_5= RULE_RPAREN
                    {
                    this_ABS_2=(Token)match(input,RULE_ABS,FOLLOW_11); 

                    				newLeafNode(this_ABS_2, grammarAccess.getReal_expressionAccess().getABSTerminalRuleCall_2_0());
                    			
                    this_LPAREN_3=(Token)match(input,RULE_LPAREN,FOLLOW_7); 

                    				newLeafNode(this_LPAREN_3, grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_2_1());
                    			
                    // InternalSTL.g:915:4: ( (lv_exp_4_0= rulereal_expression ) )
                    // InternalSTL.g:916:5: (lv_exp_4_0= rulereal_expression )
                    {
                    // InternalSTL.g:916:5: (lv_exp_4_0= rulereal_expression )
                    // InternalSTL.g:917:6: lv_exp_4_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_exp_4_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReal_expressionRule());
                    						}
                    						set(
                    							current,
                    							"exp",
                    							lv_exp_4_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_5=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_5, grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_2_3());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalSTL.g:940:3: (this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN )
                    {
                    // InternalSTL.g:940:3: (this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN )
                    // InternalSTL.g:941:4: this_SQRT_6= RULE_SQRT this_LPAREN_7= RULE_LPAREN ( (lv_exp_8_0= rulereal_expression ) ) this_RPAREN_9= RULE_RPAREN
                    {
                    this_SQRT_6=(Token)match(input,RULE_SQRT,FOLLOW_11); 

                    				newLeafNode(this_SQRT_6, grammarAccess.getReal_expressionAccess().getSQRTTerminalRuleCall_3_0());
                    			
                    this_LPAREN_7=(Token)match(input,RULE_LPAREN,FOLLOW_7); 

                    				newLeafNode(this_LPAREN_7, grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_3_1());
                    			
                    // InternalSTL.g:949:4: ( (lv_exp_8_0= rulereal_expression ) )
                    // InternalSTL.g:950:5: (lv_exp_8_0= rulereal_expression )
                    {
                    // InternalSTL.g:950:5: (lv_exp_8_0= rulereal_expression )
                    // InternalSTL.g:951:6: lv_exp_8_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_exp_8_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReal_expressionRule());
                    						}
                    						set(
                    							current,
                    							"exp",
                    							lv_exp_8_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_9=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_9, grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_3_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalSTL.g:974:3: (this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN )
                    {
                    // InternalSTL.g:974:3: (this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN )
                    // InternalSTL.g:975:4: this_EXP_10= RULE_EXP this_LPAREN_11= RULE_LPAREN ( (lv_exp_12_0= rulereal_expression ) ) this_RPAREN_13= RULE_RPAREN
                    {
                    this_EXP_10=(Token)match(input,RULE_EXP,FOLLOW_11); 

                    				newLeafNode(this_EXP_10, grammarAccess.getReal_expressionAccess().getEXPTerminalRuleCall_4_0());
                    			
                    this_LPAREN_11=(Token)match(input,RULE_LPAREN,FOLLOW_7); 

                    				newLeafNode(this_LPAREN_11, grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_4_1());
                    			
                    // InternalSTL.g:983:4: ( (lv_exp_12_0= rulereal_expression ) )
                    // InternalSTL.g:984:5: (lv_exp_12_0= rulereal_expression )
                    {
                    // InternalSTL.g:984:5: (lv_exp_12_0= rulereal_expression )
                    // InternalSTL.g:985:6: lv_exp_12_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_exp_12_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReal_expressionRule());
                    						}
                    						set(
                    							current,
                    							"exp",
                    							lv_exp_12_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_13=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_13, grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_4_3());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalSTL.g:1008:3: (this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN )
                    {
                    // InternalSTL.g:1008:3: (this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN )
                    // InternalSTL.g:1009:4: this_POW_14= RULE_POW this_LPAREN_15= RULE_LPAREN ( (lv_exp1_16_0= rulereal_expression ) ) this_COMMA_17= RULE_COMMA ( (lv_exp2_18_0= rulereal_expression ) ) this_RPAREN_19= RULE_RPAREN
                    {
                    this_POW_14=(Token)match(input,RULE_POW,FOLLOW_11); 

                    				newLeafNode(this_POW_14, grammarAccess.getReal_expressionAccess().getPOWTerminalRuleCall_5_0());
                    			
                    this_LPAREN_15=(Token)match(input,RULE_LPAREN,FOLLOW_7); 

                    				newLeafNode(this_LPAREN_15, grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_5_1());
                    			
                    // InternalSTL.g:1017:4: ( (lv_exp1_16_0= rulereal_expression ) )
                    // InternalSTL.g:1018:5: (lv_exp1_16_0= rulereal_expression )
                    {
                    // InternalSTL.g:1018:5: (lv_exp1_16_0= rulereal_expression )
                    // InternalSTL.g:1019:6: lv_exp1_16_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getReal_expressionAccess().getExp1Real_expressionParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_exp1_16_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReal_expressionRule());
                    						}
                    						set(
                    							current,
                    							"exp1",
                    							lv_exp1_16_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_COMMA_17=(Token)match(input,RULE_COMMA,FOLLOW_7); 

                    				newLeafNode(this_COMMA_17, grammarAccess.getReal_expressionAccess().getCOMMATerminalRuleCall_5_3());
                    			
                    // InternalSTL.g:1040:4: ( (lv_exp2_18_0= rulereal_expression ) )
                    // InternalSTL.g:1041:5: (lv_exp2_18_0= rulereal_expression )
                    {
                    // InternalSTL.g:1041:5: (lv_exp2_18_0= rulereal_expression )
                    // InternalSTL.g:1042:6: lv_exp2_18_0= rulereal_expression
                    {

                    						newCompositeNode(grammarAccess.getReal_expressionAccess().getExp2Real_expressionParserRuleCall_5_4_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_exp2_18_0=rulereal_expression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReal_expressionRule());
                    						}
                    						set(
                    							current,
                    							"exp2",
                    							lv_exp2_18_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.real_expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    this_RPAREN_19=(Token)match(input,RULE_RPAREN,FOLLOW_2); 

                    				newLeafNode(this_RPAREN_19, grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_5_5());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulereal_expression"


    // $ANTLR start "entryRulecomparisonOp"
    // InternalSTL.g:1068:1: entryRulecomparisonOp returns [String current=null] : iv_rulecomparisonOp= rulecomparisonOp EOF ;
    public final String entryRulecomparisonOp() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulecomparisonOp = null;


        try {
            // InternalSTL.g:1068:52: (iv_rulecomparisonOp= rulecomparisonOp EOF )
            // InternalSTL.g:1069:2: iv_rulecomparisonOp= rulecomparisonOp EOF
            {
             newCompositeNode(grammarAccess.getComparisonOpRule()); 
            pushFollow(FOLLOW_1);
            iv_rulecomparisonOp=rulecomparisonOp();

            state._fsp--;

             current =iv_rulecomparisonOp.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulecomparisonOp"


    // $ANTLR start "rulecomparisonOp"
    // InternalSTL.g:1075:1: rulecomparisonOp returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_LesserOrEqualOperator_0= ruleLesserOrEqualOperator | this_GreaterOrEqualOperator_1= ruleGreaterOrEqualOperator | this_LesserOperator_2= ruleLesserOperator | this_GreaterOperator_3= ruleGreaterOperator | this_EqualOperator_4= ruleEqualOperator | this_NotEqualOperator_5= ruleNotEqualOperator ) ;
    public final AntlrDatatypeRuleToken rulecomparisonOp() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_LesserOrEqualOperator_0 = null;

        AntlrDatatypeRuleToken this_GreaterOrEqualOperator_1 = null;

        AntlrDatatypeRuleToken this_LesserOperator_2 = null;

        AntlrDatatypeRuleToken this_GreaterOperator_3 = null;

        AntlrDatatypeRuleToken this_EqualOperator_4 = null;

        AntlrDatatypeRuleToken this_NotEqualOperator_5 = null;



        	enterRule();

        try {
            // InternalSTL.g:1081:2: ( (this_LesserOrEqualOperator_0= ruleLesserOrEqualOperator | this_GreaterOrEqualOperator_1= ruleGreaterOrEqualOperator | this_LesserOperator_2= ruleLesserOperator | this_GreaterOperator_3= ruleGreaterOperator | this_EqualOperator_4= ruleEqualOperator | this_NotEqualOperator_5= ruleNotEqualOperator ) )
            // InternalSTL.g:1082:2: (this_LesserOrEqualOperator_0= ruleLesserOrEqualOperator | this_GreaterOrEqualOperator_1= ruleGreaterOrEqualOperator | this_LesserOperator_2= ruleLesserOperator | this_GreaterOperator_3= ruleGreaterOperator | this_EqualOperator_4= ruleEqualOperator | this_NotEqualOperator_5= ruleNotEqualOperator )
            {
            // InternalSTL.g:1082:2: (this_LesserOrEqualOperator_0= ruleLesserOrEqualOperator | this_GreaterOrEqualOperator_1= ruleGreaterOrEqualOperator | this_LesserOperator_2= ruleLesserOperator | this_GreaterOperator_3= ruleGreaterOperator | this_EqualOperator_4= ruleEqualOperator | this_NotEqualOperator_5= ruleNotEqualOperator )
            int alt11=6;
            switch ( input.LA(1) ) {
            case 104:
                {
                alt11=1;
                }
                break;
            case 103:
                {
                alt11=2;
                }
                break;
            case 106:
                {
                alt11=3;
                }
                break;
            case 105:
                {
                alt11=4;
                }
                break;
            case 99:
            case 100:
                {
                alt11=5;
                }
                break;
            case 101:
            case 102:
                {
                alt11=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalSTL.g:1083:3: this_LesserOrEqualOperator_0= ruleLesserOrEqualOperator
                    {

                    			newCompositeNode(grammarAccess.getComparisonOpAccess().getLesserOrEqualOperatorParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_LesserOrEqualOperator_0=ruleLesserOrEqualOperator();

                    state._fsp--;


                    			current.merge(this_LesserOrEqualOperator_0);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1094:3: this_GreaterOrEqualOperator_1= ruleGreaterOrEqualOperator
                    {

                    			newCompositeNode(grammarAccess.getComparisonOpAccess().getGreaterOrEqualOperatorParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_GreaterOrEqualOperator_1=ruleGreaterOrEqualOperator();

                    state._fsp--;


                    			current.merge(this_GreaterOrEqualOperator_1);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalSTL.g:1105:3: this_LesserOperator_2= ruleLesserOperator
                    {

                    			newCompositeNode(grammarAccess.getComparisonOpAccess().getLesserOperatorParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_LesserOperator_2=ruleLesserOperator();

                    state._fsp--;


                    			current.merge(this_LesserOperator_2);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalSTL.g:1116:3: this_GreaterOperator_3= ruleGreaterOperator
                    {

                    			newCompositeNode(grammarAccess.getComparisonOpAccess().getGreaterOperatorParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_GreaterOperator_3=ruleGreaterOperator();

                    state._fsp--;


                    			current.merge(this_GreaterOperator_3);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalSTL.g:1127:3: this_EqualOperator_4= ruleEqualOperator
                    {

                    			newCompositeNode(grammarAccess.getComparisonOpAccess().getEqualOperatorParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_EqualOperator_4=ruleEqualOperator();

                    state._fsp--;


                    			current.merge(this_EqualOperator_4);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalSTL.g:1138:3: this_NotEqualOperator_5= ruleNotEqualOperator
                    {

                    			newCompositeNode(grammarAccess.getComparisonOpAccess().getNotEqualOperatorParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_NotEqualOperator_5=ruleNotEqualOperator();

                    state._fsp--;


                    			current.merge(this_NotEqualOperator_5);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulecomparisonOp"


    // $ANTLR start "entryRulenum_literal"
    // InternalSTL.g:1152:1: entryRulenum_literal returns [EObject current=null] : iv_rulenum_literal= rulenum_literal EOF ;
    public final EObject entryRulenum_literal() throws RecognitionException {
        EObject current = null;

        EObject iv_rulenum_literal = null;


        try {
            // InternalSTL.g:1152:52: (iv_rulenum_literal= rulenum_literal EOF )
            // InternalSTL.g:1153:2: iv_rulenum_literal= rulenum_literal EOF
            {
             newCompositeNode(grammarAccess.getNum_literalRule()); 
            pushFollow(FOLLOW_1);
            iv_rulenum_literal=rulenum_literal();

            state._fsp--;

             current =iv_rulenum_literal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulenum_literal"


    // $ANTLR start "rulenum_literal"
    // InternalSTL.g:1159:1: rulenum_literal returns [EObject current=null] : ( ( (lv_ilit_0_0= RULE_INTEGERLITERAL ) ) | ( (lv_rlit_1_0= ruleRealLiteral ) ) | (this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) ) ) ) ;
    public final EObject rulenum_literal() throws RecognitionException {
        EObject current = null;

        Token lv_ilit_0_0=null;
        Token this_MINUS_2=null;
        AntlrDatatypeRuleToken lv_rlit_1_0 = null;

        EObject lv_lit_3_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:1165:2: ( ( ( (lv_ilit_0_0= RULE_INTEGERLITERAL ) ) | ( (lv_rlit_1_0= ruleRealLiteral ) ) | (this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) ) ) ) )
            // InternalSTL.g:1166:2: ( ( (lv_ilit_0_0= RULE_INTEGERLITERAL ) ) | ( (lv_rlit_1_0= ruleRealLiteral ) ) | (this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) ) ) )
            {
            // InternalSTL.g:1166:2: ( ( (lv_ilit_0_0= RULE_INTEGERLITERAL ) ) | ( (lv_rlit_1_0= ruleRealLiteral ) ) | (this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) ) ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case RULE_INTEGERLITERAL:
                {
                alt12=1;
                }
                break;
            case RULE_DECIMALREALLITERAL:
                {
                alt12=2;
                }
                break;
            case RULE_MINUS:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalSTL.g:1167:3: ( (lv_ilit_0_0= RULE_INTEGERLITERAL ) )
                    {
                    // InternalSTL.g:1167:3: ( (lv_ilit_0_0= RULE_INTEGERLITERAL ) )
                    // InternalSTL.g:1168:4: (lv_ilit_0_0= RULE_INTEGERLITERAL )
                    {
                    // InternalSTL.g:1168:4: (lv_ilit_0_0= RULE_INTEGERLITERAL )
                    // InternalSTL.g:1169:5: lv_ilit_0_0= RULE_INTEGERLITERAL
                    {
                    lv_ilit_0_0=(Token)match(input,RULE_INTEGERLITERAL,FOLLOW_2); 

                    					newLeafNode(lv_ilit_0_0, grammarAccess.getNum_literalAccess().getIlitIntegerLiteralTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getNum_literalRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"ilit",
                    						lv_ilit_0_0,
                    						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.IntegerLiteral");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1186:3: ( (lv_rlit_1_0= ruleRealLiteral ) )
                    {
                    // InternalSTL.g:1186:3: ( (lv_rlit_1_0= ruleRealLiteral ) )
                    // InternalSTL.g:1187:4: (lv_rlit_1_0= ruleRealLiteral )
                    {
                    // InternalSTL.g:1187:4: (lv_rlit_1_0= ruleRealLiteral )
                    // InternalSTL.g:1188:5: lv_rlit_1_0= ruleRealLiteral
                    {

                    					newCompositeNode(grammarAccess.getNum_literalAccess().getRlitRealLiteralParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_rlit_1_0=ruleRealLiteral();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getNum_literalRule());
                    					}
                    					set(
                    						current,
                    						"rlit",
                    						lv_rlit_1_0,
                    						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.RealLiteral");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:1206:3: (this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) ) )
                    {
                    // InternalSTL.g:1206:3: (this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) ) )
                    // InternalSTL.g:1207:4: this_MINUS_2= RULE_MINUS ( (lv_lit_3_0= rulenum_literal ) )
                    {
                    this_MINUS_2=(Token)match(input,RULE_MINUS,FOLLOW_14); 

                    				newLeafNode(this_MINUS_2, grammarAccess.getNum_literalAccess().getMINUSTerminalRuleCall_2_0());
                    			
                    // InternalSTL.g:1211:4: ( (lv_lit_3_0= rulenum_literal ) )
                    // InternalSTL.g:1212:5: (lv_lit_3_0= rulenum_literal )
                    {
                    // InternalSTL.g:1212:5: (lv_lit_3_0= rulenum_literal )
                    // InternalSTL.g:1213:6: lv_lit_3_0= rulenum_literal
                    {

                    						newCompositeNode(grammarAccess.getNum_literalAccess().getLitNum_literalParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_lit_3_0=rulenum_literal();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNum_literalRule());
                    						}
                    						set(
                    							current,
                    							"lit",
                    							lv_lit_3_0,
                    							"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.num_literal");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulenum_literal"


    // $ANTLR start "entryRuleinterval"
    // InternalSTL.g:1235:1: entryRuleinterval returns [EObject current=null] : iv_ruleinterval= ruleinterval EOF ;
    public final EObject entryRuleinterval() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinterval = null;


        try {
            // InternalSTL.g:1235:49: (iv_ruleinterval= ruleinterval EOF )
            // InternalSTL.g:1236:2: iv_ruleinterval= ruleinterval EOF
            {
             newCompositeNode(grammarAccess.getIntervalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleinterval=ruleinterval();

            state._fsp--;

             current =iv_ruleinterval; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinterval"


    // $ANTLR start "ruleinterval"
    // InternalSTL.g:1242:1: ruleinterval returns [EObject current=null] : (this_LBRACK_0= RULE_LBRACK ( (lv_time1_1_0= ruleintervalTime ) ) (this_COLON_2= RULE_COLON | this_COMMA_3= RULE_COMMA ) ( (lv_time2_4_0= ruleintervalTime ) ) this_RBRACK_5= RULE_RBRACK ) ;
    public final EObject ruleinterval() throws RecognitionException {
        EObject current = null;

        Token this_LBRACK_0=null;
        Token this_COLON_2=null;
        Token this_COMMA_3=null;
        Token this_RBRACK_5=null;
        EObject lv_time1_1_0 = null;

        EObject lv_time2_4_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:1248:2: ( (this_LBRACK_0= RULE_LBRACK ( (lv_time1_1_0= ruleintervalTime ) ) (this_COLON_2= RULE_COLON | this_COMMA_3= RULE_COMMA ) ( (lv_time2_4_0= ruleintervalTime ) ) this_RBRACK_5= RULE_RBRACK ) )
            // InternalSTL.g:1249:2: (this_LBRACK_0= RULE_LBRACK ( (lv_time1_1_0= ruleintervalTime ) ) (this_COLON_2= RULE_COLON | this_COMMA_3= RULE_COMMA ) ( (lv_time2_4_0= ruleintervalTime ) ) this_RBRACK_5= RULE_RBRACK )
            {
            // InternalSTL.g:1249:2: (this_LBRACK_0= RULE_LBRACK ( (lv_time1_1_0= ruleintervalTime ) ) (this_COLON_2= RULE_COLON | this_COMMA_3= RULE_COMMA ) ( (lv_time2_4_0= ruleintervalTime ) ) this_RBRACK_5= RULE_RBRACK )
            // InternalSTL.g:1250:3: this_LBRACK_0= RULE_LBRACK ( (lv_time1_1_0= ruleintervalTime ) ) (this_COLON_2= RULE_COLON | this_COMMA_3= RULE_COMMA ) ( (lv_time2_4_0= ruleintervalTime ) ) this_RBRACK_5= RULE_RBRACK
            {
            this_LBRACK_0=(Token)match(input,RULE_LBRACK,FOLLOW_15); 

            			newLeafNode(this_LBRACK_0, grammarAccess.getIntervalAccess().getLBRACKTerminalRuleCall_0());
            		
            // InternalSTL.g:1254:3: ( (lv_time1_1_0= ruleintervalTime ) )
            // InternalSTL.g:1255:4: (lv_time1_1_0= ruleintervalTime )
            {
            // InternalSTL.g:1255:4: (lv_time1_1_0= ruleintervalTime )
            // InternalSTL.g:1256:5: lv_time1_1_0= ruleintervalTime
            {

            					newCompositeNode(grammarAccess.getIntervalAccess().getTime1IntervalTimeParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_16);
            lv_time1_1_0=ruleintervalTime();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIntervalRule());
            					}
            					set(
            						current,
            						"time1",
            						lv_time1_1_0,
            						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.intervalTime");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSTL.g:1273:3: (this_COLON_2= RULE_COLON | this_COMMA_3= RULE_COMMA )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_COLON) ) {
                alt13=1;
            }
            else if ( (LA13_0==RULE_COMMA) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalSTL.g:1274:4: this_COLON_2= RULE_COLON
                    {
                    this_COLON_2=(Token)match(input,RULE_COLON,FOLLOW_15); 

                    				newLeafNode(this_COLON_2, grammarAccess.getIntervalAccess().getCOLONTerminalRuleCall_2_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1279:4: this_COMMA_3= RULE_COMMA
                    {
                    this_COMMA_3=(Token)match(input,RULE_COMMA,FOLLOW_15); 

                    				newLeafNode(this_COMMA_3, grammarAccess.getIntervalAccess().getCOMMATerminalRuleCall_2_1());
                    			

                    }
                    break;

            }

            // InternalSTL.g:1284:3: ( (lv_time2_4_0= ruleintervalTime ) )
            // InternalSTL.g:1285:4: (lv_time2_4_0= ruleintervalTime )
            {
            // InternalSTL.g:1285:4: (lv_time2_4_0= ruleintervalTime )
            // InternalSTL.g:1286:5: lv_time2_4_0= ruleintervalTime
            {

            					newCompositeNode(grammarAccess.getIntervalAccess().getTime2IntervalTimeParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_17);
            lv_time2_4_0=ruleintervalTime();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIntervalRule());
            					}
            					set(
            						current,
            						"time2",
            						lv_time2_4_0,
            						"org.eclipse.papyrus.robotics.assertions.languages.stl.STL.intervalTime");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_RBRACK_5=(Token)match(input,RULE_RBRACK,FOLLOW_2); 

            			newLeafNode(this_RBRACK_5, grammarAccess.getIntervalAccess().getRBRACKTerminalRuleCall_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinterval"


    // $ANTLR start "entryRuleintervalTime"
    // InternalSTL.g:1311:1: entryRuleintervalTime returns [EObject current=null] : iv_ruleintervalTime= ruleintervalTime EOF ;
    public final EObject entryRuleintervalTime() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleintervalTime = null;


        try {
            // InternalSTL.g:1311:53: (iv_ruleintervalTime= ruleintervalTime EOF )
            // InternalSTL.g:1312:2: iv_ruleintervalTime= ruleintervalTime EOF
            {
             newCompositeNode(grammarAccess.getIntervalTimeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleintervalTime=ruleintervalTime();

            state._fsp--;

             current =iv_ruleintervalTime; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleintervalTime"


    // $ANTLR start "ruleintervalTime"
    // InternalSTL.g:1318:1: ruleintervalTime returns [EObject current=null] : ( (this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )? ) | (this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )? ) ) ;
    public final EObject ruleintervalTime() throws RecognitionException {
        EObject current = null;

        Token this_UNIT_1=null;
        Token this_Identifier_2=null;
        Token this_UNIT_3=null;
        EObject this_num_literal_0 = null;



        	enterRule();

        try {
            // InternalSTL.g:1324:2: ( ( (this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )? ) | (this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )? ) ) )
            // InternalSTL.g:1325:2: ( (this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )? ) | (this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )? ) )
            {
            // InternalSTL.g:1325:2: ( (this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )? ) | (this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )? ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( ((LA16_0>=RULE_INTEGERLITERAL && LA16_0<=RULE_MINUS)||LA16_0==RULE_DECIMALREALLITERAL) ) {
                alt16=1;
            }
            else if ( (LA16_0==RULE_IDENTIFIER) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalSTL.g:1326:3: (this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )? )
                    {
                    // InternalSTL.g:1326:3: (this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )? )
                    // InternalSTL.g:1327:4: this_num_literal_0= rulenum_literal (this_UNIT_1= RULE_UNIT )?
                    {

                    				newCompositeNode(grammarAccess.getIntervalTimeAccess().getNum_literalParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_18);
                    this_num_literal_0=rulenum_literal();

                    state._fsp--;


                    				current = this_num_literal_0;
                    				afterParserOrEnumRuleCall();
                    			
                    // InternalSTL.g:1335:4: (this_UNIT_1= RULE_UNIT )?
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0==RULE_UNIT) ) {
                        alt14=1;
                    }
                    switch (alt14) {
                        case 1 :
                            // InternalSTL.g:1336:5: this_UNIT_1= RULE_UNIT
                            {
                            this_UNIT_1=(Token)match(input,RULE_UNIT,FOLLOW_2); 

                            					newLeafNode(this_UNIT_1, grammarAccess.getIntervalTimeAccess().getUNITTerminalRuleCall_0_1());
                            				

                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1343:3: (this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )? )
                    {
                    // InternalSTL.g:1343:3: (this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )? )
                    // InternalSTL.g:1344:4: this_Identifier_2= RULE_IDENTIFIER (this_UNIT_3= RULE_UNIT )?
                    {
                    this_Identifier_2=(Token)match(input,RULE_IDENTIFIER,FOLLOW_18); 

                    				newLeafNode(this_Identifier_2, grammarAccess.getIntervalTimeAccess().getIdentifierTerminalRuleCall_1_0());
                    			
                    // InternalSTL.g:1348:4: (this_UNIT_3= RULE_UNIT )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==RULE_UNIT) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // InternalSTL.g:1349:5: this_UNIT_3= RULE_UNIT
                            {
                            this_UNIT_3=(Token)match(input,RULE_UNIT,FOLLOW_2); 

                            					newLeafNode(this_UNIT_3, grammarAccess.getIntervalTimeAccess().getUNITTerminalRuleCall_1_1());
                            				

                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleintervalTime"


    // $ANTLR start "ruleOrOperator"
    // InternalSTL.g:1360:1: ruleOrOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'or' | kw= '|' ) ;
    public final AntlrDatatypeRuleToken ruleOrOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1366:2: ( (kw= 'or' | kw= '|' ) )
            // InternalSTL.g:1367:2: (kw= 'or' | kw= '|' )
            {
            // InternalSTL.g:1367:2: (kw= 'or' | kw= '|' )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==76) ) {
                alt17=1;
            }
            else if ( (LA17_0==77) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalSTL.g:1368:3: kw= 'or'
                    {
                    kw=(Token)match(input,76,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOrOperatorAccess().getOrKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1374:3: kw= '|'
                    {
                    kw=(Token)match(input,77,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOrOperatorAccess().getVerticalLineKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrOperator"


    // $ANTLR start "ruleAndOperator"
    // InternalSTL.g:1384:1: ruleAndOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'and' | kw= '&' ) ;
    public final AntlrDatatypeRuleToken ruleAndOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1390:2: ( (kw= 'and' | kw= '&' ) )
            // InternalSTL.g:1391:2: (kw= 'and' | kw= '&' )
            {
            // InternalSTL.g:1391:2: (kw= 'and' | kw= '&' )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==78) ) {
                alt18=1;
            }
            else if ( (LA18_0==79) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalSTL.g:1392:3: kw= 'and'
                    {
                    kw=(Token)match(input,78,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAndOperatorAccess().getAndKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1398:3: kw= '&'
                    {
                    kw=(Token)match(input,79,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAndOperatorAccess().getAmpersandKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndOperator"


    // $ANTLR start "ruleIffOperator"
    // InternalSTL.g:1408:1: ruleIffOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'iff' | kw= '<->' ) ;
    public final AntlrDatatypeRuleToken ruleIffOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1414:2: ( (kw= 'iff' | kw= '<->' ) )
            // InternalSTL.g:1415:2: (kw= 'iff' | kw= '<->' )
            {
            // InternalSTL.g:1415:2: (kw= 'iff' | kw= '<->' )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==80) ) {
                alt19=1;
            }
            else if ( (LA19_0==81) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalSTL.g:1416:3: kw= 'iff'
                    {
                    kw=(Token)match(input,80,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getIffOperatorAccess().getIffKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1422:3: kw= '<->'
                    {
                    kw=(Token)match(input,81,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getIffOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIffOperator"


    // $ANTLR start "ruleImpliesOperator"
    // InternalSTL.g:1432:1: ruleImpliesOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'implies' | kw= '->' ) ;
    public final AntlrDatatypeRuleToken ruleImpliesOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1438:2: ( (kw= 'implies' | kw= '->' ) )
            // InternalSTL.g:1439:2: (kw= 'implies' | kw= '->' )
            {
            // InternalSTL.g:1439:2: (kw= 'implies' | kw= '->' )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==82) ) {
                alt20=1;
            }
            else if ( (LA20_0==83) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalSTL.g:1440:3: kw= 'implies'
                    {
                    kw=(Token)match(input,82,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getImpliesOperatorAccess().getImpliesKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1446:3: kw= '->'
                    {
                    kw=(Token)match(input,83,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getImpliesOperatorAccess().getHyphenMinusGreaterThanSignKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImpliesOperator"


    // $ANTLR start "ruleXorOperator"
    // InternalSTL.g:1456:1: ruleXorOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'xor' ;
    public final AntlrDatatypeRuleToken ruleXorOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1462:2: (kw= 'xor' )
            // InternalSTL.g:1463:2: kw= 'xor'
            {
            kw=(Token)match(input,84,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getXorOperatorAccess().getXorKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXorOperator"


    // $ANTLR start "ruleRiseOperator"
    // InternalSTL.g:1472:1: ruleRiseOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'rise' ;
    public final AntlrDatatypeRuleToken ruleRiseOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1478:2: (kw= 'rise' )
            // InternalSTL.g:1479:2: kw= 'rise'
            {
            kw=(Token)match(input,85,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getRiseOperatorAccess().getRiseKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRiseOperator"


    // $ANTLR start "ruleFallOperator"
    // InternalSTL.g:1488:1: ruleFallOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'fall' ;
    public final AntlrDatatypeRuleToken ruleFallOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1494:2: (kw= 'fall' )
            // InternalSTL.g:1495:2: kw= 'fall'
            {
            kw=(Token)match(input,86,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getFallOperatorAccess().getFallKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFallOperator"


    // $ANTLR start "ruleAlwaysOperator"
    // InternalSTL.g:1504:1: ruleAlwaysOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'always' | kw= 'G' ) ;
    public final AntlrDatatypeRuleToken ruleAlwaysOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1510:2: ( (kw= 'always' | kw= 'G' ) )
            // InternalSTL.g:1511:2: (kw= 'always' | kw= 'G' )
            {
            // InternalSTL.g:1511:2: (kw= 'always' | kw= 'G' )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==87) ) {
                alt21=1;
            }
            else if ( (LA21_0==88) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalSTL.g:1512:3: kw= 'always'
                    {
                    kw=(Token)match(input,87,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAlwaysOperatorAccess().getAlwaysKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1518:3: kw= 'G'
                    {
                    kw=(Token)match(input,88,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAlwaysOperatorAccess().getGKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlwaysOperator"


    // $ANTLR start "ruleEventuallyOperator"
    // InternalSTL.g:1528:1: ruleEventuallyOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'eventually' | kw= 'F' ) ;
    public final AntlrDatatypeRuleToken ruleEventuallyOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1534:2: ( (kw= 'eventually' | kw= 'F' ) )
            // InternalSTL.g:1535:2: (kw= 'eventually' | kw= 'F' )
            {
            // InternalSTL.g:1535:2: (kw= 'eventually' | kw= 'F' )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==89) ) {
                alt22=1;
            }
            else if ( (LA22_0==90) ) {
                alt22=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalSTL.g:1536:3: kw= 'eventually'
                    {
                    kw=(Token)match(input,89,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEventuallyOperatorAccess().getEventuallyKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1542:3: kw= 'F'
                    {
                    kw=(Token)match(input,90,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEventuallyOperatorAccess().getFKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventuallyOperator"


    // $ANTLR start "ruleHistoricallyOperator"
    // InternalSTL.g:1552:1: ruleHistoricallyOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'historically' | kw= 'H' ) ;
    public final AntlrDatatypeRuleToken ruleHistoricallyOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1558:2: ( (kw= 'historically' | kw= 'H' ) )
            // InternalSTL.g:1559:2: (kw= 'historically' | kw= 'H' )
            {
            // InternalSTL.g:1559:2: (kw= 'historically' | kw= 'H' )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==91) ) {
                alt23=1;
            }
            else if ( (LA23_0==92) ) {
                alt23=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalSTL.g:1560:3: kw= 'historically'
                    {
                    kw=(Token)match(input,91,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getHistoricallyOperatorAccess().getHistoricallyKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1566:3: kw= 'H'
                    {
                    kw=(Token)match(input,92,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getHistoricallyOperatorAccess().getHKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHistoricallyOperator"


    // $ANTLR start "ruleOnceOperator"
    // InternalSTL.g:1576:1: ruleOnceOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'once' | kw= 'O' ) ;
    public final AntlrDatatypeRuleToken ruleOnceOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1582:2: ( (kw= 'once' | kw= 'O' ) )
            // InternalSTL.g:1583:2: (kw= 'once' | kw= 'O' )
            {
            // InternalSTL.g:1583:2: (kw= 'once' | kw= 'O' )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==93) ) {
                alt24=1;
            }
            else if ( (LA24_0==94) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // InternalSTL.g:1584:3: kw= 'once'
                    {
                    kw=(Token)match(input,93,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOnceOperatorAccess().getOnceKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1590:3: kw= 'O'
                    {
                    kw=(Token)match(input,94,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOnceOperatorAccess().getOKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOnceOperator"


    // $ANTLR start "ruleNextOperator"
    // InternalSTL.g:1600:1: ruleNextOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'next' | kw= 'X' ) ;
    public final AntlrDatatypeRuleToken ruleNextOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1606:2: ( (kw= 'next' | kw= 'X' ) )
            // InternalSTL.g:1607:2: (kw= 'next' | kw= 'X' )
            {
            // InternalSTL.g:1607:2: (kw= 'next' | kw= 'X' )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==95) ) {
                alt25=1;
            }
            else if ( (LA25_0==96) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // InternalSTL.g:1608:3: kw= 'next'
                    {
                    kw=(Token)match(input,95,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNextOperatorAccess().getNextKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1614:3: kw= 'X'
                    {
                    kw=(Token)match(input,96,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNextOperatorAccess().getXKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNextOperator"


    // $ANTLR start "rulePreviousOperator"
    // InternalSTL.g:1624:1: rulePreviousOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'prev' | kw= 'Y' ) ;
    public final AntlrDatatypeRuleToken rulePreviousOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1630:2: ( (kw= 'prev' | kw= 'Y' ) )
            // InternalSTL.g:1631:2: (kw= 'prev' | kw= 'Y' )
            {
            // InternalSTL.g:1631:2: (kw= 'prev' | kw= 'Y' )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==97) ) {
                alt26=1;
            }
            else if ( (LA26_0==98) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // InternalSTL.g:1632:3: kw= 'prev'
                    {
                    kw=(Token)match(input,97,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getPreviousOperatorAccess().getPrevKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1638:3: kw= 'Y'
                    {
                    kw=(Token)match(input,98,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getPreviousOperatorAccess().getYKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePreviousOperator"


    // $ANTLR start "ruleEqualOperator"
    // InternalSTL.g:1648:1: ruleEqualOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '==' | kw= '=' ) ;
    public final AntlrDatatypeRuleToken ruleEqualOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1654:2: ( (kw= '==' | kw= '=' ) )
            // InternalSTL.g:1655:2: (kw= '==' | kw= '=' )
            {
            // InternalSTL.g:1655:2: (kw= '==' | kw= '=' )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==99) ) {
                alt27=1;
            }
            else if ( (LA27_0==100) ) {
                alt27=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // InternalSTL.g:1656:3: kw= '=='
                    {
                    kw=(Token)match(input,99,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEqualOperatorAccess().getEqualsSignEqualsSignKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1662:3: kw= '='
                    {
                    kw=(Token)match(input,100,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEqualOperatorAccess().getEqualsSignKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEqualOperator"


    // $ANTLR start "ruleNotEqualOperator"
    // InternalSTL.g:1672:1: ruleNotEqualOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '!==' | kw= '!=' ) ;
    public final AntlrDatatypeRuleToken ruleNotEqualOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1678:2: ( (kw= '!==' | kw= '!=' ) )
            // InternalSTL.g:1679:2: (kw= '!==' | kw= '!=' )
            {
            // InternalSTL.g:1679:2: (kw= '!==' | kw= '!=' )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==101) ) {
                alt28=1;
            }
            else if ( (LA28_0==102) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // InternalSTL.g:1680:3: kw= '!=='
                    {
                    kw=(Token)match(input,101,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNotEqualOperatorAccess().getExclamationMarkEqualsSignEqualsSignKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1686:3: kw= '!='
                    {
                    kw=(Token)match(input,102,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNotEqualOperatorAccess().getExclamationMarkEqualsSignKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotEqualOperator"


    // $ANTLR start "ruleGreaterOrEqualOperator"
    // InternalSTL.g:1696:1: ruleGreaterOrEqualOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '>=' ;
    public final AntlrDatatypeRuleToken ruleGreaterOrEqualOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1702:2: (kw= '>=' )
            // InternalSTL.g:1703:2: kw= '>='
            {
            kw=(Token)match(input,103,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getGreaterOrEqualOperatorAccess().getGreaterThanSignEqualsSignKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGreaterOrEqualOperator"


    // $ANTLR start "ruleLesserOrEqualOperator"
    // InternalSTL.g:1712:1: ruleLesserOrEqualOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '<=' ;
    public final AntlrDatatypeRuleToken ruleLesserOrEqualOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1718:2: (kw= '<=' )
            // InternalSTL.g:1719:2: kw= '<='
            {
            kw=(Token)match(input,104,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getLesserOrEqualOperatorAccess().getLessThanSignEqualsSignKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLesserOrEqualOperator"


    // $ANTLR start "ruleGreaterOperator"
    // InternalSTL.g:1728:1: ruleGreaterOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '>' ;
    public final AntlrDatatypeRuleToken ruleGreaterOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1734:2: (kw= '>' )
            // InternalSTL.g:1735:2: kw= '>'
            {
            kw=(Token)match(input,105,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getGreaterOperatorAccess().getGreaterThanSignKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGreaterOperator"


    // $ANTLR start "ruleLesserOperator"
    // InternalSTL.g:1744:1: ruleLesserOperator returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '<' ;
    public final AntlrDatatypeRuleToken ruleLesserOperator() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1750:2: (kw= '<' )
            // InternalSTL.g:1751:2: kw= '<'
            {
            kw=(Token)match(input,106,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getLesserOperatorAccess().getLessThanSignKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLesserOperator"


    // $ANTLR start "ruleTRUE"
    // InternalSTL.g:1760:1: ruleTRUE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'TRUE' ) ;
    public final AntlrDatatypeRuleToken ruleTRUE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1766:2: ( (kw= 'true' | kw= 'TRUE' ) )
            // InternalSTL.g:1767:2: (kw= 'true' | kw= 'TRUE' )
            {
            // InternalSTL.g:1767:2: (kw= 'true' | kw= 'TRUE' )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==107) ) {
                alt29=1;
            }
            else if ( (LA29_0==108) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // InternalSTL.g:1768:3: kw= 'true'
                    {
                    kw=(Token)match(input,107,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTRUEAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1774:3: kw= 'TRUE'
                    {
                    kw=(Token)match(input,108,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getTRUEAccess().getTRUEKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTRUE"


    // $ANTLR start "ruleFALSE"
    // InternalSTL.g:1784:1: ruleFALSE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'false' | kw= 'FALSE' ) ;
    public final AntlrDatatypeRuleToken ruleFALSE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalSTL.g:1790:2: ( (kw= 'false' | kw= 'FALSE' ) )
            // InternalSTL.g:1791:2: (kw= 'false' | kw= 'FALSE' )
            {
            // InternalSTL.g:1791:2: (kw= 'false' | kw= 'FALSE' )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==109) ) {
                alt30=1;
            }
            else if ( (LA30_0==110) ) {
                alt30=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalSTL.g:1792:3: kw= 'false'
                    {
                    kw=(Token)match(input,109,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getFALSEAccess().getFalseKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSTL.g:1798:3: kw= 'FALSE'
                    {
                    kw=(Token)match(input,110,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getFALSEAccess().getFALSEKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFALSE"


    // $ANTLR start "entryRuleRealLiteral"
    // InternalSTL.g:1807:1: entryRuleRealLiteral returns [String current=null] : iv_ruleRealLiteral= ruleRealLiteral EOF ;
    public final String entryRuleRealLiteral() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRealLiteral = null;


        try {
            // InternalSTL.g:1807:51: (iv_ruleRealLiteral= ruleRealLiteral EOF )
            // InternalSTL.g:1808:2: iv_ruleRealLiteral= ruleRealLiteral EOF
            {
             newCompositeNode(grammarAccess.getRealLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRealLiteral=ruleRealLiteral();

            state._fsp--;

             current =iv_ruleRealLiteral.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRealLiteral"


    // $ANTLR start "ruleRealLiteral"
    // InternalSTL.g:1814:1: ruleRealLiteral returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_DecimalRealLiteral_0= RULE_DECIMALREALLITERAL ;
    public final AntlrDatatypeRuleToken ruleRealLiteral() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_DecimalRealLiteral_0=null;


        	enterRule();

        try {
            // InternalSTL.g:1820:2: (this_DecimalRealLiteral_0= RULE_DECIMALREALLITERAL )
            // InternalSTL.g:1821:2: this_DecimalRealLiteral_0= RULE_DECIMALREALLITERAL
            {
            this_DecimalRealLiteral_0=(Token)match(input,RULE_DECIMALREALLITERAL,FOLLOW_2); 

            		current.merge(this_DecimalRealLiteral_0);
            	

            		newLeafNode(this_DecimalRealLiteral_0, grammarAccess.getRealLiteralAccess().getDecimalRealLiteralTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRealLiteral"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000000L,0x0000001800000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000000437B0L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000002L,0x000007FFFFFFF000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000043790L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000000L,0x000007FFFFFFF000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000000477B0L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000437B0L,0x000007FFFFFFF000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000043000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000043010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000020002L});

}