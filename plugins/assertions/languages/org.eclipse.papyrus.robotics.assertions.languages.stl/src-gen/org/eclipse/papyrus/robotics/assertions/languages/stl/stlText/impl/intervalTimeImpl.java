/**
 * generated by Xtext 2.27.0
 */
package org.eclipse.papyrus.robotics.assertions.languages.stl.stlText.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.robotics.assertions.languages.stl.stlText.StlTextPackage;
import org.eclipse.papyrus.robotics.assertions.languages.stl.stlText.intervalTime;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>interval Time</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class intervalTimeImpl extends MinimalEObjectImpl.Container implements intervalTime
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected intervalTimeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return StlTextPackage.Literals.INTERVAL_TIME;
  }

} //intervalTimeImpl
