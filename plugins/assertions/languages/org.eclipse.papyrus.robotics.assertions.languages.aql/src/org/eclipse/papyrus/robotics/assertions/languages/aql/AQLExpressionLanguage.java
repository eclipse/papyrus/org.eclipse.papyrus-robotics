/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 * 	 Angel López , Tecnalia
 *   Alejandra Ruiz, Tecnalia 
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.aql;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.acceleo.query.runtime.EvaluationResult;
import org.eclipse.acceleo.query.runtime.IQueryBuilderEngine;
import org.eclipse.acceleo.query.runtime.IQueryBuilderEngine.AstResult;
import org.eclipse.acceleo.query.runtime.IQueryEnvironment;
import org.eclipse.acceleo.query.runtime.IQueryEvaluationEngine;
import org.eclipse.acceleo.query.runtime.Query;
import org.eclipse.acceleo.query.runtime.impl.QueryBuilderEngine;
import org.eclipse.acceleo.query.runtime.impl.QueryEvaluationEngine;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.assertions.languages.IExpressionLanguage;

@SuppressWarnings("restriction")
public class AQLExpressionLanguage implements IExpressionLanguage {

	@Override
	public String getName() {
		return "AQL"; //$NON-NLS-1$
	}

	@Override
	public Object evaluate(EObject context, String expression) {
		IQueryEnvironment queryEnvironment = Query.newEnvironmentWithDefaultServices(null);
		IQueryBuilderEngine builder = new QueryBuilderEngine(queryEnvironment);
		AstResult astResult = builder.build(expression);
		IQueryEvaluationEngine engine = new QueryEvaluationEngine(queryEnvironment);
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("self", context); //$NON-NLS-1$
		EvaluationResult evaluationResult = engine.eval(astResult, variables);
		return evaluationResult.getResult();
	}

	@Override
	public boolean isGlobalEvaluation() {
		return false;
	}

}
