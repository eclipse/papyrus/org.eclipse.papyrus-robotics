package org.eclipse.papyrus.robotics.assertions.languages.stl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.eclipse.papyrus.robotics.assertions.languages.stl.services.STLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSTLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_DECIMALREALLITERAL", "RULE_COLON", "RULE_COMMA", "RULE_LPAREN", "RULE_RPAREN", "RULE_ABS", "RULE_SQRT", "RULE_EXP", "RULE_POW", "RULE_MINUS", "RULE_LBRACK", "RULE_RBRACK", "RULE_UNIT", "RULE_IDENTIFIER", "RULE_INTEGERLITERAL", "RULE_SEC", "RULE_MSEC", "RULE_USEC", "RULE_NSEC", "RULE_PLUS", "RULE_TIMES", "RULE_DIVIDE", "RULE_LBRACE", "RULE_RBRACE", "RULE_SEMICOLON", "RULE_DOT", "RULE_AT", "RULE_PSEC", "RULE_ROS_TOPIC", "RULE_IMPORT", "RULE_INPUT", "RULE_OUTPUT", "RULE_INTERNAL", "RULE_CONSTANT", "RULE_DOMAINTYPEREAL", "RULE_DOMAINTYPEFLOAT", "RULE_DOMAINTYPELONG", "RULE_DOMAINTYPECOMPLEX", "RULE_DOMAINTYPEINT", "RULE_DOMAINTYPEBOOL", "RULE_DECIMALNUMERAL", "RULE_HEXNUMERAL", "RULE_BINARYNUMERAL", "RULE_NONZERODIGIT", "RULE_DIGITS", "RULE_UNDERSCORES", "RULE_DIGIT", "RULE_DIGITSANDUNDERSCORES", "RULE_DIGITORUNDERSCORE", "RULE_HEXDIGITS", "RULE_HEXDIGIT", "RULE_HEXDIGITSANDUNDERSCORES", "RULE_HEXDIGITORUNDERSCORE", "RULE_BINARYDIGITS", "RULE_BINARYDIGIT", "RULE_BINARYDIGITSANDUNDERSCORES", "RULE_BINARYDIGITORUNDERSCORE", "RULE_EXPONENTPART", "RULE_EXPONENTINDICATOR", "RULE_SIGNEDINTEGER", "RULE_SIGN", "RULE_IDENTIFIERSTART", "RULE_IDENTIFIERPART", "RULE_LETTERORUNDERSCORE", "RULE_LETTER", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'xor'", "'rise'", "'fall'", "'>='", "'<='", "'>'", "'<'", "'or'", "'|'", "'and'", "'&'", "'iff'", "'<->'", "'implies'", "'->'", "'always'", "'G'", "'eventually'", "'F'", "'historically'", "'H'", "'once'", "'O'", "'next'", "'X'", "'prev'", "'Y'", "'=='", "'='", "'!=='", "'!='", "'true'", "'TRUE'", "'false'", "'FALSE'"
    };
    public static final int RULE_EXP=11;
    public static final int RULE_MSEC=20;
    public static final int RULE_SIGN=64;
    public static final int RULE_DOMAINTYPEREAL=38;
    public static final int RULE_BINARYDIGITS=57;
    public static final int RULE_INTERNAL=36;
    public static final int RULE_DOMAINTYPEINT=42;
    public static final int RULE_UNDERSCORES=49;
    public static final int RULE_OUTPUT=35;
    public static final int RULE_NONZERODIGIT=47;
    public static final int RULE_ID=69;
    public static final int RULE_TIMES=24;
    public static final int RULE_DIGIT=50;
    public static final int RULE_COLON=5;
    public static final int RULE_LETTERORUNDERSCORE=67;
    public static final int RULE_DOMAINTYPELONG=40;
    public static final int RULE_USEC=21;
    public static final int RULE_INT=70;
    public static final int RULE_ML_COMMENT=72;
    public static final int RULE_BINARYDIGITORUNDERSCORE=60;
    public static final int RULE_IMPORT=33;
    public static final int RULE_DIGITSANDUNDERSCORES=51;
    public static final int RULE_AT=30;
    public static final int RULE_POW=12;
    public static final int RULE_DIVIDE=25;
    public static final int RULE_CONSTANT=37;
    public static final int RULE_SEC=19;
    public static final int RULE_DOMAINTYPEFLOAT=39;
    public static final int RULE_DOT=29;
    public static final int RULE_RBRACK=15;
    public static final int RULE_PSEC=31;
    public static final int RULE_LBRACE=26;
    public static final int RULE_HEXNUMERAL=45;
    public static final int RULE_HEXDIGITS=53;
    public static final int RULE_RBRACE=27;
    public static final int RULE_LETTER=68;
    public static final int RULE_EXPONENTINDICATOR=62;
    public static final int RULE_LBRACK=14;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int RULE_HEXDIGITORUNDERSCORE=56;
    public static final int RULE_LPAREN=7;
    public static final int RULE_DOMAINTYPECOMPLEX=41;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int RULE_NSEC=22;
    public static final int RULE_HEXDIGIT=54;
    public static final int RULE_COMMA=6;
    public static final int RULE_DOMAINTYPEBOOL=43;
    public static final int RULE_SQRT=10;
    public static final int RULE_DECIMALNUMERAL=44;
    public static final int RULE_BINARYNUMERAL=46;
    public static final int RULE_DIGITORUNDERSCORE=52;
    public static final int RULE_ABS=9;
    public static final int RULE_INTEGERLITERAL=18;
    public static final int RULE_IDENTIFIERPART=66;
    public static final int RULE_HEXDIGITSANDUNDERSCORES=55;
    public static final int RULE_SEMICOLON=28;
    public static final int RULE_IDENTIFIER=17;
    public static final int RULE_STRING=71;
    public static final int RULE_INPUT=34;
    public static final int RULE_DIGITS=48;
    public static final int RULE_UNIT=16;
    public static final int RULE_SL_COMMENT=73;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int RULE_PLUS=23;
    public static final int RULE_IDENTIFIERSTART=65;
    public static final int EOF=-1;
    public static final int T__76=76;
    public static final int RULE_ROS_TOPIC=32;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=74;
    public static final int RULE_SIGNEDINTEGER=63;
    public static final int RULE_ANY_OTHER=75;
    public static final int RULE_MINUS=13;
    public static final int RULE_RPAREN=8;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int RULE_BINARYDIGITSANDUNDERSCORES=59;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int RULE_BINARYDIGIT=58;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int RULE_EXPONENTPART=61;
    public static final int T__87=87;
    public static final int T__105=105;
    public static final int RULE_DECIMALREALLITERAL=4;

    // delegates
    // delegators


        public InternalSTLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSTLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSTLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSTL.g"; }


    	private STLGrammarAccess grammarAccess;

    	public void setGrammarAccess(STLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleassignments"
    // InternalSTL.g:53:1: entryRuleassignments : ruleassignments EOF ;
    public final void entryRuleassignments() throws RecognitionException {
        try {
            // InternalSTL.g:54:1: ( ruleassignments EOF )
            // InternalSTL.g:55:1: ruleassignments EOF
            {
             before(grammarAccess.getAssignmentsRule()); 
            pushFollow(FOLLOW_1);
            ruleassignments();

            state._fsp--;

             after(grammarAccess.getAssignmentsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleassignments"


    // $ANTLR start "ruleassignments"
    // InternalSTL.g:62:1: ruleassignments : ( ( rule__Assignments__AssignmentsAssignment )* ) ;
    public final void ruleassignments() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:66:2: ( ( ( rule__Assignments__AssignmentsAssignment )* ) )
            // InternalSTL.g:67:2: ( ( rule__Assignments__AssignmentsAssignment )* )
            {
            // InternalSTL.g:67:2: ( ( rule__Assignments__AssignmentsAssignment )* )
            // InternalSTL.g:68:3: ( rule__Assignments__AssignmentsAssignment )*
            {
             before(grammarAccess.getAssignmentsAccess().getAssignmentsAssignment()); 
            // InternalSTL.g:69:3: ( rule__Assignments__AssignmentsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_IDENTIFIER) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSTL.g:69:4: rule__Assignments__AssignmentsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Assignments__AssignmentsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getAssignmentsAccess().getAssignmentsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleassignments"


    // $ANTLR start "entryRuleassignment"
    // InternalSTL.g:78:1: entryRuleassignment : ruleassignment EOF ;
    public final void entryRuleassignment() throws RecognitionException {
        try {
            // InternalSTL.g:79:1: ( ruleassignment EOF )
            // InternalSTL.g:80:1: ruleassignment EOF
            {
             before(grammarAccess.getAssignmentRule()); 
            pushFollow(FOLLOW_1);
            ruleassignment();

            state._fsp--;

             after(grammarAccess.getAssignmentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleassignment"


    // $ANTLR start "ruleassignment"
    // InternalSTL.g:87:1: ruleassignment : ( ( rule__Assignment__Group__0 ) ) ;
    public final void ruleassignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:91:2: ( ( ( rule__Assignment__Group__0 ) ) )
            // InternalSTL.g:92:2: ( ( rule__Assignment__Group__0 ) )
            {
            // InternalSTL.g:92:2: ( ( rule__Assignment__Group__0 ) )
            // InternalSTL.g:93:3: ( rule__Assignment__Group__0 )
            {
             before(grammarAccess.getAssignmentAccess().getGroup()); 
            // InternalSTL.g:94:3: ( rule__Assignment__Group__0 )
            // InternalSTL.g:94:4: rule__Assignment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleassignment"


    // $ANTLR start "entryRuleexpression"
    // InternalSTL.g:103:1: entryRuleexpression : ruleexpression EOF ;
    public final void entryRuleexpression() throws RecognitionException {
        try {
            // InternalSTL.g:104:1: ( ruleexpression EOF )
            // InternalSTL.g:105:1: ruleexpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleexpression"


    // $ANTLR start "ruleexpression"
    // InternalSTL.g:112:1: ruleexpression : ( ( rule__Expression__Group__0 ) ) ;
    public final void ruleexpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:116:2: ( ( ( rule__Expression__Group__0 ) ) )
            // InternalSTL.g:117:2: ( ( rule__Expression__Group__0 ) )
            {
            // InternalSTL.g:117:2: ( ( rule__Expression__Group__0 ) )
            // InternalSTL.g:118:3: ( rule__Expression__Group__0 )
            {
             before(grammarAccess.getExpressionAccess().getGroup()); 
            // InternalSTL.g:119:3: ( rule__Expression__Group__0 )
            // InternalSTL.g:119:4: rule__Expression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Expression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleexpression"


    // $ANTLR start "entryRuleexpression_start"
    // InternalSTL.g:128:1: entryRuleexpression_start : ruleexpression_start EOF ;
    public final void entryRuleexpression_start() throws RecognitionException {
        try {
            // InternalSTL.g:129:1: ( ruleexpression_start EOF )
            // InternalSTL.g:130:1: ruleexpression_start EOF
            {
             before(grammarAccess.getExpression_startRule()); 
            pushFollow(FOLLOW_1);
            ruleexpression_start();

            state._fsp--;

             after(grammarAccess.getExpression_startRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleexpression_start"


    // $ANTLR start "ruleexpression_start"
    // InternalSTL.g:137:1: ruleexpression_start : ( ( rule__Expression_start__Alternatives ) ) ;
    public final void ruleexpression_start() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:141:2: ( ( ( rule__Expression_start__Alternatives ) ) )
            // InternalSTL.g:142:2: ( ( rule__Expression_start__Alternatives ) )
            {
            // InternalSTL.g:142:2: ( ( rule__Expression_start__Alternatives ) )
            // InternalSTL.g:143:3: ( rule__Expression_start__Alternatives )
            {
             before(grammarAccess.getExpression_startAccess().getAlternatives()); 
            // InternalSTL.g:144:3: ( rule__Expression_start__Alternatives )
            // InternalSTL.g:144:4: rule__Expression_start__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Expression_start__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExpression_startAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleexpression_start"


    // $ANTLR start "entryRuleexpression_cont"
    // InternalSTL.g:153:1: entryRuleexpression_cont : ruleexpression_cont EOF ;
    public final void entryRuleexpression_cont() throws RecognitionException {
        try {
            // InternalSTL.g:154:1: ( ruleexpression_cont EOF )
            // InternalSTL.g:155:1: ruleexpression_cont EOF
            {
             before(grammarAccess.getExpression_contRule()); 
            pushFollow(FOLLOW_1);
            ruleexpression_cont();

            state._fsp--;

             after(grammarAccess.getExpression_contRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleexpression_cont"


    // $ANTLR start "ruleexpression_cont"
    // InternalSTL.g:162:1: ruleexpression_cont : ( ( rule__Expression_cont__Alternatives ) ) ;
    public final void ruleexpression_cont() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:166:2: ( ( ( rule__Expression_cont__Alternatives ) ) )
            // InternalSTL.g:167:2: ( ( rule__Expression_cont__Alternatives ) )
            {
            // InternalSTL.g:167:2: ( ( rule__Expression_cont__Alternatives ) )
            // InternalSTL.g:168:3: ( rule__Expression_cont__Alternatives )
            {
             before(grammarAccess.getExpression_contAccess().getAlternatives()); 
            // InternalSTL.g:169:3: ( rule__Expression_cont__Alternatives )
            // InternalSTL.g:169:4: rule__Expression_cont__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleexpression_cont"


    // $ANTLR start "entryRulereal_expression"
    // InternalSTL.g:178:1: entryRulereal_expression : rulereal_expression EOF ;
    public final void entryRulereal_expression() throws RecognitionException {
        try {
            // InternalSTL.g:179:1: ( rulereal_expression EOF )
            // InternalSTL.g:180:1: rulereal_expression EOF
            {
             before(grammarAccess.getReal_expressionRule()); 
            pushFollow(FOLLOW_1);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getReal_expressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulereal_expression"


    // $ANTLR start "rulereal_expression"
    // InternalSTL.g:187:1: rulereal_expression : ( ( rule__Real_expression__Alternatives ) ) ;
    public final void rulereal_expression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:191:2: ( ( ( rule__Real_expression__Alternatives ) ) )
            // InternalSTL.g:192:2: ( ( rule__Real_expression__Alternatives ) )
            {
            // InternalSTL.g:192:2: ( ( rule__Real_expression__Alternatives ) )
            // InternalSTL.g:193:3: ( rule__Real_expression__Alternatives )
            {
             before(grammarAccess.getReal_expressionAccess().getAlternatives()); 
            // InternalSTL.g:194:3: ( rule__Real_expression__Alternatives )
            // InternalSTL.g:194:4: rule__Real_expression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getReal_expressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulereal_expression"


    // $ANTLR start "entryRulecomparisonOp"
    // InternalSTL.g:203:1: entryRulecomparisonOp : rulecomparisonOp EOF ;
    public final void entryRulecomparisonOp() throws RecognitionException {
        try {
            // InternalSTL.g:204:1: ( rulecomparisonOp EOF )
            // InternalSTL.g:205:1: rulecomparisonOp EOF
            {
             before(grammarAccess.getComparisonOpRule()); 
            pushFollow(FOLLOW_1);
            rulecomparisonOp();

            state._fsp--;

             after(grammarAccess.getComparisonOpRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulecomparisonOp"


    // $ANTLR start "rulecomparisonOp"
    // InternalSTL.g:212:1: rulecomparisonOp : ( ( rule__ComparisonOp__Alternatives ) ) ;
    public final void rulecomparisonOp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:216:2: ( ( ( rule__ComparisonOp__Alternatives ) ) )
            // InternalSTL.g:217:2: ( ( rule__ComparisonOp__Alternatives ) )
            {
            // InternalSTL.g:217:2: ( ( rule__ComparisonOp__Alternatives ) )
            // InternalSTL.g:218:3: ( rule__ComparisonOp__Alternatives )
            {
             before(grammarAccess.getComparisonOpAccess().getAlternatives()); 
            // InternalSTL.g:219:3: ( rule__ComparisonOp__Alternatives )
            // InternalSTL.g:219:4: rule__ComparisonOp__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonOp__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonOpAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulecomparisonOp"


    // $ANTLR start "entryRulenum_literal"
    // InternalSTL.g:228:1: entryRulenum_literal : rulenum_literal EOF ;
    public final void entryRulenum_literal() throws RecognitionException {
        try {
            // InternalSTL.g:229:1: ( rulenum_literal EOF )
            // InternalSTL.g:230:1: rulenum_literal EOF
            {
             before(grammarAccess.getNum_literalRule()); 
            pushFollow(FOLLOW_1);
            rulenum_literal();

            state._fsp--;

             after(grammarAccess.getNum_literalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulenum_literal"


    // $ANTLR start "rulenum_literal"
    // InternalSTL.g:237:1: rulenum_literal : ( ( rule__Num_literal__Alternatives ) ) ;
    public final void rulenum_literal() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:241:2: ( ( ( rule__Num_literal__Alternatives ) ) )
            // InternalSTL.g:242:2: ( ( rule__Num_literal__Alternatives ) )
            {
            // InternalSTL.g:242:2: ( ( rule__Num_literal__Alternatives ) )
            // InternalSTL.g:243:3: ( rule__Num_literal__Alternatives )
            {
             before(grammarAccess.getNum_literalAccess().getAlternatives()); 
            // InternalSTL.g:244:3: ( rule__Num_literal__Alternatives )
            // InternalSTL.g:244:4: rule__Num_literal__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Num_literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNum_literalAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulenum_literal"


    // $ANTLR start "entryRuleinterval"
    // InternalSTL.g:253:1: entryRuleinterval : ruleinterval EOF ;
    public final void entryRuleinterval() throws RecognitionException {
        try {
            // InternalSTL.g:254:1: ( ruleinterval EOF )
            // InternalSTL.g:255:1: ruleinterval EOF
            {
             before(grammarAccess.getIntervalRule()); 
            pushFollow(FOLLOW_1);
            ruleinterval();

            state._fsp--;

             after(grammarAccess.getIntervalRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleinterval"


    // $ANTLR start "ruleinterval"
    // InternalSTL.g:262:1: ruleinterval : ( ( rule__Interval__Group__0 ) ) ;
    public final void ruleinterval() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:266:2: ( ( ( rule__Interval__Group__0 ) ) )
            // InternalSTL.g:267:2: ( ( rule__Interval__Group__0 ) )
            {
            // InternalSTL.g:267:2: ( ( rule__Interval__Group__0 ) )
            // InternalSTL.g:268:3: ( rule__Interval__Group__0 )
            {
             before(grammarAccess.getIntervalAccess().getGroup()); 
            // InternalSTL.g:269:3: ( rule__Interval__Group__0 )
            // InternalSTL.g:269:4: rule__Interval__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Interval__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntervalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleinterval"


    // $ANTLR start "entryRuleintervalTime"
    // InternalSTL.g:278:1: entryRuleintervalTime : ruleintervalTime EOF ;
    public final void entryRuleintervalTime() throws RecognitionException {
        try {
            // InternalSTL.g:279:1: ( ruleintervalTime EOF )
            // InternalSTL.g:280:1: ruleintervalTime EOF
            {
             before(grammarAccess.getIntervalTimeRule()); 
            pushFollow(FOLLOW_1);
            ruleintervalTime();

            state._fsp--;

             after(grammarAccess.getIntervalTimeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleintervalTime"


    // $ANTLR start "ruleintervalTime"
    // InternalSTL.g:287:1: ruleintervalTime : ( ( rule__IntervalTime__Alternatives ) ) ;
    public final void ruleintervalTime() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:291:2: ( ( ( rule__IntervalTime__Alternatives ) ) )
            // InternalSTL.g:292:2: ( ( rule__IntervalTime__Alternatives ) )
            {
            // InternalSTL.g:292:2: ( ( rule__IntervalTime__Alternatives ) )
            // InternalSTL.g:293:3: ( rule__IntervalTime__Alternatives )
            {
             before(grammarAccess.getIntervalTimeAccess().getAlternatives()); 
            // InternalSTL.g:294:3: ( rule__IntervalTime__Alternatives )
            // InternalSTL.g:294:4: rule__IntervalTime__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__IntervalTime__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIntervalTimeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleintervalTime"


    // $ANTLR start "ruleOrOperator"
    // InternalSTL.g:304:1: ruleOrOperator : ( ( rule__OrOperator__Alternatives ) ) ;
    public final void ruleOrOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:308:2: ( ( ( rule__OrOperator__Alternatives ) ) )
            // InternalSTL.g:309:2: ( ( rule__OrOperator__Alternatives ) )
            {
            // InternalSTL.g:309:2: ( ( rule__OrOperator__Alternatives ) )
            // InternalSTL.g:310:3: ( rule__OrOperator__Alternatives )
            {
             before(grammarAccess.getOrOperatorAccess().getAlternatives()); 
            // InternalSTL.g:311:3: ( rule__OrOperator__Alternatives )
            // InternalSTL.g:311:4: rule__OrOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OrOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOrOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrOperator"


    // $ANTLR start "ruleAndOperator"
    // InternalSTL.g:321:1: ruleAndOperator : ( ( rule__AndOperator__Alternatives ) ) ;
    public final void ruleAndOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:325:2: ( ( ( rule__AndOperator__Alternatives ) ) )
            // InternalSTL.g:326:2: ( ( rule__AndOperator__Alternatives ) )
            {
            // InternalSTL.g:326:2: ( ( rule__AndOperator__Alternatives ) )
            // InternalSTL.g:327:3: ( rule__AndOperator__Alternatives )
            {
             before(grammarAccess.getAndOperatorAccess().getAlternatives()); 
            // InternalSTL.g:328:3: ( rule__AndOperator__Alternatives )
            // InternalSTL.g:328:4: rule__AndOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AndOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAndOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAndOperator"


    // $ANTLR start "ruleIffOperator"
    // InternalSTL.g:338:1: ruleIffOperator : ( ( rule__IffOperator__Alternatives ) ) ;
    public final void ruleIffOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:342:2: ( ( ( rule__IffOperator__Alternatives ) ) )
            // InternalSTL.g:343:2: ( ( rule__IffOperator__Alternatives ) )
            {
            // InternalSTL.g:343:2: ( ( rule__IffOperator__Alternatives ) )
            // InternalSTL.g:344:3: ( rule__IffOperator__Alternatives )
            {
             before(grammarAccess.getIffOperatorAccess().getAlternatives()); 
            // InternalSTL.g:345:3: ( rule__IffOperator__Alternatives )
            // InternalSTL.g:345:4: rule__IffOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__IffOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getIffOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIffOperator"


    // $ANTLR start "ruleImpliesOperator"
    // InternalSTL.g:355:1: ruleImpliesOperator : ( ( rule__ImpliesOperator__Alternatives ) ) ;
    public final void ruleImpliesOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:359:2: ( ( ( rule__ImpliesOperator__Alternatives ) ) )
            // InternalSTL.g:360:2: ( ( rule__ImpliesOperator__Alternatives ) )
            {
            // InternalSTL.g:360:2: ( ( rule__ImpliesOperator__Alternatives ) )
            // InternalSTL.g:361:3: ( rule__ImpliesOperator__Alternatives )
            {
             before(grammarAccess.getImpliesOperatorAccess().getAlternatives()); 
            // InternalSTL.g:362:3: ( rule__ImpliesOperator__Alternatives )
            // InternalSTL.g:362:4: rule__ImpliesOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ImpliesOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getImpliesOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImpliesOperator"


    // $ANTLR start "ruleXorOperator"
    // InternalSTL.g:372:1: ruleXorOperator : ( 'xor' ) ;
    public final void ruleXorOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:376:2: ( ( 'xor' ) )
            // InternalSTL.g:377:2: ( 'xor' )
            {
            // InternalSTL.g:377:2: ( 'xor' )
            // InternalSTL.g:378:3: 'xor'
            {
             before(grammarAccess.getXorOperatorAccess().getXorKeyword()); 
            match(input,76,FOLLOW_2); 
             after(grammarAccess.getXorOperatorAccess().getXorKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXorOperator"


    // $ANTLR start "ruleRiseOperator"
    // InternalSTL.g:389:1: ruleRiseOperator : ( 'rise' ) ;
    public final void ruleRiseOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:393:2: ( ( 'rise' ) )
            // InternalSTL.g:394:2: ( 'rise' )
            {
            // InternalSTL.g:394:2: ( 'rise' )
            // InternalSTL.g:395:3: 'rise'
            {
             before(grammarAccess.getRiseOperatorAccess().getRiseKeyword()); 
            match(input,77,FOLLOW_2); 
             after(grammarAccess.getRiseOperatorAccess().getRiseKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRiseOperator"


    // $ANTLR start "ruleFallOperator"
    // InternalSTL.g:406:1: ruleFallOperator : ( 'fall' ) ;
    public final void ruleFallOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:410:2: ( ( 'fall' ) )
            // InternalSTL.g:411:2: ( 'fall' )
            {
            // InternalSTL.g:411:2: ( 'fall' )
            // InternalSTL.g:412:3: 'fall'
            {
             before(grammarAccess.getFallOperatorAccess().getFallKeyword()); 
            match(input,78,FOLLOW_2); 
             after(grammarAccess.getFallOperatorAccess().getFallKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFallOperator"


    // $ANTLR start "ruleAlwaysOperator"
    // InternalSTL.g:423:1: ruleAlwaysOperator : ( ( rule__AlwaysOperator__Alternatives ) ) ;
    public final void ruleAlwaysOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:427:2: ( ( ( rule__AlwaysOperator__Alternatives ) ) )
            // InternalSTL.g:428:2: ( ( rule__AlwaysOperator__Alternatives ) )
            {
            // InternalSTL.g:428:2: ( ( rule__AlwaysOperator__Alternatives ) )
            // InternalSTL.g:429:3: ( rule__AlwaysOperator__Alternatives )
            {
             before(grammarAccess.getAlwaysOperatorAccess().getAlternatives()); 
            // InternalSTL.g:430:3: ( rule__AlwaysOperator__Alternatives )
            // InternalSTL.g:430:4: rule__AlwaysOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AlwaysOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAlwaysOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAlwaysOperator"


    // $ANTLR start "ruleEventuallyOperator"
    // InternalSTL.g:440:1: ruleEventuallyOperator : ( ( rule__EventuallyOperator__Alternatives ) ) ;
    public final void ruleEventuallyOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:444:2: ( ( ( rule__EventuallyOperator__Alternatives ) ) )
            // InternalSTL.g:445:2: ( ( rule__EventuallyOperator__Alternatives ) )
            {
            // InternalSTL.g:445:2: ( ( rule__EventuallyOperator__Alternatives ) )
            // InternalSTL.g:446:3: ( rule__EventuallyOperator__Alternatives )
            {
             before(grammarAccess.getEventuallyOperatorAccess().getAlternatives()); 
            // InternalSTL.g:447:3: ( rule__EventuallyOperator__Alternatives )
            // InternalSTL.g:447:4: rule__EventuallyOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EventuallyOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEventuallyOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEventuallyOperator"


    // $ANTLR start "ruleHistoricallyOperator"
    // InternalSTL.g:457:1: ruleHistoricallyOperator : ( ( rule__HistoricallyOperator__Alternatives ) ) ;
    public final void ruleHistoricallyOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:461:2: ( ( ( rule__HistoricallyOperator__Alternatives ) ) )
            // InternalSTL.g:462:2: ( ( rule__HistoricallyOperator__Alternatives ) )
            {
            // InternalSTL.g:462:2: ( ( rule__HistoricallyOperator__Alternatives ) )
            // InternalSTL.g:463:3: ( rule__HistoricallyOperator__Alternatives )
            {
             before(grammarAccess.getHistoricallyOperatorAccess().getAlternatives()); 
            // InternalSTL.g:464:3: ( rule__HistoricallyOperator__Alternatives )
            // InternalSTL.g:464:4: rule__HistoricallyOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__HistoricallyOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getHistoricallyOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHistoricallyOperator"


    // $ANTLR start "ruleOnceOperator"
    // InternalSTL.g:474:1: ruleOnceOperator : ( ( rule__OnceOperator__Alternatives ) ) ;
    public final void ruleOnceOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:478:2: ( ( ( rule__OnceOperator__Alternatives ) ) )
            // InternalSTL.g:479:2: ( ( rule__OnceOperator__Alternatives ) )
            {
            // InternalSTL.g:479:2: ( ( rule__OnceOperator__Alternatives ) )
            // InternalSTL.g:480:3: ( rule__OnceOperator__Alternatives )
            {
             before(grammarAccess.getOnceOperatorAccess().getAlternatives()); 
            // InternalSTL.g:481:3: ( rule__OnceOperator__Alternatives )
            // InternalSTL.g:481:4: rule__OnceOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OnceOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOnceOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOnceOperator"


    // $ANTLR start "ruleNextOperator"
    // InternalSTL.g:491:1: ruleNextOperator : ( ( rule__NextOperator__Alternatives ) ) ;
    public final void ruleNextOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:495:2: ( ( ( rule__NextOperator__Alternatives ) ) )
            // InternalSTL.g:496:2: ( ( rule__NextOperator__Alternatives ) )
            {
            // InternalSTL.g:496:2: ( ( rule__NextOperator__Alternatives ) )
            // InternalSTL.g:497:3: ( rule__NextOperator__Alternatives )
            {
             before(grammarAccess.getNextOperatorAccess().getAlternatives()); 
            // InternalSTL.g:498:3: ( rule__NextOperator__Alternatives )
            // InternalSTL.g:498:4: rule__NextOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__NextOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNextOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNextOperator"


    // $ANTLR start "rulePreviousOperator"
    // InternalSTL.g:508:1: rulePreviousOperator : ( ( rule__PreviousOperator__Alternatives ) ) ;
    public final void rulePreviousOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:512:2: ( ( ( rule__PreviousOperator__Alternatives ) ) )
            // InternalSTL.g:513:2: ( ( rule__PreviousOperator__Alternatives ) )
            {
            // InternalSTL.g:513:2: ( ( rule__PreviousOperator__Alternatives ) )
            // InternalSTL.g:514:3: ( rule__PreviousOperator__Alternatives )
            {
             before(grammarAccess.getPreviousOperatorAccess().getAlternatives()); 
            // InternalSTL.g:515:3: ( rule__PreviousOperator__Alternatives )
            // InternalSTL.g:515:4: rule__PreviousOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__PreviousOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPreviousOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePreviousOperator"


    // $ANTLR start "ruleEqualOperator"
    // InternalSTL.g:525:1: ruleEqualOperator : ( ( rule__EqualOperator__Alternatives ) ) ;
    public final void ruleEqualOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:529:2: ( ( ( rule__EqualOperator__Alternatives ) ) )
            // InternalSTL.g:530:2: ( ( rule__EqualOperator__Alternatives ) )
            {
            // InternalSTL.g:530:2: ( ( rule__EqualOperator__Alternatives ) )
            // InternalSTL.g:531:3: ( rule__EqualOperator__Alternatives )
            {
             before(grammarAccess.getEqualOperatorAccess().getAlternatives()); 
            // InternalSTL.g:532:3: ( rule__EqualOperator__Alternatives )
            // InternalSTL.g:532:4: rule__EqualOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EqualOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEqualOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEqualOperator"


    // $ANTLR start "ruleNotEqualOperator"
    // InternalSTL.g:542:1: ruleNotEqualOperator : ( ( rule__NotEqualOperator__Alternatives ) ) ;
    public final void ruleNotEqualOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:546:2: ( ( ( rule__NotEqualOperator__Alternatives ) ) )
            // InternalSTL.g:547:2: ( ( rule__NotEqualOperator__Alternatives ) )
            {
            // InternalSTL.g:547:2: ( ( rule__NotEqualOperator__Alternatives ) )
            // InternalSTL.g:548:3: ( rule__NotEqualOperator__Alternatives )
            {
             before(grammarAccess.getNotEqualOperatorAccess().getAlternatives()); 
            // InternalSTL.g:549:3: ( rule__NotEqualOperator__Alternatives )
            // InternalSTL.g:549:4: rule__NotEqualOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__NotEqualOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNotEqualOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNotEqualOperator"


    // $ANTLR start "ruleGreaterOrEqualOperator"
    // InternalSTL.g:559:1: ruleGreaterOrEqualOperator : ( '>=' ) ;
    public final void ruleGreaterOrEqualOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:563:2: ( ( '>=' ) )
            // InternalSTL.g:564:2: ( '>=' )
            {
            // InternalSTL.g:564:2: ( '>=' )
            // InternalSTL.g:565:3: '>='
            {
             before(grammarAccess.getGreaterOrEqualOperatorAccess().getGreaterThanSignEqualsSignKeyword()); 
            match(input,79,FOLLOW_2); 
             after(grammarAccess.getGreaterOrEqualOperatorAccess().getGreaterThanSignEqualsSignKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGreaterOrEqualOperator"


    // $ANTLR start "ruleLesserOrEqualOperator"
    // InternalSTL.g:576:1: ruleLesserOrEqualOperator : ( '<=' ) ;
    public final void ruleLesserOrEqualOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:580:2: ( ( '<=' ) )
            // InternalSTL.g:581:2: ( '<=' )
            {
            // InternalSTL.g:581:2: ( '<=' )
            // InternalSTL.g:582:3: '<='
            {
             before(grammarAccess.getLesserOrEqualOperatorAccess().getLessThanSignEqualsSignKeyword()); 
            match(input,80,FOLLOW_2); 
             after(grammarAccess.getLesserOrEqualOperatorAccess().getLessThanSignEqualsSignKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLesserOrEqualOperator"


    // $ANTLR start "ruleGreaterOperator"
    // InternalSTL.g:593:1: ruleGreaterOperator : ( '>' ) ;
    public final void ruleGreaterOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:597:2: ( ( '>' ) )
            // InternalSTL.g:598:2: ( '>' )
            {
            // InternalSTL.g:598:2: ( '>' )
            // InternalSTL.g:599:3: '>'
            {
             before(grammarAccess.getGreaterOperatorAccess().getGreaterThanSignKeyword()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getGreaterOperatorAccess().getGreaterThanSignKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGreaterOperator"


    // $ANTLR start "ruleLesserOperator"
    // InternalSTL.g:610:1: ruleLesserOperator : ( '<' ) ;
    public final void ruleLesserOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:614:2: ( ( '<' ) )
            // InternalSTL.g:615:2: ( '<' )
            {
            // InternalSTL.g:615:2: ( '<' )
            // InternalSTL.g:616:3: '<'
            {
             before(grammarAccess.getLesserOperatorAccess().getLessThanSignKeyword()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getLesserOperatorAccess().getLessThanSignKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLesserOperator"


    // $ANTLR start "ruleTRUE"
    // InternalSTL.g:627:1: ruleTRUE : ( ( rule__TRUE__Alternatives ) ) ;
    public final void ruleTRUE() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:631:2: ( ( ( rule__TRUE__Alternatives ) ) )
            // InternalSTL.g:632:2: ( ( rule__TRUE__Alternatives ) )
            {
            // InternalSTL.g:632:2: ( ( rule__TRUE__Alternatives ) )
            // InternalSTL.g:633:3: ( rule__TRUE__Alternatives )
            {
             before(grammarAccess.getTRUEAccess().getAlternatives()); 
            // InternalSTL.g:634:3: ( rule__TRUE__Alternatives )
            // InternalSTL.g:634:4: rule__TRUE__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TRUE__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTRUEAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTRUE"


    // $ANTLR start "ruleFALSE"
    // InternalSTL.g:644:1: ruleFALSE : ( ( rule__FALSE__Alternatives ) ) ;
    public final void ruleFALSE() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:648:2: ( ( ( rule__FALSE__Alternatives ) ) )
            // InternalSTL.g:649:2: ( ( rule__FALSE__Alternatives ) )
            {
            // InternalSTL.g:649:2: ( ( rule__FALSE__Alternatives ) )
            // InternalSTL.g:650:3: ( rule__FALSE__Alternatives )
            {
             before(grammarAccess.getFALSEAccess().getAlternatives()); 
            // InternalSTL.g:651:3: ( rule__FALSE__Alternatives )
            // InternalSTL.g:651:4: rule__FALSE__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__FALSE__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFALSEAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFALSE"


    // $ANTLR start "entryRuleRealLiteral"
    // InternalSTL.g:660:1: entryRuleRealLiteral : ruleRealLiteral EOF ;
    public final void entryRuleRealLiteral() throws RecognitionException {
        try {
            // InternalSTL.g:661:1: ( ruleRealLiteral EOF )
            // InternalSTL.g:662:1: ruleRealLiteral EOF
            {
             before(grammarAccess.getRealLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleRealLiteral();

            state._fsp--;

             after(grammarAccess.getRealLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRealLiteral"


    // $ANTLR start "ruleRealLiteral"
    // InternalSTL.g:669:1: ruleRealLiteral : ( RULE_DECIMALREALLITERAL ) ;
    public final void ruleRealLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:673:2: ( ( RULE_DECIMALREALLITERAL ) )
            // InternalSTL.g:674:2: ( RULE_DECIMALREALLITERAL )
            {
            // InternalSTL.g:674:2: ( RULE_DECIMALREALLITERAL )
            // InternalSTL.g:675:3: RULE_DECIMALREALLITERAL
            {
             before(grammarAccess.getRealLiteralAccess().getDecimalRealLiteralTerminalRuleCall()); 
            match(input,RULE_DECIMALREALLITERAL,FOLLOW_2); 
             after(grammarAccess.getRealLiteralAccess().getDecimalRealLiteralTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRealLiteral"


    // $ANTLR start "rule__Expression_start__Alternatives"
    // InternalSTL.g:684:1: rule__Expression_start__Alternatives : ( ( ( rule__Expression_start__RAssignment_0 ) ) | ( ( rule__Expression_start__Group_1__0 ) ) );
    public final void rule__Expression_start__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:688:1: ( ( ( rule__Expression_start__RAssignment_0 ) ) | ( ( rule__Expression_start__Group_1__0 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_DECIMALREALLITERAL||(LA2_0>=RULE_ABS && LA2_0<=RULE_MINUS)||(LA2_0>=RULE_IDENTIFIER && LA2_0<=RULE_INTEGERLITERAL)) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_LPAREN) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalSTL.g:689:2: ( ( rule__Expression_start__RAssignment_0 ) )
                    {
                    // InternalSTL.g:689:2: ( ( rule__Expression_start__RAssignment_0 ) )
                    // InternalSTL.g:690:3: ( rule__Expression_start__RAssignment_0 )
                    {
                     before(grammarAccess.getExpression_startAccess().getRAssignment_0()); 
                    // InternalSTL.g:691:3: ( rule__Expression_start__RAssignment_0 )
                    // InternalSTL.g:691:4: rule__Expression_start__RAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_start__RAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_startAccess().getRAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:695:2: ( ( rule__Expression_start__Group_1__0 ) )
                    {
                    // InternalSTL.g:695:2: ( ( rule__Expression_start__Group_1__0 ) )
                    // InternalSTL.g:696:3: ( rule__Expression_start__Group_1__0 )
                    {
                     before(grammarAccess.getExpression_startAccess().getGroup_1()); 
                    // InternalSTL.g:697:3: ( rule__Expression_start__Group_1__0 )
                    // InternalSTL.g:697:4: rule__Expression_start__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_start__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_startAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Alternatives"


    // $ANTLR start "rule__Expression_cont__Alternatives"
    // InternalSTL.g:705:1: rule__Expression_cont__Alternatives : ( ( ( rule__Expression_cont__Group_0__0 ) ) | ( ( rule__Expression_cont__Group_1__0 ) ) | ( ( rule__Expression_cont__Group_2__0 ) ) | ( ( rule__Expression_cont__Group_3__0 ) ) | ( ( rule__Expression_cont__Group_4__0 ) ) | ( ( rule__Expression_cont__Group_5__0 ) ) | ( ( rule__Expression_cont__Group_6__0 ) ) | ( ( rule__Expression_cont__Group_7__0 ) ) | ( ( rule__Expression_cont__Group_8__0 ) ) | ( ( rule__Expression_cont__Group_9__0 ) ) | ( ( rule__Expression_cont__Group_10__0 ) ) | ( ( rule__Expression_cont__Group_11__0 ) ) | ( ( rule__Expression_cont__Group_12__0 ) ) | ( ( rule__Expression_cont__Group_13__0 ) ) );
    public final void rule__Expression_cont__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:709:1: ( ( ( rule__Expression_cont__Group_0__0 ) ) | ( ( rule__Expression_cont__Group_1__0 ) ) | ( ( rule__Expression_cont__Group_2__0 ) ) | ( ( rule__Expression_cont__Group_3__0 ) ) | ( ( rule__Expression_cont__Group_4__0 ) ) | ( ( rule__Expression_cont__Group_5__0 ) ) | ( ( rule__Expression_cont__Group_6__0 ) ) | ( ( rule__Expression_cont__Group_7__0 ) ) | ( ( rule__Expression_cont__Group_8__0 ) ) | ( ( rule__Expression_cont__Group_9__0 ) ) | ( ( rule__Expression_cont__Group_10__0 ) ) | ( ( rule__Expression_cont__Group_11__0 ) ) | ( ( rule__Expression_cont__Group_12__0 ) ) | ( ( rule__Expression_cont__Group_13__0 ) ) )
            int alt3=14;
            switch ( input.LA(1) ) {
            case 91:
            case 92:
                {
                alt3=1;
                }
                break;
            case 93:
            case 94:
                {
                alt3=2;
                }
                break;
            case 95:
            case 96:
                {
                alt3=3;
                }
                break;
            case 97:
            case 98:
                {
                alt3=4;
                }
                break;
            case 77:
                {
                alt3=5;
                }
                break;
            case 78:
                {
                alt3=6;
                }
                break;
            case 101:
            case 102:
                {
                alt3=7;
                }
                break;
            case 99:
            case 100:
                {
                alt3=8;
                }
                break;
            case 83:
            case 84:
                {
                alt3=9;
                }
                break;
            case 85:
            case 86:
                {
                alt3=10;
                }
                break;
            case 89:
            case 90:
                {
                alt3=11;
                }
                break;
            case 87:
            case 88:
                {
                alt3=12;
                }
                break;
            case 76:
                {
                alt3=13;
                }
                break;
            case 79:
            case 80:
            case 81:
            case 82:
            case 103:
            case 104:
            case 105:
            case 106:
                {
                alt3=14;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalSTL.g:710:2: ( ( rule__Expression_cont__Group_0__0 ) )
                    {
                    // InternalSTL.g:710:2: ( ( rule__Expression_cont__Group_0__0 ) )
                    // InternalSTL.g:711:3: ( rule__Expression_cont__Group_0__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_0()); 
                    // InternalSTL.g:712:3: ( rule__Expression_cont__Group_0__0 )
                    // InternalSTL.g:712:4: rule__Expression_cont__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:716:2: ( ( rule__Expression_cont__Group_1__0 ) )
                    {
                    // InternalSTL.g:716:2: ( ( rule__Expression_cont__Group_1__0 ) )
                    // InternalSTL.g:717:3: ( rule__Expression_cont__Group_1__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_1()); 
                    // InternalSTL.g:718:3: ( rule__Expression_cont__Group_1__0 )
                    // InternalSTL.g:718:4: rule__Expression_cont__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:722:2: ( ( rule__Expression_cont__Group_2__0 ) )
                    {
                    // InternalSTL.g:722:2: ( ( rule__Expression_cont__Group_2__0 ) )
                    // InternalSTL.g:723:3: ( rule__Expression_cont__Group_2__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_2()); 
                    // InternalSTL.g:724:3: ( rule__Expression_cont__Group_2__0 )
                    // InternalSTL.g:724:4: rule__Expression_cont__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSTL.g:728:2: ( ( rule__Expression_cont__Group_3__0 ) )
                    {
                    // InternalSTL.g:728:2: ( ( rule__Expression_cont__Group_3__0 ) )
                    // InternalSTL.g:729:3: ( rule__Expression_cont__Group_3__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_3()); 
                    // InternalSTL.g:730:3: ( rule__Expression_cont__Group_3__0 )
                    // InternalSTL.g:730:4: rule__Expression_cont__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalSTL.g:734:2: ( ( rule__Expression_cont__Group_4__0 ) )
                    {
                    // InternalSTL.g:734:2: ( ( rule__Expression_cont__Group_4__0 ) )
                    // InternalSTL.g:735:3: ( rule__Expression_cont__Group_4__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_4()); 
                    // InternalSTL.g:736:3: ( rule__Expression_cont__Group_4__0 )
                    // InternalSTL.g:736:4: rule__Expression_cont__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalSTL.g:740:2: ( ( rule__Expression_cont__Group_5__0 ) )
                    {
                    // InternalSTL.g:740:2: ( ( rule__Expression_cont__Group_5__0 ) )
                    // InternalSTL.g:741:3: ( rule__Expression_cont__Group_5__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_5()); 
                    // InternalSTL.g:742:3: ( rule__Expression_cont__Group_5__0 )
                    // InternalSTL.g:742:4: rule__Expression_cont__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalSTL.g:746:2: ( ( rule__Expression_cont__Group_6__0 ) )
                    {
                    // InternalSTL.g:746:2: ( ( rule__Expression_cont__Group_6__0 ) )
                    // InternalSTL.g:747:3: ( rule__Expression_cont__Group_6__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_6()); 
                    // InternalSTL.g:748:3: ( rule__Expression_cont__Group_6__0 )
                    // InternalSTL.g:748:4: rule__Expression_cont__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_6__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalSTL.g:752:2: ( ( rule__Expression_cont__Group_7__0 ) )
                    {
                    // InternalSTL.g:752:2: ( ( rule__Expression_cont__Group_7__0 ) )
                    // InternalSTL.g:753:3: ( rule__Expression_cont__Group_7__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_7()); 
                    // InternalSTL.g:754:3: ( rule__Expression_cont__Group_7__0 )
                    // InternalSTL.g:754:4: rule__Expression_cont__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_7__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalSTL.g:758:2: ( ( rule__Expression_cont__Group_8__0 ) )
                    {
                    // InternalSTL.g:758:2: ( ( rule__Expression_cont__Group_8__0 ) )
                    // InternalSTL.g:759:3: ( rule__Expression_cont__Group_8__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_8()); 
                    // InternalSTL.g:760:3: ( rule__Expression_cont__Group_8__0 )
                    // InternalSTL.g:760:4: rule__Expression_cont__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_8__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalSTL.g:764:2: ( ( rule__Expression_cont__Group_9__0 ) )
                    {
                    // InternalSTL.g:764:2: ( ( rule__Expression_cont__Group_9__0 ) )
                    // InternalSTL.g:765:3: ( rule__Expression_cont__Group_9__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_9()); 
                    // InternalSTL.g:766:3: ( rule__Expression_cont__Group_9__0 )
                    // InternalSTL.g:766:4: rule__Expression_cont__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_9__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalSTL.g:770:2: ( ( rule__Expression_cont__Group_10__0 ) )
                    {
                    // InternalSTL.g:770:2: ( ( rule__Expression_cont__Group_10__0 ) )
                    // InternalSTL.g:771:3: ( rule__Expression_cont__Group_10__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_10()); 
                    // InternalSTL.g:772:3: ( rule__Expression_cont__Group_10__0 )
                    // InternalSTL.g:772:4: rule__Expression_cont__Group_10__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_10__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_10()); 

                    }


                    }
                    break;
                case 12 :
                    // InternalSTL.g:776:2: ( ( rule__Expression_cont__Group_11__0 ) )
                    {
                    // InternalSTL.g:776:2: ( ( rule__Expression_cont__Group_11__0 ) )
                    // InternalSTL.g:777:3: ( rule__Expression_cont__Group_11__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_11()); 
                    // InternalSTL.g:778:3: ( rule__Expression_cont__Group_11__0 )
                    // InternalSTL.g:778:4: rule__Expression_cont__Group_11__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_11__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_11()); 

                    }


                    }
                    break;
                case 13 :
                    // InternalSTL.g:782:2: ( ( rule__Expression_cont__Group_12__0 ) )
                    {
                    // InternalSTL.g:782:2: ( ( rule__Expression_cont__Group_12__0 ) )
                    // InternalSTL.g:783:3: ( rule__Expression_cont__Group_12__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_12()); 
                    // InternalSTL.g:784:3: ( rule__Expression_cont__Group_12__0 )
                    // InternalSTL.g:784:4: rule__Expression_cont__Group_12__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_12__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_12()); 

                    }


                    }
                    break;
                case 14 :
                    // InternalSTL.g:788:2: ( ( rule__Expression_cont__Group_13__0 ) )
                    {
                    // InternalSTL.g:788:2: ( ( rule__Expression_cont__Group_13__0 ) )
                    // InternalSTL.g:789:3: ( rule__Expression_cont__Group_13__0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getGroup_13()); 
                    // InternalSTL.g:790:3: ( rule__Expression_cont__Group_13__0 )
                    // InternalSTL.g:790:4: rule__Expression_cont__Group_13__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__Group_13__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getGroup_13()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Alternatives"


    // $ANTLR start "rule__Expression_cont__Alternatives_10_1"
    // InternalSTL.g:798:1: rule__Expression_cont__Alternatives_10_1 : ( ( ( rule__Expression_cont__EAssignment_10_1_0 ) ) | ( ( rule__Expression_cont__EAssignment_10_1_1 ) ) );
    public final void rule__Expression_cont__Alternatives_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:802:1: ( ( ( rule__Expression_cont__EAssignment_10_1_0 ) ) | ( ( rule__Expression_cont__EAssignment_10_1_1 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_DECIMALREALLITERAL||LA4_0==RULE_LPAREN||(LA4_0>=RULE_ABS && LA4_0<=RULE_MINUS)||(LA4_0>=RULE_IDENTIFIER && LA4_0<=RULE_INTEGERLITERAL)) ) {
                alt4=1;
            }
            else if ( ((LA4_0>=76 && LA4_0<=106)) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalSTL.g:803:2: ( ( rule__Expression_cont__EAssignment_10_1_0 ) )
                    {
                    // InternalSTL.g:803:2: ( ( rule__Expression_cont__EAssignment_10_1_0 ) )
                    // InternalSTL.g:804:3: ( rule__Expression_cont__EAssignment_10_1_0 )
                    {
                     before(grammarAccess.getExpression_contAccess().getEAssignment_10_1_0()); 
                    // InternalSTL.g:805:3: ( rule__Expression_cont__EAssignment_10_1_0 )
                    // InternalSTL.g:805:4: rule__Expression_cont__EAssignment_10_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__EAssignment_10_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getEAssignment_10_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:809:2: ( ( rule__Expression_cont__EAssignment_10_1_1 ) )
                    {
                    // InternalSTL.g:809:2: ( ( rule__Expression_cont__EAssignment_10_1_1 ) )
                    // InternalSTL.g:810:3: ( rule__Expression_cont__EAssignment_10_1_1 )
                    {
                     before(grammarAccess.getExpression_contAccess().getEAssignment_10_1_1()); 
                    // InternalSTL.g:811:3: ( rule__Expression_cont__EAssignment_10_1_1 )
                    // InternalSTL.g:811:4: rule__Expression_cont__EAssignment_10_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__EAssignment_10_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getExpression_contAccess().getEAssignment_10_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Alternatives_10_1"


    // $ANTLR start "rule__Real_expression__Alternatives"
    // InternalSTL.g:819:1: rule__Real_expression__Alternatives : ( ( ( rule__Real_expression__IdAssignment_0 ) ) | ( ( rule__Real_expression__LitAssignment_1 ) ) | ( ( rule__Real_expression__Group_2__0 ) ) | ( ( rule__Real_expression__Group_3__0 ) ) | ( ( rule__Real_expression__Group_4__0 ) ) | ( ( rule__Real_expression__Group_5__0 ) ) );
    public final void rule__Real_expression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:823:1: ( ( ( rule__Real_expression__IdAssignment_0 ) ) | ( ( rule__Real_expression__LitAssignment_1 ) ) | ( ( rule__Real_expression__Group_2__0 ) ) | ( ( rule__Real_expression__Group_3__0 ) ) | ( ( rule__Real_expression__Group_4__0 ) ) | ( ( rule__Real_expression__Group_5__0 ) ) )
            int alt5=6;
            switch ( input.LA(1) ) {
            case RULE_IDENTIFIER:
                {
                alt5=1;
                }
                break;
            case RULE_DECIMALREALLITERAL:
            case RULE_MINUS:
            case RULE_INTEGERLITERAL:
                {
                alt5=2;
                }
                break;
            case RULE_ABS:
                {
                alt5=3;
                }
                break;
            case RULE_SQRT:
                {
                alt5=4;
                }
                break;
            case RULE_EXP:
                {
                alt5=5;
                }
                break;
            case RULE_POW:
                {
                alt5=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalSTL.g:824:2: ( ( rule__Real_expression__IdAssignment_0 ) )
                    {
                    // InternalSTL.g:824:2: ( ( rule__Real_expression__IdAssignment_0 ) )
                    // InternalSTL.g:825:3: ( rule__Real_expression__IdAssignment_0 )
                    {
                     before(grammarAccess.getReal_expressionAccess().getIdAssignment_0()); 
                    // InternalSTL.g:826:3: ( rule__Real_expression__IdAssignment_0 )
                    // InternalSTL.g:826:4: rule__Real_expression__IdAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_expression__IdAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getReal_expressionAccess().getIdAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:830:2: ( ( rule__Real_expression__LitAssignment_1 ) )
                    {
                    // InternalSTL.g:830:2: ( ( rule__Real_expression__LitAssignment_1 ) )
                    // InternalSTL.g:831:3: ( rule__Real_expression__LitAssignment_1 )
                    {
                     before(grammarAccess.getReal_expressionAccess().getLitAssignment_1()); 
                    // InternalSTL.g:832:3: ( rule__Real_expression__LitAssignment_1 )
                    // InternalSTL.g:832:4: rule__Real_expression__LitAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_expression__LitAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getReal_expressionAccess().getLitAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:836:2: ( ( rule__Real_expression__Group_2__0 ) )
                    {
                    // InternalSTL.g:836:2: ( ( rule__Real_expression__Group_2__0 ) )
                    // InternalSTL.g:837:3: ( rule__Real_expression__Group_2__0 )
                    {
                     before(grammarAccess.getReal_expressionAccess().getGroup_2()); 
                    // InternalSTL.g:838:3: ( rule__Real_expression__Group_2__0 )
                    // InternalSTL.g:838:4: rule__Real_expression__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_expression__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getReal_expressionAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSTL.g:842:2: ( ( rule__Real_expression__Group_3__0 ) )
                    {
                    // InternalSTL.g:842:2: ( ( rule__Real_expression__Group_3__0 ) )
                    // InternalSTL.g:843:3: ( rule__Real_expression__Group_3__0 )
                    {
                     before(grammarAccess.getReal_expressionAccess().getGroup_3()); 
                    // InternalSTL.g:844:3: ( rule__Real_expression__Group_3__0 )
                    // InternalSTL.g:844:4: rule__Real_expression__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_expression__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getReal_expressionAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalSTL.g:848:2: ( ( rule__Real_expression__Group_4__0 ) )
                    {
                    // InternalSTL.g:848:2: ( ( rule__Real_expression__Group_4__0 ) )
                    // InternalSTL.g:849:3: ( rule__Real_expression__Group_4__0 )
                    {
                     before(grammarAccess.getReal_expressionAccess().getGroup_4()); 
                    // InternalSTL.g:850:3: ( rule__Real_expression__Group_4__0 )
                    // InternalSTL.g:850:4: rule__Real_expression__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_expression__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getReal_expressionAccess().getGroup_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalSTL.g:854:2: ( ( rule__Real_expression__Group_5__0 ) )
                    {
                    // InternalSTL.g:854:2: ( ( rule__Real_expression__Group_5__0 ) )
                    // InternalSTL.g:855:3: ( rule__Real_expression__Group_5__0 )
                    {
                     before(grammarAccess.getReal_expressionAccess().getGroup_5()); 
                    // InternalSTL.g:856:3: ( rule__Real_expression__Group_5__0 )
                    // InternalSTL.g:856:4: rule__Real_expression__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Real_expression__Group_5__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getReal_expressionAccess().getGroup_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Alternatives"


    // $ANTLR start "rule__ComparisonOp__Alternatives"
    // InternalSTL.g:864:1: rule__ComparisonOp__Alternatives : ( ( ruleLesserOrEqualOperator ) | ( ruleGreaterOrEqualOperator ) | ( ruleLesserOperator ) | ( ruleGreaterOperator ) | ( ruleEqualOperator ) | ( ruleNotEqualOperator ) );
    public final void rule__ComparisonOp__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:868:1: ( ( ruleLesserOrEqualOperator ) | ( ruleGreaterOrEqualOperator ) | ( ruleLesserOperator ) | ( ruleGreaterOperator ) | ( ruleEqualOperator ) | ( ruleNotEqualOperator ) )
            int alt6=6;
            switch ( input.LA(1) ) {
            case 80:
                {
                alt6=1;
                }
                break;
            case 79:
                {
                alt6=2;
                }
                break;
            case 82:
                {
                alt6=3;
                }
                break;
            case 81:
                {
                alt6=4;
                }
                break;
            case 103:
            case 104:
                {
                alt6=5;
                }
                break;
            case 105:
            case 106:
                {
                alt6=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalSTL.g:869:2: ( ruleLesserOrEqualOperator )
                    {
                    // InternalSTL.g:869:2: ( ruleLesserOrEqualOperator )
                    // InternalSTL.g:870:3: ruleLesserOrEqualOperator
                    {
                     before(grammarAccess.getComparisonOpAccess().getLesserOrEqualOperatorParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLesserOrEqualOperator();

                    state._fsp--;

                     after(grammarAccess.getComparisonOpAccess().getLesserOrEqualOperatorParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:875:2: ( ruleGreaterOrEqualOperator )
                    {
                    // InternalSTL.g:875:2: ( ruleGreaterOrEqualOperator )
                    // InternalSTL.g:876:3: ruleGreaterOrEqualOperator
                    {
                     before(grammarAccess.getComparisonOpAccess().getGreaterOrEqualOperatorParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleGreaterOrEqualOperator();

                    state._fsp--;

                     after(grammarAccess.getComparisonOpAccess().getGreaterOrEqualOperatorParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:881:2: ( ruleLesserOperator )
                    {
                    // InternalSTL.g:881:2: ( ruleLesserOperator )
                    // InternalSTL.g:882:3: ruleLesserOperator
                    {
                     before(grammarAccess.getComparisonOpAccess().getLesserOperatorParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleLesserOperator();

                    state._fsp--;

                     after(grammarAccess.getComparisonOpAccess().getLesserOperatorParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalSTL.g:887:2: ( ruleGreaterOperator )
                    {
                    // InternalSTL.g:887:2: ( ruleGreaterOperator )
                    // InternalSTL.g:888:3: ruleGreaterOperator
                    {
                     before(grammarAccess.getComparisonOpAccess().getGreaterOperatorParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleGreaterOperator();

                    state._fsp--;

                     after(grammarAccess.getComparisonOpAccess().getGreaterOperatorParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalSTL.g:893:2: ( ruleEqualOperator )
                    {
                    // InternalSTL.g:893:2: ( ruleEqualOperator )
                    // InternalSTL.g:894:3: ruleEqualOperator
                    {
                     before(grammarAccess.getComparisonOpAccess().getEqualOperatorParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleEqualOperator();

                    state._fsp--;

                     after(grammarAccess.getComparisonOpAccess().getEqualOperatorParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalSTL.g:899:2: ( ruleNotEqualOperator )
                    {
                    // InternalSTL.g:899:2: ( ruleNotEqualOperator )
                    // InternalSTL.g:900:3: ruleNotEqualOperator
                    {
                     before(grammarAccess.getComparisonOpAccess().getNotEqualOperatorParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleNotEqualOperator();

                    state._fsp--;

                     after(grammarAccess.getComparisonOpAccess().getNotEqualOperatorParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonOp__Alternatives"


    // $ANTLR start "rule__Num_literal__Alternatives"
    // InternalSTL.g:909:1: rule__Num_literal__Alternatives : ( ( ( rule__Num_literal__IlitAssignment_0 ) ) | ( ( rule__Num_literal__RlitAssignment_1 ) ) | ( ( rule__Num_literal__Group_2__0 ) ) );
    public final void rule__Num_literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:913:1: ( ( ( rule__Num_literal__IlitAssignment_0 ) ) | ( ( rule__Num_literal__RlitAssignment_1 ) ) | ( ( rule__Num_literal__Group_2__0 ) ) )
            int alt7=3;
            switch ( input.LA(1) ) {
            case RULE_INTEGERLITERAL:
                {
                alt7=1;
                }
                break;
            case RULE_DECIMALREALLITERAL:
                {
                alt7=2;
                }
                break;
            case RULE_MINUS:
                {
                alt7=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalSTL.g:914:2: ( ( rule__Num_literal__IlitAssignment_0 ) )
                    {
                    // InternalSTL.g:914:2: ( ( rule__Num_literal__IlitAssignment_0 ) )
                    // InternalSTL.g:915:3: ( rule__Num_literal__IlitAssignment_0 )
                    {
                     before(grammarAccess.getNum_literalAccess().getIlitAssignment_0()); 
                    // InternalSTL.g:916:3: ( rule__Num_literal__IlitAssignment_0 )
                    // InternalSTL.g:916:4: rule__Num_literal__IlitAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Num_literal__IlitAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNum_literalAccess().getIlitAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:920:2: ( ( rule__Num_literal__RlitAssignment_1 ) )
                    {
                    // InternalSTL.g:920:2: ( ( rule__Num_literal__RlitAssignment_1 ) )
                    // InternalSTL.g:921:3: ( rule__Num_literal__RlitAssignment_1 )
                    {
                     before(grammarAccess.getNum_literalAccess().getRlitAssignment_1()); 
                    // InternalSTL.g:922:3: ( rule__Num_literal__RlitAssignment_1 )
                    // InternalSTL.g:922:4: rule__Num_literal__RlitAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Num_literal__RlitAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getNum_literalAccess().getRlitAssignment_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalSTL.g:926:2: ( ( rule__Num_literal__Group_2__0 ) )
                    {
                    // InternalSTL.g:926:2: ( ( rule__Num_literal__Group_2__0 ) )
                    // InternalSTL.g:927:3: ( rule__Num_literal__Group_2__0 )
                    {
                     before(grammarAccess.getNum_literalAccess().getGroup_2()); 
                    // InternalSTL.g:928:3: ( rule__Num_literal__Group_2__0 )
                    // InternalSTL.g:928:4: rule__Num_literal__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Num_literal__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getNum_literalAccess().getGroup_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__Alternatives"


    // $ANTLR start "rule__Interval__Alternatives_2"
    // InternalSTL.g:936:1: rule__Interval__Alternatives_2 : ( ( RULE_COLON ) | ( RULE_COMMA ) );
    public final void rule__Interval__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:940:1: ( ( RULE_COLON ) | ( RULE_COMMA ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_COLON) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_COMMA) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalSTL.g:941:2: ( RULE_COLON )
                    {
                    // InternalSTL.g:941:2: ( RULE_COLON )
                    // InternalSTL.g:942:3: RULE_COLON
                    {
                     before(grammarAccess.getIntervalAccess().getCOLONTerminalRuleCall_2_0()); 
                    match(input,RULE_COLON,FOLLOW_2); 
                     after(grammarAccess.getIntervalAccess().getCOLONTerminalRuleCall_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:947:2: ( RULE_COMMA )
                    {
                    // InternalSTL.g:947:2: ( RULE_COMMA )
                    // InternalSTL.g:948:3: RULE_COMMA
                    {
                     before(grammarAccess.getIntervalAccess().getCOMMATerminalRuleCall_2_1()); 
                    match(input,RULE_COMMA,FOLLOW_2); 
                     after(grammarAccess.getIntervalAccess().getCOMMATerminalRuleCall_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Alternatives_2"


    // $ANTLR start "rule__IntervalTime__Alternatives"
    // InternalSTL.g:957:1: rule__IntervalTime__Alternatives : ( ( ( rule__IntervalTime__Group_0__0 ) ) | ( ( rule__IntervalTime__Group_1__0 ) ) );
    public final void rule__IntervalTime__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:961:1: ( ( ( rule__IntervalTime__Group_0__0 ) ) | ( ( rule__IntervalTime__Group_1__0 ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_DECIMALREALLITERAL||LA9_0==RULE_MINUS||LA9_0==RULE_INTEGERLITERAL) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_IDENTIFIER) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalSTL.g:962:2: ( ( rule__IntervalTime__Group_0__0 ) )
                    {
                    // InternalSTL.g:962:2: ( ( rule__IntervalTime__Group_0__0 ) )
                    // InternalSTL.g:963:3: ( rule__IntervalTime__Group_0__0 )
                    {
                     before(grammarAccess.getIntervalTimeAccess().getGroup_0()); 
                    // InternalSTL.g:964:3: ( rule__IntervalTime__Group_0__0 )
                    // InternalSTL.g:964:4: rule__IntervalTime__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__IntervalTime__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getIntervalTimeAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:968:2: ( ( rule__IntervalTime__Group_1__0 ) )
                    {
                    // InternalSTL.g:968:2: ( ( rule__IntervalTime__Group_1__0 ) )
                    // InternalSTL.g:969:3: ( rule__IntervalTime__Group_1__0 )
                    {
                     before(grammarAccess.getIntervalTimeAccess().getGroup_1()); 
                    // InternalSTL.g:970:3: ( rule__IntervalTime__Group_1__0 )
                    // InternalSTL.g:970:4: rule__IntervalTime__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__IntervalTime__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getIntervalTimeAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Alternatives"


    // $ANTLR start "rule__OrOperator__Alternatives"
    // InternalSTL.g:978:1: rule__OrOperator__Alternatives : ( ( 'or' ) | ( '|' ) );
    public final void rule__OrOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:982:1: ( ( 'or' ) | ( '|' ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==83) ) {
                alt10=1;
            }
            else if ( (LA10_0==84) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalSTL.g:983:2: ( 'or' )
                    {
                    // InternalSTL.g:983:2: ( 'or' )
                    // InternalSTL.g:984:3: 'or'
                    {
                     before(grammarAccess.getOrOperatorAccess().getOrKeyword_0()); 
                    match(input,83,FOLLOW_2); 
                     after(grammarAccess.getOrOperatorAccess().getOrKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:989:2: ( '|' )
                    {
                    // InternalSTL.g:989:2: ( '|' )
                    // InternalSTL.g:990:3: '|'
                    {
                     before(grammarAccess.getOrOperatorAccess().getVerticalLineKeyword_1()); 
                    match(input,84,FOLLOW_2); 
                     after(grammarAccess.getOrOperatorAccess().getVerticalLineKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrOperator__Alternatives"


    // $ANTLR start "rule__AndOperator__Alternatives"
    // InternalSTL.g:999:1: rule__AndOperator__Alternatives : ( ( 'and' ) | ( '&' ) );
    public final void rule__AndOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1003:1: ( ( 'and' ) | ( '&' ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==85) ) {
                alt11=1;
            }
            else if ( (LA11_0==86) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalSTL.g:1004:2: ( 'and' )
                    {
                    // InternalSTL.g:1004:2: ( 'and' )
                    // InternalSTL.g:1005:3: 'and'
                    {
                     before(grammarAccess.getAndOperatorAccess().getAndKeyword_0()); 
                    match(input,85,FOLLOW_2); 
                     after(grammarAccess.getAndOperatorAccess().getAndKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1010:2: ( '&' )
                    {
                    // InternalSTL.g:1010:2: ( '&' )
                    // InternalSTL.g:1011:3: '&'
                    {
                     before(grammarAccess.getAndOperatorAccess().getAmpersandKeyword_1()); 
                    match(input,86,FOLLOW_2); 
                     after(grammarAccess.getAndOperatorAccess().getAmpersandKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AndOperator__Alternatives"


    // $ANTLR start "rule__IffOperator__Alternatives"
    // InternalSTL.g:1020:1: rule__IffOperator__Alternatives : ( ( 'iff' ) | ( '<->' ) );
    public final void rule__IffOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1024:1: ( ( 'iff' ) | ( '<->' ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==87) ) {
                alt12=1;
            }
            else if ( (LA12_0==88) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalSTL.g:1025:2: ( 'iff' )
                    {
                    // InternalSTL.g:1025:2: ( 'iff' )
                    // InternalSTL.g:1026:3: 'iff'
                    {
                     before(grammarAccess.getIffOperatorAccess().getIffKeyword_0()); 
                    match(input,87,FOLLOW_2); 
                     after(grammarAccess.getIffOperatorAccess().getIffKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1031:2: ( '<->' )
                    {
                    // InternalSTL.g:1031:2: ( '<->' )
                    // InternalSTL.g:1032:3: '<->'
                    {
                     before(grammarAccess.getIffOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1()); 
                    match(input,88,FOLLOW_2); 
                     after(grammarAccess.getIffOperatorAccess().getLessThanSignHyphenMinusGreaterThanSignKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IffOperator__Alternatives"


    // $ANTLR start "rule__ImpliesOperator__Alternatives"
    // InternalSTL.g:1041:1: rule__ImpliesOperator__Alternatives : ( ( 'implies' ) | ( '->' ) );
    public final void rule__ImpliesOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1045:1: ( ( 'implies' ) | ( '->' ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==89) ) {
                alt13=1;
            }
            else if ( (LA13_0==90) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalSTL.g:1046:2: ( 'implies' )
                    {
                    // InternalSTL.g:1046:2: ( 'implies' )
                    // InternalSTL.g:1047:3: 'implies'
                    {
                     before(grammarAccess.getImpliesOperatorAccess().getImpliesKeyword_0()); 
                    match(input,89,FOLLOW_2); 
                     after(grammarAccess.getImpliesOperatorAccess().getImpliesKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1052:2: ( '->' )
                    {
                    // InternalSTL.g:1052:2: ( '->' )
                    // InternalSTL.g:1053:3: '->'
                    {
                     before(grammarAccess.getImpliesOperatorAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
                    match(input,90,FOLLOW_2); 
                     after(grammarAccess.getImpliesOperatorAccess().getHyphenMinusGreaterThanSignKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImpliesOperator__Alternatives"


    // $ANTLR start "rule__AlwaysOperator__Alternatives"
    // InternalSTL.g:1062:1: rule__AlwaysOperator__Alternatives : ( ( 'always' ) | ( 'G' ) );
    public final void rule__AlwaysOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1066:1: ( ( 'always' ) | ( 'G' ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==91) ) {
                alt14=1;
            }
            else if ( (LA14_0==92) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalSTL.g:1067:2: ( 'always' )
                    {
                    // InternalSTL.g:1067:2: ( 'always' )
                    // InternalSTL.g:1068:3: 'always'
                    {
                     before(grammarAccess.getAlwaysOperatorAccess().getAlwaysKeyword_0()); 
                    match(input,91,FOLLOW_2); 
                     after(grammarAccess.getAlwaysOperatorAccess().getAlwaysKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1073:2: ( 'G' )
                    {
                    // InternalSTL.g:1073:2: ( 'G' )
                    // InternalSTL.g:1074:3: 'G'
                    {
                     before(grammarAccess.getAlwaysOperatorAccess().getGKeyword_1()); 
                    match(input,92,FOLLOW_2); 
                     after(grammarAccess.getAlwaysOperatorAccess().getGKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AlwaysOperator__Alternatives"


    // $ANTLR start "rule__EventuallyOperator__Alternatives"
    // InternalSTL.g:1083:1: rule__EventuallyOperator__Alternatives : ( ( 'eventually' ) | ( 'F' ) );
    public final void rule__EventuallyOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1087:1: ( ( 'eventually' ) | ( 'F' ) )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==93) ) {
                alt15=1;
            }
            else if ( (LA15_0==94) ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalSTL.g:1088:2: ( 'eventually' )
                    {
                    // InternalSTL.g:1088:2: ( 'eventually' )
                    // InternalSTL.g:1089:3: 'eventually'
                    {
                     before(grammarAccess.getEventuallyOperatorAccess().getEventuallyKeyword_0()); 
                    match(input,93,FOLLOW_2); 
                     after(grammarAccess.getEventuallyOperatorAccess().getEventuallyKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1094:2: ( 'F' )
                    {
                    // InternalSTL.g:1094:2: ( 'F' )
                    // InternalSTL.g:1095:3: 'F'
                    {
                     before(grammarAccess.getEventuallyOperatorAccess().getFKeyword_1()); 
                    match(input,94,FOLLOW_2); 
                     after(grammarAccess.getEventuallyOperatorAccess().getFKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventuallyOperator__Alternatives"


    // $ANTLR start "rule__HistoricallyOperator__Alternatives"
    // InternalSTL.g:1104:1: rule__HistoricallyOperator__Alternatives : ( ( 'historically' ) | ( 'H' ) );
    public final void rule__HistoricallyOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1108:1: ( ( 'historically' ) | ( 'H' ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==95) ) {
                alt16=1;
            }
            else if ( (LA16_0==96) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalSTL.g:1109:2: ( 'historically' )
                    {
                    // InternalSTL.g:1109:2: ( 'historically' )
                    // InternalSTL.g:1110:3: 'historically'
                    {
                     before(grammarAccess.getHistoricallyOperatorAccess().getHistoricallyKeyword_0()); 
                    match(input,95,FOLLOW_2); 
                     after(grammarAccess.getHistoricallyOperatorAccess().getHistoricallyKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1115:2: ( 'H' )
                    {
                    // InternalSTL.g:1115:2: ( 'H' )
                    // InternalSTL.g:1116:3: 'H'
                    {
                     before(grammarAccess.getHistoricallyOperatorAccess().getHKeyword_1()); 
                    match(input,96,FOLLOW_2); 
                     after(grammarAccess.getHistoricallyOperatorAccess().getHKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__HistoricallyOperator__Alternatives"


    // $ANTLR start "rule__OnceOperator__Alternatives"
    // InternalSTL.g:1125:1: rule__OnceOperator__Alternatives : ( ( 'once' ) | ( 'O' ) );
    public final void rule__OnceOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1129:1: ( ( 'once' ) | ( 'O' ) )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==97) ) {
                alt17=1;
            }
            else if ( (LA17_0==98) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalSTL.g:1130:2: ( 'once' )
                    {
                    // InternalSTL.g:1130:2: ( 'once' )
                    // InternalSTL.g:1131:3: 'once'
                    {
                     before(grammarAccess.getOnceOperatorAccess().getOnceKeyword_0()); 
                    match(input,97,FOLLOW_2); 
                     after(grammarAccess.getOnceOperatorAccess().getOnceKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1136:2: ( 'O' )
                    {
                    // InternalSTL.g:1136:2: ( 'O' )
                    // InternalSTL.g:1137:3: 'O'
                    {
                     before(grammarAccess.getOnceOperatorAccess().getOKeyword_1()); 
                    match(input,98,FOLLOW_2); 
                     after(grammarAccess.getOnceOperatorAccess().getOKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OnceOperator__Alternatives"


    // $ANTLR start "rule__NextOperator__Alternatives"
    // InternalSTL.g:1146:1: rule__NextOperator__Alternatives : ( ( 'next' ) | ( 'X' ) );
    public final void rule__NextOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1150:1: ( ( 'next' ) | ( 'X' ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==99) ) {
                alt18=1;
            }
            else if ( (LA18_0==100) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalSTL.g:1151:2: ( 'next' )
                    {
                    // InternalSTL.g:1151:2: ( 'next' )
                    // InternalSTL.g:1152:3: 'next'
                    {
                     before(grammarAccess.getNextOperatorAccess().getNextKeyword_0()); 
                    match(input,99,FOLLOW_2); 
                     after(grammarAccess.getNextOperatorAccess().getNextKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1157:2: ( 'X' )
                    {
                    // InternalSTL.g:1157:2: ( 'X' )
                    // InternalSTL.g:1158:3: 'X'
                    {
                     before(grammarAccess.getNextOperatorAccess().getXKeyword_1()); 
                    match(input,100,FOLLOW_2); 
                     after(grammarAccess.getNextOperatorAccess().getXKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NextOperator__Alternatives"


    // $ANTLR start "rule__PreviousOperator__Alternatives"
    // InternalSTL.g:1167:1: rule__PreviousOperator__Alternatives : ( ( 'prev' ) | ( 'Y' ) );
    public final void rule__PreviousOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1171:1: ( ( 'prev' ) | ( 'Y' ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==101) ) {
                alt19=1;
            }
            else if ( (LA19_0==102) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalSTL.g:1172:2: ( 'prev' )
                    {
                    // InternalSTL.g:1172:2: ( 'prev' )
                    // InternalSTL.g:1173:3: 'prev'
                    {
                     before(grammarAccess.getPreviousOperatorAccess().getPrevKeyword_0()); 
                    match(input,101,FOLLOW_2); 
                     after(grammarAccess.getPreviousOperatorAccess().getPrevKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1178:2: ( 'Y' )
                    {
                    // InternalSTL.g:1178:2: ( 'Y' )
                    // InternalSTL.g:1179:3: 'Y'
                    {
                     before(grammarAccess.getPreviousOperatorAccess().getYKeyword_1()); 
                    match(input,102,FOLLOW_2); 
                     after(grammarAccess.getPreviousOperatorAccess().getYKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PreviousOperator__Alternatives"


    // $ANTLR start "rule__EqualOperator__Alternatives"
    // InternalSTL.g:1188:1: rule__EqualOperator__Alternatives : ( ( '==' ) | ( '=' ) );
    public final void rule__EqualOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1192:1: ( ( '==' ) | ( '=' ) )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==103) ) {
                alt20=1;
            }
            else if ( (LA20_0==104) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalSTL.g:1193:2: ( '==' )
                    {
                    // InternalSTL.g:1193:2: ( '==' )
                    // InternalSTL.g:1194:3: '=='
                    {
                     before(grammarAccess.getEqualOperatorAccess().getEqualsSignEqualsSignKeyword_0()); 
                    match(input,103,FOLLOW_2); 
                     after(grammarAccess.getEqualOperatorAccess().getEqualsSignEqualsSignKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1199:2: ( '=' )
                    {
                    // InternalSTL.g:1199:2: ( '=' )
                    // InternalSTL.g:1200:3: '='
                    {
                     before(grammarAccess.getEqualOperatorAccess().getEqualsSignKeyword_1()); 
                    match(input,104,FOLLOW_2); 
                     after(grammarAccess.getEqualOperatorAccess().getEqualsSignKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EqualOperator__Alternatives"


    // $ANTLR start "rule__NotEqualOperator__Alternatives"
    // InternalSTL.g:1209:1: rule__NotEqualOperator__Alternatives : ( ( '!==' ) | ( '!=' ) );
    public final void rule__NotEqualOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1213:1: ( ( '!==' ) | ( '!=' ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==105) ) {
                alt21=1;
            }
            else if ( (LA21_0==106) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalSTL.g:1214:2: ( '!==' )
                    {
                    // InternalSTL.g:1214:2: ( '!==' )
                    // InternalSTL.g:1215:3: '!=='
                    {
                     before(grammarAccess.getNotEqualOperatorAccess().getExclamationMarkEqualsSignEqualsSignKeyword_0()); 
                    match(input,105,FOLLOW_2); 
                     after(grammarAccess.getNotEqualOperatorAccess().getExclamationMarkEqualsSignEqualsSignKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1220:2: ( '!=' )
                    {
                    // InternalSTL.g:1220:2: ( '!=' )
                    // InternalSTL.g:1221:3: '!='
                    {
                     before(grammarAccess.getNotEqualOperatorAccess().getExclamationMarkEqualsSignKeyword_1()); 
                    match(input,106,FOLLOW_2); 
                     after(grammarAccess.getNotEqualOperatorAccess().getExclamationMarkEqualsSignKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotEqualOperator__Alternatives"


    // $ANTLR start "rule__TRUE__Alternatives"
    // InternalSTL.g:1230:1: rule__TRUE__Alternatives : ( ( 'true' ) | ( 'TRUE' ) );
    public final void rule__TRUE__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1234:1: ( ( 'true' ) | ( 'TRUE' ) )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==107) ) {
                alt22=1;
            }
            else if ( (LA22_0==108) ) {
                alt22=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // InternalSTL.g:1235:2: ( 'true' )
                    {
                    // InternalSTL.g:1235:2: ( 'true' )
                    // InternalSTL.g:1236:3: 'true'
                    {
                     before(grammarAccess.getTRUEAccess().getTrueKeyword_0()); 
                    match(input,107,FOLLOW_2); 
                     after(grammarAccess.getTRUEAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1241:2: ( 'TRUE' )
                    {
                    // InternalSTL.g:1241:2: ( 'TRUE' )
                    // InternalSTL.g:1242:3: 'TRUE'
                    {
                     before(grammarAccess.getTRUEAccess().getTRUEKeyword_1()); 
                    match(input,108,FOLLOW_2); 
                     after(grammarAccess.getTRUEAccess().getTRUEKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TRUE__Alternatives"


    // $ANTLR start "rule__FALSE__Alternatives"
    // InternalSTL.g:1251:1: rule__FALSE__Alternatives : ( ( 'false' ) | ( 'FALSE' ) );
    public final void rule__FALSE__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1255:1: ( ( 'false' ) | ( 'FALSE' ) )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==109) ) {
                alt23=1;
            }
            else if ( (LA23_0==110) ) {
                alt23=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalSTL.g:1256:2: ( 'false' )
                    {
                    // InternalSTL.g:1256:2: ( 'false' )
                    // InternalSTL.g:1257:3: 'false'
                    {
                     before(grammarAccess.getFALSEAccess().getFalseKeyword_0()); 
                    match(input,109,FOLLOW_2); 
                     after(grammarAccess.getFALSEAccess().getFalseKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSTL.g:1262:2: ( 'FALSE' )
                    {
                    // InternalSTL.g:1262:2: ( 'FALSE' )
                    // InternalSTL.g:1263:3: 'FALSE'
                    {
                     before(grammarAccess.getFALSEAccess().getFALSEKeyword_1()); 
                    match(input,110,FOLLOW_2); 
                     after(grammarAccess.getFALSEAccess().getFALSEKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FALSE__Alternatives"


    // $ANTLR start "rule__Assignment__Group__0"
    // InternalSTL.g:1272:1: rule__Assignment__Group__0 : rule__Assignment__Group__0__Impl rule__Assignment__Group__1 ;
    public final void rule__Assignment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1276:1: ( rule__Assignment__Group__0__Impl rule__Assignment__Group__1 )
            // InternalSTL.g:1277:2: rule__Assignment__Group__0__Impl rule__Assignment__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Assignment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assignment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0"


    // $ANTLR start "rule__Assignment__Group__0__Impl"
    // InternalSTL.g:1284:1: rule__Assignment__Group__0__Impl : ( ( rule__Assignment__IdAssignment_0 ) ) ;
    public final void rule__Assignment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1288:1: ( ( ( rule__Assignment__IdAssignment_0 ) ) )
            // InternalSTL.g:1289:1: ( ( rule__Assignment__IdAssignment_0 ) )
            {
            // InternalSTL.g:1289:1: ( ( rule__Assignment__IdAssignment_0 ) )
            // InternalSTL.g:1290:2: ( rule__Assignment__IdAssignment_0 )
            {
             before(grammarAccess.getAssignmentAccess().getIdAssignment_0()); 
            // InternalSTL.g:1291:2: ( rule__Assignment__IdAssignment_0 )
            // InternalSTL.g:1291:3: rule__Assignment__IdAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__IdAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentAccess().getIdAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__0__Impl"


    // $ANTLR start "rule__Assignment__Group__1"
    // InternalSTL.g:1299:1: rule__Assignment__Group__1 : rule__Assignment__Group__1__Impl rule__Assignment__Group__2 ;
    public final void rule__Assignment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1303:1: ( rule__Assignment__Group__1__Impl rule__Assignment__Group__2 )
            // InternalSTL.g:1304:2: rule__Assignment__Group__1__Impl rule__Assignment__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Assignment__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Assignment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1"


    // $ANTLR start "rule__Assignment__Group__1__Impl"
    // InternalSTL.g:1311:1: rule__Assignment__Group__1__Impl : ( ruleEqualOperator ) ;
    public final void rule__Assignment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1315:1: ( ( ruleEqualOperator ) )
            // InternalSTL.g:1316:1: ( ruleEqualOperator )
            {
            // InternalSTL.g:1316:1: ( ruleEqualOperator )
            // InternalSTL.g:1317:2: ruleEqualOperator
            {
             before(grammarAccess.getAssignmentAccess().getEqualOperatorParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleEqualOperator();

            state._fsp--;

             after(grammarAccess.getAssignmentAccess().getEqualOperatorParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__1__Impl"


    // $ANTLR start "rule__Assignment__Group__2"
    // InternalSTL.g:1326:1: rule__Assignment__Group__2 : rule__Assignment__Group__2__Impl ;
    public final void rule__Assignment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1330:1: ( rule__Assignment__Group__2__Impl )
            // InternalSTL.g:1331:2: rule__Assignment__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2"


    // $ANTLR start "rule__Assignment__Group__2__Impl"
    // InternalSTL.g:1337:1: rule__Assignment__Group__2__Impl : ( ( rule__Assignment__ExprAssignment_2 ) ) ;
    public final void rule__Assignment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1341:1: ( ( ( rule__Assignment__ExprAssignment_2 ) ) )
            // InternalSTL.g:1342:1: ( ( rule__Assignment__ExprAssignment_2 ) )
            {
            // InternalSTL.g:1342:1: ( ( rule__Assignment__ExprAssignment_2 ) )
            // InternalSTL.g:1343:2: ( rule__Assignment__ExprAssignment_2 )
            {
             before(grammarAccess.getAssignmentAccess().getExprAssignment_2()); 
            // InternalSTL.g:1344:2: ( rule__Assignment__ExprAssignment_2 )
            // InternalSTL.g:1344:3: rule__Assignment__ExprAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Assignment__ExprAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentAccess().getExprAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__Group__2__Impl"


    // $ANTLR start "rule__Expression__Group__0"
    // InternalSTL.g:1353:1: rule__Expression__Group__0 : rule__Expression__Group__0__Impl rule__Expression__Group__1 ;
    public final void rule__Expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1357:1: ( rule__Expression__Group__0__Impl rule__Expression__Group__1 )
            // InternalSTL.g:1358:2: rule__Expression__Group__0__Impl rule__Expression__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Expression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0"


    // $ANTLR start "rule__Expression__Group__0__Impl"
    // InternalSTL.g:1365:1: rule__Expression__Group__0__Impl : ( ( rule__Expression__FirstAssignment_0 ) ) ;
    public final void rule__Expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1369:1: ( ( ( rule__Expression__FirstAssignment_0 ) ) )
            // InternalSTL.g:1370:1: ( ( rule__Expression__FirstAssignment_0 ) )
            {
            // InternalSTL.g:1370:1: ( ( rule__Expression__FirstAssignment_0 ) )
            // InternalSTL.g:1371:2: ( rule__Expression__FirstAssignment_0 )
            {
             before(grammarAccess.getExpressionAccess().getFirstAssignment_0()); 
            // InternalSTL.g:1372:2: ( rule__Expression__FirstAssignment_0 )
            // InternalSTL.g:1372:3: rule__Expression__FirstAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Expression__FirstAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getFirstAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0__Impl"


    // $ANTLR start "rule__Expression__Group__1"
    // InternalSTL.g:1380:1: rule__Expression__Group__1 : rule__Expression__Group__1__Impl ;
    public final void rule__Expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1384:1: ( rule__Expression__Group__1__Impl )
            // InternalSTL.g:1385:2: rule__Expression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1"


    // $ANTLR start "rule__Expression__Group__1__Impl"
    // InternalSTL.g:1391:1: rule__Expression__Group__1__Impl : ( ( rule__Expression__EcAssignment_1 )? ) ;
    public final void rule__Expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1395:1: ( ( ( rule__Expression__EcAssignment_1 )? ) )
            // InternalSTL.g:1396:1: ( ( rule__Expression__EcAssignment_1 )? )
            {
            // InternalSTL.g:1396:1: ( ( rule__Expression__EcAssignment_1 )? )
            // InternalSTL.g:1397:2: ( rule__Expression__EcAssignment_1 )?
            {
             before(grammarAccess.getExpressionAccess().getEcAssignment_1()); 
            // InternalSTL.g:1398:2: ( rule__Expression__EcAssignment_1 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( ((LA24_0>=76 && LA24_0<=106)) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalSTL.g:1398:3: rule__Expression__EcAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression__EcAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpressionAccess().getEcAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1__Impl"


    // $ANTLR start "rule__Expression_start__Group_1__0"
    // InternalSTL.g:1407:1: rule__Expression_start__Group_1__0 : rule__Expression_start__Group_1__0__Impl rule__Expression_start__Group_1__1 ;
    public final void rule__Expression_start__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1411:1: ( rule__Expression_start__Group_1__0__Impl rule__Expression_start__Group_1__1 )
            // InternalSTL.g:1412:2: rule__Expression_start__Group_1__0__Impl rule__Expression_start__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__Expression_start__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_start__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__0"


    // $ANTLR start "rule__Expression_start__Group_1__0__Impl"
    // InternalSTL.g:1419:1: rule__Expression_start__Group_1__0__Impl : ( RULE_LPAREN ) ;
    public final void rule__Expression_start__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1423:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:1424:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:1424:1: ( RULE_LPAREN )
            // InternalSTL.g:1425:2: RULE_LPAREN
            {
             before(grammarAccess.getExpression_startAccess().getLPARENTerminalRuleCall_1_0()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getExpression_startAccess().getLPARENTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__0__Impl"


    // $ANTLR start "rule__Expression_start__Group_1__1"
    // InternalSTL.g:1434:1: rule__Expression_start__Group_1__1 : rule__Expression_start__Group_1__1__Impl rule__Expression_start__Group_1__2 ;
    public final void rule__Expression_start__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1438:1: ( rule__Expression_start__Group_1__1__Impl rule__Expression_start__Group_1__2 )
            // InternalSTL.g:1439:2: rule__Expression_start__Group_1__1__Impl rule__Expression_start__Group_1__2
            {
            pushFollow(FOLLOW_6);
            rule__Expression_start__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_start__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__1"


    // $ANTLR start "rule__Expression_start__Group_1__1__Impl"
    // InternalSTL.g:1446:1: rule__Expression_start__Group_1__1__Impl : ( ( rule__Expression_start__E1Assignment_1_1 ) ) ;
    public final void rule__Expression_start__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1450:1: ( ( ( rule__Expression_start__E1Assignment_1_1 ) ) )
            // InternalSTL.g:1451:1: ( ( rule__Expression_start__E1Assignment_1_1 ) )
            {
            // InternalSTL.g:1451:1: ( ( rule__Expression_start__E1Assignment_1_1 ) )
            // InternalSTL.g:1452:2: ( rule__Expression_start__E1Assignment_1_1 )
            {
             before(grammarAccess.getExpression_startAccess().getE1Assignment_1_1()); 
            // InternalSTL.g:1453:2: ( rule__Expression_start__E1Assignment_1_1 )
            // InternalSTL.g:1453:3: rule__Expression_start__E1Assignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_start__E1Assignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_startAccess().getE1Assignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__1__Impl"


    // $ANTLR start "rule__Expression_start__Group_1__2"
    // InternalSTL.g:1461:1: rule__Expression_start__Group_1__2 : rule__Expression_start__Group_1__2__Impl rule__Expression_start__Group_1__3 ;
    public final void rule__Expression_start__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1465:1: ( rule__Expression_start__Group_1__2__Impl rule__Expression_start__Group_1__3 )
            // InternalSTL.g:1466:2: rule__Expression_start__Group_1__2__Impl rule__Expression_start__Group_1__3
            {
            pushFollow(FOLLOW_7);
            rule__Expression_start__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_start__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__2"


    // $ANTLR start "rule__Expression_start__Group_1__2__Impl"
    // InternalSTL.g:1473:1: rule__Expression_start__Group_1__2__Impl : ( rulecomparisonOp ) ;
    public final void rule__Expression_start__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1477:1: ( ( rulecomparisonOp ) )
            // InternalSTL.g:1478:1: ( rulecomparisonOp )
            {
            // InternalSTL.g:1478:1: ( rulecomparisonOp )
            // InternalSTL.g:1479:2: rulecomparisonOp
            {
             before(grammarAccess.getExpression_startAccess().getComparisonOpParserRuleCall_1_2()); 
            pushFollow(FOLLOW_2);
            rulecomparisonOp();

            state._fsp--;

             after(grammarAccess.getExpression_startAccess().getComparisonOpParserRuleCall_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__2__Impl"


    // $ANTLR start "rule__Expression_start__Group_1__3"
    // InternalSTL.g:1488:1: rule__Expression_start__Group_1__3 : rule__Expression_start__Group_1__3__Impl rule__Expression_start__Group_1__4 ;
    public final void rule__Expression_start__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1492:1: ( rule__Expression_start__Group_1__3__Impl rule__Expression_start__Group_1__4 )
            // InternalSTL.g:1493:2: rule__Expression_start__Group_1__3__Impl rule__Expression_start__Group_1__4
            {
            pushFollow(FOLLOW_8);
            rule__Expression_start__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_start__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__3"


    // $ANTLR start "rule__Expression_start__Group_1__3__Impl"
    // InternalSTL.g:1500:1: rule__Expression_start__Group_1__3__Impl : ( ( rule__Expression_start__E2Assignment_1_3 ) ) ;
    public final void rule__Expression_start__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1504:1: ( ( ( rule__Expression_start__E2Assignment_1_3 ) ) )
            // InternalSTL.g:1505:1: ( ( rule__Expression_start__E2Assignment_1_3 ) )
            {
            // InternalSTL.g:1505:1: ( ( rule__Expression_start__E2Assignment_1_3 ) )
            // InternalSTL.g:1506:2: ( rule__Expression_start__E2Assignment_1_3 )
            {
             before(grammarAccess.getExpression_startAccess().getE2Assignment_1_3()); 
            // InternalSTL.g:1507:2: ( rule__Expression_start__E2Assignment_1_3 )
            // InternalSTL.g:1507:3: rule__Expression_start__E2Assignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__Expression_start__E2Assignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getExpression_startAccess().getE2Assignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__3__Impl"


    // $ANTLR start "rule__Expression_start__Group_1__4"
    // InternalSTL.g:1515:1: rule__Expression_start__Group_1__4 : rule__Expression_start__Group_1__4__Impl ;
    public final void rule__Expression_start__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1519:1: ( rule__Expression_start__Group_1__4__Impl )
            // InternalSTL.g:1520:2: rule__Expression_start__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_start__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__4"


    // $ANTLR start "rule__Expression_start__Group_1__4__Impl"
    // InternalSTL.g:1526:1: rule__Expression_start__Group_1__4__Impl : ( RULE_RPAREN ) ;
    public final void rule__Expression_start__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1530:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:1531:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:1531:1: ( RULE_RPAREN )
            // InternalSTL.g:1532:2: RULE_RPAREN
            {
             before(grammarAccess.getExpression_startAccess().getRPARENTerminalRuleCall_1_4()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getExpression_startAccess().getRPARENTerminalRuleCall_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__Group_1__4__Impl"


    // $ANTLR start "rule__Expression_cont__Group_0__0"
    // InternalSTL.g:1542:1: rule__Expression_cont__Group_0__0 : rule__Expression_cont__Group_0__0__Impl rule__Expression_cont__Group_0__1 ;
    public final void rule__Expression_cont__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1546:1: ( rule__Expression_cont__Group_0__0__Impl rule__Expression_cont__Group_0__1 )
            // InternalSTL.g:1547:2: rule__Expression_cont__Group_0__0__Impl rule__Expression_cont__Group_0__1
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_0__0"


    // $ANTLR start "rule__Expression_cont__Group_0__0__Impl"
    // InternalSTL.g:1554:1: rule__Expression_cont__Group_0__0__Impl : ( ruleAlwaysOperator ) ;
    public final void rule__Expression_cont__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1558:1: ( ( ruleAlwaysOperator ) )
            // InternalSTL.g:1559:1: ( ruleAlwaysOperator )
            {
            // InternalSTL.g:1559:1: ( ruleAlwaysOperator )
            // InternalSTL.g:1560:2: ruleAlwaysOperator
            {
             before(grammarAccess.getExpression_contAccess().getAlwaysOperatorParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAlwaysOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getAlwaysOperatorParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_0__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_0__1"
    // InternalSTL.g:1569:1: rule__Expression_cont__Group_0__1 : rule__Expression_cont__Group_0__1__Impl rule__Expression_cont__Group_0__2 ;
    public final void rule__Expression_cont__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1573:1: ( rule__Expression_cont__Group_0__1__Impl rule__Expression_cont__Group_0__2 )
            // InternalSTL.g:1574:2: rule__Expression_cont__Group_0__1__Impl rule__Expression_cont__Group_0__2
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_0__1"


    // $ANTLR start "rule__Expression_cont__Group_0__1__Impl"
    // InternalSTL.g:1581:1: rule__Expression_cont__Group_0__1__Impl : ( ( rule__Expression_cont__IAssignment_0_1 )? ) ;
    public final void rule__Expression_cont__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1585:1: ( ( ( rule__Expression_cont__IAssignment_0_1 )? ) )
            // InternalSTL.g:1586:1: ( ( rule__Expression_cont__IAssignment_0_1 )? )
            {
            // InternalSTL.g:1586:1: ( ( rule__Expression_cont__IAssignment_0_1 )? )
            // InternalSTL.g:1587:2: ( rule__Expression_cont__IAssignment_0_1 )?
            {
             before(grammarAccess.getExpression_contAccess().getIAssignment_0_1()); 
            // InternalSTL.g:1588:2: ( rule__Expression_cont__IAssignment_0_1 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==RULE_LBRACK) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalSTL.g:1588:3: rule__Expression_cont__IAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__IAssignment_0_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpression_contAccess().getIAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_0__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_0__2"
    // InternalSTL.g:1596:1: rule__Expression_cont__Group_0__2 : rule__Expression_cont__Group_0__2__Impl ;
    public final void rule__Expression_cont__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1600:1: ( rule__Expression_cont__Group_0__2__Impl )
            // InternalSTL.g:1601:2: rule__Expression_cont__Group_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_0__2"


    // $ANTLR start "rule__Expression_cont__Group_0__2__Impl"
    // InternalSTL.g:1607:1: rule__Expression_cont__Group_0__2__Impl : ( ( rule__Expression_cont__EAssignment_0_2 ) ) ;
    public final void rule__Expression_cont__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1611:1: ( ( ( rule__Expression_cont__EAssignment_0_2 ) ) )
            // InternalSTL.g:1612:1: ( ( rule__Expression_cont__EAssignment_0_2 ) )
            {
            // InternalSTL.g:1612:1: ( ( rule__Expression_cont__EAssignment_0_2 ) )
            // InternalSTL.g:1613:2: ( rule__Expression_cont__EAssignment_0_2 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_0_2()); 
            // InternalSTL.g:1614:2: ( rule__Expression_cont__EAssignment_0_2 )
            // InternalSTL.g:1614:3: rule__Expression_cont__EAssignment_0_2
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_0_2();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_0__2__Impl"


    // $ANTLR start "rule__Expression_cont__Group_1__0"
    // InternalSTL.g:1623:1: rule__Expression_cont__Group_1__0 : rule__Expression_cont__Group_1__0__Impl rule__Expression_cont__Group_1__1 ;
    public final void rule__Expression_cont__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1627:1: ( rule__Expression_cont__Group_1__0__Impl rule__Expression_cont__Group_1__1 )
            // InternalSTL.g:1628:2: rule__Expression_cont__Group_1__0__Impl rule__Expression_cont__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_1__0"


    // $ANTLR start "rule__Expression_cont__Group_1__0__Impl"
    // InternalSTL.g:1635:1: rule__Expression_cont__Group_1__0__Impl : ( ruleEventuallyOperator ) ;
    public final void rule__Expression_cont__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1639:1: ( ( ruleEventuallyOperator ) )
            // InternalSTL.g:1640:1: ( ruleEventuallyOperator )
            {
            // InternalSTL.g:1640:1: ( ruleEventuallyOperator )
            // InternalSTL.g:1641:2: ruleEventuallyOperator
            {
             before(grammarAccess.getExpression_contAccess().getEventuallyOperatorParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEventuallyOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEventuallyOperatorParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_1__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_1__1"
    // InternalSTL.g:1650:1: rule__Expression_cont__Group_1__1 : rule__Expression_cont__Group_1__1__Impl rule__Expression_cont__Group_1__2 ;
    public final void rule__Expression_cont__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1654:1: ( rule__Expression_cont__Group_1__1__Impl rule__Expression_cont__Group_1__2 )
            // InternalSTL.g:1655:2: rule__Expression_cont__Group_1__1__Impl rule__Expression_cont__Group_1__2
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_1__1"


    // $ANTLR start "rule__Expression_cont__Group_1__1__Impl"
    // InternalSTL.g:1662:1: rule__Expression_cont__Group_1__1__Impl : ( ( rule__Expression_cont__IAssignment_1_1 )? ) ;
    public final void rule__Expression_cont__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1666:1: ( ( ( rule__Expression_cont__IAssignment_1_1 )? ) )
            // InternalSTL.g:1667:1: ( ( rule__Expression_cont__IAssignment_1_1 )? )
            {
            // InternalSTL.g:1667:1: ( ( rule__Expression_cont__IAssignment_1_1 )? )
            // InternalSTL.g:1668:2: ( rule__Expression_cont__IAssignment_1_1 )?
            {
             before(grammarAccess.getExpression_contAccess().getIAssignment_1_1()); 
            // InternalSTL.g:1669:2: ( rule__Expression_cont__IAssignment_1_1 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==RULE_LBRACK) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalSTL.g:1669:3: rule__Expression_cont__IAssignment_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__IAssignment_1_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpression_contAccess().getIAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_1__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_1__2"
    // InternalSTL.g:1677:1: rule__Expression_cont__Group_1__2 : rule__Expression_cont__Group_1__2__Impl ;
    public final void rule__Expression_cont__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1681:1: ( rule__Expression_cont__Group_1__2__Impl )
            // InternalSTL.g:1682:2: rule__Expression_cont__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_1__2"


    // $ANTLR start "rule__Expression_cont__Group_1__2__Impl"
    // InternalSTL.g:1688:1: rule__Expression_cont__Group_1__2__Impl : ( ( rule__Expression_cont__EAssignment_1_2 ) ) ;
    public final void rule__Expression_cont__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1692:1: ( ( ( rule__Expression_cont__EAssignment_1_2 ) ) )
            // InternalSTL.g:1693:1: ( ( rule__Expression_cont__EAssignment_1_2 ) )
            {
            // InternalSTL.g:1693:1: ( ( rule__Expression_cont__EAssignment_1_2 ) )
            // InternalSTL.g:1694:2: ( rule__Expression_cont__EAssignment_1_2 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_1_2()); 
            // InternalSTL.g:1695:2: ( rule__Expression_cont__EAssignment_1_2 )
            // InternalSTL.g:1695:3: rule__Expression_cont__EAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_1__2__Impl"


    // $ANTLR start "rule__Expression_cont__Group_2__0"
    // InternalSTL.g:1704:1: rule__Expression_cont__Group_2__0 : rule__Expression_cont__Group_2__0__Impl rule__Expression_cont__Group_2__1 ;
    public final void rule__Expression_cont__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1708:1: ( rule__Expression_cont__Group_2__0__Impl rule__Expression_cont__Group_2__1 )
            // InternalSTL.g:1709:2: rule__Expression_cont__Group_2__0__Impl rule__Expression_cont__Group_2__1
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_2__0"


    // $ANTLR start "rule__Expression_cont__Group_2__0__Impl"
    // InternalSTL.g:1716:1: rule__Expression_cont__Group_2__0__Impl : ( ruleHistoricallyOperator ) ;
    public final void rule__Expression_cont__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1720:1: ( ( ruleHistoricallyOperator ) )
            // InternalSTL.g:1721:1: ( ruleHistoricallyOperator )
            {
            // InternalSTL.g:1721:1: ( ruleHistoricallyOperator )
            // InternalSTL.g:1722:2: ruleHistoricallyOperator
            {
             before(grammarAccess.getExpression_contAccess().getHistoricallyOperatorParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleHistoricallyOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getHistoricallyOperatorParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_2__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_2__1"
    // InternalSTL.g:1731:1: rule__Expression_cont__Group_2__1 : rule__Expression_cont__Group_2__1__Impl rule__Expression_cont__Group_2__2 ;
    public final void rule__Expression_cont__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1735:1: ( rule__Expression_cont__Group_2__1__Impl rule__Expression_cont__Group_2__2 )
            // InternalSTL.g:1736:2: rule__Expression_cont__Group_2__1__Impl rule__Expression_cont__Group_2__2
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_2__1"


    // $ANTLR start "rule__Expression_cont__Group_2__1__Impl"
    // InternalSTL.g:1743:1: rule__Expression_cont__Group_2__1__Impl : ( ( rule__Expression_cont__IAssignment_2_1 )? ) ;
    public final void rule__Expression_cont__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1747:1: ( ( ( rule__Expression_cont__IAssignment_2_1 )? ) )
            // InternalSTL.g:1748:1: ( ( rule__Expression_cont__IAssignment_2_1 )? )
            {
            // InternalSTL.g:1748:1: ( ( rule__Expression_cont__IAssignment_2_1 )? )
            // InternalSTL.g:1749:2: ( rule__Expression_cont__IAssignment_2_1 )?
            {
             before(grammarAccess.getExpression_contAccess().getIAssignment_2_1()); 
            // InternalSTL.g:1750:2: ( rule__Expression_cont__IAssignment_2_1 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_LBRACK) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalSTL.g:1750:3: rule__Expression_cont__IAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__IAssignment_2_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpression_contAccess().getIAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_2__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_2__2"
    // InternalSTL.g:1758:1: rule__Expression_cont__Group_2__2 : rule__Expression_cont__Group_2__2__Impl ;
    public final void rule__Expression_cont__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1762:1: ( rule__Expression_cont__Group_2__2__Impl )
            // InternalSTL.g:1763:2: rule__Expression_cont__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_2__2"


    // $ANTLR start "rule__Expression_cont__Group_2__2__Impl"
    // InternalSTL.g:1769:1: rule__Expression_cont__Group_2__2__Impl : ( ( rule__Expression_cont__EAssignment_2_2 ) ) ;
    public final void rule__Expression_cont__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1773:1: ( ( ( rule__Expression_cont__EAssignment_2_2 ) ) )
            // InternalSTL.g:1774:1: ( ( rule__Expression_cont__EAssignment_2_2 ) )
            {
            // InternalSTL.g:1774:1: ( ( rule__Expression_cont__EAssignment_2_2 ) )
            // InternalSTL.g:1775:2: ( rule__Expression_cont__EAssignment_2_2 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_2_2()); 
            // InternalSTL.g:1776:2: ( rule__Expression_cont__EAssignment_2_2 )
            // InternalSTL.g:1776:3: rule__Expression_cont__EAssignment_2_2
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_2__2__Impl"


    // $ANTLR start "rule__Expression_cont__Group_3__0"
    // InternalSTL.g:1785:1: rule__Expression_cont__Group_3__0 : rule__Expression_cont__Group_3__0__Impl rule__Expression_cont__Group_3__1 ;
    public final void rule__Expression_cont__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1789:1: ( rule__Expression_cont__Group_3__0__Impl rule__Expression_cont__Group_3__1 )
            // InternalSTL.g:1790:2: rule__Expression_cont__Group_3__0__Impl rule__Expression_cont__Group_3__1
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_3__0"


    // $ANTLR start "rule__Expression_cont__Group_3__0__Impl"
    // InternalSTL.g:1797:1: rule__Expression_cont__Group_3__0__Impl : ( ruleOnceOperator ) ;
    public final void rule__Expression_cont__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1801:1: ( ( ruleOnceOperator ) )
            // InternalSTL.g:1802:1: ( ruleOnceOperator )
            {
            // InternalSTL.g:1802:1: ( ruleOnceOperator )
            // InternalSTL.g:1803:2: ruleOnceOperator
            {
             before(grammarAccess.getExpression_contAccess().getOnceOperatorParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOnceOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getOnceOperatorParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_3__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_3__1"
    // InternalSTL.g:1812:1: rule__Expression_cont__Group_3__1 : rule__Expression_cont__Group_3__1__Impl rule__Expression_cont__Group_3__2 ;
    public final void rule__Expression_cont__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1816:1: ( rule__Expression_cont__Group_3__1__Impl rule__Expression_cont__Group_3__2 )
            // InternalSTL.g:1817:2: rule__Expression_cont__Group_3__1__Impl rule__Expression_cont__Group_3__2
            {
            pushFollow(FOLLOW_9);
            rule__Expression_cont__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_3__1"


    // $ANTLR start "rule__Expression_cont__Group_3__1__Impl"
    // InternalSTL.g:1824:1: rule__Expression_cont__Group_3__1__Impl : ( ( rule__Expression_cont__IAssignment_3_1 )? ) ;
    public final void rule__Expression_cont__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1828:1: ( ( ( rule__Expression_cont__IAssignment_3_1 )? ) )
            // InternalSTL.g:1829:1: ( ( rule__Expression_cont__IAssignment_3_1 )? )
            {
            // InternalSTL.g:1829:1: ( ( rule__Expression_cont__IAssignment_3_1 )? )
            // InternalSTL.g:1830:2: ( rule__Expression_cont__IAssignment_3_1 )?
            {
             before(grammarAccess.getExpression_contAccess().getIAssignment_3_1()); 
            // InternalSTL.g:1831:2: ( rule__Expression_cont__IAssignment_3_1 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_LBRACK) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalSTL.g:1831:3: rule__Expression_cont__IAssignment_3_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Expression_cont__IAssignment_3_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExpression_contAccess().getIAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_3__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_3__2"
    // InternalSTL.g:1839:1: rule__Expression_cont__Group_3__2 : rule__Expression_cont__Group_3__2__Impl ;
    public final void rule__Expression_cont__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1843:1: ( rule__Expression_cont__Group_3__2__Impl )
            // InternalSTL.g:1844:2: rule__Expression_cont__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_3__2"


    // $ANTLR start "rule__Expression_cont__Group_3__2__Impl"
    // InternalSTL.g:1850:1: rule__Expression_cont__Group_3__2__Impl : ( ( rule__Expression_cont__EAssignment_3_2 ) ) ;
    public final void rule__Expression_cont__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1854:1: ( ( ( rule__Expression_cont__EAssignment_3_2 ) ) )
            // InternalSTL.g:1855:1: ( ( rule__Expression_cont__EAssignment_3_2 ) )
            {
            // InternalSTL.g:1855:1: ( ( rule__Expression_cont__EAssignment_3_2 ) )
            // InternalSTL.g:1856:2: ( rule__Expression_cont__EAssignment_3_2 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_3_2()); 
            // InternalSTL.g:1857:2: ( rule__Expression_cont__EAssignment_3_2 )
            // InternalSTL.g:1857:3: rule__Expression_cont__EAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_3__2__Impl"


    // $ANTLR start "rule__Expression_cont__Group_4__0"
    // InternalSTL.g:1866:1: rule__Expression_cont__Group_4__0 : rule__Expression_cont__Group_4__0__Impl rule__Expression_cont__Group_4__1 ;
    public final void rule__Expression_cont__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1870:1: ( rule__Expression_cont__Group_4__0__Impl rule__Expression_cont__Group_4__1 )
            // InternalSTL.g:1871:2: rule__Expression_cont__Group_4__0__Impl rule__Expression_cont__Group_4__1
            {
            pushFollow(FOLLOW_10);
            rule__Expression_cont__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__0"


    // $ANTLR start "rule__Expression_cont__Group_4__0__Impl"
    // InternalSTL.g:1878:1: rule__Expression_cont__Group_4__0__Impl : ( ruleRiseOperator ) ;
    public final void rule__Expression_cont__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1882:1: ( ( ruleRiseOperator ) )
            // InternalSTL.g:1883:1: ( ruleRiseOperator )
            {
            // InternalSTL.g:1883:1: ( ruleRiseOperator )
            // InternalSTL.g:1884:2: ruleRiseOperator
            {
             before(grammarAccess.getExpression_contAccess().getRiseOperatorParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleRiseOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getRiseOperatorParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_4__1"
    // InternalSTL.g:1893:1: rule__Expression_cont__Group_4__1 : rule__Expression_cont__Group_4__1__Impl rule__Expression_cont__Group_4__2 ;
    public final void rule__Expression_cont__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1897:1: ( rule__Expression_cont__Group_4__1__Impl rule__Expression_cont__Group_4__2 )
            // InternalSTL.g:1898:2: rule__Expression_cont__Group_4__1__Impl rule__Expression_cont__Group_4__2
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__1"


    // $ANTLR start "rule__Expression_cont__Group_4__1__Impl"
    // InternalSTL.g:1905:1: rule__Expression_cont__Group_4__1__Impl : ( RULE_LPAREN ) ;
    public final void rule__Expression_cont__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1909:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:1910:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:1910:1: ( RULE_LPAREN )
            // InternalSTL.g:1911:2: RULE_LPAREN
            {
             before(grammarAccess.getExpression_contAccess().getLPARENTerminalRuleCall_4_1()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getExpression_contAccess().getLPARENTerminalRuleCall_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_4__2"
    // InternalSTL.g:1920:1: rule__Expression_cont__Group_4__2 : rule__Expression_cont__Group_4__2__Impl rule__Expression_cont__Group_4__3 ;
    public final void rule__Expression_cont__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1924:1: ( rule__Expression_cont__Group_4__2__Impl rule__Expression_cont__Group_4__3 )
            // InternalSTL.g:1925:2: rule__Expression_cont__Group_4__2__Impl rule__Expression_cont__Group_4__3
            {
            pushFollow(FOLLOW_8);
            rule__Expression_cont__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__2"


    // $ANTLR start "rule__Expression_cont__Group_4__2__Impl"
    // InternalSTL.g:1932:1: rule__Expression_cont__Group_4__2__Impl : ( ( rule__Expression_cont__EAssignment_4_2 ) ) ;
    public final void rule__Expression_cont__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1936:1: ( ( ( rule__Expression_cont__EAssignment_4_2 ) ) )
            // InternalSTL.g:1937:1: ( ( rule__Expression_cont__EAssignment_4_2 ) )
            {
            // InternalSTL.g:1937:1: ( ( rule__Expression_cont__EAssignment_4_2 ) )
            // InternalSTL.g:1938:2: ( rule__Expression_cont__EAssignment_4_2 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_4_2()); 
            // InternalSTL.g:1939:2: ( rule__Expression_cont__EAssignment_4_2 )
            // InternalSTL.g:1939:3: rule__Expression_cont__EAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__2__Impl"


    // $ANTLR start "rule__Expression_cont__Group_4__3"
    // InternalSTL.g:1947:1: rule__Expression_cont__Group_4__3 : rule__Expression_cont__Group_4__3__Impl ;
    public final void rule__Expression_cont__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1951:1: ( rule__Expression_cont__Group_4__3__Impl )
            // InternalSTL.g:1952:2: rule__Expression_cont__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__3"


    // $ANTLR start "rule__Expression_cont__Group_4__3__Impl"
    // InternalSTL.g:1958:1: rule__Expression_cont__Group_4__3__Impl : ( RULE_RPAREN ) ;
    public final void rule__Expression_cont__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1962:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:1963:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:1963:1: ( RULE_RPAREN )
            // InternalSTL.g:1964:2: RULE_RPAREN
            {
             before(grammarAccess.getExpression_contAccess().getRPARENTerminalRuleCall_4_3()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getExpression_contAccess().getRPARENTerminalRuleCall_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_4__3__Impl"


    // $ANTLR start "rule__Expression_cont__Group_5__0"
    // InternalSTL.g:1974:1: rule__Expression_cont__Group_5__0 : rule__Expression_cont__Group_5__0__Impl rule__Expression_cont__Group_5__1 ;
    public final void rule__Expression_cont__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1978:1: ( rule__Expression_cont__Group_5__0__Impl rule__Expression_cont__Group_5__1 )
            // InternalSTL.g:1979:2: rule__Expression_cont__Group_5__0__Impl rule__Expression_cont__Group_5__1
            {
            pushFollow(FOLLOW_10);
            rule__Expression_cont__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__0"


    // $ANTLR start "rule__Expression_cont__Group_5__0__Impl"
    // InternalSTL.g:1986:1: rule__Expression_cont__Group_5__0__Impl : ( ruleFallOperator ) ;
    public final void rule__Expression_cont__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:1990:1: ( ( ruleFallOperator ) )
            // InternalSTL.g:1991:1: ( ruleFallOperator )
            {
            // InternalSTL.g:1991:1: ( ruleFallOperator )
            // InternalSTL.g:1992:2: ruleFallOperator
            {
             before(grammarAccess.getExpression_contAccess().getFallOperatorParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleFallOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getFallOperatorParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_5__1"
    // InternalSTL.g:2001:1: rule__Expression_cont__Group_5__1 : rule__Expression_cont__Group_5__1__Impl rule__Expression_cont__Group_5__2 ;
    public final void rule__Expression_cont__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2005:1: ( rule__Expression_cont__Group_5__1__Impl rule__Expression_cont__Group_5__2 )
            // InternalSTL.g:2006:2: rule__Expression_cont__Group_5__1__Impl rule__Expression_cont__Group_5__2
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__1"


    // $ANTLR start "rule__Expression_cont__Group_5__1__Impl"
    // InternalSTL.g:2013:1: rule__Expression_cont__Group_5__1__Impl : ( RULE_LPAREN ) ;
    public final void rule__Expression_cont__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2017:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:2018:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:2018:1: ( RULE_LPAREN )
            // InternalSTL.g:2019:2: RULE_LPAREN
            {
             before(grammarAccess.getExpression_contAccess().getLPARENTerminalRuleCall_5_1()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getExpression_contAccess().getLPARENTerminalRuleCall_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_5__2"
    // InternalSTL.g:2028:1: rule__Expression_cont__Group_5__2 : rule__Expression_cont__Group_5__2__Impl rule__Expression_cont__Group_5__3 ;
    public final void rule__Expression_cont__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2032:1: ( rule__Expression_cont__Group_5__2__Impl rule__Expression_cont__Group_5__3 )
            // InternalSTL.g:2033:2: rule__Expression_cont__Group_5__2__Impl rule__Expression_cont__Group_5__3
            {
            pushFollow(FOLLOW_8);
            rule__Expression_cont__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__2"


    // $ANTLR start "rule__Expression_cont__Group_5__2__Impl"
    // InternalSTL.g:2040:1: rule__Expression_cont__Group_5__2__Impl : ( ( rule__Expression_cont__EAssignment_5_2 ) ) ;
    public final void rule__Expression_cont__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2044:1: ( ( ( rule__Expression_cont__EAssignment_5_2 ) ) )
            // InternalSTL.g:2045:1: ( ( rule__Expression_cont__EAssignment_5_2 ) )
            {
            // InternalSTL.g:2045:1: ( ( rule__Expression_cont__EAssignment_5_2 ) )
            // InternalSTL.g:2046:2: ( rule__Expression_cont__EAssignment_5_2 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_5_2()); 
            // InternalSTL.g:2047:2: ( rule__Expression_cont__EAssignment_5_2 )
            // InternalSTL.g:2047:3: rule__Expression_cont__EAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__2__Impl"


    // $ANTLR start "rule__Expression_cont__Group_5__3"
    // InternalSTL.g:2055:1: rule__Expression_cont__Group_5__3 : rule__Expression_cont__Group_5__3__Impl ;
    public final void rule__Expression_cont__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2059:1: ( rule__Expression_cont__Group_5__3__Impl )
            // InternalSTL.g:2060:2: rule__Expression_cont__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__3"


    // $ANTLR start "rule__Expression_cont__Group_5__3__Impl"
    // InternalSTL.g:2066:1: rule__Expression_cont__Group_5__3__Impl : ( RULE_RPAREN ) ;
    public final void rule__Expression_cont__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2070:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:2071:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:2071:1: ( RULE_RPAREN )
            // InternalSTL.g:2072:2: RULE_RPAREN
            {
             before(grammarAccess.getExpression_contAccess().getRPARENTerminalRuleCall_5_3()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getExpression_contAccess().getRPARENTerminalRuleCall_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_5__3__Impl"


    // $ANTLR start "rule__Expression_cont__Group_6__0"
    // InternalSTL.g:2082:1: rule__Expression_cont__Group_6__0 : rule__Expression_cont__Group_6__0__Impl rule__Expression_cont__Group_6__1 ;
    public final void rule__Expression_cont__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2086:1: ( rule__Expression_cont__Group_6__0__Impl rule__Expression_cont__Group_6__1 )
            // InternalSTL.g:2087:2: rule__Expression_cont__Group_6__0__Impl rule__Expression_cont__Group_6__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_6__0"


    // $ANTLR start "rule__Expression_cont__Group_6__0__Impl"
    // InternalSTL.g:2094:1: rule__Expression_cont__Group_6__0__Impl : ( rulePreviousOperator ) ;
    public final void rule__Expression_cont__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2098:1: ( ( rulePreviousOperator ) )
            // InternalSTL.g:2099:1: ( rulePreviousOperator )
            {
            // InternalSTL.g:2099:1: ( rulePreviousOperator )
            // InternalSTL.g:2100:2: rulePreviousOperator
            {
             before(grammarAccess.getExpression_contAccess().getPreviousOperatorParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            rulePreviousOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getPreviousOperatorParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_6__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_6__1"
    // InternalSTL.g:2109:1: rule__Expression_cont__Group_6__1 : rule__Expression_cont__Group_6__1__Impl ;
    public final void rule__Expression_cont__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2113:1: ( rule__Expression_cont__Group_6__1__Impl )
            // InternalSTL.g:2114:2: rule__Expression_cont__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_6__1"


    // $ANTLR start "rule__Expression_cont__Group_6__1__Impl"
    // InternalSTL.g:2120:1: rule__Expression_cont__Group_6__1__Impl : ( ( rule__Expression_cont__EAssignment_6_1 ) ) ;
    public final void rule__Expression_cont__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2124:1: ( ( ( rule__Expression_cont__EAssignment_6_1 ) ) )
            // InternalSTL.g:2125:1: ( ( rule__Expression_cont__EAssignment_6_1 ) )
            {
            // InternalSTL.g:2125:1: ( ( rule__Expression_cont__EAssignment_6_1 ) )
            // InternalSTL.g:2126:2: ( rule__Expression_cont__EAssignment_6_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_6_1()); 
            // InternalSTL.g:2127:2: ( rule__Expression_cont__EAssignment_6_1 )
            // InternalSTL.g:2127:3: rule__Expression_cont__EAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_6__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_7__0"
    // InternalSTL.g:2136:1: rule__Expression_cont__Group_7__0 : rule__Expression_cont__Group_7__0__Impl rule__Expression_cont__Group_7__1 ;
    public final void rule__Expression_cont__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2140:1: ( rule__Expression_cont__Group_7__0__Impl rule__Expression_cont__Group_7__1 )
            // InternalSTL.g:2141:2: rule__Expression_cont__Group_7__0__Impl rule__Expression_cont__Group_7__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_7__0"


    // $ANTLR start "rule__Expression_cont__Group_7__0__Impl"
    // InternalSTL.g:2148:1: rule__Expression_cont__Group_7__0__Impl : ( ruleNextOperator ) ;
    public final void rule__Expression_cont__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2152:1: ( ( ruleNextOperator ) )
            // InternalSTL.g:2153:1: ( ruleNextOperator )
            {
            // InternalSTL.g:2153:1: ( ruleNextOperator )
            // InternalSTL.g:2154:2: ruleNextOperator
            {
             before(grammarAccess.getExpression_contAccess().getNextOperatorParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleNextOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getNextOperatorParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_7__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_7__1"
    // InternalSTL.g:2163:1: rule__Expression_cont__Group_7__1 : rule__Expression_cont__Group_7__1__Impl ;
    public final void rule__Expression_cont__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2167:1: ( rule__Expression_cont__Group_7__1__Impl )
            // InternalSTL.g:2168:2: rule__Expression_cont__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_7__1"


    // $ANTLR start "rule__Expression_cont__Group_7__1__Impl"
    // InternalSTL.g:2174:1: rule__Expression_cont__Group_7__1__Impl : ( ( rule__Expression_cont__EAssignment_7_1 ) ) ;
    public final void rule__Expression_cont__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2178:1: ( ( ( rule__Expression_cont__EAssignment_7_1 ) ) )
            // InternalSTL.g:2179:1: ( ( rule__Expression_cont__EAssignment_7_1 ) )
            {
            // InternalSTL.g:2179:1: ( ( rule__Expression_cont__EAssignment_7_1 ) )
            // InternalSTL.g:2180:2: ( rule__Expression_cont__EAssignment_7_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_7_1()); 
            // InternalSTL.g:2181:2: ( rule__Expression_cont__EAssignment_7_1 )
            // InternalSTL.g:2181:3: rule__Expression_cont__EAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_7__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_8__0"
    // InternalSTL.g:2190:1: rule__Expression_cont__Group_8__0 : rule__Expression_cont__Group_8__0__Impl rule__Expression_cont__Group_8__1 ;
    public final void rule__Expression_cont__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2194:1: ( rule__Expression_cont__Group_8__0__Impl rule__Expression_cont__Group_8__1 )
            // InternalSTL.g:2195:2: rule__Expression_cont__Group_8__0__Impl rule__Expression_cont__Group_8__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_8__0"


    // $ANTLR start "rule__Expression_cont__Group_8__0__Impl"
    // InternalSTL.g:2202:1: rule__Expression_cont__Group_8__0__Impl : ( ruleOrOperator ) ;
    public final void rule__Expression_cont__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2206:1: ( ( ruleOrOperator ) )
            // InternalSTL.g:2207:1: ( ruleOrOperator )
            {
            // InternalSTL.g:2207:1: ( ruleOrOperator )
            // InternalSTL.g:2208:2: ruleOrOperator
            {
             before(grammarAccess.getExpression_contAccess().getOrOperatorParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleOrOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getOrOperatorParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_8__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_8__1"
    // InternalSTL.g:2217:1: rule__Expression_cont__Group_8__1 : rule__Expression_cont__Group_8__1__Impl ;
    public final void rule__Expression_cont__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2221:1: ( rule__Expression_cont__Group_8__1__Impl )
            // InternalSTL.g:2222:2: rule__Expression_cont__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_8__1"


    // $ANTLR start "rule__Expression_cont__Group_8__1__Impl"
    // InternalSTL.g:2228:1: rule__Expression_cont__Group_8__1__Impl : ( ( rule__Expression_cont__EAssignment_8_1 ) ) ;
    public final void rule__Expression_cont__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2232:1: ( ( ( rule__Expression_cont__EAssignment_8_1 ) ) )
            // InternalSTL.g:2233:1: ( ( rule__Expression_cont__EAssignment_8_1 ) )
            {
            // InternalSTL.g:2233:1: ( ( rule__Expression_cont__EAssignment_8_1 ) )
            // InternalSTL.g:2234:2: ( rule__Expression_cont__EAssignment_8_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_8_1()); 
            // InternalSTL.g:2235:2: ( rule__Expression_cont__EAssignment_8_1 )
            // InternalSTL.g:2235:3: rule__Expression_cont__EAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_8__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_9__0"
    // InternalSTL.g:2244:1: rule__Expression_cont__Group_9__0 : rule__Expression_cont__Group_9__0__Impl rule__Expression_cont__Group_9__1 ;
    public final void rule__Expression_cont__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2248:1: ( rule__Expression_cont__Group_9__0__Impl rule__Expression_cont__Group_9__1 )
            // InternalSTL.g:2249:2: rule__Expression_cont__Group_9__0__Impl rule__Expression_cont__Group_9__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_9__0"


    // $ANTLR start "rule__Expression_cont__Group_9__0__Impl"
    // InternalSTL.g:2256:1: rule__Expression_cont__Group_9__0__Impl : ( ruleAndOperator ) ;
    public final void rule__Expression_cont__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2260:1: ( ( ruleAndOperator ) )
            // InternalSTL.g:2261:1: ( ruleAndOperator )
            {
            // InternalSTL.g:2261:1: ( ruleAndOperator )
            // InternalSTL.g:2262:2: ruleAndOperator
            {
             before(grammarAccess.getExpression_contAccess().getAndOperatorParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleAndOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getAndOperatorParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_9__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_9__1"
    // InternalSTL.g:2271:1: rule__Expression_cont__Group_9__1 : rule__Expression_cont__Group_9__1__Impl ;
    public final void rule__Expression_cont__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2275:1: ( rule__Expression_cont__Group_9__1__Impl )
            // InternalSTL.g:2276:2: rule__Expression_cont__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_9__1"


    // $ANTLR start "rule__Expression_cont__Group_9__1__Impl"
    // InternalSTL.g:2282:1: rule__Expression_cont__Group_9__1__Impl : ( ( rule__Expression_cont__EAssignment_9_1 ) ) ;
    public final void rule__Expression_cont__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2286:1: ( ( ( rule__Expression_cont__EAssignment_9_1 ) ) )
            // InternalSTL.g:2287:1: ( ( rule__Expression_cont__EAssignment_9_1 ) )
            {
            // InternalSTL.g:2287:1: ( ( rule__Expression_cont__EAssignment_9_1 ) )
            // InternalSTL.g:2288:2: ( rule__Expression_cont__EAssignment_9_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_9_1()); 
            // InternalSTL.g:2289:2: ( rule__Expression_cont__EAssignment_9_1 )
            // InternalSTL.g:2289:3: rule__Expression_cont__EAssignment_9_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_9_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_9__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_10__0"
    // InternalSTL.g:2298:1: rule__Expression_cont__Group_10__0 : rule__Expression_cont__Group_10__0__Impl rule__Expression_cont__Group_10__1 ;
    public final void rule__Expression_cont__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2302:1: ( rule__Expression_cont__Group_10__0__Impl rule__Expression_cont__Group_10__1 )
            // InternalSTL.g:2303:2: rule__Expression_cont__Group_10__0__Impl rule__Expression_cont__Group_10__1
            {
            pushFollow(FOLLOW_11);
            rule__Expression_cont__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_10__0"


    // $ANTLR start "rule__Expression_cont__Group_10__0__Impl"
    // InternalSTL.g:2310:1: rule__Expression_cont__Group_10__0__Impl : ( ruleImpliesOperator ) ;
    public final void rule__Expression_cont__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2314:1: ( ( ruleImpliesOperator ) )
            // InternalSTL.g:2315:1: ( ruleImpliesOperator )
            {
            // InternalSTL.g:2315:1: ( ruleImpliesOperator )
            // InternalSTL.g:2316:2: ruleImpliesOperator
            {
             before(grammarAccess.getExpression_contAccess().getImpliesOperatorParserRuleCall_10_0()); 
            pushFollow(FOLLOW_2);
            ruleImpliesOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getImpliesOperatorParserRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_10__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_10__1"
    // InternalSTL.g:2325:1: rule__Expression_cont__Group_10__1 : rule__Expression_cont__Group_10__1__Impl ;
    public final void rule__Expression_cont__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2329:1: ( rule__Expression_cont__Group_10__1__Impl )
            // InternalSTL.g:2330:2: rule__Expression_cont__Group_10__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_10__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_10__1"


    // $ANTLR start "rule__Expression_cont__Group_10__1__Impl"
    // InternalSTL.g:2336:1: rule__Expression_cont__Group_10__1__Impl : ( ( rule__Expression_cont__Alternatives_10_1 ) ) ;
    public final void rule__Expression_cont__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2340:1: ( ( ( rule__Expression_cont__Alternatives_10_1 ) ) )
            // InternalSTL.g:2341:1: ( ( rule__Expression_cont__Alternatives_10_1 ) )
            {
            // InternalSTL.g:2341:1: ( ( rule__Expression_cont__Alternatives_10_1 ) )
            // InternalSTL.g:2342:2: ( rule__Expression_cont__Alternatives_10_1 )
            {
             before(grammarAccess.getExpression_contAccess().getAlternatives_10_1()); 
            // InternalSTL.g:2343:2: ( rule__Expression_cont__Alternatives_10_1 )
            // InternalSTL.g:2343:3: rule__Expression_cont__Alternatives_10_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Alternatives_10_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getAlternatives_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_10__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_11__0"
    // InternalSTL.g:2352:1: rule__Expression_cont__Group_11__0 : rule__Expression_cont__Group_11__0__Impl rule__Expression_cont__Group_11__1 ;
    public final void rule__Expression_cont__Group_11__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2356:1: ( rule__Expression_cont__Group_11__0__Impl rule__Expression_cont__Group_11__1 )
            // InternalSTL.g:2357:2: rule__Expression_cont__Group_11__0__Impl rule__Expression_cont__Group_11__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_11__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_11__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_11__0"


    // $ANTLR start "rule__Expression_cont__Group_11__0__Impl"
    // InternalSTL.g:2364:1: rule__Expression_cont__Group_11__0__Impl : ( ruleIffOperator ) ;
    public final void rule__Expression_cont__Group_11__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2368:1: ( ( ruleIffOperator ) )
            // InternalSTL.g:2369:1: ( ruleIffOperator )
            {
            // InternalSTL.g:2369:1: ( ruleIffOperator )
            // InternalSTL.g:2370:2: ruleIffOperator
            {
             before(grammarAccess.getExpression_contAccess().getIffOperatorParserRuleCall_11_0()); 
            pushFollow(FOLLOW_2);
            ruleIffOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getIffOperatorParserRuleCall_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_11__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_11__1"
    // InternalSTL.g:2379:1: rule__Expression_cont__Group_11__1 : rule__Expression_cont__Group_11__1__Impl ;
    public final void rule__Expression_cont__Group_11__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2383:1: ( rule__Expression_cont__Group_11__1__Impl )
            // InternalSTL.g:2384:2: rule__Expression_cont__Group_11__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_11__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_11__1"


    // $ANTLR start "rule__Expression_cont__Group_11__1__Impl"
    // InternalSTL.g:2390:1: rule__Expression_cont__Group_11__1__Impl : ( ( rule__Expression_cont__EAssignment_11_1 ) ) ;
    public final void rule__Expression_cont__Group_11__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2394:1: ( ( ( rule__Expression_cont__EAssignment_11_1 ) ) )
            // InternalSTL.g:2395:1: ( ( rule__Expression_cont__EAssignment_11_1 ) )
            {
            // InternalSTL.g:2395:1: ( ( rule__Expression_cont__EAssignment_11_1 ) )
            // InternalSTL.g:2396:2: ( rule__Expression_cont__EAssignment_11_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_11_1()); 
            // InternalSTL.g:2397:2: ( rule__Expression_cont__EAssignment_11_1 )
            // InternalSTL.g:2397:3: rule__Expression_cont__EAssignment_11_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_11_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_11_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_11__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_12__0"
    // InternalSTL.g:2406:1: rule__Expression_cont__Group_12__0 : rule__Expression_cont__Group_12__0__Impl rule__Expression_cont__Group_12__1 ;
    public final void rule__Expression_cont__Group_12__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2410:1: ( rule__Expression_cont__Group_12__0__Impl rule__Expression_cont__Group_12__1 )
            // InternalSTL.g:2411:2: rule__Expression_cont__Group_12__0__Impl rule__Expression_cont__Group_12__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_12__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_12__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_12__0"


    // $ANTLR start "rule__Expression_cont__Group_12__0__Impl"
    // InternalSTL.g:2418:1: rule__Expression_cont__Group_12__0__Impl : ( ruleXorOperator ) ;
    public final void rule__Expression_cont__Group_12__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2422:1: ( ( ruleXorOperator ) )
            // InternalSTL.g:2423:1: ( ruleXorOperator )
            {
            // InternalSTL.g:2423:1: ( ruleXorOperator )
            // InternalSTL.g:2424:2: ruleXorOperator
            {
             before(grammarAccess.getExpression_contAccess().getXorOperatorParserRuleCall_12_0()); 
            pushFollow(FOLLOW_2);
            ruleXorOperator();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getXorOperatorParserRuleCall_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_12__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_12__1"
    // InternalSTL.g:2433:1: rule__Expression_cont__Group_12__1 : rule__Expression_cont__Group_12__1__Impl ;
    public final void rule__Expression_cont__Group_12__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2437:1: ( rule__Expression_cont__Group_12__1__Impl )
            // InternalSTL.g:2438:2: rule__Expression_cont__Group_12__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_12__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_12__1"


    // $ANTLR start "rule__Expression_cont__Group_12__1__Impl"
    // InternalSTL.g:2444:1: rule__Expression_cont__Group_12__1__Impl : ( ( rule__Expression_cont__EAssignment_12_1 ) ) ;
    public final void rule__Expression_cont__Group_12__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2448:1: ( ( ( rule__Expression_cont__EAssignment_12_1 ) ) )
            // InternalSTL.g:2449:1: ( ( rule__Expression_cont__EAssignment_12_1 ) )
            {
            // InternalSTL.g:2449:1: ( ( rule__Expression_cont__EAssignment_12_1 ) )
            // InternalSTL.g:2450:2: ( rule__Expression_cont__EAssignment_12_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_12_1()); 
            // InternalSTL.g:2451:2: ( rule__Expression_cont__EAssignment_12_1 )
            // InternalSTL.g:2451:3: rule__Expression_cont__EAssignment_12_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_12_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_12_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_12__1__Impl"


    // $ANTLR start "rule__Expression_cont__Group_13__0"
    // InternalSTL.g:2460:1: rule__Expression_cont__Group_13__0 : rule__Expression_cont__Group_13__0__Impl rule__Expression_cont__Group_13__1 ;
    public final void rule__Expression_cont__Group_13__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2464:1: ( rule__Expression_cont__Group_13__0__Impl rule__Expression_cont__Group_13__1 )
            // InternalSTL.g:2465:2: rule__Expression_cont__Group_13__0__Impl rule__Expression_cont__Group_13__1
            {
            pushFollow(FOLLOW_5);
            rule__Expression_cont__Group_13__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_13__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_13__0"


    // $ANTLR start "rule__Expression_cont__Group_13__0__Impl"
    // InternalSTL.g:2472:1: rule__Expression_cont__Group_13__0__Impl : ( rulecomparisonOp ) ;
    public final void rule__Expression_cont__Group_13__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2476:1: ( ( rulecomparisonOp ) )
            // InternalSTL.g:2477:1: ( rulecomparisonOp )
            {
            // InternalSTL.g:2477:1: ( rulecomparisonOp )
            // InternalSTL.g:2478:2: rulecomparisonOp
            {
             before(grammarAccess.getExpression_contAccess().getComparisonOpParserRuleCall_13_0()); 
            pushFollow(FOLLOW_2);
            rulecomparisonOp();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getComparisonOpParserRuleCall_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_13__0__Impl"


    // $ANTLR start "rule__Expression_cont__Group_13__1"
    // InternalSTL.g:2487:1: rule__Expression_cont__Group_13__1 : rule__Expression_cont__Group_13__1__Impl ;
    public final void rule__Expression_cont__Group_13__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2491:1: ( rule__Expression_cont__Group_13__1__Impl )
            // InternalSTL.g:2492:2: rule__Expression_cont__Group_13__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__Group_13__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_13__1"


    // $ANTLR start "rule__Expression_cont__Group_13__1__Impl"
    // InternalSTL.g:2498:1: rule__Expression_cont__Group_13__1__Impl : ( ( rule__Expression_cont__EAssignment_13_1 ) ) ;
    public final void rule__Expression_cont__Group_13__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2502:1: ( ( ( rule__Expression_cont__EAssignment_13_1 ) ) )
            // InternalSTL.g:2503:1: ( ( rule__Expression_cont__EAssignment_13_1 ) )
            {
            // InternalSTL.g:2503:1: ( ( rule__Expression_cont__EAssignment_13_1 ) )
            // InternalSTL.g:2504:2: ( rule__Expression_cont__EAssignment_13_1 )
            {
             before(grammarAccess.getExpression_contAccess().getEAssignment_13_1()); 
            // InternalSTL.g:2505:2: ( rule__Expression_cont__EAssignment_13_1 )
            // InternalSTL.g:2505:3: rule__Expression_cont__EAssignment_13_1
            {
            pushFollow(FOLLOW_2);
            rule__Expression_cont__EAssignment_13_1();

            state._fsp--;


            }

             after(grammarAccess.getExpression_contAccess().getEAssignment_13_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__Group_13__1__Impl"


    // $ANTLR start "rule__Real_expression__Group_2__0"
    // InternalSTL.g:2514:1: rule__Real_expression__Group_2__0 : rule__Real_expression__Group_2__0__Impl rule__Real_expression__Group_2__1 ;
    public final void rule__Real_expression__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2518:1: ( rule__Real_expression__Group_2__0__Impl rule__Real_expression__Group_2__1 )
            // InternalSTL.g:2519:2: rule__Real_expression__Group_2__0__Impl rule__Real_expression__Group_2__1
            {
            pushFollow(FOLLOW_10);
            rule__Real_expression__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__0"


    // $ANTLR start "rule__Real_expression__Group_2__0__Impl"
    // InternalSTL.g:2526:1: rule__Real_expression__Group_2__0__Impl : ( RULE_ABS ) ;
    public final void rule__Real_expression__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2530:1: ( ( RULE_ABS ) )
            // InternalSTL.g:2531:1: ( RULE_ABS )
            {
            // InternalSTL.g:2531:1: ( RULE_ABS )
            // InternalSTL.g:2532:2: RULE_ABS
            {
             before(grammarAccess.getReal_expressionAccess().getABSTerminalRuleCall_2_0()); 
            match(input,RULE_ABS,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getABSTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__0__Impl"


    // $ANTLR start "rule__Real_expression__Group_2__1"
    // InternalSTL.g:2541:1: rule__Real_expression__Group_2__1 : rule__Real_expression__Group_2__1__Impl rule__Real_expression__Group_2__2 ;
    public final void rule__Real_expression__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2545:1: ( rule__Real_expression__Group_2__1__Impl rule__Real_expression__Group_2__2 )
            // InternalSTL.g:2546:2: rule__Real_expression__Group_2__1__Impl rule__Real_expression__Group_2__2
            {
            pushFollow(FOLLOW_7);
            rule__Real_expression__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__1"


    // $ANTLR start "rule__Real_expression__Group_2__1__Impl"
    // InternalSTL.g:2553:1: rule__Real_expression__Group_2__1__Impl : ( RULE_LPAREN ) ;
    public final void rule__Real_expression__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2557:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:2558:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:2558:1: ( RULE_LPAREN )
            // InternalSTL.g:2559:2: RULE_LPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_2_1()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__1__Impl"


    // $ANTLR start "rule__Real_expression__Group_2__2"
    // InternalSTL.g:2568:1: rule__Real_expression__Group_2__2 : rule__Real_expression__Group_2__2__Impl rule__Real_expression__Group_2__3 ;
    public final void rule__Real_expression__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2572:1: ( rule__Real_expression__Group_2__2__Impl rule__Real_expression__Group_2__3 )
            // InternalSTL.g:2573:2: rule__Real_expression__Group_2__2__Impl rule__Real_expression__Group_2__3
            {
            pushFollow(FOLLOW_8);
            rule__Real_expression__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__2"


    // $ANTLR start "rule__Real_expression__Group_2__2__Impl"
    // InternalSTL.g:2580:1: rule__Real_expression__Group_2__2__Impl : ( ( rule__Real_expression__ExpAssignment_2_2 ) ) ;
    public final void rule__Real_expression__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2584:1: ( ( ( rule__Real_expression__ExpAssignment_2_2 ) ) )
            // InternalSTL.g:2585:1: ( ( rule__Real_expression__ExpAssignment_2_2 ) )
            {
            // InternalSTL.g:2585:1: ( ( rule__Real_expression__ExpAssignment_2_2 ) )
            // InternalSTL.g:2586:2: ( rule__Real_expression__ExpAssignment_2_2 )
            {
             before(grammarAccess.getReal_expressionAccess().getExpAssignment_2_2()); 
            // InternalSTL.g:2587:2: ( rule__Real_expression__ExpAssignment_2_2 )
            // InternalSTL.g:2587:3: rule__Real_expression__ExpAssignment_2_2
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__ExpAssignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getReal_expressionAccess().getExpAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__2__Impl"


    // $ANTLR start "rule__Real_expression__Group_2__3"
    // InternalSTL.g:2595:1: rule__Real_expression__Group_2__3 : rule__Real_expression__Group_2__3__Impl ;
    public final void rule__Real_expression__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2599:1: ( rule__Real_expression__Group_2__3__Impl )
            // InternalSTL.g:2600:2: rule__Real_expression__Group_2__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__3"


    // $ANTLR start "rule__Real_expression__Group_2__3__Impl"
    // InternalSTL.g:2606:1: rule__Real_expression__Group_2__3__Impl : ( RULE_RPAREN ) ;
    public final void rule__Real_expression__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2610:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:2611:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:2611:1: ( RULE_RPAREN )
            // InternalSTL.g:2612:2: RULE_RPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_2_3()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_2__3__Impl"


    // $ANTLR start "rule__Real_expression__Group_3__0"
    // InternalSTL.g:2622:1: rule__Real_expression__Group_3__0 : rule__Real_expression__Group_3__0__Impl rule__Real_expression__Group_3__1 ;
    public final void rule__Real_expression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2626:1: ( rule__Real_expression__Group_3__0__Impl rule__Real_expression__Group_3__1 )
            // InternalSTL.g:2627:2: rule__Real_expression__Group_3__0__Impl rule__Real_expression__Group_3__1
            {
            pushFollow(FOLLOW_10);
            rule__Real_expression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__0"


    // $ANTLR start "rule__Real_expression__Group_3__0__Impl"
    // InternalSTL.g:2634:1: rule__Real_expression__Group_3__0__Impl : ( RULE_SQRT ) ;
    public final void rule__Real_expression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2638:1: ( ( RULE_SQRT ) )
            // InternalSTL.g:2639:1: ( RULE_SQRT )
            {
            // InternalSTL.g:2639:1: ( RULE_SQRT )
            // InternalSTL.g:2640:2: RULE_SQRT
            {
             before(grammarAccess.getReal_expressionAccess().getSQRTTerminalRuleCall_3_0()); 
            match(input,RULE_SQRT,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getSQRTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__0__Impl"


    // $ANTLR start "rule__Real_expression__Group_3__1"
    // InternalSTL.g:2649:1: rule__Real_expression__Group_3__1 : rule__Real_expression__Group_3__1__Impl rule__Real_expression__Group_3__2 ;
    public final void rule__Real_expression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2653:1: ( rule__Real_expression__Group_3__1__Impl rule__Real_expression__Group_3__2 )
            // InternalSTL.g:2654:2: rule__Real_expression__Group_3__1__Impl rule__Real_expression__Group_3__2
            {
            pushFollow(FOLLOW_7);
            rule__Real_expression__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__1"


    // $ANTLR start "rule__Real_expression__Group_3__1__Impl"
    // InternalSTL.g:2661:1: rule__Real_expression__Group_3__1__Impl : ( RULE_LPAREN ) ;
    public final void rule__Real_expression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2665:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:2666:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:2666:1: ( RULE_LPAREN )
            // InternalSTL.g:2667:2: RULE_LPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_3_1()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__1__Impl"


    // $ANTLR start "rule__Real_expression__Group_3__2"
    // InternalSTL.g:2676:1: rule__Real_expression__Group_3__2 : rule__Real_expression__Group_3__2__Impl rule__Real_expression__Group_3__3 ;
    public final void rule__Real_expression__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2680:1: ( rule__Real_expression__Group_3__2__Impl rule__Real_expression__Group_3__3 )
            // InternalSTL.g:2681:2: rule__Real_expression__Group_3__2__Impl rule__Real_expression__Group_3__3
            {
            pushFollow(FOLLOW_8);
            rule__Real_expression__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__2"


    // $ANTLR start "rule__Real_expression__Group_3__2__Impl"
    // InternalSTL.g:2688:1: rule__Real_expression__Group_3__2__Impl : ( ( rule__Real_expression__ExpAssignment_3_2 ) ) ;
    public final void rule__Real_expression__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2692:1: ( ( ( rule__Real_expression__ExpAssignment_3_2 ) ) )
            // InternalSTL.g:2693:1: ( ( rule__Real_expression__ExpAssignment_3_2 ) )
            {
            // InternalSTL.g:2693:1: ( ( rule__Real_expression__ExpAssignment_3_2 ) )
            // InternalSTL.g:2694:2: ( rule__Real_expression__ExpAssignment_3_2 )
            {
             before(grammarAccess.getReal_expressionAccess().getExpAssignment_3_2()); 
            // InternalSTL.g:2695:2: ( rule__Real_expression__ExpAssignment_3_2 )
            // InternalSTL.g:2695:3: rule__Real_expression__ExpAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__ExpAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getReal_expressionAccess().getExpAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__2__Impl"


    // $ANTLR start "rule__Real_expression__Group_3__3"
    // InternalSTL.g:2703:1: rule__Real_expression__Group_3__3 : rule__Real_expression__Group_3__3__Impl ;
    public final void rule__Real_expression__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2707:1: ( rule__Real_expression__Group_3__3__Impl )
            // InternalSTL.g:2708:2: rule__Real_expression__Group_3__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_3__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__3"


    // $ANTLR start "rule__Real_expression__Group_3__3__Impl"
    // InternalSTL.g:2714:1: rule__Real_expression__Group_3__3__Impl : ( RULE_RPAREN ) ;
    public final void rule__Real_expression__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2718:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:2719:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:2719:1: ( RULE_RPAREN )
            // InternalSTL.g:2720:2: RULE_RPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_3_3()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_3__3__Impl"


    // $ANTLR start "rule__Real_expression__Group_4__0"
    // InternalSTL.g:2730:1: rule__Real_expression__Group_4__0 : rule__Real_expression__Group_4__0__Impl rule__Real_expression__Group_4__1 ;
    public final void rule__Real_expression__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2734:1: ( rule__Real_expression__Group_4__0__Impl rule__Real_expression__Group_4__1 )
            // InternalSTL.g:2735:2: rule__Real_expression__Group_4__0__Impl rule__Real_expression__Group_4__1
            {
            pushFollow(FOLLOW_10);
            rule__Real_expression__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__0"


    // $ANTLR start "rule__Real_expression__Group_4__0__Impl"
    // InternalSTL.g:2742:1: rule__Real_expression__Group_4__0__Impl : ( RULE_EXP ) ;
    public final void rule__Real_expression__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2746:1: ( ( RULE_EXP ) )
            // InternalSTL.g:2747:1: ( RULE_EXP )
            {
            // InternalSTL.g:2747:1: ( RULE_EXP )
            // InternalSTL.g:2748:2: RULE_EXP
            {
             before(grammarAccess.getReal_expressionAccess().getEXPTerminalRuleCall_4_0()); 
            match(input,RULE_EXP,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getEXPTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__0__Impl"


    // $ANTLR start "rule__Real_expression__Group_4__1"
    // InternalSTL.g:2757:1: rule__Real_expression__Group_4__1 : rule__Real_expression__Group_4__1__Impl rule__Real_expression__Group_4__2 ;
    public final void rule__Real_expression__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2761:1: ( rule__Real_expression__Group_4__1__Impl rule__Real_expression__Group_4__2 )
            // InternalSTL.g:2762:2: rule__Real_expression__Group_4__1__Impl rule__Real_expression__Group_4__2
            {
            pushFollow(FOLLOW_7);
            rule__Real_expression__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__1"


    // $ANTLR start "rule__Real_expression__Group_4__1__Impl"
    // InternalSTL.g:2769:1: rule__Real_expression__Group_4__1__Impl : ( RULE_LPAREN ) ;
    public final void rule__Real_expression__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2773:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:2774:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:2774:1: ( RULE_LPAREN )
            // InternalSTL.g:2775:2: RULE_LPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_4_1()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__1__Impl"


    // $ANTLR start "rule__Real_expression__Group_4__2"
    // InternalSTL.g:2784:1: rule__Real_expression__Group_4__2 : rule__Real_expression__Group_4__2__Impl rule__Real_expression__Group_4__3 ;
    public final void rule__Real_expression__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2788:1: ( rule__Real_expression__Group_4__2__Impl rule__Real_expression__Group_4__3 )
            // InternalSTL.g:2789:2: rule__Real_expression__Group_4__2__Impl rule__Real_expression__Group_4__3
            {
            pushFollow(FOLLOW_8);
            rule__Real_expression__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__2"


    // $ANTLR start "rule__Real_expression__Group_4__2__Impl"
    // InternalSTL.g:2796:1: rule__Real_expression__Group_4__2__Impl : ( ( rule__Real_expression__ExpAssignment_4_2 ) ) ;
    public final void rule__Real_expression__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2800:1: ( ( ( rule__Real_expression__ExpAssignment_4_2 ) ) )
            // InternalSTL.g:2801:1: ( ( rule__Real_expression__ExpAssignment_4_2 ) )
            {
            // InternalSTL.g:2801:1: ( ( rule__Real_expression__ExpAssignment_4_2 ) )
            // InternalSTL.g:2802:2: ( rule__Real_expression__ExpAssignment_4_2 )
            {
             before(grammarAccess.getReal_expressionAccess().getExpAssignment_4_2()); 
            // InternalSTL.g:2803:2: ( rule__Real_expression__ExpAssignment_4_2 )
            // InternalSTL.g:2803:3: rule__Real_expression__ExpAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__ExpAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getReal_expressionAccess().getExpAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__2__Impl"


    // $ANTLR start "rule__Real_expression__Group_4__3"
    // InternalSTL.g:2811:1: rule__Real_expression__Group_4__3 : rule__Real_expression__Group_4__3__Impl ;
    public final void rule__Real_expression__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2815:1: ( rule__Real_expression__Group_4__3__Impl )
            // InternalSTL.g:2816:2: rule__Real_expression__Group_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__3"


    // $ANTLR start "rule__Real_expression__Group_4__3__Impl"
    // InternalSTL.g:2822:1: rule__Real_expression__Group_4__3__Impl : ( RULE_RPAREN ) ;
    public final void rule__Real_expression__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2826:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:2827:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:2827:1: ( RULE_RPAREN )
            // InternalSTL.g:2828:2: RULE_RPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_4_3()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_4__3__Impl"


    // $ANTLR start "rule__Real_expression__Group_5__0"
    // InternalSTL.g:2838:1: rule__Real_expression__Group_5__0 : rule__Real_expression__Group_5__0__Impl rule__Real_expression__Group_5__1 ;
    public final void rule__Real_expression__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2842:1: ( rule__Real_expression__Group_5__0__Impl rule__Real_expression__Group_5__1 )
            // InternalSTL.g:2843:2: rule__Real_expression__Group_5__0__Impl rule__Real_expression__Group_5__1
            {
            pushFollow(FOLLOW_10);
            rule__Real_expression__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__0"


    // $ANTLR start "rule__Real_expression__Group_5__0__Impl"
    // InternalSTL.g:2850:1: rule__Real_expression__Group_5__0__Impl : ( RULE_POW ) ;
    public final void rule__Real_expression__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2854:1: ( ( RULE_POW ) )
            // InternalSTL.g:2855:1: ( RULE_POW )
            {
            // InternalSTL.g:2855:1: ( RULE_POW )
            // InternalSTL.g:2856:2: RULE_POW
            {
             before(grammarAccess.getReal_expressionAccess().getPOWTerminalRuleCall_5_0()); 
            match(input,RULE_POW,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getPOWTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__0__Impl"


    // $ANTLR start "rule__Real_expression__Group_5__1"
    // InternalSTL.g:2865:1: rule__Real_expression__Group_5__1 : rule__Real_expression__Group_5__1__Impl rule__Real_expression__Group_5__2 ;
    public final void rule__Real_expression__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2869:1: ( rule__Real_expression__Group_5__1__Impl rule__Real_expression__Group_5__2 )
            // InternalSTL.g:2870:2: rule__Real_expression__Group_5__1__Impl rule__Real_expression__Group_5__2
            {
            pushFollow(FOLLOW_7);
            rule__Real_expression__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__1"


    // $ANTLR start "rule__Real_expression__Group_5__1__Impl"
    // InternalSTL.g:2877:1: rule__Real_expression__Group_5__1__Impl : ( RULE_LPAREN ) ;
    public final void rule__Real_expression__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2881:1: ( ( RULE_LPAREN ) )
            // InternalSTL.g:2882:1: ( RULE_LPAREN )
            {
            // InternalSTL.g:2882:1: ( RULE_LPAREN )
            // InternalSTL.g:2883:2: RULE_LPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_5_1()); 
            match(input,RULE_LPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getLPARENTerminalRuleCall_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__1__Impl"


    // $ANTLR start "rule__Real_expression__Group_5__2"
    // InternalSTL.g:2892:1: rule__Real_expression__Group_5__2 : rule__Real_expression__Group_5__2__Impl rule__Real_expression__Group_5__3 ;
    public final void rule__Real_expression__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2896:1: ( rule__Real_expression__Group_5__2__Impl rule__Real_expression__Group_5__3 )
            // InternalSTL.g:2897:2: rule__Real_expression__Group_5__2__Impl rule__Real_expression__Group_5__3
            {
            pushFollow(FOLLOW_12);
            rule__Real_expression__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__2"


    // $ANTLR start "rule__Real_expression__Group_5__2__Impl"
    // InternalSTL.g:2904:1: rule__Real_expression__Group_5__2__Impl : ( ( rule__Real_expression__Exp1Assignment_5_2 ) ) ;
    public final void rule__Real_expression__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2908:1: ( ( ( rule__Real_expression__Exp1Assignment_5_2 ) ) )
            // InternalSTL.g:2909:1: ( ( rule__Real_expression__Exp1Assignment_5_2 ) )
            {
            // InternalSTL.g:2909:1: ( ( rule__Real_expression__Exp1Assignment_5_2 ) )
            // InternalSTL.g:2910:2: ( rule__Real_expression__Exp1Assignment_5_2 )
            {
             before(grammarAccess.getReal_expressionAccess().getExp1Assignment_5_2()); 
            // InternalSTL.g:2911:2: ( rule__Real_expression__Exp1Assignment_5_2 )
            // InternalSTL.g:2911:3: rule__Real_expression__Exp1Assignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Exp1Assignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getReal_expressionAccess().getExp1Assignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__2__Impl"


    // $ANTLR start "rule__Real_expression__Group_5__3"
    // InternalSTL.g:2919:1: rule__Real_expression__Group_5__3 : rule__Real_expression__Group_5__3__Impl rule__Real_expression__Group_5__4 ;
    public final void rule__Real_expression__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2923:1: ( rule__Real_expression__Group_5__3__Impl rule__Real_expression__Group_5__4 )
            // InternalSTL.g:2924:2: rule__Real_expression__Group_5__3__Impl rule__Real_expression__Group_5__4
            {
            pushFollow(FOLLOW_7);
            rule__Real_expression__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__3"


    // $ANTLR start "rule__Real_expression__Group_5__3__Impl"
    // InternalSTL.g:2931:1: rule__Real_expression__Group_5__3__Impl : ( RULE_COMMA ) ;
    public final void rule__Real_expression__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2935:1: ( ( RULE_COMMA ) )
            // InternalSTL.g:2936:1: ( RULE_COMMA )
            {
            // InternalSTL.g:2936:1: ( RULE_COMMA )
            // InternalSTL.g:2937:2: RULE_COMMA
            {
             before(grammarAccess.getReal_expressionAccess().getCOMMATerminalRuleCall_5_3()); 
            match(input,RULE_COMMA,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getCOMMATerminalRuleCall_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__3__Impl"


    // $ANTLR start "rule__Real_expression__Group_5__4"
    // InternalSTL.g:2946:1: rule__Real_expression__Group_5__4 : rule__Real_expression__Group_5__4__Impl rule__Real_expression__Group_5__5 ;
    public final void rule__Real_expression__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2950:1: ( rule__Real_expression__Group_5__4__Impl rule__Real_expression__Group_5__5 )
            // InternalSTL.g:2951:2: rule__Real_expression__Group_5__4__Impl rule__Real_expression__Group_5__5
            {
            pushFollow(FOLLOW_8);
            rule__Real_expression__Group_5__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_5__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__4"


    // $ANTLR start "rule__Real_expression__Group_5__4__Impl"
    // InternalSTL.g:2958:1: rule__Real_expression__Group_5__4__Impl : ( ( rule__Real_expression__Exp2Assignment_5_4 ) ) ;
    public final void rule__Real_expression__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2962:1: ( ( ( rule__Real_expression__Exp2Assignment_5_4 ) ) )
            // InternalSTL.g:2963:1: ( ( rule__Real_expression__Exp2Assignment_5_4 ) )
            {
            // InternalSTL.g:2963:1: ( ( rule__Real_expression__Exp2Assignment_5_4 ) )
            // InternalSTL.g:2964:2: ( rule__Real_expression__Exp2Assignment_5_4 )
            {
             before(grammarAccess.getReal_expressionAccess().getExp2Assignment_5_4()); 
            // InternalSTL.g:2965:2: ( rule__Real_expression__Exp2Assignment_5_4 )
            // InternalSTL.g:2965:3: rule__Real_expression__Exp2Assignment_5_4
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Exp2Assignment_5_4();

            state._fsp--;


            }

             after(grammarAccess.getReal_expressionAccess().getExp2Assignment_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__4__Impl"


    // $ANTLR start "rule__Real_expression__Group_5__5"
    // InternalSTL.g:2973:1: rule__Real_expression__Group_5__5 : rule__Real_expression__Group_5__5__Impl ;
    public final void rule__Real_expression__Group_5__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2977:1: ( rule__Real_expression__Group_5__5__Impl )
            // InternalSTL.g:2978:2: rule__Real_expression__Group_5__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Real_expression__Group_5__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__5"


    // $ANTLR start "rule__Real_expression__Group_5__5__Impl"
    // InternalSTL.g:2984:1: rule__Real_expression__Group_5__5__Impl : ( RULE_RPAREN ) ;
    public final void rule__Real_expression__Group_5__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:2988:1: ( ( RULE_RPAREN ) )
            // InternalSTL.g:2989:1: ( RULE_RPAREN )
            {
            // InternalSTL.g:2989:1: ( RULE_RPAREN )
            // InternalSTL.g:2990:2: RULE_RPAREN
            {
             before(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_5_5()); 
            match(input,RULE_RPAREN,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getRPARENTerminalRuleCall_5_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Group_5__5__Impl"


    // $ANTLR start "rule__Num_literal__Group_2__0"
    // InternalSTL.g:3000:1: rule__Num_literal__Group_2__0 : rule__Num_literal__Group_2__0__Impl rule__Num_literal__Group_2__1 ;
    public final void rule__Num_literal__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3004:1: ( rule__Num_literal__Group_2__0__Impl rule__Num_literal__Group_2__1 )
            // InternalSTL.g:3005:2: rule__Num_literal__Group_2__0__Impl rule__Num_literal__Group_2__1
            {
            pushFollow(FOLLOW_13);
            rule__Num_literal__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Num_literal__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__Group_2__0"


    // $ANTLR start "rule__Num_literal__Group_2__0__Impl"
    // InternalSTL.g:3012:1: rule__Num_literal__Group_2__0__Impl : ( RULE_MINUS ) ;
    public final void rule__Num_literal__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3016:1: ( ( RULE_MINUS ) )
            // InternalSTL.g:3017:1: ( RULE_MINUS )
            {
            // InternalSTL.g:3017:1: ( RULE_MINUS )
            // InternalSTL.g:3018:2: RULE_MINUS
            {
             before(grammarAccess.getNum_literalAccess().getMINUSTerminalRuleCall_2_0()); 
            match(input,RULE_MINUS,FOLLOW_2); 
             after(grammarAccess.getNum_literalAccess().getMINUSTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__Group_2__0__Impl"


    // $ANTLR start "rule__Num_literal__Group_2__1"
    // InternalSTL.g:3027:1: rule__Num_literal__Group_2__1 : rule__Num_literal__Group_2__1__Impl ;
    public final void rule__Num_literal__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3031:1: ( rule__Num_literal__Group_2__1__Impl )
            // InternalSTL.g:3032:2: rule__Num_literal__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Num_literal__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__Group_2__1"


    // $ANTLR start "rule__Num_literal__Group_2__1__Impl"
    // InternalSTL.g:3038:1: rule__Num_literal__Group_2__1__Impl : ( ( rule__Num_literal__LitAssignment_2_1 ) ) ;
    public final void rule__Num_literal__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3042:1: ( ( ( rule__Num_literal__LitAssignment_2_1 ) ) )
            // InternalSTL.g:3043:1: ( ( rule__Num_literal__LitAssignment_2_1 ) )
            {
            // InternalSTL.g:3043:1: ( ( rule__Num_literal__LitAssignment_2_1 ) )
            // InternalSTL.g:3044:2: ( rule__Num_literal__LitAssignment_2_1 )
            {
             before(grammarAccess.getNum_literalAccess().getLitAssignment_2_1()); 
            // InternalSTL.g:3045:2: ( rule__Num_literal__LitAssignment_2_1 )
            // InternalSTL.g:3045:3: rule__Num_literal__LitAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Num_literal__LitAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getNum_literalAccess().getLitAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__Group_2__1__Impl"


    // $ANTLR start "rule__Interval__Group__0"
    // InternalSTL.g:3054:1: rule__Interval__Group__0 : rule__Interval__Group__0__Impl rule__Interval__Group__1 ;
    public final void rule__Interval__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3058:1: ( rule__Interval__Group__0__Impl rule__Interval__Group__1 )
            // InternalSTL.g:3059:2: rule__Interval__Group__0__Impl rule__Interval__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Interval__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interval__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__0"


    // $ANTLR start "rule__Interval__Group__0__Impl"
    // InternalSTL.g:3066:1: rule__Interval__Group__0__Impl : ( RULE_LBRACK ) ;
    public final void rule__Interval__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3070:1: ( ( RULE_LBRACK ) )
            // InternalSTL.g:3071:1: ( RULE_LBRACK )
            {
            // InternalSTL.g:3071:1: ( RULE_LBRACK )
            // InternalSTL.g:3072:2: RULE_LBRACK
            {
             before(grammarAccess.getIntervalAccess().getLBRACKTerminalRuleCall_0()); 
            match(input,RULE_LBRACK,FOLLOW_2); 
             after(grammarAccess.getIntervalAccess().getLBRACKTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__0__Impl"


    // $ANTLR start "rule__Interval__Group__1"
    // InternalSTL.g:3081:1: rule__Interval__Group__1 : rule__Interval__Group__1__Impl rule__Interval__Group__2 ;
    public final void rule__Interval__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3085:1: ( rule__Interval__Group__1__Impl rule__Interval__Group__2 )
            // InternalSTL.g:3086:2: rule__Interval__Group__1__Impl rule__Interval__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Interval__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interval__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__1"


    // $ANTLR start "rule__Interval__Group__1__Impl"
    // InternalSTL.g:3093:1: rule__Interval__Group__1__Impl : ( ( rule__Interval__Time1Assignment_1 ) ) ;
    public final void rule__Interval__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3097:1: ( ( ( rule__Interval__Time1Assignment_1 ) ) )
            // InternalSTL.g:3098:1: ( ( rule__Interval__Time1Assignment_1 ) )
            {
            // InternalSTL.g:3098:1: ( ( rule__Interval__Time1Assignment_1 ) )
            // InternalSTL.g:3099:2: ( rule__Interval__Time1Assignment_1 )
            {
             before(grammarAccess.getIntervalAccess().getTime1Assignment_1()); 
            // InternalSTL.g:3100:2: ( rule__Interval__Time1Assignment_1 )
            // InternalSTL.g:3100:3: rule__Interval__Time1Assignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Interval__Time1Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIntervalAccess().getTime1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__1__Impl"


    // $ANTLR start "rule__Interval__Group__2"
    // InternalSTL.g:3108:1: rule__Interval__Group__2 : rule__Interval__Group__2__Impl rule__Interval__Group__3 ;
    public final void rule__Interval__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3112:1: ( rule__Interval__Group__2__Impl rule__Interval__Group__3 )
            // InternalSTL.g:3113:2: rule__Interval__Group__2__Impl rule__Interval__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__Interval__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interval__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__2"


    // $ANTLR start "rule__Interval__Group__2__Impl"
    // InternalSTL.g:3120:1: rule__Interval__Group__2__Impl : ( ( rule__Interval__Alternatives_2 ) ) ;
    public final void rule__Interval__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3124:1: ( ( ( rule__Interval__Alternatives_2 ) ) )
            // InternalSTL.g:3125:1: ( ( rule__Interval__Alternatives_2 ) )
            {
            // InternalSTL.g:3125:1: ( ( rule__Interval__Alternatives_2 ) )
            // InternalSTL.g:3126:2: ( rule__Interval__Alternatives_2 )
            {
             before(grammarAccess.getIntervalAccess().getAlternatives_2()); 
            // InternalSTL.g:3127:2: ( rule__Interval__Alternatives_2 )
            // InternalSTL.g:3127:3: rule__Interval__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__Interval__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getIntervalAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__2__Impl"


    // $ANTLR start "rule__Interval__Group__3"
    // InternalSTL.g:3135:1: rule__Interval__Group__3 : rule__Interval__Group__3__Impl rule__Interval__Group__4 ;
    public final void rule__Interval__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3139:1: ( rule__Interval__Group__3__Impl rule__Interval__Group__4 )
            // InternalSTL.g:3140:2: rule__Interval__Group__3__Impl rule__Interval__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__Interval__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interval__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__3"


    // $ANTLR start "rule__Interval__Group__3__Impl"
    // InternalSTL.g:3147:1: rule__Interval__Group__3__Impl : ( ( rule__Interval__Time2Assignment_3 ) ) ;
    public final void rule__Interval__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3151:1: ( ( ( rule__Interval__Time2Assignment_3 ) ) )
            // InternalSTL.g:3152:1: ( ( rule__Interval__Time2Assignment_3 ) )
            {
            // InternalSTL.g:3152:1: ( ( rule__Interval__Time2Assignment_3 ) )
            // InternalSTL.g:3153:2: ( rule__Interval__Time2Assignment_3 )
            {
             before(grammarAccess.getIntervalAccess().getTime2Assignment_3()); 
            // InternalSTL.g:3154:2: ( rule__Interval__Time2Assignment_3 )
            // InternalSTL.g:3154:3: rule__Interval__Time2Assignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Interval__Time2Assignment_3();

            state._fsp--;


            }

             after(grammarAccess.getIntervalAccess().getTime2Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__3__Impl"


    // $ANTLR start "rule__Interval__Group__4"
    // InternalSTL.g:3162:1: rule__Interval__Group__4 : rule__Interval__Group__4__Impl ;
    public final void rule__Interval__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3166:1: ( rule__Interval__Group__4__Impl )
            // InternalSTL.g:3167:2: rule__Interval__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interval__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__4"


    // $ANTLR start "rule__Interval__Group__4__Impl"
    // InternalSTL.g:3173:1: rule__Interval__Group__4__Impl : ( RULE_RBRACK ) ;
    public final void rule__Interval__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3177:1: ( ( RULE_RBRACK ) )
            // InternalSTL.g:3178:1: ( RULE_RBRACK )
            {
            // InternalSTL.g:3178:1: ( RULE_RBRACK )
            // InternalSTL.g:3179:2: RULE_RBRACK
            {
             before(grammarAccess.getIntervalAccess().getRBRACKTerminalRuleCall_4()); 
            match(input,RULE_RBRACK,FOLLOW_2); 
             after(grammarAccess.getIntervalAccess().getRBRACKTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Group__4__Impl"


    // $ANTLR start "rule__IntervalTime__Group_0__0"
    // InternalSTL.g:3189:1: rule__IntervalTime__Group_0__0 : rule__IntervalTime__Group_0__0__Impl rule__IntervalTime__Group_0__1 ;
    public final void rule__IntervalTime__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3193:1: ( rule__IntervalTime__Group_0__0__Impl rule__IntervalTime__Group_0__1 )
            // InternalSTL.g:3194:2: rule__IntervalTime__Group_0__0__Impl rule__IntervalTime__Group_0__1
            {
            pushFollow(FOLLOW_17);
            rule__IntervalTime__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntervalTime__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_0__0"


    // $ANTLR start "rule__IntervalTime__Group_0__0__Impl"
    // InternalSTL.g:3201:1: rule__IntervalTime__Group_0__0__Impl : ( rulenum_literal ) ;
    public final void rule__IntervalTime__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3205:1: ( ( rulenum_literal ) )
            // InternalSTL.g:3206:1: ( rulenum_literal )
            {
            // InternalSTL.g:3206:1: ( rulenum_literal )
            // InternalSTL.g:3207:2: rulenum_literal
            {
             before(grammarAccess.getIntervalTimeAccess().getNum_literalParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            rulenum_literal();

            state._fsp--;

             after(grammarAccess.getIntervalTimeAccess().getNum_literalParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_0__0__Impl"


    // $ANTLR start "rule__IntervalTime__Group_0__1"
    // InternalSTL.g:3216:1: rule__IntervalTime__Group_0__1 : rule__IntervalTime__Group_0__1__Impl ;
    public final void rule__IntervalTime__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3220:1: ( rule__IntervalTime__Group_0__1__Impl )
            // InternalSTL.g:3221:2: rule__IntervalTime__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntervalTime__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_0__1"


    // $ANTLR start "rule__IntervalTime__Group_0__1__Impl"
    // InternalSTL.g:3227:1: rule__IntervalTime__Group_0__1__Impl : ( ( RULE_UNIT )? ) ;
    public final void rule__IntervalTime__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3231:1: ( ( ( RULE_UNIT )? ) )
            // InternalSTL.g:3232:1: ( ( RULE_UNIT )? )
            {
            // InternalSTL.g:3232:1: ( ( RULE_UNIT )? )
            // InternalSTL.g:3233:2: ( RULE_UNIT )?
            {
             before(grammarAccess.getIntervalTimeAccess().getUNITTerminalRuleCall_0_1()); 
            // InternalSTL.g:3234:2: ( RULE_UNIT )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_UNIT) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalSTL.g:3234:3: RULE_UNIT
                    {
                    match(input,RULE_UNIT,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getIntervalTimeAccess().getUNITTerminalRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_0__1__Impl"


    // $ANTLR start "rule__IntervalTime__Group_1__0"
    // InternalSTL.g:3243:1: rule__IntervalTime__Group_1__0 : rule__IntervalTime__Group_1__0__Impl rule__IntervalTime__Group_1__1 ;
    public final void rule__IntervalTime__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3247:1: ( rule__IntervalTime__Group_1__0__Impl rule__IntervalTime__Group_1__1 )
            // InternalSTL.g:3248:2: rule__IntervalTime__Group_1__0__Impl rule__IntervalTime__Group_1__1
            {
            pushFollow(FOLLOW_17);
            rule__IntervalTime__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntervalTime__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_1__0"


    // $ANTLR start "rule__IntervalTime__Group_1__0__Impl"
    // InternalSTL.g:3255:1: rule__IntervalTime__Group_1__0__Impl : ( RULE_IDENTIFIER ) ;
    public final void rule__IntervalTime__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3259:1: ( ( RULE_IDENTIFIER ) )
            // InternalSTL.g:3260:1: ( RULE_IDENTIFIER )
            {
            // InternalSTL.g:3260:1: ( RULE_IDENTIFIER )
            // InternalSTL.g:3261:2: RULE_IDENTIFIER
            {
             before(grammarAccess.getIntervalTimeAccess().getIdentifierTerminalRuleCall_1_0()); 
            match(input,RULE_IDENTIFIER,FOLLOW_2); 
             after(grammarAccess.getIntervalTimeAccess().getIdentifierTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_1__0__Impl"


    // $ANTLR start "rule__IntervalTime__Group_1__1"
    // InternalSTL.g:3270:1: rule__IntervalTime__Group_1__1 : rule__IntervalTime__Group_1__1__Impl ;
    public final void rule__IntervalTime__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3274:1: ( rule__IntervalTime__Group_1__1__Impl )
            // InternalSTL.g:3275:2: rule__IntervalTime__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntervalTime__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_1__1"


    // $ANTLR start "rule__IntervalTime__Group_1__1__Impl"
    // InternalSTL.g:3281:1: rule__IntervalTime__Group_1__1__Impl : ( ( RULE_UNIT )? ) ;
    public final void rule__IntervalTime__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3285:1: ( ( ( RULE_UNIT )? ) )
            // InternalSTL.g:3286:1: ( ( RULE_UNIT )? )
            {
            // InternalSTL.g:3286:1: ( ( RULE_UNIT )? )
            // InternalSTL.g:3287:2: ( RULE_UNIT )?
            {
             before(grammarAccess.getIntervalTimeAccess().getUNITTerminalRuleCall_1_1()); 
            // InternalSTL.g:3288:2: ( RULE_UNIT )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_UNIT) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalSTL.g:3288:3: RULE_UNIT
                    {
                    match(input,RULE_UNIT,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getIntervalTimeAccess().getUNITTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntervalTime__Group_1__1__Impl"


    // $ANTLR start "rule__Assignments__AssignmentsAssignment"
    // InternalSTL.g:3297:1: rule__Assignments__AssignmentsAssignment : ( ruleassignment ) ;
    public final void rule__Assignments__AssignmentsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3301:1: ( ( ruleassignment ) )
            // InternalSTL.g:3302:2: ( ruleassignment )
            {
            // InternalSTL.g:3302:2: ( ruleassignment )
            // InternalSTL.g:3303:3: ruleassignment
            {
             before(grammarAccess.getAssignmentsAccess().getAssignmentsAssignmentParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleassignment();

            state._fsp--;

             after(grammarAccess.getAssignmentsAccess().getAssignmentsAssignmentParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignments__AssignmentsAssignment"


    // $ANTLR start "rule__Assignment__IdAssignment_0"
    // InternalSTL.g:3312:1: rule__Assignment__IdAssignment_0 : ( RULE_IDENTIFIER ) ;
    public final void rule__Assignment__IdAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3316:1: ( ( RULE_IDENTIFIER ) )
            // InternalSTL.g:3317:2: ( RULE_IDENTIFIER )
            {
            // InternalSTL.g:3317:2: ( RULE_IDENTIFIER )
            // InternalSTL.g:3318:3: RULE_IDENTIFIER
            {
             before(grammarAccess.getAssignmentAccess().getIdIdentifierTerminalRuleCall_0_0()); 
            match(input,RULE_IDENTIFIER,FOLLOW_2); 
             after(grammarAccess.getAssignmentAccess().getIdIdentifierTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__IdAssignment_0"


    // $ANTLR start "rule__Assignment__ExprAssignment_2"
    // InternalSTL.g:3327:1: rule__Assignment__ExprAssignment_2 : ( ruleexpression ) ;
    public final void rule__Assignment__ExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3331:1: ( ( ruleexpression ) )
            // InternalSTL.g:3332:2: ( ruleexpression )
            {
            // InternalSTL.g:3332:2: ( ruleexpression )
            // InternalSTL.g:3333:3: ruleexpression
            {
             before(grammarAccess.getAssignmentAccess().getExprExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getAssignmentAccess().getExprExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Assignment__ExprAssignment_2"


    // $ANTLR start "rule__Expression__FirstAssignment_0"
    // InternalSTL.g:3342:1: rule__Expression__FirstAssignment_0 : ( ruleexpression_start ) ;
    public final void rule__Expression__FirstAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3346:1: ( ( ruleexpression_start ) )
            // InternalSTL.g:3347:2: ( ruleexpression_start )
            {
            // InternalSTL.g:3347:2: ( ruleexpression_start )
            // InternalSTL.g:3348:3: ruleexpression_start
            {
             before(grammarAccess.getExpressionAccess().getFirstExpression_startParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression_start();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getFirstExpression_startParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__FirstAssignment_0"


    // $ANTLR start "rule__Expression__EcAssignment_1"
    // InternalSTL.g:3357:1: rule__Expression__EcAssignment_1 : ( ruleexpression_cont ) ;
    public final void rule__Expression__EcAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3361:1: ( ( ruleexpression_cont ) )
            // InternalSTL.g:3362:2: ( ruleexpression_cont )
            {
            // InternalSTL.g:3362:2: ( ruleexpression_cont )
            // InternalSTL.g:3363:3: ruleexpression_cont
            {
             before(grammarAccess.getExpressionAccess().getEcExpression_contParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression_cont();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getEcExpression_contParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__EcAssignment_1"


    // $ANTLR start "rule__Expression_start__RAssignment_0"
    // InternalSTL.g:3372:1: rule__Expression_start__RAssignment_0 : ( rulereal_expression ) ;
    public final void rule__Expression_start__RAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3376:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3377:2: ( rulereal_expression )
            {
            // InternalSTL.g:3377:2: ( rulereal_expression )
            // InternalSTL.g:3378:3: rulereal_expression
            {
             before(grammarAccess.getExpression_startAccess().getRReal_expressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getExpression_startAccess().getRReal_expressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__RAssignment_0"


    // $ANTLR start "rule__Expression_start__E1Assignment_1_1"
    // InternalSTL.g:3387:1: rule__Expression_start__E1Assignment_1_1 : ( rulereal_expression ) ;
    public final void rule__Expression_start__E1Assignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3391:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3392:2: ( rulereal_expression )
            {
            // InternalSTL.g:3392:2: ( rulereal_expression )
            // InternalSTL.g:3393:3: rulereal_expression
            {
             before(grammarAccess.getExpression_startAccess().getE1Real_expressionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getExpression_startAccess().getE1Real_expressionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__E1Assignment_1_1"


    // $ANTLR start "rule__Expression_start__E2Assignment_1_3"
    // InternalSTL.g:3402:1: rule__Expression_start__E2Assignment_1_3 : ( rulereal_expression ) ;
    public final void rule__Expression_start__E2Assignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3406:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3407:2: ( rulereal_expression )
            {
            // InternalSTL.g:3407:2: ( rulereal_expression )
            // InternalSTL.g:3408:3: rulereal_expression
            {
             before(grammarAccess.getExpression_startAccess().getE2Real_expressionParserRuleCall_1_3_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getExpression_startAccess().getE2Real_expressionParserRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_start__E2Assignment_1_3"


    // $ANTLR start "rule__Expression_cont__IAssignment_0_1"
    // InternalSTL.g:3417:1: rule__Expression_cont__IAssignment_0_1 : ( ruleinterval ) ;
    public final void rule__Expression_cont__IAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3421:1: ( ( ruleinterval ) )
            // InternalSTL.g:3422:2: ( ruleinterval )
            {
            // InternalSTL.g:3422:2: ( ruleinterval )
            // InternalSTL.g:3423:3: ruleinterval
            {
             before(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleinterval();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__IAssignment_0_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_0_2"
    // InternalSTL.g:3432:1: rule__Expression_cont__EAssignment_0_2 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3436:1: ( ( ruleexpression ) )
            // InternalSTL.g:3437:2: ( ruleexpression )
            {
            // InternalSTL.g:3437:2: ( ruleexpression )
            // InternalSTL.g:3438:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_0_2"


    // $ANTLR start "rule__Expression_cont__IAssignment_1_1"
    // InternalSTL.g:3447:1: rule__Expression_cont__IAssignment_1_1 : ( ruleinterval ) ;
    public final void rule__Expression_cont__IAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3451:1: ( ( ruleinterval ) )
            // InternalSTL.g:3452:2: ( ruleinterval )
            {
            // InternalSTL.g:3452:2: ( ruleinterval )
            // InternalSTL.g:3453:3: ruleinterval
            {
             before(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleinterval();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__IAssignment_1_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_1_2"
    // InternalSTL.g:3462:1: rule__Expression_cont__EAssignment_1_2 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3466:1: ( ( ruleexpression ) )
            // InternalSTL.g:3467:2: ( ruleexpression )
            {
            // InternalSTL.g:3467:2: ( ruleexpression )
            // InternalSTL.g:3468:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_1_2"


    // $ANTLR start "rule__Expression_cont__IAssignment_2_1"
    // InternalSTL.g:3477:1: rule__Expression_cont__IAssignment_2_1 : ( ruleinterval ) ;
    public final void rule__Expression_cont__IAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3481:1: ( ( ruleinterval ) )
            // InternalSTL.g:3482:2: ( ruleinterval )
            {
            // InternalSTL.g:3482:2: ( ruleinterval )
            // InternalSTL.g:3483:3: ruleinterval
            {
             before(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleinterval();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__IAssignment_2_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_2_2"
    // InternalSTL.g:3492:1: rule__Expression_cont__EAssignment_2_2 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3496:1: ( ( ruleexpression ) )
            // InternalSTL.g:3497:2: ( ruleexpression )
            {
            // InternalSTL.g:3497:2: ( ruleexpression )
            // InternalSTL.g:3498:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_2_2"


    // $ANTLR start "rule__Expression_cont__IAssignment_3_1"
    // InternalSTL.g:3507:1: rule__Expression_cont__IAssignment_3_1 : ( ruleinterval ) ;
    public final void rule__Expression_cont__IAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3511:1: ( ( ruleinterval ) )
            // InternalSTL.g:3512:2: ( ruleinterval )
            {
            // InternalSTL.g:3512:2: ( ruleinterval )
            // InternalSTL.g:3513:3: ruleinterval
            {
             before(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleinterval();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getIIntervalParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__IAssignment_3_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_3_2"
    // InternalSTL.g:3522:1: rule__Expression_cont__EAssignment_3_2 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3526:1: ( ( ruleexpression ) )
            // InternalSTL.g:3527:2: ( ruleexpression )
            {
            // InternalSTL.g:3527:2: ( ruleexpression )
            // InternalSTL.g:3528:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_3_2"


    // $ANTLR start "rule__Expression_cont__EAssignment_4_2"
    // InternalSTL.g:3537:1: rule__Expression_cont__EAssignment_4_2 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3541:1: ( ( ruleexpression ) )
            // InternalSTL.g:3542:2: ( ruleexpression )
            {
            // InternalSTL.g:3542:2: ( ruleexpression )
            // InternalSTL.g:3543:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_4_2"


    // $ANTLR start "rule__Expression_cont__EAssignment_5_2"
    // InternalSTL.g:3552:1: rule__Expression_cont__EAssignment_5_2 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3556:1: ( ( ruleexpression ) )
            // InternalSTL.g:3557:2: ( ruleexpression )
            {
            // InternalSTL.g:3557:2: ( ruleexpression )
            // InternalSTL.g:3558:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_5_2"


    // $ANTLR start "rule__Expression_cont__EAssignment_6_1"
    // InternalSTL.g:3567:1: rule__Expression_cont__EAssignment_6_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3571:1: ( ( ruleexpression ) )
            // InternalSTL.g:3572:2: ( ruleexpression )
            {
            // InternalSTL.g:3572:2: ( ruleexpression )
            // InternalSTL.g:3573:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_6_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_7_1"
    // InternalSTL.g:3582:1: rule__Expression_cont__EAssignment_7_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3586:1: ( ( ruleexpression ) )
            // InternalSTL.g:3587:2: ( ruleexpression )
            {
            // InternalSTL.g:3587:2: ( ruleexpression )
            // InternalSTL.g:3588:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_7_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_8_1"
    // InternalSTL.g:3597:1: rule__Expression_cont__EAssignment_8_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3601:1: ( ( ruleexpression ) )
            // InternalSTL.g:3602:2: ( ruleexpression )
            {
            // InternalSTL.g:3602:2: ( ruleexpression )
            // InternalSTL.g:3603:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_8_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_8_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_9_1"
    // InternalSTL.g:3612:1: rule__Expression_cont__EAssignment_9_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3616:1: ( ( ruleexpression ) )
            // InternalSTL.g:3617:2: ( ruleexpression )
            {
            // InternalSTL.g:3617:2: ( ruleexpression )
            // InternalSTL.g:3618:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_9_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_9_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_10_1_0"
    // InternalSTL.g:3627:1: rule__Expression_cont__EAssignment_10_1_0 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_10_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3631:1: ( ( ruleexpression ) )
            // InternalSTL.g:3632:2: ( ruleexpression )
            {
            // InternalSTL.g:3632:2: ( ruleexpression )
            // InternalSTL.g:3633:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_10_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_10_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_10_1_0"


    // $ANTLR start "rule__Expression_cont__EAssignment_10_1_1"
    // InternalSTL.g:3642:1: rule__Expression_cont__EAssignment_10_1_1 : ( ruleexpression_cont ) ;
    public final void rule__Expression_cont__EAssignment_10_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3646:1: ( ( ruleexpression_cont ) )
            // InternalSTL.g:3647:2: ( ruleexpression_cont )
            {
            // InternalSTL.g:3647:2: ( ruleexpression_cont )
            // InternalSTL.g:3648:3: ruleexpression_cont
            {
             before(grammarAccess.getExpression_contAccess().getEExpression_contParserRuleCall_10_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression_cont();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpression_contParserRuleCall_10_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_10_1_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_11_1"
    // InternalSTL.g:3657:1: rule__Expression_cont__EAssignment_11_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_11_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3661:1: ( ( ruleexpression ) )
            // InternalSTL.g:3662:2: ( ruleexpression )
            {
            // InternalSTL.g:3662:2: ( ruleexpression )
            // InternalSTL.g:3663:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_11_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_11_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_11_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_12_1"
    // InternalSTL.g:3672:1: rule__Expression_cont__EAssignment_12_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_12_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3676:1: ( ( ruleexpression ) )
            // InternalSTL.g:3677:2: ( ruleexpression )
            {
            // InternalSTL.g:3677:2: ( ruleexpression )
            // InternalSTL.g:3678:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_12_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_12_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_12_1"


    // $ANTLR start "rule__Expression_cont__EAssignment_13_1"
    // InternalSTL.g:3687:1: rule__Expression_cont__EAssignment_13_1 : ( ruleexpression ) ;
    public final void rule__Expression_cont__EAssignment_13_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3691:1: ( ( ruleexpression ) )
            // InternalSTL.g:3692:2: ( ruleexpression )
            {
            // InternalSTL.g:3692:2: ( ruleexpression )
            // InternalSTL.g:3693:3: ruleexpression
            {
             before(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_13_1_0()); 
            pushFollow(FOLLOW_2);
            ruleexpression();

            state._fsp--;

             after(grammarAccess.getExpression_contAccess().getEExpressionParserRuleCall_13_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression_cont__EAssignment_13_1"


    // $ANTLR start "rule__Real_expression__IdAssignment_0"
    // InternalSTL.g:3702:1: rule__Real_expression__IdAssignment_0 : ( RULE_IDENTIFIER ) ;
    public final void rule__Real_expression__IdAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3706:1: ( ( RULE_IDENTIFIER ) )
            // InternalSTL.g:3707:2: ( RULE_IDENTIFIER )
            {
            // InternalSTL.g:3707:2: ( RULE_IDENTIFIER )
            // InternalSTL.g:3708:3: RULE_IDENTIFIER
            {
             before(grammarAccess.getReal_expressionAccess().getIdIdentifierTerminalRuleCall_0_0()); 
            match(input,RULE_IDENTIFIER,FOLLOW_2); 
             after(grammarAccess.getReal_expressionAccess().getIdIdentifierTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__IdAssignment_0"


    // $ANTLR start "rule__Real_expression__LitAssignment_1"
    // InternalSTL.g:3717:1: rule__Real_expression__LitAssignment_1 : ( rulenum_literal ) ;
    public final void rule__Real_expression__LitAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3721:1: ( ( rulenum_literal ) )
            // InternalSTL.g:3722:2: ( rulenum_literal )
            {
            // InternalSTL.g:3722:2: ( rulenum_literal )
            // InternalSTL.g:3723:3: rulenum_literal
            {
             before(grammarAccess.getReal_expressionAccess().getLitNum_literalParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulenum_literal();

            state._fsp--;

             after(grammarAccess.getReal_expressionAccess().getLitNum_literalParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__LitAssignment_1"


    // $ANTLR start "rule__Real_expression__ExpAssignment_2_2"
    // InternalSTL.g:3732:1: rule__Real_expression__ExpAssignment_2_2 : ( rulereal_expression ) ;
    public final void rule__Real_expression__ExpAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3736:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3737:2: ( rulereal_expression )
            {
            // InternalSTL.g:3737:2: ( rulereal_expression )
            // InternalSTL.g:3738:3: rulereal_expression
            {
             before(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__ExpAssignment_2_2"


    // $ANTLR start "rule__Real_expression__ExpAssignment_3_2"
    // InternalSTL.g:3747:1: rule__Real_expression__ExpAssignment_3_2 : ( rulereal_expression ) ;
    public final void rule__Real_expression__ExpAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3751:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3752:2: ( rulereal_expression )
            {
            // InternalSTL.g:3752:2: ( rulereal_expression )
            // InternalSTL.g:3753:3: rulereal_expression
            {
             before(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__ExpAssignment_3_2"


    // $ANTLR start "rule__Real_expression__ExpAssignment_4_2"
    // InternalSTL.g:3762:1: rule__Real_expression__ExpAssignment_4_2 : ( rulereal_expression ) ;
    public final void rule__Real_expression__ExpAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3766:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3767:2: ( rulereal_expression )
            {
            // InternalSTL.g:3767:2: ( rulereal_expression )
            // InternalSTL.g:3768:3: rulereal_expression
            {
             before(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getReal_expressionAccess().getExpReal_expressionParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__ExpAssignment_4_2"


    // $ANTLR start "rule__Real_expression__Exp1Assignment_5_2"
    // InternalSTL.g:3777:1: rule__Real_expression__Exp1Assignment_5_2 : ( rulereal_expression ) ;
    public final void rule__Real_expression__Exp1Assignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3781:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3782:2: ( rulereal_expression )
            {
            // InternalSTL.g:3782:2: ( rulereal_expression )
            // InternalSTL.g:3783:3: rulereal_expression
            {
             before(grammarAccess.getReal_expressionAccess().getExp1Real_expressionParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getReal_expressionAccess().getExp1Real_expressionParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Exp1Assignment_5_2"


    // $ANTLR start "rule__Real_expression__Exp2Assignment_5_4"
    // InternalSTL.g:3792:1: rule__Real_expression__Exp2Assignment_5_4 : ( rulereal_expression ) ;
    public final void rule__Real_expression__Exp2Assignment_5_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3796:1: ( ( rulereal_expression ) )
            // InternalSTL.g:3797:2: ( rulereal_expression )
            {
            // InternalSTL.g:3797:2: ( rulereal_expression )
            // InternalSTL.g:3798:3: rulereal_expression
            {
             before(grammarAccess.getReal_expressionAccess().getExp2Real_expressionParserRuleCall_5_4_0()); 
            pushFollow(FOLLOW_2);
            rulereal_expression();

            state._fsp--;

             after(grammarAccess.getReal_expressionAccess().getExp2Real_expressionParserRuleCall_5_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Real_expression__Exp2Assignment_5_4"


    // $ANTLR start "rule__Num_literal__IlitAssignment_0"
    // InternalSTL.g:3807:1: rule__Num_literal__IlitAssignment_0 : ( RULE_INTEGERLITERAL ) ;
    public final void rule__Num_literal__IlitAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3811:1: ( ( RULE_INTEGERLITERAL ) )
            // InternalSTL.g:3812:2: ( RULE_INTEGERLITERAL )
            {
            // InternalSTL.g:3812:2: ( RULE_INTEGERLITERAL )
            // InternalSTL.g:3813:3: RULE_INTEGERLITERAL
            {
             before(grammarAccess.getNum_literalAccess().getIlitIntegerLiteralTerminalRuleCall_0_0()); 
            match(input,RULE_INTEGERLITERAL,FOLLOW_2); 
             after(grammarAccess.getNum_literalAccess().getIlitIntegerLiteralTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__IlitAssignment_0"


    // $ANTLR start "rule__Num_literal__RlitAssignment_1"
    // InternalSTL.g:3822:1: rule__Num_literal__RlitAssignment_1 : ( ruleRealLiteral ) ;
    public final void rule__Num_literal__RlitAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3826:1: ( ( ruleRealLiteral ) )
            // InternalSTL.g:3827:2: ( ruleRealLiteral )
            {
            // InternalSTL.g:3827:2: ( ruleRealLiteral )
            // InternalSTL.g:3828:3: ruleRealLiteral
            {
             before(grammarAccess.getNum_literalAccess().getRlitRealLiteralParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRealLiteral();

            state._fsp--;

             after(grammarAccess.getNum_literalAccess().getRlitRealLiteralParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__RlitAssignment_1"


    // $ANTLR start "rule__Num_literal__LitAssignment_2_1"
    // InternalSTL.g:3837:1: rule__Num_literal__LitAssignment_2_1 : ( rulenum_literal ) ;
    public final void rule__Num_literal__LitAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3841:1: ( ( rulenum_literal ) )
            // InternalSTL.g:3842:2: ( rulenum_literal )
            {
            // InternalSTL.g:3842:2: ( rulenum_literal )
            // InternalSTL.g:3843:3: rulenum_literal
            {
             before(grammarAccess.getNum_literalAccess().getLitNum_literalParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            rulenum_literal();

            state._fsp--;

             after(grammarAccess.getNum_literalAccess().getLitNum_literalParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Num_literal__LitAssignment_2_1"


    // $ANTLR start "rule__Interval__Time1Assignment_1"
    // InternalSTL.g:3852:1: rule__Interval__Time1Assignment_1 : ( ruleintervalTime ) ;
    public final void rule__Interval__Time1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3856:1: ( ( ruleintervalTime ) )
            // InternalSTL.g:3857:2: ( ruleintervalTime )
            {
            // InternalSTL.g:3857:2: ( ruleintervalTime )
            // InternalSTL.g:3858:3: ruleintervalTime
            {
             before(grammarAccess.getIntervalAccess().getTime1IntervalTimeParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleintervalTime();

            state._fsp--;

             after(grammarAccess.getIntervalAccess().getTime1IntervalTimeParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Time1Assignment_1"


    // $ANTLR start "rule__Interval__Time2Assignment_3"
    // InternalSTL.g:3867:1: rule__Interval__Time2Assignment_3 : ( ruleintervalTime ) ;
    public final void rule__Interval__Time2Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSTL.g:3871:1: ( ( ruleintervalTime ) )
            // InternalSTL.g:3872:2: ( ruleintervalTime )
            {
            // InternalSTL.g:3872:2: ( ruleintervalTime )
            // InternalSTL.g:3873:3: ruleintervalTime
            {
             before(grammarAccess.getIntervalAccess().getTime2IntervalTimeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleintervalTime();

            state._fsp--;

             after(grammarAccess.getIntervalAccess().getTime2IntervalTimeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interval__Time2Assignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000000L,0x0000018000000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000063E90L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000000L,0x000007FFFFFFF000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000063E10L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000067E90L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000063E90L,0x000007FFFFFFF000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000042010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000062010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000010000L});

}