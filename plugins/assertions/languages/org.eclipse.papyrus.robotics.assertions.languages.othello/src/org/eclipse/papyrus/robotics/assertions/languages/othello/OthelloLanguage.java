/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 * 	 Angel López, Tecnalia
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.othello;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.robotics.assertions.languages.AssertionsHelper;
import org.eclipse.papyrus.robotics.assertions.languages.IExpressionLanguage;
import org.eclipse.papyrus.robotics.assertions.languages.othello.activator.Activator;
import org.eclipse.papyrus.robotics.assertions.languages.othello.preferences.OCRAPreferencePage;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.robotics.assertions.ui.actions.ScrollableColorMessageDialog;
import org.eclipse.papyrus.robotics.bpc.profile.bpc.Port;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentPort;
import org.eclipse.papyrus.robotics.profile.robotics.components.System;
import org.eclipse.papyrus.robotics.profile.robotics.services.ServiceDefinition;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class OthelloLanguage implements IExpressionLanguage {

	public static final String OSS_EXT = ".oss"; //$NON-NLS-1$
	public static final String P4R_TEMP = "p4r_temp_"; //$NON-NLS-1$
	public static final String NAME = "Othello"; //$NON-NLS-1$

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Object evaluate(EObject context, String expression) {

		if (expression != null) {
			expression = updateExpression(context, expression);
		}

		String oss = getOSSForEvaluation(context, expression);
		String ocra_command = null;

		if ((context instanceof org.eclipse.papyrus.robotics.profile.robotics.components.System
				|| (context instanceof Element && UMLUtil.getStereotypeApplication((Element) context,
						org.eclipse.papyrus.robotics.profile.robotics.components.System.class) != null))
				&& oss.contains(" REFINEDBY ")) { //$NON-NLS-1$
			ocra_command = OCRAIntegration.COMMAND_CHECK_REFINEMENT;
		} else {
			ocra_command = OCRAIntegration.COMMAND_CHECK_CONSISTENCY;
		}

		String ocraPath = Platform.getPreferencesService().getString(Activator.PLUGIN_ID, OCRAPreferencePage.OCRA_PATH,
				StringConstants.EMPTY, null);

		// Control checks
		if (ocraPath == null || ocraPath.isEmpty()) {
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Othello language",
					"OCRA path is not defined in the Eclipse preferences.");
			return null;
		}
		File file = new File(ocraPath);
		if (!file.exists()) {
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Othello language",
					"OCRA path in the Eclipse preferences points to a file that does not exist.");
			return null;
		}

		String ocraConsole;
		try {
			File tempFile = File.createTempFile(P4R_TEMP, OSS_EXT);
			OCRAIntegration.writeFile(tempFile, oss.toString());
			ocraConsole = OCRAIntegration.getConsoleResult(ocraPath, tempFile.getAbsolutePath(), ocra_command);
			tempFile.delete();
		} catch (Exception e) {
			return null;
		}

		ScrollableColorMessageDialog dialog = new ScrollableColorMessageDialog(Display.getCurrent().getActiveShell(),
				"Using OCRA to evaluate " + getName() + " assertions", "OSS file. Command: " + ocra_command, oss.toString());
		dialog.open();

		dialog = new ScrollableColorMessageDialog(Display.getCurrent().getActiveShell(),
				"Using OCRA to evaluate " + getName() + " assertions", "OCRA Console. Command: " + ocra_command, ocraConsole);
		dialog.open();

		return ocraConsole.contains("Success: all the contracts are consistent") || ocraConsole.contains("Summary: everything is OK.");
	}

	/**
	 * OSS from system
	 * 
	 * @param system
	 * @return oss
	 */
	public StringBuffer ossFromSystem(System system, boolean addContracts) {
		StringBuffer oss = new StringBuffer();
		oss.append("COMPONENT " + system.getBase_Class().getName() + " system\n");
		oss.append("INTERFACE\n");

		int indexSystemInterface = oss.length();

		// add all refinements
		oss.append("REFINEMENT\n");
		for (ComponentInstance compInstance : system.getInstances()) {
			ComponentOrSystem compDef = compInstance.getCompdefOrSys();
			oss.append("SUB ");
			oss.append(compInstance.getBase_Property().getName());
			oss.append(":"); //$NON-NLS-1$
			oss.append(compDef.getBase_Class().getName());
			oss.append(";\n"); //$NON-NLS-1$
		}

		// add connections
		for (Connector connector : system.getBase_Class().getOwnedConnectors()) {
			oss.append("CONNECTION ");
			String input = null;
			String output = null;
			for (ConnectorEnd connectorEnd : connector.getEnds()) {
				ConnectableElement ce = connectorEnd.getRole();
				Property prop = connectorEnd.getPartWithPort();
				String portName = prop.getName() + StringConstants.DOT + ce.getName();
				ComponentInstance compInstance = UMLUtil.getStereotypeApplication(prop, ComponentInstance.class);
				ComponentOrSystem compDef = compInstance.getCompdefOrSys();
				for (Port port : compDef.getPort()) {
					if (((ComponentPort) port).getBase_Port().equals(ce)) {
						if (isInputPort(port)) {
							input = portName;
						} else {
							output = portName;
						}
						break;
					}
				}
			}
			oss.append(input);
			oss.append(" := ");
			oss.append(output);
			oss.append(";\n"); //$NON-NLS-1$

		}

		// we save this position to enter the refinedBy contracts
		int indexEndOfSystem = oss.length();
		StringBuffer refinedby = new StringBuffer();

		// add components
		List<String> alreadyAdded = new ArrayList<String>();
		for (ComponentInstance compInstance : system.getInstances()) {
			ComponentOrSystem compDef = compInstance.getCompdefOrSys();
			String compDefName = compDef.getBase_Class().getName();
			if (!alreadyAdded.contains(compDefName)) {
				oss.append("\n");
				alreadyAdded.add(compDefName);
				oss.append(ossFromCompDefinition(compDef, false));
				if (addContracts) {
					StringBuffer contract = ossFromCompInstanceContract(compInstance);
					if (contract.length() != 0) {
						oss.append(contract);
						refinedby.append(compInstance.getBase_Property().getName() + ".pass, ");
					}
				}
			}
		}

		if (addContracts && refinedby.length() != 0) {
			// remove last comma
			refinedby.setLength(refinedby.length() - ", ".length());
			oss.insert(indexEndOfSystem, "CONTRACT pass REFINEDBY " + refinedby + ";\n");
			oss.insert(indexSystemInterface, "CONTRACT pass\nassume: true;\nguarantee: true;\n");
		}

		return oss;
	}

	/**
	 * OSS for the contract of a comp instance
	 * 
	 * @param compInstance
	 * @return string
	 */
	public StringBuffer ossFromCompInstanceContract(ComponentInstance compInstance) {
		StringBuffer oss = new StringBuffer();
		List<Contract> contracts = AssertionsHelper.getCompDefinitionContracts(compInstance.getCompdefOrSys());
		for (Contract contract : contracts) {
			oss.append(ossFromContract(compInstance, contract));
		}
		return oss;
	}

	/**
	 * OSS for a contract
	 * 
	 * @param contract
	 * @return string
	 */
	public StringBuffer ossFromContract(EObject context, Contract contract) {
		StringBuffer oss = new StringBuffer();
		oss.append("CONTRACT pass\n");
		List<Assertion> assumptions = contract.getAssumptions();
		assumptions = getOthelloAssertions(assumptions);
		if (assumptions != null && !assumptions.isEmpty()) {
			// TODO only one assumption for the moment
			String assumptionValue = assumptions.get(0).getBase_Constraint().getSpecification().stringValue();
			oss.append("assume: " + updateExpression(context, assumptionValue) + ";\n");
		} else {
			oss.append("assume: true;\n");
		}
		List<Assertion> guarantees = contract.getGuarantees();
		guarantees = getOthelloAssertions(guarantees);
		if (guarantees != null && !guarantees.isEmpty()) {
			// TODO only one guarantee for the moment
			String guaranteeValue = guarantees.get(0).getBase_Constraint().getSpecification().stringValue();
			oss.append("guarantee: " + updateExpression(context, guaranteeValue) + ";\n");
		} else {
			oss.append("guarantee: true;\n");
		}
		return oss;
	}

	/**
	 * Update expression. To be overriden
	 * 
	 * @param context
	 * @param expression
	 * @return updated expression
	 */
	public String updateExpression(EObject context, String expression) {
		// No update for pure Othello
		return expression;
	}

	/**
	 * OSS from comp definition
	 * 
	 * @param compDef
	 * @return oss
	 */
	public StringBuffer ossFromCompDefinition(ComponentOrSystem compDef, boolean considerAsSystem) {
		StringBuffer oss = new StringBuffer();
		oss.append("COMPONENT " + compDef.getBase_Class().getName());
		if (considerAsSystem) {
			oss.append(" system\n");
		} else {
			oss.append("\n");
		}
		oss.append("INTERFACE\n");
		for (Port port : compDef.getPort()) {

			// input or output
			ComponentPort compPort = (ComponentPort) port;
			boolean input = isInputPort(port);
			if (input) {
				oss.append("INPUT PORT ");
			} else {
				// also for coordination ports
				oss.append("OUTPUT PORT ");
			}

			// name and type
			String name = compPort.getBase_Port().getName();
			String type = null;
			if (compPort.isCoordinationPort()) {
				type = "event";
			} else {
				ServiceDefinition serviceDefinition = compPort.getProvides();
				if (serviceDefinition == null) {
					serviceDefinition = compPort.getRequires();
				}
				String nameService = serviceDefinition.getBase_Interface().getName();
				if (nameService.toLowerCase().contains("bool")) {
					type = "boolean";
				} else if (nameService.toLowerCase().contains("real")) {
					type = "real";
				} else if (nameService.toLowerCase().contains("int")) {
					type = "integer";
				} else {
					type = "event";
				}
			}
			oss.append(name + ": " + type + ";\n");
		}
		return oss;
	}

	/**
	 * Filter a list of assertions to get only othello assertions
	 * 
	 * @param assertions
	 * @return othello assertions
	 */
	public List<Assertion> getOthelloAssertions(List<Assertion> assertions) {
		List<Assertion> othello = new ArrayList<Assertion>();
		for (Assertion assertion : assertions) {
			String name = AssertionsHelper.getAssertionLanguage(assertion);
			if (name != null && name.equals(getName())) {
				othello.add(assertion);
			}
		}
		return othello;
	}

	@Override
	public boolean isGlobalEvaluation() {
		return true;
	}

	public String getOSSForEvaluation(EObject context, String expression) {

		// Get context

		ComponentDefinition compDefinition = null;
		ComponentInstance compInstance = null;
		org.eclipse.papyrus.robotics.profile.robotics.components.System system = null;

		// isCompDefinition
		if (context instanceof ComponentDefinition) {
			compDefinition = (ComponentDefinition) context;
		} else if (context instanceof Element
				&& UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class) != null) {
			compDefinition = UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class);
		}

		// isCompInstance
		if (compDefinition == null) {
			if (context instanceof ComponentInstance) {
				compInstance = (ComponentInstance) context;
			} else if (context instanceof Element
					&& UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class) != null) {
				compInstance = UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class);
			}
		}

		// isSystem
		if (compDefinition == null && compInstance == null) {
			if (context instanceof org.eclipse.papyrus.robotics.profile.robotics.components.System) {
				system = (org.eclipse.papyrus.robotics.profile.robotics.components.System) context;
			} else if (context instanceof Element && UMLUtil.getStereotypeApplication((Element) context,
					org.eclipse.papyrus.robotics.profile.robotics.components.System.class) != null) {
				system = UMLUtil.getStereotypeApplication((Element) context,
						org.eclipse.papyrus.robotics.profile.robotics.components.System.class);
			}
		}

		StringBuffer oss = new StringBuffer();

		// Evaluating a single expression
		if (expression != null) {
			if (system != null) {
				oss = ossFromSystem(system, true);
				// insert a contract at system level
				int systemRef = oss.indexOf("REFINEMENT");
				String contract = "CONTRACT pass\nassume: true;\nguarantee: " + expression + ";\n";
				oss.insert(systemRef, contract);
			} else if (compInstance != null) {
				ComponentOrSystem compDef = compInstance.getCompdefOrSys();
				oss = ossFromCompDefinition(compDef, true);
				oss.append("CONTRACT pass\n");
				oss.append("assume: true;\n");
				oss.append("guarantee: " + expression + ";\n");
			} else if (compDefinition != null) {
				oss = ossFromCompDefinition(compDefinition, true);
				oss.append("CONTRACT pass\n");
				oss.append("assume: true;\n");
				oss.append("guarantee: " + expression + ";\n");
			}
		}

		if (expression == null) {
			// Evaluate a system
			if (system != null) {
				oss = ossFromSystem(system, true);
			}
			// Evaluating a component instance
			else if (compInstance != null) {
				ComponentOrSystem compDef = compInstance.getCompdefOrSys();
				oss = ossFromCompDefinition(compDef, true);
				List<Contract> contracts = AssertionsHelper.getCompDefinitionContracts(compDef);
				for (Contract contract : contracts) {
					oss.append(ossFromContract(compInstance, contract));
				}
				// Evaluating component definition
			} else if (compDefinition != null) {
				oss = ossFromCompDefinition(compDefinition, true);
				List<Contract> contracts = AssertionsHelper.getCompDefinitionContracts(compDefinition);
				for (Contract contract : contracts) {
					oss.append(ossFromContract(compDefinition, contract));
				}
			}
		}
		return oss.toString();
	}

	public boolean isInputPort(Port port) {
		ComponentPort compPort = (ComponentPort) port;
		if (compPort.isCoordinationPort()) {
			return false;
		}
		boolean input = compPort.getRequires() != null;
		return input;
	}

}
