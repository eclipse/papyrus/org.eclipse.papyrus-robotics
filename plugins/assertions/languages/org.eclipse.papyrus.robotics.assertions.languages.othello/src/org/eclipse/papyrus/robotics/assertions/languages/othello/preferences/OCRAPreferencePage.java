/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 * 	 Angel López , Tecnalia
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.languages.othello.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.papyrus.robotics.assertions.languages.othello.activator.Activator;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class OCRAPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public static final String OCRA_PATH = "OCRA_PATH"; //$NON-NLS-1$
	
	public OCRAPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("OCRA Preferences"); //$NON-NLS-1$
	}
	
	public void createFieldEditors() {
		addField(new FileFieldEditor(OCRA_PATH, "&OCRA executable:", getFieldEditorParent()));
		//addField(new StringFieldEditor(PreferenceConstants.P_STRING, "A &text preference:", getFieldEditorParent()));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}