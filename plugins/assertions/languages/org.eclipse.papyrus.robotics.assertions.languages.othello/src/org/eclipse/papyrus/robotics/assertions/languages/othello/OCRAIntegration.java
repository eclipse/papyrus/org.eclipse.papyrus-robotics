/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 * 	 Angel López , Tecnalia
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.othello;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class OCRAIntegration {

	public static final String COMMAND_CHECK_REFINEMENT = "ocra_check_refinement"; //$NON-NLS-1$
	public static final String COMMAND_CHECK_CONSISTENCY = "ocra_check_consistency"; //$NON-NLS-1$

	protected static final String ERROR = "ERROR"; //$NON-NLS-1$
	protected static final String OUTPUT = "OUTPUT"; //$NON-NLS-1$

	/**
	 * Get Console result
	 * 
	 * @param absolutePathToOCRA
	 * @param absolutePathToOSSFile
	 * @param command
	 * @return console result from ocra
	 */
	public static String getConsoleResult(String absolutePathToOCRA, String absolutePathToOSSFile, String command) {
		try {

			StringBuffer processText = new StringBuffer();

			Runtime rt = Runtime.getRuntime();
			// Launch the process
			Process proc = rt.exec(absolutePathToOCRA);
			OCRAIntegration ocraInt = new OCRAIntegration();
			ProcessReaderThread errorStream = ocraInt.new ProcessReaderThread(proc.getErrorStream(), ERROR, processText);
			ProcessReaderThread outputStream = ocraInt.new ProcessReaderThread(proc.getInputStream(), OUTPUT, processText);

			errorStream.start();
			outputStream.start();

			BufferedWriter processInput = new BufferedWriter(new OutputStreamWriter(proc.getOutputStream()));
			String commandToSend = command + " -i " + absolutePathToOSSFile + "\n";
			processInput.write(commandToSend);
			processInput.flush();

			commandToSend = "quit\n";
			processInput.write(commandToSend);
			processInput.flush();

			// Wait to get the exit of the process
			proc.waitFor();
			String result = processText.toString();
			// remove header comments like ocra version etc.
			result = result.replaceAll("\\*\\*\\*.*\\n", "");
			// remove command line
			result = result.replaceAll("ocra >", "");
			// remove empy lines
			result = result.replaceAll("(?m)^[ \t]*\r?\n", "");
			return result;
			// return processText.toString();
		} catch (Exception t) {
			t.printStackTrace();
			return null;
		}
	}

	/**
	 * No append, just overwrite with new text
	 * 
	 * @param file
	 * @param text
	 * @throws Exception
	 */
	public static void writeFile(File file, String text) throws Exception {
		BufferedWriter output;
		output = new BufferedWriter(new FileWriter(file, false));
		output.append(text);
		output.close();
	}

	public class ProcessReaderThread extends Thread {
		InputStream is;
		String type;

		StringBuffer processText;

		/**
		 * 
		 * @param is
		 *            an input stream
		 * @param type
		 *            OUTPUT or ERROR
		 * @param processText
		 */
		public ProcessReaderThread(InputStream is, String type, StringBuffer processText) {
			this.is = is;
			this.type = type;
			this.processText = processText;
		}

		public void run() {
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null) {
					if (type.equals(OUTPUT)) {
						processText.append(line);
						processText.append("\n"); //$NON-NLS-1$
					}
					if (type.equals(ERROR)) {
						processText.append(line);
						processText.append("\n"); //$NON-NLS-1$
					}
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
}
