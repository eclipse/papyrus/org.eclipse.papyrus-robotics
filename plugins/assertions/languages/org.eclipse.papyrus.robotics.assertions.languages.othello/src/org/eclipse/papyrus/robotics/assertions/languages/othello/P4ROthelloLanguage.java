/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.othello;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.assertions.languages.P4RExpressionsHelper;

public class P4ROthelloLanguage extends OthelloLanguage {

	@Override
	public String getName() {
		return "P4R Othello"; //$NON-NLS-1$
	}
	
	@Override
	public String updateExpression(EObject context, String expression) {
		return P4RExpressionsHelper.updateExpression(context, expression);
	}

}
