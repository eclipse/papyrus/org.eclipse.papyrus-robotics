/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.othello;

import org.eclipse.papyrus.robotics.assertions.languages.editor.P4RBasicLanguageEditor;

public class P4ROthelloLanguageEditor extends P4RBasicLanguageEditor {

	@SuppressWarnings("nls")
	public String[] keywords = new String[] { "not", "big_or", "big_and", "and", "or", "xor", "implies", "iff",
			"always", "never", "in the future", "then", "until", "releases", "historically", "in the past",
			"previously", "since", "triggered", "at next", "at last", "true", "false", "time_until", "time_since",
			"fall", "rise", "change", "der", "next", "case", "esac", "extend", "signed word", "unsigned word", "in",
			"word1", "swconst", "uwconst", "signed", "unsigned", "=", "!=", "<", ">", "<=", ">=" };
	
	@Override
	public String[] getKeywords() {
		return keywords;
	}
}
