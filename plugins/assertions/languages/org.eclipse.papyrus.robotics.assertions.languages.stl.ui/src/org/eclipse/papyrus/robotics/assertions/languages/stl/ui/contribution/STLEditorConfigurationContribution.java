/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.languages.stl.ui.contribution;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.papyrus.infra.gmfdiag.extensionpoints.editors.configuration.ICustomDirectEditorConfiguration;
import org.eclipse.papyrus.robotics.assertions.languages.stl.stlText.assignments;
import org.eclipse.papyrus.robotics.assertions.languages.stl.ui.internal.StlActivator;
import org.eclipse.papyrus.uml.xtext.integration.AbstractXtextDirectEditorConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.OpaqueExpression;

import com.google.inject.Injector;

/**
 * @author CEA LIST
 *
 *         This class is used for contribution to the Papyrus extension point DirectEditor. It is used for the integration
 *         of an xtext generated editor for RobMoSys components
 */
@SuppressWarnings("nls")
public class STLEditorConfigurationContribution extends AbstractXtextDirectEditorConfiguration implements ICustomDirectEditorConfiguration {

	OpaqueExpression oe;

	/**
	 * {@inheritDoc}
	 * Initialize lastNames map
	 */
	@Override
	public Object preEditAction(Object objectToEdit) {
		if (objectToEdit instanceof Constraint) {
			Constraint c = (Constraint) objectToEdit;
			if (c.getSpecification() instanceof OpaqueExpression) {
				oe = (OpaqueExpression) c.getSpecification();
			}
		}
		return super.preEditAction(objectToEdit);
	}

	/**
	 * Override to change style to {@link SWT}.MULTI
	 */
	@Override
	public int getStyle() {
		return SWT.MULTI | SWT.WRAP;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.papyrus.infra.gmfdiag.xtext.glue.PopupEditorConfiguration#getTextToEdit(java.lang.Object)
	 */
	@Override
	public String getTextToEdit(Object editedObject) {
		if (oe != null) {
			return oe.getBodies().get(0);
		}
		return "not an opaque expression";
	}

	/**
	 * @author CEA LIST
	 *
	 *         A command for updating the context UML model
	 */
	protected class UpdateComponentCommand extends AbstractTransactionalCommand {

		private Class clazz;

		private assignments stlText;

		public UpdateComponentCommand(TransactionalEditingDomain domain, assignments stlText) {
			super(domain, "Update component", getWorkspaceFiles(oe)); //$NON-NLS-1$
			this.stlText = stlText;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand#doExecuteWithResult(org.eclipse.core.runtime.IProgressMonitor
		 * , org.eclipse.core.runtime.IAdaptable)
		 */
		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor arg0, IAdaptable arg1) throws ExecutionException {

			// extract information from the component, and update class

			// Create the new component
			if (oe != null) {
			}

			return CommandResult.newOKCommandResult(clazz);
		}
	}

	@Override
	public Injector getInjector() {
		return StlActivator.getInstance().getInjector(StlActivator.ORG_ECLIPSE_PAPYRUS_ROBOTICS_ASSERTIONS_LANGUAGES_STL_STL);
	}

	@Override
	public ICommand getParseCommand(EObject modelObject, EObject xtextObject) {

		if (!(modelObject instanceof OpaqueExpression)) {
			return null;
		}

		assignments stlText = (assignments) xtextObject;
		// component may be null, if we have no input left

		// Creates and executes the update command
		TransactionalEditingDomain dom = TransactionUtil.getEditingDomain(oe);
		UpdateComponentCommand updateCommand = new UpdateComponentCommand(dom, stlText);
		return updateCommand;
	}
}
