/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 * 	 Angel López, Tecnalia
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.stl.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.assertions.languages.IExpressionLanguage;

public class STLLanguage implements IExpressionLanguage {

	public static final String OSS_EXT = ".stl"; //$NON-NLS-1$
	public static final String P4R_TEMP = "p4r_temp_"; //$NON-NLS-1$
	public static final String NAME = "STL"; //$NON-NLS-1$

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Object evaluate(EObject context, String expression) {
		// ...
		return null;
	}

	@Override
	public boolean isGlobalEvaluation() {
		return false;
	}
}
