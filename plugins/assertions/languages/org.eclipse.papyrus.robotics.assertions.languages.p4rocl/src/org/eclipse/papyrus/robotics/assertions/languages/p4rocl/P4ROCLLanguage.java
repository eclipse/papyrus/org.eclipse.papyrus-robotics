/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia 
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.p4rocl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.assertions.languages.AssertionsHelper;
import org.eclipse.papyrus.robotics.assertions.languages.IExpressionLanguage;
import org.eclipse.papyrus.robotics.assertions.languages.P4RExpressionsHelper;
import org.eclipse.papyrus.robotics.assertions.languages.ocl.OCLExpressionLanguage;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterInstance;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * P4R OCL allowing to access Parameters and Properties of the parameters
 * 
 * TODO Beta/Prototype version TODO Limitations in name overlapping TODO No
 * check for infinite loops
 */
public class P4ROCLLanguage implements IExpressionLanguage {

	@Override
	public String getName() {
		return "P4R OCL"; //$NON-NLS-1$
	}

	@Override
	public Object evaluate(EObject context, String expression) {

		// --> Establish the current context

		ComponentDefinition compDefinition = null;
		ComponentInstance compInstance = null;
		org.eclipse.papyrus.robotics.profile.robotics.components.System system = null;

		// isCompDefinition
		if (context instanceof ComponentDefinition) {
			compDefinition = (ComponentDefinition) context;
		} else if (context instanceof Element
				&& UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class) != null) {
			compDefinition = UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class);
		}

		// isCompInstance
		if (compDefinition == null) {
			if (context instanceof ComponentInstance) {
				compInstance = (ComponentInstance) context;
			} else if (context instanceof Element
					&& UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class) != null) {
				compInstance = UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class);
			}
		}

		// isSystem
		if (compDefinition == null && compInstance == null) {
			if (context instanceof org.eclipse.papyrus.robotics.profile.robotics.components.System) {
				system = (org.eclipse.papyrus.robotics.profile.robotics.components.System) context;
			} else if (context instanceof Element && UMLUtil.getStereotypeApplication((Element) context,
					org.eclipse.papyrus.robotics.profile.robotics.components.System.class) != null) {
				system = UMLUtil.getStereotypeApplication((Element) context,
						org.eclipse.papyrus.robotics.profile.robotics.components.System.class);
			}
		}

		if (compDefinition != null) {
			expression = P4RExpressionsHelper.updateExpression(compDefinition, expression);
		} else if (compInstance != null) {
			expression = P4RExpressionsHelper.updateExpression(compInstance, expression);
		} else if (system != null) {
			expression = P4RExpressionsHelper.updateExpression(system, expression);
		}

		// evaluate as OCL
		OCLExpressionLanguage ocl = new OCLExpressionLanguage();
		Object evaluation = ocl.evaluate(context, expression);
		return evaluation;
	}

	/**
	 * Handle global sum
	 * 
	 * @param context
	 * @param expression
	 * @return updated expression
	 */
	public String handleGlobalSum(EObject context, String expression) {
		// Allow to have more than one GlobalSum in an expression
		List<String> toReplace = new ArrayList<String>();
		int igs = expression.indexOf("GlobalSum(");
		while (igs != -1) {
			String propertyInExpression = expression.substring(igs + "GlobalSum(".length(),
					expression.indexOf(")", igs));
			if (!toReplace.contains(propertyInExpression)) {
				toReplace.add(propertyInExpression);
			}
			igs = expression.indexOf("GlobalSum(", igs + 1);
		}

		// no global sums so exit
		if (toReplace.isEmpty()) {
			return expression;
		}

		org.eclipse.papyrus.robotics.profile.robotics.components.System system = (org.eclipse.papyrus.robotics.profile.robotics.components.System) context;
		List<ComponentInstance> instances = system.getInstances();
		// For each required property
		for (String propertyInExpression : toReplace) {
			StringBuffer globalSum = new StringBuffer("");
			for (ComponentInstance instance : instances) {
				// check in properties
				Map<String, ValueSpecification> instanceProperties = AssertionsHelper.getProperties(
						instance.getCompdefOrSys().getBase_Class());
				if (instanceProperties.containsKey(propertyInExpression)) {
					globalSum.append(instance.getBase_Property().getName());
					globalSum.append(".");
					globalSum.append(propertyInExpression);
					globalSum.append(" + ");
				}
				// TODO check in parameters
			}
			// remove last plus
			globalSum.setLength(globalSum.length() - " + ".length());
			expression = expression.replaceAll("GlobalSum\\(" + propertyInExpression + "\\)", " (" + globalSum + ") ");
		}
		return expression;
	}

	/**
	 * Get component definition default parameters
	 * 
	 * @param compDef
	 *            a component definition (or system)
	 * @return default parameters
	 */
	public Map<String, String> getCompDefinitionDefaultParameters(ComponentOrSystem compDef) {
		Map<String, String> defaultParameters = new HashMap<String, String>();
		Iterator<EObject> i = compDef.getBase_Class().eAllContents();
		while (i.hasNext()) {
			EObject eo = i.next();
			ParameterEntry parameter = UMLUtil.getStereotypeApplication((Element) eo, ParameterEntry.class);
			// context was a componentDefinition
			if (parameter != null) {
				String name = parameter.getBase_Property().getName();
				String value = "";
				if (parameter.getBase_Property().getDefaultValue() != null) {
					// They use to be literals but we evaluate in case it uses another language
					Object result = AssertionsHelper.evaluateValueSpecification(compDef,
							parameter.getBase_Property().getDefaultValue());
					value = result.toString();
				}
				defaultParameters.put(name, value);
			}
		}
		return defaultParameters;
	}

	/**
	 * Get component instance parameters
	 * 
	 * @param compInstance
	 * @return parameters
	 */
	public Map<String, String> getCompInstanceParameters(ComponentInstance compInstance) {
		// first get the default ones
		Map<String, String> parameters = getCompDefinitionDefaultParameters(compInstance.getCompdefOrSys());

		// Comp Instance's parameter values will override Comp definition default values
		Iterator<EObject> i = compInstance.getBase_Property().eAllContents();
		while (i.hasNext()) {
			EObject eo = i.next();
			if (eo instanceof InstanceValue) {
				EObject eo2 = ((InstanceValue) eo).getInstance();
				ParameterInstance paraSpec = UMLUtil.getStereotypeApplication((Element) eo2, ParameterInstance.class);
				List<Slot> slots = paraSpec.getBase_InstanceSpecification().getSlots();
				// Override the default value
				if (slots != null && !slots.isEmpty()) {
					// TODO improve this, only the first is taken
					Slot slot = paraSpec.getBase_InstanceSpecification().getSlots().get(0);
					String name = slot.getDefiningFeature().getName();
					ValueSpecification vs = slot.getValues().get(0);
					String value = vs.stringValue();
					parameters.put(name, value);
				}
			}
		}
		return parameters;
	}

	@Override
	public boolean isGlobalEvaluation() {
		return false;
	}

}
