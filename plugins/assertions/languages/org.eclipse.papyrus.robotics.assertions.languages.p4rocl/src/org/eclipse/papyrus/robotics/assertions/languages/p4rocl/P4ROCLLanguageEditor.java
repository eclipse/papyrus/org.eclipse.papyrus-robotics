/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution"," and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez"," Tecnalia 
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.languages.p4rocl;

import org.eclipse.papyrus.robotics.assertions.languages.editor.P4RBasicLanguageEditor;

public class P4ROCLLanguageEditor extends P4RBasicLanguageEditor {

	@SuppressWarnings("nls")
	@Override
	public String[] getKeywords() {
		// TODO not complete
		return new String[] { "self", "if", "then", "else", "endif", "let", "in", "->", "Collection", "Set", "Bag",
				"OrderedSet", "Sequence", "oclIsTypeOf", "oclIsKindOf", "oclAsType", "oclIsInState", "oclIsNew", "div",
				"mod", "abs", "max", "min", "<", ">", "<=", ">=", "toString", "floor", "round", "not", "and", "or",
				"xor", "implies", "size", "concat", "substring", "toInteger", "toReal", "toUpperCase", "toLowerCase",
				"indexOf", "equalsIgnoreCase", "at", "characters", "toBoolean", "<>", "includes", "excludes", "count",
				"includesAll", "excludesAll", "isEmpty", "notEmpty", "sum", "product", "selectByKind", "selectByType",
				"asSet", "asOrderedSet", "asSequence", "asBag", "flatten", "any", "closure", "collect", "collectNested",
				"exists", "forAll", "isUnique", "one", "reject", "select", "sortedBy", "iterate", "union",
				"intersection", "including", "excluding", "symmetricDifference" };
	}

}
