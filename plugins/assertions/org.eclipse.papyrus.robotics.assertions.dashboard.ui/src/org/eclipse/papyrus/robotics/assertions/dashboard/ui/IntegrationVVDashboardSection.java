/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *   Alejandra Ruiz, Tecnalia -  Provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.dashboard.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.assertions.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.dashboard.ui.extrasections.IDashboardSection;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.CompIntegrationVVPage;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.P4RProjectModelHyperlink;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.osgi.framework.Bundle;

public class IntegrationVVDashboardSection implements IDashboardSection {

	@Override
	public String getName() {
		return "Early verification";
	}

	@Override
	public String getDescription() {
		return "The safety engineer performs an early verification";
	}

	@Override
	public String getExtendedPageID() {
		return CompIntegrationVVPage.ID;
	}

	@Override
	public void createComposite(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		
		ImageDescriptor imageDescContract = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/validateAssertion.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Validate assertion at system level", 
				imageDescContract.createImage(), ".system", "Assertions are validated against the system context. Use the Properties View to validate them");

		
		ImageDescriptor imageDescShow = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/validateContract.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Validate a contract at system level", 
				imageDescShow.createImage(), ".system", "Validate assumptions and guarantees included in the contract against the contest. Use the Properties View to validate them");

	
	}

}
