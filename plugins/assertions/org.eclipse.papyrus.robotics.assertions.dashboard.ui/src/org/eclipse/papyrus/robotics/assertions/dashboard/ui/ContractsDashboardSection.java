/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *   Alejandra Ruiz, Tecnalia -  Provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.dashboard.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.assertions.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.dashboard.ui.extrasections.IDashboardSection;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.P4RProjectModelHyperlink;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.SystemIntegrationPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.osgi.framework.Bundle;

public class ContractsDashboardSection implements IDashboardSection {

	@Override
	public String getName() {
		return "Contracts - Assertions";
	}

	@Override
	public String getDescription() {
		return "The system builder defines the mandatory non-functional requirements of the system through contract-based design.";
	}

	@Override
	public String getExtendedPageID() {
		return SystemIntegrationPage.ID;
	}

	@Override
	public void createComposite(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		

		ImageDescriptor imageDescContract = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/contract.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Create a contract", 
				imageDescContract.createImage(), "system", "Contracts can be attached to different Robmosys elements e.g system, component, behavior trees. Contracts have two separated sets which are the assumptions and the guarantees. These assumptions and guarantees are assertions");	

		ImageDescriptor imageDescShow = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/glass.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Show existing contracts", 
				imageDescShow.createImage(), "system", "Open the properties tab and the list of contracts associated will be shown");	
	
	}

}
