/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial creation and implementation
 *   Alejandra Ruiz, Tecnalia -  Provide content and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.dashboard.ui;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.papyrus.robotics.assertions.dashboard.ui.activator.Activator;
import org.eclipse.papyrus.robotics.dashboard.ui.extrasections.IDashboardSection;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.ComponentDesignPage;
import org.eclipse.papyrus.robotics.dashboard.ui.pages.P4RProjectModelHyperlink;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.osgi.framework.Bundle;

public class AssertionsDashboardSection implements IDashboardSection {

	@Override
	public String getName() {
		return "Assertions";
	}

	@Override
	public String getDescription() {
		return "The component supplier which is responsible of non-functional properties, shall make explicit the formal property values and assertions that should be satisfied by a component.";
	}

	@Override
	public String getExtendedPageID() {
		return ComponentDesignPage.ID;
	}

	@Override
	public void createComposite(Composite container) {
		FormToolkit toolkit = new FormToolkit(container.getDisplay());
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		
		ImageDescriptor imageDescFP = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/nfproperty.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Add a formal property to an existing Component model", 
				imageDescFP.createImage(), "compdef.di", "Similar to Parameters in RobMoSys components (pair name, value). However, parameters must be used for configuration options while NFProperties should be used to attach non-functional information to model elements (e.g. component, port). Use the Properties View to define them");
					
		ImageDescriptor imageDescAss = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/assertion.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Define Assertions attached to an existing Component model", 
				imageDescAss.createImage(), "compdef.di", 
				"Assertions should be defined in a language. A key aspect of assertions is providing the context for its evaluation. This way, assertions should be attached to elements. Use the Properties View to define them");

		ImageDescriptor imageDescV_A = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path("icons/validateAssertion.png")));
		P4RProjectModelHyperlink.openAndShowMessage(toolkit, container, "Validates component assertion", 
				imageDescV_A.createImage(), "compdef.di", "Assertions are validated against the component as a context. Use the Properties View to validate them");

	 	
	}

}
