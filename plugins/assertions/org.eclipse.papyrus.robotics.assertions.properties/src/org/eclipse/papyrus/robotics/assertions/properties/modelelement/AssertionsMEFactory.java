/*****************************************************************************
 * Copyright (c) 2019 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *  (inspired by UML/RT implementation)
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.properties.modelelement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.infra.properties.contexts.DataContextElement;
import org.eclipse.papyrus.infra.properties.ui.modelelement.EMFModelElement;
import org.eclipse.papyrus.robotics.assertions.properties.Activator;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElementFactory;
import org.eclipse.papyrus.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AssertionsMEFactory extends StereotypeModelElementFactory {

	@Override
	protected EMFModelElement doCreateFromSource(Object sourceElement, DataContextElement context) {
		EObject source = EMFHelper.getEObject(sourceElement);
		EMFModelElement modelElement = null;
		if (source != null) {
			if (source instanceof Property && StereotypeUtil.isApplied((Property) source, org.eclipse.papyrus.robotics.assertions.profile.assertions.Property.class)) {
				Property nfp = (Property) source;
				Stereotype st = nfp.getAppliedStereotypes().get(0);
				org.eclipse.papyrus.robotics.assertions.profile.assertions.Property propertySt
					= UMLUtil.getStereotypeApplication(nfp, org.eclipse.papyrus.robotics.assertions.profile.assertions.Property.class);
				modelElement = new AssertionsStereotypeME(propertySt, st);
			}
			else {
				modelElement = super.doCreateFromSource(sourceElement, context);
			}
		} else {
			Activator.log.warn("Unable to resolve the selected element to an EObject"); //$NON-NLS-1$
		}

		return modelElement;
	}
}
