/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.properties.modelelement;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Property;
import org.eclipse.papyrus.uml.properties.modelelement.StereotypeModelElement;
import org.eclipse.papyrus.uml.properties.modelelement.UMLModelElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Stereotype;

/**
 * A ModelElement provider for assertion stereotypes, currently used for displaying the opaque expression
 * of a Property (enabling the use of the OpaqueExpression property view, if a Property is selected).
 */
public class AssertionsStereotypeME extends StereotypeModelElement {

	/**
	 * Constructor.
	 */
	public AssertionsStereotypeME(EObject stereotypeApplication, Stereotype stereotype) {
		this(stereotypeApplication, stereotype, TransactionUtil.getEditingDomain(stereotypeApplication));
	}

	/**
	 * Constructor.
	 */
	public AssertionsStereotypeME(EObject stereotypeApplication, Stereotype stereotype, EditingDomain domain) {
		super(stereotypeApplication, stereotype, domain);
	}

	public void updateSource(EObject source) {
		this.source = source;
	}

	@Override
	public IObservable doGetObservable(String propertyPath) {
		IObservable observable = null;

		if (propertyPath.endsWith("body")) {
			observable = getExpressionObservable("body");
		} else if (propertyPath.endsWith("language")) {
			observable = getExpressionObservable("language");
		}

		if (observable != null) {
			return observable;
		} else {
			return super.doGetObservable(propertyPath);
		}
	}

	protected IObservable getExpressionObservable(String language) {
		if (source instanceof Property) {
			Property property = (Property) source;
			OpaqueExpression oe = property.getExpression();
			if (oe != null) {
				return new UMLModelElement(oe, domain).getObservable(language);
			}
		}
		return null;
	}
}
