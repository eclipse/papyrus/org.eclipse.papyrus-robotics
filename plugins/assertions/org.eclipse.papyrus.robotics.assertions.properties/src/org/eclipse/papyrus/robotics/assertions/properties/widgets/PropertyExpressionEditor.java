/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.properties.widgets;

import org.eclipse.papyrus.infra.properties.ui.modelelement.DataSource;
import org.eclipse.papyrus.infra.properties.ui.modelelement.ModelElement;
import org.eclipse.papyrus.uml.properties.widgets.ExpressionEditor;
import org.eclipse.swt.widgets.Composite;

public class PropertyExpressionEditor extends ExpressionEditor {

	public PropertyExpressionEditor(Composite parent, int style) {
		super(parent, style);
	}

	// use a wrapper that replaces the hard-coded body both in the standard ExpressionEditor
	class DataSourceWrapper extends DataSource {

		DataSource ds;

		protected DataSourceWrapper(DataSource ds) {
			super(ds.getView(), ds.getSelection());
			this.ds = ds;
		}

		@Override
		public ModelElement getModelElement(String propertyPath) {
			if (propertyPath.endsWith("body")) {
				propertyPath = "assertions:Property:expression.body";
			}
			return ds.getModelElement(propertyPath);
		}
	}

	@Override
	public void setInput(DataSource input) {
		super.setInput(new DataSourceWrapper(input));
	}
}
