/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *   Angel L�pez, Tecnalia - Implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.ui.actions;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.robotics.assertions.languages.AssertionsHelper;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Constraint;

public class ValidateAssertionHandler {

	public static void evaluateAndShowResult(Constraint constraint) {
		Object result = AssertionsHelper.evaluateConstraint(null, constraint);

		// display the value
		if (result != null && result.toString().equalsIgnoreCase("true")) {
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Validate Assertion",
					"Assertion is satisfied");
		} else {
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Validate Assertion",
					"Assertion is not satisfied\nReturned value: " + result);
		}
	}

}