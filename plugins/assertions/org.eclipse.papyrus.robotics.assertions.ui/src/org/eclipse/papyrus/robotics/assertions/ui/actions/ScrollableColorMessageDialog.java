/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class ScrollableColorMessageDialog extends TitleAreaDialog {
	private String title;
	private String text;
	private String scrollableText;
	private List<List<Integer>> lines;
	private List<Color> colors;
	private int type; // e.g., IMessageProvider.INFORMATION

	public ScrollableColorMessageDialog(Shell parentShell, String title, String text, String scrollableText, int type) {
		this(parentShell, title, text, scrollableText, new ArrayList<List<Integer>>(), new ArrayList<Color>(), type);
	}
	
	public ScrollableColorMessageDialog(Shell parentShell, String title, String text, String scrollableText) {
		this(parentShell, title, text, scrollableText, IMessageProvider.INFORMATION);
	}
	
	public ScrollableColorMessageDialog(Shell parentShell, String title, String text, String scrollableText,
			List<List<Integer>> lines, List<Color> colors) {
		this(parentShell, title, text, scrollableText, lines, colors, IMessageProvider.INFORMATION);
	}
	
	public ScrollableColorMessageDialog(Shell parentShell, String title, String text, String scrollableText,
			List<List<Integer>> lines, List<Color> colors, int type) {
		super(parentShell);
		this.title = title;
		this.text = text;
		this.scrollableText = scrollableText;
		this.lines = lines;
		this.colors = colors;
		this.type = type;
		setHelpAvailable(false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.verticalAlignment = GridData.FILL;

		StyledText scrollable = new StyledText(composite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.READ_ONLY);
		scrollable.setLayoutData(gridData);
		scrollable.setText(scrollableText);

		for (int ci = 0; ci < lines.size(); ci++) {
			List<Integer> fline = lines.get(ci);
			for (Integer linen : fline) {
				scrollable.setLineBackground(linen, 1, colors.get(ci));
			}
		}

		return composite;
	}

	@Override
	public void create() {
		super.create();
		setTitle(title);
		setMessage(text, type);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button okButton = createButton(parent, OK, "OK", true);
		okButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				close();
			}
		});
	}

	@Override
	protected boolean isResizable() {
		return true;
	}
}