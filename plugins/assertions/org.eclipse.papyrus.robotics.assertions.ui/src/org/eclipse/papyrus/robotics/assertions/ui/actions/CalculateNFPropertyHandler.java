/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *   Angel L�pez, Tecnalia - Implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.ui.actions;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.robotics.assertions.languages.AssertionsHelper;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CalculateNFPropertyHandler {

	public static void evaluateAndShowResult(ValueSpecification vs) {
		Object result = null;
		if(vs instanceof OpaqueExpression) {
			// Use component definition as context
			EObject context = vs.eContainer();
			while(context.eContainer() != null) {
				context = context.eContainer();
				ComponentDefinition st = UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class);
				if(st != null) {
					break;
				}
			}
			result = AssertionsHelper.evaluateOpaqueExpression(context, (OpaqueExpression)vs);
		}
		String print = null;
		if (result != null) {
			print = result.toString();
		} else {
			print = "null";
		}

		// display the value
		MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Calculate Property",
				"Value: " + print);
	}

}
