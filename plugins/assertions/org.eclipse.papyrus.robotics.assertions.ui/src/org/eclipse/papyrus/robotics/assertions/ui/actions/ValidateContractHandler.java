/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *   Angel L�pez, Tecnalia - Implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.robotics.assertions.languages.AssertionsHelper;
import org.eclipse.papyrus.robotics.assertions.languages.ExpressionLanguagesHelper;
import org.eclipse.papyrus.robotics.assertions.languages.IExpressionLanguage;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ValidateContractHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		EObject context = getSelectedElement();

		// --> Establish the current context

		ComponentDefinition compDefinition = null;
		ComponentInstance compInstance = null;
		org.eclipse.papyrus.robotics.profile.robotics.components.System system = null;
		Contract selectedContract = UMLUtil.getStereotypeApplication((Element) context, Contract.class);

		// isCompDefinition
		if (context instanceof ComponentDefinition) {
			compDefinition = (ComponentDefinition) context;
		} else if (context instanceof Element
				&& UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class) != null) {
			compDefinition = UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class);
		}

		// isCompInstance
		if (compDefinition == null) {
			if (context instanceof ComponentInstance) {
				compInstance = (ComponentInstance) context;
			} else if (context instanceof Element
					&& UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class) != null) {
				compInstance = UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class);
			}
		}

		// isSystem
		if (compDefinition == null && compInstance == null) {
			if (context instanceof org.eclipse.papyrus.robotics.profile.robotics.components.System) {
				system = (org.eclipse.papyrus.robotics.profile.robotics.components.System) context;
			} else if (context instanceof Element && UMLUtil.getStereotypeApplication((Element) context,
					org.eclipse.papyrus.robotics.profile.robotics.components.System.class) != null) {
				system = UMLUtil.getStereotypeApplication((Element) context,
						org.eclipse.papyrus.robotics.profile.robotics.components.System.class);
			}
		}

		// -> Evaluate
		List<Contract> contracts = new ArrayList<Contract>();
		if (selectedContract != null) {
			contracts.add(selectedContract);
			EObject contractContext = AssertionsHelper.getContractContext(selectedContract);
			evaluateContracts(contracts, contractContext);
		} else if (compDefinition != null) {
			contracts = AssertionsHelper.getCompDefinitionContracts(compDefinition);
			evaluateContracts(contracts, compDefinition);
		} else if (compInstance != null) {
			ComponentOrSystem cd = compInstance.getCompdefOrSys();
			contracts = AssertionsHelper.getCompDefinitionContracts(cd);
			evaluateContracts(contracts, compInstance);
		} else if (system != null) {
			// validate contracts of component instances
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Validate Contract",
					"Validating contracts of component instances");
			for (ComponentInstance ci : system.getInstances()) {
				ComponentOrSystem cd = ci.getCompdefOrSys();
				contracts = AssertionsHelper.getCompDefinitionContracts(cd);
				evaluateContracts(contracts, ci);
			}
			// validate contracts of instances
			contracts = AssertionsHelper.getSystemContracts(system);
			if (!contracts.isEmpty()) {
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Validate Contract",
						"Validating system contracts");
				evaluateContracts(contracts, system);
			}
		}

		if (contracts.isEmpty()) {
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Validate Contract",
					"Contracts validation finished");
		}

		return null;
	}

	/**
	 * Evaluate contracts
	 * 
	 * @param contracts
	 * @param context
	 */
	public static void evaluateContracts(List<Contract> contracts, EObject context) {

		List<String> globalLanguages = new ArrayList<String>();
		List<Integer> okLines = new ArrayList<Integer>();
		List<Integer> notokLines = new ArrayList<Integer>();
		
		for (Contract contract : contracts) {
			StringBuffer message = new StringBuffer();
			message.append(contract.getBase_Comment().getBody());
			message.append("\n");
			boolean assumptionsProblem = false;
			boolean guaranteesProblem = false;
			if (!contract.getAssumptions().isEmpty()) {
				message.append("Assumptions\n");
				for (Assertion assumption : contract.getAssumptions()) {
					if (!AssertionsHelper.isGlobalEvaluationAssertion(assumption)) {
						Object result = AssertionsHelper.evaluateConstraint(context,
								assumption.getBase_Constraint());
						message.append("(" + result + ") ");
						if (result == null || !result.toString().equalsIgnoreCase("true")) {
							notokLines.add(getCurrentLine(message.toString()));
							assumptionsProblem = true;
						} else {
							okLines.add(getCurrentLine(message.toString()));
						}
						message.append(assumption.getBase_Constraint().getLabel() + "\n");
					} else {
						String lang = AssertionsHelper.getAssertionLanguage(assumption);
						if (!globalLanguages.contains(lang)) {
							globalLanguages.add(lang);
						}
					}
				}
			}
			if (!contract.getGuarantees().isEmpty()) {
				message.append("Guarantees\n");
				for (Assertion guarantee : contract.getGuarantees()) {
					if (!AssertionsHelper.isGlobalEvaluationAssertion(guarantee)) {
						Object result = AssertionsHelper.evaluateConstraint(context,
								guarantee.getBase_Constraint());
						message.append("(" + result + ") ");
						if (result == null || !result.toString().equalsIgnoreCase("true")) {
							notokLines.add(getCurrentLine(message.toString()));
							guaranteesProblem = true;
						} else {
							okLines.add(getCurrentLine(message.toString()));
						}
						message.append(guarantee.getBase_Constraint().getLabel() + "\n");
					} else {
						String lang = AssertionsHelper.getAssertionLanguage(guarantee);
						if (!globalLanguages.contains(lang)) {
							globalLanguages.add(lang);
						}
					}
				}
			}

			// Global languages
			if (!globalLanguages.isEmpty()) {
				message.append("Global languages\n");
				for (String lang : globalLanguages) {
					message.append(lang);
					IExpressionLanguage langImpl = ExpressionLanguagesHelper.getExpressionLanguageByName(lang);
					Object result = langImpl.evaluate(context, null);
					message.append(" (" + result + ")\n");
					if (result == null || !result.toString().equalsIgnoreCase("true")) {
						notokLines.add(getCurrentLine(message.toString()));
						guaranteesProblem = true;
					} else {
						okLines.add(getCurrentLine(message.toString()));
					}
				}
			}

			// Display result to the user
			List<Color> colors = new ArrayList<Color>();
			Color OK = new Color(Display.getCurrent(), 174, 213, 129);
			Color NOTOK = new Color(Display.getCurrent(), 255, 138, 101);
			colors.add(OK);
			colors.add(NOTOK);
			List<List<Integer>> colorLines = new ArrayList<List<Integer>>();
			colorLines.add(okLines);
			colorLines.add(notokLines);

			ScrollableColorMessageDialog dialog = null;
			if (assumptionsProblem && !guaranteesProblem) {
				dialog = new ScrollableColorMessageDialog(Display.getCurrent().getActiveShell(), "Validate Contract",
						"Contract validation", message.toString(), colorLines, colors, IMessageProvider.WARNING);
			} else if (guaranteesProblem) {
				dialog = new ScrollableColorMessageDialog(Display.getCurrent().getActiveShell(), "Validate Contract",
						"Contract validation", message.toString(), colorLines, colors, IMessageProvider.ERROR);
			} else {
				dialog = new ScrollableColorMessageDialog(Display.getCurrent().getActiveShell(), "Validate Contract",
						"Contract validation", message.toString(), colorLines, colors);
			}
			dialog.open();
			OK.dispose();
			NOTOK.dispose();
		}
	}

	private static int getCurrentLine(String string) {
		return string.split("\n").length - 1;
	}
	
	/**
	 * Code from
	 * org.eclipse.papyrus.uml.oclconstraintevaluation.ComputeConstraintHandler
	 * 
	 * @return EObject
	 */
	public static EObject getSelectedElement() {
		EObject eObject = null;
		Object selection = null;

		// Get current selection
		selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();

		// Get first element if the selection is an IStructuredSelection
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			selection = structuredSelection.getFirstElement();
		}

		// Treat non-null selected object (try to adapt and return EObject)
		if (selection != null) {

			if (selection instanceof IAdaptable) {
				selection = ((IAdaptable) selection).getAdapter(EObject.class);
			}

			if (selection instanceof EObject) {
				eObject = (EObject) selection;
			}
		}
		return eObject;
	}

}