/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Property;
import org.eclipse.papyrus.robotics.core.utils.ParameterUtils;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterInstance;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

public class P4RExpressionsHelper {

	public static String updateExpression(EObject context, String expression) {

		// Get context

		ComponentDefinition compDefinition = null;
		ComponentInstance compInstance = null;
		org.eclipse.papyrus.robotics.profile.robotics.components.System system = null;

		// isCompDefinition
		if (context instanceof ComponentDefinition) {
			compDefinition = (ComponentDefinition) context;
		} else if (context instanceof Element
				&& UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class) != null) {
			compDefinition = UMLUtil.getStereotypeApplication((Element) context, ComponentDefinition.class);
		}

		// isCompInstance
		if (compDefinition == null) {
			if (context instanceof ComponentInstance) {
				compInstance = (ComponentInstance) context;
			} else if (context instanceof Element
					&& UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class) != null) {
				compInstance = UMLUtil.getStereotypeApplication((Element) context, ComponentInstance.class);
			}
		}

		// isSystem
		if (compDefinition == null && compInstance == null) {
			if (context instanceof org.eclipse.papyrus.robotics.profile.robotics.components.System) {
				system = (org.eclipse.papyrus.robotics.profile.robotics.components.System) context;
			} else if (context instanceof Element && UMLUtil.getStereotypeApplication((Element) context,
					org.eclipse.papyrus.robotics.profile.robotics.components.System.class) != null) {
				system = UMLUtil.getStereotypeApplication((Element) context,
						org.eclipse.papyrus.robotics.profile.robotics.components.System.class);
			}
		}

		// --> Global sum

		if (system != null) {
			expression = handleGlobalSum(system, expression);
		}

		// --> Parameters

		// get all parameter entries to replace in the expression
		Map<String, String> parameters = new HashMap<String, String>();

		if (compDefinition != null) {
			parameters = getCompDefinitionDefaultParameters(compDefinition);
		} else if (compInstance != null) {
			parameters = getCompInstanceParameters(compInstance);
		} else if (system != null) {
			List<ComponentInstance> instances = system.getInstances();
			for (ComponentInstance instance : instances) {
				String instanceName = instance.getBase_Property().getName();
				if (instanceName == null || instanceName.isEmpty()) {
					instanceName = "";
				} else {
					instanceName = instanceName + ".";
				}
				Map<String, String> instanceParameters = getCompInstanceParameters(instance);
				for (String key : instanceParameters.keySet()) {
					parameters.put(instanceName + key, instanceParameters.get(key));
				}
			}
		}

		// update expression with parameters
		for (String parameter : parameters.keySet()) {
			if (expression.contains(parameter)) {
				expression = expression.replaceAll(parameter, parameters.get(parameter));
			}
		}

		// --> Properties

		if (compDefinition != null || compInstance != null) {
			EObject compDef = null;
			if (compDefinition != null) {
				compDef = compDefinition.getBase_Class();
			} else if (compInstance != null) {
				compDef = compInstance.getCompdefOrSys().getBase_Class();
			}
			Map<String, ValueSpecification> properties = getProperties(compDef);
			for (String prop : properties.keySet()) {
				if (expression.contains(prop)) {
					Object result = AssertionsHelper.evaluateValueSpecification(compDef, properties.get(prop));
					expression = expression.replaceAll(prop, result.toString());
				}
			}
		}

		if (system != null) {
			// properties of component instances
			List<ComponentInstance> instances = system.getInstances();
			for (ComponentInstance instance : instances) {
				String instanceName = instance.getBase_Property().getName();
				if (instanceName == null || instanceName.isEmpty()) {
					instanceName = "";
				} else {
					instanceName = instanceName + ".";
				}
				Map<String, ValueSpecification> instanceProperties = getProperties(
						instance.getCompdefOrSys().getBase_Class());
				for (String key : instanceProperties.keySet()) {
					if (expression.contains(instanceName + key)) {
						Object result = AssertionsHelper.evaluateValueSpecification(instance.getCompdefOrSys(),
								instanceProperties.get(key));
						if (result == null) {
							result = "null";
						}
						expression = expression.replaceAll(instanceName + key, result.toString());
					}
				}
			}
			// system properties
			Map<String, ValueSpecification> systemProperties = getProperties(system.getBase_Class());
			for (String key : systemProperties.keySet()) {
				if (expression.contains(key)) {
					Object result = AssertionsHelper.evaluateValueSpecification(system,
							systemProperties.get(key));
					if (result == null) {
						result = "null";
					}
					expression = expression.replaceAll(key, result.toString());
				}
			}
		}
		return expression;
	}

	/**
	 * Handle global sum
	 * 
	 * @param context
	 * @param expression
	 * @return updated expression
	 */
	public static String handleGlobalSum(EObject context, String expression) {
		// Allow to have more than one GlobalSum in an expression
		List<String> toReplace = new ArrayList<String>();
		int igs = expression.indexOf("GlobalSum(");
		while (igs != -1) {
			String propertyInExpression = expression.substring(igs + "GlobalSum(".length(),
					expression.indexOf(")", igs));
			if (!toReplace.contains(propertyInExpression)) {
				toReplace.add(propertyInExpression);
			}
			igs = expression.indexOf("GlobalSum(", igs + 1);
		}

		// no global sums so exit
		if (toReplace.isEmpty()) {
			return expression;
		}

		org.eclipse.papyrus.robotics.profile.robotics.components.System system = (org.eclipse.papyrus.robotics.profile.robotics.components.System) context;
		List<ComponentInstance> instances = system.getInstances();
		// For each required property
		for (String propertyInExpression : toReplace) {
			StringBuffer globalSum = new StringBuffer("");
			for (ComponentInstance instance : instances) {
				// check in properties
				Map<String, ValueSpecification> instanceProperties = getProperties(
						instance.getCompdefOrSys().getBase_Class());
				if (instanceProperties.containsKey(propertyInExpression)) {
					globalSum.append(instance.getBase_Property().getName());
					globalSum.append(".");
					globalSum.append(propertyInExpression);
					globalSum.append(" + ");
				}
				// TODO check in parameters
			}
			// remove last plus
			globalSum.setLength(globalSum.length() - " + ".length());
			expression = expression.replaceAll("GlobalSum\\(" + propertyInExpression + "\\)", " (" + globalSum + ") ");
		}
		return expression;
	}

	/**
	 * Get Properties
	 * 
	 * @param context
	 * @return properties
	 */
	public static Map<String, ValueSpecification> getProperties(EObject context) {
		Map<String, ValueSpecification> properties = new HashMap<String, ValueSpecification>();
		Iterator<EObject> it = context.eAllContents();
		while (it.hasNext()) {
			EObject eo = it.next();
			Property property = UMLUtil.getStereotypeApplication((Element) eo, Property.class);
			if (property != null) {
				String name = property.getBase_Property().getName();
				ValueSpecification vs = property.getBase_Property().getDefaultValue();
				properties.put(name, vs);
			}
		}
		return properties;
	}

	/**
	 * Get component instance parameters
	 * 
	 * @param compInstance
	 * @return parameters
	 */
	public static Map<String, String> getCompInstanceParameters(ComponentInstance compInstance) {
		// first get the default ones
		Map<String, String> parameters = getCompDefinitionDefaultParameters(compInstance.getCompdefOrSys());

		// Comp Instance's parameter values will override Comp definition default values
		Iterator<EObject> i = compInstance.getBase_Property().eAllContents();
		while (i.hasNext()) {
			EObject eo = i.next();
			if (eo instanceof InstanceValue) {
				EObject eo2 = ((InstanceValue) eo).getInstance();
				ParameterInstance paraSpec = UMLUtil.getStereotypeApplication((Element) eo2, ParameterInstance.class);
				List<Slot> slots = paraSpec.getBase_InstanceSpecification().getSlots();
				// Override the default value
				if (slots != null && !slots.isEmpty()) {
					// TODO improve this, only the first is taken
					Slot slot = paraSpec.getBase_InstanceSpecification().getSlots().get(0);
					String name = slot.getDefiningFeature().getName();
					ValueSpecification vs = slot.getValues().get(0);
					String value = vs.stringValue();
					parameters.put(name, value);
				}
			}
		}
		return parameters;
	}

	/**
	 * Get component definition default parameters
	 * 
	 * @param compDef
	 * @return default parameters
	 */
	public static Map<String, String> getCompDefinitionDefaultParameters(ComponentOrSystem compDef) {
		Map<String, String> defaultParameters = new HashMap<String, String>();
		List<org.eclipse.uml2.uml.Property> parameters = ParameterUtils.getAllParameters(compDef.getBase_Class());
		for (org.eclipse.uml2.uml.Property parameter : parameters) {
			String name = parameter.getName();
			String value = ""; //$NON-NLS-1$
			if (parameter.getDefaultValue() != null) {
				// They use to be literals but we evaluate in case it uses another language
				Object result = AssertionsHelper.evaluateValueSpecification(compDef,
						parameter.getDefaultValue());
				value = result.toString();
			}
			defaultParameters.put(name, value);
		}
		return defaultParameters;
	}

}
