/*****************************************************************************
 * Copyright (c) 2011 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Camille Letavernier (CEA LIST) camille.letavernier@cea.fr - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.editor;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.databinding.observable.value.AbstractObservableValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.properties.ui.modelelement.ModelElement;
import org.eclipse.papyrus.infra.widgets.editors.AbstractEditor;
import org.eclipse.papyrus.infra.widgets.editors.ICommitListener;
import org.eclipse.papyrus.uml.properties.modelelement.UMLModelElement;
import org.eclipse.papyrus.uml.properties.widgets.BodyEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * Adapted from NaturalLanguageEditor to use a StyledText instead of Text
 * 
 * A BodyEditor for the natural language. This is a basic text-box.
 *
 * @author Camille Letavernier
 */
public class P4RBasicLanguageEditor implements BodyEditor {

	private StyledStringEditor editor;

	private final Set<Listener> changeListeners = new HashSet<Listener>();

	private String currentValue;

	public void createWidget(Composite parent, int style) {
		editor = new StyledStringEditor(parent, style | SWT.MULTI | SWT.WRAP) {

			@Override
			protected GridData getDefaultLayoutData() {
				GridData data = super.getDefaultLayoutData();
				data.grabExcessVerticalSpace = true;
				data.verticalAlignment = SWT.FILL;
				return data;
			}
		};

		editor.addCommitListener(new ICommitListener() {

			public void commit(AbstractEditor editor) {
				Event event = new Event();
				currentValue = (String) P4RBasicLanguageEditor.this.editor.getValue();
				event.text = (String) P4RBasicLanguageEditor.this.editor.getValue();
				for (Listener listener : changeListeners) {
					listener.handleEvent(event);
				}
			}
		});

		editor.layout();
		
		editor.setKeywords(getKeywords());
	}

	/**
	 * To be overriden by new editors
	 * @return list of keywords
	 */
	public String[] getKeywords() {
		return new String[] {};
	}

	public void setInput(String value) {
		currentValue = value;

		IObservableValue observable = new AbstractObservableValue() {

			public Object getValueType() {
				return String.class;
			}

			@Override
			protected Object doGetValue() {
				return currentValue;
			}

			@Override
			protected void doSetValue(Object value) {
				if (value instanceof String) {
					currentValue = (String) value;
				}
			}

		};

		editor.setModelObservable(observable);
		
		editor.updateSyntaxHighlighting();
	}

	public void dispose() {
		editor.dispose();
		changeListeners.clear();
	}

	public void addChangeListener(Listener listener) {
		changeListeners.add(listener);
	}

	public void removeChangeListener(Listener listener) {
		changeListeners.remove(listener);
	}

	public String getValue() {
		return currentValue;
	}

	public void setReadOnly(boolean readOnly) {
		editor.setReadOnly(readOnly);
	}

	public void setContext(ModelElement context) {
		if (context instanceof UMLModelElement) {
			EObject currentEObj = ((UMLModelElement) context).getSource();
			editor.setContext(currentEObj);
		}
		
	}

}
