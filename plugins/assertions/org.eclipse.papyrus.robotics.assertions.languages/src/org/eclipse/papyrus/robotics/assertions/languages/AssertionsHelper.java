/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Assertion;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Contract;
import org.eclipse.papyrus.robotics.assertions.profile.assertions.Property;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AssertionsHelper {
	
	/**
	 * The assertion uses a global evaluation language
	 * 
	 * @param assertion
	 * @return boolean
	 */
	public static boolean isGlobalEvaluationAssertion(Assertion assertion) {
		String language = getAssertionLanguage(assertion);
		IExpressionLanguage langImpl = ExpressionLanguagesHelper.getExpressionLanguageByName(language);
		if (langImpl != null && langImpl.isGlobalEvaluation()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Get global evaluation language
	 * 
	 * @param assertion
	 * @return IExpressionLanguage
	 */
	public static IExpressionLanguage getGlobalEvaluationAssertion(Assertion assertion) {
		String language = getAssertionLanguage(assertion);
		IExpressionLanguage langImpl = ExpressionLanguagesHelper.getExpressionLanguageByName(language);
		if (langImpl != null && langImpl.isGlobalEvaluation()) {
			return langImpl;
		}
		return null;
	}
	
	/**
	 * Get assertion language
	 * 
	 * @param assertion
	 * @return language or null
	 */
	public static String getAssertionLanguage(Assertion assertion) {
		Constraint constraint = assertion.getBase_Constraint();
		if (!(constraint.getSpecification() instanceof OpaqueExpression)) {
			return null;
		}
		OpaqueExpression opaque = (OpaqueExpression) constraint.getSpecification();
		if (opaque.getLanguages() == null || opaque.getLanguages().isEmpty()) {
			return null;
		}
		String language = opaque.getLanguages().get(0);
		return language;
	}
	
	/**
	 * Get contract context
	 * 
	 * @param selectedContract
	 * @return owner
	 */
	public static EObject getContractContext(Contract contract) {
		Element owner = contract.getBase_Comment().getOwner();
		return owner;
	}

	/**
	 * Get comp definition contracts
	 * 
	 * @param compDefinition
	 * @return list of contracts
	 */
	public static List<Contract> getCompDefinitionContracts(ComponentOrSystem compDefinition) {
		List<Contract> contracts = new ArrayList<Contract>();
		Iterator<EObject> it = compDefinition.getBase_Class().eAllContents();
		while (it.hasNext()) {
			EObject eo = it.next();
			Contract contract = UMLUtil.getStereotypeApplication((Element) eo, Contract.class);
			if (contract != null) {
				contracts.add(contract);
			}
		}
		return contracts;
	}
	
	/**
	 * Get system contracts
	 * 
	 * @param system
	 * @return list of contracts
	 */
	public static List<Contract> getSystemContracts(org.eclipse.papyrus.robotics.profile.robotics.components.System system) {
		List<Contract> contracts = new ArrayList<Contract>();
		Iterator<EObject> it = system.getBase_Class().eAllContents();
		while (it.hasNext()) {
			EObject eo = it.next();
			Contract contract = UMLUtil.getStereotypeApplication((Element) eo, Contract.class);
			if (contract != null) {
				contracts.add(contract);
			}
		}
		return contracts;
	}
	
	/**
	 * Evaluate constraint
	 * @param userSelection
	 * @param constraint
	 * @return result
	 */
	public static Object evaluateConstraint(EObject userSelection, Constraint constraint) {
		// test if this is an opaqueExpression
		if (constraint.getSpecification() instanceof OpaqueExpression) {
			EObject context = constraint.getContext();
			// if it is a component instance change the context to the instance
			if(userSelection != null && userSelection instanceof ComponentInstance) {
				context = userSelection;
			}
			return evaluateOpaqueExpression(context, (OpaqueExpression) constraint.getSpecification());
		}
		// it was not an opaque expression
		return constraint.getSpecification().stringValue();
	}
	
	/**
	 * Evaluate value specification
	 * @param context
	 * @param valueSpecification
	 * @return result
	 */
	public static Object evaluateValueSpecification(EObject context, ValueSpecification valueSpecification) {
		if(valueSpecification instanceof OpaqueExpression) {
			return evaluateOpaqueExpression(context, (OpaqueExpression) valueSpecification);
		} else {
			return valueSpecification.stringValue();
		}
	}
	
	/**
	 * Evaluate opaque expression
	 * @param context
	 * @param opaqueExpression
	 * @return result
	 */
	public static Object evaluateOpaqueExpression(EObject context, OpaqueExpression opaqueExpression) {
		// TODO now only one is evaluated but an opaque expression might have several
		// bodies
		int indexOfLangBody = -1;
		String langName = null;
		for (int i = 0; i < opaqueExpression.getLanguages().size() && indexOfLangBody == -1; i++) {
			langName = opaqueExpression.getLanguages().get(i);
			if (ExpressionLanguagesHelper.getExpressionLanguageByName(langName) != null) {
				// language exists
				if(opaqueExpression.getBodies().size() > i) {
					// it has a defined body
					indexOfLangBody = i;
				}
			}
		}
		if (indexOfLangBody != -1) {
			IExpressionLanguage lang = ExpressionLanguagesHelper.getExpressionLanguageByName(langName);
			Object result = lang.evaluate(context, opaqueExpression.getBodies().get(indexOfLangBody));
			return result;
			// TODO no registered language, return string
		} else if (opaqueExpression.getBodies().size() > 0){
			return opaqueExpression.getBodies().get(0);
		}
		return null;
	}
	
	/**
	 * Get Properties
	 * 
	 * @param context
	 * @return properties
	 */
	public static Map<String, ValueSpecification> getProperties(EObject context) {
		Map<String, ValueSpecification> properties = new HashMap<String, ValueSpecification>();
		Iterator<EObject> it = context.eAllContents();
		while (it.hasNext()) {
			EObject eo = it.next();
			Property property = UMLUtil.getStereotypeApplication((Element) eo, Property.class);
			if (property != null) {
				String name = property.getBase_Property().getName();
				ValueSpecification vs = property.getBase_Property().getDefaultValue();
				properties.put(name, vs);
			}
		}
		return properties;
	}
	
}
