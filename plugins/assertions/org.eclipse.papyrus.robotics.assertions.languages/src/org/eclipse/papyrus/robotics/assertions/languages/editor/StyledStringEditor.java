/*****************************************************************************
 * Copyright (c) 2010 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Camille Letavernier (CEA LIST) camille.letavernier@cea.fr - Initial API and implementation
 *  Thibault Le Ouay t.leouay@sherpa-eng.com - Add binding implementation
 *****************************************************************************/

package org.eclipse.papyrus.robotics.assertions.languages.editor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.papyrus.infra.widgets.databinding.StyledTextObservableValue;
import org.eclipse.papyrus.infra.widgets.editors.AbstractValueEditor;
import org.eclipse.papyrus.infra.widgets.selectors.StringSelector;
import org.eclipse.papyrus.robotics.assertions.languages.AssertionsHelper;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentDefinition;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentInstance;
import org.eclipse.papyrus.robotics.profile.robotics.components.ComponentOrSystem;
import org.eclipse.papyrus.robotics.profile.robotics.parameters.ParameterEntry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * A Property Editor representing a single-line or multi-line String value as a
 * Text. This editor's content is validated when the focus is lost, or, if the
 * editor is single-line, when the Carriage Return is pressed. For a multi-line
 * editor, ctrl+enter will also validate the editor's content.
 *
 * @see SWT#MULTI
 *
 * @author Camille Letavernier
 * @author Jabier Martinez, modified
 */
public class StyledStringEditor extends AbstractValueEditor implements KeyListener, ModifyListener {

	/**
	 * The text box for editing this editor's value
	 */
	protected final StyledText styledText;

	private int delay = 600;

	private boolean validateOnDelay = false;

	private Timer timer;

	private TimerTask currentValidateTask;

	private TimerTask changeColorTask;

	private final static int DEFAULT_HEIGHT_HINT = 55;

	private final static int DEFAULT_WIDTH_HINT = 100;

	private String[] keywords;
	private List<String> modelKeywords;

	private Color keywordsColor = new Color(Display.getCurrent(), 102, 0, 102);
	private Color modelKeywordsColor = new Color(Display.getCurrent(), 120, 150, 0);

	/**
	 *
	 * Constructor.
	 *
	 * @param parent The composite in which this editor should be displayed
	 * @param style  The style for this editor's text box
	 */
	public StyledStringEditor(Composite parent, int style) {
		this(parent, style, null, DEFAULT_HEIGHT_HINT, DEFAULT_WIDTH_HINT);

	}

	/**
	 *
	 * Constructor.
	 *
	 * @param parent The composite in which this editor should be displayed
	 * @param style  The style for this editor's text box
	 * @param label  The label for this editor
	 */
	public StyledStringEditor(Composite parent, int style, String label) {
		this(parent, style, label, DEFAULT_HEIGHT_HINT, DEFAULT_WIDTH_HINT);
	}

	/**
	 *
	 * Constructor.
	 *
	 * @param parent    The composite in which this editor should be displayed
	 * @param style     The style for this editor's text box
	 * @param heighHint Height hint of the text area in multiline mode
	 * @param widthHint Width hint of the text area in multiline mode
	 */
	public StyledStringEditor(Composite parent, int style, int heighHint, int widthHint) {
		this(parent, style, null, heighHint, widthHint);
	}

	/**
	 *
	 * Constructor.
	 *
	 * @param parent    The composite in which this editor should be displayed
	 * @param style     The style for this editor's text box
	 * @param label     The label for this editor
	 * @param heighHint Height hint of the text area in multiline mode
	 * @param widthHint Width hint of the text area in multiline mode
	 */
	public StyledStringEditor(Composite parent, int style, String label, int heighHint, int widthHint) {
		super(parent, label);

		GridData data = getDefaultLayoutData();
		data.grabExcessVerticalSpace = true;
		data.grabExcessHorizontalSpace = true;
		data.verticalAlignment = SWT.FILL;

		if ((style & SWT.MULTI) != 0) {
			data.minimumHeight = heighHint;
			data.minimumWidth = widthHint;
			style = style | SWT.V_SCROLL;
		}

		// Modified from StringEditor as there is no factory.createStyledText
		// Text text = factory.createText(this, null, style);
		styledText = new StyledText(this, SWT.BORDER | style | Window.getDefaultOrientation());

		styledText.setLayoutData(data);

		if (label != null) {
			super.label.setLayoutData(getLabelLayoutData());

		}
		styledText.addKeyListener(this);
		styledText.addModifyListener(this);
		setCommitOnFocusLost(styledText);
		controlDecoration = new ControlDecoration(styledText, SWT.LEFT | SWT.TOP);
		controlDecoration.hide();
		data.horizontalIndent = FieldDecorationRegistry.getDefault().getMaximumDecorationWidth();
		pack();

	}

	@Override
	protected GridData getLabelLayoutData() {
		GridData result = super.getLabelLayoutData();
		if (styledText != null) {
			if ((styledText.getStyle() & SWT.MULTI) != 0) {
				result.verticalAlignment = SWT.BEGINNING;
			}
		}
		return result;
	}

	/**
	 * Validates this editor when one of the following events occur : - CR released
	 * - Keypad CR released - Ctrl + [CR | Keypad CR] released
	 *
	 * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
	 *
	 * @param e
	 */
	// TODO : we should prevent the \n from being written when validating the
	// multi-line field with Ctrl + CR
	@Override
	public void keyReleased(KeyEvent e) {
		// We listen on Carriage Return or Ctrl+ Carriage return, depending on
		// whether the editor is single- or multi-line
		if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
			if ((styledText.getStyle() & SWT.MULTI) == 0) { // Single-line : Enter
				if (e.stateMask == SWT.NONE) {
					notifyChange();
				}
			} else { // Multi-line : Ctrl+Enter
				if (e.stateMask == SWT.CTRL) {
					String str = styledText.getText();
					if (str.endsWith(StringSelector.LINE_SEPARATOR)) {
						int newLength = str.length() - StringSelector.LINE_SEPARATOR.length();
						styledText.setText(str.substring(0, newLength));
						styledText.setSelection(newLength);
					}
					notifyChange();
				}
			}
		}

		updateSyntaxHighlighting();
	}

	@Override
	public void setModelObservable(IObservableValue observable) {
		setWidgetObservable(new StyledTextObservableValue(styledText, observable, SWT.FocusOut), true);
		super.setModelObservable(observable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getEditableType() {
		return String.class;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getValue() {
		return styledText.getText();
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		styledText.setEnabled(!readOnly);
	}

	@Override
	public boolean isReadOnly() {
		return !styledText.isEnabled();
	}

	protected void notifyChange() {
		styledText.notifyListeners(SWT.FocusOut, new Event());
		commit();
		changeColorField();
	}

	@Override
	public void setToolTipText(String tooltip) {
		styledText.setToolTipText(tooltip);
		super.setLabelToolTipText(tooltip);
	}

	/**
	 * Sets the current text value for this editor
	 *
	 * @param value
	 */
	public void setValue(Object value) {
		if (value instanceof String) {
			this.styledText.setText((String) value);
		} else {
			this.styledText.setText(""); //$NON-NLS-1$ ;
		}
		updateSyntaxHighlighting();
	}

	/**
	 * Indicates that this editor should be automatically validated after a timer.
	 *
	 * @param validateOnDelay
	 */
	public void setValidateOnDelay(boolean validateOnDelay) {
		this.validateOnDelay = validateOnDelay;

		if (validateOnDelay) {
			styledText.addModifyListener(this);
		} else {
			styledText.removeModifyListener(this);
			cancelCurrentTask();
		}
	}

	/**
	 * Indicates that this editor should be automatically validated after the given
	 * timer
	 *
	 * @param millis The delay after which the editor should be automatically
	 *               validated, in milliseconds. The default is 600ms
	 */
	public void setValidateOnDelay(int millis) {
		this.delay = millis;
		setValidateOnDelay(true);
		if (delay == 0) {
			cancelCurrentTask();
		}
	}

	private void cancelCurrentTask() {
		if (currentValidateTask != null) {
			currentValidateTask.cancel();
			currentValidateTask = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void modifyText(ModifyEvent e) {

		// SWT Thread
		if (validateOnDelay) {
			if (delay == 0) {
				commit(); // Direct commit on edition, to avoid creating useless
				// threads

				return;
			}

			if (timer == null) {
				timer = new Timer(true);
			}

			cancelCurrentTask();
			currentValidateTask = new TimerTask() {

				// Timer thread
				@Override
				public void run() {
					StyledStringEditor.this.getDisplay().syncExec(new Runnable() {

						// SWT Thread
						@Override
						public void run() {

							commit();
						}
					});
				}
			};
			timer.schedule(currentValidateTask, delay);
		}
		if (targetValidator != null) {
			IStatus status = targetValidator.validate(styledText.getText());
			updateStatus(status);
		}
		if (modelValidator != null) {
			IStatus status = modelValidator.validate(styledText.getText());
			updateStatus(status);
			if (binding == null) {
				update();
			}
		}

		if (modelProperty != null) { // Bug 433169: The widget may be used without an Observable Value (setValue +
										// getValue)
			if (modelProperty.getValue() != null) {
				if (!isReadOnly() && !modelProperty.getValue().toString().equals(styledText.getText())) {
					styledText.setBackground(EDIT);
				} else {
					styledText.setBackground(DEFAULT);
				}
			} else {
				if (styledText.getText().equals("")) {
					styledText.setBackground(DEFAULT);
				} else {
					styledText.setBackground(EDIT);
				}
			}
		}
	}

	@Override
	public void dispose() {
		cancelCurrentTask();
		cancelChangeColorTask();
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		super.dispose();
	}

	public StyledText getStyledText() {
		return styledText;
	}

	@Override
	public void updateStatus(IStatus status) {
		switch (status.getSeverity()) {
		case IStatus.OK:
			controlDecoration.hide();
			break;
		case IStatus.WARNING:
			FieldDecoration warning = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_WARNING);
			controlDecoration.setImage(warning.getImage());
			controlDecoration.showHoverText(status.getMessage());
			controlDecoration.setDescriptionText(status.getMessage());
			controlDecoration.show();
			break;
		case IStatus.ERROR:
			FieldDecoration error = FieldDecorationRegistry.getDefault()
					.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR);
			controlDecoration.setImage(error.getImage());
			controlDecoration.showHoverText(status.getMessage());
			controlDecoration.setDescriptionText(status.getMessage());
			controlDecoration.show();
			break;
		default:
			controlDecoration.hide();
			break;
		}

	}

	@Override
	public void changeColorField() {
		if (binding != null) {

			if (timer == null) {
				timer = new Timer(true);
			}

			cancelChangeColorTask();
			changeColorTask = new TimerTask() {

				@Override
				public void run() {
					if (StyledStringEditor.this.isDisposed()) {
						return;
					}
					StyledStringEditor.this.getDisplay().syncExec(new Runnable() {

						@Override
						public void run() {
							styledText.setBackground(DEFAULT);
							styledText.update();
						}
					});
				}
			};
			if (errorBinding) {
				styledText.setBackground(ERROR);
				styledText.update();
			} else {
				IStatus status = (IStatus) binding.getValidationStatus().getValue();
				switch (status.getSeverity()) {
				case IStatus.OK:
				case IStatus.WARNING:
					timer.schedule(changeColorTask, 600);
					styledText.setBackground(VALID);
					styledText.update();
					break;
				case IStatus.ERROR:
					styledText.setBackground(ERROR);
					styledText.update();
					break;

				}
			}
		}
	}

	private void cancelChangeColorTask() {
		if (changeColorTask != null) {
			changeColorTask.cancel();
			changeColorTask = null;
		}
	}

	public void setKeywords(String[] keywords) {
		this.keywords = keywords;
	}

	public String[] getKeywords() {
		return keywords;
	}

	/**
	 * Update Syntax Highlighting
	 * 
	 * @param styledText
	 */
	public void updateSyntaxHighlighting() {
		String s = styledText.getText();
		for (String keyword : keywords) {
			applyHighlight(styledText, s, keyword, keywordsColor, SWT.BOLD);
		}
		for (String keyword : modelKeywords) {
			applyHighlight(styledText, s, keyword, modelKeywordsColor, SWT.BOLD);
		}
		applyHighlight(styledText, s, "GlobalSum", keywordsColor, SWT.BOLD);
	}

	public void applyHighlight(StyledText styledText, String expression, String keyword, Color keywordsColor, int fontStyle) {
		int i = expression.indexOf(keyword);
		while (i != -1) {
			boolean valid = false;
			// special case
			if(keyword.equals("GlobalSum")) {
				valid = true;
			}
			// it is the first one or the whole text is just one keyword
			if (i == 0) {
				if (expression.startsWith(keyword + " ") || expression.equals(keyword)) {
					valid = true;
				}
			}
			// it is the last one
			else if (i + keyword.length() == expression.length()) {
				if (expression.charAt(i - 1) == ' ') {
					valid = true;
				}
				// in the middle
			} else {
				if ((expression.charAt(i - 1) == ' ' || expression.charAt(i - 1) == '(')
						&& (expression.charAt(i + keyword.length()) == ' ' || expression.charAt(i + keyword.length()) == ')')) {
					valid = true;
				}
			}
			if (valid) {
				StyleRange styleRange = new StyleRange();
				styleRange.start = i;
				styleRange.length = keyword.length();
				styleRange.fontStyle = fontStyle;
				styleRange.foreground = keywordsColor;
				styledText.setStyleRange(styleRange);
			}
			i = expression.indexOf(keyword, i + keyword.length());
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// auto-completion CTRL + SPACE
		if ((e.stateMask & SWT.CTRL) != 0 && e.keyCode == SWT.SPACE) {

			// add keywords
			List<Object> elementsList = new ArrayList<Object>();
			for (String keyword : getKeywords()) {
				elementsList.add(keyword);
			}
			// add model keywords from the context
			elementsList.addAll(modelKeywords);

			ElementListSelectionDialog dialog = new ElementListSelectionDialog(Display.getCurrent().getActiveShell(),
					new LabelProvider() {
						public String getText(Object element) {
							if (element == null) {
								return "";
							}
							return element.toString();
						}

						public Image getImage(Object element) {
							if (modelKeywords.contains(element)) {
								return PlatformUI.getWorkbench().getSharedImages()
										.getImage(ISharedImages.IMG_OBJ_ELEMENT);
							}
							return null;
						}
					});

			dialog.setElements(elementsList.toArray());

			dialog.setTitle("Auto-completion");
			dialog.setMultipleSelection(false);
			dialog.setHelpAvailable(false);
			dialog.setBlockOnOpen(true);
			if (dialog.open() == Window.OK) {
				// insert text
				Object[] result = dialog.getResult();
				int selIndex = styledText.getSelection().x;
				String text = styledText.getText();
				String before = text.substring(0, selIndex);
				String after = text.substring(selIndex);
				styledText.setText(before + result[0] + after);
				updateSyntaxHighlighting();
			}
		}
	}

	public void setContext(EObject currentEObj) {
		modelKeywords = new ArrayList<String>();
		// Update modelKeywords
		EObject parent = currentEObj;
		while (parent.eContainer() != null) {
			parent = parent.eContainer();
			// component definition
			if (parent instanceof Element
					&& UMLUtil.getStereotypeApplication((Element) parent, ComponentDefinition.class) != null) {
				ComponentDefinition compDefinition = UMLUtil.getStereotypeApplication((Element) parent,
						ComponentDefinition.class);
				// add port names
				modelKeywords.addAll(getPortNames(compDefinition));
				// add property names
				Map<String, ValueSpecification> properties = AssertionsHelper
						.getProperties(compDefinition.getBase_Class());
				modelKeywords.addAll(properties.keySet());
				// add parameter names
				modelKeywords.addAll(getParameterNames(compDefinition.getBase_Class()));
			// System
			} else if (parent instanceof Element
					&& UMLUtil.getStereotypeApplication((Element) parent, org.eclipse.papyrus.robotics.profile.robotics.components.System.class) != null) {
				org.eclipse.papyrus.robotics.profile.robotics.components.System system = UMLUtil.getStereotypeApplication((Element) parent, org.eclipse.papyrus.robotics.profile.robotics.components.System.class);
				// properties
				Map<String, ValueSpecification> properties = AssertionsHelper.getProperties(system.getBase_Class());
				modelKeywords.addAll(properties.keySet());
				// componentInstance names
				for(ComponentInstance instance : system.getInstances()) {
					String instanceName = instance.getBase_Property().getName();
					modelKeywords.add(instanceName);
					// component parameters
					ComponentOrSystem compDef = instance.getCompdefOrSys();
					for(String param : getParameterNames(compDef.getBase_Class())) {
						modelKeywords.add(instanceName + "." + param);
					}
					// component properties
					Map<String, ValueSpecification> compProperties = AssertionsHelper.getProperties(compDef.getBase_Class());
					for(String prop : compProperties.keySet()) {
						modelKeywords.add(instanceName + "." + prop);
					}
				}
			}
		}
	}

	public List<String> getPortNames(ComponentDefinition compDef) {
		List<String> portNames = new ArrayList<String>();
		for (org.eclipse.uml2.uml.Port port : compDef.getBase_Class().getOwnedPorts()) {
			portNames.add(port.getName());
		}
		return portNames;
	}

	public List<String> getParameterNames(EObject context) {
		List<String> names = new ArrayList<String>();
		Iterator<EObject> i = context.eAllContents();
		while (i.hasNext()) {
			EObject eo = i.next();
			ParameterEntry parameter = UMLUtil.getStereotypeApplication((Element) eo, ParameterEntry.class);
			// context was a componentDefinition
			if (parameter != null) {
				String name = parameter.getBase_Property().getName();
				names.add(name);
			}
		}
		return names;
	}

}
