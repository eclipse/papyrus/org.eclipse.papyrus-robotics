/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *   Angel L�pez, Tecnalia - Implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages.ocl;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.pivot.ExpressionInOCL;
import org.eclipse.ocl.pivot.internal.utilities.PivotUtilInternal;
import org.eclipse.ocl.pivot.uml.UMLOCL;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.ocl.pivot.utilities.MetamodelManager;
import org.eclipse.ocl.pivot.utilities.OCL;
import org.eclipse.ocl.pivot.utilities.OCLHelper;
import org.eclipse.papyrus.robotics.assertions.languages.IExpressionLanguage;

@SuppressWarnings("deprecation")
public class OCLExpressionLanguage implements IExpressionLanguage {

	@Override
	public String getName() {
		return "OCL"; //$NON-NLS-1$
	}
	
	@Override
	public Object evaluate(EObject context, String expression) {
		MetamodelManager metamodelManager = getMetamodelManager(context);
		EnvironmentFactory environmentFactory = metamodelManager.getEnvironmentFactory();
		OCL ocl = OCL.newInstance(environmentFactory);
		org.eclipse.ocl.pivot.Class contextType = ocl.getContextType(context);
		try {
			OCLHelper oclHelper = ocl.createOCLHelper(contextType);
			ExpressionInOCL createQuery = oclHelper.createQuery(expression);
			Object evaluate = ocl.evaluate(context, createQuery);
			return evaluate;
		} catch (Exception ex) {
			return null;
		}
	}
	
	/**
	 * this method comes from the org.eclipse.ocl.examples.xtext.console.OCLConsolePage
	 *
	 * @param contextObject
	 * @return the metamodelManager
	 */
	protected static MetamodelManager getMetamodelManager(EObject contextObject) {
		EnvironmentFactory environmentFactory = PivotUtilInternal.findEnvironmentFactory(contextObject);
		if (environmentFactory != null) {
			return environmentFactory.getMetamodelManager();
		}
		return UMLOCL.newInstance().getMetamodelManager();
	}

	@Override
	public boolean isGlobalEvaluation() {
		return false;
	}
	
}
