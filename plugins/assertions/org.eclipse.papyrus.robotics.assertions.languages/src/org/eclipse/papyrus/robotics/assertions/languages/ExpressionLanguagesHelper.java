/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *   Angel L�pez, Tecnalia - Implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

public class ExpressionLanguagesHelper {
	
	public static final String EXPRESSIONLANGUAGES_EXTENSIONPOINT = "org.eclipse.papyrus.robotics.assertions.languages"; //$NON-NLS-1$

	/**
	 * Get expression languages
	 * @return list of expression languages
	 */
	public static List<IExpressionLanguage> getAllExpressionLanguages() {
		List<IExpressionLanguage> result = new ArrayList<IExpressionLanguage>();
		IConfigurationElement[] extensionPoints = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(EXPRESSIONLANGUAGES_EXTENSIONPOINT);
		for (IConfigurationElement extensionPoint : extensionPoints) {
			try {
				result.add((IExpressionLanguage) extensionPoint.createExecutableExtension("class")); //$NON-NLS-1$
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**
	 * Get expression language by name
	 * @param name
	 * @return expression language or null if not found
	 */
	public static IExpressionLanguage getExpressionLanguageByName(String name) {
		for(IExpressionLanguage lang : getAllExpressionLanguages()) {
			if(lang.getName().equals(name)) {
				return lang;
			}
		}
		return null;
	}
}
