/*****************************************************************************
 * Copyright (c) 2020 TECNALIA.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 *
 * Contributors:
 * 	 Jabier Martinez, Tecnalia - Initial design and implementation
 *   Angel L�pez, Tecnalia - Implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.robotics.assertions.languages;

import org.eclipse.emf.ecore.EObject;

/**
 * Expression language interface to be contributed through an extension point.
 * This will be used to calculate values or to evaluate assertions using the
 * assertions profile
 */
public interface IExpressionLanguage {

	/**
	 * Get name
	 * 
	 * @return the name/id of the language. It should be the same that it is
	 *         declared in the extension point
	 */
	public String getName();

	/**
	 * Given a context (e.g., self in OCL) and an expression in this language it
	 * returns its evaluation. The expression can be null if the language requires a
	 * global evaluation instead of the evolution of independent expressions.
	 * 
	 * @param context
	 * @param expression (can be null if isGlobalEvaluation() is true)
	 * @return Object
	 */
	public Object evaluate(EObject context, String expression);

	/**
	 * Whether the language needs to be globally evaluated instead of independent
	 * expressions. e.g., OCL expressions are evaluated independently but other
	 * languages might need to look at all the expressions in this language evaluate
	 * to evaluate them (e.g., inconsistencies to be found among assertions)
	 * 
	 * @return boolean
	 */
	public boolean isGlobalEvaluation();

}
