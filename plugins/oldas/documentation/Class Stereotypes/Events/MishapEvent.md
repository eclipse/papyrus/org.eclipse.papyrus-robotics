# MishapEvent
## Definition
A mishap event is the undesired event that cause injuries to stakeholder.
## Constraints
### From «AbstractEvent»:
- An event cannot supertype or subtype an endurant class.
### Additional:
- A «MishapEvent» class must have a direct or indrect incoming «Causes» relation connected to «MalfunctionEvent».
- A «MishapEvent» class must have a direct or indrect incoming «Causes» relation connected to «TriggeringEvent».
## Examples
See AbstractEvent page.
