# AbstractEvent
## Definition
The events represent the dynamic aspect of the scenario representation. The abstract stereotype `AbstractEvent` is sub-typed as  «TriggeringEvent»,  «MalfunctionEvent», «MishapEvent», or  «NominalEvent».
## Constraints
- An event cannot supertype or subtype an endurant class.
## Examples
The diagram below shows an OLDAS model with causal chains related to computer vision-based systems. This example was built using [CV-HAZOP](https://vitro-testing.com/cv-hazop/). This diagram focuses on events. The class «World» and the subtypes of `LightSource`, `Particles`, and `Obstacle` are not represented (their specification in a full OLDAS model is mandatory).<br><br>
![Event Figure 1](Event_Figure1.png)<br><br>
The «NominalEvent» stereotype can also be used to represent other risk reduction events. An example is in the figure below.
![Event Figure 2](Event_Figure2.png)
