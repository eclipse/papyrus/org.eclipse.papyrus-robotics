# ActionEvent
## Definition
A action event is an event created by an agent.
## Constraints
### From  «AbstractEvent`:
- An event cannot supertype or subtype an endurant class
### Additional:
- An «ActionEvent» must have an incoming «Creates» relation with an «Agent».
## Example
See Agent Figure.
