# TriggeringEvent
## Definition
The triggering event is the first event of the hazard causal chain in specific operating conditions.
## Constraints
### From «AbstractEvent»:
- An event cannot supertype or subtype an endurant class
### Additional:
- A «TriggeringEvent» must directly or indirectly «Causes» at least one «MalfunctionEvent».
- A «TriggeringEvent» must directly or indirectly «Causes» at least one «MishapEvent».
- A «TriggeringEvent» must be connected to at least one «OperatingFeature» though a «Participates» relation.
## Example
See AbstractEvent page.
