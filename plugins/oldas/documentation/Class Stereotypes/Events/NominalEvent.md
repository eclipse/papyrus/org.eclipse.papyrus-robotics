# NominalEvent
## Definition
A nominal event is an event that do not intervene in a risk or that represent the possible return to normal condition.
## Constraints
### From  «AbstractEvent»:
- An event cannot supertype or subtype an endurant class
### Additional:
- A  «NominalEvent» class cannot «Causes» a  «TriggeringEvent», «MalfunctionEvent», or «MishapEvent».
## Example
The diagram below represents nominal events used to represent the possible return to normal condition during the threat scenario. These events can be implicit in the model. The class «World» and the subtypes of `LightSource`, `Particles`, and `Obstacle` are not represented (their specification in a full OLDAS model is mandatory).<br><br>
![NominalEvent Figure 1](NominalEvent_Figure1.png)<br><br>
The «NominalEvent» stereotype can also be used to represent other risk reduction events. An example is in the figure below.<br><br>
![NominalEvent Figure 2](NominalEvent_Figure2.png)
