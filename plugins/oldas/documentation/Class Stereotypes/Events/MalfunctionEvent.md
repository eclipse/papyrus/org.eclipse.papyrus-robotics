# MalfunctionEvent
## Definition
The malfunction event describes termination from a hardware or software component to provide its intended service
## Constraints
### From «AbstractEvent»:
- An event cannot supertype or subtype an endurant class
### Additional:
- A «MalfunctionEvent» must directly or indirectly «Causes» at least one «MishapEvent».
- A «MalfunctionEvent» must be connected to at least one «AutomationFeature» though a «Participates» relation.
- A «MalfunctionEvent» cannot direclty or indrectly «Causes» a «TriggeringEvent»
## Example
See AbstractEvent page.
