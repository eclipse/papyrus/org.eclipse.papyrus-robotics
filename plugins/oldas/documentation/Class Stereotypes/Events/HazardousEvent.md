# HazadousEvent
## Definition
The `HazardousEvent` class represents the intermediate event caused by a malfunction and that can potentially cause itself a mishap.
## Constraints
### From `AbstractEvent`:
- An event cannot supertype or subtype an endurant class.
### Additional:
- A `HazardousEvent` class must have at least one incoming direct or indirect `Causes` relation from `MalfunctionEvent`.
- A `HazardousEvent` class must have at least one incoming direct or indirect `Causes` relation from `MishapEvent`.
# Example
TODO
