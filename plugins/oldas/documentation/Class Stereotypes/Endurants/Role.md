# Role
## Definition
A role is a sub-type of a class «OperatingFeature», «AutomationFeature», , «Property», «Relator», «Agent», «Metric» that is instanciated by changes in relational contexts.
## Constraints
- A role must have exactly one «OperatingFeature», «AutomationFeature», «Property», «Relator», «Agent», «Metric» as its direct or indirect super-type.
- A role cannot have a class «OperatingFeature», «AutomationFeature», «Property», «Agent», «Metric», «Relator», «Category», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect sub-type.
- A role cannot be a direct sub-type of a «Category».
- A role cannot have a class «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect super-type.
- A role must be connected, directly or indirectly, to a «Relator» class through a «Mediates» relation.
- A role cannot be abstract.
## Example
The diagram below shows an OLDAS model with the agent `AutomatedDrivingSystem` that have the roles`AutomatedDriver` and `AutomatedDrivingAssisstant`, and the agent `HumanOperator` that have the roles `HumanSupervisor` and `HumanDriver`. Events are not represented on this diagram. <br><br> <br><br>
![Role Figure 1](Role_Figure1.png)
