# Property
## Definition
A property is a concept that existentially depend on an other concept it characterizes. It can have a structured value (e.g., speed, height, distance), or not (e.g., moving, large, wet).
## Constraints
- Every properties must be (directly or indirectly) connected to an association end of at least one «Characterizes» relation.
- The multiplicity of the characterized end must be exactly one.
- A property cannot have a class «OperatingFeature», «AutomationFeature», «State», «Role», «Relator», «Metric», «Agent», «TriggeringEvent», «MalfunctionEvent», «Mishap», or «NominalEvent» as its direct or indirect super-type;
- A property cannot have a class «Category», «OperatingFeature», «AutomationFeature», «Relator», «Metric», «Agent», «TriggeringEvent», «MalfunctionEvent», «Mishap», or «NominalEvent» as its direct or indirect sub-type.
## Examples
The diagram below shows an OLDAS model with two properties that characterizes the class OtherVehicle stereotyped «OperatingFeature»: a property `Speed` with a structured value, and a property `HighlyAutomated` without structured value. The property  `SunElevation` that characterizes the class `Day` stereotyped «State». Events are not represented on this diagram. <br><br>
![Property Figure 1](Property_Figure1.png)
