# OperatingFeature
## Definition
The operating feature are any element on which one measure the relevant parameters that describe the system's usage scenarios, including environmental conditions (weather, connectivity, luminosity, particulates), dynamic elements (subject system, other agents), and scenery (zones, static obstacles, special structures, routes).
The stereotype «OperatingFeature» is generalized by the stereotype «WorldFeature».
## Constraints
### From «WorldFeature»:
- An «OperatingFeature» class cannot have a class «Property», «Relator», «Agent», «State», «Role», «Metric», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect super-type.
- An «OperatingFeature» class cannot have a class «Category», «Property», «Relator», «Metric», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect sub-type.
- All «OperatingFeature» classes must have a direct or indirect composition relation to  the class «World».
### Additional:
- An «OperatingFeature» shall be connected to at least one «TriggeringEvent» through a «Participates» relation.
## Examples
See WorldFeature page.
