# WorldFeature
## Definition
A world attribute is a concepts which does not change of type during a scenario and which allow to distinguish individuals. This can represent operating conditions (e.g, wind, buildings, GPS signal), or automation features (e.g, camera, motion planning, object detection). The stereotype «WorldFeature» is abstract and it generalizes the stereotypes «OperatingFeature» and «AutomationFeature».
## Constraints
- A world feature cannot have a class «Property», «Relator», «Agent», «State», «Role», «Metric», «TriggeringEvent», «MalfunctionEvent», or «MishapEvent» as its direct or indirect super-type.
- A world feature cannot have a class «Category», «Property», «Relator», «Metric», or «TriggeringEvent», «MalfunctionEvent», or «MishapEvent» as its direct or indirect sub-type.
- All «WorldFeature» classes must have a direct or indirect composition relation to  the class «World».
## Examples
The diagram below shows an OLDAS model with a causal chain related to computer vision-based systems. The operating features categorized `LightSource` participate in the triggering event `TooManyLightSource`. This event can potentially cause an `OverexposedCamera` malfunction (involving the participation of the `CameraSensor` automation feature). This malfunction can cause a mishap event `ObstacleCollision` (involving the participation of any `Obstacle` operating feature). This example was built using [CV-HAZOP](https://vitro-testing.com/cv-hazop/).<br><br>
![CV Figure 1](CV_Figure1.png)
