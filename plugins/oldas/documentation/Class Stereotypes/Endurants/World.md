# World
## Definition
The `World` represents the ensemble of concepts needed to describe any possible scenarios for a given domain. It ensures the specification of all other object number of occurence (e.g., zero or one instance of *Rainfall* can be present in a scenario, zero to infinit number of *Obstacles* can be present in a scenario)
## Constraints
- The presence of exactly one class `World` is mandatory;
- All classes typed `WorldAttribute` must have a direct or indirect composition relation to the object typed `World`;
- The class typed `World` cannot be subtyped or supertyped.
- The class typed `World` cannot have ingoing composition associations instances.
## Example
In the model below, the number of `Pedestrian` in a scenario must be zero or higher whereas the number of `Rainfall` individual must be zero or one. Events are not represented on this diagram. <br><br>
![World Figure 1](World_figure1.png)
