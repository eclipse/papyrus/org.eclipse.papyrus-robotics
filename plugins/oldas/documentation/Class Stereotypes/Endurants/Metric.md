# Metric
## Definition
A metric structures a measurable property that have a value (e.g., Kilograms, Meter, Second).
## Constraint
- Every metrics must be (directly or indirectly) connected to an association end of at least one Structures relation;
- A metric cannot have a class «OperatingFeature», «AutomationFeature», «State», «Role», «Relator», «Property», «Agent»,  «TriggeringEvent», «MalfunctionEvent», «Mishap», or «NominalEvent» as its direct or indirect super-type;
- A metric cannot have a «Category», «OperatingFeature», «AutomationFeature», «Relator», «Property», «Agent»,  «TriggeringEvent», «MalfunctionEvent», «Mishap», or «NominalEvent» as its direct or indirect sub-type.
## Example
The diagram below shows an OLDAS model with the metric `Kilometer` that structures the property `Speed`. Events are not represented on this diagram. <br><br>
![Metric Figure 1](Metric_Figure1.png)
