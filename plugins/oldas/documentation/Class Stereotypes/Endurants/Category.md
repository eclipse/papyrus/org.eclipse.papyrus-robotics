# Category
## Definition
Any non-Event classes can sub-type a «Category» to allow the refactoring process.
## Constraints
- A category can not have a non-Category as its direct or indirect super-type.
- A category is always abstract.
## Example
A class stereotyped as «Category» can aggregate individuals that follow different identity principles. The diagram example shown below represents a category subtyped by a «Property» class and an «OperatingFeature» class. Events are not represented on this diagram.<br><br>
![Category Figure 1](Category_Figure1.png)<br><br>
The «Category» stereotype can also be usefull to aggregate different entities with similar roles. A diagram example is shown below (figure from Role page).<br><br>
![Category Figure 2](Role_Figure1.png)
