# Abstract
## Definition
The abstraction representation address the requirement of the multi-level hierarchy expressiveness for any endurant or event classes. For the endurant classes, in contrary to the use of the «State» or «Role» stereotypes, an individual typed by a sub-types of an abstract class cannot change of this type during a scenario. This class aims to replace the usage of UFO-A:SubKind.
## Constraint
### Adapted from UFO-A:Subkind:
- An abstract class stereotyped «OperatingFeature», «AutomationFeature», «Agent», «Relator», «Property», «Metric» must have at least one direct or indirect non-abstract subtype with the same stereotype
- The «State» and «Role» classes (anti-rigid types) cannot be abstract.

## Transformation into UFO models
The identity provider class (a class stereotyped «OperatingFeature», «AutomationFeature», «Agent», «Relator», «Property», or «Metric» without an ancestor with the same type) shall be converted into a non-abstract class and all its direct and indirect subtypes shall be converted into «UFO-A::Subkind» classes. The diagram below shows on the left an incomplete OLDAS model and its equivalent in UFO on the right.<br><br>
![Abstract OLDAS to UFO Figure 1](OLDAS2UFO_Abstract.png)

## Examples
The diagram below shows an OLDAS model built from the taxonomy PAS1883:2020 5.2.4. Events are not represented on this diagram. <br><br>
![Abstract Figure 1](Abstract_Figure1.png)
