# Agent
## Definition
An agent is a specific kind with a goal and that can creates actions to accomplish it (e.g., pedestrian, automated driving system, traffic controller). 
## Constraints
- An «Agent» class cannot have a class «Property», «Relator», «Agent», «State», «Role», «Metric», «TriggeringEvent», «MalfunctionEvent», «Mishap», or «NominalEvent» as its direct or indirect super-type.
- An «Agent» class cannot have a class «Category», «Property», «Relator», «Metric», «TriggeringEvent», «MalfunctionEvent», «Mishap», or «NominalEvent» as its direct or indirect sub-type.
- All «Agent» classes must have a direct or indirect composition relation to  the class «World`.
## Examples
The diagram below shows an OLDAS model with a class `HumanOperatorAgency` stereotyped «Agent» with two actions `Brake` and `Accelerate`. `Brake` is subtyped  by the triggering event `TooStrongBrake`. «MalfunctionEvent» and «MishapEvent» are not represented on this diagram. An agent in the model can be defined as a component of an operating feature (like in the figure below), or as a component of an automation feature (e.g., the «Agent» class `AutomatedDrivingAgent` as a component of the «AutomationFeature» class `AutomatedDrivingSystem`).<br><br>
![Agent Figure 1](Agent_Figure1.png)
