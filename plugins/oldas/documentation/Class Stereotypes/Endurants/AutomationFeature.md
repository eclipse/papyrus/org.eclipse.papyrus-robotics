# AutomationFeature
## Definition
The Automation Features are the existing Hardware and Software (\acrshort{hwsw}) solutions that implement the navigation pipeline (sensing, perception, planning, and control) for any system in the domain.
## Constraints
### From «WorldFeature»:
- An «AutomationFeature» class cannot have a class «Property», «Relator», «Agent», «State», «Role», «Metric», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect super-type.
- An «AutomationFeature» class cannot have a class «Category», «Property», «Relator», «Metric», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect sub-type.
- All «AutomationFeature» classes must have a direct or indirect composition relation to  the class «World».
### Additional:
- An «AutomationFeature» shall be connected to at least one «MalfunctionEvent» through a «Participates» relation.
## Examples
See WorldFeature page.
