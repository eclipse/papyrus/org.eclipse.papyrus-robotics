# State
## Definition
A state is a sub-type of a class «OperatingFeature», «AutomationFeature», , «Property», «Relator», «Agent», «Metric» that is instanciated by changes in its explicitly or implicitly defined intrinsic properties (e.g., rainfall/snowfall, light/moderate/heavy, on/off).
## Constraints
- A state must have exactly one «OperatingFeature», «AutomationFeature», «Property», «Relator», «Agent», «Metric» as its direct or indirect super-type.
- A state must be a part of a generalization set disjoint and complete with at least two classes.
- A state cannot be a direct sub-type of a «Category».
- A state cannot have a class «OperatingFeature», «AutomationFeature», «Property», «Agent», «Metric», «Relator», «Category», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect sub-type.
- A state cannot have a class «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect super-type.
- A state cannot be abstract.
## Examples

The diagram below shows an OLDAS model with the operating feature `AutomatedVehicle` that have the states `AcceleratingVehicle`, `DeceleratingVehicle` and `ConstantSpeedVehicle` and  the operating feature `Illumination` that have the states `Day` and `Night`. Events are not represented on this diagram. <br><br>
![State Figure 1](State_Figure1.png)

