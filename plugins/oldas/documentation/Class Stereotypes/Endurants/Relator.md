# Relator
## Definition
A relator represents a relation between at least two individuals.
## Constraints
- A relator cannot have a class «Property», «OperatingFeature», «AutomationFeature», «Agent», «State», «Role», «Metric», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect super-type.
- A relator cannot have a «Category», Property», «OperatingFeature», «AutomationFeature», «Agent», «Metric», «TriggeringEvent», «MalfunctionEvent», «MishapEvent», or «NominalEvent» as its direct or indirect sub-type.
- A relator must always be connected (directly or indirectly) through at least one relation stereotyped as «Mediates» to an endurant class.
- The sum of the minimum cardinalities of the opposite ends of the mediations connected (directly or indirectly) to the relator must be greater or equal to 2.
## Examples
<br><br>
![Relator Figure 1](Relator_Figure1.png)
