# Generates
## Definition
The relation of generation is defined from an event to an endurent and represents the appearance of an object in a scenario.
## Constraints
- A «Generates» association shall be connected to an endurant as an incoming association.
- A «Generates» association shall be connected to an event as an outcoming association.
- The source end multiplicity shall be 1.
- The target end multiplicity shall be in the range [1-\*].
## Example
See figure 2 from NominalEvent page.
