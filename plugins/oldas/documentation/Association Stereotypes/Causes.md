# Causes
## Definition
The relation of causality is defined between two events.
## Constraints
### From UFO-B:Meets:
- A «Causes» association shall be connected to an event class as an outcoming and incoming association.
- The source end multiplicity shall be 1.
- The target end multiplicity shall be in the range [0-\*].
## Example
See AbstractEvent page.
