# Creates
## Definition
The «Creates» relation is defined from agents and their possible actions.
## Constraints
- A «Creates» association shall be connected to an «ActionEvent» class as an incoming association.
- A «Creates» association shall be connected to an «Agent» class as an outcoming association.
- The source end multiplicity shall be 1.
- The target end multiplicity shall be in the range [1-\*].
## Examples

