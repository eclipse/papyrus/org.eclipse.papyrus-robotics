# Characterizes
## Definition
The relation of characterization is defined from a property to an endurant.
## Constraints
- A «Characterizes» association shall be connected to a class stereotyped as «Property» as an outcoming association.
- A «Characterizes» association shall be connected to an endurant class as an intcoming association.
- The target source multiplicity shall be in the range 0..* if the connected property is not structured by a metric, otherwise it shall be in the range 1..*.
- The target end multiplicity shall be 1.
## Example
See Property page.
