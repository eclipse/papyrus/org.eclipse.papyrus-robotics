# Structures
## Definition
The relation of structuration is defined from a property to its metrics.
## Constraints
- A «Structures» association shall be connected to a class stereotyped as «Metric» as an outcoming association.
- A «Structures» association shall be connected to a class stereotyped as «Property» as an intcoming association.
- The target source multiplicity shall be 1.
- The target end multiplicity shall be 1.
## Example
See Property page.
