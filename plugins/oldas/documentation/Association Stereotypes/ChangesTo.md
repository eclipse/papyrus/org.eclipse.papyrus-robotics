# ChangesTo
## Definition
The «ChangesTo» association is defined from an event class to a «State» or «Role» class to represent a possible transitions from a state/role to another state/role. This latter state/role is connected to the event through the «ChangesFrom» association.
## Constraints
- A «ChangesTo» association shall be connected to a «Role» or «State» class as an incoming association.
- A «ChangesTo» association shall be connected to an event` class as an outcoming association.
- The target end multiplicity shall be 1.
- The source end multiplicity shall be in the range [1-\*].
- At least one «ChangesFrom» association shall be connected to the same source end event class.
## Examples
<br><br>
![ChangesFrom ChangesTo Figure 1](ChangesFrom_ChangesTo_Figure1.png)

