# Composite
## Definition
The `Composite` association represents an exclusive parthood relation between two classes. This association is visualized with a filled diamond (⬥) on the side of the whole (i.e, the class composed of the other).

## Example
The diagram below shows a simple example of composite relations between classes. Events are not represented on this diagram.
<br><br>
![Composite Figure 1](Composite_Figure1.png)
