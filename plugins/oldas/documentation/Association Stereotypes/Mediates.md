# Mediates
## Definition
The relation of mediation is defined from a «Relator» to its entities it connects.
## Constraints
- A «Mediates» association shall be connected to a class stereotyped as «Relator» as an outcoming association.
- A «Mediates» association shall be connected to an endurant class as an incoming association.
- The source end multiplicity shall be in the range [1-\*].
- The target end multiplicity shall be in the range [1-\*].
## Example
See Relator page.
