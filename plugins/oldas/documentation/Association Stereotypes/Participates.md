# Participates
## Definition
The relation of participation is defined from non-event types to event types. It represents the type of individuals required to be resent in the environment to allow the occurrence of a specific event.
## Constraints
- A «Participates» association shall be connected to an endurant class.
- A «Participates» association shall be connected to an event class.
- The target end multiplicity shall be [1-\*].
- The source end multiplicity shall be 1.
## Example
See AbstractEvent page.
