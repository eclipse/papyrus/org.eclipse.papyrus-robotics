# Shared
# Definition
The `Shared` association represents a shared parthood relation between two classes. This association is visualized with a empty diamond (◇) on the side of the whole (i.e, the class composed of the other).

# Example
TODO
