# Destroys
## Definition
The «Destroys» relation is defined from an event to an endurent and represents the disappearance of an object in a scenario.
## Constraints
- A «Destroys» association shall be connected to an endurant as an incoming association.
- A «Destroys» association shall be connected to an event as an outcoming association.
- The source end multiplicity shall be 1.
- The target end multiplicity shall be in the range [1-\*].
## Example
See figure 2 from NominalEvent page.
