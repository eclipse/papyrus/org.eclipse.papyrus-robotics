package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.general;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Stereotype;

public class ExactlyOneStereotypeConstraint extends  AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class oldasClass = (Class) ctx.getTarget();
		EList<Stereotype> stereotypes = oldasClass.getAppliedStereotypes();
		if(stereotypes == null || stereotypes.size() != 1) {
			return ctx.createFailureStatus("Class "+oldasClass.getName()+" must have exactly one stereotype.", oldasClass.getName());
		}
		return null;
	}

}
