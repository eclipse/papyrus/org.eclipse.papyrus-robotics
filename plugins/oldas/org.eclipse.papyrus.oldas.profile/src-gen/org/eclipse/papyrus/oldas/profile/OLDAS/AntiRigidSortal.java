/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Anti Rigid Sortal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#getAntiRigidSortal()
 * @model abstract="true"
 * @generated
 */
public interface AntiRigidSortal extends EndurantClass {
} // AntiRigidSortal
