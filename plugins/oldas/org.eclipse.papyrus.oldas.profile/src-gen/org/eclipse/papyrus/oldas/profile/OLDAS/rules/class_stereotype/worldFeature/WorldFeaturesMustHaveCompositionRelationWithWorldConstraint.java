package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.worldFeature;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.World;
import org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class WorldFeaturesMustHaveCompositionRelationWithWorldConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		//TODO manage inheritance case
		Class worldFeatureClass = (Class) ctx.getTarget();
		WorldFeature worldFeatureStereotype = UMLUtil.getStereotypeApplication(worldFeatureClass, WorldFeature.class);
		//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint from Model "+oldasClass.getModel().getName()+" Class:"+oldasClass.getName()+" Stereotype:"+oldasClass.getAppliedStereotypes().get(0).getName());
		if(worldFeatureStereotype == null) {
			return null;
		}
		//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint FLAG1");
		Class worldClass = findWorldClass(worldFeatureClass.getModel());
		//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint FLAG2");
		if(worldClass == null) {
			//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint world not found!");
			return null;
		}
		//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint FLAG3");
		if(!isADirectOrIndirectComponent(worldFeatureClass, worldClass)) {
			String stereotypeName = worldFeatureClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(worldFeatureClass.getName()+" class stereotyped as "+stereotypeName+" must be a direct or indirect component of <<World>>");
		}
		return null;
	}
	
	private boolean isADirectOrIndirectComponent(Class classSource, Class classTarget) {
		EList<Association> associations = classSource.getAssociations();
		//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint classSource:"+classSource.getName()+" n associations:"+associations.size());
		for (Association association: associations) {
			for (Property memberEnd: association.getMemberEnds()) {
				if(memberEnd.isComposite() && memberEnd.getType() == classSource) {//if class source is a component of another class (represented by the current composite association)
					Class directTarget = (Class) memberEnd.getOtherEnd().getType();
					//System.out.println("LOG WorldFeaturesMustHaveCompositionRelationWithWorldConstraint directTargetClass:"+directTarget.getAppliedStereotypes().get(0)+" World:"+classTarget.getAppliedStereotypes().get(0));
					if(directTarget == classTarget) {//if class source is a direct component of class target
						return true;
					}
					if(isADirectOrIndirectComponent(directTarget, classTarget)){//if class source is a indirect component of class target
						return true;
					}
				}
			}
		}
		for(Classifier parentClass: classSource.getGenerals()) {//if at least one parent class is a direct or indirect component, then the present class is a direct or indirect component too
			if(isADirectOrIndirectComponent((Class) parentClass, classTarget)) {
				return true;
			}
		}
		return false;
	}
	
	private Class findWorldClass(Model model) {
		for(Element e: model.getOwnedElements()) {
			World worldStereotype = UMLUtil.getStereotypeApplication(e, World.class);
			if(worldStereotype != null) {
				return (Class) e;
			}
		}
		return null;
	}

}
