package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.property;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class PropertyMustBeConnectedToEndurantConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class propertyClass = (Class) ctx.getTarget();
		Property propertyStereotype = UMLUtil.getStereotypeApplication(propertyClass, Property.class);
		if(propertyStereotype == null){
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToEndurant(propertyClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(propertyClass.getName()+" class stereotyped as "+propertyClass.getAppliedStereotypes().get(0).getName()+" must be connected through at least one <<Characterizes>> relation to an non-event class");

	}

	private boolean directlyOrIndirectlyConnectedToEndurant(Class propertyClass) {
		for(Association association: propertyClass.getAssociations()) {
			Characterizes characterizesStereotype = UMLUtil.getStereotypeApplication(association, Characterizes.class);
			if(characterizesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					EndurantClass endurantStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class);
					boolean isEndurant = endurantStereotype != null;
					if(memberEnd.getType()!=propertyClass && isEndurant && memberEnd.isNavigable()) {
						return true;
					}
				}
			}
		}//direclty connected
		
		for(Classifier parent:propertyClass.parents()) {
			if(directlyOrIndirectlyConnectedToEndurant((Class) parent)) {
				return true;
			}
		}//indirectly connected
		
		return false;
	}

}
