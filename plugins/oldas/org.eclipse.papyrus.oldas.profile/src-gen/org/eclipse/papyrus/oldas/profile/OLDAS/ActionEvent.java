/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#getActionEvent()
 * @model
 * @generated
 */
public interface ActionEvent extends AbstractEvent {

} // ActionEvent
