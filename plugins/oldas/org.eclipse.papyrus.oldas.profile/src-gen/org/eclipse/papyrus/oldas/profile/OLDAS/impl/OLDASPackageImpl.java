/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.ActionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Agent;
import org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal;
import org.eclipse.papyrus.oldas.profile.OLDAS.AutomationFeature;
import org.eclipse.papyrus.oldas.profile.OLDAS.Category;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.papyrus.oldas.profile.OLDAS.ChangesFrom;
import org.eclipse.papyrus.oldas.profile.OLDAS.ChangesTo;
import org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes;
import org.eclipse.papyrus.oldas.profile.OLDAS.Creates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Destroys;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Generates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Kind;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Mediates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.NominalEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASFactory;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;
import org.eclipse.papyrus.oldas.profile.OLDAS.OperatingFeature;
import org.eclipse.papyrus.oldas.profile.OLDAS.Participates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.papyrus.oldas.profile.OLDAS.Role;
import org.eclipse.papyrus.oldas.profile.OLDAS.State;
import org.eclipse.papyrus.oldas.profile.OLDAS.Structures;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.World;
import org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OLDASPackageImpl extends EPackageImpl implements OLDASPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endurantClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass kindEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass antiRigidSortalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass worldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass worldFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass automationFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatingFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass triggeringEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass malfunctionEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mishapEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass causesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass participatesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass characterizesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structuresEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generatesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass destroysEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changesFromEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changesToEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mediatesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nominalEventEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OLDASPackageImpl() {
		super(eNS_URI, OLDASFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OLDASPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OLDASPackage init() {
		if (isInited) return (OLDASPackage)EPackage.Registry.INSTANCE.getEPackage(OLDASPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOLDASPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OLDASPackageImpl theOLDASPackage = registeredOLDASPackage instanceof OLDASPackageImpl ? (OLDASPackageImpl)registeredOLDASPackage : new OLDASPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOLDASPackage.createPackageContents();

		// Initialize created meta-data
		theOLDASPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOLDASPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OLDASPackage.eNS_URI, theOLDASPackage);
		return theOLDASPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEndurantClass() {
		return endurantClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEndurantClass_Base_Class() {
		return (EReference)endurantClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRelator() {
		return relatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategory() {
		return categoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetric() {
		return metricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKind() {
		return kindEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAntiRigidSortal() {
		return antiRigidSortalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorld() {
		return worldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWorldFeature() {
		return worldFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAutomationFeature() {
		return automationFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperatingFeature() {
		return operatingFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getState() {
		return stateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole() {
		return roleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractEvent() {
		return abstractEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractEvent_Base_Class() {
		return (EReference)abstractEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTriggeringEvent() {
		return triggeringEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMalfunctionEvent() {
		return malfunctionEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMishapEvent() {
		return mishapEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCauses() {
		return causesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCauses_Base_Association() {
		return (EReference)causesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParticipates() {
		return participatesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParticipates_Base_Association() {
		return (EReference)participatesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCharacterizes() {
		return characterizesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCharacterizes_Base_Association() {
		return (EReference)characterizesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStructures() {
		return structuresEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStructures_Base_Association() {
		return (EReference)structuresEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenerates() {
		return generatesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGenerates_Base_Association() {
		return (EReference)generatesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDestroys() {
		return destroysEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDestroys_Base_Association() {
		return (EReference)destroysEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangesFrom() {
		return changesFromEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangesFrom_Base_Association() {
		return (EReference)changesFromEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangesTo() {
		return changesToEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangesTo_Base_Association() {
		return (EReference)changesToEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreates() {
		return createsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCreates_Base_Association() {
		return (EReference)createsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMediates() {
		return mediatesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMediates_Base_Association() {
		return (EReference)mediatesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgent() {
		return agentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionEvent() {
		return actionEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNominalEvent() {
		return nominalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OLDASFactory getOLDASFactory() {
		return (OLDASFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		endurantClassEClass = createEClass(ENDURANT_CLASS);
		createEReference(endurantClassEClass, ENDURANT_CLASS__BASE_CLASS);

		relatorEClass = createEClass(RELATOR);

		categoryEClass = createEClass(CATEGORY);

		propertyEClass = createEClass(PROPERTY);

		metricEClass = createEClass(METRIC);

		kindEClass = createEClass(KIND);

		antiRigidSortalEClass = createEClass(ANTI_RIGID_SORTAL);

		worldEClass = createEClass(WORLD);

		worldFeatureEClass = createEClass(WORLD_FEATURE);

		automationFeatureEClass = createEClass(AUTOMATION_FEATURE);

		operatingFeatureEClass = createEClass(OPERATING_FEATURE);

		stateEClass = createEClass(STATE);

		roleEClass = createEClass(ROLE);

		abstractEventEClass = createEClass(ABSTRACT_EVENT);
		createEReference(abstractEventEClass, ABSTRACT_EVENT__BASE_CLASS);

		triggeringEventEClass = createEClass(TRIGGERING_EVENT);

		malfunctionEventEClass = createEClass(MALFUNCTION_EVENT);

		mishapEventEClass = createEClass(MISHAP_EVENT);

		causesEClass = createEClass(CAUSES);
		createEReference(causesEClass, CAUSES__BASE_ASSOCIATION);

		participatesEClass = createEClass(PARTICIPATES);
		createEReference(participatesEClass, PARTICIPATES__BASE_ASSOCIATION);

		characterizesEClass = createEClass(CHARACTERIZES);
		createEReference(characterizesEClass, CHARACTERIZES__BASE_ASSOCIATION);

		structuresEClass = createEClass(STRUCTURES);
		createEReference(structuresEClass, STRUCTURES__BASE_ASSOCIATION);

		generatesEClass = createEClass(GENERATES);
		createEReference(generatesEClass, GENERATES__BASE_ASSOCIATION);

		destroysEClass = createEClass(DESTROYS);
		createEReference(destroysEClass, DESTROYS__BASE_ASSOCIATION);

		changesFromEClass = createEClass(CHANGES_FROM);
		createEReference(changesFromEClass, CHANGES_FROM__BASE_ASSOCIATION);

		changesToEClass = createEClass(CHANGES_TO);
		createEReference(changesToEClass, CHANGES_TO__BASE_ASSOCIATION);

		createsEClass = createEClass(CREATES);
		createEReference(createsEClass, CREATES__BASE_ASSOCIATION);

		mediatesEClass = createEClass(MEDIATES);
		createEReference(mediatesEClass, MEDIATES__BASE_ASSOCIATION);

		agentEClass = createEClass(AGENT);

		actionEventEClass = createEClass(ACTION_EVENT);

		nominalEventEClass = createEClass(NOMINAL_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		relatorEClass.getESuperTypes().add(this.getEndurantClass());
		categoryEClass.getESuperTypes().add(this.getEndurantClass());
		propertyEClass.getESuperTypes().add(this.getEndurantClass());
		metricEClass.getESuperTypes().add(this.getEndurantClass());
		kindEClass.getESuperTypes().add(this.getEndurantClass());
		antiRigidSortalEClass.getESuperTypes().add(this.getEndurantClass());
		worldEClass.getESuperTypes().add(this.getKind());
		worldFeatureEClass.getESuperTypes().add(this.getKind());
		automationFeatureEClass.getESuperTypes().add(this.getWorldFeature());
		operatingFeatureEClass.getESuperTypes().add(this.getWorldFeature());
		stateEClass.getESuperTypes().add(this.getAntiRigidSortal());
		roleEClass.getESuperTypes().add(this.getAntiRigidSortal());
		triggeringEventEClass.getESuperTypes().add(this.getAbstractEvent());
		malfunctionEventEClass.getESuperTypes().add(this.getAbstractEvent());
		mishapEventEClass.getESuperTypes().add(this.getAbstractEvent());
		agentEClass.getESuperTypes().add(this.getWorldFeature());
		actionEventEClass.getESuperTypes().add(this.getAbstractEvent());
		nominalEventEClass.getESuperTypes().add(this.getAbstractEvent());

		// Initialize classes, features, and operations; add parameters
		initEClass(endurantClassEClass, EndurantClass.class, "EndurantClass", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEndurantClass_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, EndurantClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(relatorEClass, Relator.class, "Relator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(categoryEClass, Category.class, "Category", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(metricEClass, Metric.class, "Metric", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(kindEClass, Kind.class, "Kind", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(antiRigidSortalEClass, AntiRigidSortal.class, "AntiRigidSortal", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(worldEClass, World.class, "World", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(worldFeatureEClass, WorldFeature.class, "WorldFeature", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(automationFeatureEClass, AutomationFeature.class, "AutomationFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(operatingFeatureEClass, OperatingFeature.class, "OperatingFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stateEClass, State.class, "State", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(roleEClass, Role.class, "Role", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractEventEClass, AbstractEvent.class, "AbstractEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractEvent_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 0, 1, AbstractEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(triggeringEventEClass, TriggeringEvent.class, "TriggeringEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(malfunctionEventEClass, MalfunctionEvent.class, "MalfunctionEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mishapEventEClass, MishapEvent.class, "MishapEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(causesEClass, Causes.class, "Causes", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCauses_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Causes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(participatesEClass, Participates.class, "Participates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParticipates_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Participates.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(characterizesEClass, Characterizes.class, "Characterizes", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCharacterizes_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Characterizes.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(structuresEClass, Structures.class, "Structures", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStructures_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Structures.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(generatesEClass, Generates.class, "Generates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGenerates_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Generates.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(destroysEClass, Destroys.class, "Destroys", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDestroys_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Destroys.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(changesFromEClass, ChangesFrom.class, "ChangesFrom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChangesFrom_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, ChangesFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(changesToEClass, ChangesTo.class, "ChangesTo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChangesTo_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, ChangesTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(createsEClass, Creates.class, "Creates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCreates_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Creates.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(mediatesEClass, Mediates.class, "Mediates", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMediates_Base_Association(), theUMLPackage.getAssociation(), null, "base_Association", null, 0, 1, Mediates.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(agentEClass, Agent.class, "Agent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(actionEventEClass, ActionEvent.class, "ActionEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nominalEventEClass, NominalEvent.class, "NominalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //OLDASPackageImpl
