/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#getState()
 * @model
 * @generated
 */
public interface State extends AntiRigidSortal {
} // State
