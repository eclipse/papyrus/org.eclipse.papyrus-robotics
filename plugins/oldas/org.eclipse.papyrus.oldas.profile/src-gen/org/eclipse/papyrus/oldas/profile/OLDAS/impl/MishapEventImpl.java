/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mishap Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MishapEventImpl extends AbstractEventImpl implements MishapEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MishapEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.MISHAP_EVENT;
	}

} //MishapEventImpl
