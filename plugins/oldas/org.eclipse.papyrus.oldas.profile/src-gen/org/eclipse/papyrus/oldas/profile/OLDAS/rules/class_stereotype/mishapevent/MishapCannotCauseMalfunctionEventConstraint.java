package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.mishapevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MishapCannotCauseMalfunctionEventConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class mishapEventClass = (Class) ctx.getTarget();
		MishapEvent mishapEventStereotype = UMLUtil.getStereotypeApplication(mishapEventClass, MishapEvent.class);
		if(mishapEventStereotype == null){
			return null;
		}
		if(!directlyOrIndirectlyConnectedToMalfunctionEvent(mishapEventClass)) {
			return null;
		}
		return ctx.createFailureStatus(mishapEventClass.getName()+" class stereotyped as MishapEvent cannot be"
				+ " connected direclty or indireclty through a «Causes» relation to a «TriggeringEvent» class");
	}

	private boolean directlyOrIndirectlyConnectedToMalfunctionEvent(Class mishapEventClass) {
		for(Association association: mishapEventClass.getAssociations()) {
			Causes causesStereotype = UMLUtil.getStereotypeApplication(association, Causes.class);
			if(causesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != mishapEventClass 
							&& memberEnd.isNavigable()) {
						if( UMLUtil.getStereotypeApplication(memberEnd.getType(), MalfunctionEvent.class) != null) {
							return true;
						}// directly connected
						if(directlyOrIndirectlyConnectedToMalfunctionEvent((Class) memberEnd.getType())) {
							return true;
						} // indirectly connected through a caused event
					}
				}
			}
		}
	
		for(Classifier parent:mishapEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToMalfunctionEvent((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		
		return false;
	}

}