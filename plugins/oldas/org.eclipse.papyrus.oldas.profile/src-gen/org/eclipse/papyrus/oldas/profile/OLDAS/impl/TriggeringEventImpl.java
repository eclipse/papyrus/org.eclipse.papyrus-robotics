/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triggering Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TriggeringEventImpl extends AbstractEventImpl implements TriggeringEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TriggeringEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.TRIGGERING_EVENT;
	}

} //TriggeringEventImpl
