package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Structures;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CharacterizesConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association characterizesAssociation = (Association) ctx.getTarget();
		Characterizes characterizesStereotype = UMLUtil.getStereotypeApplication(characterizesAssociation, Characterizes.class);
		if(characterizesStereotype == null) {
			return null;
		}
		for (org.eclipse.uml2.uml.Property memberEnd: characterizesAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), Property.class) == null && !memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class) == null && memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «Characterizes» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must connect a «Property» to an endurant class.");
			}
			
			if(memberEnd.isNavigable() && (memberEnd.getLower() != 1 || memberEnd.getUpper() != 1)) {
				return ctx.createFailureStatus("The «Characterizes» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a source multiplicity equal to 1.");
			}
			
			if(!memberEnd.isNavigable() && isStructuredByAMetric((Class) memberEnd.getType()) && memberEnd.getLower() < 1) {
				return ctx.createFailureStatus("The «Characterizes» association from the class from the class "+memberEnd.getOtherEnd().getType().getName()
						 +" to the class "+memberEnd.getType().getName()+" must have a source multiplicity in the range [1..*].");
			}
		}
		
		return null;
	}

	private boolean isStructuredByAMetric(Class propertyClass) {
		for(Association association: propertyClass.getAssociations()) {
			if(UMLUtil.getStereotypeApplication(association, Structures.class) != null) {
				return true;
			}
		}
		return false;
	}

}
