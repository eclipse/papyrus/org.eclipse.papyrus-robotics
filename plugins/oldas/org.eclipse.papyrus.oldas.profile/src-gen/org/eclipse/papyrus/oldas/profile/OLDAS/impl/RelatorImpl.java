/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RelatorImpl extends EndurantClassImpl implements Relator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.RELATOR;
	}

} //RelatorImpl
