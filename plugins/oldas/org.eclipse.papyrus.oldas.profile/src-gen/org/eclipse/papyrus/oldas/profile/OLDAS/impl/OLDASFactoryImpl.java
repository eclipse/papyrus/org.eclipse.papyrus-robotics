/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.papyrus.oldas.profile.OLDAS.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OLDASFactoryImpl extends EFactoryImpl implements OLDASFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OLDASFactory init() {
		try {
			OLDASFactory theOLDASFactory = (OLDASFactory)EPackage.Registry.INSTANCE.getEFactory(OLDASPackage.eNS_URI);
			if (theOLDASFactory != null) {
				return theOLDASFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OLDASFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OLDASFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OLDASPackage.RELATOR: return createRelator();
			case OLDASPackage.CATEGORY: return createCategory();
			case OLDASPackage.PROPERTY: return createProperty();
			case OLDASPackage.METRIC: return createMetric();
			case OLDASPackage.WORLD: return createWorld();
			case OLDASPackage.AUTOMATION_FEATURE: return createAutomationFeature();
			case OLDASPackage.OPERATING_FEATURE: return createOperatingFeature();
			case OLDASPackage.STATE: return createState();
			case OLDASPackage.ROLE: return createRole();
			case OLDASPackage.TRIGGERING_EVENT: return createTriggeringEvent();
			case OLDASPackage.MALFUNCTION_EVENT: return createMalfunctionEvent();
			case OLDASPackage.MISHAP_EVENT: return createMishapEvent();
			case OLDASPackage.CAUSES: return createCauses();
			case OLDASPackage.PARTICIPATES: return createParticipates();
			case OLDASPackage.CHARACTERIZES: return createCharacterizes();
			case OLDASPackage.STRUCTURES: return createStructures();
			case OLDASPackage.GENERATES: return createGenerates();
			case OLDASPackage.DESTROYS: return createDestroys();
			case OLDASPackage.CHANGES_FROM: return createChangesFrom();
			case OLDASPackage.CHANGES_TO: return createChangesTo();
			case OLDASPackage.CREATES: return createCreates();
			case OLDASPackage.MEDIATES: return createMediates();
			case OLDASPackage.AGENT: return createAgent();
			case OLDASPackage.ACTION_EVENT: return createActionEvent();
			case OLDASPackage.NOMINAL_EVENT: return createNominalEvent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relator createRelator() {
		RelatorImpl relator = new RelatorImpl();
		return relator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category createCategory() {
		CategoryImpl category = new CategoryImpl();
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Metric createMetric() {
		MetricImpl metric = new MetricImpl();
		return metric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public World createWorld() {
		WorldImpl world = new WorldImpl();
		return world;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutomationFeature createAutomationFeature() {
		AutomationFeatureImpl automationFeature = new AutomationFeatureImpl();
		return automationFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatingFeature createOperatingFeature() {
		OperatingFeatureImpl operatingFeature = new OperatingFeatureImpl();
		return operatingFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createState() {
		StateImpl state = new StateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TriggeringEvent createTriggeringEvent() {
		TriggeringEventImpl triggeringEvent = new TriggeringEventImpl();
		return triggeringEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MalfunctionEvent createMalfunctionEvent() {
		MalfunctionEventImpl malfunctionEvent = new MalfunctionEventImpl();
		return malfunctionEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MishapEvent createMishapEvent() {
		MishapEventImpl mishapEvent = new MishapEventImpl();
		return mishapEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Causes createCauses() {
		CausesImpl causes = new CausesImpl();
		return causes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participates createParticipates() {
		ParticipatesImpl participates = new ParticipatesImpl();
		return participates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Characterizes createCharacterizes() {
		CharacterizesImpl characterizes = new CharacterizesImpl();
		return characterizes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Structures createStructures() {
		StructuresImpl structures = new StructuresImpl();
		return structures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Generates createGenerates() {
		GeneratesImpl generates = new GeneratesImpl();
		return generates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Destroys createDestroys() {
		DestroysImpl destroys = new DestroysImpl();
		return destroys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangesFrom createChangesFrom() {
		ChangesFromImpl changesFrom = new ChangesFromImpl();
		return changesFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangesTo createChangesTo() {
		ChangesToImpl changesTo = new ChangesToImpl();
		return changesTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Creates createCreates() {
		CreatesImpl creates = new CreatesImpl();
		return creates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mediates createMediates() {
		MediatesImpl mediates = new MediatesImpl();
		return mediates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent createAgent() {
		AgentImpl agent = new AgentImpl();
		return agent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionEvent createActionEvent() {
		ActionEventImpl actionEvent = new ActionEventImpl();
		return actionEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NominalEvent createNominalEvent() {
		NominalEventImpl nominalEvent = new NominalEventImpl();
		return nominalEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OLDASPackage getOLDASPackage() {
		return (OLDASPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OLDASPackage getPackage() {
		return OLDASPackage.eINSTANCE;
	}

} //OLDASFactoryImpl
