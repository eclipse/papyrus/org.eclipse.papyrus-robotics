package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.world;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.World;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class WorldCannotBeSubtypedOrSupertypedConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class worldClass = (Class) ctx.getTarget();
		//System.out.println("LOG WorldCannotBeSubtypedOrSupertypedConstraint from Model "+worldClass.getModel().getName()+" class Stereotype:"+worldClass.getAppliedStereotype("model::World"));
		World worldStereotype = UMLUtil.getStereotypeApplication(worldClass, World.class);
		if(worldStereotype == null) {
			return null;
		}
		//System.out.println("LOG WorldCannotBeSubtypedOrSupertypedConstraint from Model "+worldClass.getModel().getName()+" n Generalizations:"+worldClass.getGeneralizations().size());
		for(Relationship e:worldClass.getRelationships()) {
			if(e instanceof Generalization) {
				Class generalClass = (Class) ((Generalization) e).getGeneral();
				World generalClassWorldStereotype = UMLUtil.getStereotypeApplication(generalClass, World.class);
				if(generalClassWorldStereotype != null) {
					return ctx.createFailureStatus("<<World>> cannot be subtyped");
				}
				
				Class specificClass = (Class) ((Generalization) e).getSpecific();
				World specificClassWorldStereotype = UMLUtil.getStereotypeApplication(specificClass, World.class);
				if(specificClassWorldStereotype != null) {
					return ctx.createFailureStatus("<<World>> cannot be supertyped");
				}
			}
		}
		return null;
	}

}
