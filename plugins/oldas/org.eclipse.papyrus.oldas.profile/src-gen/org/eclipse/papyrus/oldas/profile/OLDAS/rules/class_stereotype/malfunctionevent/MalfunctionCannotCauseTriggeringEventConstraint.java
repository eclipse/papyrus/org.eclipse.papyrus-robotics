package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.malfunctionevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MalfunctionCannotCauseTriggeringEventConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class malfunctionEventClass = (Class) ctx.getTarget();
		MalfunctionEvent malfunctionEventStereotype = UMLUtil.getStereotypeApplication(malfunctionEventClass, MalfunctionEvent.class);
		if(malfunctionEventStereotype == null){
			return null;
		}
		if(!directlyOrIndirectlyConnectedToTriggeringEvent(malfunctionEventClass)) {
			return null;
		}
		return ctx.createFailureStatus(malfunctionEventClass.getName()+" class stereotyped as MalfunctionEvent cannot be"
				+ " connected direclty or indireclty through a «Causes» relation to a «TriggeringEvent» class");
	}

	private boolean directlyOrIndirectlyConnectedToTriggeringEvent(Class malfunctionEventClass) {
		for(Association association: malfunctionEventClass.getAssociations()) {
			Causes causesStereotype = UMLUtil.getStereotypeApplication(association, Causes.class);
			if(causesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != malfunctionEventClass 
							&& memberEnd.isNavigable()) {
						if( UMLUtil.getStereotypeApplication(memberEnd.getType(), TriggeringEvent.class) != null) {
							return true;
						}// directly connected
						if(directlyOrIndirectlyConnectedToTriggeringEvent((Class) memberEnd.getType())) {
							return true;
						} // indirectly connected through a caused event
					}
				}
			}
		}
	
		for(Classifier parent:malfunctionEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToTriggeringEvent((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		
		return false;
	}

}
