/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Malfunction Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#getMalfunctionEvent()
 * @model
 * @generated
 */
public interface MalfunctionEvent extends AbstractEvent {
} // MalfunctionEvent
