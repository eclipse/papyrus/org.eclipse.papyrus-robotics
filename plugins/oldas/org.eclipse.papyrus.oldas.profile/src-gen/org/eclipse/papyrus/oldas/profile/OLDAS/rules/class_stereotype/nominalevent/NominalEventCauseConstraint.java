package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.nominalevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.NominalEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class NominalEventCauseConstraint extends AbstractModelConstraint{

	private String causedClassStereotypeName;
	
	@Override
	public IStatus validate(IValidationContext ctx) {
		Class nominalEventClass = (Class) ctx.getTarget();
		NominalEvent nominalEventStereotype = UMLUtil.getStereotypeApplication(nominalEventClass, NominalEvent.class);
		if(nominalEventStereotype == null){
			return null;
		}
		
		if(!directlyOrIndirectlyConnectedToCriticalEvent(nominalEventClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(nominalEventClass.getName()+" class stereotyped as "+nominalEventClass.getAppliedStereotypes().get(0).getName()+" cannot be directly or indirectly connected through a  «Causes» relation to a «"+this.causedClassStereotypeName+"» class");

	}

	private boolean directlyOrIndirectlyConnectedToCriticalEvent(Class nominalEventClass) {
		for(Association association: nominalEventClass.getAssociations()) {
			Causes causesStereotype = UMLUtil.getStereotypeApplication(association, Causes.class);
			if(causesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != nominalEventClass 
							&& memberEnd.isNavigable()) {
						if(UMLUtil.getStereotypeApplication(memberEnd.getType(), TriggeringEvent.class) != null ||
								UMLUtil.getStereotypeApplication(memberEnd.getType(), MalfunctionEvent.class) != null ||
								UMLUtil.getStereotypeApplication(memberEnd.getType(), MishapEvent.class) != null) {
							this.causedClassStereotypeName = memberEnd.getType().getAppliedStereotypes().get(0).getName();
							return true;
						} // directly connected
						if(directlyOrIndirectlyConnectedToCriticalEvent((Class) memberEnd.getType())) {
							return true;
						} // indirectly connected through a caused event
					}
							
				}
			}
		}
	
		for(Classifier parent:nominalEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToCriticalEvent((Class) parent)) {
				return true;
			}
		}//indirectly connected through a parent
		
		return false;
	}
}