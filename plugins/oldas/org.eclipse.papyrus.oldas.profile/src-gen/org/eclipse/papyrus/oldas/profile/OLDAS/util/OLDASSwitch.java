/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.papyrus.oldas.profile.OLDAS.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage
 * @generated
 */
public class OLDASSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OLDASPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OLDASSwitch() {
		if (modelPackage == null) {
			modelPackage = OLDASPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OLDASPackage.ENDURANT_CLASS: {
				EndurantClass endurantClass = (EndurantClass)theEObject;
				T result = caseEndurantClass(endurantClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.RELATOR: {
				Relator relator = (Relator)theEObject;
				T result = caseRelator(relator);
				if (result == null) result = caseEndurantClass(relator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.CATEGORY: {
				Category category = (Category)theEObject;
				T result = caseCategory(category);
				if (result == null) result = caseEndurantClass(category);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.PROPERTY: {
				Property property = (Property)theEObject;
				T result = caseProperty(property);
				if (result == null) result = caseEndurantClass(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.METRIC: {
				Metric metric = (Metric)theEObject;
				T result = caseMetric(metric);
				if (result == null) result = caseEndurantClass(metric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.KIND: {
				Kind kind = (Kind)theEObject;
				T result = caseKind(kind);
				if (result == null) result = caseEndurantClass(kind);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.ANTI_RIGID_SORTAL: {
				AntiRigidSortal antiRigidSortal = (AntiRigidSortal)theEObject;
				T result = caseAntiRigidSortal(antiRigidSortal);
				if (result == null) result = caseEndurantClass(antiRigidSortal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.WORLD: {
				World world = (World)theEObject;
				T result = caseWorld(world);
				if (result == null) result = caseKind(world);
				if (result == null) result = caseEndurantClass(world);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.WORLD_FEATURE: {
				WorldFeature worldFeature = (WorldFeature)theEObject;
				T result = caseWorldFeature(worldFeature);
				if (result == null) result = caseKind(worldFeature);
				if (result == null) result = caseEndurantClass(worldFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.AUTOMATION_FEATURE: {
				AutomationFeature automationFeature = (AutomationFeature)theEObject;
				T result = caseAutomationFeature(automationFeature);
				if (result == null) result = caseWorldFeature(automationFeature);
				if (result == null) result = caseKind(automationFeature);
				if (result == null) result = caseEndurantClass(automationFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.OPERATING_FEATURE: {
				OperatingFeature operatingFeature = (OperatingFeature)theEObject;
				T result = caseOperatingFeature(operatingFeature);
				if (result == null) result = caseWorldFeature(operatingFeature);
				if (result == null) result = caseKind(operatingFeature);
				if (result == null) result = caseEndurantClass(operatingFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.STATE: {
				State state = (State)theEObject;
				T result = caseState(state);
				if (result == null) result = caseAntiRigidSortal(state);
				if (result == null) result = caseEndurantClass(state);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.ROLE: {
				Role role = (Role)theEObject;
				T result = caseRole(role);
				if (result == null) result = caseAntiRigidSortal(role);
				if (result == null) result = caseEndurantClass(role);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.ABSTRACT_EVENT: {
				AbstractEvent abstractEvent = (AbstractEvent)theEObject;
				T result = caseAbstractEvent(abstractEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.TRIGGERING_EVENT: {
				TriggeringEvent triggeringEvent = (TriggeringEvent)theEObject;
				T result = caseTriggeringEvent(triggeringEvent);
				if (result == null) result = caseAbstractEvent(triggeringEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.MALFUNCTION_EVENT: {
				MalfunctionEvent malfunctionEvent = (MalfunctionEvent)theEObject;
				T result = caseMalfunctionEvent(malfunctionEvent);
				if (result == null) result = caseAbstractEvent(malfunctionEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.MISHAP_EVENT: {
				MishapEvent mishapEvent = (MishapEvent)theEObject;
				T result = caseMishapEvent(mishapEvent);
				if (result == null) result = caseAbstractEvent(mishapEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.CAUSES: {
				Causes causes = (Causes)theEObject;
				T result = caseCauses(causes);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.PARTICIPATES: {
				Participates participates = (Participates)theEObject;
				T result = caseParticipates(participates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.CHARACTERIZES: {
				Characterizes characterizes = (Characterizes)theEObject;
				T result = caseCharacterizes(characterizes);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.STRUCTURES: {
				Structures structures = (Structures)theEObject;
				T result = caseStructures(structures);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.GENERATES: {
				Generates generates = (Generates)theEObject;
				T result = caseGenerates(generates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.DESTROYS: {
				Destroys destroys = (Destroys)theEObject;
				T result = caseDestroys(destroys);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.CHANGES_FROM: {
				ChangesFrom changesFrom = (ChangesFrom)theEObject;
				T result = caseChangesFrom(changesFrom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.CHANGES_TO: {
				ChangesTo changesTo = (ChangesTo)theEObject;
				T result = caseChangesTo(changesTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.CREATES: {
				Creates creates = (Creates)theEObject;
				T result = caseCreates(creates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.MEDIATES: {
				Mediates mediates = (Mediates)theEObject;
				T result = caseMediates(mediates);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.AGENT: {
				Agent agent = (Agent)theEObject;
				T result = caseAgent(agent);
				if (result == null) result = caseWorldFeature(agent);
				if (result == null) result = caseKind(agent);
				if (result == null) result = caseEndurantClass(agent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.ACTION_EVENT: {
				ActionEvent actionEvent = (ActionEvent)theEObject;
				T result = caseActionEvent(actionEvent);
				if (result == null) result = caseAbstractEvent(actionEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OLDASPackage.NOMINAL_EVENT: {
				NominalEvent nominalEvent = (NominalEvent)theEObject;
				T result = caseNominalEvent(nominalEvent);
				if (result == null) result = caseAbstractEvent(nominalEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Endurant Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Endurant Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEndurantClass(EndurantClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelator(Relator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Category</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Category</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCategory(Category object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetric(Metric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Kind</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Kind</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKind(Kind object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anti Rigid Sortal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anti Rigid Sortal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAntiRigidSortal(AntiRigidSortal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>World</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>World</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorld(World object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>World Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>World Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorldFeature(WorldFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Automation Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Automation Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAutomationFeature(AutomationFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operating Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operating Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperatingFeature(OperatingFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRole(Role object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractEvent(AbstractEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggering Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggering Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTriggeringEvent(TriggeringEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Malfunction Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Malfunction Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMalfunctionEvent(MalfunctionEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mishap Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mishap Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMishapEvent(MishapEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Causes</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Causes</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCauses(Causes object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Participates</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Participates</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParticipates(Participates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Characterizes</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Characterizes</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCharacterizes(Characterizes object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Structures</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Structures</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStructures(Structures object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generates</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generates</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenerates(Generates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Destroys</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Destroys</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDestroys(Destroys object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Changes From</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Changes From</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangesFrom(ChangesFrom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Changes To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Changes To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangesTo(ChangesTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Creates</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Creates</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreates(Creates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mediates</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mediates</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMediates(Mediates object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgent(Agent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActionEvent(ActionEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nominal Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nominal Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNominalEvent(NominalEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OLDASSwitch
