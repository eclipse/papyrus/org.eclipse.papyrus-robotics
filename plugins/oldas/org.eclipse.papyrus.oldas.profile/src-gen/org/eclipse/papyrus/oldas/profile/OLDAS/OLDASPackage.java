/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASFactory
 * @model kind="package"
 * @generated
 */
public interface OLDASPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "OLDAS";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/oldas";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "OLDAS";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OLDASPackage eINSTANCE = org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.EndurantClassImpl <em>Endurant Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.EndurantClassImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getEndurantClass()
	 * @generated
	 */
	int ENDURANT_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDURANT_CLASS__BASE_CLASS = 0;

	/**
	 * The number of structural features of the '<em>Endurant Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDURANT_CLASS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Endurant Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENDURANT_CLASS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.RelatorImpl <em>Relator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.RelatorImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getRelator()
	 * @generated
	 */
	int RELATOR = 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATOR__BASE_CLASS = ENDURANT_CLASS__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Relator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATOR_FEATURE_COUNT = ENDURANT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Relator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATOR_OPERATION_COUNT = ENDURANT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CategoryImpl <em>Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CategoryImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCategory()
	 * @generated
	 */
	int CATEGORY = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__BASE_CLASS = ENDURANT_CLASS__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_COUNT = ENDURANT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_OPERATION_COUNT = ENDURANT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.PropertyImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 3;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__BASE_CLASS = ENDURANT_CLASS__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = ENDURANT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = ENDURANT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MetricImpl <em>Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MetricImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMetric()
	 * @generated
	 */
	int METRIC = 4;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__BASE_CLASS = ENDURANT_CLASS__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_FEATURE_COUNT = ENDURANT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OPERATION_COUNT = ENDURANT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.KindImpl <em>Kind</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.KindImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getKind()
	 * @generated
	 */
	int KIND = 5;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KIND__BASE_CLASS = ENDURANT_CLASS__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Kind</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KIND_FEATURE_COUNT = ENDURANT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Kind</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KIND_OPERATION_COUNT = ENDURANT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AntiRigidSortalImpl <em>Anti Rigid Sortal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AntiRigidSortalImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAntiRigidSortal()
	 * @generated
	 */
	int ANTI_RIGID_SORTAL = 6;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANTI_RIGID_SORTAL__BASE_CLASS = ENDURANT_CLASS__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Anti Rigid Sortal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANTI_RIGID_SORTAL_FEATURE_COUNT = ENDURANT_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Anti Rigid Sortal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANTI_RIGID_SORTAL_OPERATION_COUNT = ENDURANT_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldImpl <em>World</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getWorld()
	 * @generated
	 */
	int WORLD = 7;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD__BASE_CLASS = KIND__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>World</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_FEATURE_COUNT = KIND_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>World</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_OPERATION_COUNT = KIND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldFeatureImpl <em>World Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldFeatureImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getWorldFeature()
	 * @generated
	 */
	int WORLD_FEATURE = 8;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_FEATURE__BASE_CLASS = KIND__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>World Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_FEATURE_FEATURE_COUNT = KIND_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>World Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORLD_FEATURE_OPERATION_COUNT = KIND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AutomationFeatureImpl <em>Automation Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AutomationFeatureImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAutomationFeature()
	 * @generated
	 */
	int AUTOMATION_FEATURE = 9;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATION_FEATURE__BASE_CLASS = WORLD_FEATURE__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Automation Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATION_FEATURE_FEATURE_COUNT = WORLD_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Automation Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATION_FEATURE_OPERATION_COUNT = WORLD_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.OperatingFeatureImpl <em>Operating Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OperatingFeatureImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getOperatingFeature()
	 * @generated
	 */
	int OPERATING_FEATURE = 10;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATING_FEATURE__BASE_CLASS = WORLD_FEATURE__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Operating Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATING_FEATURE_FEATURE_COUNT = WORLD_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Operating Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATING_FEATURE_OPERATION_COUNT = WORLD_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.StateImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getState()
	 * @generated
	 */
	int STATE = 11;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__BASE_CLASS = ANTI_RIGID_SORTAL__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = ANTI_RIGID_SORTAL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = ANTI_RIGID_SORTAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.RoleImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 12;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__BASE_CLASS = ANTI_RIGID_SORTAL__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = ANTI_RIGID_SORTAL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = ANTI_RIGID_SORTAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AbstractEventImpl <em>Abstract Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AbstractEventImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAbstractEvent()
	 * @generated
	 */
	int ABSTRACT_EVENT = 13;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT__BASE_CLASS = 0;

	/**
	 * The number of structural features of the '<em>Abstract Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Abstract Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.TriggeringEventImpl <em>Triggering Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.TriggeringEventImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getTriggeringEvent()
	 * @generated
	 */
	int TRIGGERING_EVENT = 14;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERING_EVENT__BASE_CLASS = ABSTRACT_EVENT__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Triggering Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERING_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Triggering Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGERING_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MalfunctionEventImpl <em>Malfunction Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MalfunctionEventImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMalfunctionEvent()
	 * @generated
	 */
	int MALFUNCTION_EVENT = 15;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MALFUNCTION_EVENT__BASE_CLASS = ABSTRACT_EVENT__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Malfunction Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MALFUNCTION_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Malfunction Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MALFUNCTION_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MishapEventImpl <em>Mishap Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MishapEventImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMishapEvent()
	 * @generated
	 */
	int MISHAP_EVENT = 16;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISHAP_EVENT__BASE_CLASS = ABSTRACT_EVENT__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Mishap Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISHAP_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Mishap Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISHAP_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CausesImpl <em>Causes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CausesImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCauses()
	 * @generated
	 */
	int CAUSES = 17;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Causes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Causes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ParticipatesImpl <em>Participates</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ParticipatesImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getParticipates()
	 * @generated
	 */
	int PARTICIPATES = 18;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPATES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Participates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPATES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Participates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPATES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CharacterizesImpl <em>Characterizes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CharacterizesImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCharacterizes()
	 * @generated
	 */
	int CHARACTERIZES = 19;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERIZES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Characterizes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERIZES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Characterizes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTERIZES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.StructuresImpl <em>Structures</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.StructuresImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getStructures()
	 * @generated
	 */
	int STRUCTURES = 20;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Structures</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Structures</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.GeneratesImpl <em>Generates</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.GeneratesImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getGenerates()
	 * @generated
	 */
	int GENERATES = 21;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Generates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Generates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.DestroysImpl <em>Destroys</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.DestroysImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getDestroys()
	 * @generated
	 */
	int DESTROYS = 22;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESTROYS__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Destroys</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESTROYS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Destroys</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESTROYS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesFromImpl <em>Changes From</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesFromImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getChangesFrom()
	 * @generated
	 */
	int CHANGES_FROM = 23;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGES_FROM__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Changes From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGES_FROM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Changes From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGES_FROM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesToImpl <em>Changes To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesToImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getChangesTo()
	 * @generated
	 */
	int CHANGES_TO = 24;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGES_TO__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Changes To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGES_TO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Changes To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGES_TO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CreatesImpl <em>Creates</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CreatesImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCreates()
	 * @generated
	 */
	int CREATES = 25;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Creates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Creates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MediatesImpl <em>Mediates</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MediatesImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMediates()
	 * @generated
	 */
	int MEDIATES = 26;

	/**
	 * The feature id for the '<em><b>Base Association</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIATES__BASE_ASSOCIATION = 0;

	/**
	 * The number of structural features of the '<em>Mediates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIATES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Mediates</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIATES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AgentImpl <em>Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AgentImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAgent()
	 * @generated
	 */
	int AGENT = 27;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__BASE_CLASS = WORLD_FEATURE__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_FEATURE_COUNT = WORLD_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_OPERATION_COUNT = WORLD_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ActionEventImpl <em>Action Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ActionEventImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getActionEvent()
	 * @generated
	 */
	int ACTION_EVENT = 28;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_EVENT__BASE_CLASS = ABSTRACT_EVENT__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Action Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Action Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.NominalEventImpl <em>Nominal Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.NominalEventImpl
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getNominalEvent()
	 * @generated
	 */
	int NOMINAL_EVENT = 29;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOMINAL_EVENT__BASE_CLASS = ABSTRACT_EVENT__BASE_CLASS;

	/**
	 * The number of structural features of the '<em>Nominal Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOMINAL_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Nominal Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOMINAL_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass <em>Endurant Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Endurant Class</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass
	 * @generated
	 */
	EClass getEndurantClass();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass#getBase_Class()
	 * @see #getEndurantClass()
	 * @generated
	 */
	EReference getEndurantClass_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Relator <em>Relator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relator</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Relator
	 * @generated
	 */
	EClass getRelator();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Category <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Category
	 * @generated
	 */
	EClass getCategory();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Metric <em>Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metric</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Metric
	 * @generated
	 */
	EClass getMetric();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Kind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Kind
	 * @generated
	 */
	EClass getKind();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal <em>Anti Rigid Sortal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Anti Rigid Sortal</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal
	 * @generated
	 */
	EClass getAntiRigidSortal();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.World <em>World</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>World</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.World
	 * @generated
	 */
	EClass getWorld();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature <em>World Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>World Feature</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature
	 * @generated
	 */
	EClass getWorldFeature();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.AutomationFeature <em>Automation Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Automation Feature</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.AutomationFeature
	 * @generated
	 */
	EClass getAutomationFeature();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.OperatingFeature <em>Operating Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operating Feature</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OperatingFeature
	 * @generated
	 */
	EClass getOperatingFeature();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent <em>Abstract Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Event</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent
	 * @generated
	 */
	EClass getAbstractEvent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent#getBase_Class()
	 * @see #getAbstractEvent()
	 * @generated
	 */
	EReference getAbstractEvent_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent <em>Triggering Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triggering Event</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent
	 * @generated
	 */
	EClass getTriggeringEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent <em>Malfunction Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Malfunction Event</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent
	 * @generated
	 */
	EClass getMalfunctionEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent <em>Mishap Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mishap Event</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent
	 * @generated
	 */
	EClass getMishapEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Causes <em>Causes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Causes</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Causes
	 * @generated
	 */
	EClass getCauses();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Causes#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Causes#getBase_Association()
	 * @see #getCauses()
	 * @generated
	 */
	EReference getCauses_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Participates <em>Participates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Participates</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Participates
	 * @generated
	 */
	EClass getParticipates();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Participates#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Participates#getBase_Association()
	 * @see #getParticipates()
	 * @generated
	 */
	EReference getParticipates_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes <em>Characterizes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Characterizes</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes
	 * @generated
	 */
	EClass getCharacterizes();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Characterizes#getBase_Association()
	 * @see #getCharacterizes()
	 * @generated
	 */
	EReference getCharacterizes_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Structures <em>Structures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structures</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Structures
	 * @generated
	 */
	EClass getStructures();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Structures#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Structures#getBase_Association()
	 * @see #getStructures()
	 * @generated
	 */
	EReference getStructures_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Generates <em>Generates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generates</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Generates
	 * @generated
	 */
	EClass getGenerates();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Generates#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Generates#getBase_Association()
	 * @see #getGenerates()
	 * @generated
	 */
	EReference getGenerates_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Destroys <em>Destroys</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Destroys</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Destroys
	 * @generated
	 */
	EClass getDestroys();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Destroys#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Destroys#getBase_Association()
	 * @see #getDestroys()
	 * @generated
	 */
	EReference getDestroys_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.ChangesFrom <em>Changes From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Changes From</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.ChangesFrom
	 * @generated
	 */
	EClass getChangesFrom();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.ChangesFrom#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.ChangesFrom#getBase_Association()
	 * @see #getChangesFrom()
	 * @generated
	 */
	EReference getChangesFrom_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.ChangesTo <em>Changes To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Changes To</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.ChangesTo
	 * @generated
	 */
	EClass getChangesTo();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.ChangesTo#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.ChangesTo#getBase_Association()
	 * @see #getChangesTo()
	 * @generated
	 */
	EReference getChangesTo_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Creates <em>Creates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Creates</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Creates
	 * @generated
	 */
	EClass getCreates();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Creates#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Creates#getBase_Association()
	 * @see #getCreates()
	 * @generated
	 */
	EReference getCreates_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Mediates <em>Mediates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mediates</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Mediates
	 * @generated
	 */
	EClass getMediates();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Mediates#getBase_Association <em>Base Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Association</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Mediates#getBase_Association()
	 * @see #getMediates()
	 * @generated
	 */
	EReference getMediates_Base_Association();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.Agent
	 * @generated
	 */
	EClass getAgent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.ActionEvent <em>Action Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Event</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.ActionEvent
	 * @generated
	 */
	EClass getActionEvent();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.oldas.profile.OLDAS.NominalEvent <em>Nominal Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nominal Event</em>'.
	 * @see org.eclipse.papyrus.oldas.profile.OLDAS.NominalEvent
	 * @generated
	 */
	EClass getNominalEvent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OLDASFactory getOLDASFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.EndurantClassImpl <em>Endurant Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.EndurantClassImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getEndurantClass()
		 * @generated
		 */
		EClass ENDURANT_CLASS = eINSTANCE.getEndurantClass();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENDURANT_CLASS__BASE_CLASS = eINSTANCE.getEndurantClass_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.RelatorImpl <em>Relator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.RelatorImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getRelator()
		 * @generated
		 */
		EClass RELATOR = eINSTANCE.getRelator();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CategoryImpl <em>Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CategoryImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCategory()
		 * @generated
		 */
		EClass CATEGORY = eINSTANCE.getCategory();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.PropertyImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MetricImpl <em>Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MetricImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMetric()
		 * @generated
		 */
		EClass METRIC = eINSTANCE.getMetric();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.KindImpl <em>Kind</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.KindImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getKind()
		 * @generated
		 */
		EClass KIND = eINSTANCE.getKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AntiRigidSortalImpl <em>Anti Rigid Sortal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AntiRigidSortalImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAntiRigidSortal()
		 * @generated
		 */
		EClass ANTI_RIGID_SORTAL = eINSTANCE.getAntiRigidSortal();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldImpl <em>World</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getWorld()
		 * @generated
		 */
		EClass WORLD = eINSTANCE.getWorld();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldFeatureImpl <em>World Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.WorldFeatureImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getWorldFeature()
		 * @generated
		 */
		EClass WORLD_FEATURE = eINSTANCE.getWorldFeature();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AutomationFeatureImpl <em>Automation Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AutomationFeatureImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAutomationFeature()
		 * @generated
		 */
		EClass AUTOMATION_FEATURE = eINSTANCE.getAutomationFeature();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.OperatingFeatureImpl <em>Operating Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OperatingFeatureImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getOperatingFeature()
		 * @generated
		 */
		EClass OPERATING_FEATURE = eINSTANCE.getOperatingFeature();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.StateImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.RoleImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AbstractEventImpl <em>Abstract Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AbstractEventImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAbstractEvent()
		 * @generated
		 */
		EClass ABSTRACT_EVENT = eINSTANCE.getAbstractEvent();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_EVENT__BASE_CLASS = eINSTANCE.getAbstractEvent_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.TriggeringEventImpl <em>Triggering Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.TriggeringEventImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getTriggeringEvent()
		 * @generated
		 */
		EClass TRIGGERING_EVENT = eINSTANCE.getTriggeringEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MalfunctionEventImpl <em>Malfunction Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MalfunctionEventImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMalfunctionEvent()
		 * @generated
		 */
		EClass MALFUNCTION_EVENT = eINSTANCE.getMalfunctionEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MishapEventImpl <em>Mishap Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MishapEventImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMishapEvent()
		 * @generated
		 */
		EClass MISHAP_EVENT = eINSTANCE.getMishapEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CausesImpl <em>Causes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CausesImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCauses()
		 * @generated
		 */
		EClass CAUSES = eINSTANCE.getCauses();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAUSES__BASE_ASSOCIATION = eINSTANCE.getCauses_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ParticipatesImpl <em>Participates</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ParticipatesImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getParticipates()
		 * @generated
		 */
		EClass PARTICIPATES = eINSTANCE.getParticipates();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTICIPATES__BASE_ASSOCIATION = eINSTANCE.getParticipates_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CharacterizesImpl <em>Characterizes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CharacterizesImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCharacterizes()
		 * @generated
		 */
		EClass CHARACTERIZES = eINSTANCE.getCharacterizes();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHARACTERIZES__BASE_ASSOCIATION = eINSTANCE.getCharacterizes_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.StructuresImpl <em>Structures</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.StructuresImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getStructures()
		 * @generated
		 */
		EClass STRUCTURES = eINSTANCE.getStructures();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURES__BASE_ASSOCIATION = eINSTANCE.getStructures_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.GeneratesImpl <em>Generates</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.GeneratesImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getGenerates()
		 * @generated
		 */
		EClass GENERATES = eINSTANCE.getGenerates();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATES__BASE_ASSOCIATION = eINSTANCE.getGenerates_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.DestroysImpl <em>Destroys</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.DestroysImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getDestroys()
		 * @generated
		 */
		EClass DESTROYS = eINSTANCE.getDestroys();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESTROYS__BASE_ASSOCIATION = eINSTANCE.getDestroys_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesFromImpl <em>Changes From</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesFromImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getChangesFrom()
		 * @generated
		 */
		EClass CHANGES_FROM = eINSTANCE.getChangesFrom();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGES_FROM__BASE_ASSOCIATION = eINSTANCE.getChangesFrom_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesToImpl <em>Changes To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ChangesToImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getChangesTo()
		 * @generated
		 */
		EClass CHANGES_TO = eINSTANCE.getChangesTo();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGES_TO__BASE_ASSOCIATION = eINSTANCE.getChangesTo_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.CreatesImpl <em>Creates</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.CreatesImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getCreates()
		 * @generated
		 */
		EClass CREATES = eINSTANCE.getCreates();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CREATES__BASE_ASSOCIATION = eINSTANCE.getCreates_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.MediatesImpl <em>Mediates</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.MediatesImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getMediates()
		 * @generated
		 */
		EClass MEDIATES = eINSTANCE.getMediates();

		/**
		 * The meta object literal for the '<em><b>Base Association</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEDIATES__BASE_ASSOCIATION = eINSTANCE.getMediates_Base_Association();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.AgentImpl <em>Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.AgentImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getAgent()
		 * @generated
		 */
		EClass AGENT = eINSTANCE.getAgent();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.ActionEventImpl <em>Action Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.ActionEventImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getActionEvent()
		 * @generated
		 */
		EClass ACTION_EVENT = eINSTANCE.getActionEvent();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.oldas.profile.OLDAS.impl.NominalEventImpl <em>Nominal Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.NominalEventImpl
		 * @see org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASPackageImpl#getNominalEvent()
		 * @generated
		 */
		EClass NOMINAL_EVENT = eINSTANCE.getNominalEvent();

	}

} //OLDASPackage
