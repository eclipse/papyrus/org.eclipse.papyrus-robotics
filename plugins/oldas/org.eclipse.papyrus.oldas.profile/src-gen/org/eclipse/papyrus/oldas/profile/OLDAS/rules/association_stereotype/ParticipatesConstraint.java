package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Participates;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ParticipatesConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association participatesAssociation = (Association) ctx.getTarget();
		Participates participatesStereotype = UMLUtil.getStereotypeApplication(participatesAssociation, Participates.class);
		if(participatesStereotype == null) {
			return null;
		}
		for (org.eclipse.uml2.uml.Property memberEnd: participatesAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class) == null && !memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), AbstractEvent.class) == null && memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «Participates» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must connect an endurant to an event.");
			}
			
			if(memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «Participates» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a source multiplicity in the range  [1..*] .");
			}
			
			if(!memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «Participates» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a target multiplicity in the range  [1..*] .");
			}
				
		}
		return null;
	}
}