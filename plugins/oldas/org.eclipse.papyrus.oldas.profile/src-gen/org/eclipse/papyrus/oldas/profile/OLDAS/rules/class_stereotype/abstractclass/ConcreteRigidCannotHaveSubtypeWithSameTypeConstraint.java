package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.abstractclass;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Kind;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class ConcreteRigidCannotHaveSubtypeWithSameTypeConstraint extends AbstractModelConstraint{
	@Override
	public IStatus validate(IValidationContext ctx) {
		Class rigidClass = (Class) ctx.getTarget();
		if(UMLUtil.getStereotypeApplication(rigidClass, Kind.class) == null && 
				UMLUtil.getStereotypeApplication(rigidClass, Relator.class) == null && 
				UMLUtil.getStereotypeApplication(rigidClass, Property.class) == null && 
				UMLUtil.getStereotypeApplication(rigidClass, Metric.class) == null ) {
			return null;
		}

		if(!hasADirectOrIndirectConcreteSubtypeWithSameType(rigidClass)) {
			return null;
		}
		return ctx.createFailureStatus(rigidClass.getName()+" class stereotyped as "+rigidClass.getAppliedStereotypes().get(0).getName()+" cannot have a concrete subtype with the same stereotype.");
	}
	
	private boolean hasADirectOrIndirectConcreteSubtypeWithSameType(Class rigidClass) {
		String rigidClassStereotypeName = rigidClass.getAppliedStereotypes().get(0).getName();
		for(Relationship generalization:rigidClass.getRelationships()) {
			if(generalization instanceof Generalization) {
				Class subtypeClass = (Class) ((Generalization) generalization).getSpecific();
				if(subtypeClass == rigidClass) {//prevent from looping
					continue;
				}
				if(subtypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
					return true;
				}
				String subtypeStereotypeName = subtypeClass.getAppliedStereotypes().get(0).getName();
				if(subtypeStereotypeName.equals(rigidClassStereotypeName)){
					if(!subtypeClass.isAbstract()) {
						return true;
					}
					else {
						return hasADirectOrIndirectConcreteSubtypeWithSameType(subtypeClass);
					}
				}
			}
		}
		return false;
	}

}
