package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Structures;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.util.UMLUtil;

public class StructuresConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association structuresAssociation = (Association) ctx.getTarget();
		Structures structuresStereotype = UMLUtil.getStereotypeApplication(structuresAssociation, Structures.class);
		if(structuresStereotype == null) {
			return null;
		}
		for (org.eclipse.uml2.uml.Property memberEnd: structuresAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), Metric.class) == null && !memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), Property.class) == null && memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «Structures» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must connect a «Metric» to a «Property».");
			}
			
			if(memberEnd.getLower() != 1 || memberEnd.getUpper() != 1) {
				return ctx.createFailureStatus("The «Structures» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a "+ (memberEnd.isNavigable()?"source":"target")
						+" multiplicity equal to 1.");
			}
				
		}
		return null;
	}
}
