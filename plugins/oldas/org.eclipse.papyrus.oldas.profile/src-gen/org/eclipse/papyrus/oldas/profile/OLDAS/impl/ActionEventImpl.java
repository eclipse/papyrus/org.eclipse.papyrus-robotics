/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.ActionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ActionEventImpl extends AbstractEventImpl implements ActionEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.ACTION_EVENT;
	}

} //ActionEventImpl
