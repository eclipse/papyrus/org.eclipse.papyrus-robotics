package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.category;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Category;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CategoryAbstractConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class categoryClass = (Class) ctx.getTarget();
		Category categoryStereotype = UMLUtil.getStereotypeApplication(categoryClass, Category.class);
		if(categoryStereotype == null || categoryClass.isAbstract()){
			return null;
		}
		String stereotypeName = categoryClass.getAppliedStereotypes().get(0).getName();
		return ctx.createFailureStatus(categoryClass.getName()+" class stereotyped as "+stereotypeName+" must be abstract");
	}
}
