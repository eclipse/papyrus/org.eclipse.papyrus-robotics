package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.relator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Mediates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class RelatorMustBeConnectedToEndurantConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class relatorClass = (Class) ctx.getTarget();
		Relator relatorStereotype = UMLUtil.getStereotypeApplication(relatorClass, Relator.class);
		if(relatorStereotype == null){
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToEndurant(relatorClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(relatorClass.getName()+" class stereotyped as "+relatorClass.getAppliedStereotypes().get(0).getName()+" must be connected through at least one <<Characterizes>> relation to an non-event class");

	}

	private boolean directlyOrIndirectlyConnectedToEndurant(Class relatorClass) {
		for(Association association: relatorClass.getAssociations()) {
			Mediates mediatesStereotype = UMLUtil.getStereotypeApplication(association, Mediates.class);
			if(mediatesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					EndurantClass endurantStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class);
					boolean isEndurant = endurantStereotype != null;
					if(memberEnd.getType()!=relatorClass && isEndurant && memberEnd.isNavigable()) {
						return true;
					}
				}
			}
		}//direclty connected
		
		for(Classifier parent:relatorClass.parents()) {
			if(directlyOrIndirectlyConnectedToEndurant((Class) parent)) {
				return true;
			}
		}//indirectly connected
		
		return false;
	}

}
