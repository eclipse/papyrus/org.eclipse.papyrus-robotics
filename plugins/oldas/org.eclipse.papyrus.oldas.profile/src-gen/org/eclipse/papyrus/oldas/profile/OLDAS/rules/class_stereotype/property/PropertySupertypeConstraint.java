package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.property;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal;
import org.eclipse.papyrus.oldas.profile.OLDAS.Kind;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class PropertySupertypeConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class propertyClass = (Class) ctx.getTarget();
		Property propertyStereotype = UMLUtil.getStereotypeApplication(propertyClass, Property.class);
		if(propertyStereotype == null){
			return null;
		}
		
		String wrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(propertyClass);
		if(wrongSupertypeStereotypeName != null) {
			String stereotypeName = propertyClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(propertyClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSupertypeStereotypeName+" as a direct or indirect super-type.");
		}
		return null;
	}
	
	private String getWrongDirectOrIndirectSupertype(Classifier oldasClass) {
		for(Classifier supertypeClass: oldasClass.getGenerals()) {
			if(supertypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
				return null;
			}
			String supertypeStereotypeName = supertypeClass.getAppliedStereotypes().get(0).getName();
			if( UMLUtil.getStereotypeApplication(supertypeClass, AntiRigidSortal.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, Relator.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, Kind.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, Metric.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, AbstractEvent.class) != null){
				return supertypeStereotypeName;
			}// direct wrong supertype
			String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(supertypeClass);
			if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
				return indirectWrongSupertypeStereotypeName;
			}
		}
		return null;
	}

}
