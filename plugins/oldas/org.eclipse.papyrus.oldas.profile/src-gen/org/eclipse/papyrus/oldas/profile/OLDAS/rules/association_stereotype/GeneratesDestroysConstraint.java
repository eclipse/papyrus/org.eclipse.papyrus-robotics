package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Destroys;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Generates;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.util.UMLUtil;

public class GeneratesDestroysConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association generatesDestroysAssociation = (Association) ctx.getTarget();
		Generates generatesStereotype = UMLUtil.getStereotypeApplication(generatesDestroysAssociation, Generates.class);
		Destroys destroysStereotype = UMLUtil.getStereotypeApplication(generatesDestroysAssociation, Destroys.class);

		String stereotypeName;
		if(generatesStereotype != null) {
			stereotypeName = "Generates";
		}
		else if(destroysStereotype != null){
			stereotypeName = "Destroys";
		}
		else {
			return null;
		}
		
		for (org.eclipse.uml2.uml.Property memberEnd: generatesDestroysAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class) == null && memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), AbstractEvent.class) == null && !memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «"+stereotypeName+"» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must be connected from an event to an endurant.");
			}
			
			if(memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «"+stereotypeName+"» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a target multiplicity in the range  1..* .");
			}
			
			if(!memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «"+stereotypeName+"» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a source multiplicity in the range  1..* .");
			}
				
		}
		return null;
	}
}