/*****************************************************************************
 * Copyright (c) 2012 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are the property of the CEA. 
 * Any use is subject to specific agreement with the CEA.
 *
 * Contributors:
 * 
 * 		CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.oldas.profile.OLDAS;

import org.eclipse.emf.validation.model.IClientSelector;
import org.eclipse.uml2.uml.Element;

public class ValidationDelegateClientSelector implements IClientSelector {

	public boolean selects(Object object) {
		return (object instanceof Element);
	}
}
