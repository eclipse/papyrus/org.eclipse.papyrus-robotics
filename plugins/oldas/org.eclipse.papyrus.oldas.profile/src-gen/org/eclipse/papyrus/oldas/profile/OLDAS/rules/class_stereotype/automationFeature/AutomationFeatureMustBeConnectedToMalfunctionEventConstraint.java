package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.automationFeature;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AutomationFeature;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Participates;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AutomationFeatureMustBeConnectedToMalfunctionEventConstraint  extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class automationFeatureClass = (Class) ctx.getTarget();
		AutomationFeature automationFeatureStereotype = UMLUtil.getStereotypeApplication(automationFeatureClass, AutomationFeature.class);
		if(automationFeatureStereotype == null) {
			return null;
		}
		
		//System.out.println("LOG OperatingFeatureMustBeConnectedToTriggeringEventConstraint from Model "+operatingfeatureClass.getModel().getName()+", Class:"+operatingfeatureClass.getName()+", Associations:"+operatingfeatureClass.getAssociations().size());
		for(Association association: automationFeatureClass.getAssociations()) {
			//System.out.println("LOG OperatingFeatureMustBeConnectedToTriggeringEventConstraint from Model "+operatingfeatureClass.getModel().getName()+", Class:"+operatingfeatureClass.getName()+", Association name:"+association.getAppliedStereotypes());
			Participates participatesStereotype = UMLUtil.getStereotypeApplication(association, Participates.class);
			if(participatesStereotype != null) {
				//System.out.println("LOG OperatingFeatureMustBeConnectedToTriggeringEventConstraint Participates association, navigable owned ends:"+association.getMemberEnds().size()+" TEST directed:"+(association instanceof DirectedRelationship));
				for (Property memberEnd: association.getMemberEnds()) {
					//System.out.println("LOG OperatingFeatureMustBeConnectedToTriggeringEventConstraint Navigable member end");
					MalfunctionEvent malfunctionEventStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), MalfunctionEvent.class);
					if(malfunctionEventStereotype != null && memberEnd.isNavigable()) {
						return null;
					}
				}
			}
			//System.out.println("LOG OperatingFeatureMustBeConnectedToTriggeringEventConstraint Participates relation:"+relationship.getAppliedStereotype("model::Participates"));//+" ,target stereotype:"+relationship.getTargets().get(0).getAppliedStereotype("model::TriggeringEvent"));

		}
		return ctx.createFailureStatus(automationFeatureClass.getName()+" class stereotyped as "+automationFeatureClass.getAppliedStereotypes().get(0).getName()+" must be connected through at least one <<Participates>> relation to a <<MalfunctionEvent>> class");
	}
}