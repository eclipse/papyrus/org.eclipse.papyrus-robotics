/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Kind</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#getKind()
 * @model abstract="true"
 * @generated
 */
public interface Kind extends EndurantClass {
} // Kind
