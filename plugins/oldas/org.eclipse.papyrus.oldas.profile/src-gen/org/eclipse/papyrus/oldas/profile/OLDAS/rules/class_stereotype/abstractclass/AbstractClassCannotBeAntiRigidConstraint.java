package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.abstractclass;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AbstractClassCannotBeAntiRigidConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		// Equivalent ontoUML: 
		//Because it is a rigid type, a «Subkind» cannot have an anti-rigid type («Role», «Phase», «RoleMixin») as an ancestor.
		Class antirigidsortalClass = (Class) ctx.getTarget();
		AntiRigidSortal antiRigidSortalStereotype = UMLUtil.getStereotypeApplication(antirigidsortalClass, AntiRigidSortal.class);
		if(antiRigidSortalStereotype == null) {
			return null;
		}
		if(antirigidsortalClass.isAbstract()) {
			return ctx.createFailureStatus(antirigidsortalClass.getName()+" class stereotyped as "+antirigidsortalClass.getAppliedStereotypes().get(0).getName()+" must be non-abstract.");
		}
		return null;
	}

}
