package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.ActionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Agent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Creates;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CreatesConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association createsAssociation = (Association) ctx.getTarget();
		Creates createsStereotype = UMLUtil.getStereotypeApplication(createsAssociation, Creates.class);
		if(createsStereotype == null) {
			return null;
		}
		for (org.eclipse.uml2.uml.Property memberEnd: createsAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), Agent.class) == null && !memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), ActionEvent.class) == null && memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «Creates» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must connect an «Agent» to an «ActionEvent».");
			}
			
			if(!memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «Creates» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a target multiplicity in the range  [1..*] .");
			}
		}
		return null;
	}
}
		