/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Malfunction Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MalfunctionEventImpl extends AbstractEventImpl implements MalfunctionEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MalfunctionEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.MALFUNCTION_EVENT;
	}

} //MalfunctionEventImpl
