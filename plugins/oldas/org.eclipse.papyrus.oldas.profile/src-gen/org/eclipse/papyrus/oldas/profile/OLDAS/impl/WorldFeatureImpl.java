/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;
import org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>World Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class WorldFeatureImpl extends KindImpl implements WorldFeature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorldFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.WORLD_FEATURE;
	}

} //WorldFeatureImpl
