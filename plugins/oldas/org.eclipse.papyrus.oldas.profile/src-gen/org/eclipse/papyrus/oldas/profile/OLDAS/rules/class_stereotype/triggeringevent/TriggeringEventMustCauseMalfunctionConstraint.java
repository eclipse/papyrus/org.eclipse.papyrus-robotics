package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.triggeringevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class TriggeringEventMustCauseMalfunctionConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class triggeringEventClass = (Class) ctx.getTarget();
		TriggeringEvent triggeringEventStereotype = UMLUtil.getStereotypeApplication(triggeringEventClass, TriggeringEvent.class);
		if(triggeringEventStereotype == null){
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToMalfunctionEvent(triggeringEventClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(triggeringEventClass.getName()+" class stereotyped as TriggeringEvent must be"
				+ " connected through at least one «Causes» relation to a «MalfunctionEvent» class");

	}

	private boolean directlyOrIndirectlyConnectedToMalfunctionEvent(Class triggeringEventClass) {
		for(Association association: triggeringEventClass.getAssociations()) {
			Causes causesStereotype = UMLUtil.getStereotypeApplication(association, Causes.class);
			if(causesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != triggeringEventClass 
							&& memberEnd.isNavigable()) {
						if( UMLUtil.getStereotypeApplication(memberEnd.getType(), MalfunctionEvent.class) != null) {
							return true;
						}// directly connected
						if(directlyOrIndirectlyConnectedToMalfunctionEvent((Class) memberEnd.getType())) {
							return true;
						} // indirectly connected through a caused event
					}
				}
			}
		}
	
		for(Classifier parent:triggeringEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToMalfunctionEvent((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		
		return false;
	}
}
