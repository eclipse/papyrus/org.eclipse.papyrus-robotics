package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.metric;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Structures;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MetricMustBeConnectedToPropertyConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class metricClass = (Class) ctx.getTarget();
		Metric metricStereotype = UMLUtil.getStereotypeApplication(metricClass, Metric.class);
		if(metricStereotype == null){
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToEndurant(metricClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(metricClass.getName()+" class stereotyped as "+metricClass.getAppliedStereotypes().get(0).getName()+" must be connected through at least one <<Structures>> relation to a <<Property>> class");

	}

	private boolean directlyOrIndirectlyConnectedToEndurant(Class metricClass) {
		for(Association association: metricClass.getAssociations()) {
			Structures structuresStereotype = UMLUtil.getStereotypeApplication(association, Structures.class);
			if(structuresStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					Property propertyStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), Property.class);
					if(memberEnd.getType() != metricClass && propertyStereotype != null && memberEnd.isNavigable()) {
						return true;
					}
				}
			}
		}//direclty connected
	
		for(Classifier parent:metricClass.parents()) {
			if(directlyOrIndirectlyConnectedToEndurant((Class) parent)) {
				return true;
			}
		}//indirectly connected
		
		return false;
	}

}
