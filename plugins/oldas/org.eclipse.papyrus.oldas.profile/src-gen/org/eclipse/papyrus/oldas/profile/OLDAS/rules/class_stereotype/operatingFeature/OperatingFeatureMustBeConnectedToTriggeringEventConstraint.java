package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.operatingFeature;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.OperatingFeature;
import org.eclipse.papyrus.oldas.profile.OLDAS.Participates;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class OperatingFeatureMustBeConnectedToTriggeringEventConstraint  extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class operatingFeatureClass = (Class) ctx.getTarget();
		OperatingFeature operatingFeatureStereotype = UMLUtil.getStereotypeApplication(operatingFeatureClass, OperatingFeature.class);
		if(operatingFeatureStereotype == null) {
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToTriggeringEvent(operatingFeatureClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(operatingFeatureClass.getName()+" class stereotyped as "+operatingFeatureClass.getAppliedStereotypes().get(0).getName()+" must be connected through at least one <<Participates>> relation to a <<TriggeringEvent>> class");
	}

	private boolean directlyOrIndirectlyConnectedToTriggeringEvent(Class operatingFeatureClass) {
		for(Association association: operatingFeatureClass.getAssociations()) {

			Participates participatesStereotype = UMLUtil.getStereotypeApplication(association, Participates.class);
			if(participatesStereotype != null) {
				for (Property memberEnd: association.getMemberEnds()) {
					
					TriggeringEvent triggeringEventStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), TriggeringEvent.class);
					if(triggeringEventStereotype != null && memberEnd.isNavigable()) {
						return true;
					}
				}
			}
		} //directly connected
		
		for(Classifier parent:operatingFeatureClass.parents()) {
			if(directlyOrIndirectlyConnectedToTriggeringEvent((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		return false;
	}
}