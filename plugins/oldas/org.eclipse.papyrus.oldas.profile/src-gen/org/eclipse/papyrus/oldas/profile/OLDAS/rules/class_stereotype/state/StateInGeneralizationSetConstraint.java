package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.state;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.State;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.util.UMLUtil;

public class StateInGeneralizationSetConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class stateClass = (Class) ctx.getTarget();
		State stateStereotype = UMLUtil.getStereotypeApplication(stateClass, State.class);
		if(stateStereotype == null) {
			return null;
		}
		
		for(Generalization generalization: stateClass.getGeneralizations()) {
			if(generalization.getGeneralizationSets().size() != 1 
					|| generalization.getGeneralizationSets().get(0).getGeneralizations().size() < 2
					|| !generalization.getGeneralizationSets().get(0).isDisjoint()
					|| !generalization.getGeneralizationSets().get(0).isCovering()) {
				return ctx.createFailureStatus(stateClass.getName()+" class stereotyped must be a part of a generalization set disjoint and complete with at least two classes");
			}	
		}
		
		return null;
	}

}
