package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.worldFeature;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.papyrus.oldas.profile.OLDAS.Role;
import org.eclipse.papyrus.oldas.profile.OLDAS.State;
import org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class WorldFeatureSupertypeConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class worldFeatureClass = (Class) ctx.getTarget();
		WorldFeature worldFeatureStereotype = UMLUtil.getStereotypeApplication(worldFeatureClass, WorldFeature.class);
		if(worldFeatureStereotype == null) {
			return null;
		}
		String wrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(worldFeatureClass);
		if(wrongSupertypeStereotypeName != null) {
			String stereotypeName = worldFeatureClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(worldFeatureClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSupertypeStereotypeName+" as a direct or indirect super-type");
		}
		return null;
	}

	private String getWrongDirectOrIndirectSupertype(Classifier oldasClass) {
		for(Classifier supertypeClass: oldasClass.getGenerals()) {
			if(supertypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
				return null;
			}
			String supertypeStereotypeName = supertypeClass.getAppliedStereotypes().get(0).getName();
			if( UMLUtil.getStereotypeApplication(supertypeClass, Property.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, Relator.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, State.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, Role.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, Metric.class) != null ||
				UMLUtil.getStereotypeApplication(supertypeClass, AbstractEvent.class) != null){
				return supertypeStereotypeName;
			}// direct wrong supertype
			String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(supertypeClass);
			if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
				return indirectWrongSupertypeStereotypeName;
			}
		}
		return null;
	}

}
