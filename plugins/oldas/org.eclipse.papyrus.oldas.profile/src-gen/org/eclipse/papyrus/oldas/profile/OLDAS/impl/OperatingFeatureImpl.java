/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;
import org.eclipse.papyrus.oldas.profile.OLDAS.OperatingFeature;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operating Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OperatingFeatureImpl extends WorldFeatureImpl implements OperatingFeature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatingFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.OPERATING_FEATURE;
	}

} //OperatingFeatureImpl
