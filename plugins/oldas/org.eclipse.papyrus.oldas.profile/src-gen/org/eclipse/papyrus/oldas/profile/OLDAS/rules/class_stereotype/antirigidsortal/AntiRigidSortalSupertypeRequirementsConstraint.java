package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.antirigidsortal;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal;
import org.eclipse.papyrus.oldas.profile.OLDAS.Kind;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AntiRigidSortalSupertypeRequirementsConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class antirigidsortalClass = (Class) ctx.getTarget();
		AntiRigidSortal antiRigidSortalStereotype = UMLUtil.getStereotypeApplication(antirigidsortalClass, AntiRigidSortal.class);
		if(antiRigidSortalStereotype == null) {
			return null;
		}
		
		int nIdentityProvider = 0;
		if(antirigidsortalClass.parents().size() > 1) {
			String stereotypeName = antirigidsortalClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(antirigidsortalClass.getName()+" class stereotyped as "+stereotypeName+" must have maximum one «OperatingFeature», «Agent», «AutomationFeature», «Property», «Relator», «Metric» as its direct super-type.");
		}
		for(Classifier supertypeClass: antirigidsortalClass.allParents()) {
			if(supertypeClass.getAppliedStereotypes().size() == 0) {//invalid model
				return null;
			}
			if(UMLUtil.getStereotypeApplication(supertypeClass, Kind.class) != null ||
					UMLUtil.getStereotypeApplication(supertypeClass, Property.class) != null ||
					UMLUtil.getStereotypeApplication(supertypeClass, Relator.class) != null ||
					UMLUtil.getStereotypeApplication(supertypeClass, Metric.class) != null) {
				nIdentityProvider += 1;
			}
		}
		if(nIdentityProvider >= 1) {
			return null;
		}
		String stereotypeName = antirigidsortalClass.getAppliedStereotypes().get(0).getName();
		return ctx.createFailureStatus(antirigidsortalClass.getName()+" class stereotyped as "+stereotypeName+" must have at least one «OperatingFeature», «Agent», «AutomationFeature», «Property», «Relator», «Metric» as its direct or indirect super-type.");
		
	}

}
