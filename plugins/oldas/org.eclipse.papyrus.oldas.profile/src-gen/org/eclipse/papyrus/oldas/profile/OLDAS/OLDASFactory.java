/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage
 * @generated
 */
public interface OLDASFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OLDASFactory eINSTANCE = org.eclipse.papyrus.oldas.profile.OLDAS.impl.OLDASFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Relator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relator</em>'.
	 * @generated
	 */
	Relator createRelator();

	/**
	 * Returns a new object of class '<em>Category</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Category</em>'.
	 * @generated
	 */
	Category createCategory();

	/**
	 * Returns a new object of class '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property</em>'.
	 * @generated
	 */
	Property createProperty();

	/**
	 * Returns a new object of class '<em>Metric</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Metric</em>'.
	 * @generated
	 */
	Metric createMetric();

	/**
	 * Returns a new object of class '<em>World</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>World</em>'.
	 * @generated
	 */
	World createWorld();

	/**
	 * Returns a new object of class '<em>Automation Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Automation Feature</em>'.
	 * @generated
	 */
	AutomationFeature createAutomationFeature();

	/**
	 * Returns a new object of class '<em>Operating Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operating Feature</em>'.
	 * @generated
	 */
	OperatingFeature createOperatingFeature();

	/**
	 * Returns a new object of class '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State</em>'.
	 * @generated
	 */
	State createState();

	/**
	 * Returns a new object of class '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role</em>'.
	 * @generated
	 */
	Role createRole();

	/**
	 * Returns a new object of class '<em>Triggering Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Triggering Event</em>'.
	 * @generated
	 */
	TriggeringEvent createTriggeringEvent();

	/**
	 * Returns a new object of class '<em>Malfunction Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Malfunction Event</em>'.
	 * @generated
	 */
	MalfunctionEvent createMalfunctionEvent();

	/**
	 * Returns a new object of class '<em>Mishap Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mishap Event</em>'.
	 * @generated
	 */
	MishapEvent createMishapEvent();

	/**
	 * Returns a new object of class '<em>Causes</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Causes</em>'.
	 * @generated
	 */
	Causes createCauses();

	/**
	 * Returns a new object of class '<em>Participates</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Participates</em>'.
	 * @generated
	 */
	Participates createParticipates();

	/**
	 * Returns a new object of class '<em>Characterizes</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Characterizes</em>'.
	 * @generated
	 */
	Characterizes createCharacterizes();

	/**
	 * Returns a new object of class '<em>Structures</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Structures</em>'.
	 * @generated
	 */
	Structures createStructures();

	/**
	 * Returns a new object of class '<em>Generates</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generates</em>'.
	 * @generated
	 */
	Generates createGenerates();

	/**
	 * Returns a new object of class '<em>Destroys</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Destroys</em>'.
	 * @generated
	 */
	Destroys createDestroys();

	/**
	 * Returns a new object of class '<em>Changes From</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Changes From</em>'.
	 * @generated
	 */
	ChangesFrom createChangesFrom();

	/**
	 * Returns a new object of class '<em>Changes To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Changes To</em>'.
	 * @generated
	 */
	ChangesTo createChangesTo();

	/**
	 * Returns a new object of class '<em>Creates</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Creates</em>'.
	 * @generated
	 */
	Creates createCreates();

	/**
	 * Returns a new object of class '<em>Mediates</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mediates</em>'.
	 * @generated
	 */
	Mediates createMediates();

	/**
	 * Returns a new object of class '<em>Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent</em>'.
	 * @generated
	 */
	Agent createAgent();

	/**
	 * Returns a new object of class '<em>Action Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Event</em>'.
	 * @generated
	 */
	ActionEvent createActionEvent();

	/**
	 * Returns a new object of class '<em>Nominal Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nominal Event</em>'.
	 * @generated
	 */
	NominalEvent createNominalEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OLDASPackage getOLDASPackage();

} //OLDASFactory
