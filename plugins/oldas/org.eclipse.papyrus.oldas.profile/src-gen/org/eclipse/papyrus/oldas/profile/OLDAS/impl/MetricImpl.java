/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MetricImpl extends EndurantClassImpl implements Metric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.METRIC;
	}

} //MetricImpl
