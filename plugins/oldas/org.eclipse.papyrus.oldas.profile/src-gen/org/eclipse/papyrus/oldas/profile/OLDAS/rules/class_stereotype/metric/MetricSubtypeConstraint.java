package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.metric;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Category;
import org.eclipse.papyrus.oldas.profile.OLDAS.Kind;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MetricSubtypeConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class metricClass = (Class) ctx.getTarget();
		Metric metricStereotype = UMLUtil.getStereotypeApplication(metricClass, Metric.class);
		if(metricStereotype == null){
			return null;
		}
		String wrongSubtypeStereotypeName = getWrongDirectOrIndirectSubtype(metricClass);
		if(wrongSubtypeStereotypeName != null) {
			String stereotypeName = metricClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(metricClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSubtypeStereotypeName+" as a direct or indirect sub-type");
		}
		return null;
	}

	private String getWrongDirectOrIndirectSubtype(Classifier oldasClass) {
		//System.out.println("LOG WorldFeatureSubtypeConstraint Class:"+oldasClass.getName()+" n generalization:"+oldasClass.getGeneralizations().size());
		for(Relationship generalization:oldasClass.getRelationships()) {
			if(generalization instanceof Generalization) {
				Classifier subtypeClass = ((Generalization) generalization).getSpecific();
				if(subtypeClass == oldasClass) {//prevent from looping
					continue;
				}
				if(subtypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
					return null;
				}
				String subtypeStereotypeName = subtypeClass.getAppliedStereotypes().get(0).getName();
				if( UMLUtil.getStereotypeApplication(subtypeClass, Category.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, Relator.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, Kind.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, Property.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, AbstractEvent.class) != null){
					return subtypeStereotypeName;
				}// direct wrong subtype
				String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSubtype(subtypeClass);
				if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
					return indirectWrongSupertypeStereotypeName;
				}
			}
		}
		return null;
	}

}
