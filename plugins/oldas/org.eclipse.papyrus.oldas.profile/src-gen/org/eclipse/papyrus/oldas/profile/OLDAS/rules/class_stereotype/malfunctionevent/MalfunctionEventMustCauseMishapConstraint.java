package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.malfunctionevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.MishapEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MalfunctionEventMustCauseMishapConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class malfunctionEventClass = (Class) ctx.getTarget();
		MalfunctionEvent malfunctionEventStereotype = UMLUtil.getStereotypeApplication(malfunctionEventClass, MalfunctionEvent.class);
		if(malfunctionEventStereotype == null){
			return null;
		}
		if(directlyOrIndirectlyConnectedToMishapEvent(malfunctionEventClass)) {
			return null;
		}
		return ctx.createFailureStatus(malfunctionEventClass.getName()+" class stereotyped as MalfunctionEvent must be"
				+ " connected direclty or indireclty through at least one «Causes» relation to a «MishapEvent» class");
	}
	
	private boolean directlyOrIndirectlyConnectedToMishapEvent(Class malfunctionEventClass) {
		for(Association association: malfunctionEventClass.getAssociations()) {
			Causes causesStereotype = UMLUtil.getStereotypeApplication(association, Causes.class);
			if(causesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != malfunctionEventClass 
							&& memberEnd.isNavigable()) {
						if( UMLUtil.getStereotypeApplication(memberEnd.getType(), MishapEvent.class) != null) {
							return true;
						}// directly connected
						if(directlyOrIndirectlyConnectedToMishapEvent((Class) memberEnd.getType())) {
							return true;
						} // indirectly connected through a caused event
					}
				}
			}
		}
	
		for(Classifier parent:malfunctionEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToMishapEvent((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		
		return false;
	}

}
