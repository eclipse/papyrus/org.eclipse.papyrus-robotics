/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage#getMetric()
 * @model
 * @generated
 */
public interface Metric extends EndurantClass {
} // Metric
