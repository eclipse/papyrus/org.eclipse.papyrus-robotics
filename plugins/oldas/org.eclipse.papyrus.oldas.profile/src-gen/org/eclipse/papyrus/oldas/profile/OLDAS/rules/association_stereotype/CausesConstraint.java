package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Causes;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CausesConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association causesAssociation = (Association) ctx.getTarget();
		Causes causesStereotype = UMLUtil.getStereotypeApplication(causesAssociation, Causes.class);
		if(causesStereotype == null) {
			return null;
		}
		for (org.eclipse.uml2.uml.Property memberEnd: causesAssociation.getMemberEnds()) {
			Type classFrom = (memberEnd.isNavigable()?memberEnd.getOtherEnd().getType():memberEnd.getType());
			Type classTo = (memberEnd.isNavigable()?memberEnd.getType():memberEnd.getOtherEnd().getType());
			
			if(UMLUtil.getStereotypeApplication(memberEnd.getType(), AbstractEvent.class) == null) {
				return ctx.createFailureStatus("The «Causes» association from the class "+classFrom.getName()
						+" to the class "+classTo.getName()+" must connect an event to another event.");
			}
		}
		return null;
	}
}