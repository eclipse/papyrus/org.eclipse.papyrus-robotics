package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.relator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Mediates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class RelatorSumOppositeEndsCardinalitiesConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class relatorClass = (Class) ctx.getTarget();
		Relator relatorStereotype = UMLUtil.getStereotypeApplication(relatorClass, Relator.class);
		if(relatorStereotype == null){
			return null;
		}
		if(sumOppositeEndsMediatesAssociation(relatorClass) >= 2) {
			return null;
		}
		
		return ctx.createFailureStatus(relatorClass.getName()+" class stereotyped as "+relatorClass.getAppliedStereotypes().get(0).getName()+" sum of the minimum cardinalities of the opposite ends of the mediations connected (directly or indirectly) to this relator must be greater or equal to 2.");

	}

	private int sumOppositeEndsMediatesAssociation(Class relatorClass) {
		int sum = 0;
		for(Association association: relatorClass.getAssociations()) {
			Mediates mediatesStereotype = UMLUtil.getStereotypeApplication(association, Mediates.class);
			if(mediatesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					EndurantClass endurantStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class);
					boolean isEndurant = endurantStereotype != null;
					if(memberEnd.getType()!=relatorClass && isEndurant && memberEnd.isNavigable()) {
						sum += memberEnd.lowerBound();
					}
				}
			}
		}//direclty connected
		
		for(Classifier parent:relatorClass.parents()) {
			sum += sumOppositeEndsMediatesAssociation((Class) parent);
		}//indirectly connected
		return sum ;
	}

}
