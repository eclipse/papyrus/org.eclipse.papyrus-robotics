package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.state;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Category;
import org.eclipse.papyrus.oldas.profile.OLDAS.State;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class StateCannotDirectSubtypeCategoryConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class stateClass = (Class) ctx.getTarget();
		State stateStereotype = UMLUtil.getStereotypeApplication(stateClass, State.class);
		if(stateStereotype == null) {
			return null;
		}
		
		for(Classifier supertypeClass: stateClass.getGenerals()) {
			if(supertypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
				return null;
			}
			Category categoryStereotype = UMLUtil.getStereotypeApplication(supertypeClass, Category.class);
			if( categoryStereotype != null){
				return ctx.createFailureStatus(stateClass.getName()+" class stereotyped as State cannot directlty subtype a <<Category>>");
			}
		}
		return null;
	}

}
