package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal;
import org.eclipse.papyrus.oldas.profile.OLDAS.ChangesFrom;
import org.eclipse.papyrus.oldas.profile.OLDAS.ChangesTo;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.uml2.uml.Class;

public class ChangesFromChangesToConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association changesFromChangesToAssociation = (Association) ctx.getTarget();
		ChangesFrom changesFromStereotype = UMLUtil.getStereotypeApplication(changesFromChangesToAssociation, ChangesFrom.class);
		ChangesTo changesToStereotype = UMLUtil.getStereotypeApplication(changesFromChangesToAssociation, ChangesTo.class);
		
		boolean isChangesFrom;
		if(changesFromStereotype != null) {
			isChangesFrom = true;
		}
		else if(changesToStereotype != null){
			isChangesFrom = false;
		}
		else {
			return null;
		}
		
		Class oldasEventClass = null;
		Class oldasAntiRigidSortalClass = null;
		for (org.eclipse.uml2.uml.Property memberEnd: changesFromChangesToAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), AntiRigidSortal.class) == null && memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), AbstractEvent.class) == null && !memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «"+(isChangesFrom?"ChangesFrom":"ChangesTo")+"» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must connect an event to an endurant.");
			}
			
			if(memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «"+(isChangesFrom?"ChangesFrom":"ChangesTo")+"» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a target multiplicity in the range  1..* .");
			}
			
			if(!memberEnd.isNavigable() && memberEnd.getLower() == 0) {
				return ctx.createFailureStatus("The «"+(isChangesFrom?"ChangesFrom":"ChangesTo")+"» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must have a source multiplicity in the range  1..* .");
			}
			
			if(!memberEnd.isNavigable()) {
				oldasEventClass = (Class) memberEnd.getType();
				oldasAntiRigidSortalClass = (Class) memberEnd.getOtherEnd().getType();
			}
		}
		
		if(oldasEventClass != null && !hasComplementAssociation(oldasEventClass, changesFromChangesToAssociation, isChangesFrom)) {
			return ctx.createFailureStatus("The «"+(isChangesFrom?"ChangesFrom":"ChangesTo")+"» association from the class "+oldasEventClass.getName()
					+" to the class "+oldasAntiRigidSortalClass.getName()+" must have a connection to a  «"+(isChangesFrom?"ChangesTo":"ChangesFrom")+"» association.");
		}
		return null;
	}

	private boolean hasComplementAssociation(Class oldasEventClass, Association changesFromChangesToAssociation, boolean isChangesFrom) {
		for(Association association: oldasEventClass.getAssociations()) {
			if(isChangesFrom && UMLUtil.getStereotypeApplication(association, ChangesTo.class) != null) {
				return true;
			}
			if(!isChangesFrom && UMLUtil.getStereotypeApplication(association, ChangesFrom.class) != null) {
				return true;
			}
		}
		return false;
	}//TODO verify otherMemberEnd typed AntiRigidSortal in generalization set.
}
