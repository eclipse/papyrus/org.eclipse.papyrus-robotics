package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.antirigidsortal;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.AntiRigidSortal;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AntiRigidSortalSupertypeRestrictionsConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class antirigidsortalClass = (Class) ctx.getTarget();
		AntiRigidSortal antiRigidSortalStereotype = UMLUtil.getStereotypeApplication(antirigidsortalClass, AntiRigidSortal.class);
		if(antiRigidSortalStereotype == null) {
			return null;
		}
		String wrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(antirigidsortalClass);
		if(wrongSupertypeStereotypeName != null) {
			String stereotypeName = antirigidsortalClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(antirigidsortalClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSupertypeStereotypeName+" as a direct or indirect super-type");
		}
		return null;
	}
	
	private String getWrongDirectOrIndirectSupertype(Classifier oldasClass) {
		for(Classifier supertypeClass: oldasClass.getGenerals()) {
			if(supertypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
				return null;
			}
			String supertypeStereotypeName = supertypeClass.getAppliedStereotypes().get(0).getName();
			if( UMLUtil.getStereotypeApplication(supertypeClass, AbstractEvent.class) != null ){
				return supertypeStereotypeName;
			}// direct wrong supertype
			String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(supertypeClass);
			if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
				return indirectWrongSupertypeStereotypeName;
			}
		}
		return null;
	}

}
