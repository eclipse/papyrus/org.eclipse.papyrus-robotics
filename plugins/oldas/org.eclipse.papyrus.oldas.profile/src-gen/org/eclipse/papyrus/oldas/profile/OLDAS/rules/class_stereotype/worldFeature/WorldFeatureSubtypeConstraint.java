package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.worldFeature;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Category;
import org.eclipse.papyrus.oldas.profile.OLDAS.Metric;
import org.eclipse.papyrus.oldas.profile.OLDAS.Property;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.papyrus.oldas.profile.OLDAS.WorldFeature;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class WorldFeatureSubtypeConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class worldFeatureClass = (Class) ctx.getTarget();
		WorldFeature worldFeatureStereotype = UMLUtil.getStereotypeApplication(worldFeatureClass, WorldFeature.class);
		if(worldFeatureStereotype == null) {
			return null;
		}
		String wrongSubtypeStereotypeName = getWrongDirectOrIndirectSubtype(worldFeatureClass);
		if(wrongSubtypeStereotypeName != null) {
			String stereotypeName = worldFeatureClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(worldFeatureClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSubtypeStereotypeName+" as a direct or indirect sub-type");
		}
		return null;
	}

	private String getWrongDirectOrIndirectSubtype(Classifier oldasClass) {
		//System.out.println("LOG WorldFeatureSubtypeConstraint Class:"+oldasClass.getName()+" n generalization:"+oldasClass.getGeneralizations().size());
		for(Relationship generalization:oldasClass.getRelationships()) {
			if(generalization instanceof Generalization) {
				Classifier subtypeClass = ((Generalization) generalization).getSpecific();
				if(subtypeClass == oldasClass) {//prevent from looping
					continue;
				}
				if(subtypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
					return null;
				}
				
				if( UMLUtil.getStereotypeApplication(subtypeClass, Category.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, Relator.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, Property.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, Metric.class) != null ||
					UMLUtil.getStereotypeApplication(subtypeClass, AbstractEvent.class) != null){
					
					String subtypeStereotypeName = subtypeClass.getAppliedStereotypes().get(0).getName();
					return subtypeStereotypeName;
				}// direct wrong subtype
				String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSubtype(subtypeClass);
				if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
					return indirectWrongSupertypeStereotypeName;
				}
			}
		}
		return null;
	}

}
