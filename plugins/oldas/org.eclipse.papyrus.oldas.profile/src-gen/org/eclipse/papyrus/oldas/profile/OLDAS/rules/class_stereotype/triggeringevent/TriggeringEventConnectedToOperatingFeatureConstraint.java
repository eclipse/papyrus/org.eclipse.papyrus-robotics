package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.triggeringevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.OperatingFeature;
import org.eclipse.papyrus.oldas.profile.OLDAS.Participates;
import org.eclipse.papyrus.oldas.profile.OLDAS.TriggeringEvent;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class TriggeringEventConnectedToOperatingFeatureConstraint extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class triggeringEventClass = (Class) ctx.getTarget();
		TriggeringEvent triggeringEventStereotype = UMLUtil.getStereotypeApplication(triggeringEventClass, TriggeringEvent.class);
		if(triggeringEventStereotype == null){
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToOperatingFeature(triggeringEventClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(triggeringEventClass.getName()+" class stereotyped as TriggeringEvent must be"
				+ " connected directly or indirectly through at least one incoming «Participates» relation to a «OperatingFeature» class");
	}

	private boolean directlyOrIndirectlyConnectedToOperatingFeature(Class triggeringEventClass) {
		for(Association association: triggeringEventClass.getAssociations()) {
			Participates participatesStereotype = UMLUtil.getStereotypeApplication(association, Participates.class);
			if(participatesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != triggeringEventClass 
							&& memberEnd.getOtherEnd().isNavigable()) {//filter on incoming event
						if(UMLUtil.getStereotypeApplication(memberEnd.getType(), OperatingFeature.class) != null) {
							return true;
						}// directly connected
						if(isOperatingFeatureSupertype((Class) memberEnd.getType())) {
							return true;
						} // directly connected to an operatingFeature supertype 
					}
				}
			}
		}
	
		for(Classifier parent:triggeringEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToOperatingFeature((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		
		return false;
	}

	private boolean isOperatingFeatureSupertype(Class oldasClass) {
		if(UMLUtil.getStereotypeApplication(oldasClass, OperatingFeature.class) != null) {
			return true;
		}
		for(Relationship generalization:oldasClass.getRelationships()) {
			if(generalization instanceof Generalization) {
				Classifier subtypeClass = ((Generalization) generalization).getSpecific();
				if(subtypeClass == oldasClass) {//prevent from looping
					continue;
				}
				if(isOperatingFeatureSupertype((Class) subtypeClass)) {//avoid errors with classes with no stereotypes
					return true;
				}
			}
		}
		return false;
	}

}
