package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.malfunctionevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AutomationFeature;
import org.eclipse.papyrus.oldas.profile.OLDAS.MalfunctionEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.Participates;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MalfunctionConnectedToAutomationFeatureConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class malfunctionEventClass = (Class) ctx.getTarget();
		MalfunctionEvent malfunctionEventStereotype = UMLUtil.getStereotypeApplication(malfunctionEventClass, MalfunctionEvent.class);
		if(malfunctionEventStereotype == null){
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToAutomationFeature(malfunctionEventClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(malfunctionEventClass.getName()+" class stereotyped as MalfunctionEvent must be"
				+ " connected directly or indirectly through at least one incoming «Participates» relation to a «AutomationFeature» class");
	}

	private boolean directlyOrIndirectlyConnectedToAutomationFeature(Class malfunctionEventClass) {
		for(Association association: malfunctionEventClass.getAssociations()) {
			Participates participatesStereotype = UMLUtil.getStereotypeApplication(association, Participates.class);
			if(participatesStereotype != null) {
				for (org.eclipse.uml2.uml.Property memberEnd: association.getMemberEnds()) {
					if(memberEnd.getType() != malfunctionEventClass 
							&& memberEnd.getOtherEnd().isNavigable()) {//filter on incoming event
						if(UMLUtil.getStereotypeApplication(memberEnd.getType(), AutomationFeature.class) != null) {
							return true;
						}// directly connected
						if(isAutomationFeatureSupertype((Class) memberEnd.getType())) {
							return true;
						} // directly connected to an operatingFeature supertype 
					}
				}
			}
		}
	
		for(Classifier parent:malfunctionEventClass.parents()) {
			if(directlyOrIndirectlyConnectedToAutomationFeature((Class) parent)) {
				return true;
			}
		}// indirectly connected through a parent
		
		return false;
	}

	private boolean isAutomationFeatureSupertype(Class oldasClass) {
		if(UMLUtil.getStereotypeApplication(oldasClass, AutomationFeature.class) != null) {
			return true;
		}
		for(Relationship generalization:oldasClass.getRelationships()) {
			if(generalization instanceof Generalization) {
				Classifier subtypeClass = ((Generalization) generalization).getSpecific();
				if(subtypeClass == oldasClass) {//prevent from looping
					continue;
				}
				if(isAutomationFeatureSupertype((Class) subtypeClass)) {//avoid errors with classes with no stereotypes
					return true;
				}
			}
		}
		return false;
	}

}
