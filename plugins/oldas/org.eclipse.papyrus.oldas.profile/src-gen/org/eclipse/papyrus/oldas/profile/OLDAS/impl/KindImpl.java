/**
 */
package org.eclipse.papyrus.oldas.profile.OLDAS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.oldas.profile.OLDAS.Kind;
import org.eclipse.papyrus.oldas.profile.OLDAS.OLDASPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Kind</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class KindImpl extends EndurantClassImpl implements Kind {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KindImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OLDASPackage.Literals.KIND;
	}

} //KindImpl
