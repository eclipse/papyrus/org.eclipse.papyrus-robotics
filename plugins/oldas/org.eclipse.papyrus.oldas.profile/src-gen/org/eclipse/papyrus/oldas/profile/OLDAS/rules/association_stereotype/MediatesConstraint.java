package org.eclipse.papyrus.oldas.profile.OLDAS.rules.association_stereotype;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.papyrus.oldas.profile.OLDAS.Mediates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.util.UMLUtil;

public class MediatesConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association mediatesAssociation = (Association) ctx.getTarget();
		Mediates mediatesStereotype = UMLUtil.getStereotypeApplication(mediatesAssociation, Mediates.class);
		if(mediatesStereotype == null) {
			return null;
		}
		for (org.eclipse.uml2.uml.Property memberEnd: mediatesAssociation.getMemberEnds()) {
			if((UMLUtil.getStereotypeApplication(memberEnd.getType(), Relator.class) == null && !memberEnd.isNavigable())
				|| (UMLUtil.getStereotypeApplication(memberEnd.getType(), EndurantClass.class) == null && memberEnd.isNavigable())) {
				return ctx.createFailureStatus("The «Mediates» association from the class "+memberEnd.getOtherEnd().getType().getName()
						+" to the class "+memberEnd.getType().getName()+" must be connected from a «Relator» to an endurant class.");
			}
			
			if(memberEnd.getLower() == 0 ||  memberEnd.getUpper() == 0) {
				//System.out.println("upper:"+memberEnd.getUpper()+" "
				//					+memberEnd.getOtherEnd().getType().getName()+" "
				//					+memberEnd.getType().getName());
				return ctx.createFailureStatus("The «Mediates» association from the class "
						+(memberEnd.isNavigable()?memberEnd.getType().getName():memberEnd.getOtherEnd().getType().getName())
						+" to the class "
						+(memberEnd.isNavigable()?memberEnd.getOtherEnd().getType().getName():memberEnd.getType().getName())
						+" must have a "+ (memberEnd.isNavigable()?"source":"target")
						+" multiplicity in the range 1..* .");
			}
				
		}
		return null;
	}
}