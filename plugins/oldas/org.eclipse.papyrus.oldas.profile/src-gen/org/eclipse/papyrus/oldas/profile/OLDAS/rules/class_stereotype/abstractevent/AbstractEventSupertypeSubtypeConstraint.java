package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.abstractevent;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.AbstractEvent;
import org.eclipse.papyrus.oldas.profile.OLDAS.EndurantClass;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.util.UMLUtil;

public class AbstractEventSupertypeSubtypeConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class abstractEventClass = (Class) ctx.getTarget();
		AbstractEvent abstractEventStereotype = UMLUtil.getStereotypeApplication(abstractEventClass, AbstractEvent.class);
		if(abstractEventStereotype == null){
			return null;
		}
		
		String wrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(abstractEventClass);
		if(wrongSupertypeStereotypeName != null) {
			String stereotypeName = abstractEventClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(abstractEventClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSupertypeStereotypeName+" as a direct or indirect super-type.");
		}
		
		String wrongSubtypeStereotypeName = getWrongDirectOrIndirectSubtype(abstractEventClass);
		if(wrongSubtypeStereotypeName != null) {
			String stereotypeName = abstractEventClass.getAppliedStereotypes().get(0).getName();
			return ctx.createFailureStatus(abstractEventClass.getName()+" class stereotyped as "+stereotypeName+" cannot have a class stereotyped "+wrongSubtypeStereotypeName+" as a direct or indirect sub-type.");
		}
		return null;
	}
	
	private String getWrongDirectOrIndirectSupertype(Classifier oldasClass) {
		for(Classifier supertypeClass: oldasClass.getGenerals()) {
			if(supertypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
				return null;
			}
			String supertypeStereotypeName = supertypeClass.getAppliedStereotypes().get(0).getName();
			if( UMLUtil.getStereotypeApplication(supertypeClass, EndurantClass.class) != null ){
				return supertypeStereotypeName;
			}// direct wrong supertype
			String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSupertype(supertypeClass);
			if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
				return indirectWrongSupertypeStereotypeName;
			}
		}
		return null;
	}
	
	private String getWrongDirectOrIndirectSubtype(Classifier oldasClass) {
		//System.out.println("LOG WorldFeatureSubtypeConstraint Class:"+oldasClass.getName()+" n generalization:"+oldasClass.getGeneralizations().size());
		for(Relationship generalization:oldasClass.getRelationships()) {
			if(generalization instanceof Generalization) {
				Classifier subtypeClass = ((Generalization) generalization).getSpecific();
				if(subtypeClass == oldasClass) {//prevent from looping
					continue;
				}
				if(subtypeClass.getAppliedStereotypes().size() == 0) {//avoid errors with classes with no stereotypes
					return null;
				}
				String subtypeStereotypeName = subtypeClass.getAppliedStereotypes().get(0).getName();
				if( UMLUtil.getStereotypeApplication(subtypeClass, EndurantClass.class) != null){
					return subtypeStereotypeName;
				}// direct wrong subtype
				String indirectWrongSupertypeStereotypeName = getWrongDirectOrIndirectSubtype(subtypeClass);
				if(indirectWrongSupertypeStereotypeName != null) { //indirect wrong supertype
					return indirectWrongSupertypeStereotypeName;
				}
			}
		}
		return null;
	}

}
