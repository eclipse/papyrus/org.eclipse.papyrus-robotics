package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.role;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.Mediates;
import org.eclipse.papyrus.oldas.profile.OLDAS.Relator;
import org.eclipse.papyrus.oldas.profile.OLDAS.Role;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class RoleConnectedToRelatorConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Class roleClass = (Class) ctx.getTarget();
		Role roleStereotype = UMLUtil.getStereotypeApplication(roleClass, Role.class);
		if(roleStereotype == null) {
			return null;
		}
		
		if(directlyOrIndirectlyConnectedToRelator(roleClass)) {
			return null;
		}
		
		return ctx.createFailureStatus(roleClass.getName()+" class stereotyped as "+roleClass.getAppliedStereotypes().get(0).getName()+" must be connected through at least one <<Mediates>> relation to a <<Relator>> class");
	}

	private boolean directlyOrIndirectlyConnectedToRelator(Class roleClass) {
		for(Association association: roleClass.getAssociations()) {
			Mediates mediatesStereotype = UMLUtil.getStereotypeApplication(association, Mediates.class);
			if(mediatesStereotype != null) {
				for (Property memberEnd: association.getMemberEnds()) {
					Relator relatorStereotype = UMLUtil.getStereotypeApplication(memberEnd.getType(), Relator.class);
					if(relatorStereotype != null && memberEnd.getOtherEnd().isNavigable()) {
						return true;
					}
				}
			}
		}//direclty connected
		
		for(Classifier parent:roleClass.parents()) {
			if(directlyOrIndirectlyConnectedToRelator((Class) parent)) {
				return true;
			}
		}//indirectly connected
		return false;
	}

}
