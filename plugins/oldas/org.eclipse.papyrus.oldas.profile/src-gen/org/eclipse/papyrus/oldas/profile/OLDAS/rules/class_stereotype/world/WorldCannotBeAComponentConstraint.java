package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.world;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.World;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

public class WorldCannotBeAComponentConstraint extends AbstractModelConstraint{

	@Override
	public IStatus validate(IValidationContext ctx) {
		Association association = (Association) ctx.getTarget();
		//System.out.println("LOG WorldCannotBeAComponentConstraint from Model "+association.getModel().getName()+" Association:"+association.getName()+" n property:"+association.getMemberEnds().size());
		for(Property f:association.getMemberEnds()) {
			if(f.getType() == null) { continue;}
			World worldStereotype = UMLUtil.getStereotypeApplication(f.getType(), World.class);
			if(worldStereotype != null && f.isComposite()) {
				return ctx.createFailureStatus("<<World>> cannot be a component of another class.");
			}
			//System.out.println("LOG WorldCannotBeAComponentConstraint Property name: "+f.getName()+" Is Composite:"+f.isComposite()+" Class Stereotype:"+f.getType().getAppliedStereotypes().get(0).getName());
		}
		
		return null;
		
	}

}
