package org.eclipse.papyrus.oldas.profile.OLDAS.rules.class_stereotype.world;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.oldas.profile.OLDAS.World;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.util.UMLUtil;


public class ExactlyOneWorldConstraint extends  AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx) {
		Model model = (Model) ctx.getTarget();
		EList<Element> elements = model.getOwnedElements(); //get the list of elements from the model
		int counter = 0;
		for (Element e: elements) {
			if(e.getAppliedStereotypes().size() == 0) {
				continue;
			}
			World worldStereotype = UMLUtil.getStereotypeApplication(e, World.class);
			//System.out.println("LOG ExactlyOneWorldConstraint from Model "+model.getName()+" :"+e.getAppliedStereotypes().get(0).getQualifiedName());
			if(worldStereotype != null) {//count the number of element stereotyped "World"
				counter = counter + 1;
			}
		}
		if(counter != 1) { // return an error message if the number of class stereotyped "World" is not exactly one
			return ctx.createFailureStatus("OLDAS diagram must contains exactly one class stereotyped \"World\"");
		}
		return null;
	}

}
