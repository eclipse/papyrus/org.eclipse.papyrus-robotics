# --------------------------------------------------------
# Copyright (c)
#
# contributions by author
#                  author@somewhere.net
# maintained by    maintainer
#                  maintainer@somewhere.net

from py_publishsubscribe_extcode.periodicpublishercompdef.periodicpublisher import PeriodicPublisher
from simple_msgs.msg import Map

class PeriodicPublisher_impl(PeriodicPublisher):

	def __init__(self, options):
		super().__init__(options)

	def fPublishing(self):
		transMap = Map()
		transMap.ogm.width = 16
		transMap.ogm.height = 16
		transMap.ogm.resolution = 0.1

		self.get_logger().info('Publishing map...')
		self.pMap_pub_.publish(transMap)
