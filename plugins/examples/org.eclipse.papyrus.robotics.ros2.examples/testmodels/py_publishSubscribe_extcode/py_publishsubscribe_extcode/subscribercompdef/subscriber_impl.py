# --------------------------------------------------------
# Copyright (c)
#
# contributions by author
#                  author@somewhere.net
# maintained by    maintainer
#                  maintainer@somewhere.net

from py_publishsubscribe_extcode.subscribercompdef.subscriber import Subscriber

class Subscriber_impl(Subscriber):

	def __init__(self, options):
		super().__init__(options)

	def fListening(self, commobj):
		print("Map data received")
		print("width: " + str(commobj.ogm.width))
		print("height: " + str(commobj.ogm.height))
		print("resolution: " + str(commobj.ogm.resolution))
