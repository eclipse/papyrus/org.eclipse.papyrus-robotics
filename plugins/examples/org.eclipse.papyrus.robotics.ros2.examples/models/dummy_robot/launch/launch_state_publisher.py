from launch import LaunchDescription
from launch_ros.actions import LifecycleNode
from ament_index_python.packages import get_package_share_directory

share_dir = get_package_share_directory('dummy_robot')

# Start of user code imports
import os
# End of user code

def generate_entity():
	# define empty user parameter (eventually redefined in the protected section afterwards)
	state_publisher_custom_params = {}

	# Start of user code parameters
	bringup_share = get_package_share_directory('dummy_robot_bringup')
	urdf_filename = os.path.join(bringup_share, 'launch', 'single_rrbot.urdf')
	print (urdf_filename)
	with open(urdf_filename, 'r') as infp:
		robot_desc = infp.read()
	state_publisher_custom_params = { 'robot_description': robot_desc }
	# End of user code
		
	# Add the actions to the launch description.
	return LifecycleNode(
		name='state_publisher',
		package='robot_state_publisher', executable='robot_state_publisher',
		remappings=[
			('joint_states', 'joint_states/JointState/joint_states'), ('tf', 'state_publisher/TFMessage/tf'), ('tf_static', 'state_publisher/TFMessage/tf_static')
		],
		parameters=[share_dir+'/launch/cfg/param.yaml', state_publisher_custom_params],
		output='screen',
		emulate_tty=True	# assure that RCLCPP output gets flushed
	)

def generate_launch_description():

	# Launch Description
	ld = LaunchDescription()
	ld.add_entity(generate_entity())
	
	return ld
