#
# Copyright (c) CEA LIST 2019-2020 All Rights Reserved.
#
#  The content is the property of CEA LIST. Any distribution
#  to third parties is expressly prohibited without the prior
#  written consent of CEA LIST.
#
#  Contributors:
#   CEA LIST / DIASI / LRI
#

import msgpackrpc
import socket
# import numpy as np
# from copy import *
from time import sleep


# from math import sin, cos, pi

class isybotAPI:
    """Sybot API"""

    # ===Connection=====================================================================
    def connectTo(self, HOST="localhost", PORT=9123, timeout=None):
        """Connect to rpc server named HOST at port PORT
        Timeout is expressed in second and is by default desactivated
        Return a client msgpackrpc if ok, return None if an error occured
        """
        try:
            self._addr = msgpackrpc.Address(HOST, PORT, socket.AF_INET)
            self._client = msgpackrpc.Client(self._addr, timeout, unpack_encoding='utf-8')
            print("Connected to " + HOST + " at " + str(PORT) + ".")
            self._HOST = HOST
            self._PORT = PORT
            self._defaultEvtTimeout = 5
            done = True
        except:
            print("An error occurred while connecting via msgpackrpc.")
            self._client = None
            done = False
        return done

    def whose(self):
        """Return IP connection detail"""
        if self._client != None:
            print("This API is connected to " + self._HOST + " at " + str(self._PORT) + ".")
        return [self._HOST, self._PORT]

    def disconnect(self):
        """Close the current connection"""
        if self._client != None:
            self._client.close()
        print("Disconnected from " + self._HOST + " at " + str(self._PORT) + ".")
        self._client = None
        self._addr = None
        self._HOST = None
        self._PORT = None
        return True

    # ===Status functions=================================================================

    def enable(self):
        """Set robot enable to true, turning robot power on"""
        return self._callAPI('enable')

    def disable(self):
        """Set robot enable to false, turning robot power off"""
        return self._callAPI('disable')

    def resetController(self):
        self.disable()
#        print("disable")
        sleep(0.3)
        self.enable()
#        print("enable") 
        self._client.call(self.POW+'.rise.wait', 0) #wait for robot to be ON
#        print("enabled")

    def getPowered(self):
        """Get robot power status"""
        return self._callAPI('getPowered')

    def getRobotMode(self):
        """Get robot current mode index"""
        return self._callAPI('getRobotMode')

    def setRobotMode(self, index):
        """Set robot current mode index"""
        return self._callAPI('setRobotMode', index)

    # ===Movement functions=================================================================

    def getJointPose(self):
        """Get robot articular pose express as [[q1],[q2],[q3],[q4],[q5],[q6]]
        unused axis values are equal to 0.0"""
        return self._callAPI('getJointPose');

    def getJointPoseVisual(self):
        """Get robot articular pose tweak in order to be used for visualization"""
        return self._callAPI('getJointPoseVisu');

    def getCartePose(self):
        """Get robot carte pose express as [[x,y,z],[e1,e2,e3,e4]]
        where [x,y,z] is the position and [e1,e2,e3,e4] is the orientation via quaternions"""
        return self._callAPI('getCartePose');

    def getJointSpeed(self):
        """Get robot articular velocity express as [[dq1],[dq2],[dq3],[dq4],[dq5],[dq6]]"""
        return self._callAPI('getJointSpeed');

    def getCarteSpeed(self):
        """Get robot carte pose express as a kinematic screw"""
        return self._callAPI('getCarteSpeed');

    def moveCarte(self, posCarte):
        """Set robot target carte pose express as [[x,y,z],[e1,e2,e3,e4]]"""
        return self._callAPI('moveCarte', posCarte)

    def moveJoint(self, jointPose):
        """Set robot target articular pose express as [[q1],[q2],[q3],[q4],[q5],[q6]]"""
        return self._callAPI('moveJoint', jointPose)

    def openGripper(self):
        sleep(0.5) # pretended gripper opening time
        print("Gripper Open")

    def closeGripper(self):
        sleep(0.5) # pretended gripper closing time
        print("Gripper Closed")

    # TODO
    def setInhibDrives(self, inhibs):
        self._setRcrVar(self.ROBOT + '.io.inhibDrive', inhibs)

    def getInhibDrives(self):
        return self._getRcrVar(self.ROBOT + '.io.inhibDrive')

    def setGTArtiSpeedRatio(self, dqRatio):
        self._setRcrVar(self.GTARTI + '.dqRatio', dqRatio)

    def getGTArtiSpeedRatio(self):
        return self._getRcrVar(self.GTARTI + '.dqRatio')

    def setGTArtiSpeedMax(self, dqMax):
        self._setRcrVar(self.GTARTI + '.dqMax', dqMax)

    def getGTArtiSpeedMax(self):
        return self._getRcrVar(self.GTARTI + '.dqMax')

    # === Cartesian Spline mode functions==========================================================
    def setGTCarteMode(self, idx):
        return self._callAPI('setGTCarteMode', idx)

    def getGTCarteMode(self):
        return self._callAPI('getGTCarteMode')

    def splineRecorderOFF(self):
        """Turn off spline recording of the current trajectory"""
        return self._callAPI('splineRecorderOFF')

    def splineRecorderON(self):
        """Turn on spline recording of the current trajectory"""
        return self._callAPI('splineRecorderON')

    def splineResetPosition(self, sReset):
        """TODO """
        return self._callAPI('splineResetPosition', sReset)

    def splineGetLength(self):
        """Return spline length"""
        return self._callAPI('splineGetSmax')

    def splineDataSize(self):
        return self._callAPI('splineDataSize')

    def splineGenTraj(self):
        return self._callAPI('splineGenTraj')

    def splineGetEndPts(self):
        return self._callAPI('splineGetEndPts')

    def setSplineSpeed(self, dqRatio):
        return self._callAPI('setSplineSpeed', dqRatio)

    def moveSpline(self, sTarget):
        """TODO """
        return self._callAPI('moveSpline', sTarget)

    # === Restrictive workspace Functions===!!! WIP !!!============================================

    def repelWorkspacePAUSE(self):
        """Turn off repel forces as long as the robot is not back inside the restrictive workspace.
        Use it when you have to enable the robot when a workspace is set to avoid sudden mouvement"""
        return self._callAPI('repelWorkspacePAUSE')

    def repelWorkspaceON(self):
        return self._callAPI('repelWorkspaceON')

    def repelWorkspaceOFF(self):
        return self._callAPI('repelWorkspaceOFF')

    def setRepelWorkspaceLim(self, limits):
        return self._callAPI('setRepelWorkspaceLim', limits)

    def resetRepelWorkspaceLim(self):
        return self._callAPI('resetRepelWorkspaceLim')

    # ===Virtual Guide Functions===!!! WIP !!!============================================

    def updateVG(self, xList):
        """Update the spline used for the virtual guide by giving a vector of control points
        Points are written as  [[x,y,z],[e1,e2,e3,e4]]"""
        return self._callAPI('setTrajVG', xList)

    def getVGlength(self):
        """Return the length of the virtual guide"""
        return self._callAPI('getVGlength')

    # ===LEDs management functions=========================================================

    def forceLEDstateTo(self, LEDidx, state):
        return self._callAPI('forceLEDstateTo', LEDidx, state)

    def inhibLEDdefaultState(self, LEDidx, state):
        return self._callAPI('inhibLEDdefaultState', LEDidx, state)

    # ===Parameters set/get================================================================
    def setParam(self, type, idx, value):
        self._setRcrVar(self.API + '.PARAM_' + type + '_' + str(idx), value)

    def getParam(self, type, idx):
        return self._getRcrVar(self.API + '.PARAM_' + type + '_' + str(idx))

    # ===Event handling Functions==========================================================
    """Example of event function use :
    
    go to initial arti pose
    robot.setRobotMode(robot.POSARTI_MODE) #switch mode
    print("in POSARTI_MODE")
    evt = robot.watch(robot.GTARTI+'.achievedEvt', 60)
    robot.goToPosArti([[0.0],[0.0],[-1.57],[1.57],[0.0],[0.0]])
    robot.wait(evt) #wait for robot to reach target
    print("Initial pose reach")
    """

    def watch(self, evtName, *timeout):
        """Start watching for the trigger of a given event. Return an handle on that surveillance
        timeout is used to avoid waiting indefinitely. If no given timeout, default one is used"""
        if not timeout:
            evt = self._client.call_async(evtName + '.wait', self._defaultEvtTimeout)
        else:
            evt = self._client.call_async(evtName + '.wait', *timeout)
        return evt

    def wait(self, evt):
        """Wait for the given event to be trigger or for the timeout to be elapsed after
        the given timeout in the watch function.
        Return true when event is triggered, or false if timeout elapses"""
        return evt.get()

    def moveCarteSynchWait(self):
        self._client.call(self.GTCARTE+'.achievedEvt.wait', 0)

    def moveJointSynchWait(self):
        self._client.call(self.GTARTI+'.achievedEvt.wait', 0)

    # ===Private===========================================================================
    def _setRcrVar(self, rcrName, value):
        # """Set recor variable to a given value
        # rcrName is recor name of the variable written according to the following exemple
        # isb16_cont_robot_X_mes ==> cont.robot.X_mes"""
        self._client.call(rcrName + '.set', value);

    def _getRcrVar(self, rcrName):
        # """Get recor variable value
        # rcrName is recor name of the variable written according to the following exemple
        # isb16_cont_robot_X_mes ==> cont.robot.X_mes"""
        return self._client.call(rcrName + '.get');

    def _callAPI(self, funcName, *args):
        # """Private function embedding the msgpackrpc services call
        # Return the output value of the called service
        # Display any existing message return by the service"""
        try:
            assert (self._client != None)
            if not args:
                apiOutput = self._client.call(self.API + '.api_' + funcName)
            else:
                apiOutput = self._client.call(self.API + '.api_' + funcName, *args)
            isDeny = apiOutput[2]
            if not isDeny:
                val = apiOutput[0]
            else:
                val = None
            errMsg = apiOutput[1]
            if (errMsg != ""):
                print(errMsg)
        except msgpackrpc.error.RPCError:
            val = None
            print('Error: invalid argument')
        except AssertionError:
            val = None
            print('Error: Not connected to any controller')
        return val

    # list of accessible access path
    PROXY = 'evtProxy'
    CONT = 'cont'

    POW = CONT + '.pow'
    FAILURE = POW + '.failState'
    BPMBT = CONT + '.bpm'
    TOOLBT = CONT + '.bpTool'
    BPABT = CONT + '.bpa'
    POSARTI = CONT + '.posArti'
    POSCARTE = CONT + '.posCarte'
    API = CONT + '.api'
    GTARTI = POSARTI + '.gt'
    GTARTITARGET = GTARTI + '.qTargetSet'
    GTCARTE = POSCARTE + '.gt'
    GTCARTETARGET = GTCARTE + '.XTargetSet'
    GTSPLINE = POSCARTE + '.curvilignAbscissaGT'
    GTSPLINETARGET = GTSPLINE + '.qTargetSet'
    ROBOTENABLE = CONT + '.enable'

    MODESWITCH = CONT + '.mode'
    JOINT_MODE = 0
    CARTE_MODE = 1
    FREE_MODE = 2
    VG_MODE = 3

    CARTE_GT = 0
    SPLINE_GT = 1

    ROBOT = CONT + '.robot'
    QROBOT = ROBOT + '.mes_q'
    XROBOT = ROBOT + '.mes_X'
    TOOL = CONT + '.tool'
    XTOOL = TOOL + '.mes_Xt'
    API = CONT + '.api'

    def __init__(self):
        self._client = None
        self._addr = None
        self._HOST = None
        self._defaultEvtTimeout = 5  # for safety
        self._PORT = None

