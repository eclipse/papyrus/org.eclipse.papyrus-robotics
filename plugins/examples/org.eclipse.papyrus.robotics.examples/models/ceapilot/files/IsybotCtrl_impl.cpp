// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#define IsybotCtrlCompdef_IsybotCtrl_impl_BODY

/************************************************************
 IsybotCtrl_impl class body
 ************************************************************/
#include <vector>

// include associated header file
#include "IsybotCtrlCompdef/IsybotCtrl_impl.h"

// Derived includes directives
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

extern int
initialize_controller(
    void
);

extern int
shutdown_controller(
    void
);

extern int
moveToCartePos_algorithm(
    double, double, double,
    double, double, double, double
);

extern int
moveToJointPos_algorithm(
    std::vector<double>&
);

extern int
openGripper_algorithm(
    void
);

extern int
closeGripper_algorithm(
    void
);

namespace IsybotCtrlCompdef {

// static attributes (if any)

/**
 * 
 * @param options 
 */
IsybotCtrl_impl::IsybotCtrl_impl(rclcpp::NodeOptions /*in*/options) :
		IsybotCtrl(options) {
    initialize_controller();
    RCLCPP_INFO(this->get_logger(), "Controller initialized");
}

/**
 * 
 * @param uuid 
 * @param goal 
 * @return return 
 */
rclcpp_action::GoalResponse IsybotCtrl_impl::moveToCartePos_goal(
		const rclcpp_action::GoalUUID & /*in*/uuid,
		std::shared_ptr<
				const picknplacecoordservicedef::action::MoveToPoseCS::Goal> /*in*/goal) {
    (void)uuid;
    if (goal == nullptr)
        return rclcpp_action::GoalResponse::REJECT;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    
}

/**
 * 
 * @param goal_handle 
 * @return return 
 */
rclcpp_action::CancelResponse IsybotCtrl_impl::moveToCartePos_cancel(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::MoveToPoseCS>> /*in*/goal_handle) {
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
}

/**
 * 
 * @param goal_handle 
 */
void IsybotCtrl_impl::moveToCartePos_accepted(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::MoveToPoseCS>> /*in*/goal_handle) {
    const auto goal = goal_handle->get_goal();
    auto p = goal->position;
    auto o = goal->orientation;
    moveToCartePos_algorithm(p.x, p.y, p.z, o.x, o.y, o.z, o.w);
    RCLCPP_INFO(this->get_logger(), "Cartesian Pose reached successfully");
    auto result = std::make_shared<picknplacecoordservicedef::action::MoveToPoseCS::Result>();
    goal_handle->succeed(result);
}

/**
 * 
 * @param uuid 
 * @param goal 
 * @return return 
 */
rclcpp_action::GoalResponse IsybotCtrl_impl::moveToJointPos_goal(
		const rclcpp_action::GoalUUID & /*in*/uuid,
		std::shared_ptr<
				const picknplacecoordservicedef::action::MoveToJointStateCS::Goal> /*in*/goal) {
    (void)uuid;
    if (goal == nullptr)
        return rclcpp_action::GoalResponse::REJECT;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

/**
 * 
 * @param goal_handle 
 * @return return 
 */
rclcpp_action::CancelResponse IsybotCtrl_impl::moveToJointPos_cancel(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::MoveToJointStateCS>> /*in*/goal_handle) {
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
}

/**
 * 
 * @param goal_handle 
 */
void IsybotCtrl_impl::moveToJointPos_accepted(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::MoveToJointStateCS>> /*in*/goal_handle) {
    const auto goal = goal_handle->get_goal();
    auto q = goal->position;
    moveToJointPos_algorithm(q);
    RCLCPP_INFO(this->get_logger(), "Joint Pose reached successfully");
    auto result = std::make_shared<picknplacecoordservicedef::action::MoveToJointStateCS::Result>();
    goal_handle->succeed(result);
}

/**
 * 
 * @param uuid 
 * @param goal 
 * @return return 
 */
rclcpp_action::GoalResponse IsybotCtrl_impl::openGripper_goal(
		const rclcpp_action::GoalUUID & /*in*/uuid,
		std::shared_ptr<
				const picknplacecoordservicedef::action::OpenGripperCS::Goal> /*in*/goal) {
    (void)uuid;
    if (goal == nullptr)
        return rclcpp_action::GoalResponse::REJECT;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

/**
 * 
 * @param goal_handle 
 * @return return 
 */
rclcpp_action::CancelResponse IsybotCtrl_impl::openGripper_cancel(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::OpenGripperCS>> /*in*/goal_handle) {
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
}

/**
 * 
 * @param goal_handle 
 */
void IsybotCtrl_impl::openGripper_accepted(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::OpenGripperCS>> /*in*/goal_handle) {
    openGripper_algorithm();
    auto result = std::make_shared<picknplacecoordservicedef::action::OpenGripperCS::Result>();
    goal_handle->succeed(result);
}

/**
 * 
 * @param uuid 
 * @param goal 
 * @return return 
 */
rclcpp_action::GoalResponse IsybotCtrl_impl::closeGripper_goal(
		const rclcpp_action::GoalUUID & /*in*/uuid,
		std::shared_ptr<
				const picknplacecoordservicedef::action::CloseGripperCS::Goal> /*in*/goal) {
    (void)uuid;
    if (goal == nullptr)
        return rclcpp_action::GoalResponse::REJECT;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

/**
 * 
 * @param goal_handle 
 * @return return 
 */
rclcpp_action::CancelResponse IsybotCtrl_impl::closeGripper_cancel(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::CloseGripperCS>> /*in*/goal_handle) {
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
}

/**
 * 
 * @param goal_handle 
 */
void IsybotCtrl_impl::closeGripper_accepted(
		const std::shared_ptr<
				rclcpp_action::ServerGoalHandle<
						picknplacecoordservicedef::action::CloseGripperCS>> /*in*/goal_handle) {
    closeGripper_algorithm();
    auto result = std::make_shared<picknplacecoordservicedef::action::CloseGripperCS::Result>();
    goal_handle->succeed(result);
}

/**
 * 
 */
void IsybotCtrl_impl::on_cleanup() {
    shutdown_controller();
    RCLCPP_INFO(this->get_logger(), "Controller Shut Down");
}

} // of namespace IsybotCtrlCompdef

/************************************************************
 End of IsybotCtrl_impl class body
 ************************************************************/
