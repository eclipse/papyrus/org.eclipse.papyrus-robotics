cmake_minimum_required(VERSION 3.5.0)
project(ceapilot)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_lifecycle REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(picknplacecoordservicedef REQUIRED)
find_package(ceapilot_skillrealizations REQUIRED)

include_directories(
	# assure that generated .h files are found
	${PROJECT_SOURCE_DIR}/src
	${PROJECT_SOURCE_DIR}/src-gen
)

add_executable(IsybotCtrl
	src-gen/IsybotCtrlCompdef/IsybotCtrl.cpp
	src-gen/IsybotCtrlCompdef/IsybotCtrl_main.cpp
	src/IsybotCtrlCompdef/IsybotCtrl_impl.cpp
)
ament_target_dependencies(IsybotCtrl
	rclcpp
	rclcpp_lifecycle
	rclcpp_action
	picknplacecoordservicedef
	ceapilot_skillrealizations
)


# Start of user code dependencies
ament_python_install_module(scripts/isybotAPI.py)
add_library(isybotcppapi SHARED IMPORTED)
set_target_properties(isybotcppapi PROPERTIES IMPORTED_LOCATION ${PROJECT_SOURCE_DIR}/libs/libisybotcppapi.so)
target_link_libraries(IsybotCtrl isybotcppapi)
install(FILES
    libs/libisybotcppapi.so
    DESTINATION lib/${PROJECT_NAME}
)
# End of user code

install(TARGETS
	IsybotCtrl
	DESTINATION lib/${PROJECT_NAME}
)

# Install launch files.
install(DIRECTORY
	launch
	DESTINATION share/${PROJECT_NAME}/
)
ament_package()
