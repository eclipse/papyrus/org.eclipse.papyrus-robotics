// Generated by Papyrus4Robotics
// ...

#include <string>
#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "sensor_msgs/msg/joint_state.hpp"
#include "behaviortree_cpp_v3/action_node.h"

class ComputeGoalsAction : public BT::SyncActionNode
{

public:
  // Any TreeNode with ports must have a constructor with this signature
  ComputeGoalsAction(
    const std::string& name,
    const BT::NodeConfiguration& config)
  : SyncActionNode(name, config)
  {
  }

  // ComputeGoals has in/out parameters => must provide a providedPorts method
  static BT::PortsList providedPorts()
  {
    return{
      BT::OutputPort<geometry_msgs::msg::Pose>("home"),
      BT::OutputPort<sensor_msgs::msg::JointState>("obj_appr"),
      BT::OutputPort<geometry_msgs::msg::Pose>("dep_appr")
    };
  }

  BT::NodeStatus tick() override
  {


	// Share through the blackboard
	//
	geometry_msgs::msg::Pose home;
	/* -----------------------------------------
	 * USER CODE HERE TO INITIALIZE THE VARIABLE
	 * -----------------------------------------
	 */
	home.position.x    = 0.7998;
    home.position.y    = -0.07;
    home.position.z    = 0.648;
    home.orientation.x = 0.5;
    home.orientation.y = -0.49;
    home.orientation.z = 0.49;
    home.orientation.w = 0.50;
	setOutput("home", home);

	sensor_msgs::msg::JointState obj_appr;
	/* -----------------------------------------
	 * USER CODE HERE TO INITIALIZE THE VARIABLE
	 * -----------------------------------------
	 */
	obj_appr.position = {-0.3,0.6,-1.5,1.0,0.8,0.8};
	setOutput("obj_appr", obj_appr);

	geometry_msgs::msg::Pose dep_appr;
	/* -----------------------------------------
	 * USER CODE HERE TO INITIALIZE THE VARIABLE
	 * -----------------------------------------
	 */
	dep_appr.position.x    = 1.0232925449655361;
    dep_appr.position.y    = 0.3176419508864178;
    dep_appr.position.z    = -0.3897326230388757;
    dep_appr.orientation.x = 0.680309338689832;
    dep_appr.orientation.y = 0.17502526594518417;
    dep_appr.orientation.z = 0.7001948729091994;
    dep_appr.orientation.w = -0.1275637092745863;
	setOutput("dep_appr", dep_appr);

  	return BT::NodeStatus::SUCCESS;
  }

};

#include "behaviortree_cpp_v3/bt_factory.h"

// This function must be implemented in the .cpp file to create
// a plugin that can be loaded at run-time
BT_REGISTER_NODES(factory)
{
    factory.registerNodeType<ComputeGoalsAction>("ComputeGoals");
}
